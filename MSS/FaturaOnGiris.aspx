﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FaturaOnGiris.aspx.cs" Inherits="MSS1.FaturaOnGiris" %>

<%@ Register Src="~/controls/DataTableControlGAC.ascx" TagName="StDataTable" TagPrefix="STDT" %>

<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../css/GAC_MenuStyle.css" rel="stylesheet" />
    <link href="../css/GAC_NewStyle.css?v=V1" rel="stylesheet" />
    <link href="../css/GAC_CardStyle.css?v=V1" rel="stylesheet" />
    <style>
        .dxflGroup {
            padding: 0px 5px;
            width: 100%;
        }

            .dxflGroup tr:first-child > .dxflGroupCell > .dxflGroupBox, .dxflGroup > .dxflChildInFirstRowSys > .dxflGroupCell > .dxflGroupBox {
                margin-top: -5px;
            }

        .dx-vam {
            color: white;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%">
            <asp:HiddenField runat="server" ID="HiddenDocIsSaved" Value="0" />
            <table style="width: 100%; border-width: 0px;">
                <tr>
                    <td style="width: 35%; vertical-align: top">
                        <dx:ASPxFormLayout ID="Box1" runat="server" Width="100%">
                            <Items>
                                <dx:LayoutGroup ShowCaption="False">
                                    <Items>
                                        <dx:LayoutItem FieldName="lblFaturaTuru" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtFaturaTuru" Enabled="false" runat="server" ForeColor="Black" CssClass="textboxStyle" Width="100%" Height="5px"></dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblVendCust" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxComboBox ID="cmbVendCust" DataSourceID="DSVendCust" TextField="Text" ValueField="Code" Theme="Glass"
                                                        ClientInstanceName="cmbVendCust" runat="server" AutoPostBack="true" CssClass="dropdownStyle" Width="100%"
                                                        OnSelectedIndexChanged="cmbVendCust_SelectedIndexChanged">
                                                    </dx:ASPxComboBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblReferansNo" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtReferansNo" runat="server" CssClass="textboxStyle" Width="100%"></dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblSerino" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtSeriNo" runat="server" CssClass="textboxStyle" Width="100%"></dx:ASPxTextBox>

                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblHariciBelgeNo" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtHariciBelgeNo" runat="server" CssClass="textboxStyle" Width="100%"></dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblBelgeTarihi" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxDateEdit ID="txtBelgeTarihi" ClientInstanceName="txtBelgeTarihi" EditFormatString="dd.MM.yyyy" DisplayFormatString="{0:dd.MM.yyyy}" runat="server" CssClass="DropDownList" Theme="Glass" Width="100%">
                                                        <ClientSideEvents DateChanged="function (s,e) { txtDeftereNakilTarihi.SetValue(txtBelgeTarihi.GetValue()); }"
                                                            ValueChanged="function(s, e) { cmbDovizCinsiChanged(s); }" />
                                                    </dx:ASPxDateEdit>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblDeftereNakilTarihi" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxDateEdit ID="txtDeftereNakilTarihi" ClientInstanceName="txtDeftereNakilTarihi" EditFormatString="dd.MM.yyyy" DisplayFormatString="{0:dd.MM.yyyy}" runat="server" CssClass="DropDownList" Theme="Glass" Width="100%">
                                                    </dx:ASPxDateEdit>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>

                                    </Items>
                                    <GroupBoxStyle CssClass="cardBox"></GroupBoxStyle>
                                    <ParentContainerStyle Paddings-Padding="0"></ParentContainerStyle>
                                </dx:LayoutGroup>
                            </Items>
                        </dx:ASPxFormLayout>
                    </td>
                    <td style="width: 35%; vertical-align: top">
                        <dx:ASPxFormLayout ID="Box2" runat="server" Width="100%">
                            <Items>
                                <dx:LayoutGroup ShowCaption="False">
                                    <Items>
                                        <dx:LayoutItem FieldName="lblDovizCinsi" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxComboBox ID="cmbDovizCinsi" runat="server" CssClass="dropdownStyle" Width="100%" DataSourceID="DTCurrency"
                                                        ValueField="Code" TextField="Text" ClientInstanceName="cmbDovizCinsi"
                                                        AutoPostBack="false">
                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) { cmbDovizCinsiChanged(s); }" />
                                                    </dx:ASPxComboBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblDovizKuru" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxCallbackPanel ID="callbackPanelDoviz" runat="server" Width="100%" ClientInstanceName="callbackPanelDoviz" OnCallback="callbackPanelDoviz_Callback"
                                                        SettingsLoadingPanel-Enabled="false">
                                                        <PanelCollection>
                                                            <dx:PanelContent>
                                                                <dx:ASPxSpinEdit ID="txtDovizKuru" ClientInstanceName="txtDovizKuru" runat="server" CssClass="textboxStyle" Width="100%" SpinButtons-ClientVisible="false"
                                                                    NumberType="Float" HorizontalAlign="Right">
                                                                </dx:ASPxSpinEdit>
                                                            </dx:PanelContent>
                                                        </PanelCollection>

                                                    </dx:ASPxCallbackPanel>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblMatrah" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxSpinEdit ID="txtMatrah" ClientInstanceName="txtMatrah" runat="server" CssClass="numbertextboxStyle" Width="100%"
                                                        NumberType="Float" HorizontalAlign="Right"
                                                        SpinButtons-ClientVisible="false">
                                                        <ClientSideEvents ValueChanged="function(s,e){                               
                                                        var _val1,_val2;
                                                        if(txtMatrah.GetNumber()==null) 
                                                        _val1=0;
                                                        else
                                                        _val1=txtMatrah.GetNumber();
                                                        if(txtKdvTutari.GetNumber()==null)
                                                        _val2=0;
                                                        else
                                                        _val2=txtKdvTutari.GetNumber();
                                                        var _valT=parseFloat(_val1)+parseFloat(_val2)
                                                        txtToplamTutar.SetNumber(_valT);
                                                        }" />
                                                    </dx:ASPxSpinEdit>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>

                                        <dx:LayoutItem FieldName="lblKdvTutari" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxSpinEdit ID="txtKdvTutari" ClientInstanceName="txtKdvTutari" runat="server" CssClass="numbertextboxStyle" Width="100%" SpinButtons-ClientVisible="false"
                                                        NumberType="Float" HorizontalAlign="Right">
                                                        <ClientSideEvents ValueChanged="function(s,e){ 
                                                        var _val1,_val2;
                                                        if(txtMatrah.GetNumber()==null) 
                                                        _val1=0;
                                                        else
                                                        _val1=txtMatrah.GetNumber();
                                                        if(txtKdvTutari.GetNumber()==null)
                                                        _val2=0;
                                                        else
                                                        _val2=txtKdvTutari.GetNumber();
                                                        var _valT=parseFloat(_val1)+parseFloat(_val2)
                                                        txtToplamTutar.SetNumber(_valT);
                                                        }" />
                                                    </dx:ASPxSpinEdit>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblToplamTutar" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxSpinEdit ID="txtToplamTutar" ClientInstanceName="txtToplamTutar" runat="server" CssClass="numbertextboxStyle" Width="100%" SpinButtons-ClientVisible="false"
                                                        NumberType="Float" HorizontalAlign="Right">
                                                    </dx:ASPxSpinEdit>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblToplamTutartl" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxSpinEdit ID="txtToplamTutartl" ClientInstanceName="txtToplamTutartl" runat="server" CssClass="numbertextboxStyle" Width="100%" SpinButtons-ClientVisible="false"
                                                        NumberType="Float" HorizontalAlign="Right">
                                                    </dx:ASPxSpinEdit>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>

                                        <dx:LayoutItem FieldName="lblFirmaOdemeVadesi" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtFirmaOdemeVadesi" Enabled="false" runat="server" CssClass="textboxStyle"
                                                        ClientInstanceName="txtFirmaOdemeVadesi" Height="100%"
                                                        ForeColor="Black" Width="100%" EnableCallbackMode="True">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblFaturaOdemeVadesi" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxComboBox ID="cmbFaturaVadesi" runat="server" CssClass="dropdownStyle" Width="100%" Style="border-spacing: 0px; padding: 0px" Height="100%"
                                                        DataSourceID="DTPaymentTerm" TextField="Text" ValueField="Code">
                                                    </dx:ASPxComboBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>

                                        <dx:LayoutItem Visible="false" FieldName="lblchRM" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxCheckBox ID="chRM" runat="server"></dx:ASPxCheckBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                    </Items>
                                    <GroupBoxStyle CssClass="cardBox"></GroupBoxStyle>
                                    <ParentContainerStyle Paddings-Padding="0"></ParentContainerStyle>
                                </dx:LayoutGroup>
                            </Items>

                        </dx:ASPxFormLayout>
                    </td>
                    <td style="width: 30%; vertical-align: top">
                        <dx:ASPxFormLayout ID="Box3" runat="server" Width="100%">
                            <Items>
                                <dx:LayoutGroup ShowCaption="False" Height="182px">
                                    <Items>
                                        <dx:LayoutItem FieldName="lblNot" ShowCaption="True" CaptionSettings-Location="Top">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <asp:TextBox ID="txtNot" runat="server" Width="100%" Height="170px" MaxLength="1000"
                                                        TextMode="MultiLine" CssClass="carditemStyle"></asp:TextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                    </Items>
                                    <GroupBoxStyle CssClass="cardBox"></GroupBoxStyle>
                                    <ParentContainerStyle Paddings-Padding="0"></ParentContainerStyle>
                                </dx:LayoutGroup>
                            </Items>
                        </dx:ASPxFormLayout>
                    </td>

                </tr>
                <tr>
                    <td style="width: 100%" colspan="3">
                        <asp:Button ID="btnKaydet" runat="server" CssClass="formglobalbuttonStyle" OnClick="btnKaydet_Click" AutoPostBack="False"></asp:Button>
                        <input type="button" id="btnDosyaBagla" runat="server" class="formglobalbuttonStyle" onclick="DosyaPopup()" />
                        <asp:Button ID="btnDosyaGor" runat="server" CssClass="formglobalbuttonStyle" OnClick="btnDosyaGor_Click" Visible="false"></asp:Button>
                        <input type="button" id="btnDosyaSil" runat="server" class="formglobalbuttonStyle" onclick="DosyaPopupSil()" />
                        <asp:Button ID="btnKayitSil" runat="server" CssClass="formglobalbuttonStyle" OnClick="btnKayitSil_Click" OnClientClick="javascript: return ConfirmMessage('Silme')"></asp:Button>
                        <asp:Button ID="btnOnayla" EnableTheming="false" runat="server" CssClass="formglobalbuttonStyle" OnClick="btnOnayla_Click" OnClientClick="javascript: return OdemevadesiKontrol()"></asp:Button>
                        <input type="button" id="btnReddet" runat="server" class="formglobalbuttonStyle" onclick="ShowCreateAccountWindow()" />
                        <asp:Button ID="btnFaturaOlustur" runat="server" CssClass="formglobalbuttonStyle" OnClick="btnFaturaOlustur_Click"></asp:Button>
                        <input type="button" id="btnOpPdf" visible="false" runat="server" value="Pdf Aç" class="formglobalbuttonStyle" onclick="window.parent.openpdfpopup();" />

                    </td>
                </tr>
            </table>


        </div>
        <STDT:StDataTable ID="DTVendCust" runat="server" />
        <asp:SqlDataSource ID="DSVendCust" runat="server" ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
        <STDT:StDataTable ID="DTCurrency" runat="server" />
        <STDT:StDataTable ID="DTPaymentTerm" runat="server" />
        <STDT:StDataTable ID="DTJobTypes" runat="server" />
        <STDT:StDataTable ID="DTOnayDepartmani" runat="server" />
        <STDT:StDataTable ID="DTJobNo" runat="server" />
        <STDT:StDataTable ID="DTNots" runat="server" />
        <div style="display: none">
            <asp:TextBox ID="lblconfirmOnayaGonderMessage" runat="server"></asp:TextBox>
            <asp:TextBox ID="lblSilmeMessage" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtFirmaOdemeVadesiCode" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtSayfaTurveNo" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtSayfaTur" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtselectedPaymentTerms" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtodemeVadesiMessage" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtPageRedirectMessage" runat="server"></asp:TextBox>
            <asp:Label ID="lblGId" runat="server"></asp:Label>

        </div>
        <dx:ASPxPopupControl ID="pcAciklama" runat="server" CloseAction="CloseButton" CloseOnEscape="true"
            PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="pcAciklama"
            HeaderText="Açıklama" AllowDragging="True" Modal="True" PopupAnimationType="Fade" Width="20%" Height="10%"
            EnableViewState="False" PopupHorizontalOffset="30" PopupVerticalOffset="30">
            <ClientSideEvents PopUp="function(s, e) { ASPxClientEdit.ClearGroup('createAccountGroup'); }" />
            <SizeGripImage Width="21px" />
            <ContentCollection>
                <dx:PopupControlContentControl>
                    <dx:ASPxPanel ID="NotPanel" runat="server" DefaultButton="btnReddet">
                        <PanelCollection>
                            <dx:PanelContent>
                                <dx:ASPxMemo ID="txtAciklama" runat="server" Width="250px" Height="100px"></dx:ASPxMemo>
                                <br />
                                <asp:Button ID="btnAciklamaKaydet" runat="server" CssClass="formglobalbuttonStyle" OnClick="btnAciklamaKaydet_Click"></asp:Button>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxPanel>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:ASPxPopupControl>



        <script type="text/javascript" lang="javascript">
            function ShowCreateAccountWindow() {
                pcAciklama.Show();
            }

            function ReloadParentPage(FaturaNo) {
                var shref = window.parent.location.href;
                if (shref.includes('&P1=')) {
                    var n = shref.indexOf("&P1", shref.lenght);
                    window.parent.location.href = shref.substring(0, n) + '&P1=' + FaturaNo;
                }
                else
                    window.parent.location.href = window.parent.location.href + '&P1=' + FaturaNo;
            }

            function pageClose(pageType) {
                window.opener.location.href = AdGIdToLink("/FaturaOnOnayListe.aspx?InvoiceType=" + pageType);
                window.close();

            }

            function AdGIdToLink(_val) {
                if (_val.includes("GId=")) return _val;
                var ObjGId = "<%= lblGId.ClientID %>";
                if (_val.includes("?"))
                    _val = _val + "&GId=" + $("#" + ObjGId).text();
                else
                    _val = _val + "?GId=" + $("#" + ObjGId).text();

                return _val;

            }

            function OdemevadesiKontrol() {
                var message = "", firmaOdemeVadesi = "", faturaOdemeVadesi = "", Tip = "";

                var splitter = $('#<%=txtodemeVadesiMessage.ClientID%>').val().split('-');

                message = splitter[0].toString();
                firmaOdemeVadesi = splitter[1].toString();
                faturaOdemeVadesi = splitter[2].toString();

                if (firmaOdemeVadesi != "" && faturaOdemeVadesi != "") {
                    if (firmaOdemeVadesi != faturaOdemeVadesi) {
                        Tip = "Show";
                    }
                    else {
                        Tip = "Not";
                    }
                }
                else {
                    Tip = "Not";
                }



                if (Tip == "Show") {
                    if (confirm(message)) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return true;
                }


            }



            function ConfirmMessage(tip) {
                var message = "";
                if (tip == "Silme") {
                    message = $('#<%=lblSilmeMessage.ClientID%>').val();
                }
                else if (tip == "Onayla") {
                    message = $('#<%=lblconfirmOnayaGonderMessage.ClientID%>').val();
                }

                if (confirm(message)) {
                    return true;
                }
                else {
                    return false;
                }
            }


            function DosyaPopup() {
                var Popup;
                var splitter = $('#<%=txtSayfaTurveNo.ClientID%>').val().split('-');
                var link = "/dosyabagla.aspx?BelgeNo=" + splitter[0].toString() + "&tur=28&dtur=28" + "&FaturaOnGiris=" + splitter[1].toString();
                Popup = window.open(AdGIdToLink(link), "Popup", "scrollbars=no,resizable=no,width=650,height=460,top=200,left=400");
                Popup.focus();
            }

            function DosyaPopupSil() {
                var Popup;
                var splitter = $('#<%=txtSayfaTurveNo.ClientID%>').val().split('-');
                var link = "dosyagor.aspx?BelgeNo=" + splitter[0].toString() + "&tur=28&dtur=28&Silme=1";
                Popup = window.open(AdGIdToLink(link), "Popup", "scrollbars=no,resizable=no,width=650,height=460,top=200,left=400");
                Popup.focus();
            }

            var lastvendCust = null;
            var postponedCallbackRequired = false;
            function cmbVendCustChanged(cmbVendCust) {
                if (callbackPanel.InCallback()) {
                    postponedCallbackRequired = true;
                }
                else {
                    callbackPanel.PerformCallback(cmbVendCust.GetValue().toString());
                }
            }

            function OnEndCallback(s, e) {
                if (postponedCallbackRequired) {
                    callbackPanel.PerformCallback();
                    postponedCallbackRequired = false;
                }
            }



            var lastdovizCinsi = null;
            var dovizcinsi = null;

            function SettxtToplamTutarTl() {
                if (cmbDovizCinsi.GetValue() == null) {
                    dovizcinsi = "TL";
                }
                else {
                    dovizcinsi = cmbDovizCinsi.GetValue().toString() == '' ? 'TL' : cmbDovizCinsi.GetValue().toString();
                }
                var _kur = 1, tutar = 0;
                if (dovizcinsi != 'TL') {
                    if (txtDovizKuru.GetNumber() == null)
                        _kur = 1;
                    else if (txtDovizKuru.GetNumber() == 0)
                        _kur = 1;
                    else
                        _kur = txtDovizKuru.GetNumber();
                    if (txtToplamTutar.GetNumber() == null)
                        tutar = 0;
                    else
                        tutar = txtToplamTutar.GetNumber();

                    var _valT = parseFloat(_kur) * parseFloat(tutar)
                    txtToplamTutartl.SetNumber(_valT);

                }
                else {
                    if (txtToplamTutar.GetNumber() == null)
                        tutar = 0;
                    else
                        tutar = txtToplamTutar.GetNumber();
                    var _valT = parseFloat(tutar)
                    txtToplamTutartl.SetNumber(_valT);
                }
            }
            function cmbDovizCinsiChanged(cmbDovizCinsi) {
                if (cmbDovizCinsi.GetValue() == null) {
                    dovizcinsi = "TL";
                }
                else {
                    dovizcinsi = cmbDovizCinsi.GetValue().toString();
                }

                if (callbackPanelDoviz.InCallback())
                    lastdovizCinsi = dovizcinsi
                else
                    callbackPanelDoviz.PerformCallback(dovizcinsi);
            }
            function OnEndCallbackDoviz(s, e) {
                if (lastdovizCinsi) {
                    callbackPanelDoviz.PerformCallback(cmbDovizCinsiChanged);
                    lastdovizCinsi = null;
                }
            }
            function ShowCreateAccountWindow() {
                pcAciklama.Show();

            }
        </script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>

    </form>
</body>
</html>


