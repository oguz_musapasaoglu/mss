﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MSS1.Default" ValidateRequest="false" %>

<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MSS Portal - Login Page</title>
    <link href="css/GAC_NewStyle.css" rel="stylesheet" />
    
    <style type="text/css">
        html {
            height: 100%;
            padding: 0px;
            border: 0px;
            margin: 0px;
        }

        body {
            height: 100%;
            padding: 0px;
            border: 0px;
            margin: 0px;
            background-image: url(images/background.png?v=3);
            background-size: cover;
            -o-background-size: cover;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            background-repeat: no-repeat;
            background-position: center center;
        }



        .loader {
            background: url(images/ajax-loader.gif) no-repeat scroll 0 0 transparent;
            width: 16px;
            height: 16px;
            display: block;
            float: right;
        }

        input:-webkit-autofill,
        input:-webkit-autofill:hover,
        input:-webkit-autofill:focus,
        input:-webkit-autofill:active {
            -webkit-box-shadow: 0 0 0px 1000px #36A6D4 inset !important;
            -webkit-text-fill-color: #FFF !important;
        }
    </style>


</head>
<body>
    <script type="text/javascript">
        function callbackError(textvalue) {
            if (textvalue != "") {
                lblMessage.SetValue(textvalue);
            }
        }

        function OnGotFocus(s, e) {
         if (s.cp_bound) return;
            cbPanel.PerformCallback();
        }

        function OnEndCallback(s, e) {
            if (s.cp_result == "ERROR" ) {
                alert(s.cp_Message);
            }
            else { 
                ddlsirketsec.ShowDropDown();
            }
        }
        

    </script>
    <form id="form1" runat="server">
        
                    <table style="width: 100%; padding-top: 30%">
                        <tr>
                            <td style="width: 75%">&nbsp;
                            </td>
                            <td style="width: 25%; vertical-align: bottom; text-align: right">
                                <table style="background-color: white; background: hsla(0, 0%, 95%, 0.41); border-radius: 8px; padding-right: 5%; padding-left: 7%; padding-top: 5%; padding-bottom: 5%; height: 200px; width: 320px" cellspacing="5">
                                     <tr>
                            <td>
                                <dx:ASPxTextBox ID="TextBox1" AutoCompleteType="None" runat="server" CssClass="LoginFormTextboxStyle"
                                    BackColor="#36a6d4" ForeColor="White" NullText="Login" Font-Size="11pt" Font-Bold="true"
                                    BackgroundImage-ImageUrl="~/images/Personel.png" BackgroundImage-Repeat="NoRepeat"
                                    BackgroundImage-HorizontalPosition="left" BackgroundImage-VerticalPosition="center">                                  
                                </dx:ASPxTextBox>

                            </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxTextBox ID="TextBox2" runat="server" ClientInstanceName="TextBox2" CssClass="LoginFormTextboxStyle" AutoCompleteType="None"
                                        BackColor="#36a6d4" ForeColor="White" NullText="Password" Password="true" Font-Size="11pt" Font-Bold="true"
                                        BackgroundImage-ImageUrl="~/images/kilit.png" BackgroundImage-Repeat="NoRepeat"
                                        BackgroundImage-HorizontalPosition="left" BackgroundImage-VerticalPosition="center">
                                     
                                    </dx:ASPxTextBox>

                                </td>
                            </tr>
                                    <tr>
                                        <td>
                                             <dx:ASPxCallbackPanel ID="cbPanel" runat="server" Width="200px" ClientInstanceName="cbPanel" OnCallback="cbPanel_Callback"> 
                                                    <PanelCollection>
                                                        <dx:PanelContent runat="server">
                                                                <dx:ASPxComboBox ID="ddlsirketsec" Theme="Glass" ClientInstanceName="ddlsirketsec" runat="server" 
                                                                CssClass="LoginFormTextboxStyle"   EnableCallbackMode="True"
                                                                BackColor="#36a6d4" ForeColor="White"   NullText="Şirket Seçiniz."
                                                                ValueField="Code" TextField="Text" 
                                                                BackgroundImage-ImageUrl="~/images/Bina.png" BackgroundImage-Repeat="NoRepeat"
                                                                BackgroundImage-HorizontalPosition="left" BackgroundImage-VerticalPosition="center">
                                                                
                                                                <ListBoxStyle BackColor="#36a6d4" ForeColor="White"></ListBoxStyle>
                                                                <ItemStyle BackColor="#36a6d4" Font-Bold="true" Paddings-PaddingLeft="30px" Cursor="pointer" HoverStyle-BackColor="White" HoverStyle-ForeColor="#36a6d4" ForeColor="White" />
                                                                <ButtonStyle CssClass="cbuttonColor" Width="38px" Border-BorderWidth="0px"></ButtonStyle>
                                                                <DropDownButton>
                                                                    <Image Url="~/images/cmbarrowdown.png" Height="12px" Width="14px"></Image>
                                                                </DropDownButton>
                                                                <ClientSideEvents GotFocus="OnGotFocus"/>
                                                            </dx:ASPxComboBox>
                                                        </dx:PanelContent>
                                                    </PanelCollection> 
                                                        <ClientSideEvents EndCallback="OnEndCallback" />
                                                 </dx:ASPxCallbackPanel>
                                           
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">
                                            <asp:Button ID="Button1" runat="server" Width="250px" CssClass="LoginFormbuttonStyle" ForeColor="White" Text=""
                                                OnClick="Button1_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left; text-wrap: none">
                                            <dx:ASPxLabel ID="lblMessage" ForeColor="Red" Font-Italic="true" ClientInstanceName="lblMessage" runat="server"></dx:ASPxLabel>
                                            <%--<asp:Label ID="lbletiket" runat="server" CssClass="LoginetiketlabelStyle" Text="LYONEL A. MAKZUME GROUP OF COMPANIES"></asp:Label>--%>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                

        <div style="display: none">
            <asp:Label ID="lblParola" runat="server"></asp:Label>
              <asp:Label ID="lblGuid" runat="server"></asp:Label>
        </div>
        <asp:SqlDataSource ID="DSComp" runat="server" ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource> 
        <dx:ASPxLoadingPanel ID="loadingPanel" runat="server" ClientInstanceName="loadingPanel" ContainerElementID="ddlsirketsec">
             <Template>
                <span class="loader" />
            </Template>
        </dx:ASPxLoadingPanel>
    </form>
</body>
</html>
