﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SalesReportDocuments.aspx.cs" Inherits="MSS1.SalesReportDocuments" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>KDVLİ FATURA DÖKÜMANLARI</h1>
            <table  cellspacing="10">
                <tr>
                    <td>
                        <asp:Label ID="lblKdvDurum" runat="server" Text="KDV Durumu"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlKdvDurum" runat="server">
                            <asp:ListItem Value="9" Text="Tümü"></asp:ListItem>
                            <asp:ListItem Value="1" Text="KDV li Dökümanlar"></asp:ListItem>
                            <asp:ListItem Value="0" Text="KDV siz Dökümanlar"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblDept" runat="server" Text="Departman"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlDepartman" runat="server">
                            <asp:ListItem Value="" Text="Tümü"></asp:ListItem>
                            <asp:ListItem Value="BOYUTSUZ" Text="BOYUTSUZ"></asp:ListItem>
                            <asp:ListItem Value="GENEL MÜDÜRLÜK" Text="GENEL MÜDÜRLÜK"></asp:ListItem>
                            <asp:ListItem Value="ACENTE" Text="ACENTE"></asp:ListItem>
                            <asp:ListItem Value="LOJİSTİK" Text="LOJİSTİK"></asp:ListItem>
                            <asp:ListItem Value="HAT VE PROJE" Text="HAT VE PROJE"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblDonem" runat="server" Text="Dönem"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlYil" runat="server">
                            <asp:ListItem Value="2016" Text="2016"></asp:ListItem>
                            <asp:ListItem Value="2017" Text="2017"></asp:ListItem>
                            <asp:ListItem Value="2018" Text="2018"></asp:ListItem>
                            <asp:ListItem Value="2019" Text="2019"></asp:ListItem>
                            <asp:ListItem Value="2020" Text="2020"></asp:ListItem>
                        </asp:DropDownList>

                        <asp:DropDownList ID="ddlAy" runat="server">
                            <asp:ListItem Value="1" Text="Ocak"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Şubat"></asp:ListItem>
                            <asp:ListItem Value="3" Text="Mart"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Nisan"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Mayıs"></asp:ListItem>
                            <asp:ListItem Value="6" Text="Haziran"></asp:ListItem>
                            <asp:ListItem Value="7" Text="Temmuz"></asp:ListItem>
                            <asp:ListItem Value="8" Text="Ağustos"></asp:ListItem>
                            <asp:ListItem Value="9" Text="Eylül"></asp:ListItem>
                            <asp:ListItem Value="10" Text="Ekim"></asp:ListItem>
                            <asp:ListItem Value="11" Text="Kasım"></asp:ListItem>
                            <asp:ListItem Value="12" Text="Aralık"></asp:ListItem>
                        </asp:DropDownList>
                    </td>


                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="btndocTopla" runat="server" Text="Dökümanları Kaydet" OnClick="btndocTopla_Click" /></td>
                </tr>
                <tr>
                    <td colspan="10">
                        <asp:Repeater ID="rptSource" runat="server">
                            <HeaderTemplate>
                                <table style="min-width: 1340px">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>COMPANY</th>
                                            <th>DOCUMENT NO</th>
                                            <th>EXTERNAL DOCUMENT NO</th>
                                            <th>MONTH</th>
                                            <th>YEAR</th>
                                            <th>DEPARTMENT</th>
                                            <th>KDV STATUS</th> 
                                            <th>FTPATH EXCEL</th> 
                                            <th>BLPATH EXCEL</th>
                                        </tr>
                                    </thead>
                            </HeaderTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                            <ItemTemplate>
                                <tr>

                                    <td>
                                        <asp:Literal ID="ltr1" runat="server" Text='<%# Eval("ID") %>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("COMPANY") %>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("Document No_") %>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("External Document No_") %>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("AY") %>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal5" runat="server" Text='<%# Eval("YIL") %>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("ANADEPT") %>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal7" runat="server" Text='<%# Eval("KDVDURUM") %>'></asp:Literal>
                                    </td> 
                                    <td>
                                        <asp:Literal ID="Literal9" runat="server" Text='<%# Eval("FATURAPATH") %>'></asp:Literal>
                                    </td> 
                                    <td>
                                        <asp:Literal ID="Literal11" runat="server" Text='<%# Eval("BLPATH") %>'></asp:Literal>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>




                    </td>

                </tr>
            </table>
        </div>
    </form>
</body>
</html>
