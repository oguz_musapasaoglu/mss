﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BookingCard.aspx.cs" Inherits="MSS1.Booking.BookingCard" %>

<%@ Register Src="~/controls/DataTableControlGAC.ascx" TagName="StDataTable" TagPrefix="STDT" %>

<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../css/GAC_MenuStyle.css" rel="stylesheet" />
    <link href="../css/GAC_NewStyle.css?v=V1" rel="stylesheet" />
    <link href="../css/GAC_CardStyle.css?v=V1" rel="stylesheet" />
    <style>
        .dxflGroup {
            padding: 0px 5px;
            width: 100%;
        }

            .dxflGroup tr:first-child > .dxflGroupCell > .dxflGroupBox, .dxflGroup > .dxflChildInFirstRowSys > .dxflGroupCell > .dxflGroupBox {
                margin-top: -5px;
            }

        .dx-vam {
            color: white;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%">
            <table style="width: 100%; border-width: 0px;">
                <tr>
                    <td style="width: 50%; vertical-align: top">
                        <dx:ASPxFormLayout ID="Box1" runat="server" Width="100%">
                            <Items>
                                <dx:LayoutGroup ShowCaption="False">
                                    <Items>
                                        <dx:LayoutItem FieldName="PaymentType" Caption="PAYMENT TYPE" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtPaymentType" ReadOnly="true" runat="server" Width="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="Customer" Caption="CUSTOMER NAME" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtCustomerName" ReadOnly="true" runat="server" Width="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="Country" Border-BorderColor="DarkBlue" Caption="COUNTRY" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtCountry" ReadOnly="true" runat="server" Width="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="CurrencyType" Border-BorderColor="DarkBlue" Caption="DOVİZ CİNSİ" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtCurrency" ReadOnly="true" runat="server" Width="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="PriceBooking" Border-BorderColor="DarkBlue" Caption="PRICE BOOKING" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtBookingPrice" ReadOnly="true" runat="server" Width="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="DovizKuru" Border-BorderColor="DarkBlue" Caption="DOVİZ KURU" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtDovizKuru" runat="server" Width="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                         <dx:LayoutItem FieldName="Ad" Border-BorderColor="DarkBlue" Caption="AD" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtAd" runat="server" Width="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="Mail" Border-BorderColor="DarkBlue" Caption="MAIL" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtMail" runat="server" Width="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                    </Items>
                                    <GroupBoxStyle CssClass="cardBox"></GroupBoxStyle>
                                    <ParentContainerStyle Paddings-Padding="0"></ParentContainerStyle>
                                </dx:LayoutGroup>
                            </Items>
                        </dx:ASPxFormLayout>
                    </td>
                    <td style="width: 50%; vertical-align: top">
                        <dx:ASPxFormLayout ID="Box2" runat="server" Width="100%">
                            <Items>
                                <dx:LayoutGroup ShowCaption="False">
                                    <Items>
                                        <dx:LayoutItem FieldName="Tarih" Border-BorderColor="DarkBlue" Caption="INVOICE DATE" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxDateEdit runat="server" ID="txtDate" AutoPostBack="true" DisplayFormatString="dd.MM.yyyy" EditFormatString="dd.MM.yyyy" OnValueChanged="txtDate_ValueChanged"></dx:ASPxDateEdit>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="Serino" Border-BorderColor="DarkBlue" Caption="SERİ NO" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtSerino" runat="server" Width="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="HariciBelgeNo" Border-BorderColor="DarkBlue" Caption="HARİCİ BELGE NO" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtHariciBelgeNo" runat="server" Width="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="identityNo" Border-BorderColor="DarkBlue" Caption="IDENTITY NO" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtIdentityNo" runat="server" Width="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="PassportNo" Border-BorderColor="DarkBlue" Caption="PASSPORT NO" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtPassportNo" runat="server" Width="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="Faturaekmetin" Border-BorderColor="DarkBlue" Caption="FATURA EK METIN" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtFaturaEkMetin" runat="server" Width="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>  
                                        <dx:LayoutItem FieldName="Soyad" Border-BorderColor="DarkBlue" Caption="SOYAD" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtSoyad" runat="server" Width="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="City" Border-BorderColor="DarkBlue" Caption="CITY" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtCity" runat="server" Width="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                    </Items>
                                    <GroupBoxStyle CssClass="cardBox"></GroupBoxStyle>
                                    <ParentContainerStyle Paddings-Padding="0"></ParentContainerStyle>
                                </dx:LayoutGroup>
                            </Items>
                        </dx:ASPxFormLayout>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%" colspan="3">
                        <dx:ASPxButton ID="btnCreateInv" Text="CREATE INVOICE" runat="server" CssClass="formglobalbuttonStyleCRM" OnClick="btnCreateInv_Click"></dx:ASPxButton>
                        <dx:ASPxButton ID="btnPrintInv" Text="CREATE INVOICE" runat="server" CssClass="formglobalbuttonStyleCRM" OnClick="btnPrintInv_Click"></dx:ASPxButton>
                    </td>
                </tr>
            </table>


        </div>
        <div style="display: none">
            <asp:Label runat="server" ID="lblCustomerNo"></asp:Label>
            <asp:Label runat="server" ID="lblinvoiceNo"></asp:Label>
            <asp:CheckBox runat="server" ID="IsCust" Checked="false"></asp:CheckBox>
        </div>
    </form>
</body>
</html>

