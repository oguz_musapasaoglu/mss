﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.WSGeneric;
using System.ServiceModel;
using System.Data.SqlClient;
using System.Configuration;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using MSS1.Codes;


namespace MSS1.Booking
{
    public partial class BookingCard : Bases
    {
        fonksiyonlar f = new fonksiyonlar();
        Language l = new Language();
        Methods method = new Methods();
        datasourceHandler dsh = new datasourceHandler();
        webObjectHandler woh = new webObjectHandler();
        string ConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        SessionBase _obj;
        private string currentCardUniqueID
        {
            get
            {
                string _cardId = string.Empty;
                try
                {
                    _cardId = Request.QueryString["BookingNo"].ToString();
                }
                catch (Exception)
                {
                    _cardId = string.Empty;
                }
                return _cardId;
            }
        }


        ws_baglantı ws = new ws_baglantı();
        void dilgetir()
        {
            Box1.FindItemByFieldName("PaymentType").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0011");
            Box1.FindItemByFieldName("Country").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0012");
            Box1.FindItemByFieldName("CurrencyType").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0013");
            Box2.FindItemByFieldName("Serino").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0014");
            Box2.FindItemByFieldName("HariciBelgeNo").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0015");

            Box2.FindItemByFieldName("Tarih").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0016");
            Box1.FindItemByFieldName("Customer").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0017");
            Box1.FindItemByFieldName("PriceBooking").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0018");
            Box2.FindItemByFieldName("Faturaekmetin").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0019");
            Box1.FindItemByFieldName("DovizKuru").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0020");
            Box2.FindItemByFieldName("identityNo").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0021");
            Box2.FindItemByFieldName("PassportNo").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0022");

            Box1.FindItemByFieldName("Ad").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0054");
            Box2.FindItemByFieldName("Soyad").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0055");
            Box1.FindItemByFieldName("Mail").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0053");
            Box2.FindItemByFieldName("City").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0056");

            btnCreateInv.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "F1001");
            btnPrintInv.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "F1002");


        }
        private void preparepageFields()
        {
            lblinvoiceNo.Text = string.Empty;
        }


        private void createPK(DataTable dt)
        {
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = dt.Columns["ID"];
            dt.PrimaryKey = pricols;
        }


        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsCallback | IsPostBack) { return; }



            preparepageFields();

            preparepageForms();

            dilgetir();


        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }

        private void preparepageForms()
        {
            if (currentCardUniqueID != string.Empty)
            {
                using (SqlConnection conn = new SqlConnection(ConnString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {

                        conn.Open();
                        cmd.Parameters.Clear();
                        cmd.CommandText = "select * from [0_W161100] where RCOMP=@COMP and R0011=@R0011";
                        cmd.Parameters.AddWithValue("@COMP", _obj.Comp);
                        cmd.Parameters.AddWithValue("@R0011", currentCardUniqueID);
                        cmd.Connection = conn;
                        using (SqlDataAdapter adapter = new SqlDataAdapter())
                        {
                            adapter.SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                adapter.Fill(dt);
                                if (dt.Rows.Count > 0)
                                {
                                    txtAd.Text = dt.Rows[0]["R0054"].ToString();
                                    txtSoyad.Text = dt.Rows[0]["R0055"].ToString();
                                    txtMail.Text = dt.Rows[0]["R0053"].ToString();
                                    txtCity.Text = dt.Rows[0]["R0056"].ToString();
                                    txtCountry.Text = dt.Rows[0]["R0040"].ToString();
                                    txtBookingPrice.Text = string.Format("{0:n2}", dt.Rows[0]["R0038"]);
                                    txtCustomerName.Text = dt.Rows[0]["R0029"].ToString();
                                    txtPaymentType.Text = dt.Rows[0]["R0039"].ToString();
                                    txtCurrency.Text = dt.Rows[0]["R0016"].ToString();
                                    lblCustomerNo.Text = dt.Rows[0]["R0028"].ToString();
                                    if (lblCustomerNo.Text == "CS000001" | lblCustomerNo.Text == "CS000002")
                                        IsCust.Checked = true;
                                    else
                                        IsCust.Checked = false;

                                    if (!IsCust.Checked)
                                    {
                                        txtIdentityNo.Enabled = false;
                                        txtPassportNo.Enabled = false;
                                    }

                                    txtDate.Date = DateTime.Now.Date;
                                    txtDate_ValueChanged(txtDate, null);

                                    if (dt.Rows[0]["RYTK9"].ToString() == "0")
                                    {
                                        btnCreateInv.Visible = false;
                                        btnPrintInv.Visible = false;
                                    }



                                }
                            }
                        }

                        conn.Close();



                    }
                }
            }
        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }



        private void SaveEditCreate()
        {
            try
            {
                // zorunluluk kaldırıldı MKZ01660 -- HAKAN DOĞAN ST 
                //if(IsCust.Checked)
                //{
                //    if (string.IsNullOrEmpty(txtIdentityNo.Text) | string.IsNullOrEmpty(txtPassportNo.Text))
                //    {
                //        MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "P161110_O161150", "R0004"));
                //        return;
                //    }
                //}



                string invoiceNo = GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SealineBookingCreateCashInvoice§" + currentCardUniqueID + "§" + txtSerino.Text + "§" + txtHariciBelgeNo.Text + "§" +
                     txtFaturaEkMetin.Text + "§" + "0" + "§" + txtDovizKuru.Text + "§" + method.DateToStringNAVType(txtDate.Text)
                     + "§" + lblCustomerNo.Text + "§" + "" + "§" + "0" + "§" + _obj.UserName + "§" + txtCurrency.Text + "§" + string.Empty + "§" + txtPassportNo.Text + "§" + txtIdentityNo.Text + "§" + txtAd.Text + "§" + txtSoyad.Text + "§" + txtMail.Text + "§" + txtCity.Text
                     , _obj.Comp);




                if (invoiceNo.Contains("|"))
                {
                    if (invoiceNo.Substring(0, 2).ToLower() == "ok")
                    {
                        lblinvoiceNo.Text = invoiceNo.Split('|')[1];

                        ClientScript.RegisterStartupScript(GetType(), "JSMusteriSave", @"<script type=""text/javascript""> "
                      + "window.open('../Raporlar/DynamicReport.aspx?ReportCode=RP055&DocNo=" + lblinvoiceNo.Text + "&DETAYTOPLU=1&TOPLUFATURAMETNI=&EKMETIN=" + txtFaturaEkMetin.Text + "&NORMALPROFORMA=1&ONIZLEME=2&GId=" + _obj.GuidKey + "');</script>");

                    }
                }
                else
                    MessageBox(invoiceNo.Replace("'", ""));


                // HAKAN DOĞAN ST - KAPATILDI
                //if (!ClientScript.IsStartupScriptRegistered("JSSave"))
                //{
                //    ClientScript.RegisterStartupScript(GetType(), "JSSave", @"<script type=""text/javascript""> "
                //    + "window.parent.grid1.PerformCallback('ReloadWith');window.location.href = window.location.href;</script>");
                //}

                //if (!ClientScript.IsStartupScriptRegistered("JSMusteriSave"))
                //{
                //    ClientScript.RegisterStartupScript(GetType(), "JSMusteriSave", @"<script type=""text/javascript""> "
                //    + "window.parent.location='./../OnePageList30.aspx?OJ1=P161110;O161110&OJ2=P161110;O161200&OJ3=P161110;O161300&OJ4=P161110;O161400&OJ5=P161110;O161500&OJ6=P161110;" +
                //    "O161600&OJ7=P161110;O161700&OJ8=P161110;OJ9999&OJ9=P161110;OJ9999&OJ10=P161110;OJ9999&OJ11=P161110;OJ9999&OJ12=P161110;OJ9999&OJ13=P161110;OJ9999&OJ14=P161110;" +
                //    "OJ9999&OJ15=P161110;OJ9999&P1=" + currentCardUniqueID + "';</script>");
                //}
            }
            catch (Exception ex)
            {
                MessageBox(ex.Message.ToString().Replace("'", " "));
            }
        }

        private void SaveEditPrint()
        {
            try
            {
                CrystalReportOpen();
                string drm = GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SeaLineInvoicePost§" + lblinvoiceNo.Text, _obj.Comp);


                if (drm == "True")
                {

                    if (!ClientScript.IsStartupScriptRegistered("JSSave"))
                    {
                        ClientScript.RegisterStartupScript(GetType(), "JSSave", @"<script type=""text/javascript""> "
                        + "window.parent.grid1.PerformCallback('ReloadWith');window.location.href = window.location.href;" +
                        "var popup=window.open('../" + CurrentFolderName(_obj.Comp) + "/PDF/" + lblinvoiceNo.Text + ".pdf');popup.focus();" +
                        "</script>");
                    }
                }
                else
                {
                    MessageBox(drm);
                }
                Response.Redirect(Request.RawUrl);
                //HAKAN DOĞAN ST - KAPATILDI 
                //if (!ClientScript.IsStartupScriptRegistered("JSMusteriSave"))
                //{
                //    ClientScript.RegisterStartupScript(GetType(), "JSMusteriSave", @"<script type=""text/javascript""> "
                //    + "window.parent.location='./../OnePageList30.aspx?OJ1=P161110;O161110&OJ2=P161110;O161200&OJ3=P161110;O161300&OJ4=P161110;O161400&OJ5=P161110;O161500&OJ6=P161110;" +
                //    "O161600&OJ7=P161110;O161700&OJ8=P161110;OJ9999&OJ9=P161110;OJ9999&OJ10=P161110;OJ9999&OJ11=P161110;OJ9999&OJ12=P161110;OJ9999&OJ13=P161110;OJ9999&OJ14=P161110;" +
                //    "OJ9999&OJ15=P161110;OJ9999&P1=" + currentCardUniqueID + "';</script>");
                //}
            }
            catch (Exception ex)
            {
                MessageBox(ex.Message.ToString().Replace("'", " "));
            }
        }

        private void CrystalReportOpen()
        {
            ReportDocument objrptdoc = new ReportDocument();
            objrptdoc.Load(f.GetReportPath("INVOICE STENA.rpt", _obj.Comp));
            objrptdoc.ReportOptions.EnableSaveDataWithReport = false;
            objrptdoc.SetParameterValue("@DOCNO", lblinvoiceNo.Text);
            objrptdoc.SetParameterValue("@COMP", _obj.Comp);
            objrptdoc.SetParameterValue("DETAYTOPLU", 1);
            objrptdoc.SetParameterValue("TOPLUFATURAMETNI", "");
            objrptdoc.SetParameterValue("NORMALPROFORMA", 1);
            objrptdoc.SetParameterValue("EKMETIN", txtFaturaEkMetin.Text);
            objrptdoc.SetParameterValue("ONIZLEME", 1);
            objrptdoc.ExportToDisk(ExportFormatType.PortableDocFormat, Server.MapPath("~/" + CurrentFolderName(_obj.Comp) + "/PDF/" + lblinvoiceNo.Text + ".pdf"));


            if (objrptdoc != null)
            {
                objrptdoc.Close();
                objrptdoc.Dispose();
                objrptdoc = null;
            }
        }





        protected void btnPrintInv_Click(object sender, EventArgs e)
        {
            SaveEditPrint();
        }

        protected void btnCreateInv_Click(object sender, EventArgs e)
        {
            SaveEditCreate();
        }

        protected void txtDate_ValueChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCurrency.Text) & !string.IsNullOrEmpty(txtDate.Text))
            {
                using (SqlConnection conn = new SqlConnection(ConnString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        try
                        {
                            conn.Open();
                            cmd.Parameters.Clear();
                            cmd.CommandText = "select " + txtCurrency.Text + " from [00_EXCHANGE RATES_01] where COMPANY=@COMP and datediff(day,[Starting Date],@date)=0";
                            cmd.Parameters.AddWithValue("@COMP", _obj.Comp);
                            cmd.Parameters.AddWithValue("@date", txtDate.Date);
                            cmd.Connection = conn;
                            using (SqlDataAdapter adapter = new SqlDataAdapter())
                            {
                                adapter.SelectCommand = cmd;
                                using (DataTable dt = new DataTable())
                                {
                                    adapter.Fill(dt);
                                    if (dt.Rows.Count > 0)
                                    {
                                        txtDovizKuru.Text = string.Format("{0:n4}", dt.Rows[0][txtCurrency.Text]);
                                    }
                                    else
                                    {
                                        txtDovizKuru.Text = string.Format("{0:n4}", 0);
                                    }
                                }
                            }

                            conn.Close();
                        }
                        catch
                        {
                            txtDovizKuru.Text = string.Format("{0:n4}", 0);

                        }





                    }
                }
            }
        }
    }
}