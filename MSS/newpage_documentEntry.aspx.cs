﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.WSGeneric;
using System.ServiceModel;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DevExpress.Web;
using System.Web.UI.HtmlControls;
using System.IO;
using MSS1.Codes;

namespace MSS1
{
    public partial class newpage_documentEntry : Bases
    {
        #region Connection String & Service
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();

        public ws_baglantı ws = new ws_baglantı();
        SqlConnection baglanti;
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        string kontrol = string.Empty;
        DataTable DtFields = new DataTable();
        static SessionBase _obj;
        #endregion

        #region Servis Arama
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod(EnableSession = true)]
        public static List<string> SearchTaxarea(string prefixText, int count)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
            if (prefixText == "*")
            {
                string query = "Select Description as name from  [GAC_NAV2].[dbo].[0C_00318_00_TAX AREA] where COMPANY='" + _obj.Comp + "' order by Sıralama asc";
                SqlCommand cmd = new SqlCommand(query, conn);
                conn.Open();
                List<string> bul = new List<string>();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        bul.Add(sdr["name"].ToString());
                    }
                }
                conn.Close();

                return bul;
            }
            else
            {
                string query = "Select Description as name from [GAC_NAV2].[dbo].[0C_00318_00_TAX AREA] " +
                    "where COMPANY='" + _obj.Comp + "' AND Description like '" + prefixText + "%'  order by Sıralama asc";
                SqlCommand cmd = new SqlCommand(query, conn);
                conn.Open();
                List<string> bul = new List<string>();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        bul.Add(sdr["name"].ToString());
                    }
                }
                conn.Close();

                return bul;
            }

        }
        #endregion

        #region Dil Getir
        public void dilgetir()
        {
            Language l = new Language();
            lblsayfaadi.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "lblsayfaadi");
            lblCompany.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "lblCompany");
            lblDocumentGroup.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "lblDocumentGroup");
            lblDocumentType.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "lblDocumentType");
            btnDosyaBagla.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "btnDosyaBagla");

        }
        #endregion
        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {

            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack || IsCallback) return;
            dilgetir();
            InitDTComponents(STDT1.Table);

            cmbCompany.Text = _obj.Comp.ToUpper();
            cmbCompany.Enabled = false;

            if (Request.QueryString["DocType"] != null)
            {
                if (Request.QueryString["CreatedDoc"] != null)
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg07"));
                cmbDocumentGroup.Value = Request.QueryString["DocType"].ToString();
                cmbDocumentGroup.Enabled = false;
                FillDocumentType(Convert.ToInt32(Request.QueryString["DocType"].ToString()));
                lblno.Text = Request.QueryString["DocNo"].ToString();
            }
            else
            {
                if (Request.QueryString["CreatedDoc"] != null)
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg07"));
                cmbDocumentGroup.Value = "7";
                cmbDocumentGroup.Enabled = false;
                FillDocumentType(7);
                lblno.Text = "DH000" + GetDahiliNextNo();
            }


        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }

        int GetDahiliNextNo()
        {
            string sorgu = "select count(*) from [dbo].[0C_70002_00_DOCUMENTS DETAILS GENEL] " +
                "where (COMPANY='" + _obj.Comp + "' OR COMPANY2='" + _obj.Comp + "') AND [Dokuman Grubu]=7";
            int DahiliNextNo = Convert.ToInt32(f.tekbirsonucdonder(sorgu)) + 1;
            return DahiliNextNo;
        }

        int GetNextEntryNo()
        {
            try
            {
                string sorgu = "select MAX([Entry No])  from [dbo].[0C_70002_00_DOCUMENTS DETAILS GENEL] where COMPANY='" + _obj.Comp + "' ";
                int NextEntryNo = Convert.ToInt32(f.tekbirsonucdonder(sorgu)) + 1;
                return NextEntryNo;
            }
            catch
            {

                return 0;
            }

        }

        private void InitDTComponents(DataTable dt)
        {
            dt.Columns.Add("FieldId", typeof(string));
            dt.Columns.Add("FieldType", typeof(string));
            dt.Columns.Add("FieldNavNo", typeof(string));
            dt.Columns.Add("IsMandatory", typeof(int));
            dt.Columns.Add("FieldName", typeof(string));
            dt.Columns.Add("IsLookups", typeof(int));

        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        protected void cmbDocumenType_SelectedIndexChanged(object sender, EventArgs e)
        {

            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    clearValues();
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "select t1.*,t2.TR,t2.EN,t2.FR from [GAC_NAV2].[dbo].[0C_70001_00_DOCUMENTS FIELDS GENEL] t1"
                    + " left outer join [GAC_NAV2].[dbo].[0C_50032_01_WEB PAGE CAPTIONS] t2 on  " +
                    "t1.COMPANY=t2.COMPANY AND t1.[Field ID]=t2.FieldID and t2.PageID='newpage_documentEntry'"
                    + " where t1.COMPANY='" + _obj.Comp + "' AND [Belge No]=@BelgeNo Order By t1.[Web alan sira no]";
                    cmd.Parameters.Clear();
                    if (!string.IsNullOrEmpty(this.cmbDocumenType.Text))
                        cmd.Parameters.AddWithValue("@BelgeNo", this.cmbDocumenType.Value.ToString());
                    else
                        cmd.Parameters.AddWithValue("@BelgeNo", string.Empty);

                    for (int i = 1; i <= 25; i++)
                    {
                        SetComponentVisibility("tr" + i.ToString(), false);
                    }
                    STDT1.Table.Rows.Clear();
                    using (SqlDataAdapter adptr = new SqlDataAdapter(cmd))
                    {
                        using (DataSet ds = new DataSet())
                        {
                            adptr.Fill(ds);

                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                int CIndex = 1, Lang = 0;
                                string _Lan = f.tekbirsonucdonder("select Case When Language=0 Then 'TR' When Language=1 Then " +
                                    "'EN' When Language=2 Then 'FR' Else 'TR' END from " +
                                    "GAC_NAV2.dbo.[0C_50005_00_YETKILENDIRME] " +
                                    "where [Kullanıcı Adı]='" + _obj.UserName + "' and COMPANY ='" + _obj.Comp + "'");
                                if (_Lan == "TR")
                                    Lang = 0;
                                else if (_Lan == "EN")
                                    Lang = 1;
                                else if (_Lan == "FR")
                                    Lang = 2;
                                else
                                    Lang = 0;

                                string LookupsSorgu = string.Empty;
                                foreach (DataRow row in ds.Tables[0].Rows)
                                {
                                    SetComponentVisibility("tr" + CIndex.ToString(), true);
                                    Label lbl = GetLabelFromDynamicPanel("Label" + CIndex.ToString());
                                    if (lbl != null) lbl.Text = Lang == 0 ? row["TR"].ToString() : Lang == 1 ? row["EN"].ToString() : Lang == 2 ? row["FR"].ToString() : row["TR"].ToString();

                                    if (row["Zorunlu"].ToString() == "1")
                                        lbl.Text = lbl.Text + "*";

                                    switch (row["Alan Turu"].ToString().Substring(0, 3))
                                    {
                                        case "TAR":
                                            DataRow dr = this.STDT1.Table.NewRow();
                                            dr["FieldId"] = "Tar" + CIndex.ToString();
                                            dr["FieldType"] = "TAR";
                                            dr["FieldNavNo"] = row["Alan No NAV"].ToString();
                                            dr["IsMandatory"] = row["Zorunlu"].ToString() == "1" ? 1 : 0;
                                            dr["FieldName"] = lbl.Text;
                                            dr["IsLookups"] = 0;
                                            this.STDT1.Table.Rows.Add(dr);
                                            ((ASPxDateEdit)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tar" + CIndex.ToString())).Visible = true;
                                            ((ASPxTextBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tex" + CIndex.ToString())).Visible = false;
                                            ((ASPxCheckBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Chk" + CIndex.ToString())).Visible = false;
                                            ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).Visible = false;
                                            break;
                                        case "TEX":
                                            if (!string.IsNullOrEmpty(row["Kaynak Tablo"].ToString()))
                                            {
                                                DataRow dr1 = this.STDT1.Table.NewRow();
                                                dr1["FieldId"] = "Cmb" + CIndex.ToString();
                                                dr1["FieldType"] = "TEX";
                                                dr1["FieldNavNo"] = row["Alan No NAV"].ToString();
                                                dr1["IsMandatory"] = row["Zorunlu"].ToString() == "1" ? 1 : 0;
                                                dr1["FieldName"] = lbl.Text;
                                                if (row["Kaynak Tablo"].ToString() == "Lookups")
                                                {

                                                    dr1["IsLookups"] = 1;
                                                    this.STDT1.Table.Rows.Add(dr1);
                                                    LookupsSorgu = "Select [LineNo] Code, Adi from [0C_50055_00_LOOKUPS] " +
                                                        "where COMPANY='" + _obj.Comp + "' AND Tur='" + row["Lookups Tur"].ToString() + "' Order By [LineNo]";
                                                    ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).SelectCommand = LookupsSorgu;
                                                    ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).DataBind();

                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).ValueField = "Adi";
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).TextField = "Adi";
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).DataSource = ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString()));
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).DataBind();

                                                }
                                                else
                                                {
                                                    dr1["IsLookups"] = 2;
                                                    this.STDT1.Table.Rows.Add(dr1);
                                                    if (row["Kaynak Tablo"].ToString().Substring(0, 2) == "[0")
                                                        LookupsSorgu = "Select [" + row["Kaynak Tablo Alan Adi"].ToString().Split(',')[0] + "] Code, [" + row["Kaynak Tablo Alan Adi"].ToString().Split(',')[1] + "] Adi from " + row["Kaynak Tablo"].ToString() + " where COMPANY='" + _obj.Comp + "'  Order By [" + row["Kaynak Tablo Alan Adi"].ToString().Split(',')[1] + "]";
                                                    else
                                                        LookupsSorgu = "Select [" + row["Kaynak Tablo Alan Adi"].ToString().Split(',')[0] + "] Code, [" + row["Kaynak Tablo Alan Adi"].ToString().Split(',')[1] + "] Adi from " + row["Kaynak Tablo"].ToString() + "  Order By [" + row["Kaynak Tablo Alan Adi"].ToString().Split(',')[1] + "]";

                                                    ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).SelectCommand = LookupsSorgu;
                                                    ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).DataBind();

                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).ValueField = "Code";
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).TextField = "Adi";
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).DataSource = ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString()));
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).EnableCallbackMode = true;
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).CallbackPageSize = 20;
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).DataBind();
                                                }

                                                ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).Visible = true;
                                                ((ASPxDateEdit)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tar" + CIndex.ToString())).Visible = false;
                                                ((ASPxCheckBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Chk" + CIndex.ToString())).Visible = false;
                                                ((ASPxTextBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tex" + CIndex.ToString())).Visible = false;
                                            }
                                            else
                                            {
                                                DataRow dr1 = this.STDT1.Table.NewRow();
                                                dr1["FieldId"] = "Tex" + CIndex.ToString();
                                                dr1["FieldType"] = "TEX";
                                                dr1["FieldNavNo"] = row["Alan No NAV"].ToString();
                                                dr1["IsMandatory"] = row["Zorunlu"].ToString() == "1" ? 1 : 0;
                                                dr1["FieldName"] = lbl.Text;
                                                dr1["IsLookups"] = 0;
                                                this.STDT1.Table.Rows.Add(dr1);
                                                ((ASPxTextBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tex" + CIndex.ToString())).Visible = true;
                                                ((ASPxDateEdit)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tar" + CIndex.ToString())).Visible = false;
                                                ((ASPxCheckBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Chk" + CIndex.ToString())).Visible = false;
                                                ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).Visible = false;
                                            }
                                            break;
                                        case "COD":
                                            if (!string.IsNullOrEmpty(row["Kaynak Tablo"].ToString()))
                                            {
                                                DataRow dr2 = this.STDT1.Table.NewRow();
                                                dr2["FieldId"] = "Tex" + CIndex.ToString();
                                                dr2["FieldType"] = "TEX";
                                                dr2["FieldNavNo"] = row["Alan No NAV"].ToString();
                                                dr2["IsMandatory"] = row["Zorunlu"].ToString() == "1" ? 1 : 0;
                                                dr2["FieldName"] = lbl.Text;


                                                if (row["Kaynak Tablo"].ToString() == "Lookups")
                                                {
                                                    dr2["IsLookups"] = 1;
                                                    this.STDT1.Table.Rows.Add(dr2);
                                                    LookupsSorgu = "Select [LineNo] Code, Adi from [0C_50055_00_LOOKUPS] where" +
                                                        " COMPANY='" + _obj.Comp + "' AND Tur='" + row["Lookups Tur"].ToString() + "' Order By [LineNo]";
                                                    ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).SelectCommand = LookupsSorgu;
                                                    ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).DataBind();

                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).ValueField = "Adi";
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).TextField = "Adi";
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).DataSource = ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString()));
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).DataBind();

                                                }
                                                else
                                                {
                                                    dr2["IsLookups"] = 2;
                                                    this.STDT1.Table.Rows.Add(dr2);
                                                    if (row["Kaynak Tablo"].ToString().Substring(0, 2) == "[0")
                                                        LookupsSorgu = "Select [" + row["Kaynak Tablo Alan Adi"].ToString().Split(',')[0] + "] Code, [" + row["Kaynak Tablo Alan Adi"].ToString().Split(',')[1] + "] Adi from " + row["Kaynak Tablo"].ToString() + " where COMPANY='" + _obj.Comp + "'  Order By [" + row["Kaynak Tablo Alan Adi"].ToString().Split(',')[1] + "]";
                                                    else
                                                        LookupsSorgu = "Select [" + row["Kaynak Tablo Alan Adi"].ToString().Split(',')[0] + "] Code, [" + row["Kaynak Tablo Alan Adi"].ToString().Split(',')[1] + "] Adi from " + row["Kaynak Tablo"].ToString() + "  Order By [" + row["Kaynak Tablo Alan Adi"].ToString().Split(',')[1] + "]";

                                                    ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).SelectCommand = LookupsSorgu;
                                                    ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).DataBind();

                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).ValueField = "Code";
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).TextField = "Adi";
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).DataSource = ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString()));
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).EnableCallbackMode = true;
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).CallbackPageSize = 20;
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).DataBind();
                                                }

                                                ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).Visible = true;
                                                ((ASPxDateEdit)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tar" + CIndex.ToString())).Visible = false;
                                                ((ASPxCheckBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Chk" + CIndex.ToString())).Visible = false;
                                                ((ASPxTextBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tex" + CIndex.ToString())).Visible = false;
                                            }
                                            else
                                            {
                                                DataRow dr2 = this.STDT1.Table.NewRow();
                                                dr2["FieldId"] = "Tex" + CIndex.ToString();
                                                dr2["FieldType"] = "TEX";
                                                dr2["FieldNavNo"] = row["Alan No NAV"].ToString();
                                                dr2["IsMandatory"] = row["Zorunlu"].ToString() == "1" ? 1 : 0;
                                                dr2["FieldName"] = lbl.Text;
                                                dr2["IsLookups"] = 0;
                                                this.STDT1.Table.Rows.Add(dr2);
                                                ((ASPxTextBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tex" + CIndex.ToString())).Visible = true;
                                                ((ASPxDateEdit)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tar" + CIndex.ToString())).Visible = false;
                                                ((ASPxCheckBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Chk" + CIndex.ToString())).Visible = false;
                                                ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).Visible = false;
                                            }
                                            break;
                                        case "INT":
                                            if (!string.IsNullOrEmpty(row["Kaynak Tablo"].ToString()))
                                            {
                                                DataRow dr3 = this.STDT1.Table.NewRow();
                                                dr3["FieldId"] = "Cmb" + CIndex.ToString();
                                                dr3["FieldType"] = "INT";
                                                dr3["FieldNavNo"] = row["Alan No NAV"].ToString();
                                                dr3["IsMandatory"] = row["Zorunlu"].ToString() == "1" ? 1 : 0;
                                                dr3["FieldName"] = lbl.Text;
                                                dr3["IsLookups"] = 1;
                                                this.STDT1.Table.Rows.Add(dr3);

                                                if (row["Kaynak Tablo"].ToString() == "Lookups")
                                                {
                                                    LookupsSorgu = "Select [LineNo] Code, Adi from [0C_50055_00_LOOKUPS] where " +
                                                        "COMPANY='" + _obj.Comp + "' AND Tur='" + row["Lookups Tur"].ToString() + "' Order By [LineNo]";
                                                    ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).SelectCommand = LookupsSorgu;
                                                    ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).DataBind();

                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).ValueField = "Code";
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).TextField = "Adi";
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).DataSource = ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString()));
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).DataBind();

                                                }
                                                ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).Visible = true;
                                                ((ASPxDateEdit)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tar" + CIndex.ToString())).Visible = false;
                                                ((ASPxCheckBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Chk" + CIndex.ToString())).Visible = false;
                                                ((ASPxTextBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tex" + CIndex.ToString())).Visible = false;
                                            }
                                            break;
                                        case "BOO":
                                            DataRow dr4 = this.STDT1.Table.NewRow();
                                            dr4["FieldId"] = "Chk" + CIndex.ToString();
                                            dr4["FieldType"] = "BOO";
                                            dr4["FieldNavNo"] = row["Alan No NAV"].ToString();
                                            dr4["IsMandatory"] = row["Zorunlu"].ToString() == "1" ? 1 : 0;
                                            dr4["FieldName"] = lbl.Text;
                                            dr4["IsLookups"] = 0;
                                            this.STDT1.Table.Rows.Add(dr4);
                                            ((ASPxCheckBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Chk" + CIndex.ToString())).Visible = true;
                                            ((ASPxDateEdit)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tar" + CIndex.ToString())).Visible = false;
                                            ((ASPxTextBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tex" + CIndex.ToString())).Visible = false;
                                            ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).Visible = false;
                                            break;
                                        case "DEC":
                                            DataRow dr5 = this.STDT1.Table.NewRow();
                                            dr5["FieldId"] = "Tex" + CIndex.ToString();
                                            dr5["FieldType"] = "DEC";
                                            dr5["FieldNavNo"] = row["Alan No NAV"].ToString();
                                            dr5["IsMandatory"] = row["Zorunlu"].ToString() == "1" ? 1 : 0;
                                            dr5["FieldName"] = lbl.Text;
                                            dr5["IsLookups"] = 0;
                                            this.STDT1.Table.Rows.Add(dr5);
                                            ((ASPxTextBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tex" + CIndex.ToString())).Visible = true;
                                            ((ASPxDateEdit)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tar" + CIndex.ToString())).Visible = false;
                                            ((ASPxCheckBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Chk" + CIndex.ToString())).Visible = false;
                                            ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).Visible = false;
                                            break;
                                    }
                                    CIndex++;
                                }
                                this.STDT1.Table.AcceptChanges();

                            }
                            else
                            {

                            }
                        }

                    }
                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[GAC_NAV2].[dbo].[SP_0WEB_DOCUMENT_PERMISSIONS_BY_DOCNO]";
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@DocNo", this.cmbDocumenType.Value.ToString());
                    cmd.Parameters.AddWithValue("@Company", _obj.Comp);
                    cmd.Parameters.AddWithValue("@UserName", _obj.UserName);
                    using (SqlDataAdapter adptr = new SqlDataAdapter(cmd))
                    {
                        using (DataSet ds = new DataSet())
                        {
                            adptr.Fill(ds);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) > 0)
                                    btnDosyaBagla.Enabled = true;
                                else
                                    btnDosyaBagla.Enabled = false;
                            }
                        }
                    }

                }
            }
        }

        void clearValues()
        {
            Tex1.Text = string.Empty;
            Tex2.Text = string.Empty;
            Tex3.Text = string.Empty;
            Tex4.Text = string.Empty;
            Tex5.Text = string.Empty;
            Tex6.Text = string.Empty;
            Tex7.Text = string.Empty;
            Tex8.Text = string.Empty;
            Tex9.Text = string.Empty;
            Tex10.Text = string.Empty;
            Tex11.Text = string.Empty;
            Tex12.Text = string.Empty;
            Tex13.Text = string.Empty;
            Tex14.Text = string.Empty;
            Tex15.Text = string.Empty;
            Tex16.Text = string.Empty;
            Tex17.Text = string.Empty;
            Tex18.Text = string.Empty;
            Tex19.Text = string.Empty;
            Tex20.Text = string.Empty;
            Tex21.Text = string.Empty;
            Tex22.Text = string.Empty;
            Tex23.Text = string.Empty;
            Tex24.Text = string.Empty;
            Tex25.Text = string.Empty;

            Tar1.Text = string.Empty;
            Tar2.Text = string.Empty;
            Tar3.Text = string.Empty;
            Tar4.Text = string.Empty;
            Tar5.Text = string.Empty;
            Tar6.Text = string.Empty;
            Tar7.Text = string.Empty;
            Tar8.Text = string.Empty;
            Tar9.Text = string.Empty;
            Tar10.Text = string.Empty;
            Tar11.Text = string.Empty;
            Tar12.Text = string.Empty;
            Tar13.Text = string.Empty;
            Tar14.Text = string.Empty;
            Tar15.Text = string.Empty;
            Tar16.Text = string.Empty;
            Tar17.Text = string.Empty;
            Tar18.Text = string.Empty;
            Tar19.Text = string.Empty;
            Tar20.Text = string.Empty;
            Tar21.Text = string.Empty;
            Tar22.Text = string.Empty;
            Tar23.Text = string.Empty;
            Tar24.Text = string.Empty;
            Tar25.Text = string.Empty;

            Cmb1.SelectedIndex = -1;
            Cmb2.SelectedIndex = -1;
            Cmb3.SelectedIndex = -1;
            Cmb4.SelectedIndex = -1;
            Cmb5.SelectedIndex = -1;
            Cmb6.SelectedIndex = -1;
            Cmb7.SelectedIndex = -1;
            Cmb8.SelectedIndex = -1;
            Cmb9.SelectedIndex = -1;
            Cmb10.SelectedIndex = -1;
            Cmb11.SelectedIndex = -1;
            Cmb12.SelectedIndex = -1;
            Cmb13.SelectedIndex = -1;
            Cmb14.SelectedIndex = -1;
            Cmb15.SelectedIndex = -1;
            Cmb16.SelectedIndex = -1;
            Cmb17.SelectedIndex = -1;
            Cmb18.SelectedIndex = -1;
            Cmb19.SelectedIndex = -1;
            Cmb20.SelectedIndex = -1;
            Cmb21.SelectedIndex = -1;
            Cmb22.SelectedIndex = -1;
            Cmb23.SelectedIndex = -1;
            Cmb24.SelectedIndex = -1;
            Cmb25.SelectedIndex = -1;

            Chk1.Checked = false;
            Chk2.Checked = false;
            Chk3.Checked = false;
            Chk4.Checked = false;
            Chk5.Checked = false;
            Chk6.Checked = false;
            Chk7.Checked = false;
            Chk8.Checked = false;
            Chk9.Checked = false;
            Chk10.Checked = false;
            Chk11.Checked = false;
            Chk12.Checked = false;
            Chk13.Checked = false;
            Chk14.Checked = false;
            Chk15.Checked = false;
            Chk16.Checked = false;
            Chk17.Checked = false;
            Chk18.Checked = false;
            Chk19.Checked = false;
            Chk20.Checked = false;
            Chk21.Checked = false;
            Chk22.Checked = false;
            Chk23.Checked = false;
            Chk24.Checked = false;
            Chk25.Checked = false;

        }

        void SetComponentVisibility(string _FieldId, bool _visible)
        {
            try
            {
                var container = this.Master.FindControl("ContentPlaceHolder1");
                var control = container.FindControl(_FieldId);
                if (control == null) return;
                if (control.GetType() == typeof(HtmlTableRow))
                    control.Visible = _visible;
            }
            catch { }

        }

        private Label GetLabelFromDynamicPanel(string _FieldId)
        {
            try
            {
                var container = this.Master.FindControl("ContentPlaceHolder1");
                var control = container.FindControl(_FieldId);
                if (control == null) return null;
                if (control.GetType() == typeof(Label))
                {
                    return (Label)control;
                }

            }
            catch { return null; }
            return null;
        }
        private HtmlTableCell GetTableCellFromDynamicPanel(string _FieldId)
        {
            try
            {
                var container = this.Master.FindControl("ContentPlaceHolder1");
                var control = container.FindControl(_FieldId);
                if (control == null) return null;
                if (control.GetType() == typeof(HtmlTableCell))
                {
                    return (HtmlTableCell)control;
                }

            }
            catch { return null; }
            return null;
        }


        protected void btnDosyaBagla_Click(object sender, EventArgs e)
        {
            int Company = 0, DocumentGroup = 0;
            string DocumentType = string.Empty;
            string DocumentTarget = string.Empty;


            if (string.IsNullOrEmpty(cmbCompany.Text))
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg01"));
                return;
            }
            if (string.IsNullOrEmpty(cmbDocumentGroup.Text))
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg02"));
                return;
            }
            if (string.IsNullOrEmpty(cmbDocumenType.Text))
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg03"));
                return;
            }

            try
            {
                Company = Convert.ToInt32(f.tekbirsonucdonder("select top 1 [LineNo] from " +
                    "[GAC_NAV2].[dbo].[0C_50055_06_LOOKUPS_IZIN_TALEP_SIRKETI] where Adi='" + cmbCompany.Text + "'"));
            }
            catch
            {
                Company = 0;
            }

            try
            {
                DocumentGroup = Convert.ToInt32(f.tekbirsonucdonder("select top 1 [LineNo] from [GAC_NAV2].[dbo].[0C_50055_01_LOOKUPS_DOKUMAN_GRUBU]" +
                    " where COMPANY='" + _obj.Comp + "' and Adi='" + cmbDocumentGroup.Text + "'"));
            }
            catch
            {
                DocumentGroup = 0;
            }

            try
            {
                DocumentType = f.tekbirsonucdonder("select top 1 [Belge No] from [GAC_NAV2].[dbo].[0C_70000_01_DOCUMENTS GENEL] " +
                    "Where COMPANY='" + _obj.Comp + "' and [Dokuman Grubu]=" + DocumentGroup.ToString() + " and [Belge Adi]='" + cmbDocumenType.Text + "'");
            }
            catch
            {
                DocumentType = string.Empty;
            }

            try
            {
                using (DataTable dt = new DataTable())
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.CommandText = "[sp_0WEB_DOSYA_BAGLA_YENI]";
                            cmd.Connection = conn;
                            cmd.CommandTimeout = 500;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            cmd.Parameters.AddWithValue("@COMP", _obj.Comp);
                            cmd.Parameters.AddWithValue("@PRM1", DocumentGroup.ToString());

                            using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                            {
                                adp.Fill(dt);
                            }
                            if (dt.Rows.Count > 0)
                                DocumentTarget = dt.Rows[0]["PATH"].ToString();
                            else
                                DocumentTarget = string.Empty;
                        }
                    }

                }
                //DocumentTarget = f.tekbirsonucdonder("select top 1 [Aciklama 1] from [GAC_NAV2].[dbo].[0C_50055_01_LOOKUPS_DOKUMAN_GRUBU]" +
                //   " where COMPANY='" + _obj.Comp + "' and Adi='" + cmbDocumentGroup.Text + "'");
            }
            catch
            {
                DocumentTarget = string.Empty;
            }

            foreach (DataRow row in STDT1.Table.Rows)
            {
                if (row["IsMandatory"].ToString() == "0") continue;
                var container = this.Master.FindControl("ContentPlaceHolder1");
                var control = container.FindControl(row["FieldId"].ToString());
                if (control == null) continue;
                if (row["FieldType"].ToString() == "TEX")
                {
                    if (control.GetType() == typeof(ASPxTextBox))
                    {
                        if (string.IsNullOrEmpty(((ASPxTextBox)control).Text))
                        {
                            MessageBox(row["FieldName"].ToString() + " " + l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg04"));
                            return;
                        }
                    }
                    else if (control.GetType() == typeof(ASPxComboBox))
                    {
                        if (string.IsNullOrEmpty(((ASPxComboBox)control).Text))
                        {
                            MessageBox(row["FieldName"].ToString() + " " + l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg04"));
                            return;
                        }
                    }
                }
                else if (row["FieldType"].ToString() == "COD")
                {
                    if (control.GetType() == typeof(ASPxTextBox))
                    {
                        if (string.IsNullOrEmpty(((ASPxTextBox)control).Text))
                        {
                            MessageBox(row["FieldName"].ToString() + " " + l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg04"));
                            return;
                        }
                    }
                    else if (control.GetType() == typeof(ASPxComboBox))
                    {
                        if (string.IsNullOrEmpty(((ASPxComboBox)control).Text))
                        {
                            MessageBox(row["FieldName"].ToString() + " " + l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg04"));
                            return;
                        }
                    }
                }
                else if (row["FieldType"].ToString() == "TAR")
                {
                    if (control.GetType() == typeof(ASPxDateEdit))
                    {
                        if (string.IsNullOrEmpty(((ASPxDateEdit)control).Text))
                        {
                            MessageBox(row["FieldName"].ToString() + " " + l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg04"));
                            return;
                        }
                    }
                }
                else if (row["FieldType"].ToString() == "INT")
                {
                    if (control.GetType() == typeof(ASPxComboBox))
                    {
                        if (string.IsNullOrEmpty(((ASPxComboBox)control).Text))
                        {
                            MessageBox(row["FieldName"].ToString() + " " + l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg04"));
                            return;
                        }
                    }
                }
                else if (row["FieldType"].ToString() == "BOO")
                {
                    if (control.GetType() == typeof(ASPxCheckBox))
                    {
                        if (!((ASPxCheckBox)control).Checked)
                        {
                            MessageBox(row["FieldName"].ToString() + " " + l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg04"));
                            return;
                        }
                    }
                }
                else if (row["FieldType"].ToString() == "DEC")
                {
                    if (control.GetType() == typeof(ASPxTextBox))
                    {
                        if (string.IsNullOrEmpty(((ASPxTextBox)control).Text))
                        {
                            MessageBox(row["FieldName"].ToString() + " " + l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg04"));
                            return;
                        }
                    }
                }


            }

            if (!FileUpload1.HasFile)
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg05"));
                return;
            }

            string extn = System.IO.Path.GetExtension(FileUpload1.FileName);
            string save_path = string.Empty, dosyaupload = string.Empty;
            int tur = Convert.ToInt32(cmbDocumentGroup.Value.ToString());
            int _NextEntryNo = GetNextEntryNo();
            if (extn.ToLower() == ".pdf")
            {
                if (!Directory.Exists(Server.MapPath(CurrentFolderName(_obj.Comp) + DocumentTarget)))
                    Directory.CreateDirectory(Server.MapPath(CurrentFolderName(_obj.Comp) + DocumentTarget));
                save_path = DocumentTarget + lblno.Text + _NextEntryNo.ToString() + extn;
                dosyaupload = Server.MapPath(CurrentFolderName(_obj.Comp) + DocumentTarget) + lblno.Text + _NextEntryNo.ToString() + extn;
                if (File.Exists(dosyaupload.Trim()))
                    File.Delete(dosyaupload.Trim());
                FileUpload1.PostedFile.SaveAs(dosyaupload.Trim());

                // Musa 23.02.2019 Target folder dinamik hale getirilerek Lookup tablosundan okunmaya başlandı.
                //switch (tur)
                //{
                //    case 1:
                //        if (!Directory.Exists(Server.MapPath("Dosya\\MUSTERI\\")))
                //            Directory.CreateDirectory(Server.MapPath("Dosya\\MUSTERI\\"));
                //        save_path = ".\\Dosya\\MUSTERI\\" + lblno.Text + _NextEntryNo.ToString() + extn;
                //        dosyaupload = Server.MapPath("Dosya\\MUSTERI\\") + lblno.Text + _NextEntryNo.ToString() + extn;
                //        if (File.Exists(dosyaupload.Trim()))
                //            File.Delete(dosyaupload.Trim());
                //        FileUpload1.PostedFile.SaveAs(dosyaupload.Trim());
                //        break;
                //    case 2:
                //        if (!Directory.Exists(Server.MapPath("Dosya\\TEDARIKCI\\")))
                //            Directory.CreateDirectory(Server.MapPath("Dosya\\TEDARIKCI\\"));
                //        save_path = ".\\Dosya\\TEDARIKCI\\" + lblno.Text + _NextEntryNo.ToString() + extn;
                //        dosyaupload = Server.MapPath("Dosya\\TEDARIKCI\\") + lblno.Text + _NextEntryNo.ToString() + extn;
                //        if (File.Exists(dosyaupload.Trim()))
                //            File.Delete(dosyaupload.Trim());
                //        FileUpload1.PostedFile.SaveAs(dosyaupload.Trim());
                //        break;
                //    case 3:
                //        if (!Directory.Exists(Server.MapPath("Dosya\\ADAY\\")))
                //            Directory.CreateDirectory(Server.MapPath("Dosya\\ADAY\\"));
                //        save_path = ".\\Dosya\\ADAY\\" + lblno.Text + _NextEntryNo.ToString() + extn;
                //        dosyaupload = Server.MapPath("Dosya\\ADAY\\") + lblno.Text + _NextEntryNo.ToString() + extn;
                //        if (File.Exists(dosyaupload.Trim()))
                //            File.Delete(dosyaupload.Trim());
                //        FileUpload1.PostedFile.SaveAs(dosyaupload.Trim());
                //        break;
                //    case 4:
                //        if (!Directory.Exists(Server.MapPath("Dosya\\JOB\\")))
                //            Directory.CreateDirectory(Server.MapPath("Dosya\\JOB\\"));
                //        save_path = ".\\Dosya\\JOB\\" + lblno.Text + _NextEntryNo.ToString() + extn;
                //        dosyaupload = Server.MapPath("Dosya\\JOB\\") + lblno.Text + _NextEntryNo.ToString() + extn;
                //        if (File.Exists(dosyaupload.Trim()))
                //            File.Delete(dosyaupload.Trim());
                //        FileUpload1.PostedFile.SaveAs(dosyaupload.Trim());
                //        break;
                //    case 5:
                //        if (!Directory.Exists(Server.MapPath("Dosya\\ILETISIM\\")))
                //            Directory.CreateDirectory(Server.MapPath("Dosya\\ILETISIM\\"));
                //        save_path = ".\\Dosya\\ILETISIM\\" + lblno.Text + _NextEntryNo.ToString() + extn;
                //        dosyaupload = Server.MapPath("Dosya\\ILETISIM\\") + lblno.Text + _NextEntryNo.ToString() + extn;
                //        if (File.Exists(dosyaupload.Trim()))
                //            File.Delete(dosyaupload.Trim());
                //        FileUpload1.PostedFile.SaveAs(dosyaupload.Trim());
                //        break; 
                //    case 6:
                //        if (!Directory.Exists(Server.MapPath("Dosya\\PERSONEL\\")))
                //            Directory.CreateDirectory(Server.MapPath("Dosya\\PERSONEL\\"));
                //        save_path = ".\\Dosya\\PERSONEL\\" + lblno.Text + _NextEntryNo.ToString() + extn;
                //        dosyaupload = Server.MapPath("Dosya\\PERSONEL\\") + lblno.Text + _NextEntryNo.ToString() + extn;
                //        if (File.Exists(dosyaupload.Trim()))
                //            File.Delete(dosyaupload.Trim());
                //        FileUpload1.PostedFile.SaveAs(dosyaupload.Trim());
                //        break;
                //    case 7:
                //        if (!Directory.Exists(Server.MapPath("Dosya\\DAHILI\\")))
                //            Directory.CreateDirectory(Server.MapPath("Dosya\\DAHILI\\"));
                //        save_path = ".\\Dosya\\DAHILI\\" + lblno.Text + _NextEntryNo.ToString() + extn;
                //        dosyaupload = Server.MapPath("Dosya\\DAHILI\\") + lblno.Text + _NextEntryNo.ToString() + extn;
                //        if (File.Exists(dosyaupload.Trim()))
                //            File.Delete(dosyaupload.Trim());
                //        FileUpload1.PostedFile.SaveAs(dosyaupload.Trim());
                //        break;
                //    case 8:
                //        if (!Directory.Exists(Server.MapPath("Dosya\\TAYFA\\")))
                //            Directory.CreateDirectory(Server.MapPath("Dosya\\TAYFA\\"));
                //        save_path = ".\\Dosya\\TAYFA\\" + lblno.Text + _NextEntryNo.ToString() + extn;
                //        dosyaupload = Server.MapPath("Dosya\\TAYFA\\") + lblno.Text + _NextEntryNo.ToString() + extn;
                //        if (File.Exists(dosyaupload.Trim()))
                //            File.Delete(dosyaupload.Trim());
                //        FileUpload1.PostedFile.SaveAs(dosyaupload.Trim());
                //        break;
                //    case 9:
                //        if (!Directory.Exists(Server.MapPath("Dosya\\MUTABAKAT\\")))
                //            Directory.CreateDirectory(Server.MapPath("Dosya\\MUTABAKAT\\"));
                //        save_path = ".\\Dosya\\MUTABAKAT\\" + lblno.Text + _NextEntryNo.ToString() + extn;
                //        dosyaupload = Server.MapPath("Dosya\\MUTABAKAT\\") + lblno.Text + _NextEntryNo.ToString() + extn;
                //        if (File.Exists(dosyaupload.Trim()))
                //            File.Delete(dosyaupload.Trim());
                //        FileUpload1.PostedFile.SaveAs(dosyaupload.Trim());
                //        break;
                //    case 10:
                //        if (!Directory.Exists(Server.MapPath("Dosya\\CONTAINER\\")))
                //            Directory.CreateDirectory(Server.MapPath("Dosya\\CONTAINER\\"));
                //        save_path = ".\\Dosya\\CONTAINER\\" + lblno.Text + _NextEntryNo.ToString() + extn;
                //        dosyaupload = Server.MapPath("Dosya\\CONTAINER\\") + lblno.Text + _NextEntryNo.ToString() + extn;
                //        if (File.Exists(dosyaupload.Trim()))
                //            File.Delete(dosyaupload.Trim());
                //        FileUpload1.PostedFile.SaveAs(dosyaupload.Trim());
                //        break;
                //}
            }
            else
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg06"));
                return;
            }



            string FieldsAndValues = string.Empty, FieldValue = string.Empty, FieldType = string.Empty;
            bool ilk = true;
            foreach (DataRow row in STDT1.Table.Rows)
            {
                if (row["FieldType"].ToString() == "TEX" || row["FieldType"].ToString() == "COD")
                    FieldType = "TEX";
                else
                    FieldType = row["FieldType"].ToString();

                if (FieldType == "TAR")
                    FieldValue = string.Format("{0:MM.dd.yyyy}", ((ASPxDateEdit)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Date);
                else if (FieldType == "TEX")
                {
                    if (row["IsLookups"].ToString() == "0")
                        FieldValue = ((ASPxTextBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Text;
                    else if (row["IsLookups"].ToString() == "1")
                        FieldValue = ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Text;
                    else if (row["IsLookups"].ToString() == "2")
                        FieldValue = ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Value.ToString();
                }
                else if (FieldType == "INT")
                    FieldValue = ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Value.ToString();
                else if (FieldType == "BOO")
                    FieldValue = ((ASPxCheckBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Checked ? "YES" : "NO";
                else if (FieldType == "DEC")
                    FieldValue = ((ASPxTextBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Text.Replace(",", ".");
                if (ilk)
                {
                    FieldsAndValues = row["FieldNavNo"].ToString() + ";" + FieldValue.Replace(";", string.Empty).Replace(",", string.Empty) + ";" + FieldType;
                    ilk = false;
                }
                else
                    FieldsAndValues += "," + row["FieldNavNo"].ToString() + ";" + FieldValue.Replace(";", string.Empty).Replace(",", string.Empty) + ";" + FieldType;
            }

            string[] _params = { Company.ToString(), DocumentGroup.ToString(), DocumentType, FieldsAndValues, _obj.UserName, save_path, lblno.Text };
            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "DocDetailsGenelOlustur§" + string.Join("§", _params), _obj.Comp);



            if (Request.RawUrl.Contains("?"))
                Response.Redirect(Request.RawUrl + "&CreatedDoc=1");
            else
                Response.Redirect(Request.RawUrl + "?CreatedDoc=1");



        }

        void WriteLog(string ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex);
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            string path = HttpContext.Current.Server.MapPath("~/App_Data/Log.txt");
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(path, true))
            {
                writer.WriteLine(message);
                writer.Close();
            }
        }
        void FillDocumentType(int Group)
        {
            cmbDocumenType.Items.Clear();
            DSDocumentType.SelectCommand = "select [Belge No] Code, [Belge Adi] Adi from [GAC_NAV2].[dbo].[0C_70000_01_DOCUMENTS GENEL] " +
                "where  COMPANY='" + _obj.Comp + "' and [Dokuman Grubu]=" + Group.ToString() + "  order by [Belge Adi]";
            cmbDocumenType.DataBind();
        }
        protected void cmbDocumentGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            int DocumentGroup = 0;
            try
            {
                DocumentGroup = Convert.ToInt32(f.tekbirsonucdonder("select top 1 [LineNo] from [GAC_NAV2].[dbo].[0C_50055_01_LOOKUPS_DOKUMAN_GRUBU]" +
                    " where  COMPANY='" + _obj.Comp + "' and  Adi='" + cmbDocumentGroup.Text + "'"));
            }
            catch
            {
                DocumentGroup = 0;
            }
            FillDocumentType(DocumentGroup);


        }
    }
}