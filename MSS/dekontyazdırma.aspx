﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="dekontyazdırma.aspx.cs" Inherits="MSS1.dekontyazdırma" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function proceed() {
            opener.location.reload(true);
            opener.location = 'satis_sip.aspx';
            self.close();
        }
        function nakletsoru() {
            if (confirm('Deftere Naklet/Yazdırmak istiyor musunuz ?')) {
                return true;
            }
            else {
                alert("Nakil İşlemi İptal Edildi !");
                return false;
            }

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table cellspacing="10">
            <tr>
                <td class="style2">
                    <asp:Label ID="lblFtTuruB1" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlft1" CssClass="textbox" runat="server" Width="300px" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlft1_SelectedIndexChanged">
                        <asp:ListItem Selected="True"></asp:ListItem>
                        <asp:ListItem Value="1">1 - DETAYLI</asp:ListItem>
                        <asp:ListItem Value="2">2 - TOPLU</asp:ListItem>
                        <asp:ListItem Value="3">3 - GRUPLU - KAYNAK </asp:ListItem>
                        <asp:ListItem Value="4">4 - GRUPLU - MANUEL</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="lblFtTuruB2" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlft2" CssClass="textbox" runat="server" Width="300px" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlft2_SelectedIndexChanged" Enabled="False">
                        <asp:ListItem></asp:ListItem>
                        <asp:ListItem Value="1" Selected="True">1 - NORMAL</asp:ListItem>
                        <asp:ListItem Value="2">2 - PROFORMA</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="lblTopFtMetni1" runat="server"></asp:Label>
&nbsp;</td>
                <td>
                    <asp:TextBox ID="txtftmetni1" CssClass="textbox" runat="server" Enabled="False" 
                        MaxLength="80" Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="lblTopFtMetni2" runat="server"></asp:Label>
&nbsp;</td>
                <td>
                    <asp:TextBox ID="txtftmetni2" CssClass="textbox" runat="server" Enabled="False" 
                        MaxLength="60" Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="lblTopFtMetni3" runat="server"></asp:Label>
&nbsp;</td>
                <td>
                    <asp:TextBox ID="txtftmetni3" CssClass="textbox" runat="server" Enabled="False" 
                        MaxLength="60" Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="lblGrup1" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtgrup1a" CssClass="textbox" runat="server" 
                        MaxLength="80" Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="lblGrup2" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtgrup2a" CssClass="textbox" runat="server" 
                        MaxLength="80" Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="lblGrup3" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtgrup3a" CssClass="textbox" runat="server" 
                        MaxLength="80" Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;<asp:Label ID="lblFtEkMetinB" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtftekmetin" CssClass="textbox" runat="server" Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;<asp:Label ID="lblFtTarihi" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtfaturatarihi" CssClass="textbox" runat="server" Style="text-align: justify"
                        ValidationGroup="MKE" Width="300px" Enabled="false" />
                    <cc1:CalendarExtender ID="txtfaturatarihi_CalendarExtender" runat="server" TargetControlID="txtfaturatarihi"
                        Format="dd.MM.yyyy" />
                    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptLocalization="true"
                        EnableScriptGlobalization="true" EnablePageMethods="true">
                    </asp:ScriptManager>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="lblticari" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtticariunvan" CssClass="textbox" runat="server" MaxLength="100" 
                        Width="300px"></asp:TextBox>
                </td>
            </tr>
        </table>
        <br />
        <table cellspacing="10">
            <tr>
                <td class="style3">
                    <asp:Label ID="lblHariciBelgeNoB" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txthbno" CssClass="textbox" runat="server" MaxLength="20" 
                        Width="300px" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Label ID="lblLimanB" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtliman" CssClass="textbox" runat="server" MaxLength="50" 
                        Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Label ID="lblTanimB" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txttanim" CssClass="textbox" runat="server" MaxLength="50" 
                        Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    &nbsp;<asp:Label ID="lblTasiyiciAdB" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtseferno" CssClass="textbox" runat="server" MaxLength="30" 
                        Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    &nbsp;<asp:Label ID="lblKonsimentoB" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtawb" CssClass="textbox" runat="server" MaxLength="30" 
                        Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    &nbsp;<asp:Label ID="lblKAPAdetB" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtadet" CssClass="textbox" runat="server" MaxLength="20" 
                        Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    &nbsp;<asp:Label ID="lblKargoB" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtagırlık" CssClass="textbox" runat="server" MaxLength="20" 
                        Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    &nbsp;<asp:Label ID="lblBirimB" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtbirim" CssClass="textbox" runat="server" MaxLength="20" 
                        Width="300px"></asp:TextBox>
                </td>
            </tr>
        </table>
        <div id="altmenuyazdir">
            <asp:Button ID="btnönizleme" runat="server" CssClass="myButton" ViewStateMode="Enabled"
                OnClick="btnönizleme_Click" />
            <asp:Button ID="btnyazdır" runat="server" CssClass="myButton"
                OnClick="btnyazdır_Click" ViewStateMode="Enabled" OnClientClick="javascript: return nakletsoru()" />
        </div>
    </div>
    </form>
</body>
</html>
