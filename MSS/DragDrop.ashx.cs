﻿using MSS1.WSGeneric;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.SessionState;

namespace MSS1
{
    /// <summary>
    /// DragDrop için özet açıklama
    /// </summary>
    public class DragDrop : IHttpHandler, IRequiresSessionState
    {

        public static Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            Web_GenericFunction_PortClient webFonksiyonlari;
            BasicHttpBinding _binding;
            ws_baglantı ws = new ws_baglantı();
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            EndpointAddress _endpoint = new EndpointAddress(new Uri(ws.sirketal(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }
        public void ProcessRequest(HttpContext context)
        {
            string result = "";
            string[] getUrlParams = context.Request.QueryString["Params"].Split(';');
            string username = getUrlParams[0];
            string company = getUrlParams[1];
            string dosyayolu = "";
            if (company == "GAC" || company == "LAM")
                dosyayolu = "GACLAM";
            else
                dosyayolu = company;

            string pageparams = getUrlParams[2];

            string hiddenparams = context.Request.Params[1];
            if (!string.IsNullOrEmpty(context.Request.Params[2]))
            {
                hiddenparams += "_" + context.Request.Params[2];
            }

            try
            {
                foreach (string key in context.Request.Files)
                {
                    HttpPostedFile postedFile = context.Request.Files[key];

                    if (!Directory.Exists(context.Server.MapPath("~/" + dosyayolu + "/Dosya/_TEMP")))
                        Directory.CreateDirectory(context.Server.MapPath("~/" + dosyayolu + "/Dosya/_TEMP"));
                    string newFileName = DateTime.Now.ToFileTime() + "_" + postedFile.FileName;
                    string TargetFilePath = context.Server.MapPath("~/" + dosyayolu + "/Dosya/_TEMP/" + newFileName);
                    if (File.Exists(TargetFilePath))
                    {
                        return;
                    }
                    if (!string.IsNullOrEmpty(postedFile.FileName))
                    {
                        postedFile.SaveAs(TargetFilePath);
                    }
                    string[] _params = { username, pageparams, TargetFilePath
                         , newFileName, hiddenparams, ""
                         , "", "", "" };
                    string navReturn = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(username, "50000", "AfterWebDocImport§" + string.Join("§", _params), company);
                    if (!string.IsNullOrEmpty(navReturn))
                        result = navReturn + "|"+postedFile.FileName;
                }
                context.Response.ContentType = "text/plain";
                context.Response.Write(result);

            }
            catch (Exception ex)
            {

                context.Response.ContentType = "text/plain";
                context.Response.Write(ex.Message);
            }

           

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}