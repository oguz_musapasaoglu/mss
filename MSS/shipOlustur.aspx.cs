﻿using MSS1.Codes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.WSGeneric; 
using System.ServiceModel;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;


namespace MSS1
{
    public partial class shipOlustur : Bases
    {
        #region Connection String & Service
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar(); 
        public ws_baglantı ws = new ws_baglantı(); 
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        SessionBase _obj;
        #endregion

        public void dilgetir()
        {
            lblShipName.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "shipOlustur", "lblShipName");
            lblImoNo.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "shipOlustur", "lblImoNo");
            lblFlag.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "shipOlustur", "lblFlag");
            lblPasif.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "shipOlustur", "lblPasif");
            lblExName.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "shipOlustur", "lblExName");
        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }


        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            btnOlustur.Attributes.Add("onClick", "displayLoadingImage()");
            if (!IsPostBack)
            {
                fillcombox();
                dilgetir();
                if (Request.QueryString["ID"] != null)
                {
                    lblBaslik.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "shipOlustur", "Duzenle");
                    shipDoldur(_obj.Comp, Request.QueryString["ID"].ToString());
                    btnOlustur.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "shipOlustur", "Gemi Duzenle");
                }
                else
                {

                    lblBaslik.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "shipOlustur", "lblBaslik");
                    txtShipName.Text = string.Empty;
                    txtImoNo.Text = string.Empty;
                    btnOlustur.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "shipOlustur", "btnOlustur");
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }

        void fillcombox()
        {
            using (SqlConnection baglanti = new SqlConnection(strConnString))
            {
                baglanti.Open();
                SqlCommand sorgu = new SqlCommand();

                sorgu.Connection = baglanti;
                sorgu.CommandText = "Select Code,Name From [0C_00009_00_COUNTRY] where COMPANY='" + _obj.Comp + "' Order By Name";

                SqlDataAdapter sorguDA = new SqlDataAdapter(sorgu);
                DataTable sorgugridDT = new DataTable();
                sorguDA.Fill(sorgugridDT);

                ddlFlag.DataTextField = "Name";
                ddlFlag.DataValueField = "Code";
                ddlFlag.DataSource = sorgugridDT;
                ddlFlag.DataBind();
                ddlFlag.SelectedValue = "-1";

            }
        }

        protected void btnOlustur_Click(object sender, EventArgs e)
        {
            
            if (btnOlustur.Text == "Gemi Oluştur")
            {
                if (string.IsNullOrEmpty(txtImoNo.Text))
                {
                    MessageBox("Imo No alanı boş geçilemez!");
                    return;
                }
                if (!CheckImoNo(txtImoNo.Text))
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "shipOlustur", "msg003"));
                    return;
                }
                int _imocount = 0;
                string sql = "select count(*) from [0C_50008_00_VESSELS] where COMPANY='" + _obj.Comp + "' and [IMO No]='" + txtImoNo.Text + "' and Pasif=0";
                _imocount = Convert.ToInt32(f.tekbirsonucdonder(sql));
                if (_imocount > 0)
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "shipOlustur", "msg004"));
                    return;
                }
                if (txtShipName.Text != string.Empty)
                { 
                    GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName,"50000",
                      "ShipOlustur" + "§" + txtShipName.Text + "§" + txtImoNo.Text + "§" + ddlFlag.SelectedValue + "§" + txtExName.Text 
                      , _obj.Comp);

                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "shipOlustur", "msg002"));
                    txtShipName.Text = string.Empty;
                    txtImoNo.Text = string.Empty;
                    ddlFlag.SelectedValue = "-1";
                    txtExName.Text = string.Empty;

                }
                else
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "shipOlustur", "msg001"));
                }
            }
            else
            {
                if (txtShipName.Text != string.Empty)
                {
                    if (string.IsNullOrEmpty(txtImoNo.Text))
                    {
                        MessageBox("Imo No alanı boş geçilemez!");
                        return;
                    }
                    if (!CheckImoNo(txtImoNo.Text))
                    {
                        MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "shipOlustur", "msg003"));
                        return;
                    }
                    int _imocount = 0;
                    string sql = "select count(*) from [0C_50008_00_VESSELS] where COMPANY='" + _obj.Comp + "' and [IMO No]='" + txtImoNo.Text + "' and [Ship No]!='" + Request.QueryString["ID"].ToString() + "' and Pasif=0";
                    _imocount = Convert.ToInt32(f.tekbirsonucdonder(sql));
                    if (_imocount > 0)
                    {
                        MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "shipOlustur", "msg004"));
                        return;
                    }
                     

                    

                    GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName,"50000", 
                        "ShipDuzenle" + "§" + 
                        Request.QueryString["ID"] + "§"+ 
                        txtShipName.Text + "§" + 
                        txtImoNo.Text + "§" + 
                        ddlFlag.SelectedValue + "§" + 
                        txtExName.Text + "§" + CheckDurum()
                        , _obj.Comp);



                    txtShipName.Text = string.Empty;
                    txtImoNo.Text = string.Empty;
                    ddlFlag.SelectedValue = "-1";
                    Response.Redirect("~/shipOlustur.aspx?ID=" + Request.QueryString["ID"].ToString()+ "&GId=" + Request.QueryString["GId"]);
                }
                else
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "tayfagir", "msg001"));
                }
            }

        }

        private string CheckDurum()
        {
            if (chkPasif.Checked)
                return "1";
            else
                return "0";
        }
        private bool CheckImoNo(string imono)
        {
            bool _result = true;
            try
            { 
                string drm = GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName,"50007", "CheckIMO" + "§"+ imono, _obj.Comp);
                if (drm == "True")
                    _result = true;
                else
                    _result = false;
                 
            }
            catch
            {
                _result = false;
            }

            return _result;
        }

        private void shipDoldur(string sirket, string id)
        {
            using (SqlConnection baglanti = new SqlConnection(strConnString))
            {
                baglanti.Open();

                using (SqlCommand sorgu = new SqlCommand("sp_0WEB_LUCENT_GEMI_DOLDUR", baglanti))
                {
                    sorgu.CommandType = CommandType.StoredProcedure;
                    sorgu.Parameters.AddWithValue("@id", id);
                    sorgu.Parameters.AddWithValue("@sirket", sirket);

                    SqlDataAdapter sorguDA = new SqlDataAdapter(sorgu);
                    DataTable sorgugridDT = new DataTable();
                    sorguDA.Fill(sorgugridDT);
                    SqlDataReader sqr;
                    sqr = sorgu.ExecuteReader();
                    if (sqr.Read())
                    {
                        txtShipName.Text = sqr["ShipName"].ToString();
                        txtImoNo.Text = sqr["imono"].ToString();
                        txtExName.Text = sqr["exshipname"].ToString();
                        if (!String.IsNullOrEmpty(sqr["Flag"].ToString()))
                            ddlFlag.SelectedValue = sqr["Flag"].ToString();
                        else
                            ddlFlag.SelectedValue = "-1";

                        chkPasif.Checked = Convert.ToInt32(sqr["Pasif"]) == 0 ? false : true;
                    }
                    sqr.Close();
                }
            }

        }
    }
}