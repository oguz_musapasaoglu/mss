﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using MSS1.Codes;

namespace MSS1
{
    public partial class Home : System.Web.UI.Page
    {
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        SessionBase _obj;
        protected void Page_Load(object sender, EventArgs e)
        {
            
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());
            if (IsCallback || IsPostBack) return;
            preparemiddleside();
            LoadDocument(_obj.UserName);
        }

        void preparemiddleside()
        {
            
            int rowsayac = 1, cellsayac = 1;
            for (int i = 1; i <= 100; i++)
            {
                HtmlTableRow row = new HtmlTableRow();
                row.ID = "Tr" + rowsayac.ToString();

                for (int j = 1; j <= 100; j++)
                {

                    HtmlTableCell cell = new HtmlTableCell();
                    cell.ID = "Td" + cellsayac.ToString();
                    row.Cells.Add(cell);
                    cellsayac++;
                }

                tblmain.Rows.Add(row);
                rowsayac++;


            }
        }

        void LoadDocument(string UserName)
        {
            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "SP_GetWizardDetails";
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@UserName", _obj.UserName);
                    cmd.Parameters.AddWithValue("@Company", _obj.Comp);

                    using (SqlDataAdapter adptr = new SqlDataAdapter(cmd))
                    {
                        using (DataSet ds = new DataSet())
                        {
                            adptr.Fill(ds);

                            // TO DO
                            string labeltext = string.Empty, linktext = string.Empty, sqlquery = string.Empty, alantipi = string.Empty, DeptUser = string.Empty
                            , PostBackUrl = string.Empty, PageId = string.Empty, OJ1 = string.Empty, OJ2 = string.Empty, OJ3 = string.Empty, parameters = string.Empty;
                            string ekipdepts = string.Empty;
                            string[] ekips = ("a|b").Split('|');
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                sqlquery = string.Empty;

                                try
                                {
                                    //PageId = row["Path"].ToString();
                                    //sqlquery = "select top 1 [Web Object ID] from WebPages where [Web Page ID]='" + PageId + "' and [Object Order]=1";
                                    //OJ1 = f.tekbirsonucdonder(sqlquery);
                                    //sqlquery = "select top 1 [Web Object ID] from WebPages where [Web Page ID]='" + PageId + "'and [Object Order]=2";
                                    //OJ2 = f.tekbirsonucdonder(sqlquery);
                                    //sqlquery = "select top 1 [Web Object ID] from WebPages where [Web Page ID]='" + PageId + "'and [Object Order]=3";
                                    //OJ3 = f.tekbirsonucdonder(sqlquery);
                                    //sqlquery = string.Empty;

                                    labeltext = row["Ad TR"].ToString();
                                    linktext = row["Birim TR"].ToString();
                                    //if (OJ3 != "NULL")
                                    //{
                                    //    parameters = "?OJ1=" + PageId + ";" + OJ1 + "&OJ2=" + PageId + ";" + OJ2 + "&OJ3=" + PageId + ";" + OJ3;
                                    //}
                                    //else
                                    //{
                                    //    parameters = "?OJ1=" + PageId + ";" + OJ1 + "&OJ2=" + PageId + ";" + OJ2;
                                    //}


                                    PostBackUrl = "JavaScript:window.open('" + row["Path"].ToString() + "&GId=" + Request.QueryString["GId"] + "'); return false;";
                                    SetLabelAndLinks(Convert.ToInt32(row["Alan 1"].ToString().Replace("R", string.Empty).Replace("L", string.Empty)), labeltext, linktext, PostBackUrl);

                                }
                                catch
                                {

                                    labeltext = "Kayıtta Hata Var";
                                    linktext = string.Empty;
                                    PostBackUrl = "JavaScript: return false;";
                                    SetLabelAndLinks(Convert.ToInt32(row["Alan 1"].ToString().Replace("R", string.Empty).Replace("L", string.Empty)), labeltext, linktext, PostBackUrl);
                                }


                            }

                        }
                    }
                }
            }
        }

        void SetLabelAndLinks(int sayac, string LabelText, string LabelLinkText, string PostBackUrl)
        {
            try
            {
                switch (sayac)
                {
                    case 1:
                        TodoRight1.Attributes.Add("class", "todorighttd");
                        Label1.Text = LabelText;
                        LinkButton1.Text = LabelLinkText.Replace("NULL", "0");
                        LinkButton1.OnClientClick = PostBackUrl;
                        break;
                    case 2:
                        TodoRight2.Attributes.Add("class", "todorighttd");
                        Label2.Text = LabelText;
                        LinkButton2.Text = LabelLinkText.Replace("NULL", "0");
                        LinkButton2.OnClientClick = PostBackUrl;
                        break;
                    case 3:
                        TodoRight3.Attributes.Add("class", "todorighttd");
                        Label3.Text = LabelText;
                        LinkButton3.Text = LabelLinkText.Replace("NULL", "0");
                        LinkButton3.OnClientClick = PostBackUrl;
                        break;
                    case 4:
                        TodoRight4.Attributes.Add("class", "todorighttd");
                        Label4.Text = LabelText;
                        LinkButton4.Text = LabelLinkText.Replace("NULL", "0");
                        LinkButton4.OnClientClick = PostBackUrl;
                        break;
                    case 5:
                        TodoRight5.Attributes.Add("class", "todorighttd");
                        Label5.Text = LabelText;
                        LinkButton5.Text = LabelLinkText.Replace("NULL", "0");
                        LinkButton5.OnClientClick = PostBackUrl;
                        break;
                    case 6:
                        TodoRight6.Attributes.Add("class", "todorighttd");
                        Label6.Text = LabelText;
                        LinkButton6.Text = LabelLinkText.Replace("NULL", "0");
                        LinkButton6.OnClientClick = PostBackUrl;
                        break;
                    case 7:
                        TodoRight7.Attributes.Add("class", "todorighttd");
                        Label7.Text = LabelText;
                        LinkButton7.Text = LabelLinkText.Replace("NULL", "0");
                        LinkButton7.OnClientClick = PostBackUrl;
                        break;
                    case 8:
                        TodoRight8.Attributes.Add("class", "todorighttd");
                        Label8.Text = LabelText;
                        LinkButton8.Text = LabelLinkText.Replace("NULL", "0");
                        LinkButton8.OnClientClick = PostBackUrl;
                        break;
                    case 9:
                        TodoRight9.Attributes.Add("class", "todorighttd");
                        Label9.Text = LabelText;
                        LinkButton9.Text = LabelLinkText.Replace("NULL", "0");
                        LinkButton9.OnClientClick = PostBackUrl;
                        break;
                    case 10:
                        TodoRight10.Attributes.Add("class", "todorighttd");
                        Label10.Text = LabelText;
                        LinkButton10.Text = LabelLinkText.Replace("NULL", "0");
                        LinkButton10.OnClientClick = PostBackUrl;
                        break;
                    case 11:
                        TodoRight11.Attributes.Add("class", "todorighttd");
                        Label11.Text = LabelText;
                        LinkButton11.Text = LabelLinkText.Replace("NULL", "0");
                        LinkButton11.OnClientClick = PostBackUrl;
                        break;
                    case 12:
                        TodoRight12.Attributes.Add("class", "todorighttd");
                        Label12.Text = LabelText;
                        LinkButton12.Text = LabelLinkText.Replace("NULL", "0");
                        LinkButton12.OnClientClick = PostBackUrl;
                        break;
                    case 13:
                        TodoRight13.Attributes.Add("class", "todorighttd");
                        Label13.Text = LabelText;
                        LinkButton13.Text = LabelLinkText.Replace("NULL", "0");
                        LinkButton13.OnClientClick = PostBackUrl;
                        break;
                    case 14:
                        TodoRight14.Attributes.Add("class", "todorighttd");
                        Label14.Text = LabelText;
                        LinkButton14.Text = LabelLinkText.Replace("NULL", "0");
                        LinkButton14.OnClientClick = PostBackUrl;
                        break;
                    case 15:
                        TodoRight15.Attributes.Add("class", "todorighttd");
                        Label15.Text = LabelText;
                        LinkButton15.Text = LabelLinkText.Replace("NULL", "0");
                        LinkButton15.OnClientClick = PostBackUrl;
                        break;
                    case 16:
                        TodoRight16.Attributes.Add("class", "todorighttd");
                        Label16.Text = LabelText;
                        LinkButton16.Text = LabelLinkText.Replace("NULL", "0");
                        LinkButton16.OnClientClick = PostBackUrl;
                        break;
                    case 17:
                        TodoRight17.Attributes.Add("class", "todorighttd");
                        Label17.Text = LabelText;
                        LinkButton17.Text = LabelLinkText.Replace("NULL", "0");
                        LinkButton17.OnClientClick = PostBackUrl;
                        break;
                    case 18:
                        TodoRight18.Attributes.Add("class", "todorighttd");
                        Label18.Text = LabelText;
                        LinkButton18.Text = LabelLinkText.Replace("NULL", "0");
                        LinkButton18.OnClientClick = PostBackUrl;
                        break;
                    case 19:
                        TodoRight19.Attributes.Add("class", "todorighttd");
                        Label19.Text = LabelText;
                        LinkButton19.Text = LabelLinkText.Replace("NULL", "0");
                        LinkButton19.OnClientClick = PostBackUrl;
                        break;
                    case 20:
                        TodoRight20.Attributes.Add("class", "todorighttd");
                        Label20.Text = LabelText;
                        LinkButton20.Text = LabelLinkText.Replace("NULL", "0");
                        LinkButton20.OnClientClick = PostBackUrl;
                        break;
                    default:
                        break;
                }
            }
            catch { }
        }
    }
}