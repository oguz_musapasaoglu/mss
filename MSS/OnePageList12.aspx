﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="OnePageList12.aspx.cs" EnableViewState="true" Inherits="MSS1.OnePageList12" %>

<%@ Register Src="~/controls/DataTableControlGAC.ascx" TagName="StDataTable" TagPrefix="STDT" %>

<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        iframe {
            -moz-transform: scale(0.99, 0.99);
            -webkit-transform: scale(0.99, 0.99);
            -o-transform: scale(0.99, 0.99);
            -ms-transform: scale(0.99, 0.99);
            transform: scale(0.99, 0.99);
            -moz-transform-origin: top center;
            -webkit-transform-origin: top center;
            -o-transform-origin: top center;
            -ms-transform-origin: top center;
            transform-origin: top center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript"> 

        function OnRefreshAllObjects(s, e) {
            if (s.cpIsUpdated) {
                try { grid1.PerformCallback(''); } catch (err) { }  
            }
        }


        function OnGridFocusedRowChanged(s, e) {
            var link = $("#" + "<%= txtlink.ClientID %>").val();
            var linkfield = "<%= txtlinkField.ClientID %>";
            if (link == "JOB") {
                s.GetRowValues(s.GetFocusedRowIndex(), $("#" + linkfield).val(), OnGetRowValuesJob);
            }
            else if (link == "FILTER") {
                s.GetRowValues(s.GetFocusedRowIndex(), $("#" + linkfield).val(), OnGetRowValuesFilter);
            }
            else {
                s.GetRowValues(s.GetFocusedRowIndex(), $("#" + linkfield).val(), OnGetRowValues);
            }
        }

        function OnGetRowValues(values) {
            var linkFormat = "<%= txtlink.ClientID %>";
            var link = $("#" + linkFormat).val();
            link = link.replace("XXXXX", values);
            //link = link.replace("YYYYY", values[1]);
            window.location.href = AdGIdToLink(link);
            window.focus();
        }

        function AdGIdToLink(_val) {
            if (_val.includes("GId=")) return _val;
            var ObjGId = "<%= lblGId.ClientID %>";
            if (_val.includes("?"))
                _val = _val + "&GId=" + $("#" + ObjGId).text();
            else
                _val = _val + "?GId=" + $("#" + ObjGId).text();

            return _val;

        }


        function OnGetRowValuesFilter(values) {
            var queryText = "";
            var linkFormat = "<%= txtlinkField.ClientID %>";
            var filterobject = "<%= txtFilterText.ClientID %>";
            var link = $("#" + linkFormat).val();
            var filterFields = link.split(';');

            if (filterFields.length == 1) {
                queryText = filterFields[0] + " = '" + values + "'";
            }
            else if (filterFields.length == 2) {
                queryText = filterFields[0] + " = '" + values[0] + "' and " + filterFields[1] + " = '" + values[1] + "'";
            }
            else if (filterFields.length == 3) {
                queryText = filterFields[0] + " = '" + values[0] + "' and " + filterFields[1] + " = '" + values[1] + " and " + filterFields[2] + " = '" + values[2] + "'";
            }
            else if (filterFields.length == 4) {
                queryText = filterFields[0] + " = '" + values[0] + "' and " + filterFields[1] + " = '" + values[1] + " and " + filterFields[2] + " = '" + values[2] + "' and " + filterFields[3] + " = '" + values[3] + "'";
            }
             
            rpCollepse1.SetCollapsed(false);
            rpCollepse2.SetCollapsed(true);
            rpCollepse3.SetCollapsed(true);
            rpCollepse4.SetCollapsed(true);
            rpCollepse5.SetCollapsed(true);
            rpCollepse6.SetCollapsed(true);
            rpCollepse7.SetCollapsed(true);
            rpCollepse8.SetCollapsed(true);
 



            document.getElementById('ctl00_ContentPlaceHolder1_HiddenLoadqueryText').value = queryText;
            var splitter = values.toString().split(","); 
            var pageTypeFormat = "<%= txtpageType.ClientID %>";
            var pageType = $("#" + pageTypeFormat).val();

            if (pageType=="FIRMA") {
                document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rpCollepse1_iframeView').src = AdGIdToLink('/CRM/FirmaCard.aspx?CCUID=' + splitter[0]); 
            }
            else if (pageType == "MUSTERI") { 
                document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rpCollepse1_iframeView').src = AdGIdToLink('/CRM/musteriCard.aspx?CCUID=' + splitter[0]);
            }
            else if (pageType == "TEDARIKCI") {
                document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rpCollepse1_iframeView').src = AdGIdToLink('/CRM/FirmaCard.aspx?CCUID=' + splitter[0]);
            }
            else if (pageType == "KONTAK") {
                document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rpCollepse1_iframeView').src = AdGIdToLink('/CRM/FirmaCard.aspx?CCUID=' + splitter[0]);  
            }
            
        }



        function OnGetRowValuesJob(values) {
            //alert(values);
            var splitter = values.split(';');
            var targetLink = "";
            if (splitter[1] == "1") {
                window.open("/Jobs/mainJobs.aspx?MJID=" + splitter[0]);
            }
            else {

                window.open("/projeolustur.aspx?Tur=1&ProjeNo=" + splitter[0]);
            }
        }


        var postponedCallbackRequired = false;
        function OnListBoxIndexChanged(values) {
            if (iframecallbackPanel.InCallback())
                postponedCallbackRequired = true;
            else
                iframecallbackPanel.PerformCallback(values);
        }
        function OnEndCallback(values) { 
            if (postponedCallbackRequired) {
                iframecallbackPanel.PerformCallback(values);
                postponedCallbackRequired = false;
            }
        }
    </script>
    <asp:HiddenField runat="server" ID="HiddenLoadqueryText" Value="AA" />
    <asp:Label ID="lblden" runat="server"></asp:Label>
    <dx:ASPxRadioButtonList RepeatDirection="Horizontal" Theme="Glass" runat="server" Width="100%" ID="rblDynamicList" 
        OnSelectedIndexChanged="rblDynamicList_SelectedIndexChanged" AutoPostBack="true"> 
    </dx:ASPxRadioButtonList>
      

     <dx:ASPxMenu ID="menu1" runat="server" AutoSeparators="RootOnly"
        Theme="Glass"
        ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
        ShowSubMenuShadow="False" OnItemClick="menu1_ItemClick">
        <SubMenuStyle GutterWidth="0px" />
        <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
        <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
        </SubMenuItemStyle>
        <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
    </dx:ASPxMenu>
    <dx:ASPxGridView ID="grid1" ClientInstanceName="grid1" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
        Theme="Glass" OnCustomCallback="grid1_CustomCallback" OnCellEditorInitialize="grid1_CellEditorInitialize" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
        EnableTheming="false" DataSourceID="dtgrid1" KeyFieldName="ID" OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
        OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnRowDeleting="grid_RowDeleting">
        <ClientSideEvents RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e);} " EndCallback="function(s, e) {OnRefreshAllObjects(s,e)}"  />
        <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" AllowFocusedRow="true" />
        <SettingsPager PageSize="7" ShowSeparators="True"></SettingsPager>
        <SettingsText EmptyDataRow=" " />
        <SettingsEditing Mode="PopupEditForm" EditFormColumnCount="3" UseFormLayout="true" NewItemRowPosition="Top"></SettingsEditing>
        <SettingsPopup EditForm-Modal="true" CustomizationWindow-CloseOnEscape="True" EditForm-ShowFooter="false" EditForm-Width="800" EditForm-AllowResize="true" EditForm-HorizontalAlign="WindowCenter" EditForm-VerticalAlign="WindowCenter"></SettingsPopup>
                                                
        <SettingsCommandButton>
            <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
            </CancelButton>
            <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
            </UpdateButton>
        </SettingsCommandButton>
    
        <Styles >
            <Cell Paddings-Padding="1" CssClass="gridNewCellStyle"></Cell>
                                                        
            <Header ForeColor="White"></Header>
            <HeaderPanel ForeColor="White"></HeaderPanel>
            <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
        </Styles>
    </dx:ASPxGridView>
    <dx:ASPxGridViewExporter ID="gridExport1" ExportedRowType="All" runat="server" GridViewID="grid1"></dx:ASPxGridViewExporter>
    <STDT:stdatatable id="dtgrid1" runat="server" />
  
    <STDT:StDataTable ID="DS11" runat="server" />
    <STDT:StDataTable ID="DS12" runat="server" />
    <STDT:StDataTable ID="DS13" runat="server" />
    <STDT:StDataTable ID="DS14" runat="server" />
    <STDT:StDataTable ID="DS15" runat="server" />
    <STDT:StDataTable ID="DS16" runat="server" />
    <STDT:StDataTable ID="DS17" runat="server" />
    <STDT:StDataTable ID="DS18" runat="server" />
    <STDT:StDataTable ID="DS19" runat="server" />
    <STDT:StDataTable ID="DS110" runat="server" />
    <STDT:StDataTable ID="DS111" runat="server" />
    <STDT:StDataTable ID="DS112" runat="server" />
    <STDT:StDataTable ID="DS113" runat="server" />
    <STDT:StDataTable ID="DS114" runat="server" />
    <STDT:StDataTable ID="DS115" runat="server" />
    <STDT:StDataTable ID="DS116" runat="server" />
    <STDT:StDataTable ID="DS117" runat="server" />
    <STDT:StDataTable ID="DS118" runat="server" />
    <STDT:StDataTable ID="DS119" runat="server" />
    <STDT:StDataTable ID="DS120" runat="server" />
     
    <div style="display: none">
         <asp:Label ID="lblGId" runat="server"></asp:Label>
        <asp:TextBox ID="txtlink" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtFilterText" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams1" runat="server"></asp:TextBox>
        
        <asp:TextBox ID="txtpageType" runat="server"></asp:TextBox>
       
    </div>
</asp:Content>
