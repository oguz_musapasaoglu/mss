﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="satisyazdırma.aspx.cs" Inherits="MSS1.satisyazdırma" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function proceed() {
            opener.location.reload(true);
            opener.location = 'satis_sip.aspx';
            self.close();
        }
        function nakletsoru() {
            if (confirm('Deftere Naklet/Yazdırmak istiyor musunuz ?')) {
                return true;
            }
            else {
                alert("Nakil İşlemi İptal Edildi !");
                return false;
            }

        }
        function earsivsoru() {
            if (confirm('E-Arşive göndermek istiyor musunuz ?')) {
                return true;
            }
            else {
                alert("E-Arşiv İşlemi İptal Edildi !");
                return false;
            }

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table cellspacing="10">
                <tr>
                    <td class="style2">
                        <asp:Label ID="lblFtTuru1" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlft1" CssClass="textbox" DataValueField="LineNo" DataTextField="Adi" 
                            runat="server" Width="300px" AutoPostBack="True" DataSourceID="DSFt"
                            OnSelectedIndexChanged="ddlft1_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        <asp:Label ID="lblFtTuru2" runat="server"></asp:Label>
                        &nbsp;
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlft2" CssClass="textbox" runat="server" Width="300px" AutoPostBack="True"
                            OnSelectedIndexChanged="ddlft2_SelectedIndexChanged">
                            <asp:ListItem></asp:ListItem>
                            <asp:ListItem Value="1" Selected="True">1 - NORMAL</asp:ListItem>
                            <asp:ListItem Value="3">3 - BEDELSİZ</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        <asp:Label ID="lblTopFtMetni1" runat="server"></asp:Label>
                        &nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtftmetni1" CssClass="textbox" runat="server" Enabled="False"
                            MaxLength="80" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        <asp:Label ID="lblTopFtMetni2" runat="server"></asp:Label>
                        &nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtftmetni2" CssClass="textbox" runat="server" Enabled="False"
                            MaxLength="60" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        <asp:Label ID="lblTopFtMetni3" runat="server"></asp:Label>
                        &nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtftmetni3" CssClass="textbox" runat="server" Enabled="False"
                            MaxLength="60" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        <asp:Label ID="lblAciklama1" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtgrup1a" CssClass="textbox" runat="server"
                            MaxLength="80" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        <asp:Label ID="lblAciklama2" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtgrup2a" CssClass="textbox" runat="server"
                            MaxLength="80" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        <asp:Label ID="lblAciklama3" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtgrup3a" CssClass="textbox" runat="server"
                            MaxLength="80" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        <asp:Label ID="lblFaturaEkMetinB" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtftekmetin" CssClass="textbox" runat="server" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        <asp:Label ID="lblFaturaEkMetin2B" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtftekmetin2" CssClass="textbox" runat="server" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        <asp:Label ID="lblFtTarihiB" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtfaturatarihi" CssClass="textbox" runat="server" Style="text-align: justify"
                            ValidationGroup="MKE" Width="300px" Enabled="False" />
                        <cc1:CalendarExtender ID="txtfaturatarihi_CalendarExtender" runat="server" TargetControlID="txtfaturatarihi"
                            Format="dd.MM.yyyy" />
                        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptLocalization="true"
                            EnableScriptGlobalization="true" EnablePageMethods="true">
                        </asp:ScriptManager>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        <asp:Label ID="lblticari" runat="server" Text="Ticari Ünvan"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtticariunvan" CssClass="textbox" runat="server" MaxLength="100"
                            Width="300px"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <br />
            <table cellspacing="10">
                <tr>
                    <td class="style3">
                        <asp:Label ID="lblSeriNoB" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtSeriNo" CssClass="textbox" runat="server" MaxLength="10"
                            Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style3">
                        <asp:Label ID="lblHaBeNoB" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txthbno" CssClass="textbox" runat="server" MaxLength="20"
                            Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style3">
                        <asp:Label ID="lblLimanB" runat="server"></asp:Label>
                        &nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtliman" CssClass="textbox" runat="server" MaxLength="50"
                            Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style3">
                        <asp:Label ID="lblTanimB" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txttanim" CssClass="textbox" runat="server" MaxLength="50"
                            Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style3">
                        <asp:Label ID="lblTasiyiciB" runat="server"></asp:Label>
                        &nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtseferno" CssClass="textbox" runat="server" MaxLength="30"
                            Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style3">
                        <asp:Label ID="lblKonsB" runat="server"></asp:Label>
                        &nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtawb" CssClass="textbox" runat="server" MaxLength="30"
                            Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style3">
                        <asp:Label ID="lblKapB" runat="server"></asp:Label>
                        &nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtadet" CssClass="textbox" runat="server" MaxLength="20"
                            Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style3">
                        <asp:Label ID="lblKargoB" runat="server"></asp:Label>
                        &nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtagırlık" CssClass="textbox" runat="server" MaxLength="20"
                            Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style3">
                        <asp:Label ID="lblBirimB" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtbirim" CssClass="textbox" runat="server" MaxLength="20"
                            Width="300px"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <div id="altmenuyazdir">
                <asp:Button ID="btnönizleme" runat="server" CssClass="myButton" ViewStateMode="Enabled"
                    OnClick="btnönizleme_Click" />
                <asp:Button ID="btnyazdır" runat="server" CssClass="myButton"
                    OnClick="btnyazdır_Click" ViewStateMode="Enabled"
                    OnClientClick="javascript: return nakletsoru()" />
                <asp:Button ID="btnearsiv" runat="server" CssClass="myButton" Visible="false"
                    OnClick="btnearsiv_Click" Text="E-Arşive Gönder" ViewStateMode="Enabled"
                    OnClientClick="javascript: return earsivsoru()" />
                 <asp:Button ID="btnearsivonizleme" runat="server" CssClass="myButton" Visible="false" ViewStateMode="Enabled"
                    OnClick="btnearsivonizleme_Click" />
                <asp:Button ID="btneFaturaYazdir" runat="server" CssClass="myButton"
                    ViewStateMode="Enabled" OnClick="btneFaturaYazdir_Click" />
            </div>
        </div>
         <asp:SqlDataSource ID="DSFt" runat="server" ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    </form>
</body>
</html>
