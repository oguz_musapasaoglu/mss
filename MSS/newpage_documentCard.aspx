﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" EnableViewState="true" CodeBehind="newpage_documentCard.aspx.cs" Inherits="MSS1.newpage_documentCard" %>

<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Src="~/Controls/DataTableControlGAC.ascx" TagName="StDataTable" TagPrefix="STDT" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .TD {
            padding: 0px;
            color: black;
            font-weight: 400;
            font-style: normal;
            text-decoration: none;
            font-family: Calibri, sans-serif;
            text-align: center;
            vertical-align: bottom;
            border: none;
        }
    </style>
    <script type="text/javascript">
        function ConfirmFileChange() {
            var s = document.getElementById('<%=FileUpload1.ClientID %>');
            var mv = document.getElementById('<%=Hiddenmsg04.ClientID %>');
            if (s.value != '') {
                if (confirm(mv.value) == false) {
                    s.value = '';
                    return false;
                }
                else
                    return true;
            }
            else
                return true;
        }

        function ConfirmFormDelete() {
            if (confirm('Silmek istediğinize emin misiniz?'))
                return true;
            else
                return false;
        }
        function BindCommentGrid() {
            grid.PerformCallback('A');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:SqlDataSource ID="DSFirma" runat="server" SelectCommand="select [LineNo] Code, Adi from [0C_50055_00_LOOKUPS] where COMPANY=@Sirket  AND Tur='Izin Talep Company' order by [LineNo]"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>">
         <SelectParameters>
            <asp:SessionParameter Name="Sirket" SessionField="Sirket" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="DSDocumentGroup" runat="server" SelectCommand="select [LineNo] Code, Adi from [0C_50055_00_LOOKUPS] where COMPANY=@Sirket  AND Tur='Dokuman Grubu' order by [LineNo]"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>">
         <SelectParameters>
            <asp:SessionParameter Name="Sirket" SessionField="Sirket" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="DSDocumentType" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="DSDocumentStatus" runat="server" SelectCommand="select [LineNo] Code, Adi from [0C_50055_00_LOOKUPS] where COMPANY=@Sirket  AND Tur='Dokuman Takip Durum' order by [LineNo]"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>">
         <SelectParameters>
            <asp:SessionParameter Name="Sirket" SessionField="Sirket" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="DSComments" runat="server" SelectCommand="select t1.No_,t1.[Line No_] LnNo,t1.Date Tarih,t1.Comment,t1.[Olusturan Kullanıcı] Kullanici 
        from [0D_00097_00_COMMENT_LINE] t1 where COMPANY=@Sirket  AND t1.No_=@EntryNo"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="-1" Name="EntryNo" Type="String" />
             <asp:SessionParameter Name="Sirket" SessionField="Sirket" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds1" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds2" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds3" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds4" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds5" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds6" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds7" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds8" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds9" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds10" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds11" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds12" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds13" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds14" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds15" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds16" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds17" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds18" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds19" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds20" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds21" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds22" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds23" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds24" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="Ds25" runat="server"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <h2>
        <asp:Label ID="lblsayfaadi" Text="DOKÜMAN GİRİŞ EKRANI" CssClass="SayfaBaslik" runat="server"></asp:Label></h2>
    <table style="width: 860px">
        <tr>
            <td style="vertical-align: top; width: 350px">
                <table style="width: 350px">
                    <tr>
                        <td class="captionStyle">
                            <asp:Label ID="lblCompany" runat="server" Text="ŞİRKET*" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td colspan="2">
                            <dx:ASPxComboBox ID="cmbCompany" DataSourceID="DSFirma" TextField="Adi" ValueField="Code" Theme="Glass" ClientInstanceName="cmbCompany" runat="server">
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="captionStyle">
                            <asp:Label ID="lblDocumentGroup" runat="server" Text="DOKÜMAN GRUBU*" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td colspan="2">
                            <dx:ASPxComboBox ID="cmbDocumentGroup" DataSourceID="DSDocumentGroup" TextField="Adi" ValueField="Code" Theme="Glass" ClientInstanceName="cmbDocumentGroup" runat="server">
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="captionStyle">
                            <asp:Label ID="lblDurum" runat="server" Text="DURUM" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td colspan="2">
                            <dx:ASPxComboBox ID="cmbStatus" DataSourceID="DSDocumentStatus" TextField="Adi" ValueField="Code" Theme="Glass" ClientInstanceName="cmbDocumenType" runat="server">
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="captionStyle">
                            <asp:Label ID="lblDocumentType" runat="server" Text="DOKÜMAN TİPİ*" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td colspan="2">
                            <dx:ASPxComboBox ID="cmbDocumenType" DataSourceID="DSDocumentType" TextField="Adi" ValueField="Code" Theme="Glass" ClientInstanceName="cmbDocumenType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cmbDocumenType_SelectedIndexChanged">
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr1" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label1" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td1" colspan="2">
                            <dx:ASPxTextBox ID="Tex1" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar1" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb1" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk1" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr2" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label2" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td2" colspan="2">
                            <dx:ASPxTextBox ID="Tex2" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar2" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb2" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk2" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr3" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label3" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td3" colspan="2">
                            <dx:ASPxTextBox ID="Tex3" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar3" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb3" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk3" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr4" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label4" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td4" colspan="2">
                            <dx:ASPxTextBox ID="Tex4" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar4" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb4" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk4" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr5" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label5" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td5" colspan="2">
                            <dx:ASPxTextBox ID="Tex5" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar5" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb5" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk5" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr6" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label6" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td6" colspan="2">
                            <dx:ASPxTextBox ID="Tex6" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar6" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb6" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk6" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr7" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label7" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td7" colspan="2">
                            <dx:ASPxTextBox ID="Tex7" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar7" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb7" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk7" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr8" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label8" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td8" colspan="2">
                            <dx:ASPxTextBox ID="Tex8" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar8" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb8" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk8" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr9" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label9" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td9" colspan="2">
                            <dx:ASPxTextBox ID="Tex9" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar9" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb9" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk9" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr10" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label10" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td10" colspan="2">
                            <dx:ASPxTextBox ID="Tex10" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar10" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb10" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk10" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr11" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label11" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td11" colspan="2">
                            <dx:ASPxTextBox ID="Tex11" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar11" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb11" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk11" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr12" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label12" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td12" colspan="2">
                            <dx:ASPxTextBox ID="Tex12" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar12" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb12" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk12" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr13" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label13" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td13" colspan="2">
                            <dx:ASPxTextBox ID="Tex13" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar13" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb13" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk13" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr14" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label14" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td14" colspan="2">
                            <dx:ASPxTextBox ID="Tex14" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar14" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb14" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk14" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr15" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label15" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td15" colspan="2">
                            <dx:ASPxTextBox ID="Tex15" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar15" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb15" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk15" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr16" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label16" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td16" colspan="2">
                            <dx:ASPxTextBox ID="Tex16" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar16" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb16" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk16" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr17" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label17" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td17" colspan="2">
                            <dx:ASPxTextBox ID="Tex17" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar17" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb17" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk17" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr18" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label18" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td18" colspan="2">
                            <dx:ASPxTextBox ID="Tex18" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar18" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb18" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk18" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr19" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label19" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td19" colspan="2">
                            <dx:ASPxTextBox ID="Tex19" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar19" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb19" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk19" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr20" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label20" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td20" colspan="2">
                            <dx:ASPxTextBox ID="Tex20" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar20" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb20" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk20" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr21" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label21" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td21" colspan="2">
                            <dx:ASPxTextBox ID="Tex21" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar21" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb21" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk21" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr22" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label22" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td22" colspan="2">
                            <dx:ASPxTextBox ID="Tex22" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar22" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb22" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk22" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr23" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label23" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td23" colspan="2">
                            <dx:ASPxTextBox ID="Tex23" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar23" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb23" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk23" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr24" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label24" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td24" colspan="2">
                            <dx:ASPxTextBox ID="Tex24" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar24" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb24" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk24" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr25" visible="false">
                        <td class="captionStyle">
                            <asp:Label ID="Label25" runat="server" Text="" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 10px"></td>
                        <td runat="server" id="td25" colspan="2">
                            <dx:ASPxTextBox ID="Tex25" runat="server" Theme="Glass"></dx:ASPxTextBox>
                            <dx:ASPxDateEdit ID="Tar25" runat="server" Theme="Glass"></dx:ASPxDateEdit>
                            <dx:ASPxComboBox ID="Cmb25" runat="server" Theme="Glass"></dx:ASPxComboBox>
                            <dx:ASPxCheckBox ID="Chk25" runat="server" Theme="Glass"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:FileUpload ID="FileUpload1" Width="94%" CssClass="textbox" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button runat="server" CssClass="formglobalbuttonStyle" OnClick="btnDosyaBagla_Click" Text="DOSYA BAĞLA VE KAYDET" ID="btnDosyaBagla" OnClientClick="return ConfirmFileChange()" /></td>
                                    <td>
                                        <asp:Button runat="server" CssClass="formglobalbuttonStyle" OnClick="btnDelete_Click" OnClientClick="return ConfirmFormDelete()" Text="SİL" ID="btnDelete" /></td>
                                </tr>
                            </table>


                        </td>
                    </tr>
                </table>
            </td>
            <td style="vertical-align: top; width: 10px">&nbsp;</td>
            <td style="vertical-align: top; width: 500px">
                <table style="width: 100%">
                    <tr>
                        <td style="width: 100%; vertical-align: top" colspan="3">
                            <dx:ASPxMemo ID="txtComment" runat="server" Width="100%" Height="50px" Style="padding: 0px 3px 0px 3px;" Theme="Glass" MaxLength="1000"></dx:ASPxMemo>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%">
                            <asp:Button runat="server" CssClass="formglobalbuttonStyle" OnClick="btnAddComment_Click" Text="YORUM GİR" ID="btnAddComment" /></td>
                        <td style="width: 15%">
                            <asp:Button runat="server" CssClass="formglobalbuttonStyle" Text="YORUM GÖR" ID="btnShowComments" /></td>
                        <td style="width: 70%">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False" Width="100%" Theme="Glass"
                                KeyFieldName="LineNo" DataSourceID="DTComments" EnableRowsCache="false" OnCustomCallback="grid_CustomCallback">
                                <Settings ShowTitlePanel="true" ShowFilterRow="true" />
                                <SettingsBehavior ColumnResizeMode="Control" />
                                <SettingsPager Mode="ShowPager" PageSize="20"></SettingsPager>
                                <SettingsText EmptyDataRow="#" />

                                <Border BorderWidth="0px" />
                                <SettingsCommandButton>
                                    <ClearFilterButton Image-ToolTip="Clear Filter" Image-Url="~/images/clear_filter.png"></ClearFilterButton>
                                </SettingsCommandButton>
                                <Styles>
                                    <Header ForeColor="White" CssClass="grid1CaptionStyle"></Header>
                                    <HeaderPanel ForeColor="White" CssClass="grid1CaptionStyle"></HeaderPanel>
                                </Styles>
                                <Columns>
                                    <dx:GridViewCommandColumn ButtonType="Image" Width="40px" ShowClearFilterButton="true" VisibleIndex="0">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataColumn Visible="false" FieldName="LineNo" VisibleIndex="1">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataMemoColumn Caption="YORUM" FieldName="Comment" Width="50%" VisibleIndex="2">
                                        <Settings AutoFilterCondition="Contains" />
                                    </dx:GridViewDataMemoColumn>
                                    <dx:GridViewDataDateColumn Width="80px" Caption="OLUŞTURMA TARİHİ" FieldName="Tarih" VisibleIndex="3">
                                        <HeaderCaptionTemplate>
                                            OLUŞTURMA<br />
                                            TARİHİ
                                        </HeaderCaptionTemplate>
                                        <PropertiesDateEdit DisplayFormatString="dd.MM.yyyy" DateOnError="Null" MinDate="01.01.2000"></PropertiesDateEdit>
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataDateColumn Width="80px" Caption="OLUŞTURMA SAATİ" FieldName="Saat" VisibleIndex="4">
                                        <HeaderCaptionTemplate>
                                            OLUŞTURMA<br />
                                            SAATİ
                                        </HeaderCaptionTemplate>
                                        <PropertiesDateEdit DisplayFormatString="HH:mm" DateOnError="Null"></PropertiesDateEdit>
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataColumn Caption="OLUŞTURAN KULLANICI" Width="100px" FieldName="Kullanici" VisibleIndex="5">
                                        <HeaderCaptionTemplate>
                                            OLUŞTURAN<br />
                                            KULLANICI
                                        </HeaderCaptionTemplate>
                                    </dx:GridViewDataColumn>
                                </Columns>
                            </dx:ASPxGridView>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>



    <div style="display: none">
        <STDT:StDataTable ID="STDT1" runat="server" />
        <STDT:StDataTable ID="DTComments" runat="server" />
        <asp:Label ID="lblno" runat="server"></asp:Label>
        <asp:HiddenField ID="Hiddenmsg04" runat="server" />
    </div>
</asp:Content>

