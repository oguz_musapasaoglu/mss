﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.IO;
using System.Data.OleDb;
using System.Data;
using System.Data.Odbc;
using DevExpress.Spreadsheet;
using DevExpress.Web.Office;
using MSS1.WSGeneric;
using System.ServiceModel;
using DevExpress.Spreadsheet.Export;
using System.Data.SqlClient;
using System.Globalization;
using MSS1.Codes;

namespace MSS1
{
    public partial class OdemePlaniAktarim :  Bases
    {
        Language l = new Language();
        public ws_baglantı ws = new ws_baglantı();
        fonksiyonlar f = new fonksiyonlar();
        Methods method = new Methods();
        DataTable dt;
        SessionBase _obj;

        string FilePath
        {
            get { return Session["ExcelFilePath"] == null ? String.Empty : Session["ExcelFilePath"].ToString(); }
            set { Session["ExcelFilePath"] = value; }
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPostBack || IsCallback) return;
            FilePath = String.Empty;
            SetSessionValues();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            DilGetir();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }

        private void DilGetir()
        {
            Language l = new Language();

            Button1.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "OdemePlaniAktarim", "Button1");
            lblFileSuccess.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "OdemePlaniAktarim", "lblFileSuccess");
            lblSuccess.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "OdemePlaniAktarim", "lblSuccess");
            lblUnSuccess.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "OdemePlaniAktarim", "lblUnSuccess");
            lblPleaseSelectFile.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "OdemePlaniAktarim", "lblPleaseSelectFile");

        }

        private string currentBankNo
        {
            get
            {
                return Request.QueryString["BankaNo"].ToString();
            }
        }

        protected void Upload_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
        {

            if (System.IO.File.Exists(Server.MapPath("~/Dosya/Ödeme Planı Aktarım/") + e.UploadedFile.FileName))
                System.IO.File.Delete(Server.MapPath("~/Dosya/Ödeme Planı Aktarım/") + e.UploadedFile.FileName);


            String uploadedFilePath = Server.MapPath("~/Dosya/Ödeme Planı Aktarım/") + e.UploadedFile.FileName;
            e.UploadedFile.SaveAs(uploadedFilePath);
            if (!String.IsNullOrEmpty(uploadedFilePath))
                DocumentManager.CloseDocument(uploadedFilePath);
            FilePath = uploadedFilePath;

        }

        protected void Spreadsheet_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(FilePath))
            {
                Spreadsheet.Open(FilePath);
            }
        }

        protected void btnAktar_Click(object sender, EventArgs e)
        {
            try
            {
                Worksheet Ws = Spreadsheet.Document.Worksheets["ODEMEPLANI"];
                Range range = Ws.GetDataRange();
                dt = Ws.CreateDataTable(range, true);
                DataTableExporter exp = Ws.CreateDataTableExporter(range, dt, true);
                exp.Export();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "odemeplaniaktarim", "msg001"));
                return;
            }

            bool kontrol = false;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i][0].ToString() != "")
                {
                    if (dt.Rows[i][0].ToString() == currentBankNo)
                        kontrol = true;
                    else
                    {
                        kontrol = false;
                        break;
                    }
                }

            }
            if (kontrol)
            {
                Import(dt);
                Spreadsheet.Document.Worksheets.ActiveWorksheet.Clear(Spreadsheet.Document.Worksheets.ActiveWorksheet.GetDataRange());
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "odemeplaniaktarim", "msg002"));
            }
            else
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "odemeplaniaktarim", "msg003"));
            }



        }




        private void Import(DataTable dt)
        {
            string[] _res = GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "GetOdemePlanAktarimNo§" + dt.Rows[0][0].ToString(),_obj.Comp).Split('-');
           


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i][0].ToString() != "")
                {
                    string[] _params = {
                                   dt.Rows[i][0].ToString(),
                                   method.DateToStringNAVType(dt.Rows[i][1].ToString()),
                                   method.StringToDecimal(dt.Rows[i][2].ToString()).ToString(),
                                   method.StringToDecimal(dt.Rows[i][3].ToString()).ToString(),
                                   method.StringToDecimal(dt.Rows[i][4].ToString()).ToString(),
                                   _obj.UserName,
                                   method.StringToInt(_res[1].ToString()).ToString(),
                                   _res[0].ToString(),
                                   method.StringToDecimal(dt.Rows[i][7].ToString()).ToString(),
                                   method.StringToDecimal(dt.Rows[i][8].ToString()).ToString(),
                                   method.StringToInt(dt.Rows[i][6].ToString()).ToString()
                    };
                    NavServicesContents serviceContent = new NavServicesContents();
                    string[] _pars = serviceContent.NavServiceParams("OdemePlaniInsert", _params);
                    string faturaNo = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "OdemePlaniInsert§" + string.Join("§", _params), _obj.Comp);
                }

            }

        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }
    }
}