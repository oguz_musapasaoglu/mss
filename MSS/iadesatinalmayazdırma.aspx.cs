﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.ServiceModel;
using MSS1.WSGeneric;
using System.Configuration;
using System.Data.SqlClient;
using MSS1.Codes;

namespace MSS1
{
    public partial class iadesatinalmayazdırma : Bases
    {
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        SqlConnection baglanti;
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;

        SessionBase _obj;
        public ws_baglantı ws = new ws_baglantı();

        #region Dil Getir
        public void dilgetir()
        {
            lblFaturaTuruB.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "iadesatinalmayazdırma", "lblFaturaTuruB");
            lblTopFtMetni1B.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "iadesatinalmayazdırma", "lblTopFtMetni1B");
            lblTopFtMetni2B.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "iadesatinalmayazdırma", "lblTopFtMetni2B");
            lblTopFtMetni3B.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "iadesatinalmayazdırma", "lblTopFtMetni3B");
            lblFtEkMetinB.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "iadesatinalmayazdırma", "lblFtEkMetinB");
            lblFtTarihiB.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "iadesatinalmayazdırma", "lblFtTarihiB");
            lblHariciBNoB.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "iadesatinalmayazdırma", "lblHariciBNoB");
            lblKonsNoB.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "iadesatinalmayazdırma", "lblKonsNoB");
            btnönizleme.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "iadesatinalmayazdırma", "btnönizleme");
            btnyazdır.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "iadesatinalmayazdırma", "btnyazdır");
            lblSeriNoB.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "iadesatinalmayazdırma", "lblSeriNoB");
            btnEFaturaYazdir.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "iadesatinalmayazdırma", "btnEFaturaYazdir");
            btnearsivonizleme.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "iadesatinalmayazdırma", "btnonarsivonizleme");
            btnearsiv.Text= l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "iadesatinalmayazdırma", "btnearsiv");

        }
        #endregion

        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            _obj = TabSessions.ControlState(this.Page, Request.QueryString["GId"].ToString());
            if (!IsPostBack)
            {
                txtfaturatarihi.Text = Request.QueryString["Tarih"];
                efaturakontrol(_obj.Comp.ToString(), Request.QueryString["BelgeNo"].ToString());
            }
            dilgetir();
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }

        }
        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }
        protected void btnyazdır_Click(object sender, EventArgs e)
        {
            string emukellefmi = f.tekbirsonucdonder("SELECT EF_Kaydi FROM [0D_00038_00_PURCHASE HEADER] where COMPANY='" + _obj.Comp.ToString() + "' and No_='" + Request.QueryString["BelgeNo"].ToString() + "'");

            if (emukellefmi == "0")
            {
                if (ddlft1.SelectedValue != string.Empty && txthbno.Text != string.Empty && txtfaturatarihi.Text != string.Empty && txtSeriNo.Text != string.Empty)
                {
                    string[] tcevir = txtfaturatarihi.Text.Split('.');


                    string[] _params = { Request.QueryString["BelgeNo"], txthbno.Text, txtawb.Text, "1", tcevir[1] + "." + tcevir[0] + "." + tcevir[2], Convert.ToInt32(ddlft1.SelectedValue).ToString(), txtftmetni1.Text, txtftmetni2.Text, txtftmetni3.Text, txtSeriNo.Text };
                    GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "IadeSatinSipYazdır§" + string.Join("§", _params), _obj.Comp);

                    ReportDocument crystalReport;

                    string reportPath = f.GetReportPath("INVOICE IADE TUM GRUP.rpt", _obj.Comp);
                    crystalReport = new ReportDocument();
                    crystalReport.Load(reportPath);
                    crystalReport.SetParameterValue("@DOCNO", Request.QueryString["BelgeNo"]);
                    crystalReport.SetParameterValue("@COMP", _obj.Comp.ToString());
                    crystalReport.SetParameterValue("DETAY TOPLU", ddlft1.SelectedValue);
                    crystalReport.SetParameterValue("NORMALPROFORMA", 1);
                    crystalReport.SetParameterValue("TOPLU FATURA METNİ", ddlft1.SelectedValue);
                    crystalReport.SetParameterValue("EK METİN", txtftekmetin.Text);
                    crystalReport.SetParameterValue("ÖN İZLEME", 1);
                    crystalReport.ExportToDisk(ExportFormatType.PortableDocFormat, Server.MapPath(CurrentFolderName(_obj.Comp) + "/PDF/" + Request.QueryString["BelgeNo"] + ".pdf"));
                    ClientScript.RegisterStartupScript(this.Page.GetType(), "popupOpener", "var popup=window.open('" + CurrentFolderName(_obj.Comp) + "/PDF/" + Request.QueryString["BelgeNo"] + ".pdf');popup.focus();", true);

                    if (crystalReport != null)
                    {

                        crystalReport.Close();

                        crystalReport.Dispose();

                        crystalReport = null;

                    }

                    string[] _params2 = { Request.QueryString["BelgeNo"], _obj.NameSurName.ToString() };
                    GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "IadeSatınAlmaDefNaklet§" + string.Join("§", _params2), _obj.Comp);

                    if (!ClientScript.IsStartupScriptRegistered("JSFaturaAcik"))
                    {
                        ClientScript.RegisterStartupScript(GetType(), "JSFaturaAcik", @"window.opener.parent.RefreshAllCollapse();self.close();", true);
                    }
                    //ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: proceed();", true);

                }
                else if (emukellefmi == "1")
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "iadesatinalmayazdırma", "msg162"));
                }
            }
            else if (emukellefmi == "1")
            {
                if (ddlft1.SelectedValue != string.Empty && txtfaturatarihi.Text != string.Empty)
                {
                    string[] tcevir = txtfaturatarihi.Text.Split('.');

                    string[] _params = { Request.QueryString["BelgeNo"], txthbno.Text, txtawb.Text, "1", tcevir[1] + "." + tcevir[0] + "." + tcevir[2], Convert.ToInt32(ddlft1.SelectedValue).ToString(), txtftmetni1.Text, txtftmetni2.Text, txtftmetni3.Text, txtSeriNo.Text };
                    GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "IadeSatinSipYazdır§" + string.Join("§", _params), _obj.Comp);

                    ReportDocument crystalReport;
                    string reportPath = f.GetReportPath("INVOICE IADE TUM GRUP.rpt", _obj.Comp);
                    crystalReport = new ReportDocument();
                    crystalReport.Load(reportPath);
                    crystalReport.SetParameterValue("@DOCNO", Request.QueryString["BelgeNo"]);
                    crystalReport.SetParameterValue("@COMP", _obj.Comp.ToString());
                    crystalReport.SetParameterValue("DETAY TOPLU", ddlft1.SelectedValue);
                    crystalReport.SetParameterValue("NORMALPROFORMA", 1);
                    crystalReport.SetParameterValue("TOPLU FATURA METNİ", ddlft1.SelectedValue);
                    crystalReport.SetParameterValue("EK METİN", txtftekmetin.Text);
                    crystalReport.SetParameterValue("ÖN İZLEME", 2);
                    crystalReport.ExportToDisk(ExportFormatType.PortableDocFormat, Server.MapPath(CurrentFolderName(_obj.Comp) + "/PDF/" + Request.QueryString["BelgeNo"] + ".pdf"));
                    ClientScript.RegisterStartupScript(this.Page.GetType(), "popupOpener", "var popup=window.open('" + CurrentFolderName(_obj.Comp) + "/PDF/" + Request.QueryString["BelgeNo"] + ".pdf');popup.focus();", true);

                    if (crystalReport != null)
                    {

                        crystalReport.Close();

                        crystalReport.Dispose();

                        crystalReport = null;

                    }

                    string[] _params2 = { Request.QueryString["BelgeNo"], _obj.NameSurName.ToString() };
                    GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "IadeSatınAlmaDefNaklet§" + string.Join("§", _params2), _obj.Comp);


                    if (!ClientScript.IsStartupScriptRegistered("JSFaturaAcik"))
                    {
                        ClientScript.RegisterStartupScript(GetType(), "JSFaturaAcik", @"window.opener.parent.RefreshAllCollapse();self.close();", true);
                    }
                    // ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: proceed();", true);

                }
                else
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "iadesatinalmayazdırma", "msg162"));
                }
            }




        }
        protected void btnönizleme_Click(object sender, EventArgs e)
        {
            string emukellefmi = f.tekbirsonucdonder("SELECT EF_Kaydi FROM [0D_00038_00_PURCHASE HEADER] where COMPANY='" + _obj.Comp.ToString() + "' and No_='" + Request.QueryString["BelgeNo"].ToString() + "'");

            if (emukellefmi == "0")
            {
                if (ddlft1.SelectedValue != string.Empty && txtfaturatarihi.Text != string.Empty && txtSeriNo.Text != string.Empty)
                {
                    string[] tcevir = txtfaturatarihi.Text.Split('.');

                    string[] _params = { Request.QueryString["BelgeNo"], txthbno.Text, txtawb.Text, "1", tcevir[1] + "." + tcevir[0] + "." + tcevir[2], Convert.ToInt32(ddlft1.SelectedValue).ToString(), txtftmetni1.Text, txtftmetni2.Text, txtftmetni3.Text, txtSeriNo.Text };
                    GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "IadeSatinSipYazdır§" + string.Join("§", _params), _obj.Comp);

                    this.ClientScript.RegisterClientScriptBlock(this.GetType(), "popupOpener", "var popup=window.open('Raporlar/iadesatinalmarapor.aspx?BelgeNo=" + Request.QueryString["BelgeNo"] + "&FT1=" + ddlft1.SelectedValue + "&FTM=" + txtftmetni1.Text + "&FTEM=" + txtftekmetin.Text + "&GId=" + Request.QueryString["GId"] + "');popup.focus();", true);
                }
                else
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "iadesatinalmayazdırma", "msg163"));
                }
            }
            else if (emukellefmi == "1")
            {
                if (ddlft1.SelectedValue != string.Empty && txtfaturatarihi.Text != string.Empty)
                {
                    string[] tcevir = txtfaturatarihi.Text.Split('.');

                    string[] _params = { Request.QueryString["BelgeNo"], txthbno.Text, txtawb.Text, "1", tcevir[1] + "." + tcevir[0] + "." + tcevir[2], Convert.ToInt32(ddlft1.SelectedValue).ToString(), txtftmetni1.Text, txtftmetni2.Text, txtftmetni3.Text, txtSeriNo.Text };
                    GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "IadeSatinSipYazdır§" + string.Join("§", _params), _obj.Comp);

                    this.ClientScript.RegisterClientScriptBlock(this.GetType(), "popupOpener", "var popup=window.open('Raporlar/iadesatinalmarapor.aspx?BelgeNo=" + Request.QueryString["BelgeNo"] + "&FT1=" + ddlft1.SelectedValue + "&FTM=" + txtftmetni1.Text + "&FTEM=" + txtftekmetin.Text + "&GId=" + Request.QueryString["GId"] + "');popup.focus();", true);
                }
                else
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "iadesatinalmayazdırma", "msg163"));
                }
            }


        }
        protected void ddlft1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlft1.SelectedIndex == 2)
            {
                txtftmetni1.Enabled = true;
                txtftmetni2.Enabled = true;
                txtftmetni3.Enabled = true;
            }
            else
            {
                txtftmetni1.Enabled = false;
                txtftmetni2.Enabled = false;
                txtftmetni3.Enabled = false;
            }
        }
        protected void txtfaturatarihi_TextChanged(object sender, EventArgs e)
        {
            string[] tcevir = txtfaturatarihi.Text.Split('.');
        }

        private void efaturakontrol(string sirket, string sipno)
        {
            string efaturamuk = f.tekbirsonucdonder("select SH.EF_Kaydi from [0D_00038_00_PURCHASE HEADER] SH where COMPANY='" + _obj.Comp.ToString() + "' and  SH.No_='" + sipno + "' and [Document Date] >= '2016-01-01'");

            if (efaturamuk == "0" || efaturamuk == "NULL" || efaturamuk == string.Empty)
            {
                string earsiv = f.tekbirsonucdonder("select [E-Arsiv] from [0C_70043_00_COMP_INFO2] where COMPANY='" + _obj.Comp + "'");
                if (earsiv == "0" || earsiv == "NULL" || earsiv == string.Empty)
                {
                    btnearsiv.Visible = false;
                    btnyazdır.Visible = true;
                    btnEFaturaYazdir.Visible = false;
                }
                else
                {


                    btnearsiv.Visible = true;
                    btnyazdır.Visible = false;
                    btnEFaturaYazdir.Visible = false;
                    btnönizleme.Visible = false;
                    btnearsivonizleme.Visible = true;
                }

            }
            else
            {
                btnyazdır.Visible = false;
                btnEFaturaYazdir.Visible = true;
            }
        }

            protected void btnEFaturaYazdir_Click(object sender, EventArgs e)
            {
                string ftmetni1 = "";
                string ftmetni2 = "";
                string ftmetni3 = "";
                if (ddlft1.SelectedValue == "2")
                {
                    ftmetni1 = txtftmetni1.Text;
                    ftmetni2 = txtftmetni2.Text;
                    ftmetni3 = txtftmetni3.Text;
                }

                string[] _params = { Request.QueryString["BelgeNo"].ToString(), "", "", "", txtawb.Text, "", "", "", _obj.NameSurName.ToString(), txtftekmetin.Text, "",
                 Convert.ToInt32(ddlft1.SelectedValue).ToString(), txtftmetni1.Text, txtftmetni2.Text, txtftmetni2.Text};
                string message = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "EF_GidenBaslikIade§" + string.Join("§", _params), _obj.Comp);


                //MessageBox(message);

                if (!ClientScript.IsStartupScriptRegistered("JSFaturaAcik"))
                {
                    ClientScript.RegisterStartupScript(GetType(), "JSFaturaAcik", @"window.opener.parent.RefreshAllCollapse();alert('" + message.Replace("'", "") + "');self.close();", true);
                }
            }
            protected void txtftmetni1_TextChanged(object sender, EventArgs e)
            {

            }

            protected void btnearsiv_Click(object sender, EventArgs e)
            {
                string ftmetni1 = "";
                string ftmetni2 = "";
                string ftmetni3 = "";
                if (ddlft1.SelectedValue == "2")
                {
                    ftmetni1 = txtftmetni1.Text;
                    ftmetni2 = txtftmetni2.Text;
                    ftmetni3 = txtftmetni3.Text;
                }

                string[] _params = { Request.QueryString["BelgeNo"].ToString(),"","","",txtawb.Text,"","","",
                                _obj.NameSurName, txtftekmetin.Text,"", ddlft1.SelectedValue ,
                                ftmetni1, ftmetni2, ftmetni3 };

                string message = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "EA_GidenIadeBaslik§" + string.Join("§", _params), _obj.Comp);



                MessageBox(message);
            }

            protected void btnearsivonizleme_Click(object sender, EventArgs e)
            {

                string[] tcevir = txtfaturatarihi.Text.Split('.');

                string[] _params = { Request.QueryString["BelgeNo"], txthbno.Text, txtawb.Text, tcevir[1] + "." + tcevir[0] + "." + tcevir[2],
                        ddlft1.SelectedValue, txtftmetni1.Text, txtftmetni2.Text, txtftmetni3.Text,txtSeriNo.Text };
                GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatisSipYazdır§" + string.Join("§", _params), _obj.Comp);
                this.ClientScript.RegisterClientScriptBlock(this.GetType(), "popupOpener", "var popup=window.open('Raporlar/Default.aspx?BelgeNo=" +
                    Request.QueryString["BelgeNo"] + "&FT1=" + ddlft1.SelectedValue + "&FTM=" + txtftmetni1.Text + "&FTEM=" + txtftekmetin.Text + "&GId=" + Request.QueryString["GId"] + "');popup.focus();", true);
            }
        }
    }