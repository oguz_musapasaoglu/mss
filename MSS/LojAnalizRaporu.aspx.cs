﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.Codes;

namespace MSS1
{
    public partial class LojAnalizRaporu : System.Web.UI.Page
    {
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        SqlConnection baglanti;
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        public ws_baglantı ws = new ws_baglantı();
        static SessionBase _obj;
        private void DilGetir()
        {
            lblsayfaadi.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "lojAylikraporParameter", "lblsayfaadi");
            lblRaporTuruYeni.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "lojAylikraporParameter", "lblRaporTuru");
            lblSirket.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "lojAylikraporParameter", "lblSirket");
            lblYil.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "lojAylikraporParameter", "lblYil");
            lblAgent.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "lojAylikraporParameter", "lblAgent");
            chOpDept.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "lojAylikraporParameter", "chOpDept");
            chOpBolge.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "lojAylikraporParameter", "chOpBolge");
            chMtDept.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "lojAylikraporParameter", "chMtDept");
            chMT.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "lojAylikraporParameter", "chMT");
            chMusteri.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "lojAylikraporParameter", "chMusteri");
            chCarrier.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "lojAylikraporParameter", "chCarrier");
            chPolPod.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "lojAylikraporParameter", "chPolPod");
            lblLiman.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "lojAylikraporParameter", "lblLiman");
            lblUlke.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "lojAylikraporParameter", "lblUlke");
            lblBolge.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "lojAylikraporParameter", "lblBolge");
            btnRaporOlustur.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "lojAylikraporParameter", "btnRaporOlustur");
            lblRaporGrup.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "lojAylikraporParameter", "lblRaporGrup");
            lblTop10Konu.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "lojAylikraporParameter", "lblTop10Konu");
            lblTop10Grup.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "lojAylikraporParameter", "lblTop10Grup");
            lblOpType.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "lojAylikraporParameter", "lblOpType");
            lbljobCusttype.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "lojAylikraporParameter", "lbljobCusttype");
            chIncome.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "lojAylikraporParameter", "chIncome");
            chExpense.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "lojAylikraporParameter", "chExpense");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DilGetir();
            Visibility();
            AutoCompleteExtender2.ContextKey = "Operation";
            AutoCompleteExtender3.ContextKey = "MT";
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }

        private void Visibility()
        {
            txtOpDept.Enabled = chOpDept.Checked;
            txtOpBolge.Enabled = chOpBolge.Checked;
            txtMtDept.Enabled = chMtDept.Checked;
            txtMT.Enabled = chMT.Checked;
            txtMusteri.Enabled = chMusteri.Checked;
            txtCarrier.Enabled = chCarrier.Checked;
            txtPOLLimanListe.Enabled = chPolPod.Checked;
            txtPODLimanListe.Enabled = chPolPod.Checked;
            txtPOLUlkeListe.Enabled = chPolPod.Checked;
            txtPODUlkeListe.Enabled = chPolPod.Checked;
            txtPOLBolgeListe.Enabled = chPolPod.Checked;
            txtPODBolgeListe.Enabled = chPolPod.Checked;
            txtAgentListe.Enabled = lblAgent.Checked;
        }

        #region Kodu Getir
        protected void txtOpDept_TextChanged(object sender, EventArgs e)
        {
            if (txtOpDept.Text.Replace("*", "") != "")
            {
                string[] arr = txtOpDept.Text.Split('|');
                txtOpDept.Text = arr[0].ToString();
                ltrOpDept.Text = arr[1].ToString();
                AutoCompleteExtender11.ContextKey = txtOpDept.Text;
                AutoCompleteExtender12.ContextKey = txtOpDept.Text;
            }
            else if (txtOpDept.Text == "*")
            {
                txtOpDept.Text = "";
            }
        }

        protected void txtOpBolge_TextChanged(object sender, EventArgs e)
        {
            if (txtOpBolge.Text.Replace("*", "") != "")
            {
                string[] arr = txtOpBolge.Text.Split('|');
                txtOpBolge.Text = arr[0].ToString();
                ltrOpBolge.Text = arr[1].ToString();
            }
            else if (txtOpBolge.Text == "*")
            {
                txtOpBolge.Text = "";
            }
        }

        protected void txtMtDept_TextChanged(object sender, EventArgs e)
        {
            if (txtMtDept.Text.Replace("*", "") != "")
            {
                string[] arr = txtMtDept.Text.Split('|');
                txtMtDept.Text = arr[0].ToString();
                ltrMtDept.Text = arr[1].ToString();

            }
            else if (txtMtDept.Text == "*")
            {
                txtMtDept.Text = "";
            }
        }

        protected void txtMT_TextChanged(object sender, EventArgs e)
        {
            if (txtMT.Text.Replace("*", "") != "")
            {
                string[] arr = txtMT.Text.Split('|');
                txtMT.Text = arr[0].ToString();
                ltrMT.Text = arr[1].ToString();
            }
            else if (txtMT.Text == "*")
            {
                txtMT.Text = "";
            }
        }

        protected void txtMusteri_TextChanged(object sender, EventArgs e)
        {
            if (txtMusteri.Text.Replace("*", "") != "")
            {
                string[] arr = txtMusteri.Text.Split('|');
                txtMusteri.Text = arr[0].ToString();
                ltrMusteri.Text = arr[1].ToString();
            }
            else if (txtMusteri.Text == "*")
            {
                txtMusteri.Text = "";
            }
        }

        protected void txtCarrier_TextChanged(object sender, EventArgs e)
        {
            if (txtCarrier.Text.Replace("*", "") != "")
            {
                string[] arr = txtCarrier.Text.Split('|');
                txtCarrier.Text = arr[0].ToString();
                ltrCarrier.Text = arr[1].ToString();
            }
            else if (txtCarrier.Text == "*")
            {
                txtCarrier.Text = "";
            }
        }

        protected void txtPOLUlkeListe_TextChanged(object sender, EventArgs e)
        {
            if (txtPOLUlkeListe.Text.Replace("*", "") != "")
            {
                string[] arr = txtPOLUlkeListe.Text.Split('|');
                txtPOLUlkeListe.Text = arr[0].ToString();
                ltrPOLUlkeListe.Text = arr[1].ToString();
            }
            else if (txtPOLUlkeListe.Text == "*")
            {
                txtPOLUlkeListe.Text = "";
            }
        }

        protected void txtPODUlkeListe_TextChanged(object sender, EventArgs e)
        {
            if (txtPODUlkeListe.Text.Replace("*", "") != "")
            {
                string[] arr = txtPODUlkeListe.Text.Split('|');
                txtPODUlkeListe.Text = arr[0].ToString();
                ltrPODUlkeListe.Text = arr[1].ToString();
            }
            else if (txtPODUlkeListe.Text == "*")
            {
                txtPODUlkeListe.Text = "";
            }
        }

        protected void txtPOLBolgeListe_TextChanged(object sender, EventArgs e)
        {
            if (txtPOLBolgeListe.Text.Replace("*", "") != "")
            {
                string[] arr = txtPOLBolgeListe.Text.Split('|');
                txtPOLBolgeListe.Text = arr[0].ToString();
                ltrPOLBolgeListe.Text = arr[1].ToString();
            }
            else if (txtPOLBolgeListe.Text == "*")
            {
                txtPOLBolgeListe.Text = "";
            }
        }

        protected void txtPODBolgeListe_TextChanged(object sender, EventArgs e)
        {
            if (txtPODBolgeListe.Text.Replace("*", "") != "")
            {
                string[] arr = txtPODBolgeListe.Text.Split('|');
                txtPODBolgeListe.Text = arr[0].ToString();
                ltrPODBolgeListe.Text = arr[1].ToString();
            }
            else if (txtPODBolgeListe.Text == "*")
            {
                txtPODBolgeListe.Text = "";
            }
        }
        protected void txtPOLLimanListe_TextChanged(object sender, EventArgs e)
        {
            if (txtPOLLimanListe.Text.Replace("*", "") != "")
            {
                string[] arr = txtPOLLimanListe.Text.Split('|');
                txtPOLLimanListe.Text = arr[0].ToString();
                ltrPOLLimanListe.Text = arr[1].ToString();
            }
            else if (txtPOLLimanListe.Text == "*")
            {
                txtPOLLimanListe.Text = "";
            }
        }

        protected void txtPODLimanListe_TextChanged(object sender, EventArgs e)
        {
            if (txtPODLimanListe.Text.Replace("*", "") != "")
            {
                string[] arr = txtPODLimanListe.Text.Split('|');
                txtPODLimanListe.Text = arr[0].ToString();
                ltrPODLimanListe.Text = arr[1].ToString();
            }
            else if (txtPODLimanListe.Text == "*")
            {
                txtPODLimanListe.Text = "";
            }
        }
        protected void txtAgentListe_TextChanged(object sender, EventArgs e)
        {
            if (txtAgentListe.Text.Replace("*", "") != "")
            {
                string[] arr = txtAgentListe.Text.Split('|');
                txtAgentListe.Text = arr[0].ToString();
                ltrAgentListe.Text = arr[1].ToString();
            }
            else if (txtAgentListe.Text == "*")
            {
                txtAgentListe.Text = "";
            }
        }



        #endregion
        #region Services

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchDept(string prefixText, int count, string contextKey)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    string query = "Select Name+'|'+Code Show from [0C_00349_99_DIMENSION VALUES] where COMPANY='" + _obj.Comp + "' and [Dimension Code]= 'DEPARTMAN' and Blocked = 0 and Totaling=''";
                    if (contextKey == "Operation")
                    {
                        query += " and [Loj_ Op_  Main Type]=1";
                    }
                    else if (contextKey == "MT")
                    {
                        query += " and  [satis dept] =1";
                    }

                    if (prefixText != "*")
                    {
                        query += " and UPPER(Name) like '" + prefixText.ToUpper() + "%'";
                    }
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;

                    cmd.Connection = conn;
                    conn.Open();
                    List<string> Cust = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {

                            Cust.Add(sdr["Show"].ToString());
                        }
                    }
                    conn.Close();
                    return Cust;
                }
            }
        }


        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchBolge(string prefixText, int count)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    string query = "Select Name+'|'+Code Show from [0C_00349_99_DIMENSION VALUES] where COMPANY='" + _obj.Comp + "' and [Dimension Code]= 'BÖLGE' and Blocked = 0 and Totaling=''";
                    if (prefixText != "*")
                    {
                        query += " and UPPER(Name) like '" + prefixText.ToUpper() + "%'";
                    }
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;

                    cmd.Connection = conn;
                    conn.Open();
                    List<string> Cust = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {

                            Cust.Add(sdr["Show"].ToString());
                        }
                    }
                    conn.Close();
                    return Cust;
                }
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchMusteri(string prefixText, int count)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    string query = "Select Top 200 Name+'|'+No_ Show from  [0C_00018_00_CUSTOMER] where COMPANY='" + _obj.Comp + "' and  Blocked=0";
                    if (prefixText != "*")
                    {
                        query += " and UPPER(Name) like '" + prefixText.ToUpper() + "%'";
                    }
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;

                    cmd.Connection = conn;
                    conn.Open();
                    List<string> Cust = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {

                            Cust.Add(sdr["Show"].ToString());
                        }
                    }
                    conn.Close();
                    return Cust;
                }
            }
        }


        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchCarrier(string prefixText, int count)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    string query = "Select [Carrier Name]+'|'+Code Show from [0C_50037_00_CARRIER] where COMPANY='" + _obj.Comp + "' and  Deleted=0";
                    if (prefixText != "*")
                    {
                        query += " and UPPER([Carrier Name]) like '" + prefixText.ToUpper() + "%'";
                    }
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;

                    cmd.Connection = conn;
                    conn.Open();
                    List<string> Cust = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {

                            Cust.Add(sdr["Show"].ToString());
                        }
                    }
                    conn.Close();
                    return Cust;
                }
            }
        }


        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchMT(string prefixText, int count)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    string query = "Select Name+'|'+Code Show from [0C_00013_00_SALESPERSON] where COMPANY='" + _obj.Comp + "' and   [İsten Ayrildi]=0";
                    if (prefixText != "*")
                    {
                        query += " and UPPER(Name) like '" + prefixText.ToUpper() + "%'";
                    }
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;

                    cmd.Connection = conn;
                    conn.Open();
                    List<string> Cust = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {

                            Cust.Add(sdr["Show"].ToString());
                        }
                    }
                    conn.Close();
                    return Cust;
                }
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchUlke(string prefixText, int count)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    string query = "Select Aciklama +'|'+Kod Show from [0C_90773_01_FAMILY_COUNTRIES] where COMPANY='" + _obj.Comp + "' and Tür=0 and Deleted=0";
                    if (prefixText != "*")
                    {
                        query += " and UPPER(Aciklama) like '" + prefixText.ToUpper() + "%'";
                    }
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;

                    cmd.Connection = conn;
                    conn.Open();
                    List<string> Cust = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {

                            Cust.Add(sdr["Show"].ToString());
                        }
                    }
                    conn.Close();
                    return Cust;
                }
            }
        }



        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchBolgePolPod(string prefixText, int count)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    string query = "Select Aciklama +'|'+Kod Show from [0C_90773_02_FAMILY_REGIONS] where COMPANY='" + _obj.Comp + "' and Tür=1 and Deleted=0";
                    if (prefixText != "*")
                    {
                        query += " and UPPER(Aciklama) like '" + prefixText.ToUpper() + "%'";
                    }
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;

                    cmd.Connection = conn;
                    conn.Open();
                    List<string> Cust = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {

                            Cust.Add(sdr["Show"].ToString());
                        }
                    }
                    conn.Close();
                    return Cust;
                }
            }
        }



        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchLiman(string prefixText, int count, string contextKey)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    string query = "Select TOP 200 Description+'|'+Code Show  from [0C_50020_03_JOB_EK_BILGILER_PORTS] where COMPANY='" + _obj.Comp + "' ";
                    if (prefixText != "*")
                    {
                        query += " and UPPER(Description) like '" + prefixText.ToUpper() + "%'";
                    }
                    if (contextKey == "3300" || contextKey == "3400")
                    {
                        query += " and  Tur = " + contextKey.Replace("3300", "1").Replace("3400", "2");
                    }
                    else
                    {
                        query += " and  Tur = 1";
                    }
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;

                    cmd.Connection = conn;
                    conn.Open();
                    List<string> Cust = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {

                            Cust.Add(sdr["Show"].ToString());
                        }
                    }
                    conn.Close();
                    return Cust;
                }
            }
        }


        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchAgent(string prefixText, int count, string contextKey)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    string query = "SELECT TOP 200 [Name]+'|'+ No_ Show FROM [0C_05050_00_CONTACT]  where Agent=1 ";
                    if (prefixText != "*")
                    {
                        query += " and UPPER(Name) like '" + prefixText.ToUpper() + "%'";
                    }

                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;

                    cmd.Connection = conn;
                    conn.Open();
                    List<string> Cust = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {

                            Cust.Add(sdr["Show"].ToString());
                        }
                    }
                    conn.Close();
                    return Cust;
                }
            }
        }


        #endregion


        protected void btnRaporOlustur_Click(object sender, EventArgs e)
        {
            fonksiyonlar.lojanalizRaporParameters[1] = ddlSirket.SelectedValue.ToString();
            fonksiyonlar.lojanalizRaporParameters[2] = ddlYil.SelectedValue.ToString();
            fonksiyonlar.lojanalizRaporParameters[3] = ddlRaporTuruYeni.SelectedValue.ToString();
            fonksiyonlar.lojanalizRaporParameters[4] = "YEAR TOTAL";
            fonksiyonlar.lojanalizRaporParameters[5] = ltrOpDept.Text;
            fonksiyonlar.lojanalizRaporParameters[6] = ltrOpBolge.Text;
            fonksiyonlar.lojanalizRaporParameters[7] = ltrMtDept.Text;
            fonksiyonlar.lojanalizRaporParameters[8] = ltrMT.Text;
            fonksiyonlar.lojanalizRaporParameters[9] = ltrMusteri.Text;
            fonksiyonlar.lojanalizRaporParameters[10] = ltrCarrier.Text;
            fonksiyonlar.lojanalizRaporParameters[11] = ltrPOLLimanListe.Text;
            fonksiyonlar.lojanalizRaporParameters[12] = ltrPODLimanListe.Text;
            fonksiyonlar.lojanalizRaporParameters[13] = ltrPOLUlkeListe.Text;
            fonksiyonlar.lojanalizRaporParameters[14] = ltrPODUlkeListe.Text;
            fonksiyonlar.lojanalizRaporParameters[15] = ltrPOLBolgeListe.Text;
            fonksiyonlar.lojanalizRaporParameters[16] = ltrPODBolgeListe.Text;
            fonksiyonlar.lojanalizRaporParameters[17] = ltrAgentListe.Text;
            fonksiyonlar.lojanalizRaporParameters[18] = ddlRaporGrup.SelectedValue.ToString();
            fonksiyonlar.lojanalizRaporParameters[19] = ddlTop10Group.SelectedValue.ToString();
            fonksiyonlar.lojanalizRaporParameters[20] = ddlTop10Konu.SelectedValue.ToString();
            fonksiyonlar.lojanalizRaporParameters[21] = ddlOpType.SelectedValue.ToString();
            fonksiyonlar.lojanalizRaporParameters[22] = ddljobCusttype.SelectedValue.ToString();
            fonksiyonlar.lojanalizRaporParameters[23] = Convert.ToInt32(chIncome.Checked).ToString();
            // fonksiyonlar.lojanalizRaporParameters[24] = Convert.ToInt32(chExpense.Checked).ToString();
            fonksiyonlar.lojanalizRaporParameters[24] = Request.QueryString["GId"].ToString();
            fonksiyonlar.lojanalizRaporParameters[25] = ddlDonem.SelectedValue.ToString();
            Response.Redirect("Raporlar/lojAnalizRaporu.aspx?GId=" + Request.QueryString["GId"]);
        }


        protected void chOpDept_CheckedChanged(object sender, EventArgs e)
        {
            if (!chOpDept.Checked)
            {
                txtOpDept.Text = "";
                ltrOpDept.Text = "";
            }
        }

        protected void chOpBolge_CheckedChanged(object sender, EventArgs e)
        {
            if (!chOpBolge.Checked)
            {
                txtOpBolge.Text = "";
                ltrOpBolge.Text = "";
            }
        }

        protected void chMtDept_CheckedChanged(object sender, EventArgs e)
        {
            if (!chMtDept.Checked)
            {
                txtMtDept.Text = "";
                ltrMtDept.Text = "";
            }
        }

        protected void chMT_CheckedChanged(object sender, EventArgs e)
        {
            if (!chMT.Checked)
            {
                txtMT.Text = "";
                ltrMT.Text = "";
            }
        }

        protected void chMusteri_CheckedChanged(object sender, EventArgs e)
        {
            if (!chMusteri.Checked)
            {
                txtMusteri.Text = "";
                ltrMusteri.Text = "";
            }
        }

        protected void chCarrier_CheckedChanged(object sender, EventArgs e)
        {
            if (!chCarrier.Checked)
            {
                txtCarrier.Text = "";
                ltrCarrier.Text = "";
            }
        }

        protected void lblAgent_CheckedChanged(object sender, EventArgs e)
        {
            if (!lblAgent.Checked)
            {
                txtAgentListe.Text = "";
                ltrAgentListe.Text = "";
            }
        }

        protected void chPolPod_CheckedChanged(object sender, EventArgs e)
        {
            if (!chPolPod.Checked)
            {
                txtPODBolgeListe.Text = "";
                txtPOLBolgeListe.Text = "";
                txtPOLLimanListe.Text = "";
                txtPODLimanListe.Text = "";
                txtPOLUlkeListe.Text = "";
                txtPODLimanListe.Text = "";

                ltrPODBolgeListe.Text = "";
                ltrPOLBolgeListe.Text = "";
                ltrPOLLimanListe.Text = "";
                ltrPODLimanListe.Text = "";
                ltrPOLUlkeListe.Text = "";
                ltrPODLimanListe.Text = "";
            }
        }

        protected void ddlYil_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlYil.SelectedItem.ToString() == "2016" | ddlYil.SelectedItem.ToString() == "2017" | ddlYil.SelectedItem.ToString() == "2018")
            {
                ddlDonem.Enabled = true;
            }
            else
            {
                ddlDonem.Enabled = false;
                ddlDonem.SelectedIndex = 0;
            }
        }
    }
}