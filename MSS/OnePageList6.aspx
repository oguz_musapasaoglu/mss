﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="OnePageList6.aspx.cs" EnableViewState="true" Inherits="MSS1.OnePageList6" %>

<%@ Register Src="~/Controls/DataTableControlGAC.ascx" TagName="StDataTable" TagPrefix="STDT" %>

<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        function OnGridFocusedRowChanged(s, e) {
            var link = $("#" + "<%= txtlink.ClientID %>").val();
            var linkfield = "<%= txtlinkField.ClientID %>";
            if (link == "JOB") {
                s.GetRowValues(s.GetFocusedRowIndex(), $("#" + linkfield).val(), OnGetRowValuesJob);
            }
            else {
                s.GetRowValues(s.GetFocusedRowIndex(), $("#" + linkfield).val(), OnGetRowValues);
            }
        }

        function AdGIdToLink(_val) {
            if (_val.includes("GId=")) return _val;
            var ObjGId = "<%= lblGId.ClientID %>";
            if (_val.includes("?"))
                _val = _val + "&GId=" + $("#" + ObjGId).text();
            else
                _val = _val + "?GId=" + $("#" + ObjGId).text();

            return _val;

        }

        function OnGetRowValues(values) {

            var linkFormat = "<%= txtlink.ClientID %>";
            var link = $("#" + linkFormat).val();

            var splitter = values.toString().split(",");

            link = link.replace("XXXXX", splitter[0]);
            link = link.replace("YYYYY", splitter[1]);


            if (splitter[0] == 'PG0066;') {

                window.location.href = AdGIdToLink('./OnePageList6.aspx?OJ1=PG0055;OJ0021&OJ2=PG0066');
                window.focus();
            }
            else if (splitter[0] == 'PG0067;') {
                window.location.href = AdGIdToLink('./OnePageList6.aspx?OJ1=PG0055;OJ0021&OJ2=PG0067');
                window.focus();
            }
            else if (splitter[0] == 'PG0071;OJ0021') {
                window.location.href = AdGIdToLink('./OnePageList6.aspx?OJ1=PG0055;OJ0021&OJ2=PG0071;OJ0021');
                window.focus();
            }
            else {
                if (link.indexOf('null') == -1) {
                    window.location.href = AdGIdToLink(link);
                    window.focus();
                }
            }

        }

        function OnGetRowValuesJob(values) {

            var splitter = values.split(';');
            var targetLink = "";
            if (splitter[1] == "1") {
                window.open("/Jobs/mainJobs.aspx?MJID=" + splitter[0]);
            }
            else {

                window.open("/projeolustur.aspx?Tur=1&ProjeNo=" + splitter[0]);
            }
        }

        function OnRefreshAllObjects(s, e) {
            if (s.cpIsUpdated) {
                try { grid2.PerformCallback(''); } catch (err) { }
            }
        }



    </script>
    <asp:Label ID="lblden" runat="server"></asp:Label>
    <dx:ASPxSplitter Theme="iOS" ID="ASPxSplitter1" runat="server" Height="99%" Width="99%">
        <Panes>
            <dx:SplitterPane ShowCollapseBackwardButton="True" ShowSeparatorImage="True" Size="20%" Name="MenuContainer">
                <ContentCollection>
                    <dx:SplitterContentControl ID="SplitterContentControl1" runat="server">
                        <dx:ASPxMenu ID="menu1" runat="server" AutoSeparators="RootOnly"
                            Theme="Glass"
                            ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                            ShowSubMenuShadow="False" OnItemClick="menu1_ItemClick">
                            <SubMenuStyle GutterWidth="0px" />
                            <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                            <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                            </SubMenuItemStyle>
                            <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                        </dx:ASPxMenu>
                        <dx:ASPxGridView ID="grid1" ClientInstanceName="grid1" Caption="" runat="server" AutoGenerateColumns="False"
                            Theme="Glass" EnableTheming="false" DataSourceID="dtgrid1" OnHtmlRowPrepared="grid_HtmlRowPrepared" KeyFieldName="ID" EnableRowsCache="false"
                            OnCellEditorInitialize="grid1_CellEditorInitialize"
                            OnRowInserting="grid1_RowInserting" OnHtmlDataCellPrepared="grid1_HtmlDataCellPrepared" OnRowUpdating="grid1_RowUpdating"
                            OnRowDeleting="grid1_RowDeleting">
                            <Settings />
                            <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="True" AllowFocusedRow="true" />
                            <SettingsEditing Mode="Inline"></SettingsEditing>
                            <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                            <ClientSideEvents RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e); }" />
                            <SettingsText EmptyDataRow=" " />
                            <SettingsCommandButton>
                                <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                </CancelButton>
                                <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                </UpdateButton>
                            </SettingsCommandButton>
                            <Styles>
                                <Header ForeColor="White"></Header>
                                <HeaderPanel ForeColor="White"></HeaderPanel>
                                <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                            </Styles>
                        </dx:ASPxGridView>
                        <dx:ASPxGridViewExporter ID="gridExport1" ExportedRowType="All" runat="server" GridViewID="grid1"></dx:ASPxGridViewExporter>
                    </dx:SplitterContentControl>
                </ContentCollection>
            </dx:SplitterPane>
            <dx:SplitterPane ShowCollapseForwardButton="false" ShowSeparatorImage="True" Size="80%" Name="ContentContainer">
                <ContentCollection>
                    <dx:SplitterContentControl ID="SplitterContentControl2" runat="server">
                        <asp:Panel runat="server" ID="pnlGrid" Visible="true" BorderWidth="0px" Height="100%">
                            <dx:ASPxMenu ID="menu2" runat="server" AutoSeparators="RootOnly"
                                Theme="Glass"
                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                ShowSubMenuShadow="False" OnItemClick="menu2_ItemClick">
                                <SubMenuStyle GutterWidth="0px" />
                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                </SubMenuItemStyle>
                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                            </dx:ASPxMenu>
                            <dx:ASPxGridView ID="grid2" ClientInstanceName="grid2" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                Theme="Glass" OnCustomCallback="grid2_CustomCallback" OnCellEditorInitialize="grid2_CellEditorInitialize" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                EnableTheming="false" DataSourceID="dtgrid2" Settings-HorizontalScrollBarMode="Auto" KeyFieldName="ID" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnRowValidating="grid_RowValidating" OnAfterPerformCallback="grid_AfterPerformCallback"
                                OnRowInserting="grid2_RowInserting" OnRowUpdating="grid2_RowUpdating" OnRowDeleting="grid2_RowDeleting">
                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'2');} " />
                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" />
                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                                <SettingsText EmptyDataRow=" " />
                                <SettingsCommandButton>
                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                    </CancelButton>
                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                    </UpdateButton>
                                </SettingsCommandButton>
                                <Styles>
                                    <Header ForeColor="White"></Header>
                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                </Styles>
                            </dx:ASPxGridView>
                            <dx:ASPxGridViewExporter ID="gridExport2" ExportedRowType="All" runat="server" GridViewID="grid2"></dx:ASPxGridViewExporter>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlFrame" Visible="false" BorderWidth="0px" Height="100%">
                            <iframe id="iframeView" width="100%" height="100%" runat="server"></iframe>
                        </asp:Panel>
                    </dx:SplitterContentControl>
                </ContentCollection>
            </dx:SplitterPane>
        </Panes>
    </dx:ASPxSplitter>
    <STDT:StDataTable ID="dtgrid1" runat="server" />
    <STDT:StDataTable ID="dtgrid2" runat="server" />
    <STDT:StDataTable ID="DS11" runat="server" />
    <STDT:StDataTable ID="DS12" runat="server" />
    <STDT:StDataTable ID="DS13" runat="server" />
    <STDT:StDataTable ID="DS14" runat="server" />
    <STDT:StDataTable ID="DS15" runat="server" />
    <STDT:StDataTable ID="DS16" runat="server" />
    <STDT:StDataTable ID="DS17" runat="server" />
    <STDT:StDataTable ID="DS18" runat="server" />
    <STDT:StDataTable ID="DS19" runat="server" />
    <STDT:StDataTable ID="DS110" runat="server" />
    <STDT:StDataTable ID="DS111" runat="server" />
    <STDT:StDataTable ID="DS112" runat="server" />
    <STDT:StDataTable ID="DS113" runat="server" />
    <STDT:StDataTable ID="DS114" runat="server" />
    <STDT:StDataTable ID="DS115" runat="server" />
    <STDT:StDataTable ID="DS116" runat="server" />
    <STDT:StDataTable ID="DS117" runat="server" />
    <STDT:StDataTable ID="DS118" runat="server" />
    <STDT:StDataTable ID="DS119" runat="server" />
    <STDT:StDataTable ID="DS120" runat="server" />

    <div style="display: none">
        <asp:HiddenField runat="server" ID="HiddenP1" Value="" />
        <asp:HiddenField runat="server" ID="HiddenP2" Value="" />
        <asp:Label ID="lblGId" runat="server"></asp:Label>
        <asp:TextBox ID="txtlink" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams2" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams2" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams2" runat="server"></asp:TextBox>

        <asp:TextBox ID="txtSubInsertPer2" runat="server" Text=""></asp:TextBox>
    </div>
</asp:Content>


