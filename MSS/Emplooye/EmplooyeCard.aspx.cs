﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.WSGeneric;
using System.ServiceModel;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using MSS1.Codes;

namespace MSS1.Emplooye
{
    public partial class EmplooyeCard : Bases
    {
        fonksiyonlar f = new fonksiyonlar();
        Language l = new Language();
        Methods method = new Methods();
        datasourceHandler dsh = new datasourceHandler();
        webObjectHandler woh = new webObjectHandler();
        string ConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        SessionBase _obj;
        private string currentCardUniqueID
        {
            get
            {
                string _cardId = string.Empty;
                try
                {
                    _cardId = Request.QueryString["Id"].ToString();
                }
                catch (Exception)
                {
                    _cardId = string.Empty;
                }
                return _cardId;
            }
        }


        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }


        ws_baglantı ws = new ws_baglantı();
        void dilgetir()
        {

        }
        private void preparepageFields()
        {


            //dtstatus.Table = dsh.InitComboDataTable("LUCENTSTATUS");
            //createPK(dtstatus.Table);


            //dtUlke.Table = dsh.InitComboDataTable("LUCENTNAT");
            //createPK(dtUlke.Table);

            //dtLucentRank.Table = dsh.InitComboDataTable("LUCENTRANK");
            //createPK(dtLucentRank.Table);

            //cmbstatus.DataBind();
            //cmbUlke.DataBind();
            //cmbLucentRank.DataBind();
            //cmbNationality.DataBind();
            //cmbCoc.DataBind();


        }





        private void createPK(DataTable dt)
        {
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = dt.Columns["ID"];
            dt.PrimaryKey = pricols;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsCallback | IsPostBack) { return; }



            preparepageFields();

            preparepageForms();

            dilgetir();


        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }

        private void preparepageForms()
        {
            if (currentCardUniqueID != string.Empty)
            {
                using (SqlConnection conn = new SqlConnection(ConnString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {

                        conn.Open();
                        cmd.Parameters.Clear();
                        cmd.CommandText = "SP_0_W13100";
                        cmd.Parameters.AddWithValue("@COMP", _obj.Comp);
                        cmd.Parameters.AddWithValue("@USER", _obj.UserName);
                        cmd.Parameters.AddWithValue("@YTKG", DBNull.Value);
                        cmd.Parameters.AddWithValue("@YTK1", DBNull.Value);
                        cmd.Parameters.AddWithValue("@YTK2", DBNull.Value);
                        cmd.Parameters.AddWithValue("@YTK3", DBNull.Value);
                        cmd.Parameters.AddWithValue("@YTK4", DBNull.Value);
                        cmd.Parameters.AddWithValue("@YTK5", DBNull.Value);
                        cmd.Parameters.AddWithValue("@PRM1", currentCardUniqueID);
                        cmd.Parameters.AddWithValue("@PRM2", DBNull.Value);
                        cmd.Parameters.AddWithValue("@PRM3", DBNull.Value);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = conn;
                        using (SqlDataAdapter adapter = new SqlDataAdapter())
                        {
                            adapter.SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                adapter.Fill(dt);
                                if (dt.Rows.Count > 0)
                                {

                                    txtName.Text = dt.Rows[0]["R0013"].ToString();
                                    txtBirthDate.Date = Convert.ToDateTime(dt.Rows[0]["R0024"]);
                                    txtIdentityNo.Text = dt.Rows[0]["R0027"].ToString();
                                    txtSocialSecurityNumber.Text = dt.Rows[0]["R0025"].ToString();
                                    txtEmail.Text = dt.Rows[0]["R0023"].ToString();
                                    txtPhoneNo.Text = dt.Rows[0]["R0022"].ToString();
                                    txtCurrentStatus.Text = dt.Rows[0]["R0029"].ToString();
                                    txtEmployementDate.Date = Convert.ToDateTime(dt.Rows[0]["R0015"]);
                                    txtTerminationDate.Text = dt.Rows[0]["R0017"].ToString();
                                    txtRegion.Text = dt.Rows[0]["R0019"].ToString();
                                    txtDepartment.Text = dt.Rows[0]["R0020"].ToString();

                                    string _Empimageurl = Server.MapPath("~/"+ CurrentFolderName(_obj.Comp) + "/Dosya/EmplooyeImage/" + currentCardUniqueID + ".jpg");

                                    if (File.Exists(_Empimageurl))
                                    {

                                        ImgPhoto.ImageUrl = "../" + CurrentFolderName(_obj.Comp) + "/Dosya/EmplooyeImage/" + currentCardUniqueID + ".jpg";
                                    }
                                    else
                                        ImgPhoto.Visible = false;

                                    try
                                    {
                                        if (!string.IsNullOrEmpty(dt.Rows[0]["R0026"].ToString()))
                                            this.cmbSex.SelectedIndex = this.cmbSex.Items.IndexOfValue(dt.Rows[0]["R0026"].ToString());

                                    }
                                    catch { }
                                }
                            }
                        }

                        conn.Close();



                    }
                }
            }
            else
                btnImageUpload.Visible = false;
        }

        void WriteLog(string ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex);
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            string path = HttpContext.Current.Server.MapPath("~/App_Data/Log.txt");
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(path, true))
            {
                writer.WriteLine(message);
                writer.Close();
            }
        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        protected void btnKaydet_Click(object sender, EventArgs e)
        {
            if (currentCardUniqueID == string.Empty)
            {
                SaveNew();
            }
            else
            {
                SaveEdit();
            }
        }

        public void SaveNew()
        {
            try
            {
                //GetWebFonksiyonlari().LucentCrew_TayfaGir(txtName.Text, txtSurName.Text
                //    , cmbstatus.SelectedItem != null ? cmbstatus.SelectedItem.Value.ToString() : ""
                //    , cmbNationality.SelectedItem != null ? cmbNationality.SelectedItem.Value.ToString() : ""
                //    , cmbSex.SelectedItem != null ? cmbSex.SelectedItem.Value.ToString() : ""
                //    , method.DateToStringNAVType(string.Format("{0:dd.MM.yyyy}", txtReadinessDate.Date))
                //    , txtIdentityNo.Text
                //    , method.DateToStringNAVType(string.Format("{0:dd.MM.yyyy}", txtBirthDate.Date))
                //    , txtPlaceOfBirth.Text
                //    , txtPhoneNo.Text, txtEmail.Text, txtAdress.Text, txtCity.Text
                //    , cmbUlke.SelectedItem != null ? cmbUlke.SelectedItem.Value.ToString() : ""
                //    , txtCurrentStatus.Text
                //    , cmbCoc.SelectedItem != null ? cmbCoc.SelectedItem.Value.ToString() : ""
                //    , cmbLucentRank.SelectedItem != null ? cmbLucentRank.SelectedItem.Value.ToString() : ""
                //    , txtTimeInRank.Text, txtPassportNo.Text
                //    , method.DateToStringNAVType(string.Format("{0:dd.MM.yyyy}", txtPassIssueDate.Date))
                //    , method.DateToStringNAVType(string.Format("{0:dd.MM.yyyy}", txtPassExDate.Date))
                //    , txtSeamanBookNo.Text
                //    , method.DateToStringNAVType(string.Format("{0:dd.MM.yyyy}", txtSeamanBookIssueDate.Date))
                //      , method.DateToStringNAVType(string.Format("{0:dd.MM.yyyy}", txtSeamanBookIssueDate.Date))
                //      , _obj.UserName);


                if (!ClientScript.IsStartupScriptRegistered("JSTayfaNew"))
                {
                    ClientScript.RegisterStartupScript(GetType(), "JSTayfaNew", @"parent.RefreshAllCollapse();", true);
                }




            }
            catch (Exception ex)
            {
                MessageBox(ex.Message.ToString().Replace("'", " "));
            }
        }

        private void SaveEdit()
        {
            try
            {
                //GetWebFonksiyonlari().LucentCrew_TayfaDuzenle(currentCardUniqueID,txtName.Text, txtSurName.Text
                //    , cmbstatus.SelectedItem != null ? cmbstatus.SelectedItem.Value.ToString() : ""
                //    , cmbNationality.SelectedItem != null ? cmbNationality.SelectedItem.Value.ToString() : ""
                //    , cmbSex.SelectedItem != null ? cmbSex.SelectedItem.Value.ToString() : ""
                //    , method.DateToStringNAVType(string.Format("{0:dd.MM.yyyy}", txtReadinessDate.Date))
                //    , txtIdentityNo.Text
                //    , method.DateToStringNAVType(string.Format("{0:dd.MM.yyyy}", txtBirthDate.Date))
                //    , txtPlaceOfBirth.Text
                //    , txtPhoneNo.Text, txtEmail.Text, txtAdress.Text, txtCity.Text
                //    , cmbUlke.SelectedItem != null ? cmbUlke.SelectedItem.Value.ToString() : ""
                //    , txtCurrentStatus.Text
                //    , cmbCoc.SelectedItem != null ? cmbCoc.SelectedItem.Value.ToString() : ""
                //    , cmbLucentRank.SelectedItem != null ? cmbLucentRank.SelectedItem.Value.ToString() : ""
                //    , txtTimeInRank.Text, txtPassportNo.Text
                //    , method.DateToStringNAVType(string.Format("{0:dd.MM.yyyy}", txtPassIssueDate.Date))
                //    , method.DateToStringNAVType(string.Format("{0:dd.MM.yyyy}", txtPassExDate.Date))
                //    , txtSeamanBookNo.Text
                //    , method.DateToStringNAVType(string.Format("{0:dd.MM.yyyy}", txtSeamanBookIssueDate.Date))
                //      , method.DateToStringNAVType(string.Format("{0:dd.MM.yyyy}", txtSeamanBookIssueDate.Date))
                //      , _obj.UserName);


                if (!ClientScript.IsStartupScriptRegistered("JSTayfaNew"))
                {
                    ClientScript.RegisterStartupScript(GetType(), "JSTayfaNew", @"parent.RefreshAllCollapse();", true);
                }
            }
            catch (Exception ex)
            {
                MessageBox(ex.Message.ToString().Replace("'", " "));
            }
        }
 



        protected void btnSil_Click(object sender, EventArgs e)
        {

        }

        protected void btnImageUpload_Click(object sender, EventArgs e)
        {
            return;
            if (!ClientScript.IsStartupScriptRegistered("JSTayfaUpImage"))
            {
                ClientScript.RegisterStartupScript(GetType(), "JSTayfaUpImage", @"window.open('./../dosyabagla.aspx?BelgeNo=" + currentCardUniqueID + "&Tur=33&dtur=33&NavType=33&GId=" + Request.QueryString["GId"]+"','List','scrollbars=no,resizable=no,width=650,height=460,top=200,left=400');", true);
            }

        }
    }
}