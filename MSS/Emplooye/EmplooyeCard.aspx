﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmplooyeCard.aspx.cs" Inherits="MSS1.Emplooye.EmplooyeCard" %>


<%@ Register Src="~/controls/DataTableControlGAC.ascx" TagName="StDataTable" TagPrefix="STDT" %>

<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../css/GAC_MenuStyle.css" rel="stylesheet" />
    <link href="../css/GAC_NewStyle.css?v=V1" rel="stylesheet" />
    <link href="../css/GAC_CardStyle.css?v=V1" rel="stylesheet" />
    <style>
        .dxflGroup {
            padding: 0px 5px;
            width: 100%;
        }

            .dxflGroup tr:first-child > .dxflGroupCell > .dxflGroupBox, .dxflGroup > .dxflChildInFirstRowSys > .dxflGroupCell > .dxflGroupBox {
                margin-top: -5px;
            }

        .dx-vam {
            color: white;
        }

        .myImage {
            max-height: 225px;
            max-width: 200px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%">
            <table style="width: 100%; border-width: 0px;">
                <tr>
                    <td style="width: 40%; vertical-align: top">
                        <dx:ASPxFormLayout ID="Box1" runat="server" Width="100%">
                            <Items>
                                <dx:LayoutGroup ShowCaption="False">
                                    <Items>
                                        <dx:LayoutItem FieldName="Name" Caption="Name Surname" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtName" runat="server" Width="100%" Height="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="BirthDate" Border-BorderColor="DarkBlue" Caption="Birth Date" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxDateEdit runat="server" ID="txtBirthDate" Width="100%" DisplayFormatString="dd.MM.yyyy" EditFormatString="dd.MM.yyyy"></dx:ASPxDateEdit>

                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="Gender" Caption="Gender" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxComboBox ID="cmbSex" runat="server" Width="100%" CssClass="carditemStyle">
                                                        <Items>
                                                            <dx:ListEditItem Selected="true" Text="" Value="0" />
                                                            <dx:ListEditItem Text="MALE" Value="1" />
                                                            <dx:ListEditItem Text="FEMALE" Value="2" />
                                                        </Items>
                                                    </dx:ASPxComboBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="IdentityNo" Caption="Identification Number" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtIdentityNo" runat="server" Width="100%" Height="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="SocialSecNumber" Caption="Social Security Number" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtSocialSecurityNumber" runat="server" Width="100%" Height="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="EMAIL" Caption="Email" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtEmail" runat="server" Width="100%" Height="10%" CssClass="carditemStyle"></dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="PHONENO" Caption="Phone" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtPhoneNo" runat="server" Width="100%" Height="10%" CssClass="carditemStyle"></dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                    </Items>
                                    <GroupBoxStyle CssClass="cardBox"></GroupBoxStyle>
                                    <ParentContainerStyle Paddings-Padding="0"></ParentContainerStyle>
                                </dx:LayoutGroup>
                            </Items>
                        </dx:ASPxFormLayout>
                    </td>
                    <td style="width: 30%; vertical-align: top">
                        <dx:ASPxFormLayout ID="Box2" runat="server" Width="100%">
                            <Items>
                                <dx:LayoutGroup ShowCaption="False">
                                    <Items>
                                        <dx:LayoutItem FieldName="CURRENTSTATUS" Border-BorderColor="DarkBlue" Caption="Status" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtCurrentStatus" ReadOnly="true" runat="server" Width="100%" Height="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="EmployementDate" Caption="Employement Date" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxDateEdit runat="server" ID="txtEmployementDate" ReadOnly="true" DisplayFormatString="dd.MM.yyyy" EditFormatString="dd.MM.yyyy"></dx:ASPxDateEdit>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="TerminationDate" Caption="Termination Date" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxDateEdit runat="server" ID="txtTerminationDate" ReadOnly="true" DisplayFormatString="dd.MM.yyyy" EditFormatString="dd.MM.yyyy"></dx:ASPxDateEdit>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="Region" Caption="Region" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtRegion" ReadOnly="true" runat="server" Width="100%" Height="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="Department" Caption="Department" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtDepartment" ReadOnly="true" runat="server" Width="100%" Height="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>

                                    </Items>
                                    <GroupBoxStyle CssClass="cardBox"></GroupBoxStyle>
                                    <ParentContainerStyle Paddings-Padding="0"></ParentContainerStyle>
                                </dx:LayoutGroup>
                            </Items>
                        </dx:ASPxFormLayout>
                    </td>
                    <td style="width: 30%; vertical-align: top">
                        <table style="width: 100%; vertical-align: top">
                            <tr>
                                <td>
                                    <dx:ASPxFormLayout ID="Box3" Visible="true" runat="server" Width="100%">
                                        <Items>
                                            <dx:LayoutGroup ShowCaption="true" Caption="PHOTO" Name="firmaTanitimi" Height="261px">
                                                <Items>
                                                    <dx:LayoutItem FieldName="Photo" ShowCaption="false" CaptionSettings-Location="Top">
                                                        <LayoutItemNestedControlCollection>
                                                            <dx:LayoutItemNestedControlContainer>
                                                                <table style="width: 100%; table-layout: fixed">
                                                                    <tr>
                                                                        <td style="text-align: center">
                                                                            <dx:ASPxImage ID="ImgPhoto" CssClass="myImage" runat="server">
                                                                            </dx:ASPxImage>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </dx:LayoutItemNestedControlContainer>
                                                        </LayoutItemNestedControlCollection>
                                                    </dx:LayoutItem>
                                                </Items>
                                                <GroupBoxStyle CssClass="cardBox"></GroupBoxStyle>
                                                <ParentContainerStyle Paddings-Padding="0"></ParentContainerStyle>
                                            </dx:LayoutGroup>
                                        </Items>
                                    </dx:ASPxFormLayout>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="width: 100%; padding-left: 5px">
                                        <tr>
                                            <td>
                                                <dx:ASPxButton ID="btnKaydet" Text="SAVE / EDIT" Visible="false" runat="server" CssClass="formglobalbuttonStyleCRM" OnClick="btnKaydet_Click"></dx:ASPxButton>
                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btnSil" Text="DELETE" Visible="false" runat="server" CssClass="formglobalbuttonStyleCRM" OnClick="btnSil_Click"></dx:ASPxButton>
                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btnImageUpload" Visible="false" Text="UPDATE PICTURE" runat="server" CssClass="formglobalbuttonStyleCRM" OnClick="btnImageUpload_Click"></dx:ASPxButton>
                                            </td>
                                        </tr>

                                    </table>



                                </td>
                            </tr>
                        </table>


                    </td>
                </tr>

            </table>
        </div>
        <STDT:StDataTable ID="dtstatus" runat="server" />
        <STDT:StDataTable ID="dtLucentRank" runat="server" />
        <STDT:StDataTable ID="dtUlke" runat="server" />
    </form>
</body>
</html>

