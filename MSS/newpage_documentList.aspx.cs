﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.WSGeneric;
using System.ServiceModel;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DevExpress.Web;
using System.Net.Mail;
using System.IO;
using MSS1.Codes;
namespace MSS1
{
    public partial class newpage_documentList : Bases
    {
        #region Connection String & Service
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        
        public ws_baglantı ws = new ws_baglantı();
        SqlConnection baglanti;
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        string kontrol = string.Empty;
        static SessionBase _obj;
        #endregion

        #region Servis Arama
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod(EnableSession = true)]
        public static List<string> SearchTaxarea(string prefixText, int count)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
            if (prefixText == "*")
            {
                string query = "Select Description as name from [0C_00318_00_TAX AREA] where " +
                    " COMPANY='" + _obj.Comp + "' AND Description!='' order by Sıralama asc";
                SqlCommand cmd = new SqlCommand(query, conn);
                conn.Open();
                List<string> bul = new List<string>();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        bul.Add(sdr["name"].ToString());
                    }
                }
                conn.Close();

                return bul;
            }
            else
            {
                string query = "Select Description as name from [0C_00318_00_TAX AREA] where " +
                    "COMPANY='" + _obj.Comp + "' AND Description like '" + prefixText + "%'  order by Sıralama asc";
                SqlCommand cmd = new SqlCommand(query, conn);
                conn.Open();
                List<string> bul = new List<string>();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        bul.Add(sdr["name"].ToString());
                    }
                }
                conn.Close();

                return bul;
            }

        }
        #endregion

        #region Dil Getir
        public void dilgetir()
        {
            Language l = new Language();
            btnSendSelectedRows.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentList", "btnSendSelectedRows");
            lblMailAdress.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentList", "lblMailAdress");
            lblSubject.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentList", "lblSubject");
            lblMailBody.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentList", "lblMailBody");
            lblsayfaadi.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentList", "lblsayfaadi");
        }
        #endregion

        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        { 
              _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
              _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }

            if (IsPostBack || IsCallback) return;
            dilgetir();
            InitGridSTDT(this.STDT1.Table);
            InitGridDTGroups(this.DTGroups.Table);



            if (Request.QueryString["DocNo"] != null && Request.QueryString["DocType"] != null)
            {
                if (Request.QueryString["DeletedDoc"] != null)
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentCard", "msg01"));
                bindlistbox(Request.QueryString["DocType"].ToString());
                LoadDocumentWithDocNo(Request.QueryString["DocType"].ToString(), Request.QueryString["DocNo"].ToString());
            }
            else
            {
                bindlistbox();
                LoadDocument("1");
            }

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }

        void bindlistbox()
        {
            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "select [LineNo] Code,Adi Name from [0C_50055_00_LOOKUPS] where COMPANY='" + _obj.Comp + "' AND Tur='Dokuman Grubu'";

                    using (SqlDataAdapter adptr = new SqlDataAdapter(cmd))
                    {
                        DataTable dt = new DataTable();
                        adptr.Fill(dt);

                        LstGroups.DataSource = dt;
                        LstGroups.TextField = "Name";
                        LstGroups.ValueField = "Code";
                        LstGroups.DataBind();
                    }
                }
            }
        }
        void bindlistbox(string DocType)
        {
            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "select [LineNo] Code,Adi Name from [0C_50055_00_LOOKUPS] " +
                        "where COMPANY='" + _obj.Comp + "' AND Tur='Dokuman Grubu' and [LineNo]=" + DocType + "";

                    using (SqlDataAdapter adptr = new SqlDataAdapter(cmd))
                    {
                        DataTable dt = new DataTable();
                        adptr.Fill(dt);

                        LstGroups.DataSource = dt;
                        LstGroups.TextField = "Name";
                        LstGroups.ValueField = "Code";
                        LstGroups.DataBind();
                    }
                }
            }
        }

        void LoadDocument(string groups)
        {
            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[SP_0WEB_DOKUMAN_SATIR_01]";
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@DocGroup", Convert.ToInt32(groups));
                    cmd.Parameters.AddWithValue("@DocNo", string.Empty);
                    cmd.Parameters.AddWithValue("@Company", _obj.Comp);
                    cmd.Parameters.AddWithValue("@UserName", _obj.UserName);

                    using (SqlDataAdapter adptr = new SqlDataAdapter(cmd))
                    {
                        DataTable dt = new DataTable();
                        adptr.Fill(dt);

                        STDT1.Table.Rows.Clear();
                        STDT1.Table.AcceptChanges();
                        foreach (DataRow row in dt.Rows)
                        {
                            DataRow dr = this.STDT1.Table.NewRow();
                            dr["ID"] = Guid.NewGuid();
                            dr["EntryNo"] = row["EntryNo"];
                            dr["DocNo"] = row["DocNo"];
                            dr["DocCompany"] = row["DocCompany"];
                            dr["DocCompanyInt"] = row["DocCompanyInt"];
                            dr["FilePath"] = row["FilePath"].ToString().Replace(@".\", string.Empty).Replace(@"\", @"\\");
                            dr["DocGrup"] = row["DocGrup"];
                            dr["DocType"] = row["DocType"];
                            dr["RelatedDocNo"] = row["RelatedDocNo"];
                            dr["RelatedDocName"] = row["RelatedDocName"];
                            dr["DocName"] = row["DocName"];
                            dr["DocCreatedDepName"] = row["DocCreatedDepName"];
                            dr["DocDate"] = row["DocDate"];
                            dr["DocValidityDate"] = row["DocValidityDate"];
                            dr["DocStatus"] = row["DocStatus"];
                            dr["DocCreatedDate"] = row["DocCreatedDate"];
                            STDT1.Table.Rows.Add(dr);
                        }
                        STDT1.Table.AcceptChanges();
                        grid.DataBind();
                        grid.ExpandAll();
                    }
                }
            }
            if (LstGroups.SelectedItem.Text == "PERSONEL")
            {
                grid.Columns[5].Caption = "BELGE NO";
                grid.Columns[6].Caption = "PERSONEL NO-ADI";
            }
            else if (LstGroups.SelectedItem.Text == "İLETİŞİM")
            {
                grid.Columns[5].Caption = "İLETİŞİM NO";
                grid.Columns[6].Caption = "İLETİŞİM ŞİRKETİ";
            }
            else
            {
                grid.Columns[5].Caption = LstGroups.SelectedItem.Text + " NO";
                grid.Columns[6].Caption = LstGroups.SelectedItem.Text + " ADI";
            }
        }

        void LoadDocumentWithDocNo(string groups, string BelgeNo)
        {
            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[SP_0WEB_DOKUMAN_SATIR_01]";
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@DocGroup", Convert.ToInt32(groups));
                    cmd.Parameters.AddWithValue("@DocNo", BelgeNo);
                    cmd.Parameters.AddWithValue("@Company", _obj.Comp);
                    cmd.Parameters.AddWithValue("@UserName", _obj.UserName);

                    using (SqlDataAdapter adptr = new SqlDataAdapter(cmd))
                    {
                        DataTable dt = new DataTable();
                        adptr.Fill(dt);

                        STDT1.Table.Rows.Clear();
                        STDT1.Table.AcceptChanges();
                        foreach (DataRow row in dt.Rows)
                        {
                            DataRow dr = this.STDT1.Table.NewRow();
                            dr["ID"] = Guid.NewGuid();
                            dr["EntryNo"] = row["EntryNo"];
                            dr["DocCompany"] = row["DocCompany"];
                            dr["DocCompanyInt"] = row["DocCompanyInt"];
                            dr["DocNo"] = row["DocNo"];
                            dr["DocGrup"] = row["DocGrup"];
                            dr["DocType"] = row["DocType"];
                            dr["FilePath"] = row["FilePath"].ToString().Replace(@".\", string.Empty).Replace(@"\", @"\\");
                            dr["RelatedDocNo"] = row["RelatedDocNo"];
                            dr["RelatedDocName"] = row["RelatedDocName"];
                            dr["DocName"] = row["DocName"];
                            dr["DocCreatedDepName"] = row["DocCreatedDepName"];
                            dr["DocDate"] = row["DocDate"];
                            dr["DocValidityDate"] = row["DocValidityDate"];
                            dr["DocStatus"] = row["DocStatus"];
                            dr["DocCreatedDate"] = row["DocCreatedDate"];
                            STDT1.Table.Rows.Add(dr);
                        }
                        STDT1.Table.AcceptChanges();
                        grid.DataBind();
                        grid.ExpandAll();
                    }
                }
            }
            if (LstGroups.SelectedItem.Text == "PERSONEL")
            {
                grid.Columns[5].Caption = "BELGE NO";
                grid.Columns[6].Caption = "PERSONEL NO-ADI";
                grid.Columns[6].Width = Unit.Pixel(100);
            }
            else if (LstGroups.SelectedItem.Text == "İLETİŞİM")
            {
                grid.Columns[5].Caption = "İLETİŞİM NO";
                grid.Columns[6].Caption = "İLETİŞİM ŞİRKETİ";
                grid.Columns[6].Width = Unit.Pixel(100);
            }
            else
            {
                grid.Columns[5].Caption = LstGroups.SelectedItem.Text + " NO";
                grid.Columns[6].Caption = LstGroups.SelectedItem.Text + " ADI";
            }
        }

        protected void menu_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("ExportExcel"))
            {
                gridExport.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("ExportPdf"))
            {
                gridExport.WritePdfToResponse();
            }
            else
            {
                this.Response.StatusCode = 500;
                this.Response.End();
            }
        }

        private void InitGridSTDT(DataTable dt)
        {
            dt.Columns.Add("ID", typeof(Guid));
            dt.Columns.Add("EntryNo", typeof(int));
            dt.Columns.Add("DocCompany", typeof(string));
            dt.Columns.Add("DocCompanyInt", typeof(int));
            dt.Columns.Add("DocGrup", typeof(string));
            dt.Columns.Add("DocNo", typeof(string));
            dt.Columns.Add("RelatedDocNo", typeof(string));
            dt.Columns.Add("RelatedDocName", typeof(string));
            dt.Columns.Add("FilePath", typeof(string));
            dt.Columns.Add("DocType", typeof(string));
            dt.Columns.Add("DocName", typeof(string));
            dt.Columns.Add("DocCreatedDepName", typeof(string));
            dt.Columns.Add("DocDate", typeof(DateTime));
            dt.Columns.Add("DocValidityDate", typeof(DateTime));
            dt.Columns.Add("DocCreatedDate", typeof(DateTime));
            dt.Columns.Add("DocStatus", typeof(string));

            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = dt.Columns["ID"];
            dt.PrimaryKey = pricols;
        }

        private void InitGridDTGroups(DataTable dt)
        {
            dt.Columns.Add("Code", typeof(string));
            dt.Columns.Add("Name", typeof(string));

        }

        protected void grid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (e.Parameters.ToString().Split('|')[0] == "A")
                LoadDocument(e.Parameters.ToString().Split('|')[1]);
            else
            {
                if (Convert.ToBoolean(e.Parameters.ToString().Split('|')[1]))
                    this.grid.Selection.SelectAll();
                else
                    this.grid.Selection.UnselectAll();
            }
        }

        protected void grid_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
        {

            if (e.CallbackName == "APPLYCOLUMNFILTER" || e.CallbackName == "APPLYFILTER")
            {
                ((ASPxGridView)sender).ExpandAll();
            }

        }
        protected void grid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.FieldName == "RelatedDocName" || e.DataColumn.FieldName == "RelatedDocNo")
            {
                e.Cell.ToolTip = string.Format("{0}", e.CellValue);
            }
        }
        protected void btnSendSelectedRows_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtMailAdresses.Text))
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentList", "msg03"));
                return;
            }
            if (string.IsNullOrEmpty(txtSubject.Text))
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentList", "msg04"));
                return;
            }
            if (string.IsNullOrEmpty(txtMailBody.Text))
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentList", "msg05"));
                return;
            }


            using (MailMessage mail = new MailMessage())
            {
                var sendList = txtMailAdresses.Text.Trim().Split(';').ToList();
                foreach (var m in sendList)
                {
                    if (m.Trim() != "")
                    {
                        if (IsValid(m))
                        {
                            mail.To.Add(m);
                        }
                        else
                        {
                            MessageBox(m + " is not valid.");
                            return;
                        }
                    }
                }
                mail.From = new MailAddress(this.CurrentMailAdres);
                mail.Subject = txtSubject.Text;
                List<object> keyValues = this.grid.GetSelectedFieldValues("ID");
                if (keyValues.Count == 0)
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentList", "msg06"));
                    return;
                }
                int sendsayac = 0;
               
                foreach (object key in keyValues)
                {
                    DataRow row = STDT1.Table.Rows.Find(key);

                    if (File.Exists(Server.MapPath(@"~/" + row["FilePath"].ToString())))
                    {
                        string[] _params = {Convert.ToInt32(row["EntryNo"].ToString()).ToString(), row["DocNo"].ToString()
                            , txtSubject.Text, txtMailAdresses.Text, txtMailBody.Text, _obj.UserName };
                        GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "DocumentsSendGenelOlustur§" + string.Join("§", _params), _obj.Comp);



                        Attachment data = new Attachment(Server.MapPath(@"~/" + row["FilePath"].ToString()));
                        mail.Attachments.Add(data);
                        sendsayac++;
                    }

                }

                if (sendsayac > 0)
                {
                    mail.Body = txtMailBody.Text;
                    mail.IsBodyHtml = true;
                    SmtpClient smtp = SmtpClientConfiguration();
                    smtp.Send(mail);
                    clearMailEdits();
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentList", "msg01"));
                    return;
                }
                else
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentList", "msg02"));
                    return;
                }
            }
        }

        void clearMailEdits()
        {
            txtSubject.Text = string.Empty;
            txtMailAdresses.Text = string.Empty;
            txtMailBody.Text = string.Empty;
        }

        public bool IsValid(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        string mailadresi = string.Empty;
        string CurrentMailAdres
        {
            get
            {
                if (_obj.Comp == "GAC")
                {
                    mailadresi = "noreply@gacturkey.com.tr";
                }
                else if (_obj.Comp == "LAM")
                {
                    mailadresi = "noreply@lam.com.tr";
                }
                else
                    mailadresi = "noreply@lam.com.tr";
                return mailadresi;
            }
        }

        private SmtpClient SmtpClientConfiguration()
        {
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.office365.com";
            smtp.Port = 587;
            smtp.Credentials = new System.Net.NetworkCredential("noreply@lam.com.tr", "s>KK4fG2B@msUbS$");
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            return smtp;
        }
    }
}