﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"  CodeBehind="OdemePlaniAktarim.aspx.cs" Inherits="MSS1.OdemePlaniAktarim" %>

<%@ Register Assembly="DevExpress.Web.ASPxSpreadsheet.v19.1" Namespace="DevExpress.Web.ASPxSpreadsheet" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function OnFileUploadComplete(s, e) {
            Spreadsheet.PerformCallback();
        }
        function UploadCompleteMessage() {

            alert($('#<%= lblFileSuccess.ClientID %>').val());

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>BANKA ÖDEME PLANI AKTARIMI</h2>
    <table border="0" style="width: 950px" cellspacing="15px">
         
        <tr>
            <td style="text-align: left; width: 300px;" colspan="4">
                <asp:Label ID="lbl4" Text="DOSYA : " runat="server"></asp:Label>

                <dx:ASPxUploadControl ID="ASPxUploadControl1" Width="450px" runat="server" ShowUploadButton="false" OnFileUploadComplete="Upload_FileUploadComplete">
                    <validationsettings allowedfileextensions=".csv,.xlsx,.xls" notallowedfileextensionerrortext="Sadece csv ve xlsx dosyaları">
                    </validationsettings>
                    <clientsideevents fileuploadcomplete="OnFileUploadComplete" />
                    <clientsideevents textchanged="function(s, e) {
                          s.SetText(e.inputIndex);
                        s.Upload();
                        }" />
                </dx:ASPxUploadControl>


            </td>
        </tr>
         

        <tr>
            <td style="text-align: left; width: 700px" colspan="2">
                <asp:Button runat="server" ID="Button1" Text="AKTAR" CssClass="btn btn-primary" OnClick="btnAktar_Click" Width="150px" Height="30px" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:ListBox ID="ListBox1" Width="500px" runat="server" Visible="false"></asp:ListBox>
            </td>
        </tr>
    </table>


    <div style="display: none">
        <asp:Label ID="lblFileSuccess" Text="Yükleme Tamam." runat="server"></asp:Label>
        <asp:Label ID="lblSuccess" Text="Aktarım Tamamlandı" runat="server"></asp:Label>
        <asp:Label ID="lblUnSuccess" Text="Aktarım esnasında hata" runat="server"></asp:Label>
        <asp:Label ID="lblPleaseSelectFile" Text="Lütfen bir dosya seçiniz" runat="server"></asp:Label>


        <dx:ASPxSpreadsheet ID="Spreadsheet" OnLoad="Spreadsheet_Load" ClientInstanceName="Spreadsheet" Width="300px" Height="150px" runat="server">
            <clientsideevents endcallback="function(s,e){ UploadCompleteMessage();  }" />
        </dx:ASPxSpreadsheet>
    </div>
</asp:Content>

