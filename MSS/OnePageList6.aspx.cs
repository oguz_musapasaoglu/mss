﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.Data;
using System.Collections;
using MSS1.Codes;

namespace MSS1
{
    public partial class OnePageList6 : System.Web.UI.Page
    {
        Hashtable copiedValues = null;
        SessionBase _obj;
        public string OBJ1_pageId { get; set; }
        public string OBJ2_pageId { get; set; }
        public string OBJ1_fieldId { get; set; }
        public string OBJ2_fieldId { get; set; }

        private void QueryStringParameterDesing()
        {
            try
            {
                string[] obj1 = Request.QueryString["OJ1"].ToString().Split(';');
                OBJ1_pageId = obj1[0].ToString();
                OBJ1_fieldId = obj1[1].ToString();
            }
            catch (Exception)
            {
                OBJ1_pageId = string.Empty;
                OBJ1_fieldId = string.Empty;
            }
            try
            {
                string[] obj2 = Request.QueryString["OJ2"].ToString().Split(';');
                OBJ2_pageId = obj2[0].ToString();
                OBJ2_fieldId = obj2[1].ToString();
            }
            catch (Exception)
            {
                OBJ2_pageId = string.Empty;
                OBJ2_fieldId = string.Empty;
            }


            grid2.JSProperties["cpIsUpdated"] = false;
        }

        webObjectHandler woh = new webObjectHandler();
        ServiceUtilities services = new ServiceUtilities();
        fonksiyonlar f = new fonksiyonlar();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            QueryStringParameterDesing();

            if (IsCallback || IsPostBack) return;
            Response.AppendHeader("Pragma", "no-cache");
            Response.AppendHeader("Cache-Control", "no-cache");
            Response.CacheControl = "no-cache";
            Response.Expires = -1;
            lblGId.Text = Request.QueryString["GId"];

            GetParameters();
            Session["grid1Cloned"] = "0";
            Session["grid2Cloned"] = "0";

            if (OBJ2_pageId == "PG0066")
            {
                pnlGrid.Visible = false;
                pnlFrame.Visible = true;
                iframeView.Attributes.Add("src", "./MainData/BLStock.aspx?GId=" + Request.QueryString["GId"]);
            }
            else if (OBJ2_pageId == "PG0067")
            {
                pnlGrid.Visible = false;
                pnlFrame.Visible = true;
                iframeView.Attributes.Add("src", "./MainData/BLAssignment.aspx?GId=" + Request.QueryString["GId"]);
            }
            else if (OBJ2_pageId == "PG0071")
            {
                pnlGrid.Visible = false;
                pnlFrame.Visible = true;
                iframeView.Attributes.Add("src", "./Mandatory/Mandatory.aspx?GId=" + Request.QueryString["GId"]);
            }

            Session["OP6Obje2Id"] = OBJ2_fieldId;
            Session["OP6Page2Id"] = OBJ2_pageId;
            callObjectData();


            Session["OPL10Grid2Rows"] = webObjectHandler.GetDynamicWebPageObjects(OBJ2_pageId, OBJ2_fieldId, _obj.Comp).Tables[4];
            FillDS1((DataTable)Session["OPL10Grid2Rows"]);
            grid2.DataSourceID = "dtgrid2";
            grid2.DataBind();
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }
        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }


        void GetParameters()
        {
            string Pages = OBJ1_pageId;
            string objects = OBJ1_fieldId;
            using (DataSet ds = f.GetDs(Pages, objects, _obj.Comp))
            {
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        txtlink.Text = ds.Tables[0].Rows[0]["LINK"].ToString();
                        txtlinkField.Text = ds.Tables[0].Rows[0]["LINK FIELD"].ToString();
                        Session["OPL6IsMenu"] = ds.Tables[0].Rows[0]["Is Menu"].ToString();

                    }



                }
            }

            Session["grid1Cloned"] = "0";
            Session["grid2Cloned"] = "0";
            Session["grid3Cloned"] = "0";
            Session["grid4Cloned"] = "0";
            Session["grid5Cloned"] = "0";
            Session["grid6Cloned"] = "0";
            Session["grid7Cloned"] = "0";
            Session["grid8Cloned"] = "0";

        }
        void FillDS1(DataTable dtRows)
        {
            int sayac = 1;
            string query = string.Empty;
            for (int i = 0; i < dtRows.Rows.Count; i++)
            {
                switch (dtRows.Rows[i]["DX Type1"].ToString())
                {
                    case "DATA":
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Source"].ToString()))
                        {
                            query = webObjectHandler.FillCombobox(dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(),
                                dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), _obj.Comp, _obj.UserName);
                            switch (sayac)
                            {
                                case 1:
                                    DS11.Table = f.datatablegonder(query);
                                    break;
                                case 2:
                                    DS12.Table = f.datatablegonder(query);
                                    break;
                                case 3:
                                    DS13.Table = f.datatablegonder(query);
                                    break;
                                case 4:
                                    DS14.Table = f.datatablegonder(query);
                                    break;
                                case 5:
                                    DS15.Table = f.datatablegonder(query);
                                    break;
                                case 6:
                                    DS16.Table = f.datatablegonder(query);
                                    break;
                                case 7:
                                    DS17.Table = f.datatablegonder(query);
                                    break;
                                case 8:
                                    DS18.Table = f.datatablegonder(query);
                                    break;
                                case 9:
                                    DS19.Table = f.datatablegonder(query);
                                    break;
                                case 10:
                                    DS110.Table = f.datatablegonder(query);
                                    break;
                                case 11:
                                    DS111.Table = f.datatablegonder(query);
                                    break;
                                case 12:
                                    DS112.Table = f.datatablegonder(query);
                                    break;
                                case 13:
                                    DS113.Table = f.datatablegonder(query);
                                    break;
                                case 14:
                                    DS114.Table = f.datatablegonder(query);
                                    break;
                                case 15:
                                    DS115.Table = f.datatablegonder(query);
                                    break;
                                case 16:
                                    DS116.Table = f.datatablegonder(query);
                                    break;
                                case 17:
                                    DS117.Table = f.datatablegonder(query);
                                    break;
                                case 18:
                                    DS118.Table = f.datatablegonder(query);
                                    break;
                                case 19:
                                    DS119.Table = f.datatablegonder(query);
                                    break;
                                case 20:
                                    DS120.Table = f.datatablegonder(query);
                                    break;
                            }
                            sayac++;
                        }
                        break;
                }
            }
        }

        private void callObjectData()
        {
            if (OBJ1_pageId.Length > 0 & OBJ1_fieldId.Length > 0)
            {
                dtgrid1.Table = woh.webObjectHandlerExtracted(grid1, menu1, txtinsertparams1, txteditparams1, txtdeleteparams1, _obj.UserName, _obj.Comp,
                 OBJ1_pageId, OBJ1_fieldId, HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString(), HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), 2).Copy();

                DataColumn[] pricols = new DataColumn[1];
                pricols[0] = dtgrid1.Table.Columns["ID"];
                dtgrid1.Table.PrimaryKey = pricols;
                grid1.DataSourceID = "dtgrid1";
                grid1.DataBind();
            }
            else
            {
                grid1.Visible = false;
            }


            if (OBJ2_pageId.Length > 0 & OBJ2_fieldId.Length > 0)
            {
                dtgrid2.Table = woh.webObjectHandlerExtracted(grid2, menu2, txtinsertparams2, txteditparams2, txtdeleteparams2, _obj.UserName, _obj.Comp,
                OBJ2_pageId, OBJ2_fieldId, HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString(), HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), 1).Copy();

                DataColumn[] pricols2 = new DataColumn[1];
                pricols2[0] = dtgrid2.Table.Columns["ID"];
                dtgrid2.Table.PrimaryKey = pricols2;
            }
            else
            {
                grid2.Visible = false;
            }


        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }


        protected void grid1_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            if (txtinsertparams1.Text.Length > 0)
            {
                string[] _temp = txtinsertparams1.Text.Split(',');
                string[] _array = new string[txtinsertparams1.Text.Split(',').Length - 1];
                for (int i = 1; i < _temp.Length; i++)
                {
                    if (_temp[i].ToString() == "COMP")
                    {
                        _array[i - 1] = _obj.Comp;
                    }
                    else if (_temp[i].ToString() == "USER")
                    {
                        _array[i - 1] = _obj.UserName;
                    }
                    else
                    {
                        if (e.NewValues[_temp[i].ToString()] != null)
                            _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                        else
                            _array[i - 1] = _temp[i].ToString();
                    }
                }

                services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);

            }
            Session[grid1.ID + "Cloned"] = "0";
        }

        protected void grid1_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            if (txteditparams1.Text.Length > 0)
            {
                string[] _temp = txteditparams1.Text.Split(',');
                string[] _array = new string[txteditparams1.Text.Split(',').Length - 1];
                for (int i = 1; i < _temp.Length; i++)
                {
                    if (_temp[i].ToString() == "COMP")
                    {
                        _array[i - 1] = _obj.Comp;
                    }
                    else if (_temp[i].ToString() == "USER")
                    {
                        _array[i - 1] = _obj.UserName;
                    }
                    else
                    {
                        if (e.NewValues[_temp[i].ToString()] != null)
                            _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                        else
                            _array[i - 1] = _temp[i].ToString();
                    }
                }
                services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);

            }
            Session[grid1.ID + "Cloned"] = "0";
        }

        protected void grid1_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            if (txtdeleteparams1.Text.Length > 0)
            {
                string[] _temp = txtdeleteparams1.Text.Split(',');
                string[] _array = new string[txtdeleteparams1.Text.Split(',').Length - 1];
                for (int i = 1; i < _temp.Length; i++)
                {
                    if (_temp[i].ToString() == "COMP")
                    {
                        _array[i - 1] = _obj.Comp;
                    }
                    else if (_temp[i].ToString() == "USER")
                    {
                        _array[i - 1] = _obj.UserName;
                    }
                    else
                    {
                        if (e.Values[_temp[i].ToString()] != null)
                            _array[i - 1] = e.Values[_temp[i].ToString()].ToString();
                        else
                            _array[i - 1] = _temp[i].ToString();
                    }
                }
                services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);

            }
            Session[grid1.ID + "Cloned"] = "0";
        }


        protected void grid2_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            if (txtinsertparams2.Text.Length > 0)
            {
                string[] _temp = txtinsertparams2.Text.Split(',');
                string[] _array = new string[txtinsertparams2.Text.Split(',').Length - 1];
                for (int i = 1; i < _temp.Length; i++)
                {
                    if (_temp[i].ToString() == "COMP")
                    {
                        _array[i - 1] = _obj.Comp;
                    }
                    else if (_temp[i].ToString() == "USER")
                    {
                        _array[i - 1] = _obj.UserName;
                    }
                    else if (string.IsNullOrEmpty(_temp[i].ToString()))
                    {
                        _array[i - 1] = "";
                    }
                    else if (_temp[i].ToString().Substring(0, 1) == "$")
                    {
                        try { _array[i - 1] = _temp[i].ToString().Substring(1, _temp[i].ToString().Length - 1); } catch { }
                    }
                    else
                    {
                        if (e.NewValues[_temp[i].ToString()] != null)
                            _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                        else
                            _array[i - 1] = "";
                    }
                }
                services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
            }
            Session[grid2.ID + "Cloned"] = "0";
            gridI.JSProperties["cpIsUpdated"] = true;
        }

        protected void grid2_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            if (txteditparams2.Text.Length > 0)
            {
                string[] _temp = txteditparams2.Text.Split(',');
                string[] _array = new string[txteditparams2.Text.Split(',').Length - 1];
                for (int i = 1; i < _temp.Length; i++)
                {
                    if (_temp[i].ToString() == "COMP")
                    {
                        _array[i - 1] = _obj.Comp;
                    }
                    else if (_temp[i].ToString() == "USER")
                    {
                        _array[i - 1] = _obj.UserName;
                    }
                    else if (string.IsNullOrEmpty(_temp[i].ToString()))
                    {
                        _array[i - 1] = "";
                    }
                    else if (_temp[i].ToString().Substring(0, 1) == "$")
                    {
                        try { _array[i - 1] = _temp[i].ToString().Substring(1, _temp[i].ToString().Length - 1); } catch { }
                    }
                    else
                    {
                        if (e.NewValues[_temp[i].ToString()] != null)
                            _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                        else
                            _array[i - 1] = "";
                    }
                }
                services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
            }
            Session[grid2.ID + "Cloned"] = "0";
            gridI.JSProperties["cpIsUpdated"] = true;
        }

        protected void grid2_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            if (txtdeleteparams2.Text.Length > 0)
            {
                string[] _temp = txtdeleteparams2.Text.Split(',');
                string[] _array = new string[txtdeleteparams2.Text.Split(',').Length - 1];
                for (int i = 1; i < _temp.Length; i++)
                {
                    if (_temp[i].ToString() == "COMP")
                    {
                        _array[i - 1] = _obj.Comp;
                    }
                    else if (_temp[i].ToString() == "USER")
                    {
                        _array[i - 1] = _obj.UserName;
                    }
                    else if (string.IsNullOrEmpty(_temp[i].ToString()))
                    {
                        _array[i - 1] = "";
                    }
                    else if (_temp[i].ToString().Substring(0, 1) == "$")
                    {
                        try { _array[i - 1] = _temp[i].ToString().Substring(1, _temp[i].ToString().Length - 1); } catch { }
                    }
                    else
                    {
                        if (e.Values[_temp[i].ToString()] != null)
                            _array[i - 1] = e.Values[_temp[i].ToString()].ToString();
                        else
                            _array[i - 1] = "";
                    }
                }
                services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);

            }
            Session[grid2.ID + "Cloned"] = "0";
            gridI.JSProperties["cpIsUpdated"] = true;
        }



        protected void menu1_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport1.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                webObjectHandler.ExportPdf(gridExport1);
            }
            else
            {
                this.Response.StatusCode = 500;
                this.Response.End();
            }
        }

        protected void menu2_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport2.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                webObjectHandler.ExportPdf(gridExport2);
            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(OBJ2_pageId, OBJ2_fieldId, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid2, rows, _obj.UserName, _obj.Comp);

                try { grid2_CustomCallback(grid2, null); } catch { }

            }
            else
            {
                this.Response.StatusCode = 500;
                this.Response.End();
            }
        }

        protected void grid1_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView xgrid = (ASPxGridView)sender;
            if (xgrid.IsNewRowEditing)
            {
                e.Editor.ReadOnly = false;
            }
        }

        protected void grid2_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {

            ASPxGridView xgrid = (ASPxGridView)sender;
            string sessionname = "OPL10Grid2Rows";
            if (xgrid.IsNewRowEditing)
            {
                DataRow[] rows = ((DataTable)Session[sessionname]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[sessionname]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[sessionname]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.Visible = false;
                    else
                        e.Editor.Visible = true;
                }

                if (Session[xgrid.ID + "Cloned"].ToString() == "1")
                {
                    DataRow[] clonedRows = ((DataTable)Session[sessionname]).Select("[DX Clone]>0");
                    if (clonedRows.Length > 0)
                    {
                        foreach (DataRow row in clonedRows)
                        {
                            if (e.Column.FieldName == row["DX Field ID"].ToString())
                            {
                                if (Convert.ToInt32(row["DX Clone  Readonly"]) > 0)
                                    e.Editor.ClientEnabled = false;
                                else if (Convert.ToInt32(row["DX Clone Hidden"]) > 0)
                                    e.Editor.Visible = false;
                            }
                        }
                    }
                }


            }
            else
            {
                DataRow[] rows = ((DataTable)Session[sessionname]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[sessionname]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[sessionname]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.Visible = false;
                    else
                        e.Editor.Visible = true;
                }
            }

            string[] obj2 = Request.QueryString["OJ2"].ToString().Split(';');
            if (obj2[1] == "OJ0031")
            {
                if (e.Column.FieldName == "RTYP2")
                {
                    ASPxComboBox cmb = e.Editor as ASPxComboBox;
                    object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP1");
                    if (_val != null)
                    {
                        if (!string.IsNullOrEmpty(_val.ToString()))
                        {
                            string sorgu = "select NEWID() PK, * from VW0111_2 where R0011='" + _val.ToString() + "'";
                            f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                        }
                    }
                    cmb.Callback += Cmb_Callback;
                }
                else if (e.Column.FieldName == "RTYP3")
                {
                    ASPxComboBox cmb = e.Editor as ASPxComboBox;
                    object _val1 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP1");
                    object _val2 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP2");
                    if (_val1 != null & _val2 != null)
                    {
                        if (!string.IsNullOrEmpty(_val1.ToString()) & !string.IsNullOrEmpty(_val2.ToString()))
                        {
                            string sorgu = "select NEWID() PK, * from VW0111_3 where R0011='" + _val1.ToString() + "' and R0012='" + _val2.ToString() + "'";
                            f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                        }
                    }
                    cmb.Callback += Cmb2_Callback;
                }
            }



        }

        private void Cmb_Callback(object sender, CallbackEventArgsBase e)
        {
            if (!string.IsNullOrEmpty(e.Parameter))
            {
                ASPxComboBox cmb = (ASPxComboBox)sender;
                string sorgu = "select NEWID() PK, * from VW0111_2 where R0011='" + e.Parameter + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                Session["OPL10R0011"] = e.Parameter.ToString();

            }

        }
        private void Cmb2_Callback(object sender, CallbackEventArgsBase e)
        {
            if (!string.IsNullOrEmpty(e.Parameter))
            {
                ASPxComboBox cmb = (ASPxComboBox)sender;
                string sorgu = "select NEWID() PK, * from VW0111_3 where R0011='" + Session["OPL10R0011"].ToString() + "' and R0012='" + e.Parameter + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");


            }

        }

        protected void grid1_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (Session["OPL6IsMenu"].ToString() == "1")
            {
                e.Cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#36A1D9");
                e.Cell.Font.Name = "Arial";
                e.Cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
            }

            try
            {
                if (e.DataColumn is GridViewDataHyperLinkColumn)
                {
                    if (string.IsNullOrEmpty(e.CellValue.ToString()))
                        e.Cell.Controls[0].Visible = false;

                }
            }
            catch
            {

            }
        }

        protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            if (copiedValues == null) return;
            string Sessionn = "OPL10Grid" + gridI.ID.Substring(gridI.ID.Length - 1) + "Rows";
            DataRow[] rows = ((DataTable)Session[Sessionn]).Select("[DX Clone] >0");
            if (rows.Length == 0) return;
            foreach (DataRow row in rows)
            {
                e.NewValues[row["DX Field ID"].ToString()] = copiedValues[row["DX Field ID"].ToString()];
            }
        }

        protected void grid2_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(Session["OP6Page2Id"].ToString(), Session["OP6Obje2Id"].ToString(), _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
            }

            string values = string.Empty;
            //if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
            //if (string.IsNullOrEmpty(values)) return;

            dtgrid2.Table = webObjectHandler.fillMainGrid(Session["OP6Page2Id"].ToString(), Session["OP6Obje2Id"].ToString(), _obj.Comp, _obj.UserName).Copy();
            DataColumn[] pricols2 = new DataColumn[1];
            pricols2[0] = dtgrid2.Table.Columns["ID"];
            dtgrid2.Table.PrimaryKey = pricols2;

            grid.DataSourceID = "dtgrid2";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL10Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";

            DataRow[] FilterEditRows = ((DataTable)Session[_sessionname]).Select("[FILTER EDIT]>0");
            if (FilterEditRows.Length > 0)
            {
                using (DataTable dt = webObjectHandler.GetSubGridInsertPermission(OBJ2_pageId, OBJ2_fieldId, _obj.Comp, _obj.UserName, OBJ1_pageId, OBJ1_fieldId, values))
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtSubInsertPer2.Text = dt.Rows[0]["REDIT"].ToString();
                    }
                    else
                        txtSubInsertPer2.Text = "0";
                }
            }
            else
                txtSubInsertPer2.Text = "1";

            grid.FilterExpression = webObjectHandler.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;
            grid.DataBind();
            grid.FilterExpression = _filterexpression;
        }

        protected void grid_RowValidating(object sender, ASPxDataValidationEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            string sessionname = string.Empty;
            switch (gridI.ID)
            {

                case "grid2":
                    sessionname = "OPL10Grid2Rows";
                    break;
                default:
                    break;
            }

            if (e.NewValues["REDIT"] == null)
                e.NewValues["REDIT"] = 0;


            DataRow[] rows1 = ((DataTable)Session[sessionname]).Select("[DX Type1]='DECIMAL' OR [DX Type2]='INTEGER'");
            DataRow[] rows2 = ((DataTable)Session[sessionname]).Select("[DX Type1]='CODE' OR [DX Type2]='TEXT'");
            foreach (DataRow row in rows1)
            {
                if (e.NewValues[row["DX Field ID"].ToString()] == null)
                    e.NewValues[row["DX Field ID"].ToString()] = 0;
            }

            foreach (DataRow row in rows2)
            {
                if (e.NewValues[row["DX Field ID"].ToString()] == null)
                    e.NewValues[row["DX Field ID"].ToString()] = string.Empty;
            }





        }

        protected void grid_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;
            GridViewCommandColumn col = grid.Columns["cmnd"] as GridViewCommandColumn;
            string perm = "0";
            switch (grid.ID)
            {
                case "grid2":
                    perm = txtSubInsertPer2.Text;
                    break;

            }

            if (perm == "0")
            {
                try
                {
                    if (col.ShowNewButtonInHeader)
                        col.ShowNewButtonInHeader = false;
                }
                catch { }
                try
                {
                    foreach (GridViewCommandColumnCustomButton item in col.CustomButtons)
                    {
                        if (item.ID == "Clone") col.CustomButtons.Remove(item);

                    }
                }
                catch { }
            }

        }

        protected void Grid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            ASPxGridView Igrid = sender as ASPxGridView;
            if (Igrid.ID == "grid2")
            {
                DataRow[] rows = ((DataTable)Session["OPL10Grid2Rows"]).Select("[DX MouseOver Text]<>'' and [DX Field ID]='" + e.DataColumn.FieldName + "'");
                if (rows.Length > 0)
                {
                    try
                    {
                        string _val = Igrid.GetRowValues(e.VisibleIndex, rows[0]["DX MouseOver Text"].ToString()).ToString();
                        e.Cell.ToolTip = _val;
                    }
                    catch { e.Cell.ToolTip = string.Format("{0}", e.CellValue); }
                }
                else
                    e.Cell.ToolTip = string.Format("{0}", e.CellValue);
            }
            else
                e.Cell.ToolTip = string.Format("{0}", e.CellValue);

            try
            {
                if (e.DataColumn is GridViewDataHyperLinkColumn)
                {
                    if (string.IsNullOrEmpty(e.CellValue.ToString()))
                        e.Cell.Controls[0].Visible = false;

                }
            }
            catch
            {

            }
        }
        protected void grid_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {

            try
            {

                if (e.RowType == GridViewRowType.Data)
                {

                    ASPxGridView gridI = sender as ASPxGridView;
                    object rfont = e.GetValue("RFONT")
                                , rback = e.GetValue("RBACK")
                                , rftsz = e.GetValue("RFTSZ")
                                , rftwt = e.GetValue("RFTWT")
                                , rftst = e.GetValue("RFTST");
                    if (rfont == null) return;



                    try
                    {
                        for (int columnIndex = 0; columnIndex < e.Row.Cells.Count; columnIndex++)
                        {
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.Color, rfont.ToString()); } catch { }
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.BackgroundColor, rback.ToString()); } catch { }
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.FontSize, rftsz.ToString() + "px"); } catch { }
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.FontWeight, Convert.ToInt32(rftwt.ToString()) == 0 ? "normal" : "bold"); } catch { }
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.FontStyle, Convert.ToInt32(rftst.ToString()) == 0 ? "normal" : "italic"); } catch { }
                        }

                    }
                    catch { }



                }
            }
            catch { }
        }

        protected void Grid_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;
            string _sessionname = "OPL10Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";
            switch (e.ButtonType)
            {
                case ColumnCommandButtonType.Edit:

                    DataRow[] EditRows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='EDIT' and [DX Display Order]>0");
                    if (EditRows.Length > 0)
                    {
                        string _col = EditRows[0]["DX Permision Field"].ToString();
                        if (grid.GetRowValues(e.VisibleIndex, _col) != null)
                        {
                            if (grid.GetRowValues(e.VisibleIndex, _col).ToString() == "0")
                            {
                                e.Visible = false;
                            }
                        }
                    }
                    else
                        e.Visible = false;
                    break;
                case ColumnCommandButtonType.Delete:
                    DataRow[] DeleteRows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='DELETE' and [DX Display Order]>0");
                    if (DeleteRows.Length > 0)
                    {
                        string _col = DeleteRows[0]["DX Permision Field"].ToString();
                        if (grid.GetRowValues(e.VisibleIndex, _col) != null)
                        {
                            if (grid.GetRowValues(e.VisibleIndex, _col).ToString() == "0")
                            {
                                e.Visible = false;
                            }
                        }
                    }
                    else
                        e.Visible = false;
                    break;
                case ColumnCommandButtonType.SelectCheckbox:
                case ColumnCommandButtonType.Select:
                    DataRow[] selectrows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='SELECT' and [DX Display Order]>0");
                    string _selcol = selectrows[0]["DX Permision Field"].ToString();
                    if (!string.IsNullOrEmpty(_selcol))
                    {
                        if (grid.GetRowValues(e.VisibleIndex, _selcol).ToString() == "0")
                            e.Enabled = false;
                    }
                    break;
            }
        }

        protected void grid_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            string _sessionname = "OPL10Grid" + gridI.ID.Substring(gridI.ID.Length - 1) + "Rows";
            if (e.ButtonID == "Clone")
            {
                string Sessionn = "OPL10Grid" + gridI.ID.Substring(gridI.ID.Length - 1) + "Rows";
                DataRow[] rows = ((DataTable)Session[Sessionn]).Select("[DX Clone]>0");
                if (rows.Length > 0)
                {
                    copiedValues = new Hashtable();
                    foreach (DataRow row in rows)
                    {
                        copiedValues[row["DX Field ID"].ToString()] = gridI.GetRowValues(e.VisibleIndex, row["DX Field ID"].ToString());
                    }
                }
                Session[gridI.ID + "Cloned"] = "1";
                gridI.AddNewRow();
            }
            else if (e.ButtonID == "SelectAll")
            {
                DataRow[] selectrows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='SELECT' and [DX Display Order]>0");
                string _selcol = selectrows[0]["DX Permision Field"].ToString();
                if (!string.IsNullOrEmpty(_selcol))
                {
                    for (int counter = 0; counter < gridI.VisibleRowCount; counter++)
                    {
                        if (Convert.ToInt32(gridI.GetRowValues(counter, _selcol)) == 1)
                            gridI.Selection.SelectRow(counter);
                    }
                }
                else
                    gridI.Selection.SelectAll();
            }
            else if (e.ButtonID == "UnSelectAll")
            {
                gridI.Selection.UnselectAll();
            }

        }
    }
}