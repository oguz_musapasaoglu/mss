﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Satinalmaiade.aspx.cs" Inherits="MSS1.FaturaCard.Satinalmaiade" %>

<%@ Register Src="~/controls/DataTableControlGAC.ascx" TagName="StDataTable" TagPrefix="STDT" %>

<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpreadsheet.v19.1" Namespace="DevExpress.Web.ASPxSpreadsheet" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../css/GAC_MenuStyle.css?v=51" rel="stylesheet" />
    <link href="../css/GAC_NewStyle.css?v=51" rel="stylesheet" />
    <link href="../css/GAC_CardStyle.css?v=51" rel="stylesheet" />
    <style>
        .dxflGroup {
            padding: 0px 5px;
            width: 100%;
        }

            .dxflGroup tr:first-child > .dxflGroupCell > .dxflGroupBox, .dxflGroup > .dxflChildInFirstRowSys > .dxflGroupCell > .dxflGroupBox {
                margin-top: -5px;
            }

        .dx-vam {
            color: white;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%">
            <asp:HiddenField runat="server" ID="HiddenDocIsSaved" Value="0" />
            <table style="width: 100%; border-width: 0px;">
                <tr>
                    <td style="width: 35%; vertical-align: top">
                        <dx:ASPxFormLayout ID="Box1" runat="server" Width="100%">
                            <Items>
                                <dx:LayoutGroup ShowCaption="False">
                                    <Items>
                                        <dx:LayoutItem FieldName="lblFaturaNo" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Names="Arial, sans-serif" CaptionStyle-Font-Size="7pt">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtFaturaNo" Enabled="false" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" runat="server" ForeColor="Black" CssClass="textboxStyle" Width="100%"></dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblVendCust" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Names="Arial, sans-serif" CaptionStyle-Font-Size="7pt">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxComboBox ID="cmbVendCust" DataSourceID="DSVendCust" TextField="Text" ValueField="Code" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt"
                                                        ClientInstanceName="cmbVendCust" runat="server" AutoPostBack="true" CssClass="dropdownStyle" Width="100%"
                                                        OnSelectedIndexChanged="cmbVendCust_SelectedIndexChanged">
                                                    </dx:ASPxComboBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblVendCustTaxNo" Visible="false" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Names="Arial, sans-serif" CaptionStyle-Font-Size="7pt">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 50%; text-align: left">
                                                                <dx:ASPxTextBox Enabled="false" ID="txtVergiDaire" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" runat="server" CssClass="textboxStyle" Width="100%"></dx:ASPxTextBox>
                                                            </td>
                                                            <td style="width: 50%; text-align: left">
                                                                <dx:ASPxTextBox ID="txtSaticiVergiDaireNo" Enabled="false" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" runat="server" ForeColor="Black" CssClass="textboxStyle" Width="100%"></dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblReferansNo" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Names="Arial, sans-serif" CaptionStyle-Font-Size="7pt">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtReferansNo" runat="server" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" CssClass="textboxStyle" Width="100%"></dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblSerino" Visible="false" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Names="Arial, sans-serif" CaptionStyle-Font-Size="7pt">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 35%; text-align: left">
                                                                <dx:ASPxTextBox ID="txtSeriNo" Enabled="false" runat="server" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" CssClass="textboxStyle" Width="100%"></dx:ASPxTextBox>
                                                            </td>
                                                            <td style="width: 65%; text-align: left">
                                                                <dx:ASPxTextBox ID="txtHariciBelgeNo" Enabled="false" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" runat="server" CssClass="textboxStyle" Width="100%"></dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblDeftereNakilTarihi" Visible="false" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Names="Arial, sans-serif" CaptionStyle-Font-Size="7pt">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxDateEdit ID="txtDeftereNakilTarihi" Enabled="false" ClientInstanceName="txtDeftereNakilTarihi" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" EditFormatString="dd.MM.yyyy" DisplayFormatString="{0:dd.MM.yyyy}" runat="server" CssClass="DropDownList" Width="100%">
                                                    </dx:ASPxDateEdit>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblBelgeTarihi" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Names="Arial, sans-serif" CaptionStyle-Font-Size="7pt">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxDateEdit ID="txtBelgeTarihi" Enabled="false" ClientInstanceName="txtBelgeTarihi" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" EditFormatString="dd.MM.yyyy" DisplayFormatString="{0:dd.MM.yyyy}" runat="server" CssClass="DropDownList" Width="100%">
                                                    </dx:ASPxDateEdit>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblFirmaOdemeVadesi" Visible="false" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Names="Arial, sans-serif" CaptionStyle-Font-Size="7pt">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 50%; text-align: left">
                                                                <dx:ASPxTextBox ID="txtFirmaOdemeVadesi" Enabled="false" runat="server" CssClass="textboxStyle"
                                                                    ClientInstanceName="txtFirmaOdemeVadesi" Height="100%" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt"
                                                                    ForeColor="Black" Width="100%" EnableCallbackMode="True">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td style="width: 50%; text-align: left">
                                                                <dx:ASPxComboBox ID="cmbFaturaVadesi" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" runat="server" CssClass="dropdownStyle" Width="100%" Style="border-spacing: 0px; padding: 0px" Height="100%"
                                                                    DataSourceID="DTPaymentTerm" TextField="Text" ValueField="Code">
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblEfKaydi" CaptionStyle-Font-Bold="true" Visible="false" CaptionStyle-Font-Names="Arial, sans-serif" CaptionStyle-Font-Size="7pt">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxCheckBox ID="IsEfKaydi" Enabled="false" ClientInstanceName="IsEfKaydi" runat="server" Width="100%">
                                                    </dx:ASPxCheckBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblMusteriTems" Visible="false" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Names="Arial, sans-serif" CaptionStyle-Font-Size="7pt">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtMusteriTemsilcisi" Enabled="false" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" ClientInstanceName="txtMusteriTemsilcisi" runat="server" CssClass="textboxStyle" Width="100%"></dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                    </Items>

                                    <GroupBoxStyle CssClass="cardBox"></GroupBoxStyle>
                                    <ParentContainerStyle Paddings-Padding="0" Paddings-PaddingBottom="0" Paddings-PaddingLeft="0" Paddings-PaddingRight="0" Paddings-PaddingTop="0"></ParentContainerStyle>
                                </dx:LayoutGroup>
                            </Items>
                        </dx:ASPxFormLayout>
                    </td>
                    <td style="width: 35%; vertical-align: top">
                        <dx:ASPxFormLayout ID="Box2" runat="server" Width="100%">
                            <Items>
                                <dx:LayoutGroup ShowCaption="False">
                                    <Items>
                                        <dx:LayoutItem FieldName="lblDovizCinsi" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Names="Arial, sans-serif" CaptionStyle-Font-Size="7pt">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 50%; text-align: left">
                                                                <dx:ASPxComboBox ID="cmbDovizCinsi" runat="server" CssClass="dropdownStyle" Width="100%" DataSourceID="DTCurrency"
                                                                    ValueField="Code" TextField="Text" ClientInstanceName="cmbDovizCinsi" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt"
                                                                    AutoPostBack="false" EnableCallbackMode="true">
                                                                    <ClientSideEvents SelectedIndexChanged="function(s,e){ callbackPanelDoviz.PerformCallback(s.GetValue()); }" />
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                            <td style="width: 50%; text-align: left">
                                                                <dx:ASPxCallbackPanel ID="callbackPanelDoviz" runat="server" Width="100%" ClientInstanceName="callbackPanelDoviz" OnCallback="callbackPanelDoviz_Callback"
                                                                    SettingsLoadingPanel-Enabled="false">
                                                                    <PanelCollection>
                                                                        <dx:PanelContent>
                                                                            <dx:ASPxSpinEdit ID="txtDovizKuru" Enabled="false" ClientInstanceName="txtDovizKuru" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" runat="server" CssClass="textboxStyle" Width="100%" SpinButtons-ClientVisible="false"
                                                                                NumberType="Float" HorizontalAlign="Right">
                                                                            </dx:ASPxSpinEdit>
                                                                        </dx:PanelContent>
                                                                    </PanelCollection>
                                                                </dx:ASPxCallbackPanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblMatrah" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Names="Arial, sans-serif" CaptionStyle-Font-Size="7pt">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 50%; text-align: left">
                                                                <dx:ASPxSpinEdit ID="txtMatrah" ClientInstanceName="txtMatrah" Enabled="false" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" runat="server" CssClass="numbertextboxStyle" Width="100%"
                                                                    NumberType="Float" HorizontalAlign="Right"
                                                                    SpinButtons-ClientVisible="false">
                                                                </dx:ASPxSpinEdit>
                                                            </td>
                                                            <td style="width: 50%; text-align: left">
                                                                <dx:ASPxSpinEdit ID="txtMatrahSatir" ClientInstanceName="txtMatrahSatir" Enabled="false" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" runat="server" CssClass="numbertextboxStyle" Width="100%"
                                                                    NumberType="Float" HorizontalAlign="Right"
                                                                    SpinButtons-ClientVisible="false">
                                                                </dx:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblKdv" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Names="Arial, sans-serif" CaptionStyle-Font-Size="7pt">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 50%; text-align: left">
                                                                <dx:ASPxSpinEdit ID="txtKdv" Enabled="false" ClientInstanceName="txtKdv" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" runat="server" CssClass="numbertextboxStyle" Width="100%"
                                                                    NumberType="Float" HorizontalAlign="Right"
                                                                    SpinButtons-ClientVisible="false">
                                                                </dx:ASPxSpinEdit>
                                                            </td>
                                                            <td style="width: 50%; text-align: left">
                                                                <dx:ASPxSpinEdit ID="txtKdvSatir" Enabled="false" ClientInstanceName="txtKdvSatir" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" runat="server" CssClass="numbertextboxStyle" Width="100%"
                                                                    NumberType="Float" HorizontalAlign="Right"
                                                                    SpinButtons-ClientVisible="false">
                                                                </dx:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblToplam" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Names="Arial, sans-serif" CaptionStyle-Font-Size="7pt">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 50%; text-align: left">
                                                                <dx:ASPxSpinEdit ID="txtToplam" Enabled="false" ClientInstanceName="txtToplam" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" runat="server" CssClass="numbertextboxStyle" Width="100%"
                                                                    NumberType="Float" HorizontalAlign="Right"
                                                                    SpinButtons-ClientVisible="false">
                                                                </dx:ASPxSpinEdit>
                                                            </td>
                                                            <td style="width: 50%; text-align: left">
                                                                <dx:ASPxSpinEdit ID="txtToplamSatir" Enabled="false" ClientInstanceName="txtToplamSatir" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" runat="server" CssClass="numbertextboxStyle" Width="100%"
                                                                    NumberType="Float" HorizontalAlign="Right"
                                                                    SpinButtons-ClientVisible="false">
                                                                </dx:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblMatrahTL" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Names="Arial, sans-serif" CaptionStyle-Font-Size="7pt">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 50%; text-align: left">
                                                                <dx:ASPxSpinEdit ID="txtMatrahtl" Enabled="false" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" ClientInstanceName="txtMatrahtl" runat="server" CssClass="numbertextboxStyle" Width="100%"
                                                                    NumberType="Float" HorizontalAlign="Right"
                                                                    SpinButtons-ClientVisible="false">
                                                                </dx:ASPxSpinEdit>
                                                            </td>
                                                            <td style="width: 50%; text-align: left">
                                                                <dx:ASPxSpinEdit ID="txtMatrahtlSatir" Enabled="false" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" ClientInstanceName="txtMatrahtlSatir" runat="server" CssClass="numbertextboxStyle" Width="100%"
                                                                    NumberType="Float" HorizontalAlign="Right"
                                                                    SpinButtons-ClientVisible="false">
                                                                </dx:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblKdvTL" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Names="Arial, sans-serif" CaptionStyle-Font-Size="7pt">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 50%; text-align: left">
                                                                <dx:ASPxSpinEdit ID="txtKdvtl" Enabled="false" ClientInstanceName="txtKdvtl" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" runat="server" CssClass="numbertextboxStyle" Width="100%"
                                                                    NumberType="Float" HorizontalAlign="Right"
                                                                    SpinButtons-ClientVisible="false">
                                                                </dx:ASPxSpinEdit>
                                                            </td>
                                                            <td style="width: 50%; text-align: left">
                                                                <dx:ASPxSpinEdit ID="txtKdvtlSatir" Enabled="false" ClientInstanceName="txtKdvtlSatir" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" runat="server" CssClass="numbertextboxStyle" Width="100%"
                                                                    NumberType="Float" HorizontalAlign="Right"
                                                                    SpinButtons-ClientVisible="false">
                                                                </dx:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblToplamtl" Visible="false" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Names="Arial, sans-serif" CaptionStyle-Font-Size="7pt">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 50%; text-align: left">
                                                                <dx:ASPxSpinEdit ID="txtToplamtl" Enabled="false" ClientInstanceName="txtToplamtl" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" runat="server" CssClass="numbertextboxStyle" Width="100%"
                                                                    NumberType="Float" HorizontalAlign="Right"
                                                                    SpinButtons-ClientVisible="false">
                                                                </dx:ASPxSpinEdit>
                                                            </td>
                                                            <td style="width: 50%; text-align: left">
                                                                <dx:ASPxSpinEdit ID="txtToplamtlSatir" Enabled="false" ClientInstanceName="txtToplamtlSatir" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" runat="server" CssClass="numbertextboxStyle" Width="100%"
                                                                    NumberType="Float" HorizontalAlign="Right"
                                                                    SpinButtons-ClientVisible="false">
                                                                </dx:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblKDV1818" Visible="false" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Names="Arial, sans-serif" CaptionStyle-Font-Size="7pt">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 33%; text-align: left">
                                                                <dx:ASPxSpinEdit ID="txtKdv1" ClientInstanceName="txtKdv1" runat="server" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" CssClass="numbertextboxStyle" Width="100%" SpinButtons-ClientVisible="false"
                                                                    NumberType="Float" HorizontalAlign="Right">
                                                                </dx:ASPxSpinEdit>
                                                            </td>
                                                            <td style="width: 33%; text-align: left">
                                                                <dx:ASPxSpinEdit ID="txtKdv8" ClientInstanceName="txtKdv8" runat="server" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" CssClass="numbertextboxStyle" Width="100%" SpinButtons-ClientVisible="false"
                                                                    NumberType="Float" HorizontalAlign="Right">
                                                                </dx:ASPxSpinEdit>
                                                            </td>
                                                            <td style="width: 33%; text-align: left">
                                                                <dx:ASPxSpinEdit ID="txtKdv18" ClientInstanceName="txtKdv18" runat="server" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" CssClass="numbertextboxStyle" Width="100%" SpinButtons-ClientVisible="false"
                                                                    NumberType="Float" HorizontalAlign="Right">
                                                                </dx:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblKurFarkiFatura" Visible="false" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Names="Arial, sans-serif" CaptionStyle-Font-Size="7pt">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 50%; text-align: left">
                                                                <dx:ASPxCheckBox ID="IsKurFarki" ClientInstanceName="IsKurFarki" OnCheckedChanged="IsKurFarki_CheckedChanged" runat="server" Width="100%">
                                                                </dx:ASPxCheckBox>
                                                            </td>
                                                            <td style="width: 50%; text-align: left">
                                                                <dx:ASPxComboBox ID="cmbKurFarkiDovizCinsi" Visible="false" runat="server" CssClass="dropdownStyle" Width="100%" DataSourceID="DTCurrency"
                                                                    ValueField="Code" TextField="Text" ClientInstanceName="cmbKurFarkiDovizCinsi" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt"
                                                                    AutoPostBack="false">
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                    </Items>
                                    <GroupBoxStyle CssClass="cardBox"></GroupBoxStyle>
                                    <ParentContainerStyle Paddings-Padding="0" Paddings-PaddingBottom="0" Paddings-PaddingLeft="0" Paddings-PaddingRight="0" Paddings-PaddingTop="0"></ParentContainerStyle>
                                </dx:LayoutGroup>
                            </Items>
                        </dx:ASPxFormLayout>
                    </td>
                    <td style="width: 30%; vertical-align: top">
                        <dx:ASPxFormLayout ID="Box3" runat="server" Width="100%">
                            <Items>
                                <dx:LayoutGroup ShowCaption="False">
                                    <Items>
                                        <dx:LayoutItem FieldName="lblButton" ShowCaption="false" CaptionSettings-Location="Top">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 50%; text-align: left">
                                                                <asp:Button ID="btnKaydetReferans" runat="server" Visible="false" Text="DEĞİŞİKLİĞİ KAYDET" CssClass="formglobalbuttonStyleFAT" OnClick="btnKaydetReferans_Click" /></td>
                                                            <td style="width: 50%; text-align: left">
                                                                <asp:Button ID="btnadresguncelle" runat="server" Visible="false" Text="ADRES GÜNCELLE" CssClass="formglobalbuttonStyleFAT" OnClick="btnadresguncelle_Click" />
                                                                <asp:Button ID="btnbelgekopyala" runat="server" Visible="false" Text="BELGE KOPYALA" CssClass="formglobalbuttonStyleFAT" OnClick="btnbelgekopyala_Click" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 50%; text-align: left">
                                                                <asp:Button ID="btnyazdır" runat="server" CssClass="formglobalbuttonStyleFAT"
                                                                    OnClick="btnyazdır_Click" Visible="False" />
                                                            </td>
                                                            <td style="width: 50%; text-align: left">
                                                                <asp:Button ID="btnacıklaragonder" runat="server" CssClass="formglobalbuttonStyleFAT"
                                                                    OnClick="btnacıklaragonder_Click"
                                                                    Visible="False" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 50%; text-align: left">
                                                                <asp:Button ID="btnsil" runat="server" Visible="false" CssClass="formglobalbuttonStyleFAT" OnClick="btnsil_Click"
                                                                    OnClientClick="javascript: return silmesoruH()" />
                                                            </td>
                                                            <td style="width: 50%; text-align: left">
                                                                <asp:Button ID="btnsiponayla" runat="server" Visible="false" CssClass="formglobalbuttonStyleFAT"
                                                                    OnClick="btnsiponayla_Click" />
                                                            </td>
                                                        </tr>
                                                         <tr>
                                                            <td style="width: 50%; text-align: left">
                                                                <asp:Button ID="BtnOpenInvoiceSelector" Text="LOJ. SATIRLARI GETİR" runat="server" CssClass="formglobalbuttonStyleFAT"
                                                                    OnClick="BtnOpenInvoiceSelector_Click" />
                                                            </td>
                                                            <td style="width: 50%; text-align: left">
                                                                
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblAdress" Visible="false" ShowCaption="True" CaptionSettings-Location="Top" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Names="Arial, sans-serif" CaptionStyle-Font-Size="7pt">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <asp:TextBox ID="txtAdress" runat="server" Width="100%" Height="50px" MaxLength="1000"
                                                        TextMode="MultiLine" CssClass="carditemStyle"></asp:TextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="lblNot" Visible="false" ShowCaption="True" CaptionSettings-Location="Top" CaptionStyle-Font-Bold="true" CaptionStyle-Font-Names="Arial, sans-serif" CaptionStyle-Font-Size="7pt">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <asp:TextBox ID="txtNot" runat="server" Width="100%" Height="50px" MaxLength="1000"
                                                        TextMode="MultiLine" CssClass="carditemStyle"></asp:TextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>

                                    </Items>
                                    <GroupBoxStyle CssClass="cardBox"></GroupBoxStyle>
                                    <ParentContainerStyle Paddings-Padding="0" Paddings-PaddingBottom="0" Paddings-PaddingLeft="0" Paddings-PaddingRight="0" Paddings-PaddingTop="0"></ParentContainerStyle>
                                </dx:LayoutGroup>
                            </Items>
                        </dx:ASPxFormLayout>
                    </td>

                </tr>
            </table>
        </div>
        <STDT:StDataTable ID="DTVendCust" runat="server" />
        <asp:SqlDataSource ID="DSVendCust" runat="server" ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
        <STDT:StDataTable ID="DTCurrency" runat="server" />
        <STDT:StDataTable ID="DTPaymentTerm" runat="server" />
        <STDT:StDataTable ID="DTJobTypes" runat="server" />
        <STDT:StDataTable ID="DTOnayDepartmani" runat="server" />
        <STDT:StDataTable ID="DTJobNo" runat="server" />
        <STDT:StDataTable ID="DTNots" runat="server" />
        <div style="display: none">
            <asp:CheckBox runat="server" ID="cbavukatlık" />
            <asp:Label runat="server" ID="lblvatpostinggrup"></asp:Label>
            <asp:Label runat="server" ID="lblprojeno"></asp:Label>
            <asp:Label runat="server" ID="lblStatusDesc"></asp:Label>
            <asp:Label runat="server" ID="lblKdvOran"></asp:Label>
            <asp:Label runat="server" ID="lblsaticivergino"></asp:Label>
            <asp:TextBox ID="lblconfirmOnayaGonderMessage" runat="server"></asp:TextBox>
            <asp:TextBox ID="lblSilmeMessage" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtFirmaOdemeVadesiCode" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtSayfaTurveNo" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtSayfaTur" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtselectedPaymentTerms" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtodemeVadesiMessage" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtPageRedirectMessage" runat="server"></asp:TextBox>
            <asp:Label ID="lblAlimStatus" runat="server"></asp:Label>
            <asp:TextBox ID="lblFaturaNo" runat="server"></asp:TextBox>
            <asp:TextBox ID="lblInvoiceNo" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtSalesInvType" runat="server"></asp:TextBox>
        </div>

        <dx:ASPxPopupControl ID="pcAciklama" runat="server" CloseAction="CloseButton" CloseOnEscape="true"
            PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="pcAciklama"
            HeaderText="Açıklama" AllowDragging="True" Modal="True" PopupAnimationType="Fade" Width="20%" Height="10%"
            EnableViewState="False" PopupHorizontalOffset="30" PopupVerticalOffset="30">
            <ClientSideEvents PopUp="function(s, e) { ASPxClientEdit.ClearGroup('createAccountGroup'); }" />
            <SizeGripImage Width="21px" />
            <ContentCollection>
                <dx:PopupControlContentControl>
                    <dx:ASPxPanel ID="NotPanel" runat="server" DefaultButton="btnReddet">
                        <PanelCollection>
                            <dx:PanelContent>
                                <dx:ASPxMemo ID="txtAciklama" runat="server" Width="250px" Height="100px"></dx:ASPxMemo>
                                <br />
                                <asp:Button ID="btnAciklamaKaydet" runat="server" CssClass="formglobalbuttonStyle" OnClick="btnAciklamaKaydet_Click"></asp:Button>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxPanel>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:ASPxPopupControl>

        <script type="text/javascript" lang="javascript">
            function ShowCreateAccountWindow() {
                pcAciklama.Show();
            }

            function ReloadParentPage(FaturaNo) {
                var shref = window.parent.location.href;
                if (shref.includes('&P1=')) {
                    var n = shref.indexOf("&P1", shref.lenght);
                    window.parent.location.href = shref.substring(0, n) + '&P1=' + FaturaNo;
                }
                else
                    window.parent.location.href = window.parent.location.href + '&P1=' + FaturaNo;
            }

            function ReloadParentPageWM(FaturaNo, msg) {
                var shref = window.parent.location.href;
                if (shref.includes('&P1=')) {
                    var n = shref.indexOf("&P1", shref.lenght);
                    window.parent.location.href = shref.substring(0, n) + '&P1=' + FaturaNo;
                }
                else
                    window.parent.location.href = window.parent.location.href + '&P1=' + FaturaNo;

                alert(msg);
            }

            function OpenAktarimPopup() {
                var FaturaNo = $('#<%=lblFaturaNo.ClientID%>').val();
                var InvoiceNo = $('#<%=lblInvoiceNo.ClientID%>').val();
                var url = './PopupExcel.aspx?FaturaNo=' + FaturaNo + '&InvoiceNo=' + InvoiceNo;
                var popwin = window.open(url, 'excelac', 'height=300,width=500,left=400,top=200');
                popwin.focus();
                return false;
            }

            function OnFileUploadComplete(s, e) {
                Spreadsheet.PerformCallback();
            }
            function UploadCompleteMessage() {

                alert('Yükleme Tamam.');

            }

            function silmesoruH() {
                if (confirm('Fatura Silmek istiyor musunuz ?')) {
                    alert("Başarılı Bir Şekilde Silindi.");
                    return true;
                }
                else {
                    alert("Silme İşlemi İptal Edildi !");
                    return false;
                }

            }

            function ShowCreateAccountWindow() {
                pcAciklama.Show();
            }

            function pageClose(pageType) {
                window.opener.location.href = "/FaturaOnOnayListe.aspx?InvoiceType=" + pageType;
                window.close();

            }

            function OdemevadesiKontrol() {
                var message = "", firmaOdemeVadesi = "", faturaOdemeVadesi = "", Tip = "";

                var splitter = $('#<%=txtodemeVadesiMessage.ClientID%>').val().split('-');

                message = splitter[0].toString();
                firmaOdemeVadesi = splitter[1].toString();
                faturaOdemeVadesi = splitter[2].toString();

                if (firmaOdemeVadesi != "" && faturaOdemeVadesi != "") {
                    if (firmaOdemeVadesi != faturaOdemeVadesi) {
                        Tip = "Show";
                    }
                    else {
                        Tip = "Not";
                    }
                }
                else {
                    Tip = "Not";
                }



                if (Tip == "Show") {
                    if (confirm(message)) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return true;
                }


            }



            function ConfirmMessage(tip) {
                var message = "";
                if (tip == "Silme") {
                    message = $('#<%=lblSilmeMessage.ClientID%>').val();
                }
                else if (tip == "Onayla") {
                    message = $('#<%=lblconfirmOnayaGonderMessage.ClientID%>').val();
                }

                if (confirm(message)) {
                    return true;
                }
                else {
                    return false;
                }
            }


            function DosyaPopup() {
                var Popup;
                var splitter = $('#<%=txtSayfaTurveNo.ClientID%>').val().split('-');
                Popup = window.open("/dosyabagla.aspx?BelgeNo=" + splitter[0].toString() + "&tur=28&dtur=28" + "&FaturaOnGiris=" + splitter[1].toString(), "Popup", "scrollbars=no,resizable=no,width=650,height=460,top=200,left=400");
                Popup.focus();
            }

            function DosyaPopupSil() {
                var Popup;
                var splitter = $('#<%=txtSayfaTurveNo.ClientID%>').val().split('-');
                Popup = window.open("dosyagor.aspx?BelgeNo=" + splitter[0].toString() + "&tur=28&dtur=28&Silme=1", "Popup", "scrollbars=no,resizable=no,width=650,height=460,top=200,left=400");
                Popup.focus();
            }

            var lastvendCust = null;
            var postponedCallbackRequired = false;
            function cmbVendCustChanged(cmbVendCust) {
                if (callbackPanel.InCallback()) {
                    postponedCallbackRequired = true;
                }
                else {
                    callbackPanel.PerformCallback(cmbVendCust.GetValue().toString());
                }
            }

            function OnEndCallback(s, e) {
                if (postponedCallbackRequired) {
                    callbackPanel.PerformCallback();
                    postponedCallbackRequired = false;
                }
            }



            var lastdovizCinsi = null;
            var dovizcinsi = null;

            function SettxtToplamTutarTl() {
                if (cmbDovizCinsi.GetValue() == null) {
                    dovizcinsi = "TL";
                }
                else {
                    dovizcinsi = cmbDovizCinsi.GetValue().toString() == '' ? 'TL' : cmbDovizCinsi.GetValue().toString();
                }
                var _kur = 1, tutar = 0;
                if (dovizcinsi != 'TL') {
                    if (txtDovizKuru.GetNumber() == null)
                        _kur = 1;
                    else if (txtDovizKuru.GetNumber() == 0)
                        _kur = 1;
                    else
                        _kur = txtDovizKuru.GetNumber();
                    if (txtToplamTutar.GetNumber() == null)
                        tutar = 0;
                    else
                        tutar = txtToplamTutar.GetNumber();

                    var _valT = parseFloat(_kur) * parseFloat(tutar)
                    txtToplamTutartl.SetNumber(_valT);

                }
                else {
                    if (txtToplamTutar.GetNumber() == null)
                        tutar = 0;
                    else
                        tutar = txtToplamTutar.GetNumber();
                    var _valT = parseFloat(tutar)
                    txtToplamTutartl.SetNumber(_valT);
                }
            }
            function cmbDovizCinsiChanged(cmbDovizCinsi) {
                if (cmbDovizCinsi.GetValue() == null) {
                    dovizcinsi = "TL";
                }
                else {
                    dovizcinsi = cmbDovizCinsi.GetValue().toString();
                }

                if (callbackPanelDoviz.InCallback())
                    lastdovizCinsi = dovizcinsi
                else
                    callbackPanelDoviz.PerformCallback(dovizcinsi);
            }
            function OnEndCallbackDoviz(s, e) {
                if (lastdovizCinsi) {
                    callbackPanelDoviz.PerformCallback(cmbDovizCinsiChanged);
                    lastdovizCinsi = null;
                }
            }
            function ShowCreateAccountWindow() {
                pcAciklama.Show();

            }

            function CalcTotalPrices(s, e) {
                var _val1, _val2;
                if (txtMatrah.GetNumber() == null)
                    _val1 = 0;
                else
                    _val1 = txtMatrah.GetNumber();
                if (txtKdv.GetNumber() == null)
                    _val2 = 0;
                else
                    _val2 = txtKdv.GetNumber();
                var _valT = parseFloat(_val1) + parseFloat(_val2)
                txtToplam.SetNumber(_valT);
            }
        </script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>

    </form>
</body>
</html>


