﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PopupExcel.aspx.cs" Inherits="MSS1.FaturaCard.PopupExcel" %>
<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpreadsheet.v19.1" Namespace="DevExpress.Web.ASPxSpreadsheet" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../css/GAC_MenuStyle.css?v=47" rel="stylesheet" />
    <link href="../css/GAC_NewStyle.css?v=47" rel="stylesheet" />
    <link href="../css/GAC_CardStyle.css?v=47" rel="stylesheet" />
    <style>
        .dxflGroup {
            padding: 0px 5px;
            width: 100%;
        }

            .dxflGroup tr:first-child > .dxflGroupCell > .dxflGroupBox, .dxflGroup > .dxflChildInFirstRowSys > .dxflGroupCell > .dxflGroupBox {
                margin-top: -5px;
            }

        .dx-vam {
            color: white;
        }
    </style>
    <script type="text/javascript" lang="javascript">
        function OpenAktarimPopup() {
            Pop.Show();
            return false;
        }

        function OnFileUploadComplete(s, e) {
            Spreadsheet.PerformCallback();
        }
        function UploadCompleteMessage() {

            alert('Yükleme Tamam.');

        }
    </script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%">
            <dx:ASPxUploadControl ID="ASPxUploadControl1" Width="450px" runat="server" ShowUploadButton="false" OnFileUploadComplete="Upload_FileUploadComplete">
                <ValidationSettings AllowedFileExtensions=".csv,.xlsx,.xls" NotAllowedFileExtensionErrorText="Sadece csv ve xlsx dosyaları">
                </ValidationSettings>
                <ClientSideEvents FileUploadComplete="OnFileUploadComplete" />
                <ClientSideEvents TextChanged="function(s, e) {
                          s.SetText(e.inputIndex);
                        s.Upload();
                        }" />
            </dx:ASPxUploadControl>
            <asp:Button runat="server" ID="btnAktar" Text="AKTAR" CssClass="formglobalbuttonStyleFAT" OnClick="btnAktar_Click" />
            <div style="display: none">
                <dx:ASPxSpreadsheet ID="Spreadsheet" OnLoad="Spreadsheet_Load" ClientInstanceName="Spreadsheet" Width="300px" Height="150px" runat="server">
                    <clientsideevents endcallback="function(s,e){ UploadCompleteMessage();  }" />
                </dx:ASPxSpreadsheet>
            </div>
        </div>
    </form>
</body>
</html>
