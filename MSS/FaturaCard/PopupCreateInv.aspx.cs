﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.WSGeneric;
using MSS1.Codes;

namespace MSS1.FaturaCard
{
    public partial class PopupCreateInv : Bases
    {
        string ConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        public ws_baglantı ws = new ws_baglantı();
        public Methods method = new Methods();
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        SessionBase _obj;
        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        { 
             _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
             _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }
        private enum invoiceTypes { NONE, SATIS, BORCDEKONT, ALIMDEKONT };

        private invoiceTypes invoiceType
        {
            get
            {
                invoiceTypes _pageType = invoiceTypes.NONE;

                if (!string.IsNullOrEmpty(Request.QueryString["InvoiceType"]))
                {
                    if (Request.QueryString["InvoiceType"].ToString() == "SATIS")
                        _pageType = invoiceTypes.SATIS;
                    else if (Request.QueryString["InvoiceType"].ToString() == "BORCDEKONT")
                        _pageType = invoiceTypes.BORCDEKONT;
                    else if (Request.QueryString["InvoiceType"].ToString() == "ALIMDEKONT")
                        _pageType = invoiceTypes.ALIMDEKONT;
                    else
                        _pageType = invoiceTypes.NONE;
                }
                return _pageType;
            }
        }

        private int FaturaTipi
        {
            get
            {
                int _result = 0;
                switch (invoiceType)
                {
                    case invoiceTypes.SATIS:
                        _result = 1;
                        break;
                    case invoiceTypes.BORCDEKONT:
                        _result = 2;
                        break;
                    case invoiceTypes.ALIMDEKONT:
                        _result = 3;
                        break;
                }
                return _result;
            }

        }
        private string JobNo
        {
            get
            {
                return Request.QueryString["JobNo"].ToString();
            }
        }

        private string TaskNo
        {
            get
            {
                return Request.QueryString["TaskNo"].ToString();
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();


        }
        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }

            if (IsPostBack || IsCallback)
            { return; }

            PreparePageFields();


        }

        private void PreparePageFields()
        {

            FillVendCust();
            FillTaskNo();

            InitDataTable(DTCurrency.Table);
            FillCurrency(DTCurrency.Table);

            if (Request.QueryString["CustomerNo"] != null)
                cmbVendCust.Value = Request.QueryString["CustomerNo"].ToString();
            if (Request.QueryString["TaskNo"] != null)
            {
                if (TaskNo != JobNo)
                {
                    cmbTask.Value = Request.QueryString["TaskNo"].ToString();
                    cmbTask.ClientEnabled = false;
                }
            }
            cmbDovizCinsi.Value = "TL";
            txtBelgeTarihi.Date = DateTime.Now;
        }

        private void InitDataTable(DataTable dt)
        {
            dt.Columns.Add("Code", typeof(string));
            dt.Columns.Add("Text", typeof(string));
        }

        private void FillCurrency(DataTable dt)
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select KOD Code, ACIKLAMA Text From [0_W10400] WHERE COMPANY='" + _obj.Comp.ToString() + "'";
                SqlCommand cmd = new SqlCommand(sorgu, conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                cmbDovizCinsi.DataBind();
            }
        }

        private void FillVendCust()
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select [No_] Code, Name Text, PAYMDESC from [0C_00018_00023_02_CUSTOMER_VENDOR]";
                switch (invoiceType)
                {
                    case invoiceTypes.SATIS:
                        sorgu += " where TYPE='CUST' and COMPANY='" + _obj.Comp.ToString() + "'";
                        break;
                    case invoiceTypes.BORCDEKONT:
                        sorgu += " where TYPE='CUST' and COMPANY='" + _obj.Comp.ToString() + "'";
                        break;
                    case invoiceTypes.ALIMDEKONT:
                        sorgu += " where TYPE='VEND' and COMPANY='" + _obj.Comp.ToString() + "'";
                        break;
                }
                cmbVendCust.Items.Clear();
                DSVendCust.SelectCommand = sorgu;
                cmbVendCust.DataBind();
            }
        }

        private void FillTaskNo()
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select [Job Task No_] Code,[Job Task No_]+' - '+[Description] [Text] from [0C_01001_00_JOB_TASK] " +
                    "where COMPANY='" + _obj.Comp.ToString() + "' and [Job No_]='" + JobNo + "'";
                cmbTask.Items.Clear();
                DSTask.SelectCommand = sorgu;
                cmbTask.DataBind();
            }
        }

         

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        protected void btnCreateInv_Click(object sender, EventArgs e)
        {
            string tskNo = string.Empty;
            try
            {
                tskNo = cmbTask.Value.ToString();
            }
            catch
            {

                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "popupcreateinv", "msg001"));
                return;
            }

            string[] _params = { cmbVendCust.Value.ToString(), txtHariciBelgeNo.Text, cmbDovizCinsi.Value.ToString()
                ,  method.DateToStringNAVType(txtBelgeTarihi.Text) ,    method.DateToStringNAVType(txtBelgeTarihi.Text)
                , JobNo, tskNo, _obj.UserName .ToString(), FaturaTipi.ToString() };

            string Docno = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "JobdanFaturaVeSatiriOlustur§" + string.Join("§", _params), _obj.Comp);
            switch (invoiceType)
            {
                case invoiceTypes.SATIS:
                    if (!string.IsNullOrEmpty(Docno))
                    {
                        if (!ClientScript.IsStartupScriptRegistered("JSOpenInv"))
                        {
                            ClientScript.RegisterStartupScript(GetType(), "JSOpenInv", @"window.opener.location.href='./../OnePageList50.aspx?P=P50050&P1=" + Docno + "&GId=" + Request.QueryString["GId"]+"';window.close();", true);
                        }
                    }
                    break;
                case invoiceTypes.BORCDEKONT:
                    if (!string.IsNullOrEmpty(Docno))
                    {
                        if (!ClientScript.IsStartupScriptRegistered("JSOpenInv"))
                        {
                            ClientScript.RegisterStartupScript(GetType(), "JSOpenInv", @"window.opener.location.href='./../OnePageList15.aspx?OJ1=P50110;J50110&OJ2=P50110;J50111&OJ3=P50110;OJ9999&OJ4=P50110;OJ9999&OJ5=P50110;OJ9999&OJ6=P50110;OJ9999&P1=" + Docno + "&GId=" + Request.QueryString["GId"]+"';window.close();", true);
                        }
                    }
                    break;
                case invoiceTypes.ALIMDEKONT:
                    if (!string.IsNullOrEmpty(Docno))
                    {
                        if (!ClientScript.IsStartupScriptRegistered("JSOpenInv"))
                        {
                            ClientScript.RegisterStartupScript(GetType(), "JSOpenInv", @"window.opener.location.href='./../OnePageList15.aspx?OJ1=P50130;J50130&OJ2=P50130;J50131&OJ3=P50130;OJ9999&OJ4=P50130;OJ9999&OJ5=P50130;OJ9999&OJ6=P50130;OJ9999&P1=" + Docno + "&GId=" + Request.QueryString["GId"]+"';window.close();", true);
                        }
                    }
                    break;
            }
        }
    }
}