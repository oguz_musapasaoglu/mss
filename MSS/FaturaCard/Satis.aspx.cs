﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.WSGeneric;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using DevExpress.Web.ASPxSpreadsheet;
using DevExpress.Web.Office;
using DevExpress.Spreadsheet;
using DevExpress.Spreadsheet.Export;
using MSS1.Codes;

namespace MSS1.FaturaCard
{
    public partial class Satis : Bases
    {
        #region Page Fields
        string ConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        public ws_baglantı ws = new ws_baglantı();
        public Methods method = new Methods();
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        SessionBase _obj;
        private enum invoiceTypes { NONE, SATIS, BORCDEKONT };

        private enum SalesInvTypes { NONE, ACIK, YAZDIRILACAK };
        private enum renderTypes { Add, Edit };


        private string faturaSirketi
        {
            get
            {
                string procName = string.Empty;
                switch (Request.QueryString["FaturaStatu"].ToString())
                {
                    case "ALIMISLEMDE":
                        procName = "0_W50319";
                        break;
                    case "ALIMTAMAMLANDI":
                        procName = "0_W50329";
                        break;
                    case "IADEISLEMDE":
                        procName = "0_W50339";
                        break;
                    case "IADETAMAMLANDI":
                        procName = "0_W50349";
                        break;

                }




                return f.tekbirsonucdonder("select RCOMP from [" + procName + "] where RCOMP='" + _obj.Comp.ToString() + "' AND R0011='" + currentinvoiceNo + "'");
            }
        }
        private bool eFatura
        {
            get
            {
                bool _type = false;
                try
                {
                    string sorgu = "select [E-Fatura] from [0D_70004_01_ALIM_FATURA_ONAY_LISTE] where COMPANY='" + _obj.Comp.ToString() + "' AND [Fatura No_]='" + currentinvoiceNo + "'";
                    if (f.tekbirsonucdonder(sorgu) == "1")
                        _type = true;
                    else
                        _type = false;
                }
                catch (Exception)
                {
                    _type = false;
                }
                return _type;
            }
        }
        private decimal limitY
        {
            get
            {
                string limitY = f.tekbirsonucdonder("select LIMITY from [0_W50306] where FTNO='" + currentinvoiceNo + "' and KULLANICI='" + _obj.UserName.ToString() + "' and COMPANY='" + _obj.Comp.ToString() + "'");
                int durum = 0;
                int sayi = 0;
                if (int.TryParse(limitY, out durum))
                {
                    sayi = durum;
                }
                else
                {
                    sayi = 0;
                }
                return Convert.ToDecimal(sayi);
            }
        }
        private invoiceTypes invoiceType
        {
            get
            {
                invoiceTypes _pageType = invoiceTypes.NONE;

                if (!string.IsNullOrEmpty(Request.QueryString["InvoiceType"]))
                {
                    if (Request.QueryString["InvoiceType"].ToString() == "SATIS")
                        _pageType = invoiceTypes.SATIS;
                    else if (Request.QueryString["InvoiceType"].ToString() == "BORCDEKONT")
                        _pageType = invoiceTypes.BORCDEKONT;
                    else
                        _pageType = invoiceTypes.NONE;
                }
                return _pageType;
            }
        }
        private string invoiceStatu
        {
            get
            {
                return Request.QueryString["FaturaStatu"].ToString();
            }
        }
        public bool dosyaVarmi { get; set; }
        public string DosyaAdi { get; set; }
        public string DosyaPath { get; set; }

        private string currentinvoiceNo
        {
            get
            {
                string _invNo = string.Empty;
                try
                {
                    //string[] _temp = Request.QueryString["invoiceNo"].ToString().Split('|');
                    //_invNo = _temp[0];
                    return Request.QueryString["invoiceNo"].ToString();
                }
                catch (Exception)
                {
                    _invNo = string.Empty;
                }
                return _invNo;
            }
        }
        private string currentJobNo
        {
            get
            {
                string _jobNo = string.Empty;
                try
                {
                    _jobNo = Request.QueryString["ProjeNo"].ToString();
                }
                catch (Exception)
                {
                    _jobNo = string.Empty;
                }
                return _jobNo;
            }
        }


        private renderTypes renderType
        {
            get
            {
                renderTypes _rendtype = renderTypes.Add;
                if (string.IsNullOrEmpty(Request.QueryString["invoiceNo"]))
                {
                    _rendtype = renderTypes.Add;
                }
                else
                {
                    _rendtype = renderTypes.Edit;
                }
                return _rendtype;
            }
        }

        private SalesInvTypes SalesInvType
        {
            get
            {
                SalesInvTypes _slstype = SalesInvTypes.NONE;
                if (lblStatusDesc.Text == "0")
                {
                    _slstype = SalesInvTypes.ACIK;
                }
                else if (lblStatusDesc.Text == "1")
                {
                    _slstype = SalesInvTypes.YAZDIRILACAK;
                }
                return _slstype;
            }
        }

        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            EndpointAddress _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }


        #endregion

        private void DilGetir()
        {

            Box1.FindItemByFieldName("lblVendCust").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50050_J50059", "R0101");

            Box1.FindItemByFieldName("lblFaturaNo").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50050_J50059", "R0100");
            Box1.FindItemByFieldName("lblVendCustTaxNo").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50050_J50059", "R0103");
            Box1.FindItemByFieldName("lblReferansNo").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50050_J50059", "R0105");
            Box1.FindItemByFieldName("lblSerino").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50050_J50059", "R0106");
            Box1.FindItemByFieldName("lblDeftereNakilTarihi").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50050_J50059", "R0108");
            Box1.FindItemByFieldName("lblBelgeTarihi").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50050_J50059", "R0109");
            Box1.FindItemByFieldName("lblFirmaOdemeVadesi").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50050_J50059", "R0113");
            Box1.FindItemByFieldName("lblEfKaydi").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50050_J50059", "R0144");
            Box1.FindItemByFieldName("lblMusteriTems").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50050_J50059", "R0123");

            Box2.FindItemByFieldName("lblDovizCinsi").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50050_J50059", "R0133");
            Box2.FindItemByFieldName("lblMatrah").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50050_J50059", "R0135");
            Box2.FindItemByFieldName("lblKdv").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50050_J50059", "R0136");
            Box2.FindItemByFieldName("lblToplam").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50050_J50059", "R0137");
            Box2.FindItemByFieldName("lblMatrahTL").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50050_J50059", "R0138");
            Box2.FindItemByFieldName("lblKdvTL").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50050_J50059", "R0139");
            Box2.FindItemByFieldName("lblToplamtl").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50050_J50059", "R0140");
            Box2.FindItemByFieldName("lblKDV1818").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50050_J50059", "R0141");
            Box2.FindItemByFieldName("lblKurFarkiFatura").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50050_J50059", "R0131");

            Box3.FindItemByFieldName("lblAdress").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50050_J50059", "R0152");
            Box3.FindItemByFieldName("lblNot").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50050_J50059", "R0156");

            btnsil.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satis_sip", "btnsil");
            btnsiponayla.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satis_sip", "btnsiponayla");
            btnyazdır.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satis_sip", "btnyazdır");
            btnacıklaragonder.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satis_sip", "btnacıklaragonder");

            lblSilmeMessage.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblSilmeMessage");
            lblconfirmOnayaGonderMessage.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblconfirmOnayaGonderMessage");
            txtodemeVadesiMessage.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGirisOnay", "txtodemeVadesiMessage");

        }
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();


        }
        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }

            if (IsPostBack || IsCallback)
            { return; }
            PreparePageFields();
            PreparePage();

        }

        private void PreparePage()
        {
            if (renderType == renderTypes.Edit)
            {
                FillFormObjectsForEdit();

            }
        }

        private void FillFormObjectsForEdit()
        {

            string ProcName = string.Empty;

            if (invoiceStatu == "SATISISLEMDE")
                ProcName = "[SP_0_W51511]";
            else if (invoiceStatu == "SATISTAMAMLANDI")
                ProcName = "[SP_0_W51501]";
            else if (invoiceStatu == "SATISTAMAMLANDITUM")
                ProcName = "[SP_0_W51502]";
            else if (invoiceStatu == "DEKONTISLEMDE")
                ProcName = "[SP_0_W51551]";
            else if (invoiceStatu == "DEKONTTAMAMLANDI")
                ProcName = "[SP_0_W51541]";


            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                conn.Open();
                using (SqlCommand sorgu = new SqlCommand())
                {

                    sorgu.Connection = conn;
                    sorgu.CommandTimeout = 1000;
                    sorgu.CommandType = CommandType.StoredProcedure;
                    sorgu.CommandText = ProcName;
                    sorgu.Parameters.AddWithValue("@COMP", _obj.Comp.ToString());
                    sorgu.Parameters.AddWithValue("@USER", _obj.UserName.ToString());
                    sorgu.Parameters.AddWithValue("@YTKG", "");
                    sorgu.Parameters.AddWithValue("@YTK1", "");
                    sorgu.Parameters.AddWithValue("@YTK2", "");
                    sorgu.Parameters.AddWithValue("@YTK3", "");
                    sorgu.Parameters.AddWithValue("@YTK4", "");
                    sorgu.Parameters.AddWithValue("@YTK5", "");
                    sorgu.Parameters.AddWithValue("@PRM1", currentinvoiceNo);
                    sorgu.Parameters.AddWithValue("@PRM2", "");
                    sorgu.Parameters.AddWithValue("@PRM3", "");


                    SqlDataReader sdr = sorgu.ExecuteReader();
                    if (sdr.Read())
                    {
                        txtFaturaNo.Text = sdr["R0100"].ToString();
                        lblInvoiceNo.Text = currentinvoiceNo;
                        lblFaturaNo.Text = sdr["R0100"].ToString();
                        HiddenDocIsSaved.Value = "1";
                        cmbVendCust.Value = sdr["R0101"];
                        txtVergiDaire.Text = sdr["R0103"].ToString();
                        txtSaticiVergiDaireNo.Text = sdr["R0104"].ToString();
                        txtReferansNo.Text = sdr["R0105"].ToString();
                        txtSeriNo.Text = sdr["R0106"].ToString();
                        txtHariciBelgeNo.Text = sdr["R0107"].ToString();
                        txtDeftereNakilTarihi.Date = Convert.ToDateTime(sdr["R0108"].ToString());
                        txtBelgeTarihi.Date = Convert.ToDateTime(sdr["R0109"].ToString());
                        txtFirmaOdemeVadesi.Text = sdr["R0113"].ToString();
                        txtFirmaOdemeVadesiCode.Text = sdr["R0113"].ToString();
                        cmbFaturaVadesi.Value = sdr["R0111"].ToString();
                        if (Convert.ToInt32(sdr["R0144"]) > 0) IsEfKaydi.Checked = true;
                        txtMusteriTemsilcisi.Text = sdr["R0123"].ToString();
                        cmbDovizCinsi.Value = sdr["R0133"];
                        txtDovizKuru.Text = string.Format("{0:N4}", sdr["R0134"]);
                        txtMatrah.Text = string.Format("{0:N2}", sdr["R0135"]);
                        txtMatrahSatir.Text = string.Format("{0:N2}", sdr["R0158"]);
                        txtKdv.Text = string.Format("{0:N2}", sdr["R0136"]);
                        txtKdvSatir.Text = string.Format("{0:N2}", sdr["R0159"]);
                        txtToplam.Text = string.Format("{0:N2}", sdr["R0137"]);
                        txtToplamSatir.Text = string.Format("{0:N2}", sdr["R0160"]);
                        txtMatrahtl.Text = string.Format("{0:N2}", sdr["R0138"]);
                        txtMatrahtlSatir.Text = string.Format("{0:N2}", sdr["R0161"]);
                        txtKdvtl.Text = string.Format("{0:N2}", sdr["R0139"]);
                        txtKdvtlSatir.Text = string.Format("{0:N2}", sdr["R0162"]);
                        txtToplamtl.Text = string.Format("{0:N2}", sdr["R0140"]);
                        txtToplamtlSatir.Text = string.Format("{0:N2}", sdr["R0163"]);
                        txtKdv1.Text = string.Format("{0:N2}", sdr["R0141"]);
                        txtKdv8.Text = string.Format("{0:N2}", sdr["R0142"]);
                        txtKdv18.Text = string.Format("{0:N2}", sdr["R0143"]);
                        if (Convert.ToInt32(sdr["R0131"]) > 0)
                        {
                            IsKurFarki.Checked = true;
                            cmbKurFarkiDovizCinsi.Visible = true;
                        }
                        cmbKurFarkiDovizCinsi.Value = sdr["R0132"];
                        txtAdress.Text = sdr["R0152"].ToString();
                        txtNot.Text = sdr["R0156"].ToString();
                        lblStatusDesc.Text = sdr["R0130"].ToString();



                        if (IsEfKaydi.Checked)
                        {
                            cmbVendCust.Enabled = false;
                            txtSeriNo.Enabled = false;
                            txtHariciBelgeNo.Enabled = false;
                            txtBelgeTarihi.Enabled = false;
                            txtDeftereNakilTarihi.Enabled = false;

                            cmbDovizCinsi.Enabled = false;
                        }
                        txtSayfaTurveNo.Text = sdr["R0100"].ToString() + "-" + invoiceType.ToString();

                        ButtonPerms(sdr);

                    }

                    conn.Close();
                }
            }


        }

        private void PreparePageFields()
        {
            txtFaturaNo.Text = currentinvoiceNo;

            DilGetir();

            //InitDataTable(DTVendCust.Table);
            FillVendCust();

            InitDataTable(DTCurrency.Table);
            FillCurrency(DTCurrency.Table);

            InitDataTable(DTCurrencyKurFarki.Table);
            FillCurrencyKurFarki(DTCurrencyKurFarki.Table);

            InitDataTable(DTPaymentTerm.Table);
            FillPaymentTerms(DTPaymentTerm.Table);

            //InitDataTableNots(DTNots.Table);
            //FillNots(DTNots.Table);
            FormInitialize();


        }

        void FormInitialize()
        {
            if (renderType == renderTypes.Add)
            {
                btnKaydetReferans.Visible = true;
                BtnOpenInvoiceSelector.Visible = false;
                cmbVendCust.Enabled = true;
                cmbFaturaVadesi.Enabled = true;
                cmbDovizCinsi.Enabled = true;
                txtDovizKuru.Enabled = true;
                btnKaydetReferans.Text = "FATURA OLUŞTUR";
                txtDovizKuru.Text = string.Format("{0:N4}", 1);
            }
            else if (renderType == renderTypes.Edit)
            {
                cmbVendCust.Enabled = false;
                cmbFaturaVadesi.Enabled = false;
                cmbDovizCinsi.Enabled = false;
                txtDovizKuru.Enabled = false;

            }
        }

        void ButtonPerms(SqlDataReader sdr)
        {
            btnAciklamaKaydet.Visible = false;
            btnacıklaragonder.Visible = false;
            btnadresguncelle.Visible = false;
            btnKaydetReferans.Visible = false;
            BtnOpenInvoiceSelector.Visible = false;
            btnsil.Visible = false;
            btnsiponayla.Visible = false;
            btnyazdır.Visible = false;

            try { btnacıklaragonder.Visible = Convert.ToInt32(sdr["RB060"]) == 1 ? true : false; } catch { }
            try { btnadresguncelle.Visible = Convert.ToInt32(sdr["RB024"]) == 1 ? true : false; } catch { }
            try { btnKaydetReferans.Visible = Convert.ToInt32(sdr["RB020"]) == 1 ? true : false; } catch { }
            try { BtnOpenInvoiceSelector.Visible = Convert.ToInt32(sdr["RB022"]) == 1 ? true : false; } catch { }
            try { btnsil.Visible = Convert.ToInt32(sdr["RB030"]) == 1 ? true : false; } catch { }
            try { btnsiponayla.Visible = (Convert.ToInt32(sdr["RB040"]) == 1 | Convert.ToInt32(sdr["RB050"]) == 1) ? true : false; } catch { }
            try { btnyazdır.Visible = Convert.ToInt32(sdr["RB070"]) == 1 ? true : false; } catch { }


            if (_obj.Comp.ToString() == "MLH" | _obj.Comp.ToString() == "STENA"
              | _obj.Comp.ToString() == "MOBILIS" | _obj.Comp.ToString() == "LAMKARA" | _obj.Comp.ToString() == "LUCENT"
              | _obj.Comp.ToString() == "SINOTRANS")
                BtnOpenInvoiceSelector.Visible = false;

        }

        private void InitDataTableNots(DataTable dt)
        {
            dt.Columns.Add("notType", typeof(string));
            dt.Columns.Add("Tarih", typeof(DateTime));
            dt.Columns.Add("Saat", typeof(DateTime));
            dt.Columns.Add("Not", typeof(string));
            dt.Columns.Add("Kullanici", typeof(string));
        }

        private void FillNots(DataTable dt)
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select Tarih,Saat, case [Hareket Turu]  when 3 Then 'Red' else 'Not' End notType, Not1+' '+Not2 [Not], KullaniciAdi Kullanici from [vw_AlimOnayTarihce] where [Fatura No_]='" + currentinvoiceNo + "' and (Not1 <>'' or Not2<>'')  and COMPANY='" + _obj.Comp.ToString() + "' order by [Fatura No_], Tarih,Saat ";
                SqlCommand cmd = new SqlCommand(sorgu, conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                //gridNot.DataBind();
            }
        }

        private void openPDF()
        {
            if (HiddenDocIsSaved.Value == "1")
                ClientScript.RegisterStartupScript(this.Page.GetType(), "popupOpener", "var popup=window.open('" + CurrentFolderName(_obj.Comp) + "/Dosya/SatinAlma_Onay/" + currentinvoiceNo + "10000.pdf" + "');popup.focus();", true);
        }

        private void InitDataTable(DataTable dt)
        {
            dt.Columns.Add("Code", typeof(string));
            dt.Columns.Add("Text", typeof(string));
        }

        private void FillVendCust()
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select [No_] Code, Name Text, PAYMDESC from [0C_00018_00023_02_CUSTOMER_VENDOR]";
                sorgu += " where TYPE='CUST' and COMPANY='" + _obj.Comp.ToString() + "'";
                cmbVendCust.Items.Clear();
                DSVendCust.SelectCommand = sorgu;
                cmbVendCust.DataBind();
            }
        }

        private void FillCurrency(DataTable dt)
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select KOD Code, ACIKLAMA Text From [0_W10400] WHERE COMPANY='" + _obj.Comp.ToString() + "'";
                SqlCommand cmd = new SqlCommand(sorgu, conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                cmbDovizCinsi.DataBind();
            }
        }
        private void FillCurrencyKurFarki(DataTable dt)
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select KOD Code, ACIKLAMA Text From [0_W10400] WHERE ACIKLAMA!='TL' and COMPANY='" + _obj.Comp.ToString() + "'";
                SqlCommand cmd = new SqlCommand(sorgu, conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                cmbDovizCinsi.DataBind();
            }
        }
        private void FillPaymentTerms(DataTable dt)
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select Code, Description Text from [0C_00003_00_PAYMENT_TERMS] where COMPANY='" + _obj.Comp.ToString() + "'";
                SqlCommand cmd = new SqlCommand(sorgu, conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                cmbFaturaVadesi.DataBind();
                cmbKurFarkiDovizCinsi.DataBind();
            }
        }

        private void FillOnayDepartmani(DataTable dt)
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select Code, Name Text from [0C_00349_00_DIMENSION VALUES] where Blocked=0 and [Dimension Code]='DEPARTMAN' and COMPANY='" + _obj.Comp.ToString() + "'";
                SqlCommand cmd = new SqlCommand(sorgu, conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                cmbFaturaVadesi.DataBind();
            }
        }
        private void FillJobTypes(DataTable dt)
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select [LineNo] Code, Adi Text from [0C_50055_13_LOOKUPS_AFO_JOB_TUR] where Tur='Job Tur' and COMPANY='" + _obj.Comp.ToString() + "'";
                SqlCommand cmd = new SqlCommand(sorgu, conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                cmbFaturaVadesi.DataBind();
            }
        }

        private void FillJobNo(DataTable dt)
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select [No_] Code ,[GAC Proje No] Text from [0C_00167_02_JOB] where COMPANY='" + _obj.Comp.ToString() + "'";
                SqlCommand cmd = new SqlCommand(sorgu, conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }
        }
        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }


        protected void callbackPanelDoviz_Callback(object sender, CallbackEventArgsBase e)
        {
            txtDovizKuru.Text = f.returnKur(string.Format("{0:dd.MM.yyyy}", DateTime.Now), e.Parameter, _obj.Comp);

        }

        protected void cmbVendCust_SelectedIndexChanged(object sender, EventArgs e)
        {

            string pasifkontrol = f.tekbirsonucdonder("SELECT [Salesperson Code] FROM [0C_00018_00_CUSTOMER] where COMPANY='" + _obj.Comp.ToString() + "' AND No_ ='" + cmbVendCust.Value.ToString() + "'");
            if (pasifkontrol == "PASİF")
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satis_sip", "msg394"));
                cmbVendCust.SelectedIndex = -1;
                return;

            }
            else
            {
                f.satisfaturamusteribilgilerisatissip1(_obj.Comp.ToString(), lblprojeno.Text, cmbVendCust.Value.ToString(), txtAdress, txtVergiDaire, txtSaticiVergiDaireNo, txtMusteriTemsilcisi, cmbFaturaVadesi, cbavukatlık, lblvatpostinggrup);
            }


            if (lblvatpostinggrup.Text == "YURTDIŞI")
            {
                lblKdvOran.Text = "0";
            }

            if (cbavukatlık.Checked)
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satis_sip", "msg395"));
                txtAdress.Text = string.Empty;
                cmbVendCust.SelectedIndex = -1;
            }

            if (f.vendororcustomercontrol(_obj.Comp.ToString(), 0, cmbVendCust.Value.ToString()))
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satis_sip", "msg396"));
                txtAdress.Text = string.Empty;
                cmbVendCust.SelectedIndex = -1;
            }
        }

        protected void btnAciklamaKaydet_Click(object sender, EventArgs e)
        {
            if (invoiceType != invoiceTypes.SATIS) return;
            if (txtAciklama.Text != string.Empty)
            {

                string[] _params = {currentinvoiceNo, _obj.UserName.ToString(), "2", txtAciklama.Text,
                   "", "0", limitY.ToString() };
                string message = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatınalmaOnayDurumDegistir§" + string.Join("§", _params), _obj.Comp);


                if (message != "1")
                {
                    MessageBox(message);
                    if (!ClientScript.IsStartupScriptRegistered("JSFaturaAc"))
                    {
                        ClientScript.RegisterStartupScript(GetType(), "JSFaturaAc", @"parent.RefreshAllCollapse();", true);
                    }
                }
                else
                {

                    if (!ClientScript.IsStartupScriptRegistered("JSFaturaAc"))
                    {
                        ClientScript.RegisterStartupScript(GetType(), "JSFaturaAc", @"parent.RefreshAllCollapse();", true);
                    }
                }
            }
        }

        protected void btnKaydetReferans_Click(object sender, EventArgs e)
        {
            if (invoiceType != invoiceTypes.SATIS) return;
            if (renderType == renderTypes.Add)
            {
                string invno = string.Empty;
                if (_obj.Comp.ToString() == "STENA")
                {
                    if (cmbVendCust.SelectedItem != null & cmbDovizCinsi.SelectedItem != null & txtDovizKuru.Text != string.Empty)
                    {
                        string adres1 = string.Empty, adres2 = string.Empty;

                        if (!string.IsNullOrEmpty(txtAdress.Text))
                        {
                            if (txtAdress.Text.Length <= 50)
                                adres1 = txtAdress.Text;
                            else
                            {
                                adres1 = txtAdress.Text.Substring(0, 50);
                                adres2 = txtAdress.Text.Substring(50, txtAdress.Text.Length - 50);
                            }
                        }
                        string dovizcins = string.Empty;
                        try { dovizcins = cmbDovizCinsi.Value.ToString(); } catch { dovizcins = "TL"; }

                        string[] _params = {cmbVendCust.Value.ToString(),
                            txtReferansNo.Text,
                            cmbFaturaVadesi.Value.ToString(),
                            _obj.UserName.ToString(),
                            dovizcins,
                            adres1, adres2, Convert.ToInt32(IsKurFarki.Checked).ToString(), cmbKurFarkiDovizCinsi.Value != null ? cmbKurFarkiDovizCinsi.Value.ToString() : "", "", "0" };
                        invno = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatisSiparisHeaderOlustur§" + string.Join("§", _params), _obj.Comp);

                    }
                    else
                    {

                        MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satis_sip", "msg460"));
                        return;
                    }
                }
                else
                {
                    if (cmbVendCust.SelectedItem != null & cmbFaturaVadesi.SelectedItem != null & txtMusteriTemsilcisi.Text != string.Empty
                        & cmbDovizCinsi.SelectedItem != null & txtDovizKuru.Text != string.Empty)
                    {
                        string adres1 = string.Empty, adres2 = string.Empty;

                        if (!string.IsNullOrEmpty(txtAdress.Text))
                        {
                            if (txtAdress.Text.Length <= 50)
                                adres1 = txtAdress.Text;
                            else
                            {
                                adres1 = txtAdress.Text.Substring(0, 50);
                                adres2 = txtAdress.Text.Substring(50, txtAdress.Text.Length - 50);
                            }
                        }
                        string dovizcins = string.Empty;
                        try { dovizcins = cmbDovizCinsi.Value.ToString(); } catch { dovizcins = "TL"; }

                        string[] _params = {cmbVendCust.Value.ToString(),
                            txtReferansNo.Text,
                            cmbFaturaVadesi.Value.ToString(),
                            _obj.UserName.ToString(),
                            dovizcins,
                            adres1, adres2, Convert.ToInt32(IsKurFarki.Checked).ToString(), cmbKurFarkiDovizCinsi.Value != null ? cmbKurFarkiDovizCinsi.Value.ToString() : "", "", "0"};
                        invno = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatisSiparisHeaderOlustur§" + string.Join("§", _params), _obj.Comp);

                    }
                    else
                    {
                        MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satis_sip", "msg460"));
                        return;
                    }
                }
                string msg = "Fatura Oluşturuldu";
                if (!ClientScript.IsStartupScriptRegistered("JSFaturaOlustur"))
                {
                    ClientScript.RegisterStartupScript(GetType(), "JSFaturaOlustur", @"ReloadParentPageWM('" + invno + "','" + msg + "');", true);
                }
            }
            else if (renderType == renderTypes.Edit)
            {
                string[] _params = { txtFaturaNo.Text, txtReferansNo.Text, _obj.UserName.ToString() };
                GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatisFaturasiDegisikligiKaydet§" + string.Join("§", _params), _obj.Comp);


                //MessageBox(l.dilgetir(_obj.SelectedLanguage ,"GAC", _obj.UserName.ToString(), "satis_sip", "msg121"));

                string msg = l.dilgetir(_obj.SelectedLanguage, "GAC", _obj.UserName.ToString(), "satis_sip", "msg121");
                if (!ClientScript.IsStartupScriptRegistered("JSFaturaAcikgonder"))
                {
                    ClientScript.RegisterStartupScript(GetType(), "JSFaturaAcikgonder", @"parent.RefreshAllCollapse();alert('" + msg + "');", true);
                }
            }
        }

        protected void btnadresguncelle_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtAdress.Text))
            {
                string[] _params = { txtFaturaNo.Text, txtAdress.Text };
                GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatisSiparisDuzenle§" + string.Join("§", _params), _obj.Comp);

            }
        }

        protected void IsKurFarki_CheckedChanged(object sender, EventArgs e)
        {
            if (IsKurFarki.Checked)
            {
                cmbKurFarkiDovizCinsi.Visible = true;
                cmbDovizCinsi.SelectedIndex = 0;
                cmbDovizCinsi.ReadOnly = true;
                txtDovizKuru.Text = "1.0000";
                txtDovizKuru.ReadOnly = true;
            }
            else
            {
                cmbKurFarkiDovizCinsi.Visible = false;
                cmbKurFarkiDovizCinsi.SelectedIndex = -1;
                cmbDovizCinsi.SelectedIndex = 0;
                cmbDovizCinsi.ReadOnly = false;
                txtDovizKuru.Text = "1.0000";
                txtDovizKuru.ReadOnly = false;
            }
        }

        protected void btnsil_Click(object sender, EventArgs e)
        {
            if (txtFaturaNo.Text != string.Empty)
            {
                string[] _params = { txtFaturaNo.Text, _obj.NameSurName.ToString() };

                GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatisSil§" + string.Join("§", _params), _obj.Comp);

                if (!ClientScript.IsStartupScriptRegistered("JSFaturaAc"))
                {
                    ClientScript.RegisterStartupScript(GetType(), "JSFaturaAc", @"parent.RefreshAllCollapse();", true);
                }
            }
            else
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satis_sip", "msg417"));
            }
        }

        protected void btnsiponayla_Click(object sender, EventArgs e)
        {
            if (IsKurFarki.Checked == false)
            {
                if (txtFaturaNo.Text != string.Empty)
                {

                    if (!ClientScript.IsStartupScriptRegistered("JSFaturaOnayAc"))
                    {
                        ClientScript.RegisterStartupScript(GetType(), "JSFaturaOnayAc", @"window.open('../faturaonayla.aspx?BelgeNo=" + txtFaturaNo.Text + "&DCins=" + cmbDovizCinsi.Value.ToString() + "&InvNo=" + currentinvoiceNo + "&GId=" + Request.QueryString["GId"] + "','List','scrollbars=yes,resizable=no,width=650,height=460,top=200,left=400');", true);
                    }
                }
                else
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satis_sip", "msg409"));
                }
            }
            else
            {
                if (cmbKurFarkiDovizCinsi.Value.ToString() != "TL" & cmbDovizCinsi.Value.ToString() == "TL")
                {

                    if (txtFaturaNo.Text != string.Empty)
                    {
                        if (!ClientScript.IsStartupScriptRegistered("JSFaturaOnayAc"))
                        {
                            ClientScript.RegisterStartupScript(GetType(), "JSFaturaOnayAc", @"window.open('../faturaonayla.aspx?BelgeNo=" + txtFaturaNo.Text + "&DCins=" + cmbDovizCinsi.Value.ToString() + "&InvNo=" + currentinvoiceNo + "&GId=" + Request.QueryString["GId"] + "','List','scrollbars=yes,resizable=no,width=650,height=460,top=200,left=400');", true);
                        }
                    }
                    else
                    {
                        MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satis_sip", "msg410"));
                    }
                }
                else
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satis_sip", "msg411"));
                }
            }
        }

        protected void btnyazdır_Click(object sender, EventArgs e)
        {
            if (invoiceType == invoiceTypes.SATIS)
            {
                string[] _params = { string.Format("{0:MM.dd.yyyy}", txtBelgeTarihi.Date) };
                string drm = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "KurKontrol§" + string.Join("§", _params), _obj.Comp);

                if (drm == "True")
                {
                    if (txtBelgeTarihi.Date > new DateTime(2000, 1, 1))
                    {
                        if (!ClientScript.IsStartupScriptRegistered("JSFaturaYazAc"))
                        {
                            ClientScript.RegisterStartupScript(GetType(), "JSFaturaYazAc", @"window.open('../satisyazdırma.aspx?BelgeNo=" + txtFaturaNo.Text + "&Tarih=" + txtBelgeTarihi.Text + "&InvNo=" + currentinvoiceNo + "&GId=" + Request.QueryString["GId"] + "','List','scrollbars=yes,resizable=no,width=650,height=460,top=200,left=400');", true);
                        }
                    }
                    else
                    {
                        MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satis_sip", "msg414"));
                    }
                }
                else
                {
                    MessageBox(txtBelgeTarihi.Text + l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satis_sip", "msg415"));
                }
            }

            if (invoiceType == invoiceTypes.BORCDEKONT)
            {
                string[] _params = { string.Format("{0:MM.dd.yyyy}", txtBelgeTarihi.Date) };
                string drm = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "KurKontrol§" + string.Join("§", _params), _obj.Comp);

                if (drm == "True")
                {
                    if (txtBelgeTarihi.Date > new DateTime(2000, 1, 1))
                    {
                        if (!ClientScript.IsStartupScriptRegistered("JSFaturaYazAc"))
                        {
                            ClientScript.RegisterStartupScript(GetType(), "JSFaturaYazAc", @"window.open('../dekontyazdırma.aspx?BelgeNo=" + txtFaturaNo.Text + "&Tarih=" + txtBelgeTarihi.Text + "&InvNo=" + currentinvoiceNo + "&GId=" + Request.QueryString["GId"] + "','List','scrollbars=yes,resizable=no,width=650,height=460,top=200,left=400');", true);
                        }
                    }
                    else
                    {
                        MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "DekontGir", "msg110"));
                    }
                }
                else
                {
                    MessageBox(txtBelgeTarihi.Text + l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "DekontGir", "msg111"));
                }
            }


        }

        protected void btnacıklaragonder_Click(object sender, EventArgs e)
        {
            try
            {
                string[] _params = { txtFaturaNo.Text };
                string drm = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatisFatacikgonder§" + string.Join("§", _params), _obj.Comp);
                
                string msg = txtFaturaNo.Text + l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satis_sip", "msg421");
                if (!ClientScript.IsStartupScriptRegistered("JSFaturaAcikgonder"))
                {
                    ClientScript.RegisterStartupScript(GetType(), "JSFaturaAcikgonder", @"parent.RefreshAllCollapse();alert('" + msg + "');", true);
                }
            }
            catch (Exception lastmsg)
            {
                string msg = lastmsg.Message;
                if (!ClientScript.IsStartupScriptRegistered("JSFaturaAcikgonderHata"))
                {
                    ClientScript.RegisterStartupScript(GetType(), "JSFaturaAcikgonderHata", @"parent.RefreshAllCollapse();alert('" + msg + "');", true);
                }
            }

        }

        protected void BtnOpenInvoiceSelector_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cmbVendCust.Text))
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satinalma", "msg106"));
                return;
            }
            if (!ClientScript.IsStartupScriptRegistered("JSInvoiceSelector"))
            {
                ClientScript.RegisterStartupScript(GetType(), "JSInvoiceSelector", @"window.open('../LogisticInvoiceSelector.aspx?CustomerNo=" + cmbVendCust.Value.ToString() + "&CurrencyCode=" + cmbDovizCinsi.Value.ToString() + "&SipNo=" + txtFaturaNo.Text + "&Page=3&UserName=" + _obj.UserName.ToString() + "&Date=" + txtDeftereNakilTarihi.Text + "&Currency=" + txtDovizKuru.Text + "&VendInvNo=" + txtHariciBelgeNo.Text + "&InvoiceNo=" + lblInvoiceNo.Text + "&GId=" + Request.QueryString["GId"] + "','List','scrollbars=yes,resizable=no,width=900,height=500,top=100,left=50');", true);
            }
        }
    }
}