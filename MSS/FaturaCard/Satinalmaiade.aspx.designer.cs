﻿//------------------------------------------------------------------------------
// <otomatik üretildi>
//     Bu kod bir araç tarafından oluşturuldu.
//
//     Bu dosyada yapılacak değişiklikler yanlış davranışa neden olabilir ve
//     kod tekrar üretildi. 
// </otomatik üretildi>
//------------------------------------------------------------------------------

namespace MSS1.FaturaCard {
    
    
    public partial class Satinalmaiade {
        
        /// <summary>
        /// form1 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;
        
        /// <summary>
        /// HiddenDocIsSaved denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField HiddenDocIsSaved;
        
        /// <summary>
        /// Box1 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxFormLayout Box1;
        
        /// <summary>
        /// txtFaturaNo denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxTextBox txtFaturaNo;
        
        /// <summary>
        /// cmbVendCust denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxComboBox cmbVendCust;
        
        /// <summary>
        /// txtVergiDaire denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxTextBox txtVergiDaire;
        
        /// <summary>
        /// txtSaticiVergiDaireNo denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxTextBox txtSaticiVergiDaireNo;
        
        /// <summary>
        /// txtReferansNo denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxTextBox txtReferansNo;
        
        /// <summary>
        /// txtSeriNo denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxTextBox txtSeriNo;
        
        /// <summary>
        /// txtHariciBelgeNo denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxTextBox txtHariciBelgeNo;
        
        /// <summary>
        /// txtDeftereNakilTarihi denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxDateEdit txtDeftereNakilTarihi;
        
        /// <summary>
        /// txtBelgeTarihi denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxDateEdit txtBelgeTarihi;
        
        /// <summary>
        /// txtFirmaOdemeVadesi denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxTextBox txtFirmaOdemeVadesi;
        
        /// <summary>
        /// cmbFaturaVadesi denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxComboBox cmbFaturaVadesi;
        
        /// <summary>
        /// IsEfKaydi denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxCheckBox IsEfKaydi;
        
        /// <summary>
        /// txtMusteriTemsilcisi denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxTextBox txtMusteriTemsilcisi;
        
        /// <summary>
        /// Box2 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxFormLayout Box2;
        
        /// <summary>
        /// cmbDovizCinsi denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxComboBox cmbDovizCinsi;
        
        /// <summary>
        /// callbackPanelDoviz denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxCallbackPanel callbackPanelDoviz;
        
        /// <summary>
        /// txtDovizKuru denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxSpinEdit txtDovizKuru;
        
        /// <summary>
        /// txtMatrah denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxSpinEdit txtMatrah;
        
        /// <summary>
        /// txtMatrahSatir denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxSpinEdit txtMatrahSatir;
        
        /// <summary>
        /// txtKdv denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxSpinEdit txtKdv;
        
        /// <summary>
        /// txtKdvSatir denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxSpinEdit txtKdvSatir;
        
        /// <summary>
        /// txtToplam denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxSpinEdit txtToplam;
        
        /// <summary>
        /// txtToplamSatir denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxSpinEdit txtToplamSatir;
        
        /// <summary>
        /// txtMatrahtl denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxSpinEdit txtMatrahtl;
        
        /// <summary>
        /// txtMatrahtlSatir denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxSpinEdit txtMatrahtlSatir;
        
        /// <summary>
        /// txtKdvtl denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxSpinEdit txtKdvtl;
        
        /// <summary>
        /// txtKdvtlSatir denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxSpinEdit txtKdvtlSatir;
        
        /// <summary>
        /// txtToplamtl denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxSpinEdit txtToplamtl;
        
        /// <summary>
        /// txtToplamtlSatir denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxSpinEdit txtToplamtlSatir;
        
        /// <summary>
        /// txtKdv1 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxSpinEdit txtKdv1;
        
        /// <summary>
        /// txtKdv8 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxSpinEdit txtKdv8;
        
        /// <summary>
        /// txtKdv18 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxSpinEdit txtKdv18;
        
        /// <summary>
        /// IsKurFarki denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxCheckBox IsKurFarki;
        
        /// <summary>
        /// cmbKurFarkiDovizCinsi denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxComboBox cmbKurFarkiDovizCinsi;
        
        /// <summary>
        /// Box3 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxFormLayout Box3;
        
        /// <summary>
        /// btnKaydetReferans denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnKaydetReferans;
        
        /// <summary>
        /// btnadresguncelle denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnadresguncelle;
        
        /// <summary>
        /// btnbelgekopyala denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnbelgekopyala;
        
        /// <summary>
        /// btnyazdır denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnyazdır;
        
        /// <summary>
        /// btnacıklaragonder denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnacıklaragonder;
        
        /// <summary>
        /// btnsil denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnsil;
        
        /// <summary>
        /// btnsiponayla denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnsiponayla;
        
        /// <summary>
        /// BtnOpenInvoiceSelector denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button BtnOpenInvoiceSelector;
        
        /// <summary>
        /// txtAdress denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtAdress;
        
        /// <summary>
        /// txtNot denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtNot;
        
        /// <summary>
        /// DTVendCust denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::MSS1.Controls.DataTableControlGAC DTVendCust;
        
        /// <summary>
        /// DSVendCust denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource DSVendCust;
        
        /// <summary>
        /// DTCurrency denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::MSS1.Controls.DataTableControlGAC DTCurrency;
        
        /// <summary>
        /// DTPaymentTerm denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::MSS1.Controls.DataTableControlGAC DTPaymentTerm;
        
        /// <summary>
        /// DTJobTypes denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::MSS1.Controls.DataTableControlGAC DTJobTypes;
        
        /// <summary>
        /// DTOnayDepartmani denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::MSS1.Controls.DataTableControlGAC DTOnayDepartmani;
        
        /// <summary>
        /// DTJobNo denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::MSS1.Controls.DataTableControlGAC DTJobNo;
        
        /// <summary>
        /// DTNots denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::MSS1.Controls.DataTableControlGAC DTNots;
        
        /// <summary>
        /// cbavukatlık denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox cbavukatlık;
        
        /// <summary>
        /// lblvatpostinggrup denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblvatpostinggrup;
        
        /// <summary>
        /// lblprojeno denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblprojeno;
        
        /// <summary>
        /// lblStatusDesc denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblStatusDesc;
        
        /// <summary>
        /// lblKdvOran denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblKdvOran;
        
        /// <summary>
        /// lblsaticivergino denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblsaticivergino;
        
        /// <summary>
        /// lblconfirmOnayaGonderMessage denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox lblconfirmOnayaGonderMessage;
        
        /// <summary>
        /// lblSilmeMessage denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox lblSilmeMessage;
        
        /// <summary>
        /// txtFirmaOdemeVadesiCode denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtFirmaOdemeVadesiCode;
        
        /// <summary>
        /// txtSayfaTurveNo denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtSayfaTurveNo;
        
        /// <summary>
        /// txtSayfaTur denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtSayfaTur;
        
        /// <summary>
        /// txtselectedPaymentTerms denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtselectedPaymentTerms;
        
        /// <summary>
        /// txtodemeVadesiMessage denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtodemeVadesiMessage;
        
        /// <summary>
        /// txtPageRedirectMessage denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtPageRedirectMessage;
        
        /// <summary>
        /// lblAlimStatus denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblAlimStatus;
        
        /// <summary>
        /// lblFaturaNo denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox lblFaturaNo;
        
        /// <summary>
        /// lblInvoiceNo denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox lblInvoiceNo;
        
        /// <summary>
        /// txtSalesInvType denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtSalesInvType;
        
        /// <summary>
        /// pcAciklama denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxPopupControl pcAciklama;
        
        /// <summary>
        /// NotPanel denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxPanel NotPanel;
        
        /// <summary>
        /// txtAciklama denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxMemo txtAciklama;
        
        /// <summary>
        /// btnAciklamaKaydet denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnAciklamaKaydet;
    }
}
