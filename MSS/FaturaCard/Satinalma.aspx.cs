﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.WSGeneric;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using DevExpress.Web.ASPxSpreadsheet;
using DevExpress.Web.Office;
using DevExpress.Spreadsheet;
using DevExpress.Spreadsheet.Export;
using System.Text;
using MSS1.Codes;
using DevExpress.Pdf;
using System.Drawing;

namespace MSS1.FaturaCard
{
    public partial class Satinalma : Bases
    {
        #region Page Fields
        string ConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        public ws_baglantı ws = new ws_baglantı();
        public Methods method = new Methods();
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        SessionBase _obj;
        private enum invoiceTypes { NONE, SATINALMA, SATIS_IADE, ALIMDEKONT };
        private enum renderTypes { Add, Edit };


        private string faturaSirketi
        {
            get
            {
                string procName = string.Empty;
                switch (Request.QueryString["FaturaStatu"].ToString())
                {
                    case "ALIMISLEMDE":
                        procName = "0_W50319";
                        break;
                    case "ALIMTAMAMLANDI":
                        procName = "0_W50329";
                        break;
                    case "IADEISLEMDE":
                        procName = "0_W50339";
                        break;
                    case "IADETAMAMLANDI":
                        procName = "0_W50349";
                        break;

                }




                return f.tekbirsonucdonder("select RCOMP from [" + procName + "] where RCOMP='" + _obj.Comp.ToString() + "' AND R0011='" + currentinvoiceNo + "'");
            }
        }
        private bool eFatura
        {
            get
            {
                bool _type = false;
                try
                {
                    string sorgu = "select [E-Fatura] from [0D_70004_01_ALIM_FATURA_ONAY_LISTE] where COMPANY='" + _obj.Comp.ToString() + "' AND [Fatura No_]='" + currentinvoiceNo + "'";
                    if (f.tekbirsonucdonder(sorgu) == "1")
                        _type = true;
                    else
                        _type = false;
                }
                catch (Exception)
                {
                    _type = false;
                }
                return _type;
            }
        }

        private decimal limitY
        {
            get
            {
                string limitY = f.tekbirsonucdonder("select LIMITY from [0_W50306] where FTNO='" + currentinvoiceNo + "' and KULLANICI='" + _obj.UserName.ToString() + "' and COMPANY='" + _obj.Comp.ToString() + "'");
                int durum = 0;
                int sayi = 0;
                if (int.TryParse(limitY, out durum))
                {
                    sayi = durum;
                }
                else
                {
                    sayi = 0;
                }
                return Convert.ToDecimal(sayi);
            }
        }
        private invoiceTypes invoiceType
        {
            get
            {
                invoiceTypes _pageType = invoiceTypes.NONE;

                if (!string.IsNullOrEmpty(Request.QueryString["InvoiceType"]))
                {
                    if (Request.QueryString["InvoiceType"].ToString() == "SATINALMA")
                        _pageType = invoiceTypes.SATINALMA;
                    else if (Request.QueryString["InvoiceType"].ToString() == "SATIS_IADE")
                        _pageType = invoiceTypes.SATIS_IADE;
                    else if (Request.QueryString["InvoiceType"].ToString() == "ALIMDEKONT")
                        _pageType = invoiceTypes.ALIMDEKONT;
                    else
                        _pageType = invoiceTypes.NONE;
                }
                return _pageType;
            }
        }
        private string invoiceStatu
        {
            get
            {
                return Request.QueryString["FaturaStatu"].ToString();
            }
        }




        private string invoiceTypeText
        {
            get
            {
                string _temp = string.Empty;
                if (invoiceType == invoiceTypes.SATINALMA)
                {
                    _temp = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblFaturaTuruAlim");
                }
                else
                {
                    _temp = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblFaturaTuruiade");
                }
                return _temp;
            }
        }

        public bool dosyaVarmi { get; set; }
        public string DosyaAdi { get; set; }
        public string DosyaPath { get; set; }

        private string currentinvoiceNo
        {
            get
            {
                string _invNo = string.Empty;
                try
                {
                    //string[] _temp = Request.QueryString["invoiceNo"].ToString().Split('|');
                    //_invNo = _temp[0];
                    return Request.QueryString["invoiceNo"].ToString();
                }
                catch (Exception)
                {
                    _invNo = string.Empty;
                }
                return _invNo;
            }
        }
        private string currentJobNo
        {
            get
            {
                string _jobNo = string.Empty;
                try
                {
                    _jobNo = Request.QueryString["ProjeNo"].ToString();
                }
                catch (Exception)
                {
                    _jobNo = string.Empty;
                }
                return _jobNo;
            }
        }


        private renderTypes renderType
        {
            get
            {
                renderTypes _rendtype = renderTypes.Add;
                if (string.IsNullOrEmpty(Request.QueryString["invoiceNo"]))
                {
                    _rendtype = renderTypes.Add;
                }
                else
                {
                    _rendtype = renderTypes.Edit;
                }
                return _rendtype;
            }
        }

        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }


        #endregion

        private void DilGetir()
        {
            if (invoiceType == invoiceTypes.SATINALMA)
            {
                Box1.FindItemByFieldName("lblVendCust").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50010_J50019", "R0101");

            }
            else
            {
                Box1.FindItemByFieldName("lblVendCust").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50030_J50039", "R0101");

            }

            Box1.FindItemByFieldName("lblFaturaNo").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50010_J50019", "R0100");
            Box1.FindItemByFieldName("lblVendCustTaxNo").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50010_J50019", "R0103");
            Box1.FindItemByFieldName("lblReferansNo").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50010_J50019", "R0105");
            Box1.FindItemByFieldName("lblSerino").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50010_J50019", "R0106");
            Box1.FindItemByFieldName("lblDeftereNakilTarihi").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50010_J50019", "R0108");
            Box1.FindItemByFieldName("lblBelgeTarihi").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50010_J50019", "R0109");
            Box1.FindItemByFieldName("lblFirmaOdemeVadesi").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50010_J50019", "R0113");
            Box1.FindItemByFieldName("lblEfKaydi").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50010_J50019", "R0144");
            Box1.FindItemByFieldName("lblMusteriTems").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50010_J50019", "R0123");

            Box2.FindItemByFieldName("lblDovizCinsi").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50010_J50019", "R0133");
            Box2.FindItemByFieldName("lblMatrah").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50010_J50019", "R0135");
            Box2.FindItemByFieldName("lblKdv").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50010_J50019", "R0136");
            Box2.FindItemByFieldName("lblToplam").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50010_J50019", "R0137");
            Box2.FindItemByFieldName("lblMatrahTL").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50010_J50019", "R0138");
            Box2.FindItemByFieldName("lblKdvTL").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50010_J50019", "R0139");
            Box2.FindItemByFieldName("lblToplamtl").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50010_J50019", "R0140");
            Box2.FindItemByFieldName("lblKDV1818").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50010_J50019", "R0141");
            Box2.FindItemByFieldName("lblKurFarkiFatura").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50010_J50019", "R0131");

            Box3.FindItemByFieldName("lblAdress").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50010_J50019", "R0152");
            Box3.FindItemByFieldName("lblNot").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "P50010_J50019", "R0156");

            btnsiponayla.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satinalma", "btnsiponayla");
            btnsil.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satinalma", "btnsil");

            lblSilmeMessage.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblSilmeMessage");
            lblconfirmOnayaGonderMessage.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblconfirmOnayaGonderMessage");
            txtodemeVadesiMessage.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGirisOnay", "txtodemeVadesiMessage");
            btndosya.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "Dekont_satinalma", "btndosya");

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();


        }
        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }

            if (IsPostBack || IsCallback)
            { return; }
            PreparePageFields();
            PreparePage();

        }

        private void PreparePage()
        {
            if (renderType == renderTypes.Edit)
            {
                FillFormObjectsForEdit();


            }
        }

        private void FillFormObjectsForEdit()
        {
            string ProcName = string.Empty;


            if (invoiceStatu == "ALIMTAMAMLANDI")
                ProcName = "[SP_0_W51401]";
            else if (invoiceStatu == "ALIMTAMAMLANDITUM")
                ProcName = "[SP_0_W51402]";
            else if (invoiceStatu == "ALIMISLEMDE")
                ProcName = "[SP_0_W51411]";
            else if (invoiceStatu == "IADEISLEMDE")
                ProcName = "[SP_0_W51531]";
            else if (invoiceStatu == "IADETAMAMLANDI")
                ProcName = "[SP_0_W51521]";
            else if (invoiceStatu == "IADETAMAMLANDITUM")
                ProcName = "[SP_0_W51522]";
            else if (invoiceStatu == "DEKONTISLEMDE")
                ProcName = "[SP_0_W51451]";
            else if (invoiceStatu == "ALIMDEKONTTAMAMLANDI")
                ProcName = "[SP_0_W51441]";
            else if (invoiceStatu == "ALIMDEKONTTAMAMLANDITUM")
                ProcName = "[SP_0_W51442]";

            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                conn.Open();

                using (SqlCommand sorgu = new SqlCommand())
                {

                    sorgu.Connection = conn;
                    sorgu.CommandTimeout = 1000;
                    sorgu.CommandType = CommandType.StoredProcedure;
                    sorgu.CommandText = ProcName;
                    sorgu.Parameters.AddWithValue("@COMP", _obj.Comp.ToString());
                    sorgu.Parameters.AddWithValue("@USER", _obj.UserName.ToString());
                    sorgu.Parameters.AddWithValue("@YTKG", "");
                    sorgu.Parameters.AddWithValue("@YTK1", "");
                    sorgu.Parameters.AddWithValue("@YTK2", "");
                    sorgu.Parameters.AddWithValue("@YTK3", "");
                    sorgu.Parameters.AddWithValue("@YTK4", "");
                    sorgu.Parameters.AddWithValue("@YTK5", "");
                    sorgu.Parameters.AddWithValue("@PRM1", currentinvoiceNo);
                    sorgu.Parameters.AddWithValue("@PRM2", "");
                    sorgu.Parameters.AddWithValue("@PRM3", "");


                    SqlDataReader sdr = sorgu.ExecuteReader();
                    if (sdr.Read())
                    {
                        txtFaturaNo.Text = sdr["R0100"].ToString();
                        lblInvoiceNo.Text = currentinvoiceNo;
                        lblFaturaNo.Text = sdr["R0100"].ToString();
                        HiddenDocIsSaved.Value = "1";
                        cmbVendCust.Value = sdr["R0101"];
                        txtVergiDaire.Text = sdr["R0103"].ToString();
                        txtSaticiVergiDaireNo.Text = sdr["R0104"].ToString();
                        txtReferansNo.Text = sdr["R0105"].ToString();
                        txtSeriNo.Text = sdr["R0106"].ToString();
                        txtHariciBelgeNo.Text = sdr["R0107"].ToString();
                        txtDeftereNakilTarihi.Date = Convert.ToDateTime(sdr["R0108"].ToString());
                        txtBelgeTarihi.Date = Convert.ToDateTime(sdr["R0109"].ToString());
                        txtFirmaOdemeVadesi.Text = sdr["R0113"].ToString();
                        txtFirmaOdemeVadesiCode.Text = sdr["R0113"].ToString();
                        cmbFaturaVadesi.Value = sdr["R0111"].ToString();
                        if (Convert.ToInt32(sdr["R0144"]) > 0) IsEfKaydi.Checked = true;
                        txtMusteriTemsilcisi.Text = sdr["R0123"].ToString();
                        cmbDovizCinsi.Value = sdr["R0133"];
                        txtDovizKuru.Text = string.Format("{0:N4}", sdr["R0134"]);
                        txtMatrah.Text = string.Format("{0:N2}", sdr["R0135"]);
                        txtMatrahSatir.Text = string.Format("{0:N2}", sdr["R0158"]);
                        txtKdv.Text = string.Format("{0:N2}", sdr["R0136"]);
                        txtKdvSatir.Text = string.Format("{0:N2}", sdr["R0159"]);
                        txtToplam.Text = string.Format("{0:N2}", sdr["R0137"]);
                        txtToplamSatir.Text = string.Format("{0:N2}", sdr["R0160"]);
                        txtMatrahtl.Text = string.Format("{0:N2}", sdr["R0138"]);
                        txtMatrahtlSatir.Text = string.Format("{0:N2}", sdr["R0161"]);
                        txtKdvtl.Text = string.Format("{0:N2}", sdr["R0139"]);
                        txtKdvtlSatir.Text = string.Format("{0:N2}", sdr["R0162"]);
                        txtToplamtl.Text = string.Format("{0:N2}", sdr["R0140"]);
                        txtToplamtlSatir.Text = string.Format("{0:N2}", sdr["R0163"]);
                        txtKdv1.Text = string.Format("{0:N2}", sdr["R0141"]);
                        txtKdv8.Text = string.Format("{0:N2}", sdr["R0142"]);
                        txtKdv18.Text = string.Format("{0:N2}", sdr["R0143"]);
                        if (Convert.ToInt32(sdr["R0131"]) > 0) IsKurFarki.Checked = true;
                        txtAdress.Text = sdr["R0152"].ToString();
                        lblFtNo.Text = sdr["R0011"].ToString();
                        lblAfNo.Text = sdr["R0151"].ToString();
                        txtNot.Text = sdr["R0156"].ToString();
                        lblStatusDesc.Text = sdr["R0129"].ToString();


                        int lineno = 10000;
                        if (invoiceType == invoiceTypes.ALIMDEKONT)
                        {
                            txtDosyaLineNo.Text = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "DosyaSatirNoAl§0§" + txtFaturaNo.Text, _obj.Comp);
                            lineno = method.StringToInt(txtDosyaLineNo.Text) - 10000;
                            txtDosyaLineNo.Text = lineno.ToString();
                        }
                        else
                        {
                            txtDosyaLineNo.Text = "10000";
                        }





                        string dosyaKontrolPath = string.Empty;
                        if (invoiceType == invoiceTypes.SATINALMA | invoiceType == invoiceTypes.ALIMDEKONT)
                            dosyaKontrolPath = "~/" + CurrentFolderName(_obj.Comp) + "/Dosya/SatinAlma/" + txtFaturaNo.Text + lineno.ToString() + ".pdf";
                        else if (invoiceType == invoiceTypes.SATIS_IADE)
                            dosyaKontrolPath = "~/" + CurrentFolderName(_obj.Comp) + "/Dosya/IadeSatis/" + txtFaturaNo.Text + lineno.ToString() + ".pdf";

                        if (File.Exists(Server.MapPath(dosyaKontrolPath)))
                        {
                            btndosyagoster.Visible = true;
                            btndosyagoster.Text = txtFaturaNo.Text + lineno.ToString();

                        }

                        if (IsEfKaydi.Checked)
                        {
                            cmbVendCust.Enabled = false;
                            txtSeriNo.Enabled = false;
                            txtHariciBelgeNo.Enabled = false;
                            txtBelgeTarihi.Enabled = false;
                            txtDeftereNakilTarihi.Enabled = false;

                            cmbDovizCinsi.Enabled = false;
                        }
                        txtSayfaTurveNo.Text = sdr["R0100"].ToString() + "-" + invoiceType.ToString();



                        if (invoiceType == invoiceTypes.SATINALMA | invoiceType == invoiceTypes.SATIS_IADE)
                        {
                            if (sdr["RSTAT"].ToString() == "7")
                            {
                                txtDeftereNakilTarihi.Enabled = true;
                                btnAciklamaKaydet.Visible = false;
                                btnAktarPopupAc.Visible = false;
                                btnKdvKaydet.Visible = false;
                                btnReddet.Visible = false;
                                BtnOpenInvoiceSelector.Visible = false;
                                btnsil.Visible = false;
                                btnsiponayla.Visible = true;
                            }
                        }

                        ButtonPerms(sdr);
                    }

                    conn.Close();
                }
            }

            OnOnayFileCreate();
        }

        private void PreparePageFields()
        {
            txtFaturaNo.Text = currentinvoiceNo;

            DilGetir();

            //InitDataTable(DTVendCust.Table);
            FillVendCust();

            InitDataTable(DTCurrency.Table);
            FillCurrency(DTCurrency.Table);
            FillCurrencyKurFarki(DTCurrencyKurFarki.Table);

            InitDataTable(DTPaymentTerm.Table);
            FillPaymentTerms(DTPaymentTerm.Table);

            //InitDataTableNots(DTNots.Table);
            //FillNots(DTNots.Table);

            lblGId.Text = Request.QueryString["GId"];

        }

        void ButtonPerms(SqlDataReader sdr)
        {
            btnAciklamaKaydet.Visible = true;
            btnAktarPopupAc.Visible = false;
            btnKdvKaydet.Visible = false;
            btnReddet.Visible = false;
            BtnOpenInvoiceSelector.Visible = false;
            btnsil.Visible = false;
            btnsiponayla.Visible = false;
            btnDNTarihDegistir.Visible = false;



            try { btnAktarPopupAc.Visible = Convert.ToInt32(sdr["RB021"]) == 1 ? true : false; } catch { }
            try { btnKdvKaydet.Visible = Convert.ToInt32(sdr["RB023"]) == 1 ? true : false; } catch { }
            try { btnReddet.Visible = Convert.ToInt32(sdr["RB051"]) == 1 ? true : false; } catch { }
            try { BtnOpenInvoiceSelector.Visible = Convert.ToInt32(sdr["RB022"]) == 1 ? true : false; } catch { }
            try { btnsil.Visible = Convert.ToInt32(sdr["RB030"]) == 1 ? true : false; } catch { }
            try { btnDNTarihDegistir.Visible = Convert.ToInt32(sdr["RB055"]) == 1 ? true : false; } catch { }
            try { btnDeftereNaklet.Visible = Convert.ToInt32(sdr["RB055"]) == 1 ? true : false; } catch { }

            try
            {
                btnsiponayla.Visible = (Convert.ToInt32(sdr["RB040"]) == 1 | Convert.ToInt32(sdr["RB050"]) == 1) ? true : false;
                if (Convert.ToInt32(sdr["RB050"]) == 1)
                    btnsiponayla.Text = "ONAYLA";
            }
            catch { }


            if (_obj.Comp.ToString() == "STENA"
                | _obj.Comp.ToString() == "MOBILIS" | _obj.Comp.ToString() == "LAMKARA" | _obj.Comp.ToString() == "LUCENT")
                BtnOpenInvoiceSelector.Visible = false;

            if (invoiceType == invoiceTypes.SATINALMA | invoiceType == invoiceTypes.SATIS_IADE | btndosyagoster.Visible == true | invoiceStatu == "ALIMDEKONTTAMAMLANDI" | invoiceStatu == "ALIMDEKONTTAMAMLANDI")
                btndosya.Visible = false;





        }

        private void InitDataTableNots(DataTable dt)
        {
            dt.Columns.Add("notType", typeof(string));
            dt.Columns.Add("Tarih", typeof(DateTime));
            dt.Columns.Add("Saat", typeof(DateTime));
            dt.Columns.Add("Not", typeof(string));
            dt.Columns.Add("Kullanici", typeof(string));
        }

        private void FillNots(DataTable dt)
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select Tarih,Saat, case [Hareket Turu]  when 3 Then 'Red' else 'Not' End notType, Not1+' '+Not2 [Not], KullaniciAdi Kullanici from [vw_AlimOnayTarihce] where [Fatura No_]='" + currentinvoiceNo + "' and (Not1 <>'' or Not2<>'')  and COMPANY='" + _obj.Comp.ToString() + "' order by [Fatura No_], Tarih,Saat ";
                SqlCommand cmd = new SqlCommand(sorgu, conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                //gridNot.DataBind();
            }
        }

        private void openPDF()
        {
            if (HiddenDocIsSaved.Value == "1")
                ClientScript.RegisterStartupScript(this.Page.GetType(), "popupOpener", "var popup=window.open('" + CurrentFolderName(_obj.Comp) + "/Dosya/SatinAlma_Onay/" + currentinvoiceNo + "10000.pdf" + "');popup.focus();", true);
        }

        private void InitDataTable(DataTable dt)
        {
            dt.Columns.Add("Code", typeof(string));
            dt.Columns.Add("Text", typeof(string));
        }

        private void FillVendCust()
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select [No_] Code, Name Text, PAYMDESC from [0C_00018_00023_02_CUSTOMER_VENDOR]";
                switch (invoiceType)
                {
                    case invoiceTypes.SATINALMA:
                        sorgu += " where TYPE='VEND' and COMPANY='" + _obj.Comp.ToString() + "'";
                        break;
                    case invoiceTypes.ALIMDEKONT:
                        sorgu += " where TYPE='VEND' and COMPANY='" + _obj.Comp.ToString() + "'";
                        break;
                    case invoiceTypes.SATIS_IADE:
                        sorgu += " where TYPE='CUST' and COMPANY='" + _obj.Comp.ToString() + "'";
                        break;
                }
                cmbVendCust.Items.Clear();
                DSVendCust.SelectCommand = sorgu;
                cmbVendCust.DataBind();
            }
        }

        private void FillCurrency(DataTable dt)
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select KOD Code, ACIKLAMA Text From [0_W10400] WHERE COMPANY='" + _obj.Comp.ToString() + "'";
                SqlCommand cmd = new SqlCommand(sorgu, conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                cmbDovizCinsi.DataBind();
            }
        }

        private void FillPaymentTerms(DataTable dt)
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select Code, Description Text from [0C_00003_00_PAYMENT_TERMS] where COMPANY='" + _obj.Comp.ToString() + "'";
                SqlCommand cmd = new SqlCommand(sorgu, conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                cmbFaturaVadesi.DataBind();
                cmbKurFarkiDovizCinsi.DataBind();
            }
        }

        private void FillOnayDepartmani(DataTable dt)
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select Code, Name Text from [0C_00349_00_DIMENSION VALUES] where Blocked=0 and [Dimension Code]='DEPARTMAN' and COMPANY='" + _obj.Comp.ToString() + "'";
                SqlCommand cmd = new SqlCommand(sorgu, conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                cmbFaturaVadesi.DataBind();
            }
        }
        private void FillJobTypes(DataTable dt)
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select [LineNo] Code, Adi Text from [0C_50055_13_LOOKUPS_AFO_JOB_TUR] where Tur='Job Tur' and COMPANY='" + _obj.Comp.ToString() + "'";
                SqlCommand cmd = new SqlCommand(sorgu, conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                cmbFaturaVadesi.DataBind();
            }
        }

        private void FillCurrencyKurFarki(DataTable dt)
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select KOD Code, ACIKLAMA Text From [0_W10400] WHERE ACIKLAMA!='TL' and COMPANY='" + _obj.Comp.ToString() + "'";
                SqlCommand cmd = new SqlCommand(sorgu, conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                cmbDovizCinsi.DataBind();
            }
        }

        private void FillJobNo(DataTable dt)
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select [No_] Code ,[GAC Proje No] Text from [0C_00167_02_JOB] where COMPANY='" + _obj.Comp.ToString() + "'";
                SqlCommand cmd = new SqlCommand(sorgu, conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }
        }
        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        protected void callbackPanelDoviz_Callback(object sender, CallbackEventArgsBase e)
        {
            txtDovizKuru.Text = f.returnKur(txtDeftereNakilTarihi.Text, cmbDovizCinsi.SelectedItem.Value.ToString(), _obj.Comp);

        }

        protected void cmbVendCust_SelectedIndexChanged(object sender, EventArgs e)
        {
            //txtSaticiVergiDaireNo.Text = f.tekbirsonucdonder("Select [Tax Registration No_] as vergino from [0C_00023_00_VENDOR] where COMPANY = '" + _obj.Comp.ToString() + "' and No_ = '" + cmbVendCust.Value.ToString() + "'");
            if (f.vendororcustomercontrol(_obj.Comp.ToString(), 1, cmbVendCust.Value.ToString()))
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satinalma", "msg367"));
                cmbVendCust.SelectedIndex = -1;
            }
        }

        protected void btnAciklamaKaydet_Click(object sender, EventArgs e)
        {
            if (txtAciklama.Text != string.Empty)
            {
                string[] _params = {currentinvoiceNo, _obj.UserName.ToString(), "2", txtAciklama.Text,
                   "", "0", limitY.ToString() };
                string message = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatınalmaOnayDurumDegistir§" + string.Join("§", _params), _obj.Comp);
                string msg = string.Empty;
                if (message != "1")
                {

                    msg = message;

                }

                if (!string.IsNullOrEmpty(msg))
                {
                    if (!ClientScript.IsStartupScriptRegistered("JSFaturaAc"))
                    {
                        ClientScript.RegisterStartupScript(GetType(), "JSFaturaAc", @"alert('" + msg.Replace("'", "") + "');parent.RefreshAllCollapse();", true);
                    }
                }
                else
                {
                    if (!ClientScript.IsStartupScriptRegistered("JSFaturaAc"))
                    {
                        ClientScript.RegisterStartupScript(GetType(), "JSFaturaAc", @"parent.RefreshAllCollapse();", true);
                    }
                }
            }
        }

        protected void btnDNTarihDegistir_Click(object sender, EventArgs e)
        {
            string[] _params = { txtFaturaNo.Text, _obj.UserName.ToString(), method.DateToStringNAVType(txtDeftereNakilTarihi.Text) };

            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "DNFaturaNakilTarihiDegistir§" + string.Join("§", _params), _obj.Comp);
            if (!ClientScript.IsStartupScriptRegistered("JSFaturaAc"))
            {
                ClientScript.RegisterStartupScript(GetType(), "JSFaturaAc", @"alert('Nakil Tarihi değiştirildi.');parent.RefreshAllCollapse();", true);
            }
        }

        protected void IsKurFarki_CheckedChanged(object sender, EventArgs e)
        {
            if (IsKurFarki.Checked)
            {
                cmbKurFarkiDovizCinsi.Visible = true;
            }
            else
            {
                cmbKurFarkiDovizCinsi.Visible = false;
            }
        }


        #region Eski Satınalmada Gelenler
        protected void btnKdvKaydet_Click(object sender, EventArgs e)
        {
            decimal kdv1 = 0, kdv8 = 0, kdv18 = 0;

            try { kdv1 = Convert.ToDecimal(txtKdv1.Text); } catch { }
            try { kdv8 = Convert.ToDecimal(txtKdv8.Text); } catch { }
            try { kdv18 = Convert.ToDecimal(txtKdv18.Text); } catch { }

            if (invoiceType == invoiceTypes.SATINALMA)
            {
                string[] _params = {txtFaturaNo.Text, kdv1.ToString(), kdv8.ToString(), kdv18.ToString()
                    , IsKurFarki.Checked ? "1" : "0", cmbKurFarkiDovizCinsi.Value != null ? cmbKurFarkiDovizCinsi.Value.ToString() : ""};

                GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatinalmaDuzenle§" + string.Join("§", _params), _obj.Comp);
            }
            else if (invoiceType == invoiceTypes.SATIS_IADE)
            {
                string[] _params = {txtFaturaNo.Text, kdv1.ToString(), kdv8.ToString(), kdv18.ToString()
                    , IsKurFarki.Checked ? "1" : "0", cmbKurFarkiDovizCinsi.Value != null ? cmbKurFarkiDovizCinsi.Value.ToString() : ""};

                GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "IadeSatisDuzenle§" + string.Join("§", _params), _obj.Comp);
            }


            if (!ClientScript.IsStartupScriptRegistered("JSFaturaAc"))
            {
                ClientScript.RegisterStartupScript(GetType(), "JSFaturaAc", @"alert('Kayıt işlemi başarılı.');parent.RefreshAllCollapse();", true);
            }
        }

        void CreateThumbnailsFromPDF(string inputfilename, string outputfilename)
        {
            using (PdfDocumentProcessor processor = new PdfDocumentProcessor())
            {
                processor.LoadDocument(inputfilename);

                int _count = 1;
                string FileNWithoutExt = outputfilename.Replace("10000.png", string.Empty);
                foreach (DevExpress.Pdf.PdfPage _page in processor.Document.Pages)
                {
                    if (_count == 1)
                    {
                        using (Bitmap pageBitmap = processor.CreateBitmap(_count, 1800))
                        {
                            try { if (File.Exists(outputfilename)) File.Delete(outputfilename); } catch { }
                            pageBitmap.Save(outputfilename);
                        }
                    }
                    else
                    {
                        using (Bitmap pageBitmap = processor.CreateBitmap(_count, 1800))
                        {
                            try { if (File.Exists(FileNWithoutExt + (_count * 10000).ToString() + ".png")) File.Delete(FileNWithoutExt + (_count * 10000).ToString() + ".png"); } catch { }
                            pageBitmap.Save(FileNWithoutExt + (_count * 10000).ToString() + ".png");
                        }
                    }
                    _count++;
                }


                string[] _params = { Request.QueryString["BelgeNo"], (_count - 1).ToString() };
                GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "DosyaSayisiUpdate§" + string.Join("§", _params), _obj.Comp);


            }

        }

        protected void btnsiponayla_Click(object sender, EventArgs e)
        {
            if (invoiceType == invoiceTypes.SATINALMA || invoiceType == invoiceTypes.SATIS_IADE)
            {
                string efaturami = f.tekbirsonucdonder("select [E-Fatura] from [0D_70004_00_ALIM_FATURA_ONAY_LISTE] where COMPANY='" + _obj.Comp.ToString() + "' and [Harici Belge No_]='" + txtHariciBelgeNo.Text + "'");

                if (txtFaturaNo.Text != string.Empty && (txtSeriNo.Text != string.Empty || Convert.ToInt32(efaturami) > 0))
                {
                    if (btndosyagoster.Visible == true)
                    {
                        string foldPath = "~/" + CurrentFolderName(_obj.Comp) + "/Dosya/SatinAlma_Onay/images/" + lblFtNo.Text + "10000.png";
                        if (!File.Exists(Server.MapPath(foldPath)))
                        {
                            string _inputfilename = Server.MapPath("~/" + CurrentFolderName(_obj.Comp)) + "\\Dosya\\SatinAlma_Onay\\" + lblFtNo.Text + "10000.pdf";
                            string _outputfilename = Server.MapPath("~/" + CurrentFolderName(_obj.Comp)) + "\\Dosya\\SatinAlma_Onay\\images\\" + lblFtNo.Text + "10000.png";
                            CreateThumbnailsFromPDF(_inputfilename, _outputfilename);
                            if (!File.Exists(Server.MapPath(foldPath)))
                            {
                                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satinalma", "msg400"));
                                return;
                            }
                        }

                        string limitKontrolMessage = string.Empty;
                        if (invoiceType == invoiceTypes.SATINALMA)
                        {
                            string[] _params = { txtFaturaNo.Text, _obj.UserName.ToString(), "" };
                            limitKontrolMessage = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatınalmaFaturaLimitKontrol§" + string.Join("§", _params), _obj.Comp);
                        }

                        else if (invoiceType == invoiceTypes.SATIS_IADE)
                        {
                            string[] _params = { txtFaturaNo.Text, _obj.UserName.ToString(), "" };
                            limitKontrolMessage = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatisIadeFaturaLimitKontrol§" + string.Join("§", _params), _obj.Comp);
                        }

                        string msg = string.Empty;
                        if (limitKontrolMessage != "1")
                        {
                            // MessageBox(limitKontrolMessage);
                            msg = limitKontrolMessage;

                        }

                        string dnaf = string.Empty;

                        if (msg.Contains("|"))
                        {
                            dnaf = msg.Replace("|", string.Empty);
                            msg = "Fatura Onaylandı.";
                        }

                        StringBuilder sb = new StringBuilder();

                        if (!string.IsNullOrEmpty(dnaf))
                        {
                            OnOnayFileCreate();

                            sb = new StringBuilder();
                            sb.Append("alert('" + msg.Replace("'", "") + "');");
                            sb.Append("parent.RefreshAllCollapse();");

                            if (!ClientScript.IsStartupScriptRegistered("JSFaturaAc"))
                                ClientScript.RegisterStartupScript(GetType(), "JSFaturaAc", sb.ToString(), true);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(msg))
                            {
                                if (!ClientScript.IsStartupScriptRegistered("JSFaturaMsg"))
                                    ClientScript.RegisterStartupScript(GetType(), "JSFaturaMsg", "alert('" + msg.Replace("'", "") + "');window.location.href=window.location.href;", true);
                            }

                        }


                    }
                    else
                    {
                        MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satinalma", "msg365"));
                    }

                }
                else
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satinalma", "msg366"));
                }
            }
            else if (invoiceType == invoiceTypes.ALIMDEKONT)
            {
                if (btndosyagoster.Visible == true)
                {
                    string[] _params = { txtHariciBelgeNo.Text, cmbVendCust.Value.ToString(), "1" };
                    string drm = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatinAlmaHariciKontrol§" + string.Join("§", _params), _obj.Comp);

                    if (drm == "True")
                    {
                        string[] _params2 = { txtFaturaNo.Text, _obj.NameSurName.ToString() };
                        GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatinalmaSipOnayla§" + string.Join("§", _params2), _obj.Comp);

                        StringBuilder sb = new StringBuilder();
                        sb.Append("alert('Fatura Onaylandı.');");
                        sb.Append("parent.RefreshAllCollapse();");
                        if (!ClientScript.IsStartupScriptRegistered("JSOnaylandi"))
                        {
                            ClientScript.RegisterStartupScript(GetType(), "JSOnaylandi", sb.ToString(), true);
                        }
                    }
                    else
                    {
                        MessageBox(txtHariciBelgeNo.Text + l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "Dekont_satinalma", "msg062"));
                    }
                }
                else
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satinalma", "msg365"));
                }
            }

        }

        private void OnOnayFileCreate()
        {
            if (lblStatusDesc.Text == "Tamamlandı") return;
            string _FatNo = string.Empty, _AfNo = string.Empty;

            if (invoiceType == invoiceTypes.SATINALMA || invoiceType == invoiceTypes.SATIS_IADE)
            {
                string reportPath = f.GetReportPath("ALIM ONAY TARIHCE.rpt", _obj.Comp);
                using (ReportDocument crystalReport = new ReportDocument())
                {
                    if (lblStatusDesc.Text == "Tamamlandı")
                    {
                        _FatNo = lblFtNo.Text;
                        _AfNo = lblAfNo.Text;
                    }
                    else
                    {
                        _FatNo = currentinvoiceNo;
                        _AfNo = txtFaturaNo.Text;
                    }

                    if (string.IsNullOrEmpty(_FatNo)) return;
                    if (_FatNo.Substring(0, 2).ToUpper() != "FT") return;

                    crystalReport.Load(reportPath);
                    crystalReport.ReportOptions.EnableSaveDataWithReport = false;
                    crystalReport.SetParameterValue("@COMP", _obj.Comp.ToString());

                    crystalReport.SetParameterValue("@FATURA", _FatNo);

                    string folder = string.Empty;
                    if (invoiceType == invoiceTypes.SATINALMA | invoiceType == invoiceTypes.ALIMDEKONT)
                    {
                        folder = "SatinAlma";
                    }
                    else
                    {
                        folder = "IadeSatis";
                    }

                    string foldPath = "~/" + CurrentFolderName(_obj.Comp) + "/Dosya/" + folder + "/" + _AfNo + "10000.pdf";
                    if (File.Exists(Server.MapPath(foldPath)))
                    {
                        File.Delete(Server.MapPath(foldPath));
                    }

                    crystalReport.ExportToDisk(ExportFormatType.PortableDocFormat, Server.MapPath(foldPath));


                }
            }
        }
        protected void btndosyagoster_Click(object sender, EventArgs e)
        {
            OnOnayFileCreate();
            string path = "";
            if (invoiceType == invoiceTypes.SATINALMA | invoiceType == invoiceTypes.ALIMDEKONT)
            {
                path = "/" + CurrentFolderName(_obj.Comp) + "/Dosya/SatinAlma/" + txtFaturaNo.Text + txtDosyaLineNo.Text + ".pdf";
            }
            else
            {
                path = "/" + CurrentFolderName(_obj.Comp) + "/Dosya/IadeSatis/" + txtFaturaNo.Text + txtDosyaLineNo.Text + ".pdf";
            }
            path = path.Replace("..", "");
            path = path.Replace("\\", "\\\\");
            if ((Request.Url.ToString()).Contains(ws.dısip))
            {
                Response.Write("<script>window.open('" + ws.dısip + path + "','List','scrollbars=no,resizable=no,width=1000,height=460,top=0,left=0');</script>");
            }
            else
            {
                Response.Write("<script>window.open('" + ws.icip + path + "','List','scrollbars=no,resizable=no,width=1000,height=460,top=0,left=0');</script>");
            }

        }
        protected void btnsil_Click(object sender, EventArgs e)
        {
            if (txtFaturaNo.Text != string.Empty)
            {
                if (invoiceType == invoiceTypes.SATINALMA)
                {
                    string[] _params = { txtFaturaNo.Text, _obj.NameSurName.ToString() };
                    string drm = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatinAlmaSil§" + string.Join("§", _params), _obj.Comp);
                }
                else if (invoiceType == invoiceTypes.ALIMDEKONT)
                {
                    string[] _params = { txtFaturaNo.Text, _obj.NameSurName.ToString() };
                    string drm = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatinAlmaSil§" + string.Join("§", _params), _obj.Comp);
                }
                else if (invoiceType == invoiceTypes.SATIS_IADE)
                {
                    string[] _params = { txtFaturaNo.Text, _obj.NameSurName.ToString() };
                    string drm = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "IadeSatisSil§" + string.Join("§", _params), _obj.Comp);
                }
                if (!ClientScript.IsStartupScriptRegistered("JSFaturaAc"))
                {
                    ClientScript.RegisterStartupScript(GetType(), "JSFaturaAc", @"parent.RefreshAllCollapse();", true);
                }
            }
            else
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satinalma", "msg377"));
            }
        }

        protected void BtnOpenInvoiceSelector_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cmbVendCust.Text))
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satinalma", "msg106"));
                return;
            }
            if (!ClientScript.IsStartupScriptRegistered("JSInvoiceSelector"))
            {
                if (invoiceType == invoiceTypes.SATINALMA)
                    ClientScript.RegisterStartupScript(GetType(), "JSInvoiceSelector", @"window.open('../LogisticInvoiceSelector.aspx?CustomerNo=" + cmbVendCust.Value.ToString() + "&CurrencyCode=" + cmbDovizCinsi.Value.ToString() + "&SipNo=" + txtFaturaNo.Text + "&Page=1&UserName=" + _obj.UserName.ToString() + "&Date=" + txtDeftereNakilTarihi.Text + "&Currency=" + txtDovizKuru.Text + "&VendInvNo=" + txtHariciBelgeNo.Text + "&InvoiceNo=" + lblInvoiceNo.Text + "&GId=" + Request.QueryString["GId"] + "','List','scrollbars=yes,resizable=no,width=900,height=500,top=100,left=50');", true);
                else if (invoiceType == invoiceTypes.SATIS_IADE)
                    ClientScript.RegisterStartupScript(GetType(), "JSInvoiceSelector", @"window.open('../LogisticInvoiceSelector.aspx?CustomerNo=" + cmbVendCust.Value.ToString() + "&CurrencyCode=" + cmbDovizCinsi.Value.ToString() + "&SipNo=" + txtFaturaNo.Text + "&Page=4&UserName=" + _obj.UserName.ToString() + "&Date=" + txtDeftereNakilTarihi.Text + "&Currency=" + txtDovizKuru.Text + "&VendInvNo=" + txtHariciBelgeNo.Text + "&InvoiceNo=" + lblInvoiceNo.Text + "&GId=" + Request.QueryString["GId"] + "','List','scrollbars=yes,resizable=no,width=900,height=500,top=100,left=50');", true);
            }
        }

        protected void btndosya_Click(object sender, EventArgs e)
        {
            if (!ClientScript.IsStartupScriptRegistered("JsDosyaAc"))
            {
                if (invoiceType == invoiceTypes.ALIMDEKONT)
                    ClientScript.RegisterStartupScript(GetType(), "JsDosyaAc", @"window.open('../dosyabagla.aspx?BelgeNo=" + txtFaturaNo.Text + "&tur=0" + "&dtur=11&GId=" + Request.QueryString["GId"] + "','List','scrollbars=no,resizable=no,width=650,height=460,top=200,left=400');", true);
            }
        }

        #endregion

        protected void btnDeftereNaklet_Click(object sender, EventArgs e)
        {
            if (invoiceType == invoiceTypes.SATINALMA | invoiceType == invoiceTypes.SATIS_IADE)
            {
                string efaturami = f.tekbirsonucdonder("select [E-Fatura] from [0D_70004_00_ALIM_FATURA_ONAY_LISTE] where COMPANY='" + _obj.Comp.ToString() + "' and [Harici Belge No_]='" + txtHariciBelgeNo.Text + "'");

                if (txtFaturaNo.Text != string.Empty && (txtSeriNo.Text != string.Empty || Convert.ToInt32(efaturami) > 0))
                {
                    if (btndosyagoster.Visible == true)
                    {
                        if (invoiceType == invoiceTypes.SATINALMA)
                        {
                            string[] _params = { txtFaturaNo.Text, _obj.UserName.ToString() };

                            string drm = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatinalmaSipOnayla§" + string.Join("§", _params), _obj.Comp);

                            if (!ClientScript.IsStartupScriptRegistered("JSFaturaMsg"))
                                ClientScript.RegisterStartupScript(GetType(), "JSFaturaMsg", "alert('Fatura Deftere Nakledildi.');window.parent.location.href=window.parent.location.href;", true);

                        }
                        else if (invoiceType == invoiceTypes.SATIS_IADE)
                        {
                            string[] _params = { txtFaturaNo.Text, txtHariciBelgeNo.Text ,method.DateToStringNAVType(txtBelgeTarihi.Text),
                                method.StringToDecimal(txtDovizKuru.Text).ToString() ,cmbDovizCinsi.Value != null ? cmbDovizCinsi.Value.ToString() : ""
                                ,_obj.UserName.ToString() };

                            string drm = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "IadeSatisOnayla§" + string.Join("§", _params), _obj.Comp);

                            if (drm.ToLower().Contains("ok|"))
                            {
                                if (!ClientScript.IsStartupScriptRegistered("JSFaturaMsg"))
                                    ClientScript.RegisterStartupScript(GetType(), "JSFaturaMsg", "alert('Fatura Deftere Nakledildi.');window.parent.location.href=window.parent.location.href;", true);
                            }
                            else
                            {
                                if (!ClientScript.IsStartupScriptRegistered("JSFaturaMsg"))
                                    ClientScript.RegisterStartupScript(GetType(), "JSFaturaMsg", "alert('" + drm.Replace("'", "") + "');", true);

                            }
                        }





                    }
                    else
                    {
                        MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satinalma", "msg365"));
                    }

                }
                else
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "satinalma", "msg366"));
                }
            }
        }
    }
}