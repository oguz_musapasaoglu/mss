﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PopupCreateInv.aspx.cs" Inherits="MSS1.FaturaCard.PopupCreateInv" %>

<%@ Register Src="~/controls/DataTableControlGAC.ascx" TagName="StDataTable" TagPrefix="STDT" %>
<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../css/GAC_MenuStyle.css?v=47" rel="stylesheet" />
    <link href="../css/GAC_NewStyle.css?v=47" rel="stylesheet" />
    <link href="../css/GAC_CardStyle.css?v=47" rel="stylesheet" />
    <style>
        .dxflGroup {
            padding: 0px 5px;
            width: 100%;
        }

            .dxflGroup tr:first-child > .dxflGroupCell > .dxflGroupBox, .dxflGroup > .dxflChildInFirstRowSys > .dxflGroupCell > .dxflGroupBox {
                margin-top: -5px;
            }

        .dx-vam {
            color: white;
        }
    </style>
    <script type="text/javascript" lang="javascript">

</script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%">
            <table style="width: 500px; border-spacing: 0; border-collapse: collapse;">
                <tr>
                    <td style="width: 100px">
                        <asp:Label ID="lblFirmaSec" runat="server" Text="FİRMA SEÇ" CssClass="captionStyle"></asp:Label></td>
                    <td style="width: 400px">
                        <dx:ASPxComboBox ID="cmbVendCust" DataSourceID="DSVendCust" TextField="Text" ValueField="Code" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt"
                            ClientInstanceName="cmbVendCust" runat="server" CssClass="dropdownStyle" Width="100%">
                        </dx:ASPxComboBox>
                    </td>
                </tr>
                 <tr>
                    <td style="width: 100px">
                        <asp:Label ID="lblTaskSec" runat="server" Text="TASK SEÇ" CssClass="captionStyle"></asp:Label></td>
                    <td style="width: 400px">
                        <dx:ASPxComboBox ID="cmbTask" DataSourceID="DSTask" TextField="Text" ValueField="Code" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt"
                            ClientInstanceName="cmbTask" runat="server" CssClass="dropdownStyle" Width="100%">
                        </dx:ASPxComboBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        <asp:Label ID="lblDovizCek" runat="server" Text="DÖVİZ SEÇ" CssClass="captionStyle"></asp:Label></td>
                    <td style="width: 400px">
                        <dx:ASPxComboBox ID="cmbDovizCinsi" runat="server" CssClass="dropdownStyle" Width="100%" DataSourceID="DTCurrency"
                            ValueField="Code" TextField="Text" ClientInstanceName="cmbDovizCinsi" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt"
                            AutoPostBack="false">
                        </dx:ASPxComboBox>
                    </td>
                </tr>
                  <tr>
                    <td style="width: 100px">
                        <asp:Label ID="lblHariciB" runat="server" Text="HARİCİ BELGE NO" CssClass="captionStyle"></asp:Label></td>
                    <td style="width: 400px">
                       <dx:ASPxTextBox ID="txtHariciBelgeNo" runat="server" ForeColor="Black" CssClass="textboxStyle" Width="100%"></dx:ASPxTextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        <asp:Label ID="lblTarih" runat="server" Text="TARİH SEÇ" CssClass="captionStyle"></asp:Label></td>
                    <td style="width: 400px">
                        <dx:ASPxDateEdit ID="txtBelgeTarihi" ClientInstanceName="txtBelgeTarihi" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="7pt" EditFormatString="dd.MM.yyyy" DisplayFormatString="{0:dd.MM.yyyy}" runat="server" CssClass="DropDownList" Width="100%">
                        </dx:ASPxDateEdit>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        <asp:Button runat="server" ID="btnCreateInv" Text="FATURA OLUŞTUR" CssClass="formglobalbuttonStyleFAT" OnClick="btnCreateInv_Click" /></td>
                    <td style="width: 400px"></td>
                </tr>
            </table>
            <div style="display: none">
                <asp:SqlDataSource ID="DSVendCust" runat="server" ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
                 <asp:SqlDataSource ID="DSTask" runat="server" ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
                <STDT:StDataTable ID="DTCurrency" runat="server" />
            </div>
        </div>
    </form>
</body>
</html>
