﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.WSGeneric;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using DevExpress.Web.ASPxSpreadsheet;
using DevExpress.Web.Office;
using DevExpress.Spreadsheet;
using DevExpress.Spreadsheet.Export;
using MSS1.Codes;


namespace MSS1.FaturaCard
{
    public partial class PopupExcel : Bases
    {

        string ConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        public ws_baglantı ws = new ws_baglantı();
        public Methods method = new Methods();
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        SessionBase _obj;
        static SessionBase _objStatic;
        private string FaturaNo
        {
            get
            {
                return Request.QueryString["FaturaNo"].ToString();
            }
        }

        private string InvoiceNo
        {
            get
            {
                return Request.QueryString["InvoiceNo"].ToString();
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();


        }
        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());
            _objStatic = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }

            if (IsPostBack || IsCallback)
            { return; }


        }

        public static string dbName
        {
            get
            {
                string _res = string.Empty;

                switch (_objStatic.Comp.ToString())
                {
                    case "LAMKARA":
                        _res = "LAMKARA.dbo.";
                        break;
                    case "MOBILIS":
                        _res = "LAMKARA.dbo.";
                        break;
                    case "STENA":
                        _res = "LAMKARA.dbo.";
                        break;
                    case "LUCENT":
                        _res = "LAMKARA.dbo.";
                        break;
                    case "LIMANSAHA":
                        _res = "LAMKARA.dbo.";
                        break;
                    case "MLH":
                    case "MLHEG":
                    case "MLHFR":
                    case "MLHGE":
                    case "MLHUA":
                        _res = "MLH.dbo.";
                        break;
                    case "GAC":
                        _res = "";
                        break;
                    case "LAM":
                        _res = "";
                        break;
                    case "SINOTRANS":
                        _res = "SINOTRANS.dbo.";
                        break;
                    case "LAMLOJ":
                    case "LAMLOJEG":
                    case "LAMLOJFR":
                    case "LAMLOJGE":
                    case "LAMLOJUA":
                    case "LAMLOJUS":
                        _res = "LAMLOJ.dbo.";
                        break;
                }
                return _res;
            }
        }


        string HataMesajıjobNo = "";
        string HataMesajıRakam = "";
        string HataMesajıGiderKodu = "";
        string HataMesajıBoyut = "";
        protected void btnAktar_Click(object sender, EventArgs e)
        {

            Worksheet Ws = Spreadsheet.Document.Worksheets[0];
            Range range = Ws.GetDataRange();
            DataTable dt = Ws.CreateDataTable(range, true);
            DataTableExporter exp = Ws.CreateDataTableExporter(range, dt, true);
            exp.Export();

            DataKontrol(dt);
            InvoiceLineImport(dt);

            string[] _params = { FaturaNo, _obj.UserName.ToString() };
            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatinalmaSatirlariAktarim§" + string.Join("§", _params), _obj.Comp);
            //Response.Redirect(Request.RawUrl);

            if (!ClientScript.IsStartupScriptRegistered("JSFaturaAc"))
            {
                ClientScript.RegisterStartupScript(GetType(), "JSFaturaAc", @"window.opener.parent.RefreshAllCollapse();window.close();", true);
            }

        }

        private void DataKontrol(DataTable dt)
        {
            JobJobTaskCheck(dt);
            RakamKontrol(dt);
            BoyutKontrol(dt);
            GiderKodKontrol(dt);
            string mesaj = "";
            if (HataMesajıjobNo.Length > 0)
                mesaj += HataMesajıjobNo + "\r\n";
            if (HataMesajıRakam.Length > 0)
                mesaj += HataMesajıRakam + "\r\n";
            if (HataMesajıBoyut.Length > 0)
                mesaj += HataMesajıBoyut + "\r\n";
            if (HataMesajıGiderKodu.Length > 0)
                mesaj += HataMesajıGiderKodu + "\r\n";
            if (mesaj.Length > 0)
            {
                throw new Exception(mesaj);
            }
        }

        private void GiderKodKontrol(DataTable dt)
        {

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (f.tekbirsonucdonder("select count(*) from [0C_00156_00_RESOURCE] where No_='" + dt.Rows[i]["GİDER KODU"].ToString() + "'") == "0")
                {
                    HataMesajıGiderKodu += (i + 2).ToString() + ",";
                }
            }

            if (HataMesajıGiderKodu.Length > 0)
                HataMesajıGiderKodu = HataMesajıGiderKodu.Remove(HataMesajıGiderKodu.Length - 1, 1) + ". satır/satırlardaki GİDER KODU alanlarını kontrol edin.";

        }

        private void BoyutKontrol(DataTable dt)
        {

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (f.tekbirsonucdonder("select count(*) from [0C_00349_00_DIMENSION VALUES] where COMPANY='" + _obj.Comp.ToString() + "'  and Code= '" + dt.Rows[i]["FAALİYET BOYUT KODU"].ToString() + "'") == "0" & dt.Rows[i]["FAALİYET BOYUT KODU"].ToString() != "")
                {
                    HataMesajıBoyut += (i + 2).ToString() + ",";
                }

                if (f.tekbirsonucdonder("select count(*) from [0C_00349_00_DIMENSION VALUES] where COMPANY='" + _obj.Comp.ToString() + "'  and Code= '" + dt.Rows[i]["BÖLGE BOYUT KODU"].ToString() + "'") == "0" & dt.Rows[i]["BÖLGE BOYUT KODU"].ToString() != "")
                {
                    HataMesajıBoyut += (i + 2).ToString() + ",";
                }

                if (f.tekbirsonucdonder("select count(*) from [0C_00349_00_DIMENSION VALUES] where COMPANY='" + _obj.Comp.ToString() + "'  and Code= '" + dt.Rows[i]["DEPARTMAN BOYUT KODU"].ToString() + "'") == "0" & dt.Rows[i]["DEPARTMAN BOYUT KODU"].ToString() != "")
                {
                    HataMesajıBoyut += (i + 2).ToString() + ",";
                }

                if (f.tekbirsonucdonder("select count(*) from [0C_00349_00_DIMENSION VALUES] where COMPANY='" + _obj.Comp.ToString() + "'  and Code= '" + dt.Rows[i]["PERSONEL BOYUT KODU"].ToString() + "'") == "0" & dt.Rows[i]["PERSONEL BOYUT KODU"].ToString() != "")
                {
                    HataMesajıBoyut += (i + 2).ToString() + ",";
                }

                if (f.tekbirsonucdonder("select count(*) from [0C_00349_00_DIMENSION VALUES] where COMPANY='" + _obj.Comp.ToString() + "'  and Code= '" + dt.Rows[i]["ARAÇ PLAKA BOYUT KODU"].ToString() + "'") == "0" & dt.Rows[i]["ARAÇ PLAKA BOYUT KODU"].ToString() != "")
                {
                    HataMesajıBoyut += (i + 2).ToString() + ",";
                }
            }

            if (HataMesajıBoyut.Length > 0)
                HataMesajıBoyut = HataMesajıBoyut.Remove(HataMesajıBoyut.Length - 1, 1) + ". satır/satırlardaki boyutlarda hatalar mevcut.Excel'i düzenleyin\r\n";




            foreach (DataRow dr in dt.Rows)
            {


                // JOB VE JOB TASK Numaralarına göre sorgulama yapıp boyutlar doluyor.
                string Genelis = f.tekbirsonucdonder("SELECT [Genel Is] FROM  [0C_00167_00_JOB] where COMPANY='" + _obj.Comp.ToString() + "'  and No_='" + dr["İŞ NO"].ToString() + "'");

                if (Genelis == "0")
                {
                    string boyutlar = f.tekbirsonucdonder("select BLG+'-'+DEPT+'-'+FAAL from [0C_01001_01_JOB_TASK] where COMPANY='" + _obj.Comp.ToString() + "'  and [Job No_]='" + dr["İŞ NO"].ToString() + "' and [Job Task No_]='" + dr["İŞ GÖREV NO"].ToString() + "' ");

                    if (boyutlar != "NULL")
                    {
                        string[] _boyutlar = boyutlar.Split('-');
                        dr["BÖLGE BOYUT KODU"] = _boyutlar[0].ToString();
                        dr["DEPARTMAN BOYUT KODU"] = _boyutlar[1].ToString();
                        dr["FAALİYET BOYUT KODU"] = _boyutlar[2].ToString();

                        if (dr["BÖLGE BOYUT KODU"].ToString() == "" | dr["DEPARTMAN BOYUT KODU"].ToString() == "" | dr["FAALİYET BOYUT KODU"].ToString() == "")
                        {
                            HataMesajıBoyut = dr["İŞ NO"].ToString() + " - " + dr["İŞ GÖREV NO"].ToString() + "boyut değerleri girilmemiş.\r\n";
                        }
                    }
                    else
                    {
                        HataMesajıBoyut = dr["İŞ NO"].ToString() + " - " + dr["İŞ GÖREV NO"].ToString() + " Job/Job Task bulunamadı.\r\n";
                    }
                }

                // EXCELde Personel alanı dolu ise boyutlar eziliyor. 
                if (dr["PERSONEL BOYUT KODU"].ToString() != "")
                {
                    string blokeKontrol = f.tekbirsonucdonder("select Blocked from [0C_00349_00_DIMENSION VALUES] where  COMPANY='" + _obj.Comp.ToString() + "' and  Code='" + dr["PERSONEL BOYUT KODU"].ToString() + "'");
                    if (blokeKontrol == "0")
                    {
                        string boyutlarPers = f.tekbirsonucdonder("SELECT  BLG+'-'+DEPT FROM [0C_00352_02_DEFAULT_DIMENSIONS] WHERE COMPANY='" + _obj.Comp.ToString() + "' and [Table ID]='5200' and PERS = '" + dr["PERSONEL BOYUT KODU"].ToString() + "'");
                        if (boyutlarPers != "NULL")
                        {
                            string[] _boyutlarPers = boyutlarPers.Split('-');
                            dr["BÖLGE BOYUT KODU"] = _boyutlarPers[0].ToString();
                            dr["DEPARTMAN BOYUT KODU"] = _boyutlarPers[1].ToString();


                            if (dr["BÖLGE BOYUT KODU"].ToString() == "" | dr["DEPARTMAN BOYUT KODU"].ToString() == "")
                            {
                                HataMesajıBoyut += dr["PERSONEL BOYUT KODU"].ToString() + " boyut değerleri girilmemiş.\r\n";
                            }
                        }
                        else
                        {
                            HataMesajıBoyut += dr["PERSONEL BOYUT KODU"].ToString() + "  DEFAULT DIMENSIONS DA PERSONEL BOYUTU BULUNAMADI.\r\n";
                        }
                    }
                    else
                    {
                        HataMesajıBoyut += dr["PERSONEL BOYUT KODU"].ToString() + " blokeli personel.  Excel'i düzenleyin\r\n";
                    }


                }

                if (dr["ARAÇ PLAKA BOYUT KODU"].ToString() != "")
                {
                    // EXCELde ARAÇ PLAKA alanı dolu ise boyutlar eziliyor. 

                    string blokeKontrol = f.tekbirsonucdonder("select Blocked from [0C_00349_00_DIMENSION VALUES] where  COMPANY='" + _obj.Comp.ToString() + "' and  Code='" + dr["ARAÇ PLAKA BOYUT KODU"].ToString() + "'");
                    if (blokeKontrol == "0")
                    {

                        string boyutlarAracPlaka = f.tekbirsonucdonder("SELECT   [Bolge Kodu] + '-' +[Departman Kodu] + '-' +[Personel Kodu] FROM  [0C_50029_00_HAT_ARAC_KAYIT] WHERE  COMPANY='" + _obj.Comp.ToString() + "' and   [Arac Boyutu]  = '" + dr["ARAÇ PLAKA BOYUT KODU"].ToString() + "'");
                        if (boyutlarAracPlaka != "NULL")
                        {
                            string[] _boyutlarArac = boyutlarAracPlaka.Split('-');
                            dr["BÖLGE BOYUT KODU"] = _boyutlarArac[0].ToString();
                            dr["DEPARTMAN BOYUT KODU"] = _boyutlarArac[1].ToString();
                            dr["PERSONEL BOYUT KODU"] = _boyutlarArac[2].ToString();

                            if (dr["BÖLGE BOYUT KODU"].ToString() == "" | dr["DEPARTMAN BOYUT KODU"].ToString() == "" | dr["PERSONEL BOYUT KODU"].ToString() == "")
                            {
                                HataMesajıBoyut += dr["ARAÇ PLAKA BOYUT KODU"].ToString() + " boyut değerleri girilmemiş. \r\n";
                            }
                        }
                        else
                        {
                            HataMesajıBoyut += dr["ARAÇ PLAKA BOYUT KODU"].ToString() + "  boyutu bulunamadı.\r\n";
                        }

                    }
                    else
                    {
                        HataMesajıBoyut += dr["ARAÇ PLAKA BOYUT KODU"].ToString() + " blokeli araç plaka. Excel'i düzenleyin\r\n";
                    }




                }




            }

        }

        private void RakamKontrol(DataTable dt)
        {

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i][3] == DBNull.Value)
                    HataMesajıRakam += (i + 2).ToString() + ",";
                else
                {
                    if (Convert.ToInt32(dt.Rows[i][3]) < 0)
                    {
                        HataMesajıRakam += (i + 2).ToString() + ",";
                    }
                }
                if (dt.Rows[i][4] == DBNull.Value)
                    HataMesajıRakam += (i + 2).ToString() + ",";
                else
                {
                    if (Convert.ToInt32(dt.Rows[i][4]) < 0)
                    {
                        HataMesajıRakam += (i + 2).ToString() + ",";
                    }
                }
                if (dt.Rows[i][5] == DBNull.Value)
                    HataMesajıRakam += (i + 2).ToString() + ",";
                else
                {
                    if (Convert.ToInt32(dt.Rows[i][5]) < 0)
                    {
                        HataMesajıRakam += (i + 2).ToString() + ",";
                    }
                }
                if (dt.Rows[i][6] == DBNull.Value)
                    HataMesajıRakam += (i + 2).ToString() + ",";
                else
                {
                    if (Convert.ToInt32(dt.Rows[i][6]) < 0)
                    {
                        HataMesajıRakam += (i + 2).ToString() + ",";
                    }
                }
                if (dt.Rows[i][7] == DBNull.Value)
                    HataMesajıRakam += (i + 2).ToString() + ",";
                else
                {
                    if (Convert.ToInt32(dt.Rows[i][7]) < 0)
                    {
                        HataMesajıRakam += (i + 2).ToString() + ",";
                    }
                }
                if (dt.Rows[i][8] == DBNull.Value)
                    HataMesajıRakam += (i + 2).ToString() + ",";
                else
                {
                    if (Convert.ToInt32(dt.Rows[i][8]) < 0)
                    {
                        HataMesajıRakam += (i + 2).ToString() + ",";
                    }
                }

                if (HataMesajıRakam.Length <= 0)
                {
                    double _tempkdvharic = (Convert.ToDouble(dt.Rows[i]["MİKTAR"]) * Convert.ToDouble(dt.Rows[i]["BİRİM FİYAT"]));
                    double _kdvHaric = Convert.ToDouble(dt.Rows[i]["KDV HARİÇ TUTAR"]);
                    if (Math.Abs(_tempkdvharic - _kdvHaric) >= 1)
                        HataMesajıRakam += (i + 2).ToString() + ",";

                    double _tempKdvTutari = ((Convert.ToDouble(dt.Rows[i]["KDV HARİÇ TUTAR"]) * Convert.ToDouble(dt.Rows[i]["KDV ORANI"])) / 100);
                    double _kdvTutari = Convert.ToDouble(dt.Rows[i]["KDV TUTARI"]);
                    if (Math.Abs(_tempKdvTutari - _kdvTutari) >= 1)
                        HataMesajıRakam += (i + 2).ToString() + ",";

                    double _tempDahilTutar = (Convert.ToDouble(dt.Rows[i]["KDV HARİÇ TUTAR"]) + Convert.ToDouble(dt.Rows[i]["KDV TUTARI"]));
                    double _dahilTutar = Convert.ToDouble(dt.Rows[i]["KDV DAHİL TUTAR"]);
                    if (Math.Abs(_tempDahilTutar - _dahilTutar) >= 1)
                        HataMesajıRakam += (i + 2).ToString() + ",";
                }


            }
            if (HataMesajıRakam.Length > 0)
                HataMesajıRakam = HataMesajıRakam.Remove(HataMesajıRakam.Length - 1, 1) + ". satır/satırlardaki rakam alanlarında hatalar mevcut. Excel'i düzenleyin.";


        }

        private void JobJobTaskCheck(DataTable dt)
        {

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (f.tekbirsonucdonder("select count(*) from [0C_01001_00_JOB_TASK] where COMPANY='" + _obj.Comp.ToString() + "'  and [Job No_] = '" + dt.Rows[i]["İŞ NO"].ToString() + "' and [Job Task No_] = '" + dt.Rows[i]["İŞ GÖREV NO"].ToString() + "'") == "0")
                {
                    HataMesajıjobNo += (i + 2).ToString() + ",";
                }
            }
            if (HataMesajıjobNo.Length > 0)
                HataMesajıjobNo = HataMesajıjobNo.Remove(HataMesajıjobNo.Length - 1, 1) + ". satır/satırlardaki İŞ NO veya İŞ GÖREV NO alanlarında hatalar mevcut. Excel'i düzenleyin.";


        }

        private void InvoiceLineImport(DataTable dt)
        {

            string sorgu = "";
            try
            {

                sorgu = "Delete from " + dbName + "[" + _obj.Comp.ToString() + "$Purchase Line Aktarim] where [Fatura No]='" + FaturaNo + "'";
                f.updateyap(sorgu);
                foreach (DataRow dr in dt.Rows)
                {
                    sorgu = "INSERT INTO  " + dbName + "[" + _obj.Comp.ToString() + "$Purchase Line Aktarim]  ([Fatura No] ,[Gider Kodu] ,[Tur1]  ,[Tur2] ,[Tur3]  ,[Proje No]  ,[Proje Gorev No] ,[Miktar] ,[Birim Fiyat]  ,[KDV Hariç Tutar] ,[KDV Oran] ," +
                      "[KDV Tutar]  ,[KDV Dahil Tutar]  ,[Araç Plaka]  ,[Personel]  ,[Departman]  ,[Bölge] ,[Faaliyet])  values " +
                      "('" + FaturaNo + "','" +
                          dr[0].ToString() + "','" +
                          dr[0].ToString().Substring(1, 3) + "','" +
                          dr[0].ToString().Substring(4, 2) + "','" +
                          dr[0].ToString().Substring(6, 2) + "','" +
                          dr[1].ToString() + "','" +
                          dr[2].ToString() + "'," +
                          method.StringToInt(dr[3].ToString()) + "," +
                          method.StringToDecimal(dr[4].ToString()) + "," +
                          method.StringToDecimal(dr[5].ToString()) + "," +
                          method.StringToInt(dr[6].ToString()) + "," +
                          method.StringToDecimal(dr[7].ToString()) + "," +
                          method.StringToDecimal(dr[8].ToString()) + ",'" +
                          dr[9].ToString() + "','" +
                          dr[10].ToString() + "','" +
                          dr[11].ToString() + "','" +
                          dr[12].ToString() + "','" +
                          dr[13].ToString() + "')";

                    f.updateyap(sorgu);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(sorgu);
            }


        }

        protected void Spreadsheet_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(FilePath))
            {
                Spreadsheet.Open(FilePath);
            }
        }

        protected void Upload_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
        {
            if (!System.IO.Directory.Exists(Server.MapPath("~/" + CurrentFolderName(_obj.Comp) + "/Dosya/Satinalma Fatura Aktarim")))
                System.IO.Directory.CreateDirectory(Server.MapPath("~/" + CurrentFolderName(_obj.Comp) + "/Dosya/Satinalma Fatura Aktarim"));
            if (System.IO.File.Exists(Server.MapPath("~/" + CurrentFolderName(_obj.Comp) + "/Dosya/Satinalma Fatura Aktarim/") + FaturaNo + "_" + _obj.UserName.ToString() + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx"))
                System.IO.File.Delete(Server.MapPath("~/" + CurrentFolderName(_obj.Comp) + "/Dosya/Satinalma Fatura Aktarim/") + FaturaNo + "_" + _obj.UserName.ToString() + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");


            String uploadedFilePath = Server.MapPath("~/" + CurrentFolderName(_obj.Comp) + "/Dosya/Satinalma Fatura Aktarim/") + FaturaNo + "_" + _obj.UserName.ToString() + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
            e.UploadedFile.SaveAs(uploadedFilePath);
            if (!String.IsNullOrEmpty(uploadedFilePath))
                DocumentManager.CloseDocument(uploadedFilePath);
            FilePath = uploadedFilePath;

        }
        string FilePath
        {
            get { return Session["ExcelFilePath"] == null ? String.Empty : Session["ExcelFilePath"].ToString(); }
            set { Session["ExcelFilePath"] = value; }
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPostBack || IsCallback) return;
            FilePath = String.Empty;
        }


        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }
    }
}