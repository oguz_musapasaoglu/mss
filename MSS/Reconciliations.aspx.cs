﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.WSGeneric;
using MSS1.Codes;

namespace MSS1
{
    public partial class Reconciliations : Bases
    {
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        SqlConnection baglanti;
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        public ws_baglantı ws = new ws_baglantı();
        NavServicesContents serviceContent = new NavServicesContents();
        List<string> _Extentions = new List<string>(new string[] { ".bmp", ".jpg", ".jpeg", ".png", ".gif"
        , ".docx", ".doc", ".xlsx", ".xls", ".txt", ".pdf" });

        SessionBase _obj;

        #region Dil Getir
        public void dilgetir()
        {
            lblDovCinsB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "satis_sip", "lblDovCinsB");
            lblMutabakatTarihB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "Reconciliations", "lblMutabakatTarihB");
            lblGecerlilikTarihiB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "Reconciliations", "lblGecerlilikTarihiB");
            lblMutabikKalinanTutarB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "Reconciliations", "lblMutabikKalinanTutarB");
            lblMutabikKalindiB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "Reconciliations", "lblMutabikKalindiB");
            lblYorumB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "Reconciliations", "lblYorumB");
            btnSave.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "Reconciliations", "BtnSave");
            lblMutabakatTuruB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "Reconciliations", "lblMutabakatTuruB");
            lblMutabakatSirketiB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "Reconciliations", "Mutabakat Şirketi");


            GrdLines.Columns[0].HeaderText = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "Reconciliations", "Mutabakat Şirketi");
            GrdLines.Columns[1].HeaderText = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "Reconciliations", "Mutabakat No");
            GrdLines.Columns[2].HeaderText = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "Reconciliations", "lblMutabakatTuruB");
            GrdLines.Columns[3].HeaderText = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "Reconciliations", "Müşteri No");
            GrdLines.Columns[4].HeaderText = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "satis_sip", "lblDovCinsB");
            GrdLines.Columns[5].HeaderText = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "Reconciliations", "lblMutabakatTarihB");
            GrdLines.Columns[6].HeaderText = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "Reconciliations", "lblGecerlilikTarihiB");
            GrdLines.Columns[7].HeaderText = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "Reconciliations", "lblMutabikKalinanTutarB");
            GrdLines.Columns[8].HeaderText = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "Reconciliations", "lblMutabikKalindiB");
            GrdLines.Columns[9].HeaderText = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "Reconciliations", "Oluşturan Kullanıcı");
            GrdLines.Columns[10].HeaderText = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "Reconciliations", "lblYorumB");

        }
        #endregion

        #region Page Sub Methods
        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            if (IsPostBack || IsCallback) return;
            HdGId.Value = Request.QueryString["GId"];
            dilgetir();

            f.DropDownDoldurIlkSatirEkle("Select Code name, Code kod From [dbo].[0C_00004_00_CURRENCY] " +
                "where COMPANY = '" + _obj.Comp + "'   Order By Code", ddldoviz, true, "TL");
            ddldoviz.SelectedIndex = 0;

            if (Request.QueryString["CustomerNo"] != null)
            {
                LoadGrid(Request.QueryString["CustomerNo"].ToString());
            }

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }

        void LoadGrid(string CustomerNo)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(strConnString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {

                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "sp_0WEB_MUTABAKAT";
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@comp", _obj.Comp);
                        cmd.Parameters.AddWithValue("@CustomerNo", CustomerNo);

                        using (SqlDataAdapter adptr = new SqlDataAdapter(cmd))
                        {
                            DataTable dt = new DataTable();
                            adptr.Fill(dt);
                            GrdLines.DataSource = dt;
                            GrdLines.DataBind();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox("Error : " + ex.Message.Replace("'", " "));
            }

        }

        void clearForm()
        {
            txtGecerlilikTarihi.Text = string.Empty;
            txtMutabakatTarih.Text = string.Empty;
            ddldoviz.SelectedIndex = -1;
            txtMutabakatTutari.Text = string.Empty;
            ddlMutabikKalindi.SelectedValue = "0";
            txtYorum.Text = string.Empty;
            ddlMutabakatTuru.SelectedValue = "0";
            ddlMutabakatSirketi.SelectedValue = "0";
        }

        private Web_GenericFunction_PortClient GetWebFonksiyonlari()
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(_obj.Comp)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlMutabakatSirketi.SelectedValue == "0")
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "reconciliations", "msg001"));

                    return;
                }
                if (ddlMutabakatTuru.SelectedValue == "0")
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "reconciliations", "msg002"));
                    return;
                }
                if (!FlpAttach0.HasFile)
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "reconciliations", "msg003"));
                    return;
                }
                if (string.IsNullOrEmpty(ddldoviz.SelectedValue))
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "reconciliations", "msg004"));
                    return;
                }
                if (ddlMutabikKalindi.SelectedValue == "0")
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "reconciliations", "msg005"));
                    return;
                }
                if (string.IsNullOrEmpty(txtMutabakatTarih.Text))
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "reconciliations", "msg006"));
                    return;
                }
                if (string.IsNullOrEmpty(txtGecerlilikTarihi.Text))
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "reconciliations", "msg007"));

                    return;
                }
                string[] _array = txtGecerlilikTarihi.Text.Replace("/", ".").Replace(" ", ".").Split('.');
                DateTime _dateGecerlilik = new DateTime(Convert.ToInt32(_array[2]), Convert.ToInt32(_array[1]), Convert.ToInt32(_array[0]));
                string[] _array2 = txtMutabakatTarih.Text.Replace("/", ".").Replace(" ", ".").Split('.');
                DateTime _dateMutabakat = new DateTime(Convert.ToInt32(_array2[2]), Convert.ToInt32(_array2[1]), Convert.ToInt32(_array2[0]));



                if (_dateMutabakat < _dateGecerlilik)
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "reconciliations", "msg008"));
                    txtGecerlilikTarihi.Text = "";
                    txtMutabakatTarih.Text = "";
                    return;
                }
                if (string.IsNullOrEmpty(txtMutabakatTutari.Text))
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "reconciliations", "msg009"));
                    return;
                }
                string extn = string.Empty;
                if (FlpAttach0.HasFile)
                {
                    extn = System.IO.Path.GetExtension(FlpAttach0.FileName);
                }

                if (!_Extentions.Contains(extn.ToLower()))
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "reconciliations", "msg010"));
                    return;
                }
                string extn1 = string.Empty;
                if (FlpAttach1.HasFile)
                {
                    extn1 = System.IO.Path.GetExtension(FlpAttach1.FileName);
                }

                if (!_Extentions.Contains(extn.ToLower()))
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "reconciliations", "msg010"));
                    return;
                }
                string extn2 = string.Empty;
                if (FlpAttach2.HasFile)
                {
                    extn2 = System.IO.Path.GetExtension(FlpAttach2.FileName);
                }

                if (!_Extentions.Contains(extn.ToLower()))
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "reconciliations", "msg010"));
                    return;
                }

                string yorum1 = string.Empty, yorum2 = string.Empty, yorum3 = string.Empty;

                if (txtYorum.Text.Length > 500)
                {
                    yorum1 = txtYorum.Text.Substring(0, 250);
                    yorum2 = txtYorum.Text.Substring(250, 250);
                    yorum3 = txtYorum.Text.Substring(500, txtYorum.Text.Length - 500);
                }
                else
                {
                    if (txtYorum.Text.Length <= 250)
                    {
                        yorum1 = txtYorum.Text;
                    }
                    else if (txtYorum.Text.Length > 250)
                    {
                        yorum1 = txtYorum.Text.Substring(0, 250);
                        yorum2 = txtYorum.Text.Substring(250, txtYorum.Text.Length - 250);
                    }
                }

                string[] tcevir = { "" }, tcevir1 = { "" };
                string tcevirtum = "", tcevirtum1 = "";

                if (txtMutabakatTarih.Text != string.Empty)
                {
                    tcevir = txtMutabakatTarih.Text.Split('.');
                    tcevirtum += tcevir[1] + tcevir[0] + tcevir[2];
                }

                if (txtGecerlilikTarihi.Text != string.Empty)
                {
                    tcevir1 = txtGecerlilikTarihi.Text.Split('.');
                    tcevirtum1 += tcevir1[1] + tcevir1[0] + tcevir1[2];
                }

                string[] _params = { Request.QueryString["Type"].ToString(), Request.QueryString["CustomerNo"].ToString()
                    , ddldoviz.SelectedValue, tcevirtum, tcevirtum1, yorum1, yorum2, yorum3, txtMutabakatTutari.Text.Replace(",", ".")
                    , ddlMutabikKalindi.SelectedValue, _obj.UserName, extn.ToLower(), extn1.ToLower(), extn2.ToLower(),
                    ddlMutabakatTuru.SelectedValue, ddlMutabakatSirketi.SelectedValue };

                string MutabakatNo = GetWebFonksiyonlari().ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "MutabakatOlustur§" + string.Join("§", _params), _obj.Comp);

                //string MutabakatNo = record.MutabakatOlustur(Convert.ToInt32(Request.QueryString["Type"].ToString()), Request.QueryString["CustomerNo"].ToString()
                //    , ddldoviz.SelectedValue, tcevirtum, tcevirtum1, yorum1, yorum2, yorum3, txtMutabakatTutari.Text.Replace(",", ".")
                //    , Convert.ToInt32(ddlMutabikKalindi.SelectedValue), _obj.UserName, extn.ToLower(), extn1.ToLower(), extn2.ToLower(),
                //    Convert.ToInt32(ddlMutabakatTuru.SelectedValue), Convert.ToInt32(ddlMutabakatSirketi.SelectedValue));

                string TargetFilePath0 = Server.MapPath("~/" + CurrentFolderName(_obj.Comp) + "/Dosya/Mutabakat/") + MutabakatNo + "_1" + extn.ToLower();
                string TargetFilePath1 = Server.MapPath("~/" + CurrentFolderName(_obj.Comp) + "/Dosya/Mutabakat/") + MutabakatNo + "_2" + extn1.ToLower();
                string TargetFilePath2 = Server.MapPath("~/" + CurrentFolderName(_obj.Comp) + "/Dosya/Mutabakat/") + MutabakatNo + "_3" + extn2.ToLower();
                if (FlpAttach0.HasFile)
                {
                    if (File.Exists(TargetFilePath0))
                        File.Delete(TargetFilePath0);
                    FlpAttach0.PostedFile.SaveAs(TargetFilePath0);
                }


                if (FlpAttach1.HasFile)
                {
                    if (File.Exists(TargetFilePath1))
                        File.Delete(TargetFilePath1);
                    FlpAttach1.PostedFile.SaveAs(TargetFilePath1);
                }


                if (FlpAttach2.HasFile)
                {
                    if (File.Exists(TargetFilePath2))
                        File.Delete(TargetFilePath2);
                    FlpAttach2.PostedFile.SaveAs(TargetFilePath2);
                }


                clearForm();
                LoadGrid(Request.QueryString["CustomerNo"].ToString());
            }
            catch (Exception ex)
            {
                MessageBox("Save Error : " + ex.Message.Replace("'", " "));
            }

        }
    }


}