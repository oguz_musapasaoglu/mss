﻿<%@ Page  Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Debug="true"
    CodeBehind="masrafgirisi.aspx.cs" Inherits="MSS1.masrafgirisi" EnableEventValidation="false"
    MaintainScrollPositionOnPostback="false" 
    %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" language="javascript">
        var oldgridSelectedColor;

        // Function to set the background color when mouse is over
        function setMouseOverColor(element) {
            oldgridSelectedColor = element.style.backgroundColor;
            element.style.backgroundColor = 'DeepSkyBlue'; // this is the color I am seting as backcolor
            element.style.cursor = 'pointer';
        }

        // Function to set the existing color when over is out of the row
        function setMouseOutColor(element) {
            element.style.backgroundColor = 'transparent';
        }
        function nakletsoru() {
            var count = document.getElementById("ContentPlaceHolder1_lbltoplamsatir").innerText
            var boyutkontrol = document.getElementById("ContentPlaceHolder1_lblboyuthata").innerText
            if (confirm('Günlük satırlarını Onaylamak istiyor musunuz ?')) {
                if (count > 0 && boyutkontrol == true) {
                    alert("Onaylandı .");
                    return true;
                }
                else if (count < 0) {
                    alert("Onaylanacak satır Yok !");
                    return false;
                }
            }
            else {
                alert("Onaylama İşlemi İptal Edildi !");
                return false;
            }

        }
        function silmesoru() {
            var count = document.getElementById("ContentPlaceHolder1_lbltoplamsatir").innerText
            if (confirm('Seçilen Satırları Silmek istiyor musunuz ?')) {
                if (count > 0) {
                    alert("Başarılı Bir Şekilde Silindi.");
                    return true;
                }
                else {
                    alert("Silmek İstediğiniz Satırı Seçiniz !");
                    return false;
                }
            }
            else {
                alert("Satır Silme İşlemi İptal Edildi !");
                return false;
            }

        }
        function resetPosition(object, args) {

            var tb = object._element; // tb.id is the associated textbox ID in the grid.

            // Get the position with scrolling.
            var tbposition = [tb.offsetLeft + tb.offsetParent.offsetLeft, tb.offsetTop + tb.offsetParent.offsetTop];

            var xposition = tbposition[0] + 10; // -40 lined it up horizontally with the textbox in my circumstance

            var yposition = tbposition[1] + 480; // 22 = textbox height + no spacing

            var ex = object._completionListElement;
            if (ex)

                $common.setLocation(ex, new Sys.UI.Point(xposition, yposition));
        } // End resetPosition
        function resetPosition2(object, args) {

            var tb = object._element; // tb.id is the associated textbox ID in the grid.

            // Get the position with scrolling.
            var tbposition = [tb.offsetLeft + tb.offsetParent.offsetLeft, tb.offsetTop + tb.offsetParent.offsetTop];

            var xposition = tbposition[0] + 10; // -40 lined it up horizontally with the textbox in my circumstance

            var yposition = tbposition[1] + 180; // 22 = textbox height + no spacing

            var ex = object._completionListElement;
            if (ex)

                $common.setLocation(ex, new Sys.UI.Point(xposition, yposition));
        } // End resetPosition
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <h2>
        <asp:Label ID="lblBaslik" runat="server"></asp:Label>
    </h2>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptLocalization="true"
        EnableScriptGlobalization="true" EnablePageMethods="true">
    </asp:ScriptManager>
    <table class="style1" cellspacing="8">
        <tr>
            <td class="style2">
                <asp:Label ID="lblAlacakliHesapB" runat="server"></asp:Label>
&nbsp;</td>
            <td class="style2">
                <asp:DropDownList ID="ddlalacakhesabı" CssClass="textbox" runat="server" Width="375PX"
                    AutoPostBack="True" AppendDataBoundItems="True" OnSelectedIndexChanged="ddlalacakhesabı_SelectedIndexChanged">
                    <asp:ListItem Selected="True"> </asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="style2">
                <asp:CheckBox ID="chper" runat="server" AutoPostBack="True" OnCheckedChanged="chper_CheckedChanged"
                    Text="Personel" />
            </td>
            <td class="style2">
                &nbsp;
            </td>
            <td class="style2">
                TL
            </td>
        </tr>
        <tr>
            <td class="style2">
                <asp:Label ID="lblBelgeNoB" runat="server"></asp:Label>
            </td>
            <td class="style2">
                <asp:Label ID="lblbelgeno" runat="server"></asp:Label>
            </td>
            <td class="style2">
                &nbsp;
            </td>
            <td class="style2">
                <asp:Label ID="lblMatrahB" runat="server"></asp:Label>
&nbsp;</td>
            <td class="style2">
                <asp:Label ID="lblsatirtoplamtl" runat="server">0,00</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblNakilTarihiB" runat="server"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtdnakil" CssClass="textbox" runat="server" Width="100px" Style="text-align: justify"
                    ValidationGroup="MKE" AutoPostBack="True" 
                    ontextchanged="txtdnakil_TextChanged" />
                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtdnakil"
                    Format="dd.MM.yyyy" />
            </td>
            <td>
               <asp:Literal ID="ltrHariciBelgeNo" runat="server" Visible="false"></asp:Literal>
                <asp:Literal ID="firmaAdi" runat="server"></asp:Literal>
            </td>
            <td>
                <asp:Label ID="lblKDVB" runat="server"></asp:Label>
&nbsp;</td>
            <td>
                <asp:Label ID="lblfarktl" runat="server">0,00</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbldosyapath" runat="server" Visible="False"></asp:Label>
                <asp:Label ID="lblbaglanmısdosya" runat="server" Font-Bold="True" Visible="False">Bağlanmış Dosya</asp:Label>
            </td>
            <td>
                <asp:Button ID="btndosyagoster" runat="server" CssClass="myButton" OnClick="btndosyagoster_Click"
                    Visible="False" Width="100px" />
            </td>
            <td>
                <asp:Label ID="lbldeger2" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblToplamB" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblsatirtoplamtlkdv" runat="server">0,00</asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <asp:Label ID="lblkaynak" runat="server" Visible="False"></asp:Label>
    <asp:Label ID="lblprojeno" runat="server" Visible="False"></asp:Label>
    <asp:Label ID="lblprojegorev" runat="server" Visible="False"></asp:Label>
    <asp:Label ID="lbltoplamsatir" runat="server" Style="display: none"></asp:Label>
    <asp:Label ID="lblboyuthata" runat="server" Style="display: none"></asp:Label>
    <br />
    <table cellspacing="1">
        <tr style="background: #dce3eb;">
            <td>
                <asp:Label ID="lblAlacakHesapB" runat="server" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblBelgeTarihiB" runat="server" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblFirmaAdiB" runat="server" Width="100px"></asp:Label>
            </td>
            <td  >
                <asp:Label ID="lblSeriNoB" width="70px" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblHariciBelgeB" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblVergiNoB" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblParaBirimiB" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblGununKuruB" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblKDVHaricB" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblKDVOranB" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblKDVTutarB" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblKDVDahilTutarB" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblTutarTLB" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblGGT1B" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblGGT2B" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblGGT3B" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblKaynakAdiB" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblIsGACNoB" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblIsGorevAdiB" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblFinalDAKuruB" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblFinalDADovizCinsiB" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblAracPlakaB" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblPersonelB" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblBolgeB" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblDepartmanB" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblFaaliyetB" runat="server"></asp:Label>
            </td>
            
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblkhesapadi" runat="server" Width="100px"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtbelgetarihi" CssClass="textbox" runat="server" Width="100px"
                    Style="text-align: justify" ValidationGroup="MKE" Height="22px" OnTextChanged="txtbelgetarihi_TextChanged" AutoPostBack="true" />
                <cc1:CalendarExtender ID="txtbelgetarihi_CalendarExtender" runat="server" TargetControlID="txtbelgetarihi"
                    Format="dd.MM.yyyy" />
            
            </td>
            <td>
                <asp:TextBox ID="txtfirma" CssClass="textbox" runat="server" Width="100px" Height="22px"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtSeriNo" CssClass="textbox" runat="server" Height="22px" Width="70px"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtharicibelgeno" CssClass="textbox" runat="server" Width="120px"
                    Height="22px"  ></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtvergino" CssClass="textbox" runat="server" Width="70px" Height="22px"
                    MaxLength="11"></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList ID="ddlparabirimi" runat="server" AppendDataBoundItems="True" CssClass="textbox"
                    Height="28px" Width="60px" AutoPostBack="True" OnSelectedIndexChanged="ddlparabirimi_SelectedIndexChanged">
                    <asp:ListItem Selected="True" Text="TL" Value="" />
                    <asp:ListItem Text="USD" Value="USD" />
                    <asp:ListItem Text="EURO" Value="EURO" />
                    <asp:ListItem Value="STERLIN">STERLIN</asp:ListItem>
                    <asp:ListItem Value="RUBLE">RUBLE</asp:ListItem>
                    <asp:ListItem Value="YUAN">YUAN</asp:ListItem>
                    <asp:ListItem Value="FRANK">FRANK</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="txtkur" CssClass="textbox" runat="server" Width="50px" Height="22px"
                    MaxLength="11" AutoPostBack="True" OnTextChanged="txtkur_TextChanged" Enabled="False">0.00</asp:TextBox>
            </td>
            <td>
                <asp:TextBox CssClass="textbox" Width="70px" ID="txtnettutar" runat="server" Height="22px"
                    AutoPostBack="True" OnTextChanged="txtnettutar_TextChanged"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="txtnettutar_FilteredTextBoxExtender" runat="server"
                    TargetControlID="txtnettutar" FilterType="Numbers,Custom" ValidChars=".">
                </cc1:FilteredTextBoxExtender>
            </td>
            <td>
                <asp:DropDownList ID="ddlkdvoran" runat="server" AppendDataBoundItems="True" CssClass="textbox"
                    Height="28px" Width="50px" AutoPostBack="True" OnSelectedIndexChanged="ddlkdvoran_SelectedIndexChanged">
                    <asp:ListItem Selected="True" Text="" Value="" />
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox CssClass="textbox" Width="68px" ID="txtkdvtutar" runat="server" Height="22px"
                    AutoPostBack="True" Enabled="False"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="txtkdvtutar_FilteredTextBoxExtender" runat="server"
                    TargetControlID="txtkdvtutar" FilterType="Numbers,Custom" ValidChars=".">
                </cc1:FilteredTextBoxExtender>
            </td>
            <td>
                <asp:TextBox CssClass="textbox" Width="70px" ID="txtkdvdahil" ReadOnly="true" runat="server" Height="22px"
                    AutoPostBack="True" EnableTheming="True" OnTextChanged="txtkdvdahil_TextChanged"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="txtkdvdahil_FilteredTextBoxExtender" runat="server"
                    TargetControlID="txtkdvdahil" FilterType="Numbers,Custom" ValidChars=".">
                </cc1:FilteredTextBoxExtender>
            </td>
            <td>
                <asp:Label ID="lbltutartl" runat="server" Text="0.00"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddltur1" runat="server" Width="80px" AppendDataBoundItems="True"
                    AutoPostBack="True" OnSelectedIndexChanged="ddltur1_SelectedIndexChanged" Height="28px">
                    <asp:ListItem Selected="True"> </asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddltur2" runat="server" Width="80px" AppendDataBoundItems="True"
                    AutoPostBack="True" OnSelectedIndexChanged="ddltur2_SelectedIndexChanged" Height="28px">
                    <asp:ListItem Selected="True"> </asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddltur3" runat="server" Width="80px" AppendDataBoundItems="True"
                    AutoPostBack="True" OnSelectedIndexChanged="ddltur3_SelectedIndexChanged" Height="28px">
                    <asp:ListItem Selected="True"> </asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="txtkaynak" runat="server" AutoPostBack="True" CssClass="textbox"
                    Height="22px" Width="120px" OnTextChanged="txtkaynak_TextChanged"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtprojeadi" runat="server" AutoPostBack="True" CssClass="textbox"
                    Height="22px" Width="100px" OnTextChanged="txtprojeadi_TextChanged"></asp:TextBox>
                <cc1:AutoCompleteExtender ServiceMethod="SearchJob" UseContextKey="true" MinimumPrefixLength="1"
                    CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtprojeadi"
                    ID="AutoCompleteExtender1" runat="server" FirstRowSelected="false" CompletionListCssClass="listMain"
                    CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected"
                    OnClientShown="resetPosition">
                </cc1:AutoCompleteExtender>
            </td>
            <td>
                <asp:TextBox ID="txtprojegorev" runat="server" AutoPostBack="True" CssClass="textbox"
                    Height="22px" Width="100px" OnTextChanged="txtprojegorev_TextChanged"></asp:TextBox>
                <cc1:AutoCompleteExtender ServiceMethod="SearchJobTask" UseContextKey="true" MinimumPrefixLength="1"
                    CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtprojegorev"
                    ID="txtprojegorev_AutoCompleteExtender" runat="server" FirstRowSelected="false"
                    CompletionListCssClass="listMain" CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected"
                    OnClientShown="resetPosition2">
                </cc1:AutoCompleteExtender>
            </td>
            <td>
                <asp:TextBox ID="txtfinaldakur" runat="server" Width="70px" Height="22px">0.00</asp:TextBox>
            </td>
            <td>
                <asp:DropDownList ID="ddlfinaldadoviz" CssClass="textbox" Width="100px" runat="server"
                    AutoPostBack="True" Height="22px">
                   <%-- <asp:ListItem Value="TL">TL</asp:ListItem>
                    <asp:ListItem Value="USD">USD</asp:ListItem>
                    <asp:ListItem Value="EURO">EURO</asp:ListItem>
                    <asp:ListItem Value="STERLIN">STERLIN</asp:ListItem>
                    <asp:ListItem Value="RUBLE">RUBLE</asp:ListItem>
                    <asp:ListItem Value="YUAN">YUAN</asp:ListItem>
                    <asp:ListItem Value="FRANK">FRANK</asp:ListItem>--%>
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddlarac" runat="server" AppendDataBoundItems="True"  Width="100px"
                    Height="28px" onselectedindexchanged="ddlarac_SelectedIndexChanged" 
                    AutoPostBack="True">
                    <asp:ListItem></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddlpersonel" runat="server" AppendDataBoundItems="True" Width="100px"
                    Height="28px" onselectedindexchanged="ddlpersonel_SelectedIndexChanged" 
                    AutoPostBack="True" >  
                    <asp:ListItem></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddlbolge" runat="server" AppendDataBoundItems="True" Width="100px"
                    Height="28px">
                    <asp:ListItem></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddldepartman" runat="server" AutoPostBack = "true" AppendDataBoundItems="True" Width="100px"
                    Height="28px" onselectedindexchanged="ddldepartman_SelectedIndexChanged"> 
                    <asp:ListItem></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddlfaaliyet" runat="server" AppendDataBoundItems="True" Width="100px"
                    Height="28px">
                    <asp:ListItem></asp:ListItem>
                </asp:DropDownList>
            </td>
            
            <td>
                <asp:Button ID="btnekle" runat="server" CssClass="ekle_buton" Height="17px" OnClick="btnekle_Click" />
            </td>
        </tr>
    </table>
    <asp:GridView ID="grdmasraf" runat="server" Width="100%" AutoGenerateColumns="False"
        PagerSettings-Mode="NumericFirstLast" PageSize="500" Font-Size="Small" Font-Bold="False"
        CellPadding="4"  ShowHeader="False" GridLines="None" OnRowCommand="grdmasraf_RowCommand"
        OnRowDataBound="grdmasraf_RowDataBound">
        <PagerSettings Mode="NumericFirstLast"></PagerSettings>
        <PagerStyle HorizontalAlign="Left" />
        <SelectedRowStyle Font-Bold="True" />

        <Columns>
            <asp:TemplateField Visible="false">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblsatirno" Text='<%# Eval("SatırNo")%>' Visible="false"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField Visible="false">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblgunlukadi" Text='<%# Eval("gunlukadi")%>' Visible="false"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField Visible="false">
                <ItemTemplate>
                    <asp:Label runat="server" ID="Label1" Text='<%# Eval("belgeno")%>' Visible="false"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField Visible="false">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblbolgekod" Text='<%# Eval("bolgekod")%>' Visible="false"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField Visible="false">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lbldepartmankod" Text='<%# Eval("departmankod")%>'
                        Visible="false"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField Visible="false">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblfaaliyetkod" Text='<%# Eval("faaliyetkod")%>' Visible="false"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField Visible="false">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblpersonelkod" Text='<%# Eval("personelkod")%>' Visible="false"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField Visible="false">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblarackod" Text='<%# Eval("arackod")%>' Visible="false"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField Visible="false">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lbltur1kod" Text='<%# Eval("tur1kod")%>' Visible="false"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField Visible="false">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lbltur2kod" Text='<%# Eval("tur2kod")%>' Visible="false"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField Visible="false">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lbltur3kod" Text='<%# Eval("tur3kod")%>' Visible="false"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="lblkhesapadisatir" runat="server" Text='<%# Bind("khesapadi") %>'>                                            </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="100px"  />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="lblbtarih" runat="server" Text='<%# Bind("btarih") %>'>                                            </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="100px"   />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="lblfirma" runat="server" Text='<%# Bind("firma") %>'>                                            </asp:Label>
                </ItemTemplate>
                <ItemStyle   Width="100px" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="lblserino" runat="server" Text='<%# Bind("SeriNo") %>'>                                            </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="70px" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="lblhbelgeno" runat="server" Text='<%# Bind("hbelgeno") %>'>                                            </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="120px"/>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="lblvergino" runat="server" Text='<%# Bind("vergino") %>'>                                            </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="70px" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="lbldovizcinsi" runat="server" Text='<%# Bind("dovizcinsi") %>'>
                    </asp:Label>
                </ItemTemplate>
                <ItemStyle  Width="60px" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="lblkur" runat="server" Text='<%# Bind("kur") %>'>
                    </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="50px" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label style="float:right" ID="lblkdvharic" runat="server" Text='<%# Eval("kdvharic") %>'>
                    </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="70px" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label style="float:right" ID="lblkdvoran" runat="server" Text='<%# Eval("kdvoran") %>'>
                    </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="50px" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label style="float:right" ID="lblkdvtutari" runat="server" Text='<%# Eval("kdvtutari") %>'> 
                    </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="68px" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label style="float:right" ID="lblkdahiltutar" runat="server" Text='<%# Eval("kdahiltutar") %>'> 
                    </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="3%" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label style="float:right" ID="Label2" runat="server" Text='<%# Eval("tutartl") %>'> 
                    </asp:Label>
                </ItemTemplate>
                <ItemStyle   />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="lbltur1" runat="server" Text='<%# Bind("tur1") %>'>                                            </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="80px" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="lbltur2" runat="server" Text='<%# Bind("tur2") %>'>                                            </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="80px" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="lbltur3" runat="server" Text='<%# Bind("tur3") %>'>                                            </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="80px" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="lblhesapadi" runat="server" Text='<%# Eval("hesapadi") %>'>
                    </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="120px" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="lblisadi" runat="server" Text='<%# Eval("isadi") %>'>
                    </asp:Label>
                </ItemTemplate>
                <ItemStyle  Width="100px" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="lblisgorevadi" runat="server" Text='<%# Eval("isgorevadi") %>'>
                    </asp:Label>
                </ItemTemplate>
                <ItemStyle  Width="100px" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="lblfinalkur" runat="server" Text='<%# Eval("finalkur") %>'> 
                    </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="70px" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="lblfinaldoviz" runat="server" Text='<%# Eval("finaldoviz") %>'> 
                    </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="100px"/>
            </asp:TemplateField>
             <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="lblarac" runat="server" Text='<%# Eval("arac") %>'> 
                    </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="100px" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="lblpersonel" runat="server" Text='<%# Eval("personel") %>'> 
                    </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="100px"/>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="lblbolge" runat="server" Text='<%# Eval("bolge") %>'> 
                    </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="100px"/>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="lbldepartman" runat="server" Text='<%# Eval("departman") %>'> 
                    </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="100px" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="lblfaaliyet" runat="server" Text='<%# Eval("faaliyet") %>'> 
                    </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="100px" />
            </asp:TemplateField>
           
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:CheckBox ID="Chksec" runat="server" Enabled="false" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <br />
    <div id="altmenumasraf">
        <asp:Button ID="btnonayla" runat="server" CssClass="myButton" OnClick="btnonayla_Click"
            OnClientClick="javascript: return nakletsoru()" />
        <asp:Button ID="btnsatirsil" runat="server" CssClass="myButton"
            OnClick="btnsatirsil_Click" OnClientClick="javascript: return silmesoru()" />
        <asp:Button ID="btndosya" runat="server" CssClass="myButton" OnClick="btndosya_Click" />
        <asp:Button ID="btndosyagor" runat="server" CssClass="myButton"
            OnClick="btndosyagor_Click" />
        <asp:Button ID="btnfaturasatirkopyala" runat="server" CssClass="myButton"
            OnClick="btnfaturasatirkopyala_Click" />
    </div>
    <br />
</asp:Content>
