﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using DevExpress.Web.ASPxSpreadsheet;
using DevExpress.Web.Office;
using MSS1.WSGeneric;
using MSS1.Codes;

namespace MSS1
{
    public partial class ExcelPopup : Bases
    {
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;

        public ws_baglantı ws = new ws_baglantı();
        Methods m = new Methods();
        SessionBase _obj;

        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _obj = TabSessions.ControlState(this.Page, Request.QueryString["GId"].ToString());
            if (IsPostBack) return;
            if (Request.QueryString["MainType"] == "AIR")
                LoadAirData();
            else
                LoadOtherData();
        }

        void LoadAirData()
        {
            txtLineNos.Text = string.Empty;
            Spreadsheet.WorkDirectory = Server.MapPath("~") + CurrentFolderName(_obj.Comp) + @"\RAPORLAR OP\BL EXCEL";
            string excelworkdirectory = Server.MapPath("~") + CurrentFolderName(_obj.Comp) + @"\RAPORLAR OP\BL EXCEL";

            //if (File.Exists(Path.Combine(excelworkdirectory, Request.QueryString["Comp"] + Request.QueryString["BlNo"] + ".xlsx")))
            //{
            //    File.Delete(Path.Combine(excelworkdirectory, Request.QueryString["Comp"] + Request.QueryString["BlNo"] + ".xlsx"));
            //}

            //if (!File.Exists(Path.Combine(excelworkdirectory, Request.QueryString["Comp"] + Request.QueryString["BlNo"] + ".xlsx")))
            File.Copy(Path.Combine(Server.MapPath("~") + CurrentFolderName(_obj.Comp) + @"\RAPORLAR OP", "AIR BL EXCEL DENEME ÇALIŞMALARI GELISTIRME.xlsx")
                , Path.Combine(excelworkdirectory, Request.QueryString["Comp"] + Request.QueryString["BlNo"] + ".xlsx"), true);

            var filePath = Path.Combine(excelworkdirectory, Request.QueryString["Comp"] + Request.QueryString["BlNo"] + ".xlsx");

            Spreadsheet.Open(filePath);

            Spreadsheet.Document.Worksheets.ActiveWorksheet = Spreadsheet.Document.Worksheets[0];


            Spreadsheet.Document.Worksheets[0].Cells["B2"].Value = ""; //BLNo1
            Spreadsheet.Document.Worksheets[0].Cells["AG2"].Value = ""; //BLNo2

            Spreadsheet.Document.Worksheets[0].Cells["B5"].Value = ""; //ShipName
            Spreadsheet.Document.Worksheets[0].Cells["B6"].Value = ""; //ShipAddress
            Spreadsheet.Document.Worksheets[0].Cells["B10"].Value = ""; //ShipTelNo
            Spreadsheet.Document.Worksheets[0].Cells["U6"].Value = ""; //IssuedBy
            Spreadsheet.Document.Worksheets[0].Cells["B13"].Value = ""; //CneeName
            Spreadsheet.Document.Worksheets[0].Cells["B14"].Value = ""; //CneeAddress
            Spreadsheet.Document.Worksheets[0].Cells["B18"].Value = ""; //CneeTelNo
            Spreadsheet.Document.Worksheets[0].Cells["B20"].Value = ""; //Carrier
            Spreadsheet.Document.Worksheets[0].Cells["U20"].Value = ""; //Zone 1
            Spreadsheet.Document.Worksheets[0].Cells["C25"].Value = ""; //IadataCode
            Spreadsheet.Document.Worksheets[0].Cells["L25"].Value = ""; //AllNo
            Spreadsheet.Document.Worksheets[0].Cells["C28"].Value = ""; //PolName
            Spreadsheet.Document.Worksheets[0].Cells["U28"].Value = ""; //AgentRef
            Spreadsheet.Document.Worksheets[0].Cells["AB29"].Value = ""; //Optional1
            Spreadsheet.Document.Worksheets[0].Cells["AH29"].Value = ""; //Optional2
            Spreadsheet.Document.Worksheets[0].Cells["B31"].Value = ""; //Zone 2-1
            Spreadsheet.Document.Worksheets[0].Cells["E31"].Value = ""; //Zone 2-2
            Spreadsheet.Document.Worksheets[0].Cells["L31"].Value = ""; //Zone 2-3
            Spreadsheet.Document.Worksheets[0].Cells["N31"].Value = ""; //Zone 2-4
            Spreadsheet.Document.Worksheets[0].Cells["P31"].Value = ""; //Zone 2-5
            Spreadsheet.Document.Worksheets[0].Cells["R31"].Value = ""; //Zone 2-6
            Spreadsheet.Document.Worksheets[0].Cells["T31"].Value = ""; //Currency
            Spreadsheet.Document.Worksheets[0].Cells["V31"].Value = ""; //Charger Code
            Spreadsheet.Document.Worksheets[0].Cells["W31"].Value = ""; //WTPP
            Spreadsheet.Document.Worksheets[0].Cells["X31"].Value = ""; //WTColl
            Spreadsheet.Document.Worksheets[0].Cells["Y31"].Value = ""; //OtherPP
            Spreadsheet.Document.Worksheets[0].Cells["Z31"].Value = ""; //OtherColl
            Spreadsheet.Document.Worksheets[0].Cells["AA31"].Value = ""; //DVCarrier
            Spreadsheet.Document.Worksheets[0].Cells["AG31"].Value = ""; //DVCustoms
            Spreadsheet.Document.Worksheets[0].Cells["B34"].Value = ""; //Zone 3-1
            Spreadsheet.Document.Worksheets[0].Cells["K34"].Value = ""; //Zone 3-2
            Spreadsheet.Document.Worksheets[0].Cells["O34"].Value = ""; //Zone 3-3
            Spreadsheet.Document.Worksheets[0].Cells["B37"].Value = ""; //Zone 4
            Spreadsheet.Document.Worksheets[0].Cells["B43"].Value = ""; //Zone 7
            Spreadsheet.Document.Worksheets[0].Cells["B52"].Value = ""; //Zone 8-1
            Spreadsheet.Document.Worksheets[0].Cells["E52"].Value = ""; //Zone 8-2
            Spreadsheet.Document.Worksheets[0].Cells["C55"].Value = ""; //Zone 9-1
            Spreadsheet.Document.Worksheets[0].Cells["C58"].Value = ""; //Zone 9-2
            Spreadsheet.Document.Worksheets[0].Cells["C61"].Value = ""; //Zone 9-3
            Spreadsheet.Document.Worksheets[0].Cells["C64"].Value = ""; //Zone 9-4
            Spreadsheet.Document.Worksheets[0].Cells["C67"].Value = ""; //Zone 9-5
            Spreadsheet.Document.Worksheets[0].Cells["C70"].Value = ""; //Zone 9-6
            Spreadsheet.Document.Worksheets[0].Cells["C73"].Value = ""; //Zone 9-7
            Spreadsheet.Document.Worksheets[0].Cells["B76"].Value = ""; //Zone 10-1
            Spreadsheet.Document.Worksheets[0].Cells["I76"].Value = ""; //Zone 10-2
            Spreadsheet.Document.Worksheets[0].Cells["B79"].Value = ""; //Zone 10-3
            Spreadsheet.Document.Worksheets[0].Cells["I79"].Value = ""; //Zone 10-4
            Spreadsheet.Document.Worksheets[0].Cells["P55"].Value = ""; //Zone 11
            Spreadsheet.Document.Worksheets[0].Cells["P68"].Value = ""; //Zone 12
            Spreadsheet.Document.Worksheets[0].Cells["P75"].Value = ""; //Zone 13-1
            Spreadsheet.Document.Worksheets[0].Cells["V75"].Value = ""; //Zone 13-2
            Spreadsheet.Document.Worksheets[0].Cells["AB75"].Value = ""; //Zone 13-3






            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    int i = 5;
                    bool _ilk = true;
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[SP_0RPT_LOJ_OP_REPORTS_BL_AIR_LINEANDHEADERS]";
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@COMP", Request.QueryString["Comp"]);
                    cmd.Parameters.AddWithValue("@BLNO", Request.QueryString["BlNo"]);

                    using (SqlDataAdapter adptr = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        adptr.Fill(ds);

                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            Spreadsheet.Document.Worksheets[0].Cells["B2"].Value = row["BLNo1"].ToString(); //BLNo1
                            Spreadsheet.Document.Worksheets[0].Cells["AG2"].Value = row["BLNo2"].ToString(); //BLNo2
                            HiddenBlNo1.Value = row["BLNo1"].ToString();
                            HiddenBlNo2.Value = row["BLNo2"].ToString();

                            Spreadsheet.Document.Worksheets[0].Cells["B5"].Value = row["ShipName"].ToString(); //ShipName
                            Spreadsheet.Document.Worksheets[0].Cells["B6"].Value = row["ShipAddress"].ToString(); //ShipAddress
                            Spreadsheet.Document.Worksheets[0].Cells["B10"].Value = row["ShipTelNo"].ToString(); //ShipTelNo
                            Spreadsheet.Document.Worksheets[0].Cells["U6"].Value = row["IssuedBy"].ToString(); //IssuedBy
                            Spreadsheet.Document.Worksheets[0].Cells["B13"].Value = row["CneeName"].ToString(); //CneeName
                            Spreadsheet.Document.Worksheets[0].Cells["B14"].Value = row["CneeAddress"].ToString(); //CneeAddress
                            Spreadsheet.Document.Worksheets[0].Cells["B18"].Value = row["CneeTelNo"].ToString(); //CneeTelNo
                            Spreadsheet.Document.Worksheets[0].Cells["B20"].Value = row["Carrier"].ToString(); //Carrier
                            Spreadsheet.Document.Worksheets[0].Cells["U20"].Value = row["Zone 1"].ToString(); //Zone 1
                            Spreadsheet.Document.Worksheets[0].Cells["C25"].Value = row["IadataCode"].ToString(); //IadataCode
                            Spreadsheet.Document.Worksheets[0].Cells["L25"].Value = row["AllNo"].ToString(); //AllNo
                            Spreadsheet.Document.Worksheets[0].Cells["C28"].Value = row["PolName"].ToString(); //PolName
                            Spreadsheet.Document.Worksheets[0].Cells["U28"].Value = row["AgentRef"].ToString(); //AgentRef
                            Spreadsheet.Document.Worksheets[0].Cells["AB29"].Value = row["Optional1"].ToString(); //Optional1
                            Spreadsheet.Document.Worksheets[0].Cells["AH29"].Value = row["Optional2"].ToString(); //Optional2
                            Spreadsheet.Document.Worksheets[0].Cells["B31"].Value = row["Zone 2-1"].ToString(); //Zone 2-1
                            Spreadsheet.Document.Worksheets[0].Cells["E31"].Value = row["Zone 2-2"].ToString(); //Zone 2-2
                            Spreadsheet.Document.Worksheets[0].Cells["L31"].Value = row["Zone 2-3"].ToString(); //Zone 2-3
                            Spreadsheet.Document.Worksheets[0].Cells["N31"].Value = row["Zone 2-4"].ToString(); //Zone 2-4
                            Spreadsheet.Document.Worksheets[0].Cells["P31"].Value = row["Zone 2-5"].ToString(); //Zone 2-5
                            Spreadsheet.Document.Worksheets[0].Cells["R31"].Value = row["Zone 2-6"].ToString(); //Zone 2-6
                            Spreadsheet.Document.Worksheets[0].Cells["T31"].Value = row["Currency"].ToString(); //Currency
                            Spreadsheet.Document.Worksheets[0].Cells["V31"].Value = row["ChargerCode"].ToString(); //Charger Code
                            Spreadsheet.Document.Worksheets[0].Cells["W31"].Value = row["WTPP"].ToString(); //WTPP
                            Spreadsheet.Document.Worksheets[0].Cells["X31"].Value = row["WTColl"].ToString(); //WTColl
                            Spreadsheet.Document.Worksheets[0].Cells["Y31"].Value = row["OtherPP"].ToString(); //OtherPP
                            Spreadsheet.Document.Worksheets[0].Cells["Z31"].Value = row["OtherColl"].ToString(); //OtherColl
                            Spreadsheet.Document.Worksheets[0].Cells["AA31"].Value = row["DVCarrier"].ToString(); //DVCarrier
                            Spreadsheet.Document.Worksheets[0].Cells["AG31"].Value = row["DVCustoms"].ToString(); //DVCustoms
                            Spreadsheet.Document.Worksheets[0].Cells["B34"].Value = row["Zone 3-1"].ToString(); //Zone 3-1
                            Spreadsheet.Document.Worksheets[0].Cells["K34"].Value = row["Zone 3-2"].ToString(); //Zone 3-2
                            Spreadsheet.Document.Worksheets[0].Cells["O34"].Value = row["Zone 3-3"].ToString(); //Zone 3-3
                            Spreadsheet.Document.Worksheets[0].Cells["B37"].Value = row["Zone 4"].ToString(); //Zone 4
                            Spreadsheet.Document.Worksheets[0].Cells["B43"].Value = row["Zone 7"].ToString(); //Zone 7
                            Spreadsheet.Document.Worksheets[0].Cells["B52"].Value = row["Zone 8-1"].ToString(); //Zone 8-1
                            Spreadsheet.Document.Worksheets[0].Cells["E52"].Value = row["Zone 8-2"].ToString(); //Zone 8-2
                            Spreadsheet.Document.Worksheets[0].Cells["C55"].Value = row["Zone 9-1"].ToString(); //Zone 9-1
                            Spreadsheet.Document.Worksheets[0].Cells["C58"].Value = row["Zone 9-2"].ToString(); //Zone 9-2
                            Spreadsheet.Document.Worksheets[0].Cells["C61"].Value = row["Zone 9-3"].ToString(); //Zone 9-3
                            Spreadsheet.Document.Worksheets[0].Cells["C64"].Value = row["Zone 9-4"].ToString(); //Zone 9-4
                            Spreadsheet.Document.Worksheets[0].Cells["C67"].Value = row["Zone 9-5"].ToString(); //Zone 9-5
                            Spreadsheet.Document.Worksheets[0].Cells["C70"].Value = row["Zone 9-6"].ToString(); //Zone 9-6
                            Spreadsheet.Document.Worksheets[0].Cells["C73"].Value = row["Zone 9-7"].ToString(); //Zone 9-7
                            Spreadsheet.Document.Worksheets[0].Cells["B76"].Value = row["Zone 10-1"].ToString(); //Zone 10-1
                            Spreadsheet.Document.Worksheets[0].Cells["I76"].Value = row["Zone 10-2"].ToString(); //Zone 10-2
                            Spreadsheet.Document.Worksheets[0].Cells["B79"].Value = row["Zone 10-3"].ToString(); //Zone 10-3
                            Spreadsheet.Document.Worksheets[0].Cells["I79"].Value = row["Zone 10-4"].ToString(); //Zone 10-4
                            Spreadsheet.Document.Worksheets[0].Cells["P55"].Value = row["Zone 11"].ToString(); //Zone 11
                            Spreadsheet.Document.Worksheets[0].Cells["P68"].Value = row["Zone 12"].ToString(); //Zone 12
                            if (!string.IsNullOrEmpty(row["Zone 13-1"].ToString()))
                                Spreadsheet.Document.Worksheets[0].Cells["P75"].Value = string.Format("{0:dd.MM.yyyy}", row["Zone 13-1"]); //Zone 13-1
                            Spreadsheet.Document.Worksheets[0].Cells["V75"].Value = row["Zone 13-2"].ToString(); //Zone 13-2
                            Spreadsheet.Document.Worksheets[0].Cells["AB75"].Value = row["Zone 13-3"].ToString(); //Zone 13-3
                        }

                        i = 5;
                        foreach (DataRow row in ds.Tables[1].Rows)
                        {
                            Spreadsheet.Document.Worksheets[1].Cells["B" + i.ToString()].Value = ""; //Zone 5-1
                            Spreadsheet.Document.Worksheets[1].Cells["D" + i.ToString()].Value = ""; //Zone 5-2
                            Spreadsheet.Document.Worksheets[1].Cells["H" + i.ToString()].Value = ""; //Zone 5-3
                            Spreadsheet.Document.Worksheets[1].Cells["J" + i.ToString()].Value = ""; //Zone 5-4
                            Spreadsheet.Document.Worksheets[1].Cells["K" + i.ToString()].Value = ""; //Zone 5-5
                            Spreadsheet.Document.Worksheets[1].Cells["P" + i.ToString()].Value = ""; //Zone 5-6
                            Spreadsheet.Document.Worksheets[1].Cells["U" + i.ToString()].Value = ""; //Zone 5-7
                            Spreadsheet.Document.Worksheets[1].Cells["Z" + i.ToString()].Value = ""; //Zone 5-8
                            Spreadsheet.Document.Worksheets[1].Cells["AF" + i.ToString()].Value = ""; //Zone 6

                            Spreadsheet.Document.Worksheets[1].Cells["B" + i.ToString()].Value = row["Zone 5-1"].ToString(); //Zone 5-1
                            Spreadsheet.Document.Worksheets[1].Cells["D" + i.ToString()].Value = row["Zone 5-2"].ToString(); //Zone 5-2
                            Spreadsheet.Document.Worksheets[1].Cells["H" + i.ToString()].Value = row["Zone 5-3"].ToString(); //Zone 5-3
                            Spreadsheet.Document.Worksheets[1].Cells["J" + i.ToString()].Value = row["Zone 5-4"].ToString(); //Zone 5-4
                            Spreadsheet.Document.Worksheets[1].Cells["K" + i.ToString()].Value = row["Zone 5-5"].ToString(); //Zone 5-5
                            Spreadsheet.Document.Worksheets[1].Cells["P" + i.ToString()].Value = row["Zone 5-6"].ToString(); //Zone 5-6
                            Spreadsheet.Document.Worksheets[1].Cells["U" + i.ToString()].Value = row["Zone 5-7"].ToString(); //Zone 5-7
                            Spreadsheet.Document.Worksheets[1].Cells["Z" + i.ToString()].Value = row["Zone 5-8"].ToString(); //Zone 5-8
                            Spreadsheet.Document.Worksheets[1].Cells["AF" + i.ToString()].Value = row["Zone 6"].ToString(); //Zone 6

                            i++;
                            if (_ilk)
                            {
                                txtLineNos.Text = row["BLGRUP"].ToString();
                                _ilk = false;
                            }
                            else
                                txtLineNos.Text += "," + row["BLGRUP"].ToString();

                        }


                    }
                }
            }
        }

        void LoadOtherData()
        {
            txtLineNos.Text = string.Empty;
            Spreadsheet.WorkDirectory = Server.MapPath("~") + CurrentFolderName(_obj.Comp) + @"\RAPORLAR OP\BL EXCEL";
            string excelworkdirectory = Server.MapPath("~") + CurrentFolderName(_obj.Comp) + @"\RAPORLAR OP\BL EXCEL";

            //if (File.Exists(Path.Combine(excelworkdirectory, Request.QueryString["Comp"] + Request.QueryString["BlNo"] + ".xlsx")))
            //{
            //    File.Delete(Path.Combine(excelworkdirectory, Request.QueryString["Comp"] + Request.QueryString["BlNo"] + ".xlsx"));
            //}

            //if (!File.Exists(Path.Combine(excelworkdirectory, Request.QueryString["Comp"] + Request.QueryString["BlNo"] + ".xlsx")))
            File.Copy(Path.Combine(Server.MapPath("~") + CurrentFolderName(_obj.Comp) + @"\RAPORLAR OP", "OCEAN BL EXCEL ÇALIŞMALARI GELISTIRME.xlsx")
                , Path.Combine(excelworkdirectory, Request.QueryString["Comp"] + Request.QueryString["BlNo"] + ".xlsx"), true);

            var filePath = Path.Combine(excelworkdirectory, Request.QueryString["Comp"] + Request.QueryString["BlNo"] + ".xlsx");



            Spreadsheet.Open(filePath);

            Spreadsheet.Document.Worksheets.ActiveWorksheet = Spreadsheet.Document.Worksheets[0];


            Spreadsheet.Document.Worksheets[0].Cells["B5"].Value = ""; //Zone 1-1
            Spreadsheet.Document.Worksheets[0].Cells["B6"].Value = ""; //Zone 1-2

            Spreadsheet.Document.Worksheets[0].Cells["B10"].Value = ""; //Zone 1-3
            Spreadsheet.Document.Worksheets[0].Cells["B13"].Value = ""; //Zone 2-1
            Spreadsheet.Document.Worksheets[0].Cells["B14"].Value = ""; //Zone 2-2
            Spreadsheet.Document.Worksheets[0].Cells["B18"].Value = ""; //Zone 2-3
            Spreadsheet.Document.Worksheets[0].Cells["B21"].Value = ""; //Zone 3-1
            Spreadsheet.Document.Worksheets[0].Cells["B22"].Value = ""; //Zone 3-2
            Spreadsheet.Document.Worksheets[0].Cells["B26"].Value = ""; //Zone 3-3
            Spreadsheet.Document.Worksheets[0].Cells["B29"].Value = ""; //Zone 10-1
            Spreadsheet.Document.Worksheets[0].Cells["K29"].Value = ""; //Zone 10-2
            Spreadsheet.Document.Worksheets[0].Cells["B34"].Value = ""; //Zone 10-5
            Spreadsheet.Document.Worksheets[0].Cells["K34"].Value = ""; //Zone 10-6
            Spreadsheet.Document.Worksheets[0].Cells["T4"].Value = ""; //Zone 4
            Spreadsheet.Document.Worksheets[0].Cells["AD4"].Value = ""; //Zone 5
            Spreadsheet.Document.Worksheets[0].Cells["T8"].Value = ""; //Zone 6
            Spreadsheet.Document.Worksheets[0].Cells["AD8"].Value = ""; //Zone 7
            Spreadsheet.Document.Worksheets[0].Cells["T12"].Value = ""; //Zone 8-1
            Spreadsheet.Document.Worksheets[0].Cells["T13"].Value = ""; //Zone 8-2
            Spreadsheet.Document.Worksheets[0].Cells["T18"].Value = ""; //Zone 8-3
            Spreadsheet.Document.Worksheets[0].Cells["T22"].Value = ""; //Zone 9
            Spreadsheet.Document.Worksheets[0].Cells["T29"].Value = ""; //Zone 10-3
            Spreadsheet.Document.Worksheets[0].Cells["AD29"].Value = ""; //Zone 10-4
            Spreadsheet.Document.Worksheets[0].Cells["T34"].Value = ""; //Zone 10-7
            Spreadsheet.Document.Worksheets[0].Cells["AD34"].Value = ""; //Zone 10-8
            Spreadsheet.Document.Worksheets[0].Cells["B38"].Value = ""; //Zone 18
            Spreadsheet.Document.Worksheets[0].Cells["B45"].Value = ""; //Zone 14-1
            Spreadsheet.Document.Worksheets[0].Cells["B48"].Value = ""; //Zone 14-2
            Spreadsheet.Document.Worksheets[0].Cells["B54"].Value = ""; //Zone 15
            Spreadsheet.Document.Worksheets[0].Cells["P44"].Value = ""; //Zone 16-1
            Spreadsheet.Document.Worksheets[0].Cells["P48"].Value = ""; //Zone 16-2
            Spreadsheet.Document.Worksheets[0].Cells["P63"].Value = ""; //Zone 17-1
            Spreadsheet.Document.Worksheets[0].Cells["P65"].Value = ""; //Zone 17-3
            Spreadsheet.Document.Worksheets[0].Cells["P67"].Value = ""; //Zone 17-4
            Spreadsheet.Document.Worksheets[0].Cells["U63"].Value = ""; //Zone 17-2


            for (int i = 5; i < 9; i++)
            {
                Spreadsheet.Document.Worksheets[1].Cells["B" + i.ToString()].Value = ""; //Zone 11-1
                Spreadsheet.Document.Worksheets[1].Cells["H" + i.ToString()].Value = ""; //Zone 11-2
                Spreadsheet.Document.Worksheets[1].Cells["K" + i.ToString()].Value = ""; //Zone 11-2A
                Spreadsheet.Document.Worksheets[1].Cells["L" + i.ToString()].Value = ""; //Zone 11-3
                Spreadsheet.Document.Worksheets[1].Cells["AA" + i.ToString()].Value = ""; //Zone 11-4
                Spreadsheet.Document.Worksheets[1].Cells["AD" + i.ToString()].Value = ""; //Zone 11-4A
                Spreadsheet.Document.Worksheets[1].Cells["AE" + i.ToString()].Value = ""; //Zone 11-5
                Spreadsheet.Document.Worksheets[1].Cells["AH" + i.ToString()].Value = ""; //Zone 11-5A
            }


            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    int i = 5;
                    bool _ilk = true;
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[SP_0RPT_LOJ_OP_REPORTS_BL_OCEAN_LINEANDHEADERS]";
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@COMP", Request.QueryString["Comp"]);
                    cmd.Parameters.AddWithValue("@BLNO", Request.QueryString["BlNo"]);

                    using (SqlDataAdapter adptr = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        adptr.Fill(ds);

                        foreach (DataRow row in ds.Tables[0].Rows)
                        {

                            Spreadsheet.Document.Worksheets[0].Cells["B5"].Value = row["Zone 1-1"].ToString(); //Zone 1-1
                            Spreadsheet.Document.Worksheets[0].Cells["B6"].Value = row["Zone 1-2"].ToString(); //Zone 1-2

                            Spreadsheet.Document.Worksheets[0].Cells["B10"].Value = row["Zone 1-3"].ToString(); //Zone 1-3
                            Spreadsheet.Document.Worksheets[0].Cells["B13"].Value = row["Zone 2-1"].ToString(); //Zone 2-1
                            Spreadsheet.Document.Worksheets[0].Cells["B14"].Value = row["Zone 2-2"].ToString(); //Zone 2-2
                            Spreadsheet.Document.Worksheets[0].Cells["B18"].Value = row["Zone 2-3"].ToString(); //Zone 2-2
                            Spreadsheet.Document.Worksheets[0].Cells["B21"].Value = row["Zone 3-1"].ToString(); //Zone 3-1
                            Spreadsheet.Document.Worksheets[0].Cells["B22"].Value = row["Zone 3-2"].ToString(); //Zone 3-2
                            Spreadsheet.Document.Worksheets[0].Cells["B26"].Value = row["Zone 3-3"].ToString(); //Zone 3-3
                            Spreadsheet.Document.Worksheets[0].Cells["B29"].Value = row["Zone 10-1"].ToString(); //Zone 10-1
                            Spreadsheet.Document.Worksheets[0].Cells["K29"].Value = row["Zone 10-2"].ToString(); //Zone 10-2
                            Spreadsheet.Document.Worksheets[0].Cells["B34"].Value = row["Zone 10-5"].ToString(); //Zone 10-5
                            Spreadsheet.Document.Worksheets[0].Cells["K34"].Value = row["Zone 10-6"].ToString(); //Zone 10-6
                            Spreadsheet.Document.Worksheets[0].Cells["T4"].Value = row["Zone 4"].ToString(); //Zone 4
                            Spreadsheet.Document.Worksheets[0].Cells["AD4"].Value = row["Zone 5"].ToString(); //Zone 5
                            Spreadsheet.Document.Worksheets[0].Cells["T8"].Value = row["Zone 6"].ToString(); //Zone 6
                            Spreadsheet.Document.Worksheets[0].Cells["AD8"].Value = row["Zone 7"].ToString(); //Zone 7
                            Spreadsheet.Document.Worksheets[0].Cells["T12"].Value = row["Zone 8-1"].ToString(); //Zone 8-1
                            Spreadsheet.Document.Worksheets[0].Cells["T13"].Value = row["Zone 8-2"].ToString(); //Zone 8-2
                            Spreadsheet.Document.Worksheets[0].Cells["T18"].Value = row["Zone 8-3"].ToString(); //Zone 8-3
                            Spreadsheet.Document.Worksheets[0].Cells["T22"].Value = row["Zone 9"].ToString(); //Zone 9
                            Spreadsheet.Document.Worksheets[0].Cells["T29"].Value = row["Zone 10-3"].ToString(); //Zone 10-3
                            Spreadsheet.Document.Worksheets[0].Cells["AD29"].Value = row["Zone 10-4"].ToString(); //Zone 10-4
                            Spreadsheet.Document.Worksheets[0].Cells["T34"].Value = row["Zone 10-7"].ToString(); //Zone 10-7
                            Spreadsheet.Document.Worksheets[0].Cells["AD34"].Value = row["Zone 10-8"].ToString(); //Zone 10-8
                            Spreadsheet.Document.Worksheets[0].Cells["B38"].Value = row["Zone 18"].ToString(); //Zone 18
                            Spreadsheet.Document.Worksheets[0].Cells["B45"].Value = row["Zone 14-1"].ToString(); //Zone 14-1
                            Spreadsheet.Document.Worksheets[0].Cells["B48"].Value = row["Zone 14-2"].ToString(); //Zone 14-2
                            Spreadsheet.Document.Worksheets[0].Cells["B54"].Value = row["Zone 15"].ToString(); //Zone 15
                            Spreadsheet.Document.Worksheets[0].Cells["P44"].Value = row["Zone 16-1"].ToString(); //Zone 16-1
                            Spreadsheet.Document.Worksheets[0].Cells["P48"].Value = row["Zone 16-2"].ToString(); //Zone 16-2
                            Spreadsheet.Document.Worksheets[0].Cells["P63"].Value = row["Zone 17-1"].ToString(); //Zone 17-1
                            Spreadsheet.Document.Worksheets[0].Cells["P65"].Value = row["Zone 17-3"].ToString(); //Zone 17-3
                            Spreadsheet.Document.Worksheets[0].Cells["P67"].Value = row["Zone 17-4"].ToString(); //Zone 17-4
                            if (!string.IsNullOrEmpty(row["Zone 17-2"].ToString()))
                                Spreadsheet.Document.Worksheets[0].Cells["U63"].Value = string.Format("{0:dd.MM.yyyy}", row["Zone 17-2"]); //Zone 17-2


                        }

                        i = 5;
                        foreach (DataRow row in ds.Tables[1].Rows)
                        {

                            Spreadsheet.Document.Worksheets[1].Cells["B" + i.ToString()].Value = row["Zone 11-1"].ToString(); //Zone 11-1
                            Spreadsheet.Document.Worksheets[1].Cells["H" + i.ToString()].Value = row["Zone 11-2"].ToString(); //Zone 11-2
                            Spreadsheet.Document.Worksheets[1].Cells["K" + i.ToString()].Value = row["Zone 11-2A"].ToString(); //Zone 11-2A
                            Spreadsheet.Document.Worksheets[1].Cells["L" + i.ToString()].Value = row["Zone 11-3"].ToString(); //Zone 11-3
                            Spreadsheet.Document.Worksheets[1].Cells["AA" + i.ToString()].Value = row["Zone 11-4"].ToString(); //Zone 11-4
                            Spreadsheet.Document.Worksheets[1].Cells["AD" + i.ToString()].Value = row["Zone 11-4A"].ToString(); //Zone 11-4A
                            Spreadsheet.Document.Worksheets[1].Cells["AE" + i.ToString()].Value = row["Zone 11-5"].ToString(); //Zone 11-5
                            Spreadsheet.Document.Worksheets[1].Cells["AH" + i.ToString()].Value = row["Zone 11-5A"].ToString(); //Zone 11-5A


                            i++;
                            if (_ilk)
                            {
                                txtLineNos.Text = row["SATIR"].ToString();
                                _ilk = false;
                            }
                            else
                                txtLineNos.Text += "," + row["SATIR"].ToString();

                        }


                    }
                }
            }
        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        protected void menu1_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("Save"))
            {

                if (Request.QueryString["MainType"] == "AIR")
                    SaveAir();
                else
                    SaveOther();

            }

        }

        void SaveAir()
        {
            string[] _params = { Request.QueryString["BlNo"], Request.QueryString["Comp"],
                HiddenBlNo1.Value, HiddenBlNo2.Value,
                Spreadsheet.Document.Worksheets[0].Cells["B5"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["B6"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["B10"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["U6"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["B13"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["B14"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["B18"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["B20"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["U20"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["C25"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["L25"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["C28"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["U28"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["AB29"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["AH29"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["B31"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["E31"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["L31"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["N31"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["P31"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["R31"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["T31"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["V31"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["W31"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["X31"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["Y31"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["Z31"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["AA31"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["AG31"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["B34"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["K34"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["O34"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["B37"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["B43"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["B52"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["E52"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["C55"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["C58"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["C61"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["C64"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["C67"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["C70"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["C73"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["B76"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["I76"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["B79"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["I79"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["P55"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["P68"].Value.ToString(),
                m.DateToStringNAVType(Spreadsheet.Document.Worksheets[0].Cells["P75"].Value.ToString()),
                Spreadsheet.Document.Worksheets[0].Cells["V75"].Value.ToString(),
                Spreadsheet.Document.Worksheets[0].Cells["AB75"].Value.ToString(),
                _obj.UserName.ToString() };
            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50014", "CreateAirBlZone§" + string.Join("§", _params), _obj.Comp);



            if (string.IsNullOrEmpty(txtLineNos.Text))
            {

                string[] _params2 = { Request.QueryString["BlNo"], Request.QueryString["Comp"],
                Spreadsheet.Document.Worksheets[1].Cells["B5"].Value.ToString(),
                Spreadsheet.Document.Worksheets[1].Cells["D5"].Value.ToString(),
                Spreadsheet.Document.Worksheets[1].Cells["H5"].Value.ToString(),
                Spreadsheet.Document.Worksheets[1].Cells["J5"].Value.ToString(),
                Spreadsheet.Document.Worksheets[1].Cells["K5"].Value.ToString(),
                Spreadsheet.Document.Worksheets[1].Cells["P5"].Value.ToString(),
                Spreadsheet.Document.Worksheets[1].Cells["U5"].Value.ToString(),
                Spreadsheet.Document.Worksheets[1].Cells["Z5"].Value.ToString(),
                Spreadsheet.Document.Worksheets[1].Cells["AF5"].Value.ToString(), "",
                _obj.UserName.ToString() };
                GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50014", "CreateAirBlZoneLINE§" + string.Join("§", _params2), _obj.Comp);

            }
            else
            {
                string[] _array = txtLineNos.Text.Split(',');
                int iindex = 5;
                foreach (string _val in _array)
                {
                    string[] _params3 = {Request.QueryString["BlNo"], Request.QueryString["Comp"],
                        Spreadsheet.Document.Worksheets[1].Cells["B" + iindex.ToString()].Value.ToString(),
                        Spreadsheet.Document.Worksheets[1].Cells["D" + iindex.ToString()].Value.ToString(),
                        Spreadsheet.Document.Worksheets[1].Cells["H" + iindex.ToString()].Value.ToString(),
                        Spreadsheet.Document.Worksheets[1].Cells["J" + iindex.ToString()].Value.ToString(),
                        Spreadsheet.Document.Worksheets[1].Cells["K" + iindex.ToString()].Value.ToString(),
                        Spreadsheet.Document.Worksheets[1].Cells["P" + iindex.ToString()].Value.ToString(),
                        Spreadsheet.Document.Worksheets[1].Cells["U" + iindex.ToString()].Value.ToString(),
                        Spreadsheet.Document.Worksheets[1].Cells["Z" + iindex.ToString()].Value.ToString(),
                        Spreadsheet.Document.Worksheets[1].Cells["AF" + iindex.ToString()].Value.ToString(), _val,
                        _obj.UserName.ToString()};
                    GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50014", "CreateAirBlZoneLINE§" + string.Join("§", _params3), _obj.Comp);

                    iindex++;
                }
            }

            Response.Redirect("~/ExcelPopup.aspx?Comp=" + Request.QueryString["Comp"] + "&BlNo=" + Request.QueryString["BlNo"] + "&MainType=AIR&GId=" + Request.QueryString["GId"]);
        }

        void SaveOther()
        {

            string[] _params = {Request.QueryString["BlNo"], Request.QueryString["Comp"],
            Spreadsheet.Document.Worksheets[0].Cells["B5"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["B6"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["B10"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["B13"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["B14"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["B18"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["B21"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["B22"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["B26"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["B29"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["K29"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["B34"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["K34"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["T4"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["AD4"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["T8"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["AD8"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["T12"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["T13"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["T18"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["T22"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["T29"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["AD29"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["T34"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["AD34"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["B38"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["B45"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["B48"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["B54"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["P44"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["P48"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["P63"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["P65"].Value.ToString()
            , Spreadsheet.Document.Worksheets[0].Cells["P67"].Value.ToString()
            , m.DateToStringNAVType(Spreadsheet.Document.Worksheets[0].Cells["U63"].Value.ToString()),
                _obj.UserName.ToString()};
            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50014", "CreateOceanBLZone§" + string.Join("§", _params), _obj.Comp);



            if (string.IsNullOrEmpty(txtLineNos.Text))
            {
                string[] _params1 = {Request.QueryString["BlNo"], Request.QueryString["Comp"],
                                     Spreadsheet.Document.Worksheets[1].Cells["B5"].Value.ToString(),
                                    Spreadsheet.Document.Worksheets[1].Cells["H5"].Value.ToString(),
                                    Spreadsheet.Document.Worksheets[1].Cells["L5"].Value.ToString(),
                                    Spreadsheet.Document.Worksheets[1].Cells["AA5"].Value.ToString(),
                                    Spreadsheet.Document.Worksheets[1].Cells["AE5"].Value.ToString(),
                                    Spreadsheet.Document.Worksheets[1].Cells["K5"].Value.ToString(),
                                    Spreadsheet.Document.Worksheets[1].Cells["AD5"].Value.ToString(),
                                    Spreadsheet.Document.Worksheets[1].Cells["AH5"].Value.ToString(), "",
                                    _obj.UserName.ToString()};
                GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50014", "CreateOceanBLZoneLINE§" + string.Join("§", _params1), _obj.Comp);

            }
            else
            {

                string[] _array = txtLineNos.Text.Split(',');
                int iindex = 5;
                foreach (string _val in _array)
                {
                    string[] _params2 = {Request.QueryString["BlNo"], Request.QueryString["Comp"],
                                         Spreadsheet.Document.Worksheets[1].Cells["B" + iindex.ToString()].Value.ToString(),
                                        Spreadsheet.Document.Worksheets[1].Cells["H" + iindex.ToString()].Value.ToString(),
                                        Spreadsheet.Document.Worksheets[1].Cells["L" + iindex.ToString()].Value.ToString(),
                                        Spreadsheet.Document.Worksheets[1].Cells["AA" + iindex.ToString()].Value.ToString(),
                                        Spreadsheet.Document.Worksheets[1].Cells["AE" + iindex.ToString()].Value.ToString(),
                                        Spreadsheet.Document.Worksheets[1].Cells["K" + iindex.ToString()].Value.ToString(),
                                        Spreadsheet.Document.Worksheets[1].Cells["AD" + iindex.ToString()].Value.ToString(),
                                        Spreadsheet.Document.Worksheets[1].Cells["AH" + iindex.ToString()].Value.ToString(), _val,
                                         _obj.UserName.ToString()};

                    GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50014", "CreateOceanBLZoneLINE§" + string.Join("§", _params2), _obj.Comp);

                    iindex++;

                    //MessageBox(Request.QueryString["BlNo"] + "," + _val);
                }
            }

            Response.Redirect("~/ExcelPopup.aspx?Comp=" + Request.QueryString["Comp"] + "&BlNo=" + Request.QueryString["BlNo"] + "&MainType=OTHER&GId=" + Request.QueryString["GId"]);
        }

    }
}