﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using MSS1.WSGeneric;
using System.ServiceModel;
using MSS1.Codes;

namespace MSS1
{
    public partial class sifre : Bases
    { 
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        SqlConnection baglanti;
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        string customSirket = "GAC";
        SessionBase _obj;
        public ws_baglantı ws = new ws_baglantı();

        #region Dil Getir
        public void dilgetir()
        {
            lblBaslik.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "sifre", "lblBaslik");
            Label1.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "sifre", "Label1");
            Label2.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "sifre", "Label2");
            Label3.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "sifre", "Label3");
            Label4.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "sifre", "Label4");
            Button1.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "sifre", "Button1");
        }
        #endregion


        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            dilgetir();
            if (!IsPostBack)
            {
                txtkad.Text = _obj.UserName;
                txtkad.Enabled = false;
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            if (txtysifre.Text != string.Empty )
            { 
                if (txtysifret.Text == txtysifret.Text)
                {
                    if (txtysifre.Text != string.Empty)
                    {
                        string[] _params = { txtkad.Text, txtesifre.Text, txtysifre.Text, txtysifret.Text };
                        string result = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "ChangeWebPassword§" + string.Join("§", _params), customSirket);
                        string[] spl = result.Split('|');
                        if (spl[0] == "OK")
                        {
                            Response.Redirect("Default.aspx");
                        }
                        else
                        {
                            MessageBox(spl[1].ToString());
                        }
                        
                    }
                    else
                    {
                        MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "sifre", "msg433"));
                    }

                }
                else
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "sifre", "msg434"));
                }
               


            }
            else
            {
                lblMesaj.Visible = true;
                txtysifre.BorderColor = System.Drawing.Color.Red;
                Language l = new Language();
                lblMesaj.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "sifre", "lblMesaj");
                txtysifre.Text = string.Empty;
                txtysifret.Text = string.Empty;
                txtesifre.Text = string.Empty;
            }

        }
        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }
        protected void txtysifre_TextChanged(object sender, EventArgs e)
        {
            if (lblMesaj.Visible == true)
            {
                txtysifre.CssClass = "textbox";
            }
        }
        protected void txtesifre_TextChanged(object sender, EventArgs e)
        {
            if (lblMesaj.Visible == true)
            {
                txtesifre.CssClass = "textbox";
            }
        }
        protected void txtysifret_TextChanged(object sender, EventArgs e)
        {
            if (lblMesaj.Visible == true)
            {
                txtysifre.CssClass = "textbox";
            }
        }

       

    }
}