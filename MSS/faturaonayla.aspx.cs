﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using MSS1.WSGeneric;
using System.ServiceModel;
using System.Data;
using MSS1.Codes;

namespace MSS1
{
    public partial class faturaonayla : Bases

    {
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        SqlConnection baglanti;
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        SessionBase _obj;

        public ws_baglantı ws = new ws_baglantı();

        #region Dil Getir
        public void dilgetir()
        {
            lblBelgeTarihiB.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "faturaonayla", "lblBelgeTarihiB");
            lblDovizKuru.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "faturaonayla", "lblDovizKuru");
            btnonayla.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "faturaonayla", "btnonayla");
        }
        #endregion

        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            _obj = TabSessions.ControlState(this.Page, Request.QueryString["GId"].ToString());
            dilgetir();
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            if (!IsPostBack)
            {
                tarihkurgetir(_obj.Comp.ToString(), Request.QueryString["BelgeNo"], 0);
            }
        }
        protected void txtbelgetarihi_TextChanged(object sender, EventArgs e)
        {
            f.kurgetir(_obj.Comp.ToString(), txtkur, txtbelgetarihi.Text, Request.QueryString["DCins"]);
        }
        private void tarihkurgetir(string sirket, string sipno, int tur)
        {
            txtbelgetarihi.Text = DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year;
            //txtkur.Text = Convert.ToDouble(sqr["kur"]).ToString();
            f.kurgetir(_obj.Comp.ToString(), txtkur, txtbelgetarihi.Text, Request.QueryString["DCins"]);


        }
        string[] tcevir;
        string tcevirtum = "";
        protected void btnonayla_Click(object sender, EventArgs e)
        {

            if (txtbelgetarihi.Text != string.Empty)
            {

                tcevir = txtbelgetarihi.Text.Split('.');
                tcevirtum += tcevir[1] + tcevir[0] + tcevir[2];
                DateTime dt1 = new DateTime(Convert.ToInt32(tcevir[2]), Convert.ToInt32(tcevir[1]), Convert.ToInt32(tcevir[0]));

                if (dt1 < DateTime.Now)
                {


                    if (Convert.ToDecimal(txtkur.Text) < 10)
                    {

                        string _dc = Request.QueryString["DCins"];
                        string _pfr = "0";
                        try
                        {
                            _pfr = Request.QueryString["PFR"];
                        }
                        catch (Exception)
                        {
                            _pfr = "0";
                        }
                        string[] _params = { Request.QueryString["BelgeNo"], new Methods().DateToStringNAVType(txtbelgetarihi.Text), txtkur.Text, _dc,
                            _obj.NameSurName.ToString() };
                        GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatisFatTarihGuncelle§" + string.Join("§", _params), _obj.Comp);


                        if (!ClientScript.IsStartupScriptRegistered("JSFaturaAcik"))
                        {
                            if (_pfr == "1")
                            {
                                ClientScript.RegisterStartupScript(GetType(), "JSFaturaAcik", @"window.opener.parent.RefreshAllCollapseAndPageRefresh();self.close();", true);
                            }
                            else
                            {
                                ClientScript.RegisterStartupScript(GetType(), "JSFaturaAcik", @"window.opener.parent.RefreshAllCollapse();self.close();", true);
                            }
                        }
                        //ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: proceed();", true);
                    }
                    else
                    {
                        MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "faturaonayla", "msg129"));
                    }
                }
                else
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "faturaonayla", "msg130"));
                }

            }
        }
        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }
    }
}