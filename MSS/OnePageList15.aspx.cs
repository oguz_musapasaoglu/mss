﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.Data;
using System.Collections;
using System.IO;
using System.Threading;
using System.Diagnostics;
using MSS1.Codes;

namespace MSS1
{
    public partial class OnePageList15 : System.Web.UI.Page
    {
        Language l = new Language();
        Hashtable copiedValues = null;
        ServiceUtilities services = new ServiceUtilities();
        fonksiyonlar f = new fonksiyonlar();
        SessionBase _obj;
        static string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        public string currentP1 { get; set; }
        public string OBJ1_pageId { get; set; }
        public string OBJ2_pageId { get; set; }
        public string OBJ3_pageId { get; set; }
        public string OBJ4_pageId { get; set; }
        public string OBJ5_pageId { get; set; }
        public string OBJ6_pageId { get; set; }
        public string OBJ7_pageId { get; set; }
        public string OBJ8_pageId { get; set; }
        public string OBJ9_pageId { get; set; }
        public string OBJ1_fieldId { get; set; }
        public string OBJ2_fieldId { get; set; }
        public string OBJ3_fieldId { get; set; }
        public string OBJ4_fieldId { get; set; }
        public string OBJ5_fieldId { get; set; }
        public string OBJ6_fieldId { get; set; }
        public string OBJ7_fieldId { get; set; }
        public string OBJ8_fieldId { get; set; }
        public string OBJ9_fieldId { get; set; }
        public string pageFaturaStatu
        {
            get
            {
                string _result = string.Empty;
                if (OBJ1_fieldId == "J50010") //Satınalma İşlemde
                {
                    _result = "ALIMISLEMDE";
                }
                else if (OBJ1_fieldId == "J50020") //Satınalma Tamamlandı
                {
                    _result = "ALIMTAMAMLANDI";
                }
                else if (OBJ1_fieldId == "J50025") //Satınalma Tamamlandı
                {
                    _result = "ALIMTAMAMLANDITUM";
                }
                else if (OBJ1_fieldId == "J50030") //SATIŞ IADE ISLEMDE 
                {
                    _result = "IADEISLEMDE";
                }
                else if (OBJ1_fieldId == "J50040") //SATIŞ IADE ISLEMDE 
                {
                    _result = "IADETAMAMLANDI";
                }
                else if (OBJ1_fieldId == "J50045") //Satınalma Tamamlandı
                {
                    _result = "IADETAMAMLANDITUM";
                }
                else if (OBJ1_fieldId == "J50050") //Satış İşlemde
                {
                    _result = "SATISISLEMDE";
                }
                else if (OBJ1_fieldId == "J50060") //Satış Tamamlandı
                {
                    _result = "SATISTAMAMLANDI";
                }
                else if (OBJ1_fieldId == "J50065") //Satış Tamamlandı
                {
                    _result = "SATISTAMAMLANDITUM";
                }
                else if (OBJ1_fieldId == "J50070") //SATINALMA  IADE ISLEMDE 
                {
                    _result = "ALIMIADEISLEMDE";
                }
                else if (OBJ1_fieldId == "J50080") //SATINALMA IADE ISLEMDE 
                {
                    _result = "ALIMIADETAMAMLANDI";
                }
                else if (OBJ1_fieldId == "J50085") //SATINALMA IADE ISLEMDE 
                {
                    _result = "ALIMIADETAMAMLANDITUM";
                }
                else if (OBJ1_fieldId == "J50110") //BORÇ DEKONTU ISLEMDE 
                {
                    _result = "DEKONTISLEMDE";
                }
                else if (OBJ1_fieldId == "J50120") //BORÇ DEKONTU TAMAMLANAN
                {
                    _result = "DEKONTTAMAMLANDI";
                }
                else if (OBJ1_fieldId == "J50125") //BORÇ DEKONTU TAMAMLANAN
                {
                    _result = "DEKONTTAMAMLANDITUM";
                }
                else if (OBJ1_fieldId == "J50130") //ALIM DEKONTU ISLEMDE 
                {
                    _result = "DEKONTISLEMDE";
                }
                else if (OBJ1_fieldId == "J50140") //ALIM DEKONTU TAMAMLANAN
                {
                    _result = "ALIMDEKONTTAMAMLANDI";
                }
                else if (OBJ1_fieldId == "J50145") //ALIM DEKONTU TAMAMLANAN
                {
                    _result = "ALIMDEKONTTAMAMLANDITUM";
                }
                return _result;
            }
        }
        public string pageOrderType
        {
            get
            {
                string _type = "";
                if (OBJ2_pageId == "PG3006")
                {
                    _type = "PREBOOKING";
                }
                else if (OBJ2_pageId == "PG3003")
                {
                    _type = "QUOTE";
                }
                else if (OBJ2_pageId == "P41010" | OBJ2_pageId == "P41020")
                {
                    _type = "SHIP";
                }
                else if (OBJ2_pageId == "P50010" | OBJ2_pageId == "P50020" | OBJ2_pageId == "P50025")
                {
                    _type = "SATINALMAONAY";
                }
                else if (OBJ2_pageId == "P50050" | OBJ2_pageId == "P50060" | OBJ2_pageId == "P50065")
                {
                    _type = "SATIS";
                }
                else if (OBJ2_pageId == "P50030" | OBJ2_pageId == "P50040" | OBJ2_pageId == "P50045")
                {
                    _type = "SATIS_IADE";
                }
                else if (OBJ1_fieldId == "J50070" | OBJ1_fieldId == "J50080" | OBJ1_fieldId == "J50080" | OBJ1_fieldId == "J50085")
                {
                    _type = "ALIMIADE";
                }
                else if (OBJ1_fieldId == "J50110" | OBJ1_fieldId == "J50120" | OBJ1_fieldId == "J50125")
                {
                    _type = "BORCDEKONT";
                }
                else if (OBJ1_fieldId == "J50130" | OBJ1_fieldId == "J50140" | OBJ1_fieldId == "J50145")
                {
                    _type = "ALIMDEKONT";
                }

                return _type;
            }
        }
        public string postURL
        {
            get
            {
                string _url = "";
                if (pageOrderType == "QUOTE")
                {
                    _url = "/Quotations/mainQuotations.aspx";
                }
                else if (pageOrderType == "PREBOOKING")
                {
                    _url = "/Prebookings/mainPrebookings.aspx";
                }
                else if (pageOrderType == "SATINALMAONAY" | pageOrderType == "SATIS_IADE")
                {
                    if ((OBJ1_pageId == "P50010" & OBJ1_fieldId == "J50010") | (OBJ1_pageId == "P50030" & OBJ1_fieldId == "J50030"))
                        _url = "/FaturaOnGiris.aspx";
                    else
                        _url = "/FaturaCard/Satinalma.aspx";
                }
                else if (pageOrderType == "SATIS")
                {
                    _url = "/FaturaCard/Satis.aspx";
                }
                else if (pageOrderType == "BORCDEKONT")
                {
                    _url = "/FaturaCard/Satis.aspx";
                }
                else if (pageOrderType == "ALIMIADE")
                {
                    _url = "/FaturaCard/Satinalmaiade.aspx";
                }
                else if (pageOrderType == "ALIMDEKONT")
                {
                    _url = "/FaturaCard/Satinalma.aspx";
                }
                return _url;
            }
        }
        public string postURLParameter
        {
            get
            {
                string _url = "";
                if (pageOrderType == "QUOTE")
                {
                    _url = "MQID";
                }
                else if (pageOrderType == "PREBOOKING")
                {
                    _url = "MPID";
                }
                else
                {
                    _url = "invoiceNo";
                }
                return _url;
            }
        }
        public string invoiceType
        {
            get
            {
                string _invoiceType = "";
                if (OBJ1_fieldId == "J50010" | OBJ1_fieldId == "J50020" | OBJ1_fieldId == "J50025")
                {
                    _invoiceType = "SATINALMA";
                }
                else if (OBJ1_fieldId == "J50030" | OBJ1_fieldId == "J50040" | OBJ1_fieldId == "J50045")
                {
                    _invoiceType = "SATIS_IADE";
                }
                else if (OBJ1_fieldId == "J50050" | OBJ1_fieldId == "J50060" | OBJ1_fieldId == "J50065")
                {
                    _invoiceType = "SATIS";
                }
                else if (OBJ1_fieldId == "J50070" | OBJ1_fieldId == "J50080" | OBJ1_fieldId == "J50085")
                {
                    _invoiceType = "ALIMIADE";
                }
                else if (OBJ1_fieldId == "J50110" | OBJ1_fieldId == "J50120" | OBJ1_fieldId == "J50125")
                {
                    _invoiceType = "BORCDEKONT";
                }
                else if (OBJ1_fieldId == "J50130" | OBJ1_fieldId == "J50140" | OBJ1_fieldId == "J50145")
                {
                    _invoiceType = "ALIMDEKONT";
                }

                return _invoiceType;
            }
        }
        private string eFatura_DahiliType
        {
            get
            {
                string _invoiceType = "";
                if (OBJ1_fieldId == "J50150" | OBJ1_fieldId == "J50160" | OBJ1_fieldId == "J50170")
                {
                    _invoiceType = "DAHILIDEKONT";
                }
                else if (OBJ1_fieldId == "J50210" | OBJ1_fieldId == "J50220" | OBJ1_fieldId == "J50230")
                {
                    _invoiceType = "EFGELEN";
                }
                else if (OBJ1_fieldId == "J50310" | OBJ1_fieldId == "J50320" | OBJ1_fieldId == "J50330")
                {
                    _invoiceType = "EFGIDEN";
                }


                return _invoiceType;
            }
        }
        public string invoiceStatu
        {
            get
            {
                string _fatStatu = "";
                switch (OBJ1_fieldId)
                {
                    case "J50010":
                        _fatStatu = "ALIMISLEMDE";
                        break;
                    case "J50020":
                        _fatStatu = "ALIMTAMAMLANDI";
                        break;
                    case "J50030":
                        _fatStatu = "IADEISLEMDE";
                        break;
                    case "J50040":
                        _fatStatu = "IADETAMAMLANDI";
                        break;
                    case "J50050":
                        _fatStatu = "SATISISLEMDE";
                        break;
                    case "J50060":
                        _fatStatu = "SATISTAMAMLANDI";
                        break;
                    case "J50070":
                        _fatStatu = "ALIMIADEISLEMDE";
                        break;
                    case "J50080":
                        _fatStatu = "ALIMIADETAMAMLANDI";
                        break;

                    case "J50110":
                        _fatStatu = "DEKONTISLEMDE";
                        break;
                    case "J50120":
                        _fatStatu = "DEKONTTAMAMLANDI";
                        break;
                    case "J50130":
                        _fatStatu = "ALIMDEKONTISLEMDE";
                        break;
                    case "J50140":
                        _fatStatu = "ALIMDEKONTTAMAMLANDI";
                        break;

                }
                return _fatStatu;
            }
        }
        private void QueryStringParameterDesing()
        {
            string[] obj1, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9 = { };
            string _val = string.Empty;
            obj1 = Request.QueryString["OJ1"].ToString().Split(';');
            try { obj2 = Request.QueryString["OJ2"].ToString().Split(';'); } catch { _val = Request.QueryString["OJ1"].ToString().Split(';')[0] + ";OJ9999"; obj2 = _val.Split(';'); }
            try { obj3 = Request.QueryString["OJ3"].ToString().Split(';'); } catch { _val = Request.QueryString["OJ1"].ToString().Split(';')[0] + ";OJ9999"; obj3 = _val.Split(';'); }
            try { obj4 = Request.QueryString["OJ4"].ToString().Split(';'); } catch { _val = Request.QueryString["OJ1"].ToString().Split(';')[0] + ";OJ9999"; obj4 = _val.Split(';'); }
            try { obj5 = Request.QueryString["OJ5"].ToString().Split(';'); } catch { _val = Request.QueryString["OJ1"].ToString().Split(';')[0] + ";OJ9999"; obj5 = _val.Split(';'); }
            try { obj6 = Request.QueryString["OJ6"].ToString().Split(';'); } catch { _val = Request.QueryString["OJ1"].ToString().Split(';')[0] + ";OJ9999"; obj6 = _val.Split(';'); }
            try { obj7 = Request.QueryString["OJ7"].ToString().Split(';'); } catch { _val = Request.QueryString["OJ1"].ToString().Split(';')[0] + ";OJ9999"; obj7 = _val.Split(';'); }
            try { obj8 = Request.QueryString["OJ8"].ToString().Split(';'); } catch { _val = Request.QueryString["OJ1"].ToString().Split(';')[0] + ";OJ9999"; obj8 = _val.Split(';'); }
            try { obj9 = Request.QueryString["OJ9"].ToString().Split(';'); } catch { _val = Request.QueryString["OJ1"].ToString().Split(';')[0] + ";OJ9999"; obj9 = _val.Split(';'); }

            if (!string.IsNullOrEmpty(Request.QueryString["P1"]))
                currentP1 = Request.QueryString["P1"].ToString();
            else
                currentP1 = string.Empty;

            try
            {
                OBJ1_pageId = obj1[0].ToString();
                OBJ1_fieldId = obj1[1].ToString();

                OBJ2_pageId = obj2[0].ToString();
                OBJ2_fieldId = obj2[1].ToString();

                OBJ3_pageId = obj3[0].ToString();
                OBJ3_fieldId = obj3[1].ToString();

                OBJ4_pageId = obj4[0].ToString();
                OBJ4_fieldId = obj4[1].ToString();

                OBJ5_pageId = obj5[0].ToString();
                OBJ5_fieldId = obj5[1].ToString();

                OBJ6_pageId = obj6[0].ToString();
                OBJ6_fieldId = obj6[1].ToString();

                OBJ7_pageId = obj7[0].ToString();
                OBJ7_fieldId = obj7[1].ToString();

                OBJ8_pageId = obj8[0].ToString();
                OBJ8_fieldId = obj8[1].ToString();

                OBJ9_pageId = obj9[0].ToString();
                OBJ9_fieldId = obj9[1].ToString();
            }
            catch (Exception)
            {
                OBJ1_pageId = string.Empty;
                OBJ1_fieldId = string.Empty;
                OBJ2_pageId = string.Empty;
                OBJ2_fieldId = string.Empty;
                OBJ3_pageId = string.Empty;
                OBJ3_fieldId = string.Empty;
                OBJ4_pageId = string.Empty;
                OBJ4_fieldId = string.Empty;
                OBJ5_pageId = string.Empty;
                OBJ5_fieldId = string.Empty;
                OBJ6_pageId = string.Empty;
                OBJ6_fieldId = string.Empty;
                OBJ7_pageId = string.Empty;
                OBJ7_fieldId = string.Empty;
                OBJ8_pageId = string.Empty;
                OBJ8_fieldId = string.Empty;
                OBJ9_pageId = string.Empty;
                OBJ9_fieldId = string.Empty;
            }

            grid1.JSProperties["cpIsUpdated"] = false;
            grid2.JSProperties["cpIsUpdated"] = false;
            grid3.JSProperties["cpIsUpdated"] = false;
            grid4.JSProperties["cpIsUpdated"] = false;
            grid5.JSProperties["cpIsUpdated"] = false;
            grid6.JSProperties["cpIsUpdated"] = false;
            grid7.JSProperties["cpIsUpdated"] = false;
            grid8.JSProperties["cpIsUpdated"] = false;
            grid9.JSProperties["cpIsUpdated"] = false;

            grid1.JSProperties["cpMsg"] = string.Empty;
            grid2.JSProperties["cpMsg"] = string.Empty;
            grid3.JSProperties["cpMsg"] = string.Empty;
            grid4.JSProperties["cpMsg"] = string.Empty;
            grid5.JSProperties["cpMsg"] = string.Empty;
            grid6.JSProperties["cpMsg"] = string.Empty;
            grid7.JSProperties["cpMsg"] = string.Empty;
            grid8.JSProperties["cpMsg"] = string.Empty;
            grid9.JSProperties["cpMsg"] = string.Empty;


            grid1.JSProperties["cpQval"] = string.Empty;
            grid2.JSProperties["cpQval"] = string.Empty;
            grid3.JSProperties["cpQval"] = string.Empty;
            grid4.JSProperties["cpQval"] = string.Empty;
            grid5.JSProperties["cpQval"] = string.Empty;
            grid6.JSProperties["cpQval"] = string.Empty;
            grid7.JSProperties["cpQval"] = string.Empty;
            grid8.JSProperties["cpQval"] = string.Empty;
            grid9.JSProperties["cpQval"] = string.Empty;

            grid1.JSProperties["cpIsLinked"] = false;
            grid2.JSProperties["cpIsLinked"] = false;
            grid3.JSProperties["cpIsLinked"] = false;
            grid4.JSProperties["cpIsLinked"] = false;
            grid5.JSProperties["cpIsLinked"] = false;
            grid6.JSProperties["cpIsLinked"] = false;
            grid7.JSProperties["cpIsLinked"] = false;
            grid8.JSProperties["cpIsLinked"] = false;
            grid9.JSProperties["cpIsLinked"] = false;



            grid1.JSProperties["cpIslemId"] = string.Empty;
            grid2.JSProperties["cpIslemId"] = string.Empty;
            grid3.JSProperties["cpIslemId"] = string.Empty;
            grid4.JSProperties["cpIslemId"] = string.Empty;
            grid5.JSProperties["cpIslemId"] = string.Empty;
            grid6.JSProperties["cpIslemId"] = string.Empty;
            grid7.JSProperties["cpIslemId"] = string.Empty;
            grid8.JSProperties["cpIslemId"] = string.Empty;
            grid9.JSProperties["cpIslemId"] = string.Empty;



            grid1.JSProperties["cpLink"] = string.Empty;
            grid2.JSProperties["cpLink"] = string.Empty;
            grid3.JSProperties["cpLink"] = string.Empty;
            grid4.JSProperties["cpLink"] = string.Empty;
            grid5.JSProperties["cpLink"] = string.Empty;
            grid6.JSProperties["cpLink"] = string.Empty;
            grid7.JSProperties["cpLink"] = string.Empty;
            grid8.JSProperties["cpLink"] = string.Empty;
            grid9.JSProperties["cpLink"] = string.Empty;


            grid1.JSProperties["cpQueryStrings"] = string.Empty;
            grid2.JSProperties["cpQueryStrings"] = string.Empty;
            grid3.JSProperties["cpQueryStrings"] = string.Empty;
            grid4.JSProperties["cpQueryStrings"] = string.Empty;
            grid5.JSProperties["cpQueryStrings"] = string.Empty;
            grid6.JSProperties["cpQueryStrings"] = string.Empty;
            grid7.JSProperties["cpQueryStrings"] = string.Empty;
            grid8.JSProperties["cpQueryStrings"] = string.Empty;
            grid9.JSProperties["cpQueryStrings"] = string.Empty;




        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            QueryStringParameterDesing();
            if (IsPostBack || IsCallback) return;
            ViewState["OBJ2_Field"] = OBJ2_fieldId;
            Session["TabIndex"] = "1";
            DilGetir();
            GetParameters();
            CollapseDisplay();
            FillDynamicObjects();
            RadioListOrder();

            PreparePage();
        }
        private void PreparePage()
        {
            lblGId.Text = Request.QueryString["GId"];
            if (OBJ1_pageId == "P50210" | OBJ1_pageId == "P50220" | OBJ1_pageId == "P50230" | OBJ1_pageId == "P50310" | OBJ1_pageId == "P50320" | OBJ1_pageId == "P50330")
            {
                rpMain.Collapsed = true;
                rpMain.Visible = false;

            }

            if (Request.QueryString["P1"] != null)
            {
                HiddenP1.Value = Request.QueryString["P1"].ToString();
                //this.iframeView.Src = postURL + "?" + postURLParameter + "=" + HiddenP1.Value;


                HiddenP1.Value = Request.QueryString["P1"].ToString();
                if (Request.QueryString["P2"] != null)
                    HiddenP2.Value = Request.QueryString["P2"].ToString();
                string link = HiddenpageType.Value;
                if (link.Contains("XXXXX"))
                {
                    link = link.Replace("XXXXX", HiddenP1.Value);
                    link = link.Replace("YYYYY", HiddenP2.Value);
                    link = link.Replace("undefined", "TOTAL");
                }
                else
                    link += HiddenP1.Value;

                this.iframeView.Attributes.Add("src", link);


            }

            if (OBJ1_fieldId == "J50010" | OBJ1_fieldId == "J50030")
            {

                string _usrs = "TM,RM,DSM,ATA,SLM";
                string[] _users = _usrs.Split(',');

                if (!_users.Any(x => x == _obj.UserName.ToString()))
                {
                    foreach (DevExpress.Web.MenuItem item in menu1.Items)
                    {
                        if (item.Name == "btnSelectedH0005")
                            item.Visible = false;
                    }
                }

            }

            if (!string.IsNullOrEmpty(currentP1))
            {
                string script = "OnGridFocusedRowChanged(grid1,'','1');";
                ClientScript.RegisterStartupScript(this.Page.GetType(), "fnct", script, true);

                grid1.FilterExpression = HiddendirectParam.Value + "='" + currentP1 + "'";
                grid1.DataSourceID = "dtgrid1";
                grid1.DataBind();
            }
        }
        void GetParameters()
        {
            Session["Grid3RTYP1"] = string.Empty;
            Session["Grid3RTYP2"] = string.Empty;
            Session["Grid3R0037"] = string.Empty;

            Session["Grid2RTYP1"] = string.Empty;
            Session["Grid2RTYP2"] = string.Empty;

            txtOBJP1.Text = OBJ1_pageId;
            txtOBJP2.Text = OBJ2_pageId;
            txtOBJP3.Text = OBJ3_pageId;
            txtOBJP4.Text = OBJ4_pageId;
            txtOBJP5.Text = OBJ5_pageId;
            txtOBJP6.Text = OBJ6_pageId;
            txtOBJP7.Text = OBJ7_pageId;
            txtOBJP8.Text = OBJ8_pageId;
            txtOBJP9.Text = OBJ9_pageId;

            txtOBJF1.Text = OBJ1_fieldId;
            txtOBJF2.Text = OBJ2_fieldId;
            txtOBJF3.Text = OBJ3_fieldId;
            txtOBJF4.Text = OBJ4_fieldId;
            txtOBJF5.Text = OBJ5_fieldId;
            txtOBJF6.Text = OBJ6_fieldId;
            txtOBJF7.Text = OBJ7_fieldId;
            txtOBJF8.Text = OBJ8_fieldId;
            txtOBJF9.Text = OBJ9_fieldId;

            #region Eski Versiyon
            //string Pages = OBJ1_pageId + "," + OBJ2_pageId + "," + OBJ3_pageId + "," + OBJ4_pageId + "," + OBJ5_pageId + "," + OBJ6_pageId;
            //string objects = OBJ1_fieldId + "," + OBJ2_fieldId + "," + OBJ3_fieldId + "," + OBJ4_fieldId + "," + OBJ5_fieldId + "," + OBJ6_fieldId;
            //using (DataSet ds = f.GetDs(Pages, objects))
            //{
            //    if (ds.Tables.Count > 0)
            //    {
            //        if (ds.Tables.Count > 0)
            //        {
            //            if (ds.Tables[0].Rows.Count > 0)
            //            {
            //                txtlink1.Text = ds.Tables[0].Rows[0]["LINK"].ToString();
            //                txtlinkField1.Text = ds.Tables[0].Rows[0]["LINK FIELD"].ToString();
            //                txtCatchParameters1.Text = ds.Tables[0].Rows[0]["Transferred Fields"].ToString();
            //                txtCollapseHeader1.Text = ds.Tables[0].Rows[0]["Collapse Dinamik Header"].ToString();
            //                HiddendirectParam.Value = ds.Tables[0].Rows[0]["PrimaryField"].ToString();
            //                txtCollapseStatu1.Text = ds.Tables[0].Rows[0]["Default Collapse State"].ToString();

            //            }

            //            if (ds.Tables[1].Rows.Count > 0)
            //            {
            //                txtlink2.Text = ds.Tables[1].Rows[0]["LINK"].ToString();
            //                txtlinkField2.Text = ds.Tables[1].Rows[0]["LINK FIELD"].ToString();
            //                txtCatchParameters2.Text = ds.Tables[1].Rows[0]["Transferred Fields"].ToString();
            //                txtCollapseHeader2.Text = ds.Tables[1].Rows[0]["Collapse Dinamik Header"].ToString();
            //                txtCollapseStatu2.Text = ds.Tables[1].Rows[0]["Default Collapse State"].ToString();
            //                txtMasterObject2.Text = ds.Tables[1].Rows[0]["MasterObject"].ToString();
            //            }

            //            if (ds.Tables[2].Rows.Count > 0)
            //            {
            //                txtlink3.Text = ds.Tables[2].Rows[0]["LINK"].ToString();
            //                txtlinkField3.Text = ds.Tables[2].Rows[0]["LINK FIELD"].ToString();
            //                txtCatchParameters3.Text = ds.Tables[2].Rows[0]["Transferred Fields"].ToString();
            //                txtCollapseHeader3.Text = ds.Tables[2].Rows[0]["Collapse Dinamik Header"].ToString();
            //                txtCollapseStatu3.Text = ds.Tables[2].Rows[0]["Default Collapse State"].ToString();
            //                txtMasterObject3.Text = ds.Tables[2].Rows[0]["MasterObject"].ToString();
            //            }

            //            if (ds.Tables[3].Rows.Count > 0)
            //            {
            //                txtlink4.Text = ds.Tables[3].Rows[0]["LINK"].ToString();
            //                txtlinkField4.Text = ds.Tables[3].Rows[0]["LINK FIELD"].ToString();
            //                txtCatchParameters4.Text = ds.Tables[3].Rows[0]["Transferred Fields"].ToString();
            //                txtCollapseHeader4.Text = ds.Tables[3].Rows[0]["Collapse Dinamik Header"].ToString();
            //                txtCollapseStatu4.Text = ds.Tables[3].Rows[0]["Default Collapse State"].ToString();
            //                txtMasterObject4.Text = ds.Tables[3].Rows[0]["MasterObject"].ToString();
            //            }

            //            if (ds.Tables[4].Rows.Count > 0)
            //            {
            //                txtlink5.Text = ds.Tables[4].Rows[0]["LINK"].ToString();
            //                txtlinkField5.Text = ds.Tables[4].Rows[0]["LINK FIELD"].ToString();
            //                txtCatchParameters5.Text = ds.Tables[4].Rows[0]["Transferred Fields"].ToString();
            //                txtCollapseHeader5.Text = ds.Tables[4].Rows[0]["Collapse Dinamik Header"].ToString();
            //                txtCollapseStatu5.Text = ds.Tables[4].Rows[0]["Default Collapse State"].ToString();
            //                txtMasterObject5.Text = ds.Tables[4].Rows[0]["MasterObject"].ToString();
            //            }

            //            if (ds.Tables[5].Rows.Count > 0)
            //            {
            //                txtlink6.Text = ds.Tables[5].Rows[0]["LINK"].ToString();
            //                txtlinkField6.Text = ds.Tables[5].Rows[0]["LINK FIELD"].ToString();
            //                txtCatchParameters6.Text = ds.Tables[5].Rows[0]["Transferred Fields"].ToString();
            //                txtCollapseHeader6.Text = ds.Tables[5].Rows[0]["Collapse Dinamik Header"].ToString();
            //                txtCollapseStatu6.Text = ds.Tables[5].Rows[0]["Default Collapse State"].ToString();
            //                txtMasterObject6.Text = ds.Tables[5].Rows[0]["MasterObject"].ToString();

            //            }

            //        }
            //    }
            //} 
            #endregion

            Session["grid1Cloned"] = "0";
            Session["grid2Cloned"] = "0";
            Session["grid3Cloned"] = "0";
            Session["grid4Cloned"] = "0";
            Session["grid5Cloned"] = "0";
            Session["grid6Cloned"] = "0";
            Session["grid7Cloned"] = "0";
            Session["grid8Cloned"] = "0";
            Session["grid9Cloned"] = "0";


        }
        void FillDS1(DataTable dtRows)
        {
            int sayac = 1;
            string query = string.Empty;
            for (int i = 0; i < dtRows.Rows.Count; i++)
            {
                switch (dtRows.Rows[i]["DX Type1"].ToString())
                {
                    case "DATA":
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Source"].ToString())
                        & !string.IsNullOrEmpty(dtRows.Rows[i]["DX Combo Value"].ToString())
                        & !string.IsNullOrEmpty(dtRows.Rows[i]["DX Combo Text"].ToString()))
                        {
                            query = DynamicUtils.FillCombobox(dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(),
                                dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), _obj.Comp.ToString(), _obj.UserName.ToString());
                            switch (sayac)
                            {
                                case 1:
                                    DS11.Table = f.datatablegonder(query);
                                    break;
                                case 2:
                                    DS12.Table = f.datatablegonder(query);
                                    break;
                                case 3:
                                    DS13.Table = f.datatablegonder(query);
                                    break;
                                case 4:
                                    DS14.Table = f.datatablegonder(query);
                                    break;
                                case 5:
                                    DS15.Table = f.datatablegonder(query);
                                    break;
                                case 6:
                                    DS16.Table = f.datatablegonder(query);
                                    break;
                                case 7:
                                    DS17.Table = f.datatablegonder(query);
                                    break;
                                case 8:
                                    DS18.Table = f.datatablegonder(query);
                                    break;
                                case 9:
                                    DS19.Table = f.datatablegonder(query);
                                    break;
                                case 10:
                                    DS110.Table = f.datatablegonder(query);
                                    break;
                                case 11:
                                    DS111.Table = f.datatablegonder(query);
                                    break;
                                case 12:
                                    DS112.Table = f.datatablegonder(query);
                                    break;
                                case 13:
                                    DS113.Table = f.datatablegonder(query);
                                    break;
                                case 14:
                                    DS114.Table = f.datatablegonder(query);
                                    break;
                                case 15:
                                    DS115.Table = f.datatablegonder(query);
                                    break;
                                case 16:
                                    DS116.Table = f.datatablegonder(query);
                                    break;
                                case 17:
                                    DS117.Table = f.datatablegonder(query);
                                    break;
                                case 18:
                                    DS118.Table = f.datatablegonder(query);
                                    break;
                                case 19:
                                    DS119.Table = f.datatablegonder(query);
                                    break;
                                case 20:
                                    DS120.Table = f.datatablegonder(query);
                                    break;
                            }
                            sayac++;
                        }
                        break;
                }
            }
        }
        void FillDS2(DataTable dtRows)
        {
            int sayac = 1;
            string query = string.Empty;
            for (int i = 0; i < dtRows.Rows.Count; i++)
            {
                switch (dtRows.Rows[i]["DX Type1"].ToString())
                {
                    case "DATA":
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Source"].ToString())
                        & !string.IsNullOrEmpty(dtRows.Rows[i]["DX Combo Value"].ToString())
                        & !string.IsNullOrEmpty(dtRows.Rows[i]["DX Combo Text"].ToString()))
                        {
                            query = DynamicUtils.FillCombobox(dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(),
                                dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), _obj.Comp.ToString(), _obj.UserName.ToString());
                            switch (sayac)
                            {
                                case 1:
                                    DS21.Table = f.datatablegonder(query);
                                    break;
                                case 2:
                                    DS22.Table = f.datatablegonder(query);
                                    break;
                                case 3:
                                    DS23.Table = f.datatablegonder(query);
                                    break;
                                case 4:
                                    DS24.Table = f.datatablegonder(query);
                                    break;
                                case 5:
                                    DS25.Table = f.datatablegonder(query);
                                    break;
                                case 6:
                                    DS26.Table = f.datatablegonder(query);
                                    break;
                                case 7:
                                    DS27.Table = f.datatablegonder(query);
                                    break;
                                case 8:
                                    DS28.Table = f.datatablegonder(query);
                                    break;
                                case 9:
                                    DS29.Table = f.datatablegonder(query);
                                    break;
                                case 10:
                                    DS210.Table = f.datatablegonder(query);
                                    break;
                                case 11:
                                    DS211.Table = f.datatablegonder(query);
                                    break;
                                case 12:
                                    DS212.Table = f.datatablegonder(query);
                                    break;
                                case 13:
                                    DS213.Table = f.datatablegonder(query);
                                    break;
                                case 14:
                                    DS214.Table = f.datatablegonder(query);
                                    break;
                                case 15:
                                    DS215.Table = f.datatablegonder(query);
                                    break;
                                case 16:
                                    DS216.Table = f.datatablegonder(query);
                                    break;
                                case 17:
                                    DS217.Table = f.datatablegonder(query);
                                    break;
                                case 18:
                                    DS218.Table = f.datatablegonder(query);
                                    break;
                                case 19:
                                    DS219.Table = f.datatablegonder(query);
                                    break;
                                case 20:
                                    DS220.Table = f.datatablegonder(query);
                                    break;
                            }
                            sayac++;
                        }
                        break;
                }
            }
        }
        void FillDS3(DataTable dtRows)
        {
            int sayac = 1;
            string query = string.Empty;
            for (int i = 0; i < dtRows.Rows.Count; i++)
            {
                switch (dtRows.Rows[i]["DX Type1"].ToString())
                {
                    case "DATA":
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Source"].ToString())
                        & !string.IsNullOrEmpty(dtRows.Rows[i]["DX Combo Value"].ToString())
                        & !string.IsNullOrEmpty(dtRows.Rows[i]["DX Combo Text"].ToString()))
                        {
                            query = DynamicUtils.FillCombobox(dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(),
                                dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), _obj.Comp.ToString(), _obj.UserName.ToString());
                            switch (sayac)
                            {
                                case 1:
                                    DS31.Table = f.datatablegonder(query);
                                    break;
                                case 2:
                                    DS32.Table = f.datatablegonder(query);
                                    break;
                                case 3:
                                    DS33.Table = f.datatablegonder(query);
                                    break;
                                case 4:
                                    DS34.Table = f.datatablegonder(query);
                                    break;
                                case 5:
                                    DS35.Table = f.datatablegonder(query);
                                    break;
                                case 6:
                                    DS36.Table = f.datatablegonder(query);
                                    break;
                                case 7:
                                    DS37.Table = f.datatablegonder(query);
                                    break;
                                case 8:
                                    DS38.Table = f.datatablegonder(query);
                                    break;
                                case 9:
                                    DS39.Table = f.datatablegonder(query);
                                    break;
                                case 10:
                                    DS310.Table = f.datatablegonder(query);
                                    break;
                                case 11:
                                    DS311.Table = f.datatablegonder(query);
                                    break;
                                case 12:
                                    DS312.Table = f.datatablegonder(query);
                                    break;
                                case 13:
                                    DS313.Table = f.datatablegonder(query);
                                    break;
                                case 14:
                                    DS314.Table = f.datatablegonder(query);
                                    break;
                                case 15:
                                    DS315.Table = f.datatablegonder(query);
                                    break;
                                case 16:
                                    DS316.Table = f.datatablegonder(query);
                                    break;
                                case 17:
                                    DS317.Table = f.datatablegonder(query);
                                    break;
                                case 18:
                                    DS318.Table = f.datatablegonder(query);
                                    break;
                                case 19:
                                    DS319.Table = f.datatablegonder(query);
                                    break;
                                case 20:
                                    DS320.Table = f.datatablegonder(query);
                                    break;
                            }
                            sayac++;
                        }
                        break;
                }
            }
        }
        void FillDS4(DataTable dtRows)
        {
            int sayac = 1;
            string query = string.Empty;
            for (int i = 0; i < dtRows.Rows.Count; i++)
            {
                switch (dtRows.Rows[i]["DX Type1"].ToString())
                {
                    case "DATA":
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Source"].ToString())
                        & !string.IsNullOrEmpty(dtRows.Rows[i]["DX Combo Value"].ToString())
                        & !string.IsNullOrEmpty(dtRows.Rows[i]["DX Combo Text"].ToString()))
                        {
                            query = DynamicUtils.FillCombobox(dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(),
                                dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), _obj.Comp.ToString(), _obj.UserName.ToString());
                            switch (sayac)
                            {
                                case 1:
                                    DS41.Table = f.datatablegonder(query);
                                    break;
                                case 2:
                                    DS42.Table = f.datatablegonder(query);
                                    break;
                                case 3:
                                    DS43.Table = f.datatablegonder(query);
                                    break;
                                case 4:
                                    DS44.Table = f.datatablegonder(query);
                                    break;
                                case 5:
                                    DS45.Table = f.datatablegonder(query);
                                    break;
                                case 6:
                                    DS46.Table = f.datatablegonder(query);
                                    break;
                                case 7:
                                    DS47.Table = f.datatablegonder(query);
                                    break;
                                case 8:
                                    DS48.Table = f.datatablegonder(query);
                                    break;
                                case 9:
                                    DS49.Table = f.datatablegonder(query);
                                    break;
                                case 10:
                                    DS410.Table = f.datatablegonder(query);
                                    break;
                                case 11:
                                    DS411.Table = f.datatablegonder(query);
                                    break;
                                case 12:
                                    DS412.Table = f.datatablegonder(query);
                                    break;
                                case 13:
                                    DS413.Table = f.datatablegonder(query);
                                    break;
                                case 14:
                                    DS414.Table = f.datatablegonder(query);
                                    break;
                                case 15:
                                    DS415.Table = f.datatablegonder(query);
                                    break;
                                case 16:
                                    DS416.Table = f.datatablegonder(query);
                                    break;
                                case 17:
                                    DS417.Table = f.datatablegonder(query);
                                    break;
                                case 18:
                                    DS418.Table = f.datatablegonder(query);
                                    break;
                                case 19:
                                    DS419.Table = f.datatablegonder(query);
                                    break;
                                case 20:
                                    DS420.Table = f.datatablegonder(query);
                                    break;
                            }
                            sayac++;
                        }
                        break;
                }
            }
        }
        void FillDS5(DataTable dtRows)
        {
            int sayac = 1;
            string query = string.Empty;
            for (int i = 0; i < dtRows.Rows.Count; i++)
            {
                switch (dtRows.Rows[i]["DX Type1"].ToString())
                {
                    case "DATA":
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Source"].ToString())
                            & !string.IsNullOrEmpty(dtRows.Rows[i]["DX Combo Value"].ToString())
                            & !string.IsNullOrEmpty(dtRows.Rows[i]["DX Combo Text"].ToString()))
                        {
                            query = DynamicUtils.FillCombobox(dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(),
                                dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), _obj.Comp.ToString(), _obj.UserName.ToString());
                            switch (sayac)
                            {
                                case 1:
                                    DS51.Table = f.datatablegonder(query);
                                    break;
                                case 2:
                                    DS52.Table = f.datatablegonder(query);
                                    break;
                                case 3:
                                    DS53.Table = f.datatablegonder(query);
                                    break;
                                case 4:
                                    DS54.Table = f.datatablegonder(query);
                                    break;
                                case 5:
                                    DS55.Table = f.datatablegonder(query);
                                    break;
                                case 6:
                                    DS56.Table = f.datatablegonder(query);
                                    break;
                                case 7:
                                    DS57.Table = f.datatablegonder(query);
                                    break;
                                case 8:
                                    DS58.Table = f.datatablegonder(query);
                                    break;
                                case 9:
                                    DS59.Table = f.datatablegonder(query);
                                    break;
                                case 10:
                                    DS510.Table = f.datatablegonder(query);
                                    break;
                                case 11:
                                    DS511.Table = f.datatablegonder(query);
                                    break;
                                case 12:
                                    DS512.Table = f.datatablegonder(query);
                                    break;
                                case 13:
                                    DS513.Table = f.datatablegonder(query);
                                    break;
                                case 14:
                                    DS514.Table = f.datatablegonder(query);
                                    break;
                                case 15:
                                    DS515.Table = f.datatablegonder(query);
                                    break;
                                case 16:
                                    DS516.Table = f.datatablegonder(query);
                                    break;
                                case 17:
                                    DS517.Table = f.datatablegonder(query);
                                    break;
                                case 18:
                                    DS518.Table = f.datatablegonder(query);
                                    break;
                                case 19:
                                    DS519.Table = f.datatablegonder(query);
                                    break;
                                case 20:
                                    DS520.Table = f.datatablegonder(query);
                                    break;
                            }
                            sayac++;
                        }
                        break;
                }
            }
        }
        void FillDS6(DataTable dtRows)
        {
            int sayac = 1;
            string query = string.Empty;
            for (int i = 0; i < dtRows.Rows.Count; i++)
            {
                switch (dtRows.Rows[i]["DX Type1"].ToString())
                {
                    case "DATA":
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Source"].ToString())
                        & !string.IsNullOrEmpty(dtRows.Rows[i]["DX Combo Value"].ToString())
                        & !string.IsNullOrEmpty(dtRows.Rows[i]["DX Combo Text"].ToString()))
                        {
                            query = DynamicUtils.FillCombobox(dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(),
                                dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), _obj.Comp.ToString(), _obj.UserName.ToString());
                            switch (sayac)
                            {
                                case 1:
                                    DS61.Table = f.datatablegonder(query);
                                    break;
                                case 2:
                                    DS62.Table = f.datatablegonder(query);
                                    break;
                                case 3:
                                    DS63.Table = f.datatablegonder(query);
                                    break;
                                case 4:
                                    DS64.Table = f.datatablegonder(query);
                                    break;
                                case 5:
                                    DS65.Table = f.datatablegonder(query);
                                    break;
                                case 6:
                                    DS66.Table = f.datatablegonder(query);
                                    break;
                                case 7:
                                    DS67.Table = f.datatablegonder(query);
                                    break;
                                case 8:
                                    DS68.Table = f.datatablegonder(query);
                                    break;
                                case 9:
                                    DS69.Table = f.datatablegonder(query);
                                    break;
                                case 10:
                                    DS610.Table = f.datatablegonder(query);
                                    break;
                                case 11:
                                    DS611.Table = f.datatablegonder(query);
                                    break;
                                case 12:
                                    DS612.Table = f.datatablegonder(query);
                                    break;
                                case 13:
                                    DS613.Table = f.datatablegonder(query);
                                    break;
                                case 14:
                                    DS614.Table = f.datatablegonder(query);
                                    break;
                                case 15:
                                    DS615.Table = f.datatablegonder(query);
                                    break;
                                case 16:
                                    DS616.Table = f.datatablegonder(query);
                                    break;
                                case 17:
                                    DS617.Table = f.datatablegonder(query);
                                    break;
                                case 18:
                                    DS618.Table = f.datatablegonder(query);
                                    break;
                                case 19:
                                    DS619.Table = f.datatablegonder(query);
                                    break;
                                case 20:
                                    DS620.Table = f.datatablegonder(query);
                                    break;
                            }
                            sayac++;
                        }
                        break;
                }
            }
        }
        void FillDS7(DataTable dtRows)
        {
            int sayac = 1;
            string query = string.Empty;
            for (int i = 0; i < dtRows.Rows.Count; i++)
            {
                switch (dtRows.Rows[i]["DX Type1"].ToString())
                {
                    case "DATA":
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Source"].ToString())
                        & !string.IsNullOrEmpty(dtRows.Rows[i]["DX Combo Value"].ToString())
                        & !string.IsNullOrEmpty(dtRows.Rows[i]["DX Combo Text"].ToString()))
                        {
                            query = DynamicUtils.FillCombobox(dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(),
                                dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), _obj.Comp.ToString(), _obj.UserName.ToString());
                            switch (sayac)
                            {
                                case 1:
                                    DS71.Table = f.datatablegonder(query);
                                    break;
                                case 2:
                                    DS72.Table = f.datatablegonder(query);
                                    break;
                                case 3:
                                    DS73.Table = f.datatablegonder(query);
                                    break;
                                case 4:
                                    DS74.Table = f.datatablegonder(query);
                                    break;
                                case 5:
                                    DS75.Table = f.datatablegonder(query);
                                    break;
                                case 6:
                                    DS76.Table = f.datatablegonder(query);
                                    break;
                                case 7:
                                    DS77.Table = f.datatablegonder(query);
                                    break;
                                case 8:
                                    DS78.Table = f.datatablegonder(query);
                                    break;
                                case 9:
                                    DS79.Table = f.datatablegonder(query);
                                    break;
                                case 10:
                                    DS710.Table = f.datatablegonder(query);
                                    break;
                                case 11:
                                    DS711.Table = f.datatablegonder(query);
                                    break;
                                case 12:
                                    DS712.Table = f.datatablegonder(query);
                                    break;
                                case 13:
                                    DS713.Table = f.datatablegonder(query);
                                    break;
                                case 14:
                                    DS714.Table = f.datatablegonder(query);
                                    break;
                                case 15:
                                    DS715.Table = f.datatablegonder(query);
                                    break;
                                case 16:
                                    DS716.Table = f.datatablegonder(query);
                                    break;
                                case 17:
                                    DS717.Table = f.datatablegonder(query);
                                    break;
                                case 18:
                                    DS718.Table = f.datatablegonder(query);
                                    break;
                                case 19:
                                    DS719.Table = f.datatablegonder(query);
                                    break;
                                case 20:
                                    DS720.Table = f.datatablegonder(query);
                                    break;
                            }
                            sayac++;
                        }
                        break;
                }
            }
        }
        void FillDS8(DataTable dtRows)
        {
            int sayac = 1;
            string query = string.Empty;
            for (int i = 0; i < dtRows.Rows.Count; i++)
            {
                switch (dtRows.Rows[i]["DX Type1"].ToString())
                {
                    case "DATA":
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Source"].ToString())
                        & !string.IsNullOrEmpty(dtRows.Rows[i]["DX Combo Value"].ToString())
                        & !string.IsNullOrEmpty(dtRows.Rows[i]["DX Combo Text"].ToString()))
                        {
                            query = DynamicUtils.FillCombobox(dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(),
                                dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), _obj.Comp.ToString(), _obj.UserName.ToString());
                            switch (sayac)
                            {
                                case 1:
                                    DS81.Table = f.datatablegonder(query);
                                    break;
                                case 2:
                                    DS82.Table = f.datatablegonder(query);
                                    break;
                                case 3:
                                    DS83.Table = f.datatablegonder(query);
                                    break;
                                case 4:
                                    DS84.Table = f.datatablegonder(query);
                                    break;
                                case 5:
                                    DS85.Table = f.datatablegonder(query);
                                    break;
                                case 6:
                                    DS86.Table = f.datatablegonder(query);
                                    break;
                                case 7:
                                    DS87.Table = f.datatablegonder(query);
                                    break;
                                case 8:
                                    DS88.Table = f.datatablegonder(query);
                                    break;
                                case 9:
                                    DS89.Table = f.datatablegonder(query);
                                    break;
                                case 10:
                                    DS810.Table = f.datatablegonder(query);
                                    break;
                                case 11:
                                    DS811.Table = f.datatablegonder(query);
                                    break;
                                case 12:
                                    DS812.Table = f.datatablegonder(query);
                                    break;
                                case 13:
                                    DS813.Table = f.datatablegonder(query);
                                    break;
                                case 14:
                                    DS814.Table = f.datatablegonder(query);
                                    break;
                                case 15:
                                    DS815.Table = f.datatablegonder(query);
                                    break;
                                case 16:
                                    DS816.Table = f.datatablegonder(query);
                                    break;
                                case 17:
                                    DS817.Table = f.datatablegonder(query);
                                    break;
                                case 18:
                                    DS818.Table = f.datatablegonder(query);
                                    break;
                                case 19:
                                    DS819.Table = f.datatablegonder(query);
                                    break;
                                case 20:
                                    DS820.Table = f.datatablegonder(query);
                                    break;
                            }
                            sayac++;
                        }
                        break;
                }
            }
        }
        void FillDS9(DataTable dtRows)
        {
            int sayac = 1;
            string query = string.Empty;
            for (int i = 0; i < dtRows.Rows.Count; i++)
            {
                switch (dtRows.Rows[i]["DX Type1"].ToString())
                {
                    case "DATA":
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Source"].ToString())
                        & !string.IsNullOrEmpty(dtRows.Rows[i]["DX Combo Value"].ToString())
                        & !string.IsNullOrEmpty(dtRows.Rows[i]["DX Combo Text"].ToString()))
                        {
                            query = DynamicUtils.FillCombobox(dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(),
                                dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), _obj.Comp.ToString(), _obj.UserName.ToString());
                            switch (sayac)
                            {
                                case 1:
                                    DS91.Table = f.datatablegonder(query);
                                    break;
                                case 2:
                                    DS92.Table = f.datatablegonder(query);
                                    break;
                                case 3:
                                    DS93.Table = f.datatablegonder(query);
                                    break;
                                case 4:
                                    DS94.Table = f.datatablegonder(query);
                                    break;
                                case 5:
                                    DS95.Table = f.datatablegonder(query);
                                    break;
                                case 6:
                                    DS96.Table = f.datatablegonder(query);
                                    break;
                                case 7:
                                    DS97.Table = f.datatablegonder(query);
                                    break;
                                case 8:
                                    DS98.Table = f.datatablegonder(query);
                                    break;
                                case 9:
                                    DS99.Table = f.datatablegonder(query);
                                    break;
                                case 10:
                                    DS910.Table = f.datatablegonder(query);
                                    break;
                                case 11:
                                    DS911.Table = f.datatablegonder(query);
                                    break;
                                case 12:
                                    DS912.Table = f.datatablegonder(query);
                                    break;
                                case 13:
                                    DS913.Table = f.datatablegonder(query);
                                    break;
                                case 14:
                                    DS914.Table = f.datatablegonder(query);
                                    break;
                                case 15:
                                    DS915.Table = f.datatablegonder(query);
                                    break;
                                case 16:
                                    DS916.Table = f.datatablegonder(query);
                                    break;
                                case 17:
                                    DS917.Table = f.datatablegonder(query);
                                    break;
                                case 18:
                                    DS918.Table = f.datatablegonder(query);
                                    break;
                                case 19:
                                    DS919.Table = f.datatablegonder(query);
                                    break;
                                case 20:
                                    DS920.Table = f.datatablegonder(query);
                                    break;
                            }
                            sayac++;
                        }
                        break;
                }
            }
        }
        private void RadioListOrder()
        {
            if (OBJ1_pageId.Length > 0 & OBJ1_fieldId.Length > 0)
            {
                DynamicUtils.preparePageRadioList(rblDynamicList, OBJ1_pageId, OBJ1_fieldId, _obj.Comp, _obj.UserName, _obj.SelectedLanguage);
                if (postURL.Contains("?"))
                {
                    HiddenpageType.Value = postURL+ "GId=" + Request.QueryString["GId"] + "&" + "FaturaStatu=" + pageFaturaStatu + "&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
                }
                else
                {
                    HiddenpageType.Value = postURL + "?" + "GId=" + Request.QueryString["GId"] + "&" + "FaturaStatu=" + pageFaturaStatu + "&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
                }
            }
        }

        private void RadioListrOder()
        {
            //if (pageOrderType == "QUOTE")
            //    RdList.Items[0].Text = "QUOTATION";
            //else if (pageOrderType == "PREBOOKING")
            //    RdList.Items[0].Text = "PREBOOKING";

            //if (pageOrderType == "SHIP")
            //{
            //    RdList.Items[0].Text = "QUOTATION";
            //    RdList.Items.Remove(RdList.Items[2]);
            //}

            //HiddenpageType.Value = postURL + "?" + postURLParameter + "=";

            //if (OBJ1_fieldId == "OJ3002" || OBJ1_fieldId == "OJ3006" || OBJ1_fieldId == "J41010" || OBJ1_fieldId == "J41030")
            //{
            //    RdList.SelectedIndex = 0;

            //}
            //else if (OBJ1_fieldId == "OJ3003" || OBJ1_fieldId == "OJ3007" || OBJ1_pageId == "P41020" || OBJ1_pageId == "P41040")
            //{
            //    RdList.SelectedIndex = 1;
            //}
            //else if (OBJ1_fieldId == "OJ3004" || OBJ1_fieldId == "OJ3008")
            //{
            //    RdList.SelectedIndex = 2;
            //}
            //else if (OBJ1_fieldId == "J50010") //Satınalma İşlemde
            //{
            //    RdListFatura.SelectedIndex = 0;
            //    HiddenpageType.Value = postURL + "?" + "FaturaStatu=ALIMISLEMDE&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
            //    rpMain.Height = Unit.Percentage(65);
            //}
            //else if (OBJ1_fieldId == "J50020") //Satınalma Tamamlandı
            //{
            //    RdListFatura.SelectedIndex = 1;
            //    HiddenpageType.Value = postURL + "?" + "FaturaStatu=ALIMTAMAMLANDI&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
            //    rpMain.Height = Unit.Percentage(65);
            //}
            //else if (OBJ1_fieldId == "J50025") //Satınalma Tamamlandı
            //{
            //    RdListFatura.SelectedIndex = 2;
            //    HiddenpageType.Value = postURL + "?" + "FaturaStatu=ALIMTAMAMLANDITUM&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
            //    rpMain.Height = Unit.Percentage(65);
            //}
            //else if (OBJ1_fieldId == "J50030") //SATIŞ IADE ISLEMDE 
            //{
            //    RdListFatura.SelectedIndex = 0;
            //    HiddenpageType.Value = postURL + "?" + "FaturaStatu=IADEISLEMDE&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
            //    rpMain.Height = Unit.Percentage(65);
            //}
            //else if (OBJ1_fieldId == "J50040") //SATIŞ IADE ISLEMDE 
            //{
            //    RdListFatura.SelectedIndex = 1;
            //    HiddenpageType.Value = postURL + "?" + "FaturaStatu=IADETAMAMLANDI&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
            //    rpMain.Height = Unit.Percentage(65);
            //}
            //else if (OBJ1_fieldId == "J50045") //Satınalma Tamamlandı
            //{
            //    RdListFatura.SelectedIndex = 2;
            //    HiddenpageType.Value = postURL + "?" + "FaturaStatu=IADETAMAMLANDITUM&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
            //    rpMain.Height = Unit.Percentage(65);
            //}
            //else if (OBJ1_fieldId == "J50050") //Satış İşlemde
            //{
            //    RdListFatura.SelectedIndex = 0;
            //    HiddenpageType.Value = postURL + "?" + "FaturaStatu=SATISISLEMDE&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
            //    rpMain.Height = Unit.Percentage(65);
            //}
            //else if (OBJ1_fieldId == "J50060") //Satış Tamamlandı
            //{
            //    RdListFatura.SelectedIndex = 1;
            //    HiddenpageType.Value = postURL + "?" + "FaturaStatu=SATISTAMAMLANDI&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
            //    rpMain.Height = Unit.Percentage(65);
            //}
            //else if (OBJ1_fieldId == "J50065") //Satış Tamamlandı
            //{
            //    RdListFatura.SelectedIndex = 2;
            //    HiddenpageType.Value = postURL + "?" + "FaturaStatu=SATISTAMAMLANDITUM&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
            //    rpMain.Height = Unit.Percentage(65);
            //}
            //else if (OBJ1_fieldId == "J50070") //SATINALMA  IADE ISLEMDE 
            //{
            //    RdListFatura.SelectedIndex = 0;
            //    HiddenpageType.Value = postURL + "?" + "FaturaStatu=ALIMIADEISLEMDE&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
            //    rpMain.Height = Unit.Percentage(65);
            //}
            //else if (OBJ1_fieldId == "J50080") //SATINALMA IADE ISLEMDE 
            //{
            //    RdListFatura.SelectedIndex = 1;
            //    HiddenpageType.Value = postURL + "?" + "FaturaStatu=ALIMIADETAMAMLANDI&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
            //    rpMain.Height = Unit.Percentage(65);
            //}
            //else if (OBJ1_fieldId == "J50085") //SATINALMA IADE ISLEMDE 
            //{
            //    RdListFatura.SelectedIndex = 2;
            //    HiddenpageType.Value = postURL + "?" + "FaturaStatu=ALIMIADETAMAMLANDITUM&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
            //    rpMain.Height = Unit.Percentage(65);
            //}
            //else if (OBJ1_fieldId == "J50110") //BORÇ DEKONTU ISLEMDE 
            //{
            //    RdListFatura.SelectedIndex = 0;
            //    HiddenpageType.Value = postURL + "?" + "FaturaStatu=DEKONTISLEMDE&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
            //    rpMain.Height = Unit.Percentage(65);

            //    RdListFatura.Items.Remove(RdListFatura.Items[2]);
            //}
            //else if (OBJ1_fieldId == "J50120") //BORÇ DEKONTU TAMAMLANAN
            //{
            //    RdListFatura.SelectedIndex = 1;
            //    HiddenpageType.Value = postURL + "?" + "FaturaStatu=DEKONTTAMAMLANDI&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
            //    rpMain.Height = Unit.Percentage(65);
            //    RdListFatura.Items.Remove(RdListFatura.Items[2]);
            //}
            //else if (OBJ1_fieldId == "J50125") //BORÇ DEKONTU TAMAMLANAN
            //{
            //    RdListFatura.SelectedIndex = 2;
            //    HiddenpageType.Value = postURL + "?" + "FaturaStatu=DEKONTTAMAMLANDITUM&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
            //    rpMain.Height = Unit.Percentage(65);
            //}
            //else if (OBJ1_fieldId == "J50130") //ALIM DEKONTU ISLEMDE 
            //{
            //    RdListFatura.SelectedIndex = 0;
            //    HiddenpageType.Value = postURL + "?" + "FaturaStatu=DEKONTISLEMDE&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
            //    rpMain.Height = Unit.Percentage(65);
            //    RdListFatura.Items.Remove(RdListFatura.Items[2]);
            //}
            //else if (OBJ1_fieldId == "J50140") //ALIM DEKONTU TAMAMLANAN
            //{
            //    RdListFatura.SelectedIndex = 1;
            //    HiddenpageType.Value = postURL + "?" + "FaturaStatu=ALIMDEKONTTAMAMLANDI&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
            //    rpMain.Height = Unit.Percentage(65);
            //    RdListFatura.Items.Remove(RdListFatura.Items[2]);
            //}
            //else if (OBJ1_fieldId == "J50145") //ALIM DEKONTU TAMAMLANAN
            //{
            //    RdListFatura.SelectedIndex = 2;
            //    HiddenpageType.Value = postURL + "?" + "FaturaStatu=ALIMDEKONTTAMAMLANDITUM&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
            //    rpMain.Height = Unit.Percentage(65);
            //}



            //if (OBJ1_fieldId == "J50150" | OBJ1_fieldId == "J50210" | OBJ1_fieldId == "J50310")
            //{
            //    RdListEFatura.SelectedIndex = 0;
            //}
            //else if (OBJ1_fieldId == "J50160" | OBJ1_fieldId == "J50220" | OBJ1_fieldId == "J50320")
            //{
            //    RdListEFatura.SelectedIndex = 1;
            //}
            //else if (OBJ1_fieldId == "J50170" | OBJ1_fieldId == "J50230" | OBJ1_fieldId == "J50330")
            //{
            //    RdListEFatura.SelectedIndex = 2;
            //}


        }
        private void CollapseDisplay()
        {
            if (OBJ2_fieldId == "OJ9999")
                rpCargoDetails.Visible = false;
            if (OBJ3_fieldId == "OJ9999")
                rpCostSales.Visible = false;
            if (OBJ4_fieldId == "OJ9999")
                rpDocuments.Visible = false;
            if (OBJ5_fieldId == "OJ9999")
                rpComments.Visible = false;
            if (OBJ6_fieldId == "OJ9999")
                rpTasks.Visible = false;
            if (OBJ7_fieldId == "OJ9999")
                rp7.Visible = false;
            if (OBJ8_fieldId == "OJ9999")
                rp8.Visible = false;
            if (OBJ9_fieldId == "OJ9999")
                rp9.Visible = false;
            if (OBJ1_pageId == "P41020" | OBJ1_pageId == "P41010")
            {
                rpMain.Collapsed = true;
                rpMain.Enabled = false;
            }

            if (OBJ1_pageId == "P171100")
            {
                rpMain.Visible = false;
            }


        }
        private void FillDynamicObjects()
        {

            if (OBJ1_pageId.Length > 0 & OBJ1_fieldId.Length > 0)
            {
                string OIds = OBJ1_pageId, FIds = OBJ1_fieldId;
                List<object> GrdObjs = new List<object>();
                List<object> MenuObjs = new List<object>();
                List<object> InsPObjs = new List<object>();
                List<object> EdtPObjs = new List<object>();
                List<object> DelPObjs = new List<object>();
                List<object> PageParObjs = new List<object>();
                GrdObjs.Add(grid1);
                MenuObjs.Add(menu1);
                InsPObjs.Add(txtinsertparams1);
                EdtPObjs.Add(txteditparams1);
                DelPObjs.Add(txtdeleteparams1);
                PageParObjs.Add(txtlink1);
                PageParObjs.Add(txtlinkField1);
                PageParObjs.Add(txtCatchParameters1);
                PageParObjs.Add(txtCollapseHeader1);
                PageParObjs.Add(txtCollapseStatu1);
                PageParObjs.Add(HiddendirectParam);
                PageParObjs.Add(txtBos);
                PageParObjs.Add(txtRef1);

                if (OBJ2_fieldId != "OJ9999")
                {
                    GrdObjs.Add(grid2);
                    MenuObjs.Add(menu2);
                    InsPObjs.Add(txtinsertparams2);
                    EdtPObjs.Add(txteditparams2);
                    DelPObjs.Add(txtdeleteparams2);
                    OIds += "," + OBJ2_pageId;
                    FIds += "," + OBJ2_fieldId;

                    PageParObjs.Add(txtlink2);
                    PageParObjs.Add(txtlinkField2);
                    PageParObjs.Add(txtCatchParameters2);
                    PageParObjs.Add(txtCollapseHeader2);
                    PageParObjs.Add(txtCollapseStatu2);
                    PageParObjs.Add(hndBos);
                    PageParObjs.Add(txtMasterObject2);
                    PageParObjs.Add(txtRef2);

                }
                else
                    grid2.Visible = false;
                if (OBJ3_fieldId != "OJ9999")
                {
                    GrdObjs.Add(grid3);
                    MenuObjs.Add(menu3);
                    InsPObjs.Add(txtinsertparams3);
                    EdtPObjs.Add(txteditparams3);
                    DelPObjs.Add(txtdeleteparams3);
                    OIds += "," + OBJ3_pageId;
                    FIds += "," + OBJ3_fieldId;

                    PageParObjs.Add(txtlink3);
                    PageParObjs.Add(txtlinkField3);
                    PageParObjs.Add(txtCatchParameters3);
                    PageParObjs.Add(txtCollapseHeader3);
                    PageParObjs.Add(txtCollapseStatu3);
                    PageParObjs.Add(hndBos);
                    PageParObjs.Add(txtMasterObject3);
                    PageParObjs.Add(txtRef3);
                }
                else
                    grid3.Visible = false;
                if (OBJ4_fieldId != "OJ9999")
                {
                    GrdObjs.Add(grid4);
                    MenuObjs.Add(menu4);
                    InsPObjs.Add(txtinsertparams4);
                    EdtPObjs.Add(txteditparams4);
                    DelPObjs.Add(txtdeleteparams4);
                    OIds += "," + OBJ4_pageId;
                    FIds += "," + OBJ4_fieldId;

                    PageParObjs.Add(txtlink4);
                    PageParObjs.Add(txtlinkField4);
                    PageParObjs.Add(txtCatchParameters4);
                    PageParObjs.Add(txtCollapseHeader4);
                    PageParObjs.Add(txtCollapseStatu4);
                    PageParObjs.Add(hndBos);
                    PageParObjs.Add(txtMasterObject4);
                    PageParObjs.Add(txtRef4);
                }
                else
                    grid4.Visible = false;
                if (OBJ5_fieldId != "OJ9999")
                {
                    GrdObjs.Add(grid5);
                    MenuObjs.Add(menu5);
                    InsPObjs.Add(txtinsertparams5);
                    EdtPObjs.Add(txteditparams5);
                    DelPObjs.Add(txtdeleteparams5);
                    OIds += "," + OBJ5_pageId;
                    FIds += "," + OBJ5_fieldId;

                    PageParObjs.Add(txtlink5);
                    PageParObjs.Add(txtlinkField5);
                    PageParObjs.Add(txtCatchParameters5);
                    PageParObjs.Add(txtCollapseHeader5);
                    PageParObjs.Add(txtCollapseStatu5);
                    PageParObjs.Add(hndBos);
                    PageParObjs.Add(txtMasterObject5);
                    PageParObjs.Add(txtRef5);
                }
                else
                    grid5.Visible = false;
                if (OBJ6_fieldId != "OJ9999")
                {
                    GrdObjs.Add(grid6);
                    MenuObjs.Add(menu6);
                    InsPObjs.Add(txtinsertparams6);
                    EdtPObjs.Add(txteditparams6);
                    DelPObjs.Add(txtdeleteparams6);
                    OIds += "," + OBJ6_pageId;
                    FIds += "," + OBJ6_fieldId;

                    PageParObjs.Add(txtlink6);
                    PageParObjs.Add(txtlinkField6);
                    PageParObjs.Add(txtCatchParameters6);
                    PageParObjs.Add(txtCollapseHeader6);
                    PageParObjs.Add(txtCollapseStatu6);
                    PageParObjs.Add(hndBos);
                    PageParObjs.Add(txtMasterObject6);
                    PageParObjs.Add(txtRef6);
                }
                else
                    grid6.Visible = false;
                if (OBJ7_fieldId != "OJ9999")
                {
                    GrdObjs.Add(grid7);
                    MenuObjs.Add(menu7);
                    InsPObjs.Add(txtinsertparams7);
                    EdtPObjs.Add(txteditparams7);
                    DelPObjs.Add(txtdeleteparams7);
                    OIds += "," + OBJ7_pageId;
                    FIds += "," + OBJ7_fieldId;

                    PageParObjs.Add(txtlink7);
                    PageParObjs.Add(txtlinkField7);
                    PageParObjs.Add(txtCatchParameters7);
                    PageParObjs.Add(txtCollapseHeader7);
                    PageParObjs.Add(txtCollapseStatu7);
                    PageParObjs.Add(hndBos);
                    PageParObjs.Add(txtMasterObject7);
                    PageParObjs.Add(txtRef7);
                }
                else
                    grid7.Visible = false;
                if (OBJ8_fieldId != "OJ9999")
                {
                    GrdObjs.Add(grid8);
                    MenuObjs.Add(menu8);
                    InsPObjs.Add(txtinsertparams8);
                    EdtPObjs.Add(txteditparams8);
                    DelPObjs.Add(txtdeleteparams8);
                    OIds += "," + OBJ8_pageId;
                    FIds += "," + OBJ8_fieldId;

                    PageParObjs.Add(txtlink8);
                    PageParObjs.Add(txtlinkField8);
                    PageParObjs.Add(txtCatchParameters8);
                    PageParObjs.Add(txtCollapseHeader8);
                    PageParObjs.Add(txtCollapseStatu8);
                    PageParObjs.Add(hndBos);
                    PageParObjs.Add(txtMasterObject8);
                    PageParObjs.Add(txtRef8);
                }
                else
                    grid8.Visible = false;
                if (OBJ9_fieldId != "OJ9999")
                {
                    GrdObjs.Add(grid9);
                    MenuObjs.Add(menu9);
                    InsPObjs.Add(txtinsertparams9);
                    EdtPObjs.Add(txteditparams9);
                    DelPObjs.Add(txtdeleteparams9);
                    OIds += "," + OBJ9_pageId;
                    FIds += "," + OBJ9_fieldId;

                    PageParObjs.Add(txtlink9);
                    PageParObjs.Add(txtlinkField9);
                    PageParObjs.Add(txtCatchParameters9);
                    PageParObjs.Add(txtCollapseHeader9);
                    PageParObjs.Add(txtCollapseStatu9);
                    PageParObjs.Add(hndBos);
                    PageParObjs.Add(txtMasterObject9);
                    PageParObjs.Add(txtRef9);
                }
                else
                    grid9.Visible = false;

                GenerateRnd();

                dtgrid1.Table = DynamicUtils.FillDynamicObjects(GrdObjs, MenuObjs, OIds, FIds, HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString()
                    , HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), InsPObjs, EdtPObjs, DelPObjs, PageParObjs,
                    "OPL" + ViewState["RndVal"].ToString() + "15Grid",_obj.Comp,_obj.UserName,_obj.SelectedLanguage, _obj.GuidKey);
                DataColumn[] pricols = new DataColumn[1];
                pricols[0] = dtgrid1.Table.Columns["ID"];
                dtgrid1.Table.PrimaryKey = pricols;
                grid1.DataSourceID = "dtgrid1";
                grid1.DataBind();

                FillDS1((DataTable)Session["OPL" + ViewState["RndVal"].ToString() + "15Grid1Rows"]);
                if (OBJ2_fieldId != "OJ9999") FillDS2((DataTable)Session["OPL" + ViewState["RndVal"].ToString() + "15Grid2Rows"]);
                if (OBJ3_fieldId != "OJ9999") FillDS3((DataTable)Session["OPL" + ViewState["RndVal"].ToString() + "15Grid3Rows"]);
                if (OBJ4_fieldId != "OJ9999") FillDS4((DataTable)Session["OPL" + ViewState["RndVal"].ToString() + "15Grid4Rows"]);
                if (OBJ5_fieldId != "OJ9999") FillDS5((DataTable)Session["OPL" + ViewState["RndVal"].ToString() + "15Grid5Rows"]);
                if (OBJ6_fieldId != "OJ9999") FillDS6((DataTable)Session["OPL" + ViewState["RndVal"].ToString() + "15Grid6Rows"]);
                if (OBJ7_fieldId != "OJ9999") FillDS7((DataTable)Session["OPL" + ViewState["RndVal"].ToString() + "15Grid7Rows"]);
                if (OBJ8_fieldId != "OJ9999") FillDS8((DataTable)Session["OPL" + ViewState["RndVal"].ToString() + "15Grid8Rows"]);
                if (OBJ9_fieldId != "OJ9999") FillDS9((DataTable)Session["OPL" + ViewState["RndVal"].ToString() + "15Grid9Rows"]);

            }
            else
            {
                grid1.Visible = false;
            }
        }

        void GenerateRnd()
        {
            Random rnd = new Random();
            ViewState["RndVal"] = rnd.Next(10000, 100000).ToString();
        }
        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }
        protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            Session["InsertedVal"] = string.Empty;
            Session["WebMessage"] = string.Empty;
            ASPxGridView gridI = sender as ASPxGridView;
            string insertparams = string.Empty;
            string[] _temp = { }, _array = { };
            switch (gridI.ID)
            {
                case "grid1":
                    insertparams = txtinsertparams1.Text;
                    _temp = txtinsertparams1.Text.Split(',');
                    _array = new string[txtinsertparams1.Text.Split(',').Length - 1];
                    break;
                case "grid2":
                    insertparams = txtinsertparams2.Text;
                    _temp = txtinsertparams2.Text.Split(',');
                    _array = new string[txtinsertparams2.Text.Split(',').Length - 1];
                    break;
                case "grid3":
                    insertparams = txtinsertparams3.Text;
                    _temp = txtinsertparams3.Text.Split(',');
                    _array = new string[txtinsertparams3.Text.Split(',').Length - 1];
                    break;
                case "grid4":
                    insertparams = txtinsertparams4.Text;
                    _temp = txtinsertparams4.Text.Split(',');
                    _array = new string[txtinsertparams4.Text.Split(',').Length - 1];
                    break;
                case "grid5":
                    insertparams = txtinsertparams5.Text;
                    _temp = txtinsertparams5.Text.Split(',');
                    _array = new string[txtinsertparams5.Text.Split(',').Length - 1];
                    break;
                case "grid6":
                    insertparams = txtinsertparams6.Text;
                    _temp = txtinsertparams6.Text.Split(',');
                    _array = new string[txtinsertparams6.Text.Split(',').Length - 1];
                    break;
                case "grid7":
                    insertparams = txtinsertparams7.Text;
                    _temp = txtinsertparams7.Text.Split(',');
                    _array = new string[txtinsertparams7.Text.Split(',').Length - 1];
                    break;
                case "grid8":
                    insertparams = txtinsertparams8.Text;
                    _temp = txtinsertparams8.Text.Split(',');
                    _array = new string[txtinsertparams8.Text.Split(',').Length - 1];
                    break;
                case "grid9":
                    insertparams = txtinsertparams9.Text;
                    _temp = txtinsertparams9.Text.Split(',');
                    _array = new string[txtinsertparams9.Text.Split(',').Length - 1];
                    break;

            }

            if (!string.IsNullOrEmpty(insertparams))
            {
                bool _dvar = false;
                for (int i = 1; i < _temp.Length; i++)
                {
                    if (_temp[i].Contains("_d") | _temp[i].Contains("_t"))
                        _dvar = true;
                    else
                        _dvar = false;
                    string _tempi = _temp[i].Replace("_d", string.Empty).Replace("_t", string.Empty);
                    if (_tempi == "COMP")
                    {
                        _array[i - 1] = _obj.Comp;
                    }
                    else if (_tempi == "USER")
                    {
                        _array[i - 1] = _obj.UserName;
                    }
                    else if (_tempi == "P1")
                    {
                        _array[i - 1] = this.HiddenP1.Value;
                    }
                    else if (_tempi == "P2")
                    {
                        _array[i - 1] = this.HiddenP2.Value;
                    }
                    else if (_tempi == "P3")
                    {
                        _array[i - 1] = this.HiddenP3.Value;
                    }
                    else if (_tempi == "P4")
                    {
                        _array[i - 1] = this.HiddenP4.Value;
                    }
                    else if (_tempi == "P5")
                    {
                        _array[i - 1] = this.HiddenP5.Value;
                    }
                    else if (string.IsNullOrEmpty(_tempi))
                    {
                        _array[i - 1] = "";
                    }
                    else if (_tempi.Substring(0, 1) == "$")
                    {
                        try { _array[i - 1] = _tempi.Substring(1, _tempi.Length - 1); } catch { _array[i - 1] = ""; }
                    }
                    else if (_tempi.Substring(0, 1) == "#")
                    {
                        try
                        {
                            string[] transparams = txtCatchParamsAndValues.Text.Split('|');
                            string _val1 = Array.Find(transparams, element => element.StartsWith(_tempi.Replace("#", string.Empty), StringComparison.Ordinal));

                            _array[i - 1] = _val1.Split(';')[1];

                        }
                        catch { _array[i - 1] = ""; }
                    }
                    else
                    {
                        if (e.NewValues[_tempi] != null)
                        {
                            if (e.NewValues[_tempi].ToString().Contains('|'))
                                _array[i - 1] = e.NewValues[_tempi].ToString().Split('|')[e.NewValues[_tempi].ToString().Split('|').Length - 1];
                            else
                            {
                                _array[i - 1] = e.NewValues[_tempi].ToString();
                            }
                        }
                        else
                            _array[i - 1] = "";

                    }

                    if (_dvar & !string.IsNullOrEmpty(_array[i - 1])) _array[i - 1] = _temp[i].Substring(0, 2) + _array[i - 1];
                }


                services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                if (!string.IsNullOrEmpty(Session["InsertedVal"].ToString()))
                    gridI.JSProperties["cpQval"] = Session["InsertedVal"].ToString();

            }
            Session[gridI.ID + "Cloned"] = "0";
            Session["TabIndex"] = "1";
            gridI.JSProperties["cpIsUpdated"] = true;
            gridI.JSProperties["cpMsg"] = Session["WebMessage"].ToString();

        }
        protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            Session["InsertedVal"] = string.Empty;
            Session["WebMessage"] = string.Empty;
            ASPxGridView gridI = sender as ASPxGridView;
            string editparams = string.Empty;
            string[] _temp = { }, _array = { };
            switch (gridI.ID)
            {
                case "grid1":
                    editparams = txteditparams1.Text;
                    _temp = txteditparams1.Text.Split(',');
                    _array = new string[txteditparams1.Text.Split(',').Length - 1];
                    break;
                case "grid2":
                    editparams = txteditparams2.Text;
                    _temp = txteditparams2.Text.Split(',');
                    _array = new string[txteditparams2.Text.Split(',').Length - 1];
                    break;
                case "grid3":
                    editparams = txteditparams3.Text;
                    _temp = txteditparams3.Text.Split(',');
                    _array = new string[txteditparams3.Text.Split(',').Length - 1];
                    break;
                case "grid4":
                    editparams = txteditparams4.Text;
                    _temp = txteditparams4.Text.Split(',');
                    _array = new string[txteditparams4.Text.Split(',').Length - 1];
                    break;
                case "grid5":
                    editparams = txteditparams5.Text;
                    _temp = txteditparams5.Text.Split(',');
                    _array = new string[txteditparams5.Text.Split(',').Length - 1];
                    break;
                case "grid6":
                    editparams = txteditparams6.Text;
                    _temp = txteditparams6.Text.Split(',');
                    _array = new string[txteditparams6.Text.Split(',').Length - 1];
                    break;
                case "grid7":
                    editparams = txteditparams7.Text;
                    _temp = txteditparams7.Text.Split(',');
                    _array = new string[txteditparams7.Text.Split(',').Length - 1];
                    break;
                case "grid8":
                    editparams = txteditparams8.Text;
                    _temp = txteditparams8.Text.Split(',');
                    _array = new string[txteditparams8.Text.Split(',').Length - 1];
                    break;
                case "grid9":
                    editparams = txteditparams9.Text;
                    _temp = txteditparams9.Text.Split(',');
                    _array = new string[txteditparams9.Text.Split(',').Length - 1];
                    break;

            }

            if (!string.IsNullOrEmpty(editparams))
            {
                bool _dvar = false;
                for (int i = 1; i < _temp.Length; i++)
                {
                    if (_temp[i].Contains("_d") | _temp[i].Contains("_t"))
                        _dvar = true;
                    else
                        _dvar = false;
                    string _tempi = _temp[i].Replace("_d", string.Empty).Replace("_t", string.Empty);
                    if (_tempi == "COMP")
                    {
                        _array[i - 1] = _obj.Comp;
                    }
                    else if (_tempi == "USER")
                    {
                        _array[i - 1] = _obj.UserName;
                    }
                    else if (_tempi == "P1")
                    {
                        _array[i - 1] = this.HiddenP1.Value;
                    }
                    else if (_tempi == "P2")
                    {
                        _array[i - 1] = this.HiddenP2.Value;
                    }
                    else if (_tempi == "P3")
                    {
                        _array[i - 1] = this.HiddenP3.Value;
                    }
                    else if (_tempi == "P4")
                    {
                        _array[i - 1] = this.HiddenP4.Value;
                    }
                    else if (_tempi == "P5")
                    {
                        _array[i - 1] = this.HiddenP5.Value;
                    }

                    else if (string.IsNullOrEmpty(_tempi))
                    {
                        _array[i - 1] = "";
                    }
                    else if (_tempi.Substring(0, 1) == "$")
                    {
                        try { _array[i - 1] = _tempi.Substring(1, _tempi.Length - 1); } catch { }
                    }
                    else if (_tempi.Substring(0, 1) == "#")
                    {
                        try
                        {
                            string[] transparams = txtCatchParamsAndValues.Text.Split('|');
                            string _val1 = Array.Find(transparams, element => element.StartsWith(_tempi.Replace("#", string.Empty), StringComparison.Ordinal));

                            _array[i - 1] = _val1.Split(';')[1];

                        }
                        catch { }
                    }
                    else
                    {
                        if (e.NewValues[_tempi] != null)
                        {
                            if (e.NewValues[_tempi].ToString().Contains('|'))
                                _array[i - 1] = e.NewValues[_tempi].ToString().Split('|')[e.NewValues[_tempi].ToString().Split('|').Length - 1];
                            else
                            {
                                _array[i - 1] = e.NewValues[_tempi].ToString();
                            }
                        }
                        else
                        {
                            _array[i - 1] = "";
                        }

                    }

                    if (_dvar & !string.IsNullOrEmpty(_array[i - 1])) _array[i - 1] = _temp[i].Substring(0, 2) + _array[i - 1];
                }
                services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                if (!string.IsNullOrEmpty(Session["InsertedVal"].ToString()))
                    gridI.JSProperties["cpQval"] = Session["InsertedVal"].ToString();
            }
            Session[gridI.ID + "Cloned"] = "0";
            Session["TabIndex"] = "1";
            gridI.JSProperties["cpIsUpdated"] = true;
        }
        protected void grid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            Session["InsertedVal"] = string.Empty;
            Session["WebMessage"] = string.Empty;
            ASPxGridView gridI = sender as ASPxGridView;
            string deleteparams = string.Empty;
            string[] _temp = { }, _array = { };
            switch (gridI.ID)
            {
                case "grid1":
                    deleteparams = txtdeleteparams1.Text;
                    _temp = txtdeleteparams1.Text.Split(',');
                    _array = new string[txtdeleteparams1.Text.Split(',').Length - 1];
                    break;
                case "grid2":
                    deleteparams = txtdeleteparams2.Text;
                    _temp = txtdeleteparams2.Text.Split(',');
                    _array = new string[txtdeleteparams2.Text.Split(',').Length - 1];
                    break;
                case "grid3":
                    deleteparams = txtdeleteparams3.Text;
                    _temp = txtdeleteparams3.Text.Split(',');
                    _array = new string[txtdeleteparams3.Text.Split(',').Length - 1];
                    break;
                case "grid4":
                    deleteparams = txtdeleteparams4.Text;
                    _temp = txtdeleteparams4.Text.Split(',');
                    _array = new string[txtdeleteparams4.Text.Split(',').Length - 1];
                    break;
                case "grid5":
                    deleteparams = txtdeleteparams5.Text;
                    _temp = txtdeleteparams5.Text.Split(',');
                    _array = new string[txtdeleteparams5.Text.Split(',').Length - 1];
                    break;
                case "grid6":
                    deleteparams = txtdeleteparams6.Text;
                    _temp = txtdeleteparams6.Text.Split(',');
                    _array = new string[txtdeleteparams6.Text.Split(',').Length - 1];
                    break;
                case "grid7":
                    deleteparams = txtdeleteparams7.Text;
                    _temp = txtdeleteparams7.Text.Split(',');
                    _array = new string[txtdeleteparams7.Text.Split(',').Length - 1];
                    break;
                case "grid8":
                    deleteparams = txtdeleteparams8.Text;
                    _temp = txtdeleteparams8.Text.Split(',');
                    _array = new string[txtdeleteparams8.Text.Split(',').Length - 1];
                    break;
                case "grid9":
                    deleteparams = txtdeleteparams9.Text;
                    _temp = txtdeleteparams9.Text.Split(',');
                    _array = new string[txtdeleteparams9.Text.Split(',').Length - 1];
                    break;

            }

            if (!string.IsNullOrEmpty(deleteparams))
            {
                bool _dvar = false;
                for (int i = 1; i < _temp.Length; i++)
                {
                    if (_temp[i].Contains("_d") | _temp[i].Contains("_t"))
                        _dvar = true;
                    else
                        _dvar = false;
                    string _tempi = _temp[i].Replace("_d", string.Empty).Replace("_t", string.Empty);
                    if (_tempi == "COMP")
                    {
                        _array[i - 1] = _obj.Comp;
                    }
                    else if (_tempi == "USER")
                    {
                        _array[i - 1] = _obj.UserName;
                    }
                    else if (_tempi == "P1")
                    {
                        _array[i - 1] = this.HiddenP1.Value;
                    }
                    else if (_tempi == "P2")
                    {
                        _array[i - 1] = this.HiddenP2.Value;
                    }
                    else if (_tempi == "P3")
                    {
                        _array[i - 1] = this.HiddenP3.Value;
                    }
                    else if (_tempi == "P4")
                    {
                        _array[i - 1] = this.HiddenP4.Value;
                    }
                    else if (_tempi == "P5")
                    {
                        _array[i - 1] = this.HiddenP5.Value;
                    }
                    else if (string.IsNullOrEmpty(_tempi))
                    {
                        _array[i - 1] = "";
                    }
                    else if (_tempi.Substring(0, 1) == "$")
                    {
                        try { _array[i - 1] = _tempi.Substring(1, _tempi.Length - 1); } catch { }
                    }
                    else if (_tempi.Substring(0, 1) == "#")
                    {
                        try
                        {
                            string[] transparams = txtCatchParamsAndValues.Text.Split('|');
                            string _val1 = Array.Find(transparams, element => element.StartsWith(_tempi.Replace("#", string.Empty), StringComparison.Ordinal));

                            _array[i - 1] = _val1.Split(';')[1];

                        }
                        catch { }
                    }
                    else
                    {
                        if (e.Values[_tempi] != null)
                        {
                            if (e.Values[_tempi].ToString().Contains('|'))
                                _array[i - 1] = e.Values[_tempi].ToString().Split('|')[e.Values[_tempi].ToString().Split('|').Length - 1];
                            else
                            {
                                if (_tempi == "R0037")
                                {
                                    if (e.Values[_tempi].ToString() == "COST")
                                        _array[i - 1] = "1";
                                    else if (e.Values[_tempi].ToString() == "SALES")
                                        _array[i - 1] = "2";
                                    else if (e.Values[_tempi].ToString() == "")
                                        _array[i - 1] = "0";
                                    else
                                        _array[i - 1] = e.Values[_tempi].ToString();

                                }
                                else
                                    _array[i - 1] = e.Values[_tempi].ToString();
                            }
                        }
                        else
                            _array[i - 1] = "";

                    }

                    if (_dvar & !string.IsNullOrEmpty(_array[i - 1])) _array[i - 1] = _temp[i].Substring(0, 2) + _array[i - 1];
                }
                services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                if (!string.IsNullOrEmpty(Session["InsertedVal"].ToString()))
                    gridI.JSProperties["cpQval"] = Session["InsertedVal"].ToString();
            }

            gridI.JSProperties["cpIsUpdated"] = true;
            gridI.JSProperties["cpMsg"] = Session["WebMessage"].ToString();
            Session[gridI.ID + "Cloned"] = "0";
            Session["TabIndex"] = "1";
        }
        protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
        {
            Session["TabIndex"] = "1";

            ASPxGridView gridI = sender as ASPxGridView;
            if (copiedValues == null) return;
            string Sessionn = "OPL" + ViewState["RndVal"].ToString() + "15Grid" + gridI.ID.Substring(gridI.ID.Length - 1) + "Rows";
            DataRow[] rows = ((DataTable)Session[Sessionn]).Select("[DX Clone] >0");
            if (rows.Length == 0) return;
            foreach (DataRow row in rows)
            {
                if (row["DX Field ID"].ToString() == "RTYP1")
                {
                    Session["Grid3RTYP1"] = copiedValues[row["DX Field ID"].ToString()];
                    Session["Grid2RTYP1"] = copiedValues[row["DX Field ID"].ToString()];
                }
                else if (row["DX Field ID"].ToString() == "RTYP2")
                {
                    Session["Grid3RTYP2"] = copiedValues[row["DX Field ID"].ToString()];
                    Session["Grid2RTYP1"] = copiedValues[row["DX Field ID"].ToString()];
                }
                else if (row["DX Field ID"].ToString() == "R0037")
                {
                    Session["Grid3R0037"] = copiedValues[row["DX Field ID"].ToString()];
                }
                else if (row["DX Field ID"].ToString() == "R0022")
                {
                    Session["Grid2R0022"] = copiedValues[row["DX Field ID"].ToString()];
                }
                e.NewValues[row["DX Field ID"].ToString()] = copiedValues[row["DX Field ID"].ToString()];
            }

        }
        protected void grid_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            Session["Grid3RTYP1"] = string.Empty;
            Session["Grid3RTYP2"] = string.Empty;
            Session["Grid3R0037"] = string.Empty;
            Session["Grid3R0022"] = string.Empty;

            Session["Grid2RTYP1"] = string.Empty;
            Session["Grid2RTYP2"] = string.Empty;
            Session["Grid2R0022"] = string.Empty;
            ASPxGridView gridI = sender as ASPxGridView;
            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "15Grid" + gridI.ID.Substring(gridI.ID.Length - 1) + "Rows";
            if (e.ButtonID == "Clone")
            {
                string Sessionn = "OPL" + ViewState["RndVal"].ToString() + "15Grid" + gridI.ID.Substring(gridI.ID.Length - 1) + "Rows";
                DataRow[] rows = ((DataTable)Session[Sessionn]).Select("[DX Clone]>0");
                if (rows.Length > 0)
                {
                    copiedValues = new Hashtable();
                    foreach (DataRow row in rows)
                    {
                        copiedValues[row["DX Field ID"].ToString()] = gridI.GetRowValues(e.VisibleIndex, row["DX Field ID"].ToString());
                    }
                }
                Session[gridI.ID + "Cloned"] = "1";
                gridI.AddNewRow();
            }
            else if (e.ButtonID == "SelectAll")
            {
                DataRow[] selectrows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='SELECT' and [DX Display Order]>0");
                string _selcol = selectrows[0]["DX Permision Field"].ToString();
                if (!string.IsNullOrEmpty(_selcol))
                {
                    for (int counter = 0; counter < gridI.VisibleRowCount; counter++)
                    {
                        if (Convert.ToInt32(gridI.GetRowValues(counter, _selcol)) == 1)
                            gridI.Selection.SelectRow(counter);
                    }
                }
                else
                    gridI.Selection.SelectAll();
            }
            else if (e.ButtonID == "UnSelectAll")
            {
                gridI.Selection.UnselectAll();
            }
        }

        #region Grid1

        protected void grid1_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView xgrid = (ASPxGridView)sender;
            string _Session = "OPL" + ViewState["RndVal"].ToString() + "15Grid1Rows";
            if (xgrid.IsNewRowEditing)
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.ClientVisible = false;
                    else
                        e.Editor.ClientVisible = true;
                }

                if (Session[xgrid.ID + "Cloned"].ToString() == "1")
                {
                    DataRow[] clonedRows = ((DataTable)Session[_Session]).Select("[DX Clone]>0");
                    if (clonedRows.Length > 0)
                    {
                        foreach (DataRow row in clonedRows)
                        {
                            if (e.Column.FieldName == row["DX Field ID"].ToString())
                            {
                                if (Convert.ToInt32(row["DX Clone  Readonly"]) > 0)
                                    e.Editor.ClientEnabled = false;
                                else if (Convert.ToInt32(row["DX Clone Hidden"]) > 0)
                                    e.Editor.ClientVisible = false;
                            }
                        }
                    }
                }

            }
            else
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.ClientVisible = false;
                    else
                        e.Editor.ClientVisible = true;
                }
            }



        }
        protected void menu1_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport1.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                DynamicUtils.ExportPdf(gridExport1);
            }
            else if (e.Item.Name.Equals("btninsert"))
            {
                this.HiddenP1.Value = "AXYZT";
                this.HiddenP2.Value = "";
                if (postURL.Contains("?"))
                {
                    iframeView.Attributes.Add("src", postURL + "FaturaStatu=" + invoiceStatu + "&" + "GId=" + Request.QueryString["GId"] + "&InvoiceType=" + invoiceType);
                }
                else
                {
                    iframeView.Attributes.Add("src", postURL + "?FaturaStatu=" + invoiceStatu + "&" + "GId=" + Request.QueryString["GId"] + "&InvoiceType=" + invoiceType);
                }
                HiddenMainStatu.Value = "99";
                try
                {
                    this.dtgrid2.Table.Rows.Clear();
                    grid2.DataBind();
                }
                catch { }
                try
                {
                    this.dtgrid3.Table.Rows.Clear();
                    grid3.DataBind();
                }
                catch { }
                try
                {
                    this.dtgrid4.Table.Rows.Clear();
                    grid4.DataBind();
                }
                catch { }
                try
                {
                    this.dtgrid5.Table.Rows.Clear();
                    grid5.DataBind();
                }
                catch { }
                try
                {
                    this.dtgrid6.Table.Rows.Clear();
                    grid6.DataBind();
                }
                catch { }
            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtils.GetDynamicWebPageObjects(OBJ1_pageId, OBJ1_fieldId, _obj.Comp, _obj.UserName).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid1, rows, _obj.UserName.ToString(), _obj.Comp.ToString());

                try { grid1_CustomCallback(grid1, null); } catch { }
                try { grid2_CustomCallback(grid2, null); } catch { }
                try { grid3_CustomCallback(grid3, null); } catch { }
                try { grid4_CustomCallback(grid4, null); } catch { }
                try { grid5_CustomCallback(grid5, null); } catch { }
                try { grid6_CustomCallback(grid6, null); } catch { }

                iframeView.Attributes.Add("Src", HiddenpageType.Value + HiddenP1.Value);


            }
        }

        #endregion

        #region Grid2
        protected void grid2_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView xgrid = (ASPxGridView)sender;
            string _Session = "OPL" + ViewState["RndVal"].ToString() + "15Grid2Rows";
            if (xgrid.IsNewRowEditing)
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                //e.Editor.TabIndex = Convert.ToInt16(rows[0]["DX Edit Popup Index"]) > -1 ? Convert.ToInt16(rows[0]["DX Edit Popup Index"]) : e.Editor.TabIndex;
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Hidden]=1");
                    if (RowsHidden.Length > 0)
                    {
                        e.Editor.ClientVisible = false;

                    }
                    else
                    {
                        e.Editor.ClientVisible = true;
                    }


                    if (ViewState["OBJ2_Field"].ToString() == "J50011" | ViewState["OBJ2_Field"].ToString() == "J50031")
                    {
                        if (e.Column.FieldName == "RTYP1")
                        {
                            ASPxComboBox cmb = e.Editor as ASPxComboBox;
                            cmb.Callback += CmbChargeTypeAna_Callback;
                        }
                        else if (e.Column.FieldName == "RTYP2")
                        {
                            ASPxComboBox cmb = e.Editor as ASPxComboBox;
                            object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP1");
                            if (!string.IsNullOrEmpty(Session["Grid2RTYP1"].ToString()))
                            {

                                _val = Session["Grid2RTYP1"].ToString();
                                if (_val.ToString().Contains("|"))
                                    _val = _val.ToString().Split('|')[0];
                            }
                            if (_val != null)
                            {
                                if (!string.IsNullOrEmpty(_val.ToString()))
                                {
                                    Session["Grid2RTYP1"] = _val.ToString();
                                    string sorgu = "select NEWID() PK, * from [0_W51162] where RCOMP='" + _obj.Comp.ToString() + "' and R0011='" + _val.ToString() + "'";
                                    f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                                }
                            }

                            cmb.Callback += CmbChargeType1_Callback;
                        }
                        else if (e.Column.FieldName == "RTYP3")
                        {
                            ASPxComboBox cmb = e.Editor as ASPxComboBox;
                            object _val1 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP1");
                            object _val2 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP2");
                            if (!string.IsNullOrEmpty(Session["Grid2RTYP1"].ToString()))
                            {
                                _val1 = Session["Grid2RTYP1"].ToString();
                                if (_val1.ToString().Contains("|"))
                                    _val1 = _val1.ToString().Split('|')[0];
                                _val2 = Session["Grid2RTYP2"].ToString();
                            }
                            if (_val1 != null & _val2 != null)
                            {
                                if (!string.IsNullOrEmpty(_val1.ToString()) & !string.IsNullOrEmpty(_val2.ToString()))
                                {
                                    string sorgu = "select NEWID() PK, * from [0_W51163] where RCOMP='" + _obj.Comp.ToString() + "' and  R0011='" + _val1.ToString() + "' and R0012='" + _val2.ToString() + "'";
                                    f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                                }
                            }
                            cmb.Callback += CmbChargeType2_Callback;

                        }
                        else if (e.Column.FieldName == "R0024")
                        {
                            ASPxComboBox cmb = e.Editor as ASPxComboBox;
                            //object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, "R0022");
                            //if (!string.IsNullOrEmpty(Session["Grid2R0022"].ToString()))
                            //{
                            //    _val = Session["Grid2R0022"].ToString();
                            //}
                            //if (_val != null)
                            //{
                            //    if (!string.IsNullOrEmpty(_val.ToString()))
                            //    {
                            //        string sorgu = "select NEWID() PK, * from  [0_W51172] where RCOMP='" + _obj.Comp.ToString() + "' and R0011='" + _val.ToString() + "'";
                            //        f.FillAspxCombobox(cmb, sorgu, "R0013", "R0012");
                            //    }
                            //}
                            cmb.Callback += CmbJobisNo_Callback;

                        }
                        else if (e.Column.FieldName == "R0040")
                        {
                            ASPxComboBox cmb = e.Editor as ASPxComboBox;
                            cmb.CallbackPageSize = 2000;
                            cmb.Callback += cmbDepartmanCallback;
                        }
                        else if (e.Column.FieldName == "R0042")
                        {
                            ASPxComboBox cmb = e.Editor as ASPxComboBox;
                            cmb.CallbackPageSize = 2000;
                            cmb.Callback += cmbBolgeCallback;
                        }
                        else if (e.Column.FieldName == "R0044")
                        {
                            ASPxComboBox cmb = e.Editor as ASPxComboBox;
                            cmb.CallbackPageSize = 2000;
                            cmb.Callback += cmbFaaliyetCallback;
                        }
                        else if (e.Column.FieldName == "R0046")
                        {
                            ASPxComboBox cmb = e.Editor as ASPxComboBox;
                            cmb.CallbackPageSize = 2000;
                            cmb.Callback += cmbPersonelCallback;
                        }
                        else if (e.Column.FieldName == "R0026")
                        {
                            ASPxComboBox cmb = e.Editor as ASPxComboBox;
                            cmb.SelectedIndex = 1;
                        }
                        else if (e.Column.FieldName == "R0013")
                        {
                            ASPxComboBox cmb = e.Editor as ASPxComboBox;
                            cmb.SelectedIndex = 1;
                        }
                        else if (e.Column.FieldName == "RTEMP")
                        {
                            ASPxComboBox cmb = e.Editor as ASPxComboBox;
                            object _val1 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP1");
                            object _val2 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP2");
                            object _val3 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP3");

                            if (_val1 != null & _val2 != null & _val3 != null)
                            {
                                if (!string.IsNullOrEmpty(_val1.ToString()) & !string.IsNullOrEmpty(_val2.ToString()) & !string.IsNullOrEmpty(_val3.ToString()))
                                {
                                    string sorgu = "select NEWID() PK, * from [0_W51164_01] where RCOMP='" + _obj.Comp.ToString() + "' and  R0013='" + _val1.ToString() + "|" + _val2.ToString() + "|" + _val3.ToString() + "' And R0021='" + Session["SelectedLanguage"].ToString() + "'";
                                    f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                                }
                            }
                            cmb.Callback += RTEMP_Callback;

                        }
                        else if (e.Column.FieldName == "R0011")
                        {
                            string[] prms = HiddenP1.Value.ToString().Split('|');
                            ASPxTextBox txt = e.Editor as ASPxTextBox;
                            if (prms.Length == 1)
                                txt.Text = prms[0].ToString();
                            else if (prms.Length == 2)
                                txt.Text = prms[1].ToString();

                        }
                        else if (e.Column.FieldName == "R0028")
                        {
                            ASPxTextBox txt = e.Editor as ASPxTextBox;
                            txt.Text = "1";
                        }
                    }
                    else if (ViewState["OBJ2_Field"].ToString() == "J50051" | ViewState["OBJ2_Field"].ToString() == "J50071" | ViewState["OBJ2_Field"].ToString() == "J50111" | ViewState["OBJ2_Field"].ToString() == "J50131")
                    {
                        if (e.Column.FieldName == "RTYP1")
                        {
                            ASPxComboBox cmb = e.Editor as ASPxComboBox;
                            cmb.Callback += CmbChargeTypeAna_Callback;
                        }
                        else if (e.Column.FieldName == "RTYP2")
                        {
                            ASPxComboBox cmb = e.Editor as ASPxComboBox;
                            object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP1");
                            if (!string.IsNullOrEmpty(Session["Grid2RTYP1"].ToString()))
                            {

                                _val = Session["Grid2RTYP1"].ToString();
                                if (_val.ToString().Contains("|"))
                                    _val = _val.ToString().Split('|')[0];
                            }
                            if (_val != null)
                            {
                                if (!string.IsNullOrEmpty(_val.ToString()))
                                {
                                    Session["Grid2RTYP1"] = _val.ToString();
                                    string sorgu = "select NEWID() PK, * from [0_W51162] where RCOMP='" + _obj.Comp.ToString() + "' and R0011='" + _val.ToString() + "'";
                                    f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                                }
                            }

                            cmb.Callback += CmbChargeType1_Callback;
                        }
                        else if (e.Column.FieldName == "RTYP3")
                        {
                            ASPxComboBox cmb = e.Editor as ASPxComboBox;
                            object _val1 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP1");
                            object _val2 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP2");
                            if (!string.IsNullOrEmpty(Session["Grid2RTYP1"].ToString()))
                            {
                                _val1 = Session["Grid2RTYP1"].ToString();
                                if (_val1.ToString().Contains("|"))
                                    _val1 = _val1.ToString().Split('|')[0];
                                _val2 = Session["Grid2RTYP2"].ToString();
                            }
                            if (_val1 != null & _val2 != null)
                            {
                                if (!string.IsNullOrEmpty(_val1.ToString()) & !string.IsNullOrEmpty(_val2.ToString()))
                                {
                                    string sorgu = "select NEWID() PK, * from [0_W51163] where RCOMP='" + _obj.Comp.ToString() + "' and  R0011='" + _val1.ToString() + "' and R0012='" + _val2.ToString() + "'";
                                    f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                                }
                            }
                            cmb.Callback += CmbChargeType2_Callback;

                        }
                        else if (e.Column.FieldName == "R0044")
                        {
                            ASPxComboBox cmb = e.Editor as ASPxComboBox;
                            //object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, "R0031");
                            //if (!string.IsNullOrEmpty(Session["Grid2R0022"].ToString()))
                            //{
                            //    _val = Session["Grid2R0022"].ToString();
                            //}
                            //if (_val != null)
                            //{
                            //    if (!string.IsNullOrEmpty(_val.ToString()))
                            //    {
                            //        string sorgu = "select NEWID() PK, * from  [0_W51172] where RCOMP='" + _obj.Comp.ToString() + "' and R0011='" + _val.ToString() + "'";
                            //        f.FillAspxCombobox(cmb, sorgu, "R0013", "R0012");
                            //    }
                            //}
                            cmb.Callback += CmbJobisNo_Callback;

                        }
                        else if (e.Column.FieldName == "R0043")
                        {
                            ASPxComboBox cmb = e.Editor as ASPxComboBox;
                            cmb.CallbackPageSize = 2000;
                            cmb.Callback += cmbDepartmanCallback;
                        }
                        else if (e.Column.FieldName == "R0042")
                        {
                            ASPxComboBox cmb = e.Editor as ASPxComboBox;
                            cmb.CallbackPageSize = 2000;
                            cmb.Callback += cmbBolgeCallback;
                        }
                        else if (e.Column.FieldName == "R0036")
                        {
                            ASPxComboBox cmb = e.Editor as ASPxComboBox;
                            cmb.CallbackPageSize = 2000;
                            cmb.Callback += cmbFaaliyetCallback;
                        }
                        else if (e.Column.FieldName == "R0038")
                        {
                            ASPxComboBox cmb = e.Editor as ASPxComboBox;
                            cmb.CallbackPageSize = 2000;
                            cmb.Callback += cmbPersonelCallback;
                        }
                        else if (e.Column.FieldName == "R0013")
                        {
                            ASPxComboBox cmb = e.Editor as ASPxComboBox;
                            cmb.SelectedIndex = 1;
                        }
                        else if (e.Column.FieldName == "RTEMP")
                        {
                            ASPxComboBox cmb = e.Editor as ASPxComboBox;
                            object _val1 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP1");
                            object _val2 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP2");
                            object _val3 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP3");

                            if (_val1 != null & _val2 != null & _val3 != null)
                            {
                                if (!string.IsNullOrEmpty(_val1.ToString()) & !string.IsNullOrEmpty(_val2.ToString()) & !string.IsNullOrEmpty(_val3.ToString()))
                                {
                                    string sorgu = "select NEWID() PK, * from [0_W51164_01] where RCOMP='" + _obj.Comp.ToString() + "' and  R0013='" + _val1.ToString() + "|" + _val2.ToString() + "|" + _val3.ToString() + "'";
                                    f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                                }
                            }
                            cmb.Callback += RTEMP_Callback;

                        }
                        else if (e.Column.FieldName == "R0011")
                        {
                            string[] prms = HiddenP1.Value.ToString().Split('|');
                            ASPxTextBox txt = e.Editor as ASPxTextBox;
                            if (prms.Length == 1)
                                txt.Text = prms[0].ToString();
                            else if (prms.Length == 2)
                                txt.Text = prms[1].ToString();

                        }
                        else if (e.Column.FieldName == "R0023")
                        {
                            ASPxTextBox txt = e.Editor as ASPxTextBox;
                            if (string.IsNullOrEmpty(txt.Text)) txt.Text = "1";
                        }
                    }

                }

                if (Session[xgrid.ID + "Cloned"].ToString() == "1")
                {
                    DataRow[] clonedRows = ((DataTable)Session[_Session]).Select("[DX Clone]>0");
                    if (clonedRows.Length > 0)
                    {
                        foreach (DataRow row in clonedRows)
                        {
                            if (e.Column.FieldName == row["DX Field ID"].ToString())
                            {
                                if (Convert.ToInt32(row["DX Clone  Readonly"]) > 0)
                                    e.Editor.ClientEnabled = false;
                                else if (Convert.ToInt32(row["DX Clone Hidden"]) > 0)
                                    e.Editor.ClientVisible = false;
                            }
                        }
                    }
                }
            }
            else
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                // e.Editor.TabIndex = Convert.ToInt16(rows[0]["DX Edit Popup Index"]) > -1 ? Convert.ToInt16(rows[0]["DX Edit Popup Index"]) : e.Editor.TabIndex;
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Hidden]=1");
                    if (RowsHidden.Length > 0)
                    {
                        e.Editor.ClientVisible = false;
                    }
                    else
                    {
                        e.Editor.ClientVisible = true;
                    }
                }

                if (ViewState["OBJ2_Field"].ToString() == "J50011" | ViewState["OBJ2_Field"].ToString() == "J50031")
                {
                    if (e.Column.FieldName == "RTYP1")
                    {
                        ASPxComboBox cmb = e.Editor as ASPxComboBox;
                        cmb.Callback += CmbChargeTypeAna_Callback;
                    }
                    else if (e.Column.FieldName == "RTYP2")
                    {
                        ASPxComboBox cmb = e.Editor as ASPxComboBox;
                        object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP1");
                        if (!string.IsNullOrEmpty(Session["Grid2RTYP1"].ToString()))
                        {

                            _val = Session["Grid2RTYP1"].ToString();
                            if (_val.ToString().Contains("|"))
                                _val = _val.ToString().Split('|')[0];
                        }
                        if (_val != null)
                        {
                            if (!string.IsNullOrEmpty(_val.ToString()))
                            {
                                Session["Grid2RTYP1"] = _val.ToString();
                                string sorgu = "select NEWID() PK, * from [0_W51162] where RCOMP='" + _obj.Comp.ToString() + "' and R0011='" + _val.ToString() + "'";
                                f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                            }
                        }

                        cmb.Callback += CmbChargeType1_Callback;
                    }
                    else if (e.Column.FieldName == "RTYP3")
                    {
                        ASPxComboBox cmb = e.Editor as ASPxComboBox;
                        object _val1 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP1");
                        object _val2 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP2");
                        if (!string.IsNullOrEmpty(Session["Grid2RTYP1"].ToString()))
                        {
                            _val1 = Session["Grid2RTYP1"].ToString();
                            if (_val1.ToString().Contains("|"))
                                _val1 = _val1.ToString().Split('|')[0];
                            _val2 = Session["Grid2RTYP2"].ToString();
                        }
                        if (_val1 != null & _val2 != null)
                        {
                            if (!string.IsNullOrEmpty(_val1.ToString()) & !string.IsNullOrEmpty(_val2.ToString()))
                            {
                                string sorgu = "select NEWID() PK, * from [0_W51163] where RCOMP='" + _obj.Comp.ToString() + "' and  R0011='" + _val1.ToString() + "' and R0012='" + _val2.ToString() + "'";
                                f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                            }
                        }
                        cmb.Callback += CmbChargeType2_Callback;

                    }
                    else if (e.Column.FieldName == "R0024")
                    {
                        ASPxComboBox cmb = e.Editor as ASPxComboBox;
                        //object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, "R0022");
                        //if (!string.IsNullOrEmpty(Session["Grid2R0022"].ToString()))
                        //{
                        //    _val = Session["Grid2R0022"].ToString();
                        //}
                        //if (_val != null)
                        //{
                        //    if (!string.IsNullOrEmpty(_val.ToString()))
                        //    {
                        //        string sorgu = "select NEWID() PK, * from  [0_W51172] where RCOMP='" + _obj.Comp.ToString() + "' and R0011='" + _val.ToString() + "'";
                        //        f.FillAspxCombobox(cmb, sorgu, "R0013", "R0012");
                        //    }
                        //}
                        cmb.Callback += CmbJobisNo_Callback;

                    }
                    else if (e.Column.FieldName == "R0040")
                    {
                        ASPxComboBox cmb = e.Editor as ASPxComboBox;
                        cmb.CallbackPageSize = 2000;
                        cmb.Callback += cmbDepartmanCallback;
                    }
                    else if (e.Column.FieldName == "R0042")
                    {
                        ASPxComboBox cmb = e.Editor as ASPxComboBox;
                        cmb.CallbackPageSize = 2000;
                        cmb.Callback += cmbBolgeCallback;
                    }
                    else if (e.Column.FieldName == "R0044")
                    {
                        ASPxComboBox cmb = e.Editor as ASPxComboBox;
                        cmb.CallbackPageSize = 2000;
                        cmb.Callback += cmbFaaliyetCallback;
                    }
                    else if (e.Column.FieldName == "R0046")
                    {
                        ASPxComboBox cmb = e.Editor as ASPxComboBox;
                        cmb.CallbackPageSize = 2000;
                        cmb.Callback += cmbPersonelCallback;
                    }
                    else if (e.Column.FieldName == "R0026")
                    {
                        ASPxComboBox cmb = e.Editor as ASPxComboBox;
                        cmb.SelectedIndex = 1;
                    }
                    else if (e.Column.FieldName == "R0013")
                    {
                        ASPxComboBox cmb = e.Editor as ASPxComboBox;
                        cmb.SelectedIndex = 1;
                    }
                    else if (e.Column.FieldName == "RTEMP")
                    {
                        ASPxComboBox cmb = e.Editor as ASPxComboBox;
                        object _val1 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP1");
                        object _val2 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP2");
                        object _val3 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP3");

                        if (_val1 != null & _val2 != null & _val3 != null)
                        {
                            if (!string.IsNullOrEmpty(_val1.ToString()) & !string.IsNullOrEmpty(_val2.ToString()) & !string.IsNullOrEmpty(_val3.ToString()))
                            {
                                string sorgu = "select NEWID() PK, * from [0_W51164_01] where RCOMP='" + _obj.Comp.ToString() + "' and  R0013='" + _val1.ToString() + "|" + _val2.ToString() + "|" + _val3.ToString() + "' And R0021='" + Session["SelectedLanguage"].ToString() + "'";
                                f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                            }
                        }
                        cmb.Callback += RTEMP_Callback;

                    }
                    else if (e.Column.FieldName == "R0011")
                    {
                        string[] prms = HiddenP1.Value.ToString().Split('|');
                        ASPxTextBox txt = e.Editor as ASPxTextBox;
                        if (prms.Length == 1)
                            txt.Text = prms[0].ToString();
                        else if (prms.Length == 2)
                            txt.Text = prms[1].ToString();

                    }
                    else if (e.Column.FieldName == "R0028")
                    {
                        ASPxTextBox txt = e.Editor as ASPxTextBox;
                        txt.Text = "1";
                    }
                }
                else if (ViewState["OBJ2_Field"].ToString() == "J50051" | ViewState["OBJ2_Field"].ToString() == "J50071" | ViewState["OBJ2_Field"].ToString() == "J50111" | ViewState["OBJ2_Field"].ToString() == "J50131")
                {
                    if (e.Column.FieldName == "RTYP1")
                    {
                        ASPxComboBox cmb = e.Editor as ASPxComboBox;
                        cmb.Callback += CmbChargeTypeAna_Callback;
                    }
                    else if (e.Column.FieldName == "RTYP2")
                    {

                        ASPxComboBox cmb = e.Editor as ASPxComboBox;
                        object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP1");

                        if (_val != null)
                        {
                            if (!string.IsNullOrEmpty(_val.ToString()))
                            {
                                string sorgu = "select NEWID() PK, * from [0_W51162] where RCOMP='" + _obj.Comp.ToString() + "' and R0011='" + _val.ToString() + "'";
                                f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                            }
                        }

                        cmb.Callback += CmbChargeType1_Callback;
                    }
                    else if (e.Column.FieldName == "RTYP3")
                    {
                        ASPxComboBox cmb = e.Editor as ASPxComboBox;
                        object _val1 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP1");
                        object _val2 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP2");

                        if (_val1 != null & _val2 != null)
                        {
                            if (!string.IsNullOrEmpty(_val1.ToString()) & !string.IsNullOrEmpty(_val2.ToString()))
                            {
                                string sorgu = "select NEWID() PK, * from [0_W51163] where RCOMP='" + _obj.Comp.ToString() + "' and  R0011='" + _val1.ToString() + "' and R0012='" + _val2.ToString() + "'";
                                f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                            }
                        }
                        cmb.Callback += CmbChargeType2_Callback;

                    }
                    else if (e.Column.FieldName == "RTEMP")
                    {
                        ASPxComboBox cmb = e.Editor as ASPxComboBox;
                        object _val1 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP1");
                        object _val2 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP2");
                        object _val3 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP3");

                        if (_val1 != null & _val2 != null & _val3 != null)
                        {
                            if (!string.IsNullOrEmpty(_val1.ToString()) & !string.IsNullOrEmpty(_val2.ToString()) & !string.IsNullOrEmpty(_val3.ToString()))
                            {
                                string sorgu = "select NEWID() PK, * from [0_W51164_01] where RCOMP='" + _obj.Comp.ToString() + "' and  R0013='" + _val3.ToString() + "' And R0021='" + Session["SelectedLanguage"].ToString() + "'";
                                f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                            }
                        }
                        cmb.Callback += RTEMP_Callback;

                    }
                    else if (e.Column.FieldName == "R0044")
                    {
                        ASPxComboBox cmb = e.Editor as ASPxComboBox;
                        object _val1 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "R0031");

                        if (!string.IsNullOrEmpty(_val1.ToString()))
                        {
                            string sorgu = "select NEWID() PK, * from [0_W51172] where RCOMP='" + _obj.Comp.ToString() + "' and R0011='" + _val1.ToString() + "'";
                            f.FillAspxCombobox(cmb, sorgu, "R0013", "R0012");
                        }

                        cmb.Callback += CmbJobisNo_Callback;

                    }
                    else if (e.Column.FieldName == "R0043")
                    {
                        ASPxComboBox cmb = e.Editor as ASPxComboBox;
                        cmb.CallbackPageSize = 2000;
                        cmb.Callback += cmbDepartmanCallback;
                    }
                    else if (e.Column.FieldName == "R0042")
                    {
                        ASPxComboBox cmb = e.Editor as ASPxComboBox;
                        cmb.CallbackPageSize = 2000;
                        cmb.Callback += cmbBolgeCallback;
                    }
                    else if (e.Column.FieldName == "R0036")
                    {
                        ASPxComboBox cmb = e.Editor as ASPxComboBox;
                        cmb.CallbackPageSize = 2000;
                        cmb.Callback += cmbFaaliyetCallback;
                    }
                    else if (e.Column.FieldName == "R0038")
                    {
                        ASPxComboBox cmb = e.Editor as ASPxComboBox;
                        cmb.CallbackPageSize = 2000;
                        cmb.Callback += cmbPersonelCallback;
                    }
                    else if (e.Column.FieldName == "R0013")
                    {
                        ASPxComboBox cmb = e.Editor as ASPxComboBox;
                        cmb.SelectedIndex = 1;
                    }
                    else if (e.Column.FieldName == "R0011")
                    {
                        string[] prms = HiddenP1.Value.ToString().Split('|');
                        ASPxTextBox txt = e.Editor as ASPxTextBox;
                        if (prms.Length == 1)
                            txt.Text = prms[0].ToString();
                        else if (prms.Length == 2)
                            txt.Text = prms[1].ToString();

                    }
                    else if (e.Column.FieldName == "R0023")
                    {
                        ASPxTextBox txt = e.Editor as ASPxTextBox;
                        if (string.IsNullOrEmpty(txt.Text)) txt.Text = "1";
                    }
                }

            }


            int tabindex = Convert.ToInt32(Session["TabIndex"]);

            switch (tabindex)
            {
                case 1:
                    e.Editor.TabIndex = 1;
                    break;
                case 4:
                    e.Editor.TabIndex = 2;
                    break;
                case 7:
                    e.Editor.TabIndex = 3;
                    break;
                case 10:
                    e.Editor.TabIndex = 4;
                    break;
                case 13:
                    e.Editor.TabIndex = 5;
                    break;
                case 16:
                    e.Editor.TabIndex = 6;
                    break;
                case 19:
                    e.Editor.TabIndex = 7;
                    break;
                case 2:
                    e.Editor.TabIndex = 8;
                    break;
                case 5:
                    e.Editor.TabIndex = 9;
                    break;
                case 8:
                    e.Editor.TabIndex = 10;
                    break;
                case 11:
                    e.Editor.TabIndex = 11;
                    break;
                case 14:
                    e.Editor.TabIndex = 12;
                    break;
                case 17:
                    e.Editor.TabIndex = 13;
                    break;
                case 20:
                    e.Editor.TabIndex = 14;
                    break;
                case 3:
                    e.Editor.TabIndex = 15;
                    break;
                case 6:
                    e.Editor.TabIndex = 16;
                    break;
                case 9:
                    e.Editor.TabIndex = 17;
                    break;
                case 12:
                    e.Editor.TabIndex = 18;
                    break;
                case 15:
                    e.Editor.TabIndex = 19;
                    break;
                case 18:
                    e.Editor.TabIndex = 20;
                    break;
                case 21:
                    e.Editor.TabIndex = 21;
                    break;
                default:
                    e.Editor.TabIndex = 100;
                    break;
            }

            //int Rtabindex = 0;
            //if (tabindex % 3 == 0)
            //    Rtabindex = tabindex * 1;
            //else if (tabindex % 3 == 1)
            //    Rtabindex = tabindex * 50;
            //else if (tabindex % 3 == 2)
            //    Rtabindex = tabindex * 100;

            //e.Editor.TabIndex = Convert.ToInt16(Rtabindex);

            Session["TabIndex"] = (tabindex + 1).ToString();





        }
        private void RTEMP_Callback(object sender, CallbackEventArgsBase e)
        {
            ASPxComboBox cmb = (ASPxComboBox)sender;
            if (!string.IsNullOrEmpty(e.Parameter))
            {
                string sorgu = "select NEWID() PK, * from [0_W51164_01] where RCOMP='" + _obj.Comp.ToString() + "' and  R0013='" + e.Parameter + "' And R0021='" + Session["SelectedLanguage"].ToString() + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                cmb.SelectedIndex = 1;
            }

        }
        private void cmbBolgeCallback(object sender, CallbackEventArgsBase e)
        {
            ASPxComboBox cmb = (ASPxComboBox)sender;

            string[] param = e.Parameter.Split(',');

            if (param[0].ToString() == "R0024" | param[0].ToString() == "R0044")
            {
                string sorgu = "select  BLG from [0C_01001_01_JOB_TASK] where COMPANY='" + _obj.Comp.ToString() + "' and [Job Task No_]='" + param[1] + "'";
                string _dept = f.tekbirsonucdonder(sorgu);
                if (_dept != "" & _dept != "NULL")
                {
                    cmb.SelectedItem = cmb.Items.FindByValue(_dept);
                }
            }
            else if (param[0].ToString() == "R0048" | param[0].ToString() == "R0040")
            {
                string sorgu = "select  [Bolge Kodu] from [0C_50029_00_HAT_ARAC_KAYIT] where COMPANY='" + _obj.Comp.ToString() + "' and [Arac Boyutu]='" + param[1] + "'";
                string _dept = f.tekbirsonucdonder(sorgu);
                if (_dept != "" & _dept != "NULL")
                {
                    cmb.SelectedItem = cmb.Items.FindByValue(_dept);
                }
            }
            else if (param[0].ToString() == "R0046" | param[0].ToString() == "R0038")
            {

                string sorgu = "select  [BLG] from [0C_05200_01_EMPLOYEE] where COMPANY='" + _obj.Comp.ToString() + "' and [No_]='" + param[1] + "'";
                string _dept = f.tekbirsonucdonder(sorgu);
                if (_dept != "" & _dept != "NULL")
                {
                    cmb.SelectedItem = cmb.Items.FindByValue(_dept);
                }

            }
        }
        private void cmbFaaliyetCallback(object sender, CallbackEventArgsBase e)
        {
            ASPxComboBox cmb = (ASPxComboBox)sender;

            string[] param = e.Parameter.Split(',');

            if (param[0].ToString() == "R0024" | param[0].ToString() == "R0044")
            {
                string sorgu = "select  FAAL from [0C_01001_01_JOB_TASK] where COMPANY='" + _obj.Comp.ToString() + "' and [Job Task No_]='" + param[1] + "'";
                string _dept = f.tekbirsonucdonder(sorgu);
                if (_dept != "" & _dept != "NULL")
                {
                    cmb.SelectedItem = cmb.Items.FindByValue(_dept);
                }
            }
            else if (param[0].ToString() == "R0046" | param[0].ToString() == "R0038")
            {

                string sorgu = "select  [FAAL] from [0C_05200_01_EMPLOYEE] where COMPANY='" + _obj.Comp.ToString() + "' and [No_]='" + param[1] + "'";
                string _dept = f.tekbirsonucdonder(sorgu);
                if (_dept != "" & _dept != "NULL")
                {
                    cmb.SelectedItem = cmb.Items.FindByValue(_dept);
                }



            }

        }
        private void cmbPersonelCallback(object sender, CallbackEventArgsBase e)
        {
            ASPxComboBox cmb = (ASPxComboBox)sender;
            string[] param = e.Parameter.Split(',');

            string sorgu = "select [Personel Kodu] from [0C_50029_00_HAT_ARAC_KAYIT] where COMPANY='" + _obj.Comp.ToString() + "' and [Arac Boyutu]='" + param[1] + "'";
            string _dept = f.tekbirsonucdonder(sorgu);
            if (_dept != "" & _dept != "NULL")
            {
                cmb.SelectedItem = cmb.Items.FindByValue(_dept);
            }

        }
        private void cmbDepartmanCallback(object sender, CallbackEventArgsBase e)
        {
            ASPxComboBox cmb = (ASPxComboBox)sender;
            string[] param = e.Parameter.Split(',');

            if (param[0].ToString() == "R0024" | param[0].ToString() == "R0044")
            {
                string sorgu = "select  DEPT from [0C_01001_01_JOB_TASK] where COMPANY='" + _obj.Comp.ToString() + "' and [Job Task No_]='" + param[1] + "'";
                string _dept = f.tekbirsonucdonder(sorgu);
                if (_dept != "" & _dept != "NULL")
                {
                    cmb.SelectedItem = cmb.Items.FindByValue(_dept);
                }

            }
            else if (param[0].ToString() == "R0048" | param[0].ToString() == "R0040")
            {
                string sorgu = "select [Departman Kodu] from [0C_50029_00_HAT_ARAC_KAYIT] where COMPANY='" + _obj.Comp.ToString() + "' and [Arac Boyutu]='" + param[1] + "'";
                string _dept = f.tekbirsonucdonder(sorgu);
                if (_dept != "" & _dept != "NULL")
                {
                    cmb.SelectedItem = cmb.Items.FindByValue(_dept);
                }
            }
            else if (param[0].ToString() == "R0046" | param[0].ToString() == "R0038")
            {

                string sorgu = "select  [DEPT] from [0C_05200_01_EMPLOYEE] where COMPANY='" + _obj.Comp.ToString() + "' and [No_]='" + param[1] + "'";
                string _dept = f.tekbirsonucdonder(sorgu);
                if (_dept != "" & _dept != "NULL")
                {
                    cmb.SelectedItem = cmb.Items.FindByValue(_dept);
                }

            }
        }
        private void CmbJobisNo_Callback(object sender, CallbackEventArgsBase e)
        {

            if (!string.IsNullOrEmpty(e.Parameter))
            {
                ASPxComboBox cmb = (ASPxComboBox)sender;
                string sorgu = "select NEWID() PK, * from [0_W51172] where RCOMP='" + _obj.Comp.ToString() + "' and R0011='" + e.Parameter + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0013", "R0012");
            }
        }
        private void CmbJobisNo1_Callback(object sender, CallbackEventArgsBase e)
        {
            if (!string.IsNullOrEmpty(Session["Grid2R0022"].ToString()))
            {
                ASPxComboBox cmb = (ASPxComboBox)sender;
                string CustVend = string.Empty;

                string sorgu = "select NEWID() PK, * from [0_W51172] where RCOMP='" + _obj.Comp.ToString() + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0013", "R0012");
                Session["Grid2R0022"] = string.Empty;
            }
            else
            {
                if (!string.IsNullOrEmpty(e.Parameter))
                {
                    ASPxComboBox cmb = (ASPxComboBox)sender;
                    string CustVend = string.Empty;

                    string sorgu = "select NEWID() PK, * from [0_W51172] where RCOMP='" + _obj.Comp.ToString() + "'";
                    f.FillAspxCombobox(cmb, sorgu, "R0013", "R0012");
                }
            }

        }
        private void CmbChargeTypeAna_Callback(object sender, CallbackEventArgsBase e)
        {
            if (!string.IsNullOrEmpty(e.Parameter))
            {
                ASPxComboBox cmb = (ASPxComboBox)sender;
                string CustVend = string.Empty;

                string sorgu = "select NEWID() PK, * from  [0_W51161]  where RCOMP='" + _obj.Comp.ToString() + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
            }
        }
        private void CmbChargeType1_Callback(object sender, CallbackEventArgsBase e)
        {
            if (!string.IsNullOrEmpty(e.Parameter))
            {

                ASPxComboBox cmb = (ASPxComboBox)sender;
                string sorgu = "select NEWID() PK, * from [0_W51162] where RCOMP='" + _obj.Comp.ToString() + "' and  R0011='" + e.Parameter + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                Session["Grid2RTYP1"] = e.Parameter.ToString();

            }

        }
        private void CmbChargeType2_Callback(object sender, CallbackEventArgsBase e)
        {

            if (!string.IsNullOrEmpty(e.Parameter))
            {
                ASPxComboBox cmb = (ASPxComboBox)sender;
                string sorgu = "select NEWID() PK, * from [0_W51163] where RCOMP='" + _obj.Comp.ToString() + "' and  R0011='" + Session["Grid2RTYP1"].ToString() + "' and R0012='" + e.Parameter + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
            }

        }

        protected void Grid_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;
            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "15Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";
            switch (e.ButtonType)
            {
                case ColumnCommandButtonType.Edit:

                    DataRow[] EditRows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='EDIT' and [DX Display Order]>0");
                    if (EditRows.Length > 0)
                    {
                        string _col = EditRows[0]["DX Permision Field"].ToString();
                        if (grid.GetRowValues(e.VisibleIndex, _col) != null)
                        {
                            if (grid.GetRowValues(e.VisibleIndex, _col).ToString() == "0")
                            {
                                e.Visible = false;
                            }
                        }
                    }
                    else
                        e.Visible = false;
                    break;
                case ColumnCommandButtonType.Delete:
                    DataRow[] DeleteRows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='DELETE' and [DX Display Order]>0");
                    if (DeleteRows.Length > 0)
                    {
                        string _col = DeleteRows[0]["DX Permision Field"].ToString();
                        if (grid.GetRowValues(e.VisibleIndex, _col) != null)
                        {
                            if (grid.GetRowValues(e.VisibleIndex, _col).ToString() == "0")
                            {
                                e.Visible = false;
                            }
                        }
                    }
                    else
                        e.Visible = false;
                    break;
                case ColumnCommandButtonType.SelectCheckbox:
                case ColumnCommandButtonType.Select:
                    DataRow[] selectrows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='SELECT' and [DX Display Order]>0");
                    string _selcol = selectrows[0]["DX Permision Field"].ToString();
                    if (!string.IsNullOrEmpty(_selcol))
                    {
                        if (grid.GetRowValues(e.VisibleIndex, _selcol).ToString() == "0")
                            e.Enabled = false;
                    }
                    break;


            }
        }
        protected void menu2_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport2.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                DynamicUtils.ExportPdf(gridExport2);
            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtils.GetDynamicWebPageObjects(OBJ2_pageId, OBJ2_fieldId, _obj.Comp, _obj.UserName).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid2, rows, _obj.UserName.ToString(), _obj.Comp.ToString());

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }
        #endregion

        #region Grid3
        protected void grid3_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView xgrid = (ASPxGridView)sender;
            string _Session = "OPL" + ViewState["RndVal"].ToString() + "15Grid3Rows";

            if (xgrid.IsNewRowEditing)
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.ClientVisible = false;
                    else
                        e.Editor.ClientVisible = true;
                }

                if (Session[xgrid.ID + "Cloned"].ToString() == "1")
                {
                    DataRow[] clonedRows = ((DataTable)Session[_Session]).Select("[DX Clone]>0");
                    if (clonedRows.Length > 0)
                    {
                        foreach (DataRow row in clonedRows)
                        {
                            if (e.Column.FieldName == row["DX Field ID"].ToString())
                            {
                                if (Convert.ToInt32(row["DX Clone  Readonly"]) > 0)
                                    e.Editor.ClientEnabled = false;
                                else if (Convert.ToInt32(row["DX Clone Hidden"]) > 0)
                                    e.Editor.ClientVisible = false;
                            }
                        }
                    }
                }


            }
            else
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.ClientVisible = false;
                    else
                        e.Editor.ClientVisible = true;
                }

                Session["Grid3RTYP1"] = string.Empty;
                Session["Grid3RTYP2"] = string.Empty;
                Session["Grid3R0037"] = string.Empty;
            }

            string[] linkFields = txtlinkField1.Text.Split(';');

            if (e.Column.FieldName == "RTYP2")
            {
                ASPxComboBox cmb = e.Editor as ASPxComboBox;

                object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP1");
                if (!string.IsNullOrEmpty(Session["Grid3RTYP1"].ToString()))
                {
                    _val = Session["Grid3RTYP1"].ToString();

                }
                if (_val != null)
                {
                    if (!string.IsNullOrEmpty(_val.ToString()))
                    {
                        Session["OPL10R0011"] = _val.ToString();
                        string sorgu = "select NEWID() PK, * from [0_W10492] where RCOMP='" + _obj.Comp.ToString() + "' and R0011='" + _val.ToString() + "'";
                        f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                    }
                }
                cmb.Callback += Cmb_Callback;
            }
            else if (e.Column.FieldName == "RTYP3")
            {
                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                object _val1 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP1");
                object _val2 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP2");
                if (!string.IsNullOrEmpty(Session["Grid3RTYP1"].ToString()))
                {
                    _val1 = Session["Grid3RTYP1"].ToString();
                    _val2 = Session["Grid3RTYP2"].ToString();
                }
                if (_val1 != null & _val2 != null)
                {
                    if (!string.IsNullOrEmpty(_val1.ToString()) & !string.IsNullOrEmpty(_val2.ToString()))
                    {
                        string sorgu = "select NEWID() PK, * from [0_W10493] where RCOMP='" + _obj.Comp.ToString() + "' and  R0011='" + _val1.ToString() + "' and R0012='" + _val2.ToString() + "'";
                        f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                    }
                }
                cmb.Callback += Cmb2_Callback;
            }
            else if (e.Column.FieldName == "R0042")
            {
                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, "R0037");
                if (!string.IsNullOrEmpty(Session["Grid3R0037"].ToString()))
                {
                    _val = Session["Grid3R0037"].ToString();
                }
                if (_val != null)
                {
                    if (!string.IsNullOrEmpty(_val.ToString()))
                    {
                        string CustVend = string.Empty;
                        if (_val.ToString() == "1" | _val.ToString() == "COST")
                            CustVend = "VENDOR";
                        else if (_val.ToString() == "2" | _val.ToString() == "SALES")
                            CustVend = "CUSTOMER";
                        string sorgu = "select NEWID() PK, * from [0_W10480] where RCOMP='" + _obj.Comp.ToString() + "' and  R0011='" + CustVend + "'";
                        f.FillAspxCombobox(cmb, sorgu, "R0013", "R0012");
                    }
                }
                cmb.Callback += Cmb3_Callback;
            }


            if (linkFields.Length == 2)
            {
                if (e.Column.FieldName == linkFields[0])
                {
                    e.Editor.Value = HiddenP1.Value;
                }
                else if (e.Column.FieldName == linkFields[1])
                {
                    e.Editor.Value = HiddenP2.Value;
                }
            }
            else if (linkFields.Length == 1)
            {
                if (e.Column.FieldName == linkFields[0])
                {
                    e.Editor.Value = HiddenP1.Value;

                }
            }

            if (e.Column.FieldName == "R0014")
            {
                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                cmb.Callback += CmbJobisNo_Callback;
            }


        }
        private void Cmb_Callback(object sender, CallbackEventArgsBase e)
        {
            if (!string.IsNullOrEmpty(Session["Grid3RTYP1"].ToString()))
            {
                ASPxComboBox cmb = (ASPxComboBox)sender;
                string sorgu = "select NEWID() PK, * from [0_W10492] where RCOMP='" + _obj.Comp.ToString() + "' and  R0011='" + Session["Grid3RTYP1"].ToString() + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                Session["OPL10R0011"] = Session["Grid3RTYP1"].ToString();
                Session["Grid3RTYP1"] = string.Empty;
            }
            else
            {
                if (!string.IsNullOrEmpty(e.Parameter))
                {

                    ASPxComboBox cmb = (ASPxComboBox)sender;
                    string sorgu = "select NEWID() PK, * from [0_W10492] where RCOMP='" + _obj.Comp.ToString() + "' and  R0011='" + e.Parameter + "'";
                    f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                    Session["OPL10R0011"] = e.Parameter.ToString();

                }
            }

        }
        private void Cmb2_Callback(object sender, CallbackEventArgsBase e)
        {
            if (!string.IsNullOrEmpty(Session["Grid3RTYP2"].ToString()))
            {
                ASPxComboBox cmb = (ASPxComboBox)sender;
                string sorgu = "select NEWID() PK, * from [0_W10493] where RCOMP='" + _obj.Comp.ToString() + "' and  R0011='" + Session["OPL10R0011"].ToString() + "' and R0012='" + Session["Grid3RTYP2"].ToString() + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                Session["Grid3RTYP2"] = string.Empty;
            }
            else
            {
                if (!string.IsNullOrEmpty(e.Parameter))
                {
                    ASPxComboBox cmb = (ASPxComboBox)sender;
                    string sorgu = "select NEWID() PK, * from [0_W10493] where RCOMP='" + _obj.Comp.ToString() + "' and  R0011='" + Session["OPL10R0011"].ToString() + "' and R0012='" + e.Parameter + "'";
                    f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                }
            }

        }
        private void Cmb3_Callback(object sender, CallbackEventArgsBase e)
        {
            //throw new Exception(e.Parameter.ToString());

            if (!string.IsNullOrEmpty(Session["Grid3R0037"].ToString()))
            {
                ASPxComboBox cmb = (ASPxComboBox)sender;
                string CustVend = string.Empty;
                if (Session["Grid3R0037"].ToString() == "1")
                    CustVend = "VENDOR";
                else if (Session["Grid3R0037"].ToString() == "2")
                    CustVend = "CUSTOMER";
                string sorgu = "select NEWID() PK, * from [0_W10480] where RCOMP='" + _obj.Comp.ToString() + "' and  R0011='" + CustVend + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0013", "R0012");
                Session["Grid3R0037"] = string.Empty;
            }
            else
            {
                if (!string.IsNullOrEmpty(e.Parameter))
                {
                    ASPxComboBox cmb = (ASPxComboBox)sender;
                    string CustVend = string.Empty;
                    if (e.Parameter.ToString() == "1")
                        CustVend = "VENDOR";
                    else if (e.Parameter.ToString() == "2")
                        CustVend = "CUSTOMER";
                    string sorgu = "select NEWID() PK, * from [0_W10480] where RCOMP='" + _obj.Comp.ToString() + "' and R0011='" + CustVend + "'";
                    f.FillAspxCombobox(cmb, sorgu, "R0013", "R0012");
                }
            }

        }
        protected void menu3_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport3.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                DynamicUtils.ExportPdf(gridExport3);
            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtils.GetDynamicWebPageObjects(OBJ3_pageId, OBJ3_fieldId, _obj.Comp, _obj.UserName).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid3, rows, _obj.UserName.ToString(), _obj.Comp.ToString());

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }
        #endregion

        #region Grid4

        protected void grid4_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView xgrid = (ASPxGridView)sender;
            string _Session = "OPL" + ViewState["RndVal"].ToString() + "15Grid4Rows";
            if (xgrid.IsNewRowEditing)
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.ClientVisible = false;
                    else
                        e.Editor.ClientVisible = true;
                }

                if (Session[xgrid.ID + "Cloned"].ToString() == "1")
                {
                    DataRow[] clonedRows = ((DataTable)Session[_Session]).Select("[DX Clone]>0");
                    if (clonedRows.Length > 0)
                    {
                        foreach (DataRow row in clonedRows)
                        {
                            if (e.Column.FieldName == row["DX Field ID"].ToString())
                            {
                                if (Convert.ToInt32(row["DX Clone  Readonly"]) > 0)
                                    e.Editor.ClientEnabled = false;
                                else if (Convert.ToInt32(row["DX Clone Hidden"]) > 0)
                                    e.Editor.ClientVisible = false;
                            }
                        }
                    }
                }


            }
            else
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.ClientVisible = false;
                    else
                        e.Editor.ClientVisible = true;
                }
            }

        }
        protected void menu4_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport4.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                DynamicUtils.ExportPdf(gridExport4);
            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtils.GetDynamicWebPageObjects(OBJ4_pageId, OBJ4_fieldId, _obj.Comp, _obj.UserName).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid4, rows, _obj.UserName.ToString(), _obj.Comp.ToString());

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }
        #endregion

        #region Grid5

        protected void grid5_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView xgrid = (ASPxGridView)sender;
            string _Session = "OPL" + ViewState["RndVal"].ToString() + "15Grid5Rows";
            if (xgrid.IsNewRowEditing)
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.ClientVisible = false;
                    else
                        e.Editor.ClientVisible = true;
                }

                if (Session[xgrid.ID + "Cloned"].ToString() == "1")
                {
                    DataRow[] clonedRows = ((DataTable)Session[_Session]).Select("[DX Clone]>0");
                    if (clonedRows.Length > 0)
                    {
                        foreach (DataRow row in clonedRows)
                        {
                            if (e.Column.FieldName == row["DX Field ID"].ToString())
                            {
                                if (Convert.ToInt32(row["DX Clone  Readonly"]) > 0)
                                    e.Editor.ClientEnabled = false;
                                else if (Convert.ToInt32(row["DX Clone Hidden"]) > 0)
                                    e.Editor.ClientVisible = false;
                            }
                        }
                    }
                }


            }
            else
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.ClientVisible = false;
                    else
                        e.Editor.ClientVisible = true;
                }
            }


        }
        protected void menu5_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport5.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                DynamicUtils.ExportPdf(gridExport5);
            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtils.GetDynamicWebPageObjects(OBJ5_pageId, OBJ5_fieldId, _obj.Comp, _obj.UserName).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid5, rows, _obj.UserName.ToString(), _obj.Comp.ToString());

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }
        #endregion

        #region Grid6

        protected void grid6_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView xgrid = (ASPxGridView)sender;
            string _Session = "OPL" + ViewState["RndVal"].ToString() + "15Grid6Rows";

            // cascading dinamik çalışması devam edecek.
            //DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Casc Target]<>''");
            //foreach (DataRow row in rows)
            //{
            //    string cascadeobj = row["Dx Field ID"].ToString();
            //    string[] cascadedobjs = row["Dx Casc Target"].ToString().Split(',');
            //}

            if (txtOBJF6.Text == "OJ3009")
            {
                if (e.Column.FieldName == "R0027")
                {
                    ASPxComboBox cmb = e.Editor as ASPxComboBox;
                    object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, "R0015");
                    if (_val != null)
                    {
                        if (!string.IsNullOrEmpty(_val.ToString()))
                        {
                            string sorgu = "select NEWID() PK, t1.* from [0_W10081] t1 left join [0_W10450] t2 on t1.RTASK=t2.RTASK where t2.RCOMP='" + _obj.Comp.ToString() + "' and t1.RCOMP='" + _obj.Comp.ToString() + "' and t1.RTASK=t2.RTASK " +
                            "and t2.R0011='" + _val.ToString() + "'";
                            f.FillAspxCombobox(cmb, sorgu, "R0014", "R0012");
                        }
                    }
                    cmb.Callback += Cmb_Callback1;
                }
                else if (e.Column.FieldName == "R0031")
                {
                    ASPxComboBox cmb = e.Editor as ASPxComboBox;
                    object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, "R0015");
                    if (_val != null)
                    {
                        if (!string.IsNullOrEmpty(_val.ToString()))
                        {
                            string sorgu = "select NEWID() PK, t1.* from [0_W10031] t1 left join [0_W10450] t2 on t1.RTASK=t2.RTASK where t2.RCOMP='" + _obj.Comp.ToString() + "' and t1.RCOMP='" + _obj.Comp.ToString() + "' and t1.RTASK=t2.RTASK " +
                            "and t2.R0011='" + _val.ToString() + "'";
                            f.FillAspxCombobox(cmb, sorgu, "R0012", "R0011");
                        }
                    }
                    cmb.Callback += Cmb_Callback2;
                }
                else if (e.Column.FieldName == "R0033")
                {
                    ASPxComboBox cmb = e.Editor as ASPxComboBox;
                    object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, "R0015");
                    if (_val != null)
                    {
                        if (!string.IsNullOrEmpty(_val.ToString()))
                        {
                            string sorgu = "select NEWID() PK, t1.* from [0_W10031] t1 left join [0_W10450] t2 on t1.RTASK=t2.RTASK where t2.RCOMP='" + _obj.Comp.ToString() + "' and  t1.RCOMP='" + _obj.Comp.ToString() + "' and t1.RTASK = t2.RTASK " +
                            "and t2.R0011='" + _val.ToString() + "'";
                            f.FillAspxCombobox(cmb, sorgu, "R0012", "R0011");
                        }
                    }
                    cmb.Callback += Cmb_Callback3;
                }
            }
            if (xgrid.IsNewRowEditing)
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.ClientVisible = false;
                    else
                        e.Editor.ClientVisible = true;
                }

                if (Session[xgrid.ID + "Cloned"].ToString() == "1")
                {
                    DataRow[] clonedRows = ((DataTable)Session[_Session]).Select("[DX Clone]>0");
                    if (clonedRows.Length > 0)
                    {
                        foreach (DataRow row in clonedRows)
                        {
                            if (e.Column.FieldName == row["DX Field ID"].ToString())
                            {
                                if (Convert.ToInt32(row["DX Clone  Readonly"]) > 0)
                                    e.Editor.ClientEnabled = false;
                                else if (Convert.ToInt32(row["DX Clone Hidden"]) > 0)
                                    e.Editor.ClientVisible = false;
                            }
                        }
                    }
                }


            }
            else
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.ClientVisible = false;
                    else
                        e.Editor.ClientVisible = true;
                }
            }



        }
        private void Cmb_Callback3(object sender, CallbackEventArgsBase e)
        {
            if (e.Parameter.ToString() != "")
            {
                ASPxComboBox cmb = (ASPxComboBox)sender;

                string sorgu = "select NEWID() PK, t1.* from [0_W10031] t1 left join [0_W10450] t2 on t1.RTASK=t2.RTASK where t2.RCOMP='" + _obj.Comp.ToString() + "' and  t1.RCOMP='" + _obj.Comp.ToString() + "' and  t1.RTASK = t2.RTASK " +
                     "and t2.R0011='" + e.Parameter.ToString() + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0012", "R0011");

            }
        }
        private void Cmb_Callback2(object sender, CallbackEventArgsBase e)
        {
            if (e.Parameter.ToString() != "")
            {
                ASPxComboBox cmb = (ASPxComboBox)sender;

                string sorgu = "select NEWID() PK, t1.* from [0_W10031] t1 left join [0_W10450] t2 on t1.RTASK=t2.RTASK where t2.RCOMP='" + _obj.Comp.ToString() + "' and  t1.RCOMP='" + _obj.Comp.ToString() + "' and  t1.RTASK = t2.RTASK " +
                    "and t2.R0011='" + e.Parameter.ToString() + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0012", "R0011");

            }
        }
        private void Cmb_Callback1(object sender, CallbackEventArgsBase e)
        {
            if (e.Parameter.ToString() != "")
            {
                ASPxComboBox cmb = (ASPxComboBox)sender;

                string sorgu = "select NEWID() PK, t1.* from [0_W10081] t1 left join [0_W10450] t2 on t1.RTASK=t2.RTASK where t2.RCOMP='" + _obj.Comp.ToString() + "' and  t1.RCOMP='" + _obj.Comp.ToString() + "' and  t1.RTASK = t2.RTASK " +
                    "and t2.R0011='" + e.Parameter.ToString() + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0014", "R0012");

            }
        }
        protected void menu6_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport6.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                DynamicUtils.ExportPdf(gridExport6);
            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtils.GetDynamicWebPageObjects(OBJ6_pageId, OBJ6_fieldId, _obj.Comp, _obj.UserName).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid6, rows, _obj.UserName.ToString(), _obj.Comp.ToString());

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }
        #endregion

        #region Grid7

        protected void grid7_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView xgrid = (ASPxGridView)sender;
            string _Session = "OPL" + ViewState["RndVal"].ToString() + "15Grid7Rows";

            // cascading dinamik çalışması devam edecek.
            //DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Casc Target]<>''");
            //foreach (DataRow row in rows)
            //{
            //    string cascadeobj = row["Dx Field ID"].ToString();
            //    string[] cascadedobjs = row["Dx Casc Target"].ToString().Split(',');
            //}
            if (xgrid.IsNewRowEditing)
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.ClientVisible = false;
                    else
                        e.Editor.ClientVisible = true;
                }

                if (Session[xgrid.ID + "Cloned"].ToString() == "1")
                {
                    DataRow[] clonedRows = ((DataTable)Session[_Session]).Select("[DX Clone]>0");
                    if (clonedRows.Length > 0)
                    {
                        foreach (DataRow row in clonedRows)
                        {
                            if (e.Column.FieldName == row["DX Field ID"].ToString())
                            {
                                if (Convert.ToInt32(row["DX Clone  Readonly"]) > 0)
                                    e.Editor.ClientEnabled = false;
                                else if (Convert.ToInt32(row["DX Clone Hidden"]) > 0)
                                    e.Editor.ClientVisible = false;
                            }
                        }
                    }
                }


            }
            else
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.ClientVisible = false;
                    else
                        e.Editor.ClientVisible = true;
                }
            }



        }

        protected void menu7_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport7.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                DynamicUtils.ExportPdf(gridExport7);
            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtils.GetDynamicWebPageObjects(OBJ7_pageId, OBJ7_fieldId, _obj.Comp, _obj.UserName).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid7, rows, _obj.UserName.ToString(), _obj.Comp.ToString());

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }
        #endregion

        #region Grid8

        protected void grid8_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView xgrid = (ASPxGridView)sender;
            string _Session = "OPL" + ViewState["RndVal"].ToString() + "15Grid8Rows";

            // cascading dinamik çalışması devam edecek.
            //DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Casc Target]<>''");
            //foreach (DataRow row in rows)
            //{
            //    string cascadeobj = row["Dx Field ID"].ToString();
            //    string[] cascadedobjs = row["Dx Casc Target"].ToString().Split(',');
            //}
            if (xgrid.IsNewRowEditing)
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.ClientVisible = false;
                    else
                        e.Editor.ClientVisible = true;
                }

                if (Session[xgrid.ID + "Cloned"].ToString() == "1")
                {
                    DataRow[] clonedRows = ((DataTable)Session[_Session]).Select("[DX Clone]>0");
                    if (clonedRows.Length > 0)
                    {
                        foreach (DataRow row in clonedRows)
                        {
                            if (e.Column.FieldName == row["DX Field ID"].ToString())
                            {
                                if (Convert.ToInt32(row["DX Clone  Readonly"]) > 0)
                                    e.Editor.ClientEnabled = false;
                                else if (Convert.ToInt32(row["DX Clone Hidden"]) > 0)
                                    e.Editor.ClientVisible = false;
                            }
                        }
                    }
                }


            }
            else
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.ClientVisible = false;
                    else
                        e.Editor.ClientVisible = true;
                }
            }



        }

        protected void menu8_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport8.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                DynamicUtils.ExportPdf(gridExport8);
            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtils.GetDynamicWebPageObjects(OBJ8_pageId, OBJ8_fieldId, _obj.Comp, _obj.UserName).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid8, rows, _obj.UserName.ToString(), _obj.Comp.ToString());

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }
        #endregion

        #region Grid9

        protected void grid9_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView xgrid = (ASPxGridView)sender;
            string _Session = "OPL" + ViewState["RndVal"].ToString() + "15Grid9Rows";

            // cascading dinamik çalışması devam edecek.
            //DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Casc Target]<>''");
            //foreach (DataRow row in rows)
            //{
            //    string cascadeobj = row["Dx Field ID"].ToString();
            //    string[] cascadedobjs = row["Dx Casc Target"].ToString().Split(',');
            //}
            if (xgrid.IsNewRowEditing)
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.ClientVisible = false;
                    else
                        e.Editor.ClientVisible = true;
                }

                if (Session[xgrid.ID + "Cloned"].ToString() == "1")
                {
                    DataRow[] clonedRows = ((DataTable)Session[_Session]).Select("[DX Clone]>0");
                    if (clonedRows.Length > 0)
                    {
                        foreach (DataRow row in clonedRows)
                        {
                            if (e.Column.FieldName == row["DX Field ID"].ToString())
                            {
                                if (Convert.ToInt32(row["DX Clone  Readonly"]) > 0)
                                    e.Editor.ClientEnabled = false;
                                else if (Convert.ToInt32(row["DX Clone Hidden"]) > 0)
                                    e.Editor.ClientVisible = false;
                            }
                        }
                    }
                }


            }
            else
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.ClientVisible = false;
                    else
                        e.Editor.ClientVisible = true;
                }
            }



        }

        protected void menu9_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport9.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                DynamicUtils.ExportPdf(gridExport9);
            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtils.GetDynamicWebPageObjects(OBJ9_pageId, OBJ9_fieldId, _obj.Comp, _obj.UserName).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid9, rows, _obj.UserName.ToString(), _obj.Comp.ToString());

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }
        #endregion

        protected void grid1_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF1.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtils.GetDynamicWebPageObjects(txtOBJP1.Text, txtOBJF1.Text, _obj.Comp, _obj.UserName).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid, rows, _obj.UserName.ToString(), _obj.Comp.ToString());
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();
            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP1.Text, txtOBJF1.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            string _filterexpression = grid.FilterExpression;
            this.dtgrid1.Table = DynamicUtils.fillMainGrid(txtOBJP1.Text, txtOBJF1.Text, _obj.Comp.ToString(), _obj.UserName.ToString());
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid1.Table.Columns["ID"];
            this.dtgrid1.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid1";
            grid.KeyFieldName = "ID";


            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "15Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";
            grid.FilterExpression = DynamicUtils.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));



            grid.DataBind();

            grid.FilterExpression = _filterexpression;

        }
        protected void grid2_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF2.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtils.GetDynamicWebPageObjects(txtOBJP2.Text, txtOBJF2.Text, _obj.Comp, _obj.UserName).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid, rows, _obj.UserName.ToString(), _obj.Comp.ToString());
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();

            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP2.Text, txtOBJF2.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP3.Value) & !string.IsNullOrEmpty(txtMasterObject2.Text))
            {

                if (!string.IsNullOrEmpty(HiddenP3.Value)) values = HiddenP3.Value;
                if (!string.IsNullOrEmpty(HiddenP4.Value)) values += ";" + HiddenP4.Value;
                if (!string.IsNullOrEmpty(HiddenP5.Value)) values += ";" + HiddenP5.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
                if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            }
            if (string.IsNullOrEmpty(values)) return;

            this.dtgrid2.Table = DynamicUtils.fillSubGrid(txtOBJP2.Text, txtOBJF2.Text, _obj.Comp.ToString(), _obj.UserName.ToString(), txtOBJP1.Text, txtOBJF1.Text, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid2.Table.Columns["ID"];
            this.dtgrid2.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid2";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "15Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";

            DataRow[] FilterEditRows = ((DataTable)Session[_sessionname]).Select("[FILTER EDIT]>0");
            if (FilterEditRows.Length > 0)
            {
                if (txtCatchParamsAndValues.Text.Contains("REDIT"))
                {
                    string[] fields = txtCatchParamsAndValues.Text.Split('|');

                    if (fields.Contains("REDIT;1"))
                        txtSubInsertPer2.Text = "1";
                    else
                        txtSubInsertPer2.Text = "0";
                }

            }
            else
                txtSubInsertPer2.Text = "1";

            grid.FilterExpression = DynamicUtils.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));
            string _filterexpression = grid.FilterExpression;

            grid.DataBind();
            HiddenP3.Value = "";
            HiddenP4.Value = "";
            grid.FilterExpression = _filterexpression;




        }
        protected void grid3_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF3.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtils.GetDynamicWebPageObjects(txtOBJP3.Text, txtOBJF3.Text, _obj.Comp, _obj.UserName).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid, rows, _obj.UserName.ToString(), _obj.Comp.ToString());
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();
            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP3.Text, txtOBJF3.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP3.Value) & !string.IsNullOrEmpty(txtMasterObject3.Text))
            {

                if (!string.IsNullOrEmpty(HiddenP3.Value)) values = HiddenP3.Value;
                if (!string.IsNullOrEmpty(HiddenP4.Value)) values += ";" + HiddenP4.Value;
                if (!string.IsNullOrEmpty(HiddenP5.Value)) values += ";" + HiddenP5.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
                if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            }
            if (string.IsNullOrEmpty(values)) return;

            this.dtgrid3.Table = DynamicUtils.fillSubGrid(txtOBJP3.Text, txtOBJF3.Text, _obj.Comp.ToString(), _obj.UserName.ToString(), txtOBJP1.Text, txtOBJF1.Text, values);

            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid3.Table.Columns["ID"];
            this.dtgrid3.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid3";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "15Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";

            DataRow[] FilterEditRows = ((DataTable)Session[_sessionname]).Select("[FILTER EDIT]>0");
            if (FilterEditRows.Length > 0)
            {
                if (txtCatchParamsAndValues.Text.Contains("REDIT"))
                {
                    string[] fields = txtCatchParamsAndValues.Text.Split('|');

                    if (fields.Contains("REDIT;1"))
                        txtSubInsertPer3.Text = "1";
                    else
                        txtSubInsertPer3.Text = "0";
                }
            }
            else
                txtSubInsertPer3.Text = "1";

            grid.FilterExpression = DynamicUtils.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;
            grid.DataBind();
            HiddenP3.Value = "";
            HiddenP4.Value = "";
            grid.FilterExpression = _filterexpression;


        }
        protected void grid4_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF4.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtils.GetDynamicWebPageObjects(txtOBJP4.Text, txtOBJF4.Text, _obj.Comp, _obj.UserName).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid, rows, _obj.UserName.ToString(), _obj.Comp.ToString());
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();
            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP4.Text, txtOBJF4.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP3.Value) & !string.IsNullOrEmpty(txtMasterObject4.Text))
            {

                if (!string.IsNullOrEmpty(HiddenP3.Value)) values = HiddenP3.Value;
                if (!string.IsNullOrEmpty(HiddenP4.Value)) values += ";" + HiddenP4.Value;
                if (!string.IsNullOrEmpty(HiddenP5.Value)) values += ";" + HiddenP5.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
                if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            }
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid4.Table = DynamicUtils.fillSubGrid(txtOBJP4.Text, txtOBJF4.Text, _obj.Comp.ToString(), _obj.UserName.ToString(), txtOBJP1.Text, txtOBJF1.Text, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid4.Table.Columns["ID"];
            this.dtgrid4.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid4";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "15Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";

            DataRow[] FilterEditRows = ((DataTable)Session[_sessionname]).Select("[FILTER EDIT]>0");
            if (FilterEditRows.Length > 0)
            {
                if (txtCatchParamsAndValues.Text.Contains("REDIT"))
                {
                    string[] fields = txtCatchParamsAndValues.Text.Split('|');

                    if (fields.Contains("REDIT;1"))
                        txtSubInsertPer4.Text = "1";
                    else
                        txtSubInsertPer4.Text = "0";
                }
            }
            else
                txtSubInsertPer4.Text = "1";
            grid.FilterExpression = DynamicUtils.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;

            grid.DataBind();
            HiddenP3.Value = "";
            HiddenP4.Value = "";
            grid.FilterExpression = _filterexpression;


        }
        protected void grid5_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF5.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtils.GetDynamicWebPageObjects(txtOBJP5.Text, txtOBJF5.Text, _obj.Comp, _obj.UserName).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid, rows, _obj.UserName.ToString(), _obj.Comp.ToString());
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();
            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP5.Text, txtOBJF5.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP3.Value) & !string.IsNullOrEmpty(txtMasterObject5.Text))
            {

                if (!string.IsNullOrEmpty(HiddenP3.Value)) values = HiddenP3.Value;
                if (!string.IsNullOrEmpty(HiddenP4.Value)) values += ";" + HiddenP4.Value;
                if (!string.IsNullOrEmpty(HiddenP5.Value)) values += ";" + HiddenP5.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
                if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            }
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid5.Table = DynamicUtils.fillSubGrid(txtOBJP5.Text, txtOBJF5.Text, _obj.Comp.ToString(), _obj.UserName.ToString(), txtOBJP1.Text, txtOBJF1.Text, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid5.Table.Columns["ID"];
            this.dtgrid5.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid5";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "15Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";

            DataRow[] FilterEditRows = ((DataTable)Session[_sessionname]).Select("[FILTER EDIT]>0");
            if (FilterEditRows.Length > 0)
            {
                if (txtCatchParamsAndValues.Text.Contains("REDIT"))
                {
                    string[] fields = txtCatchParamsAndValues.Text.Split('|');

                    if (fields.Contains("REDIT;1"))
                        txtSubInsertPer5.Text = "1";
                    else
                        txtSubInsertPer5.Text = "0";
                }
            }
            else
                txtSubInsertPer5.Text = "1";
            grid.FilterExpression = DynamicUtils.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;

            grid.DataBind();
            HiddenP3.Value = "";
            HiddenP4.Value = "";
            grid.FilterExpression = _filterexpression;



        }
        protected void grid6_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF6.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtils.GetDynamicWebPageObjects(txtOBJP6.Text, txtOBJF6.Text, _obj.Comp, _obj.UserName).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid, rows, _obj.UserName.ToString(), _obj.Comp.ToString());
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();

            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP6.Text, txtOBJF6.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP3.Value) & !string.IsNullOrEmpty(txtMasterObject6.Text))
            {

                if (!string.IsNullOrEmpty(HiddenP3.Value)) values = HiddenP3.Value;
                if (!string.IsNullOrEmpty(HiddenP4.Value)) values += ";" + HiddenP4.Value;
                if (!string.IsNullOrEmpty(HiddenP5.Value)) values += ";" + HiddenP5.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
                if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            }
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid6.Table = DynamicUtils.fillSubGrid(txtOBJP6.Text, txtOBJF6.Text, _obj.Comp.ToString(), _obj.UserName.ToString(), txtOBJP1.Text, txtOBJF1.Text, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid6.Table.Columns["ID"];
            this.dtgrid6.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid6";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "15Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";

            DataRow[] FilterEditRows = ((DataTable)Session[_sessionname]).Select("[FILTER EDIT]>0");
            if (FilterEditRows.Length > 0)
            {
                if (txtCatchParamsAndValues.Text.Contains("REDIT"))
                {
                    string[] fields = txtCatchParamsAndValues.Text.Split('|');

                    if (fields.Contains("REDIT;1"))
                        txtSubInsertPer6.Text = "1";
                    else
                        txtSubInsertPer6.Text = "0";
                }
            }
            else
                txtSubInsertPer6.Text = "1";

            grid.FilterExpression = DynamicUtils.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;

            grid.DataBind();
            HiddenP3.Value = "";
            HiddenP4.Value = "";
            grid.FilterExpression = _filterexpression;


        }
        protected void grid7_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF7.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtils.GetDynamicWebPageObjects(txtOBJP7.Text, txtOBJF7.Text, _obj.Comp, _obj.UserName).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid, rows, _obj.UserName.ToString(), _obj.Comp.ToString());
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();

            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP7.Text, txtOBJF7.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP3.Value) & !string.IsNullOrEmpty(txtMasterObject7.Text))
            {

                if (!string.IsNullOrEmpty(HiddenP3.Value)) values = HiddenP3.Value;
                if (!string.IsNullOrEmpty(HiddenP4.Value)) values += ";" + HiddenP4.Value;
                if (!string.IsNullOrEmpty(HiddenP5.Value)) values += ";" + HiddenP5.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
                if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            }
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid7.Table = DynamicUtils.fillSubGrid(txtOBJP7.Text, txtOBJF7.Text, _obj.Comp.ToString(), _obj.UserName.ToString(), txtOBJP1.Text, txtOBJF1.Text, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid7.Table.Columns["ID"];
            this.dtgrid7.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid7";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "15Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";

            DataRow[] FilterEditRows = ((DataTable)Session[_sessionname]).Select("[FILTER EDIT]>0");
            if (FilterEditRows.Length > 0)
            {
                if (txtCatchParamsAndValues.Text.Contains("REDIT"))
                {
                    string[] fields = txtCatchParamsAndValues.Text.Split('|');

                    if (fields.Contains("REDIT;1"))
                        txtSubInsertPer7.Text = "1";
                    else
                        txtSubInsertPer7.Text = "0";
                }
            }
            else
                txtSubInsertPer7.Text = "1";

            grid.FilterExpression = DynamicUtils.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;

            grid.DataBind();
            HiddenP3.Value = "";
            HiddenP4.Value = "";
            grid.FilterExpression = _filterexpression;


        }
        protected void grid8_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF8.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtils.GetDynamicWebPageObjects(txtOBJP8.Text, txtOBJF8.Text, _obj.Comp, _obj.UserName).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid, rows, _obj.UserName.ToString(), _obj.Comp.ToString());
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();

            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP8.Text, txtOBJF8.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP3.Value) & !string.IsNullOrEmpty(txtMasterObject8.Text))
            {

                if (!string.IsNullOrEmpty(HiddenP3.Value)) values = HiddenP3.Value;
                if (!string.IsNullOrEmpty(HiddenP4.Value)) values += ";" + HiddenP4.Value;
                if (!string.IsNullOrEmpty(HiddenP5.Value)) values += ";" + HiddenP5.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
                if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            }
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid8.Table = DynamicUtils.fillSubGrid(txtOBJP8.Text, txtOBJF8.Text, _obj.Comp.ToString(), _obj.UserName.ToString(), txtOBJP1.Text, txtOBJF1.Text, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid8.Table.Columns["ID"];
            this.dtgrid8.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid8";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "15Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";

            DataRow[] FilterEditRows = ((DataTable)Session[_sessionname]).Select("[FILTER EDIT]>0");
            if (FilterEditRows.Length > 0)
            {
                if (txtCatchParamsAndValues.Text.Contains("REDIT"))
                {
                    string[] fields = txtCatchParamsAndValues.Text.Split('|');

                    if (fields.Contains("REDIT;1"))
                        txtSubInsertPer8.Text = "1";
                    else
                        txtSubInsertPer8.Text = "0";
                }
            }
            else
                txtSubInsertPer8.Text = "1";

            grid.FilterExpression = DynamicUtils.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;

            grid.DataBind();
            HiddenP3.Value = "";
            HiddenP4.Value = "";
            grid.FilterExpression = _filterexpression;


        }
        protected void grid9_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF9.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtils.GetDynamicWebPageObjects(txtOBJP9.Text, txtOBJF9.Text, _obj.Comp, _obj.UserName).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid, rows, _obj.UserName.ToString(), _obj.Comp.ToString());
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();

            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP9.Text, txtOBJF9.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP3.Value) & !string.IsNullOrEmpty(txtMasterObject9.Text))
            {

                if (!string.IsNullOrEmpty(HiddenP3.Value)) values = HiddenP3.Value;
                if (!string.IsNullOrEmpty(HiddenP4.Value)) values += ";" + HiddenP4.Value;
                if (!string.IsNullOrEmpty(HiddenP5.Value)) values += ";" + HiddenP5.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
                if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            }
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid9.Table = DynamicUtils.fillSubGrid(txtOBJP9.Text, txtOBJF9.Text, _obj.Comp.ToString(), _obj.UserName.ToString(), txtOBJP1.Text, txtOBJF1.Text, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid9.Table.Columns["ID"];
            this.dtgrid9.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid9";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "15Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";

            DataRow[] FilterEditRows = ((DataTable)Session[_sessionname]).Select("[FILTER EDIT]>0");
            if (FilterEditRows.Length > 0)
            {
                if (txtCatchParamsAndValues.Text.Contains("REDIT"))
                {
                    string[] fields = txtCatchParamsAndValues.Text.Split('|');

                    if (fields.Contains("REDIT;1"))
                        txtSubInsertPer9.Text = "1";
                    else
                        txtSubInsertPer9.Text = "0";
                }
            }
            else
                txtSubInsertPer9.Text = "1";

            grid.FilterExpression = DynamicUtils.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;

            grid.DataBind();
            HiddenP3.Value = "";
            HiddenP4.Value = "";
            grid.FilterExpression = _filterexpression;


        }


        protected void iframecallbackPanel_Callback(object sender, CallbackEventArgsBase e)
        {
            if (postURL.Contains("?"))
            {
                iframeView.Attributes.Add("Src", postURL + "GId=" + Request.QueryString["GId"] + "&" + postURLParameter + " =" + e.Parameter);
            }
            else
            {
                iframeView.Attributes.Add("Src", postURL + "?GId=" + Request.QueryString["GId"] + "&" + postURLParameter + " =" + e.Parameter);
            }

        }
        protected void btnhat_Click(object sender, EventArgs e)
        {
            throw new Exception(txttask.Text);
        }
        protected void grid_RowValidating(object sender, ASPxDataValidationEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            string sessionname = string.Empty;
            switch (gridI.ID)
            {
                case "grid1":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "15Grid1Rows";
                    break;
                case "grid2":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "15Grid2Rows";
                    break;
                case "grid3":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "15Grid3Rows";
                    break;
                case "grid4":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "15Grid4Rows";
                    break;
                case "grid5":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "15Grid5Rows";
                    break;
                case "grid6":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "15Grid6Rows";
                    break;
                case "grid7":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "15Grid7Rows";
                    break;
                case "grid8":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "15Grid8Rows";
                    break;
                case "grid9":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "15Grid9Rows";
                    break;
                default:
                    break;
            }

            if (e.NewValues["REDIT"] == null)
                e.NewValues["REDIT"] = 0;

            //DataRow[] rows2 = ((DataTable)Session[sessionname]).Select("[DX Type1]='DATA'");
            //foreach (DataRow row in rows2)
            //{
            //    if (e.NewValues[row["DX Field ID"].ToString()] == null)

            //        e.NewValues[row["DX Field ID"].ToString()] = "";
            //}

            DataRow[] rows1 = ((DataTable)Session[sessionname]).Select("[DX Type1]='DECIMAL' OR [DX Type2]='INTEGER'");
            foreach (DataRow row in rows1)
            {
                if (e.NewValues[row["DX Field ID"].ToString()] == null)
                    e.NewValues[row["DX Field ID"].ToString()] = 0;
            }


            if (gridI.ID == "grid2")
            {
                if (ViewState["OBJ2_Field"].ToString() == "J50011" | ViewState["OBJ2_Field"].ToString() == "J50031")
                {
                    if (e.NewValues["R0048"] != null)
                    {
                        string sorgu = "select [Personel Kodu] PERS,[Departman Kodu] DEPT,[Bolge Kodu]  BLG from [0C_50029_00_HAT_ARAC_KAYIT] where COMPANY='" + _obj.Comp.ToString() + "' and [Arac Boyutu]='" + e.NewValues["R0048"].ToString() + "'";
                        DataTable dt = f.datatablegonder(sorgu);
                        e.NewValues["R0040"] = dt.Rows[0]["DEPT"].ToString();
                        e.NewValues["R0042"] = dt.Rows[0]["BLG"].ToString();
                        e.NewValues["R0046"] = dt.Rows[0]["PERS"].ToString();

                    }
                }
                else
                {
                    if (e.NewValues["R0040"] != null)
                    {
                        string sorgu = "select [Personel Kodu] PERS,[Departman Kodu] DEPT,[Bolge Kodu]  BLG from [0C_50029_00_HAT_ARAC_KAYIT] where COMPANY='" + _obj.Comp.ToString() + "' and [Arac Boyutu]='" + e.NewValues["R0040"].ToString() + "'";
                        DataTable dt = f.datatablegonder(sorgu);
                        e.NewValues["R0043"] = dt.Rows[0]["DEPT"].ToString();
                        e.NewValues["R0042"] = dt.Rows[0]["BLG"].ToString();
                        e.NewValues["R0038"] = dt.Rows[0]["PERS"].ToString();

                    }
                }


            }



        }
        protected void grid_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
        {
            Session["TabIndex"] = "1";
            ASPxGridView grid = (ASPxGridView)sender;
            GridViewCommandColumn col = grid.Columns["cmnd"] as GridViewCommandColumn;
            string perm = "0";
            switch (grid.ID)
            {
                case "grid2":
                    perm = txtSubInsertPer2.Text;
                    break;
                case "grid3":
                    perm = txtSubInsertPer3.Text;
                    break;
                case "grid4":
                    perm = txtSubInsertPer4.Text;
                    break;
                case "grid5":
                    perm = txtSubInsertPer5.Text;
                    break;
                case "grid6":
                    perm = txtSubInsertPer6.Text;
                    break;
                case "grid7":
                    perm = txtSubInsertPer7.Text;
                    break;
                case "grid8":
                    perm = txtSubInsertPer8.Text;
                    break;
                case "grid9":
                    perm = txtSubInsertPer9.Text;
                    break;

            }

            if (perm == "0")
            {
                try
                {
                    if (col.ShowNewButtonInHeader)
                        col.ShowNewButtonInHeader = false;
                }
                catch { }
                try
                {
                    foreach (GridViewCommandColumnCustomButton item in col.CustomButtons)
                    {
                        if (item.ID == "Clone") col.CustomButtons.Remove(item);

                    }
                }
                catch { }
            }

        }
        string AddValues(string _val1, string _val2)
        {
            if (string.IsNullOrEmpty(_val1))
                _val1 = _val2;
            else
                _val1 += "," + _val2;
            return _val1;
        }
        protected void Grid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            ASPxGridView Igrid = sender as ASPxGridView;
            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "15Grid" + Igrid.ID.Substring(Igrid.ID.Length - 1) + "Rows";

            DataRow[] rows = ((DataTable)Session[_sessionname]).Select("[DX MouseOver Text]<>'' and [DX Field ID]='" + e.DataColumn.FieldName + "'");
            if (rows.Length > 0)
            {
                try
                {
                    string _val = Igrid.GetRowValues(e.VisibleIndex, rows[0]["DX MouseOver Text"].ToString()).ToString();
                    e.Cell.ToolTip = _val;
                }
                catch { e.Cell.ToolTip = string.Format("{0}", e.CellValue); }
            }
            else
                e.Cell.ToolTip = string.Format("{0}", e.CellValue);

            try
            {
                if (e.DataColumn is GridViewDataHyperLinkColumn)
                {
                    if (string.IsNullOrEmpty(e.CellValue.ToString()))
                        e.Cell.Controls[0].Visible = false;

                }
            }
            catch
            {

            }


        }
        private void DilGetir()
        {
            txtDHT1.Text = l.dilgetirfordynamically(_obj.SelectedLanguage,_obj.Comp,_obj.UserName.ToString(), OBJ1_pageId + "_" + OBJ1_fieldId);
            rpMain.HeaderText = txtDHT1.Text;

            txtDHT2.Text = l.dilgetirfordynamically(_obj.SelectedLanguage,_obj.Comp,_obj.UserName.ToString(), OBJ2_pageId + "_" + OBJ2_fieldId);
            rpCargoDetails.HeaderText = txtDHT2.Text;

            txtDHT3.Text = l.dilgetirfordynamically(_obj.SelectedLanguage,_obj.Comp,_obj.UserName.ToString(), OBJ3_pageId + "_" + OBJ3_fieldId);
            rpCostSales.HeaderText = txtDHT3.Text;

            txtDHT4.Text = l.dilgetirfordynamically(_obj.SelectedLanguage,_obj.Comp,_obj.UserName.ToString(), OBJ4_pageId + "_" + OBJ4_fieldId);
            rpDocuments.HeaderText = txtDHT4.Text;

            txtDHT5.Text = l.dilgetirfordynamically(_obj.SelectedLanguage,_obj.Comp,_obj.UserName.ToString(), OBJ5_pageId + "_" + OBJ5_fieldId);
            rpComments.HeaderText = txtDHT5.Text;

            txtDHT6.Text = l.dilgetirfordynamically(_obj.SelectedLanguage,_obj.Comp,_obj.UserName.ToString(), OBJ6_pageId + "_" + OBJ6_fieldId);
            rpTasks.HeaderText = txtDHT6.Text;

            txtDHT7.Text = l.dilgetirfordynamically(_obj.SelectedLanguage,_obj.Comp,_obj.UserName.ToString(), OBJ7_pageId + "_" + OBJ7_fieldId);
            rp7.HeaderText = txtDHT7.Text;

            txtDHT8.Text = l.dilgetirfordynamically(_obj.SelectedLanguage,_obj.Comp,_obj.UserName.ToString(), OBJ8_pageId + "_" + OBJ8_fieldId);
            rp8.HeaderText = txtDHT8.Text;

            txtDHT9.Text = l.dilgetirfordynamically(_obj.SelectedLanguage,_obj.Comp,_obj.UserName.ToString(), OBJ9_pageId + "_" + OBJ9_fieldId);
            rp9.HeaderText = txtDHT9.Text;
        }
        protected void grid_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
           
            try
            {

                if (e.RowType == GridViewRowType.Data)
                {

                    ASPxGridView gridI = sender as ASPxGridView;
                    object rfont = e.GetValue("RFONT")
                                , rback = e.GetValue("RBACK")
                                , rftsz = e.GetValue("RFTSZ")
                                , rftwt = e.GetValue("RFTWT")
                                , rftst = e.GetValue("RFTST");

                    if (rfont == null) return;




                    try
                    {
                        for (int columnIndex = 0; columnIndex < e.Row.Cells.Count; columnIndex++)
                        {
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.Color, rfont.ToString()); } catch { }
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.BackgroundColor, rback.ToString()); } catch { }
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.FontSize, rftsz.ToString() + "px"); } catch { }
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.FontWeight, Convert.ToInt32(rftwt.ToString()) == 0 ? "normal" : "bold"); } catch { }
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.FontStyle, Convert.ToInt32(rftst.ToString()) == 0 ? "normal" : "italic"); } catch { }
                        }

                    }
                    catch { }



                }
            }
            catch { }
        }
        protected void rblDynamicList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string _url = "";
            _url = rblDynamicList.SelectedItem.Value.ToString();
            if (_url.Contains("?"))
                Response.Redirect(_url + "&GId=" + lblGId.Text);
            else
                Response.Redirect(_url + "?GId=" + lblGId.Text);
        }

        protected void callbackPanel1_Callback(object source, CallbackEventArgs e)
        {

            string[] split = txtCatchParamsAndValues.Text.Split('|');
            string _101 = "", _133 = "";
            for (int i = 0; i < split.Length; i++)
            {
                if (split[i].Contains("R0101"))
                    _101 = split[i].Split(';')[1].ToString();

                if (split[i].Contains("R0133"))
                    _133 = split[i].Split(';')[1].ToString();
            }

            string sorgu = "SELECT CONVERT(decimal(10,2),REPLACE([Unit Price],'NULL','0')) as [Unit Price]  FROM [GAC_NAV2].[dbo].[0C_07002_00_SALES_PRICE_LIST] where  COMPANY='LAMKARA' and [Item No_]='K" + e.Parameter.Replace("|", "") + "' and [Sales Code]='" + _101 + "' and [Currency Code]='" + _133 + "'  ";

            e.Result = f.tekbirsonucdonder(sorgu);


        }
        void WriteLog(string ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex);
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            string path = HttpContext.Current.Server.MapPath("~/App_Data/Log.txt");
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(path, true))
            {
                writer.WriteLine(message);
                writer.Close();
            }
        }
    }
}