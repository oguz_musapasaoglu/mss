﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="kontakCard.aspx.cs" Inherits="MSS1.CRM.kontakCard" %>

<%@ Register Src="~/controls/DataTableControlGAC.ascx" TagName="StDataTable" TagPrefix="STDT" %>

<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../css/GAC_MenuStyle.css?v=V1" rel="stylesheet" />
    <link href="../css/GAC_NewStyle.css?v=V1" rel="stylesheet" />
    <link href="../css/GAC_CardStyle.css?v=V1" rel="stylesheet" />
    <style>
        .dxflGroup  
        {
            padding: 0px 0px;
            width: 100%; 
        }
        .dxflGroup tr:first-child > .dxflGroupCell > .dxflGroupBox, .dxflGroup > .dxflChildInFirstRowSys > .dxflGroupCell > .dxflGroupBox 
        {
                margin-top: -5px;
                margin-bottom:0px;
        }
        .dx-vam {
            color: white;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%">
            <table style="width: 100%; border-width: 0px;">
                <tr>
                    <td style="width: 35%;vertical-align:top">
                        <dx:ASPxFormLayout ID="Box1" runat="server" Width="100%" >
                            <Items>
                                <dx:LayoutGroup ShowCaption="False">
                                    <Items>
                                          <dx:LayoutItem FieldName="adisoyadi" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None"   Border-BorderWidth="1" >
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtAdisoyadi" runat="server" Width="100%" Height="100%"
                                                        CssClass="carditemStyle"></dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="sirket" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None"   Border-BorderWidth="1" >
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                     <dx:ASPxTextBox ID="txtSirket" runat="server" Width="100%" Height="100%"
                                                        CssClass="carditemStyle"></dx:ASPxTextBox> 
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="bolum" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None"   Border-BorderWidth="1" >
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                        <asp:TextBox ID="txtbolumu" runat="server"  Width="50%" Height="100%" CssClass="carditemStyle"></asp:TextBox> 
                                                </dx:LayoutItemNestedControlContainer> 
                                            </LayoutItemNestedControlCollection> 
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="unvan" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None"   Border-BorderWidth="1" >
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                       <asp:TextBox ID="txtUnvan" runat="server"  Width="50%" Height="100%"  CssClass="carditemStyle"></asp:TextBox> 
                                                 </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                          <dx:LayoutItem FieldName="DogumTarihi" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None"   Border-BorderWidth="1" >
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>  
                                                    <dx:ASPxDateEdit ID="txtDogumtarihi" runat="server" Width="50%"  CssClass="carditemStyle"  ></dx:ASPxDateEdit> 
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                          <dx:LayoutItem FieldName="medeniDurum" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None"   Border-BorderWidth="1" >
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                      <asp:TextBox ID="txtmedeniDurum" runat="server"  Width="50%" Height="100%"  CssClass="carditemStyle"></asp:TextBox> 
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem> 
                                    </Items> 
                                    <GroupBoxStyle CssClass="cardBox"></GroupBoxStyle> 
                                    <ParentContainerStyle Paddings-Padding="0" ></ParentContainerStyle>
                                </dx:LayoutGroup>
                            </Items>
                        </dx:ASPxFormLayout>
                    </td>
                    <td style="width: 35%;vertical-align:top">
                        <dx:ASPxFormLayout ID="Box2" runat="server" Width="100%">
                            <Items>
                                <dx:LayoutGroup ShowCaption="False" >
                                    <Items>
                                        <dx:LayoutItem FieldName="telefon" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None"   Border-BorderWidth="1" >
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                     <asp:TextBox ID="txtTelefon" runat="server"  Width="50%" Height="100%"  CssClass="carditemStyle"></asp:TextBox> 
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                          <dx:LayoutItem FieldName="CepTel" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None"   Border-BorderWidth="1" >
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                  <asp:TextBox ID="txtCepTelefon" runat="server"  Width="50%" Height="100%"  CssClass="carditemStyle"></asp:TextBox> 
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                          <dx:LayoutItem FieldName="CepTel2" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None"   Border-BorderWidth="1" >
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                      <asp:TextBox ID="txtCepTel2" runat="server"  Width="50%" Height="100%"  CssClass="carditemStyle"></asp:TextBox> 
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                         <dx:LayoutItem FieldName="mail" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None"   Border-BorderWidth="1" >
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                     <asp:TextBox ID="txtMail" runat="server"  Width="100%" Height="100%"  CssClass="carditemStyle"></asp:TextBox> 
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                         <dx:LayoutItem FieldName="mail2" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None"   Border-BorderWidth="1" >
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                       <asp:TextBox ID="txtMail2" runat="server"  Width="50%" Height="100%"  CssClass="carditemStyle"></asp:TextBox> 
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                         <dx:LayoutItem FieldName="fax" ShowCaption="False">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem> 
                                    </Items>
                                    <GroupBoxStyle CssClass="cardBox"  ></GroupBoxStyle>  
                                    <ParentContainerStyle Paddings-Padding="0" ></ParentContainerStyle>
                                </dx:LayoutGroup>
                            </Items>
                        </dx:ASPxFormLayout>
                    </td>
                   <td style="width: 30%;vertical-align:top" >
                        <dx:ASPxFormLayout ID="Box3" runat="server" Width="100%"  >
                            <Items>
                                <dx:LayoutGroup ShowCaption="False" Name="firmaTanitimi"  Height="150px" >
                                    <Items>
                                        <dx:LayoutItem FieldName="firmaTanitimi"  ShowCaption="False" CaptionSettings-Location="Top">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <asp:TextBox ID="txtFirmaTanitimi" runat="server" Width="100%" Height="130px" MaxLength="1000"
                                                        TextMode="MultiLine" CssClass="carditemStyle"></asp:TextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        
                                    </Items>
                                    
                                    <GroupBoxStyle CssClass="cardBox" ></GroupBoxStyle>
                                    <ParentContainerStyle Paddings-Padding="0" ></ParentContainerStyle>
                                </dx:LayoutGroup>
                            </Items>
                        </dx:ASPxFormLayout>
                    </td>
                    
                </tr>
                <tr>
                    <td style="width: 35%"></td>
                    <td style="width: 35%"></td>
                    <td style="width: 30%">
                        <dx:ASPxButton ID="btnKaydet" runat="server" CssClass="formglobalbuttonStyleCRM" OnClick="btnKaydet_Click" Visible="false"></dx:ASPxButton>
                        <dx:ASPxButton ID="btnMusteriyeGit" runat="server" CssClass="formglobalbuttonStyleCRM" OnClick="btnMusteriyeGit_Click"></dx:ASPxButton>
                        <dx:ASPxButton ID="btnFirmayaGit" runat="server" CssClass="formglobalbuttonStyleCRM" OnClick="btnFirmayaGit_Click"></dx:ASPxButton>
                    </td>
                </tr>
            </table> 
        </div>
        
        <stdt:stdatatable id="dtSehir" runat="server" />
        <stdt:stdatatable id="dtMusteriGrubu" runat="server" />
        <stdt:stdatatable id="dtOdemeVadesi" runat="server" />
        <stdt:stdatatable id="dtMusteriTemsilcisi" runat="server" />
        <stdt:stdatatable id="dtVD" runat="server" />
        <stdt:stdatatable id="dtSektor" runat="server" />

    </form>
</body>
</html>

