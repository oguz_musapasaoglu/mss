﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.WSGeneric;
using System.ServiceModel;
using System.Data.SqlClient;
using System.Configuration;
using MSS1.Codes;

namespace MSS1.CRM
{
    public partial class musteriCard : Bases
    {
        fonksiyonlar f = new fonksiyonlar();
        Language l = new Language();
        Methods method = new Methods();
        datasourceHandler dsh = new datasourceHandler();
        webObjectHandler woh = new webObjectHandler();
        string ConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        SessionBase _obj;

        private bool currentCustOnOnayStatu
        {
            get
            {

                return f.GetMusteriOnOnayKontrol(currentCardUniqueID, _obj.Comp);
            }
        }

        private bool currentCustBlokestatu
        {
            get
            {
                return f.GetMusteriBlockedKontrol(currentCardUniqueID, _obj.Comp);
            }
        }

        private bool _DepoMusteriKaydiVarmi
        {
            get
            {
                return f.GETDepoMusteriKaydi(currentCardUniqueID, _obj.Comp);
            }
        }
        private string currentCardUniqueID
        {
            get
            {
                string _cardId = string.Empty;
                try
                {
                    _cardId = Request.QueryString["CCUID"].ToString();
                }
                catch (Exception)
                {
                    _cardId = string.Empty;
                }
                return _cardId;
            }
        }

        ws_baglantı ws = new ws_baglantı();
        void dilgetir()
        {

            Box1.FindItemByFieldName("Adi").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "R0011");
            Box1.FindItemByFieldName("ticariUnvan").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "R0012");
            Box1.FindItemByFieldName("adres1").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "R0013");
            Box1.FindItemByFieldName("adres2").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "R0014");
            Box1.FindItemByFieldName("sehir").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "R0015");
            Box1.FindItemByFieldName("ulke").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "R0016");
            Box1.FindItemByFieldName("postaKodu").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "R0017");
            Box1.FindItemByFieldName("musteriGrubu").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "R0018");
            Box1.FindItemByFieldName("sektor").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "R0019");
            Box1.FindItemByFieldName("odemeVadesi").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "R0020");

            Box2.FindItemByFieldName("musteriTemsilcisi").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "R0021");
            Box2.FindItemByFieldName("networkSorumlusu").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "R0022");
            Box2.FindItemByFieldName("mutabakatSorumlusu").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5006_OJ5024", "R0033");
            Box2.FindItemByFieldName("eFatura").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "R0023");
            Box2.FindItemByFieldName("tel").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "R0024");
            Box2.FindItemByFieldName("web").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "R0027");
            Box2.FindItemByFieldName("VD").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "R0028");
            Box2.FindItemByFieldName("fax").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "R0025");
            Box2.FindItemByFieldName("VDNo").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "R0029");
            Box2.FindItemByFieldName("email").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "R0026");
            Box2.FindItemByFieldName("earsivemail").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "R0031");


            Box3.FindItemByFieldName("firmaTanitimi").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "R0030");
            btnKaydet.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "F1001");
            btnMusteriyeGit.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "F1002");
            btndetay.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "F1003");
            btndetayyd.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "F1004");
            btnmusteriextre.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "F1005");
            btnmusteriextreBA.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "F1006");
            btnMutabakat.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "F1007");
            btnDashboard.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "F1008");
            btnDosyaGor.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "F1009");
            btnOnOnay.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "F1011");
            btnBloke.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "F1010");
            btnDepoCariOlustur.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "F1012");
        }
        private void preparepageFields()
        {
            OnayBlokeKontrol();

            dtSehir.Table = dsh.InitComboDataTable("SEHIR", _obj.Comp);

            createPK(dtSehir.Table);
            dtMusteriGrubu.Table = dsh.InitComboDataTable("MUSTERIGRUBU", _obj.Comp);
            createPK(dtMusteriGrubu.Table);
            dtOdemeVadesi.Table = dsh.InitComboDataTable("ODEMEVADESI", _obj.Comp);
            createPK(dtOdemeVadesi.Table);
            dtMusteriTemsilcisi.Table = dsh.InitComboDataTable("MUSTERITEMSILCISI", _obj.Comp);
            createPK(dtMusteriTemsilcisi.Table);
            dtVD.Table = dsh.InitComboDataTable("VD", _obj.Comp);
            createPK(dtVD.Table);
            dtSektor.Table = dsh.InitComboDataTable("SEKTOR", _obj.Comp);
            createPK(dtSektor.Table);
            dtUlke.Table = dsh.InitComboDataTable("ULKE", _obj.Comp);
            createPK(dtUlke.Table);
            dtNetworkSorumlusu.Table = dsh.InitComboDataTable("NETWORKSORUMLUSU", _obj.Comp);
            createPK(dtNetworkSorumlusu.Table);


            dtMutSorumlusu.Table = dsh.InitComboDataTable("EMPLOYEE", _obj.Comp);
            createPK(dtMutSorumlusu.Table);


            cmbSehir.DataBind();
            cmbMusteriGrubu.DataBind();
            cmbmusteriTemsilcisi.DataBind();
            cmbOdemeVadesi.DataBind();
            cmbVD.DataBind();
            cmbSektor.DataBind();
            cmbUlke.DataBind();
            cmbMutSorumlusu.DataBind();
            cmbnetworkSorumlusu.DataBind();
            btnBloke.Enabled = f.YetkiKontrol(_obj.Comp.ToString(), _obj.UserName.ToString(), "musteridegistirmuhasebe");
            btnOnOnay.Enabled = f.YetkiKontrol(_obj.Comp.ToString(), _obj.UserName.ToString(), "musteriolusturononay");
            if (string.IsNullOrEmpty(currentCardUniqueID))
                btnKaydet.Enabled = f.YetkiKontrol(_obj.Comp.ToString(), _obj.UserName.ToString(), "musteriolustur");
            else
                btnKaydet.Enabled = f.YetkiKontrol(_obj.Comp.ToString(), _obj.UserName.ToString(), "musteridegistir");
            btnDepoCariOlustur.Visible = f.YetkiKontrol(_obj.Comp.ToString(), _obj.UserName.ToString(), "musteridegistirmuhasebe");
            btnMutabakat.Visible = true;
            
            if (Request.QueryString["BlokeOk"] != null)
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "msg001"));
            }

            if (currentCardUniqueID != "")
            {
                ButtonInitilaizeForDepoMusteri();
            }
        }

        private void ButtonInitilaizeForDepoMusteri()
        {
            if (currentCardUniqueID.Substring(0, 2) == "CD")
            {
                btnDepoCariOlustur.Visible = false;
                btnKaydet.Visible = false;
                btnMusteriyeGit.Visible = false;
                btnDosyaGor.Visible = false;
                btndetay.Visible = true;
                btndetayyd.Visible = true;
                btnmusteriextre.Visible = true;
                btnmusteriextreBA.Visible = true;
                btnMutabakat.Visible = true;
                btnDashboard.Visible = false;
                btnOnOnay.Visible = false;

            }
            else
            {
                btnDashboard.Visible = false;
            }

            if (_DepoMusteriKaydiVarmi)
                btnDepoCariOlustur.Visible = false;
        }

        private void OnayBlokeKontrol()
        {
            if (currentCustOnOnayStatu)
            {
                if (currentCustBlokestatu)
                {
                    btnKaydet.Visible = true;
                    btnMusteriyeGit.Visible = true;
                    btnDosyaGor.Visible = true;
                    btndetay.Visible = true;
                    btndetayyd.Visible = true;
                    //btnmusteriextre.Visible = true;
                    //btnmusteriextreBA.Visible = true;
                    btnMutabakat.Visible = true;
                    btnDashboard.Visible = true;
                    btnOnOnay.Visible = false;
                    btnBloke.Visible = true;
                }
                else
                {
                    btnKaydet.Visible = true;
                    btnMusteriyeGit.Visible = true;
                    btnDosyaGor.Visible = true;
                    btndetay.Visible = true;
                    btndetayyd.Visible = true;
                    //btnmusteriextre.Visible = true;
                    //btnmusteriextreBA.Visible = true;
                    btnMutabakat.Visible = true;
                    btnDashboard.Visible = true;
                    btnOnOnay.Visible = false;
                    btnBloke.Visible = false;
                }
            }
            else
            {
                btnKaydet.Visible = true;
                btnMusteriyeGit.Visible = true;
                btnDosyaGor.Visible = true;
                btndetay.Visible = false;
                btndetayyd.Visible = false;
                btnmusteriextre.Visible = false;
                btnmusteriextreBA.Visible = false;
                btnMutabakat.Visible = false;
                btnDashboard.Visible = false;
                btnOnOnay.Visible = true;
                btnBloke.Visible = false;
            }

            if (currentCardUniqueID == string.Empty) btnKaydet.Visible = true;
            if (string.IsNullOrEmpty(currentCardUniqueID))
            {
                btnMusteriyeGit.Visible = false;
                btnDosyaGor.Visible = false;
                btnOnOnay.Visible = false;
            }

        }

        private void createPK(DataTable dt)
        {
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = dt.Columns["ID"];
            dt.PrimaryKey = pricols;
        }


        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();


        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsCallback | IsPostBack) { return; }


            preparepageFields();

            preparepageForms();

            dilgetir();


        }

        private void preparepageForms()
        {
            if (currentCardUniqueID != string.Empty)
            {
                using (SqlConnection conn = new SqlConnection(ConnString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {

                        conn.Open();
                        cmd.Parameters.Clear();

                        string cmdQuery = string.Empty;
                        if (currentCardUniqueID.Substring(0, 2) == "CD")
                            cmdQuery = "sp_0WEB_MUSTERI_KARTI_DUZENLE_DEPO";
                        else
                            cmdQuery = "sp_0WEB_MUSTERI_KARTI_DUZENLE";


                        cmd.CommandText = cmdQuery;
                        SqlParameter _param = new SqlParameter("@mustno", currentCardUniqueID);
                        cmd.Parameters.Add(_param);
                        cmd.Parameters.AddWithValue("@comp", _obj.Comp.ToString());
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = conn;
                        SqlDataReader sqr = cmd.ExecuteReader();
                        string sorgu = "";
                        if (sqr.Read())
                        {
                            txtAdi.Text = sqr["ad"].ToString();
                            txtticariUnvan.Text = sqr["unvan"].ToString();
                            txtAdres1.Text = sqr["adres"].ToString();
                            txtAdres2.Text = sqr["adres2"].ToString();
                            txtTel.Text = sqr["tel"].ToString();
                            if (!string.IsNullOrEmpty(sqr["il"].ToString()))
                            {
                                this.cmbSehir.SelectedIndex = this.cmbSehir.Items.IndexOfValue(sqr["il"].ToString());


                                sorgu = "SELECT  Code,Code+' - '+City as name   FROM  [GAC_NAV2].[dbo].[0C_00225_02_POST CODE]  WHERE " +
                                 "  COMPANY='" + _obj.Comp.ToString() + "'  AND  [İl Kodu]='" + sqr["il"].ToString() + "'   Order by name";

                                f.FillDevExCombobox(cmbIlce, sorgu, "name", "Code");
                                cmbIlce.Value = sqr["ilce"].ToString();
                            }

                            if (!string.IsNullOrEmpty(sqr["ulke"].ToString()))
                                this.cmbUlke.SelectedIndex = this.cmbUlke.Items.IndexOfValue(sqr["ulke"].ToString());
                            if (!string.IsNullOrEmpty(sqr["sektor"].ToString()))
                                this.cmbSektor.SelectedIndex = this.cmbSektor.Items.IndexOfValue(sqr["sektor"].ToString());
                            if (!string.IsNullOrEmpty(sqr["satis"].ToString()))
                                this.cmbmusteriTemsilcisi.SelectedIndex = this.cmbmusteriTemsilcisi.Items.IndexOfValue(sqr["satis"].ToString());
                            if (!string.IsNullOrEmpty(sqr["vergidaire"].ToString()))
                                this.cmbVD.SelectedIndex = this.cmbVD.Items.IndexOfValue(sqr["vergidaire"].ToString());
                            if (!string.IsNullOrEmpty(sqr["odemevade"].ToString()))
                                this.cmbOdemeVadesi.SelectedIndex = this.cmbOdemeVadesi.Items.IndexOfValue(sqr["odemevade"].ToString());
                            if (!string.IsNullOrEmpty(sqr["sablon"].ToString()))
                                this.cmbMusteriGrubu.SelectedIndex = this.cmbMusteriGrubu.Items.IndexOfValue(sqr["sablon"].ToString());
                            if (!string.IsNullOrEmpty(sqr["NS"].ToString()))
                                this.cmbnetworkSorumlusu.SelectedIndex = this.cmbnetworkSorumlusu.Items.IndexOfValue(sqr["NS"].ToString());
                            if (!string.IsNullOrEmpty(sqr["MutSor"].ToString()))
                                this.cmbMutSorumlusu.SelectedIndex = this.cmbMutSorumlusu.Items.IndexOfValue(sqr["MutSor"].ToString());
                            txtFax.Text = sqr["faks"].ToString();
                            txtMail.Text = sqr["mail"].ToString();
                            txtWeb.Text = sqr["site"].ToString();
                            txtVDNo.Text = sqr["vergino"].ToString();
                            txtFirmaTanitimi.Text = sqr["Firma Tanıtımı"].ToString();
                            txtEarsivmail.Text= sqr["earsivmail"].ToString();                           
                            cheFatura.Checked = Convert.ToBoolean(method.StringToInt(sqr["efaturami"].ToString()));
                            if (sqr["vergidaire"].ToString() == "998")
                                txtVDNo.Enabled = false;
                            txtCityText.Text = sqr["CityText"].ToString();
                            txtPostCodeText.Text = sqr["PostCodeText"].ToString();
                            if (sqr["ulke"].ToString()=="TR")
                            {
                                cmbSehir.Visible = true;
                                txtCityText.Visible = false;
                                cmbIlce.Visible = true;
                                txtPostCodeText.Visible = false;
                            }
                            else
                            {
                                cmbSehir.Visible = false;
                                txtCityText.Visible = true;
                                cmbIlce.Visible = false;
                                txtPostCodeText.Visible = true;
                            }
                        }
                        conn.Close();



                    }
                }
            }
        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        protected void btnKaydet_Click(object sender, EventArgs e)
        {
            if (currentCardUniqueID == string.Empty)
            {
                MusteriOlustur();
            }
            else
            {
                MusteriDuzenle();
            }
        }

        public void MusteriOlustur()
        {
            try
            {
                string _mgrup = cmbMusteriGrubu.SelectedItem != null ? cmbMusteriGrubu.SelectedItem.Value.ToString() : ""
                    , _mvd = cmbVD.SelectedItem != null ? cmbVD.SelectedItem.Value.ToString() : "";

                if (_mgrup == "YD MÜŞTERİ" & _mvd != "998")
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "msg002"));
                    return;
                }

                string[] _params = {txtAdi.Text, txtticariUnvan.Text,
                        cmbSehir.SelectedItem != null ? cmbSehir.SelectedItem.Value.ToString() : ""
                        , cmbIlce.SelectedItem != null ? cmbIlce.SelectedItem.Value.ToString() : ""
                        , txtTel.Text, txtFax.Text, txtWeb.Text, txtMail.Text, txtAdres1.Text, txtAdres2.Text
                        , cmbmusteriTemsilcisi.SelectedItem != null ? cmbmusteriTemsilcisi.SelectedItem.Value.ToString() : ""
                        , cmbVD.SelectedItem != null ? cmbVD.SelectedItem.Value.ToString() : ""
                        , txtVDNo.Text
                        , cmbOdemeVadesi.SelectedItem != null ? cmbOdemeVadesi.SelectedItem.Value.ToString() : ""
                        , cmbMusteriGrubu.SelectedItem != null ? cmbMusteriGrubu.SelectedItem.Value.ToString() : ""
                        , _obj.UserName.ToString()
                        , cmbSektor.SelectedItem != null ? cmbSektor.SelectedItem.Value.ToString() : ""
                        , cmbUlke.SelectedItem != null ? cmbUlke.SelectedItem.Value.ToString() : "",
                        "0", "0"
                        , cmbMutSorumlusu.SelectedItem != null ? cmbMutSorumlusu.SelectedItem.Value.ToString() : ""
                        , "", "", "", txtFirmaTanitimi.Text
                        , cmbnetworkSorumlusu.SelectedItem != null ? cmbnetworkSorumlusu.SelectedItem.Value.ToString() : ""
                        ,txtEarsivmail.Text, txtCityText.Text, txtPostCodeText.Text };


                string mustno = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "MusteriOlustur§" + string.Join("§", _params), _obj.Comp);
                string path = "./../OnePageList50.aspx?P=PG5005&P1=" + mustno + "&GId=" + Request.QueryString["GId"];



                if (!ClientScript.IsStartupScriptRegistered("JSMusteriNew"))
                {
                    ClientScript.RegisterStartupScript(GetType(), "JSMusteriNew", @"<script type=""text/javascript""> "
                    + "window.parent.location='" + path + "';</script>");
                }


            }
            catch (Exception ex)
            {
                MessageBox(ex.Message.ToString().Replace("'", " "));
            }
        }

        private void MusteriDuzenle()
        {
            try
            {
                string _mgrup = cmbMusteriGrubu.SelectedItem != null ? cmbMusteriGrubu.SelectedItem.Value.ToString() : ""
                  , _mvd = cmbVD.SelectedItem != null ? cmbVD.SelectedItem.Value.ToString() : "";

                if (_mgrup == "YD MÜŞTERİ" & _mvd != "998")
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "msg002"));
                    return;
                }

                if (!string.IsNullOrEmpty(_mgrup) & _mgrup != "YD MÜŞTERİ" & _mvd == "998")
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "PG5005_OJ5022", "msg003"));
                    return;
                }

                string[] _params = {currentCardUniqueID, txtAdi.Text, txtticariUnvan.Text
                        , cmbmusteriTemsilcisi.SelectedItem != null ? cmbmusteriTemsilcisi.SelectedItem.Value.ToString() : ""
                        , txtAdres1.Text, txtAdres2.Text, txtTel.Text, txtFax.Text, txtWeb.Text, txtMail.Text
                        , cmbSehir.SelectedItem != null ? cmbSehir.SelectedItem.Value.ToString() : ""
                        , cmbIlce.SelectedItem != null ? cmbIlce.SelectedItem.Value.ToString() : ""
                        , cmbVD.SelectedItem != null ? cmbVD.SelectedItem.Value.ToString() : ""
                        , txtVDNo.Text
                         , cmbOdemeVadesi.SelectedItem != null ? cmbOdemeVadesi.SelectedItem.Value.ToString() : ""
                        , cmbMusteriGrubu.SelectedItem != null ? cmbMusteriGrubu.SelectedItem.Value.ToString() : ""
                        , cmbSektor.SelectedItem != null ? cmbSektor.SelectedItem.Value.ToString() : ""
                        , _obj.UserName.ToString()
                         , cmbUlke.SelectedItem != null ? cmbUlke.SelectedItem.Value.ToString() : "", "0", "0"
                        , cmbMutSorumlusu.SelectedItem != null ? cmbMutSorumlusu.SelectedItem.Value.ToString() : ""
                        , "", "", "", txtFirmaTanitimi.Text
                         , cmbnetworkSorumlusu.SelectedItem != null ? cmbnetworkSorumlusu.SelectedItem.Value.ToString() : ""
                         ,txtEarsivmail.Text, txtCityText.Text,txtPostCodeText.Text };


                GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "MusteriDuzenle§" + string.Join("§", _params), _obj.Comp);

                string path = "./../OnePageList50.aspx?P=PG5005&P1=" + currentCardUniqueID + "&GId=" + Request.QueryString["GId"];

                if (!ClientScript.IsStartupScriptRegistered("JSMusteriSave"))
                {
                    ClientScript.RegisterStartupScript(GetType(), "JSMusteriSave", @"<script type=""text/javascript""> "
                    + "window.parent.location='" + path + "';</script>");
                }
            }
            catch (Exception ex)
            {
                MessageBox(ex.Message.ToString().Replace("'", " "));
            }
        }

        protected void btndetay_Click(object sender, EventArgs e)
        {
            //this.ClientScript.RegisterClientScriptBlock(this.GetType(), "popupOpener", "var popup=window.open('../Raporlar/musteridetay.aspx?MusteriNo=" + currentCardUniqueID + "','" + Guid.NewGuid().ToString() + "','scrollbars=no,resizable=no,width=1024,height=600,top=25,left=150');popup.focus();", true);
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "popupOpener", "var popup=window.open('../Raporlar/DynamicReport.aspx?ReportCode=RP100&CariNo=" + currentCardUniqueID + "&GId=" + Request.QueryString["GId"] + "','" + Guid.NewGuid().ToString() + "','scrollbars=no,resizable=no,width=1024,height=600,top=25,left=150');popup.focus();", true);
        }

        protected void btndetayyd_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "popupOpener", "var popup=window.open('../Raporlar/DynamicReport.aspx?ReportCode=RP101&CariNo=" + currentCardUniqueID + "&GId=" + Request.QueryString["GId"] + "','" + Guid.NewGuid().ToString() + "','scrollbars=no,resizable=no,width=1024,height=600,top=25,left=150');popup.focus();", true);
        }
        protected void btnmusteriextre_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "popupOpener", "var popup=window.open('../Raporlar/musteriextre.aspx?MusteriNo=" + currentCardUniqueID + "&GId=" + Request.QueryString["GId"] + "','" + Guid.NewGuid().ToString() + "','scrollbars=no,resizable=no,width=1024,height=600,top=25,left=150');popup.focus();", true);
        }

        protected void btnmusteriextreBA_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "popupOpener", "var popup=window.open('../Raporlar/DynamicReport.aspx?ReportCode=RP102&CariNo=" + currentCardUniqueID + "&GId=" + Request.QueryString["GId"] + "','" + Guid.NewGuid().ToString() + "','scrollbars=no,resizable=no,width=1024,height=600,top=25,left=150');popup.focus();", true);
        }
        protected void btnMutabakat_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "popupOpener", "var popup=window.open('../Reconciliations.aspx?Type=1&CustomerNo=" + currentCardUniqueID + "&GId=" + Request.QueryString["GId"] + "','" + Guid.NewGuid().ToString() + "','scrollbars=no,resizable=no,width=1024,height=600,top=25,left=150');popup.focus();", true);
        }

        protected void btnDashboard_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "popupOpener", "var popup=window.open('../DASHBOARDS/dashboardmusteri.aspx?musteriNo=" + currentCardUniqueID + "&GId=" + Request.QueryString["GId"] + "','" + Guid.NewGuid().ToString() + "','scrollbars=no,resizable=no,width=1024,height=600,top=25,left=150');popup.focus();", true);

        }
        protected void btnDosyaGor_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(this.Page.GetType(), "dsygor", "window.open('../dosyagor.aspx?BelgeNo=" + currentCardUniqueID + "&tur=3&dtur=5&GId=" + Request.QueryString["GId"] + "','" + Guid.NewGuid().ToString() + "','scrollbars=no,resizable=no,width=650,height=460,top=200,left=400');", true);
        }


        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }

        protected void btnMusteriyeGit_Click(object sender, EventArgs e)
        {
            string _result = f.GetMusteriContactNo(currentCardUniqueID, _obj.Comp);

            if (string.IsNullOrEmpty(_result))
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "mustericard", "msg001"));
                return;
            }

            string _path = "/OnePageList50.aspx?P=PG5004&P1=" + _result + "&GId=" + Request.QueryString["GId"];

            if (!ClientScript.IsStartupScriptRegistered("JSGoToCust"))
            {
                ClientScript.RegisterStartupScript(GetType(), "JSGoToCust", @"<script type=""text/javascript""> "
                + "window.open('" + _path + "','_blank');</script>");
            }
        }

        protected void btnBloke_Click(object sender, EventArgs e)
        {
            string[] _params = { currentCardUniqueID, _obj.UserName.ToString() };

            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "MusteriBlokeKaldır§" + string.Join("§", _params), _obj.Comp);
            Response.Redirect(Request.RawUrl + "&BlokeOk=1");
        }

        protected void btnOnOnay_Click(object sender, EventArgs e)
        {
            string[] _params = { currentCardUniqueID, _obj.UserName.ToString() };

            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "MusteriOnayla§" + string.Join("§", _params), _obj.Comp);
            Response.Redirect(Request.RawUrl);
        }

        protected void cmbIlce_Callback(object sender, CallbackEventArgsBase e)
        {
            if (string.IsNullOrEmpty(e.Parameter)) return;

            ASPxComboBox cmb = sender as ASPxComboBox;

            string sorgu = "SELECT  Code,Code+' - '+City as name   FROM  [GAC_NAV2].[dbo].[0C_00225_02_POST CODE]  WHERE " +
                "  COMPANY='" + _obj.Comp.ToString() + "'  AND  [İl Kodu]='" + e.Parameter + "'   Order by name";
            f.FillDevExCombobox(cmb, sorgu, "name", "Code");

        }

        protected void btnDepoCariOlustur_Click(object sender, EventArgs e)
        {
            string _result = string.Empty;
            if (currentCardUniqueID != "")
            {
                string[] _params = { currentCardUniqueID };
                GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "MusteriDepoCariOlustur§" + string.Join("§", _params), _obj.Comp);
            }


            string _path = "/OnePageList50.aspx?P=PG5012&P1=" + _result + "&GId=" + Request.QueryString["GId"];

            if (!ClientScript.IsStartupScriptRegistered("JSGoToCust"))
            {
                ClientScript.RegisterStartupScript(GetType(), "JSGoToCust", @"<script type=""text/javascript""> "
                + "window.open('" + _path + "','_blank');</script>");
            }
        }

        protected void cmbUlke_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbUlke.Value.ToString()=="TR")
            {
                cmbSehir.Visible = true;
                txtCityText.Visible = false;
                cmbIlce.Visible = true;
                txtPostCodeText.Visible = false;
            }
            else
            {
                cmbSehir.Visible = false;
                txtCityText.Visible = true;
                cmbIlce.Visible = false;
                txtPostCodeText.Visible = true;
            }
        }
    }
}