﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.WSGeneric;
using System.ServiceModel;
using System.Data.SqlClient;
using System.Configuration;
using MSS1.Codes;

namespace MSS1.CRM
{
    public partial class FirmaCard : Bases
    {
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        datasourceHandler dsh = new datasourceHandler();
        ws_baglantı ws = new ws_baglantı();
        NavServicesContents serviceContent = new NavServicesContents();
        string ConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        SessionBase _obj;
        private string currentCardUniqueID
        {
            get
            {
                string _cardId = string.Empty;
                try
                {
                    _cardId = Request.QueryString["CCUID"].ToString();
                }
                catch (Exception)
                {
                    _cardId = string.Empty;
                }
                return _cardId;
            }
        }

        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        public void dilgetir()
        {
            Language l = new Language();
            //Box1.FindItemByFieldName("Adi").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(),_obj.UserName.ToString(), "FirmaCard", "Adi");
            //Box1.FindItemByFieldName("ticariUnvan").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(),_obj.UserName.ToString(), "FirmaCard", "ticariUnvan");
            //Box1.FindItemByFieldName("adres1").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(),_obj.UserName.ToString(), "FirmaCard", "adres1");
            //Box1.FindItemByFieldName("adres2").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(),_obj.UserName.ToString(), "FirmaCard", "adres2");
            //Box1.FindItemByFieldName("sehir").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(),_obj.UserName.ToString(), "FirmaCard", "sehir");
            //Box1.FindItemByFieldName("ulke").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(),_obj.UserName.ToString(), "FirmaCard", "ulke");
            //Box1.FindItemByFieldName("postaKodu").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(),_obj.UserName.ToString(), "FirmaCard", "postaKodu"); 
            //Box1.FindItemByFieldName("sektor").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(),_obj.UserName.ToString(), "FirmaCard", "sektor");  
            //Box2.FindItemByFieldName("musteriTemsilcisi").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(),_obj.UserName.ToString(), "FirmaCard", "musteriTemsilcisi");
            //Box2.FindItemByFieldName("networkSorumlusu").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(),_obj.UserName.ToString(), "FirmaCard", "networkSorumlusu"); 
            //Box2.FindItemByFieldName("tel").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(),_obj.UserName.ToString(), "FirmaCard", "tel");
            //Box2.FindItemByFieldName("web").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(),_obj.UserName.ToString(), "FirmaCard", "web");
            //Box2.FindItemByFieldName("VD").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(),_obj.UserName.ToString(), "FirmaCard", "VD");
            //Box2.FindItemByFieldName("fax").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(),_obj.UserName.ToString(), "FirmaCard", "fax");
            //Box2.FindItemByFieldName("VDNo").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(),_obj.UserName.ToString(), "FirmaCard", "VDNo");
            //Box2.FindItemByFieldName("email").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(),_obj.UserName.ToString(), "FirmaCard", "email");

            Box1.FindItemByFieldName("Adi").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "PG5004_OJ5023", "R0011");
            Box1.FindItemByFieldName("ticariUnvan").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "PG5004_OJ5023", "R0012");
            Box1.FindItemByFieldName("adres1").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "PG5004_OJ5023", "R0013");
            Box1.FindItemByFieldName("adres2").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "PG5004_OJ5023", "R0014");
            Box1.FindItemByFieldName("sehir").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "PG5004_OJ5023", "R0016");
            Box1.FindItemByFieldName("ulke").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "PG5004_OJ5023", "R0015");
            Box1.FindItemByFieldName("postaKodu").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "PG5004_OJ5023", "R0017");
            //  Box1.FindItemByFieldName("musteriGrubu").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(),_obj.UserName.ToString(), "PG5004_OJ5023", "musteriGrubu");
            Box1.FindItemByFieldName("sektor").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "PG5004_OJ5023", "R0018");
            //    Box1.FindItemByFieldName("odemeVadesi").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(),_obj.UserName.ToString(), "PG5004_OJ5023", "odemeVadesi");

            Box2.FindItemByFieldName("musteriTemsilcisi").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "PG5004_OJ5023", "R0019");
            Box2.FindItemByFieldName("networkSorumlusu").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "PG5004_OJ5023", "R0026");
            Box2.FindItemByFieldName("eFatura").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "PG5004_OJ5023", "eFatura");
            Box2.FindItemByFieldName("tel").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "PG5004_OJ5023", "R0020");
            Box2.FindItemByFieldName("web").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "PG5004_OJ5023", "R0023");
            Box2.FindItemByFieldName("VD").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "PG5004_OJ5023", "R0024");
            Box2.FindItemByFieldName("fax").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "PG5004_OJ5023", "R0021");
            Box2.FindItemByFieldName("VDNo").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "PG5004_OJ5023", "R0025");
            Box2.FindItemByFieldName("email").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "PG5004_OJ5023", "R0022");

            Box3.FindItemByFieldName("firmaTanitimi").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "PG5004_OJ5023", "R0027");
            btnMusteriyeCevir.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "newpage_account_gir", "btnMusteriyeCevir");


            btnKaydet.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "PG5004_OJ5023", "F1001");
            btnMusteriyeGit.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "PG5004_OJ5023", "F1002");
            btnDosyaGor.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "PG5004_OJ5023", "F1003");
        }
        private void preparepageFields()
        {
            dtSehir.Table = dsh.InitComboDataTable("SEHIR", _obj.Comp);
            createPK(dtSehir.Table);

            dtMusteriGrubu.Table = dsh.InitComboDataTable("MUSTERIGRUBU", _obj.Comp);
            createPK(dtMusteriGrubu.Table);

            dtOdemeVadesi.Table = dsh.InitComboDataTable("ODEMEVADESI", _obj.Comp);
            createPK(dtOdemeVadesi.Table);

            dtMusteriTemsilcisi.Table = dsh.InitComboDataTable("MUSTERITEMSILCISI", _obj.Comp);
            createPK(dtMusteriTemsilcisi.Table);

            dtVD.Table = dsh.InitComboDataTable("VD", _obj.Comp);
            createPK(dtVD.Table);

            dtSektor.Table = dsh.InitComboDataTable("SEKTOR", _obj.Comp);
            createPK(dtSektor.Table);

            dtUlke.Table = dsh.InitComboDataTable("ULKE", _obj.Comp);
            createPK(dtUlke.Table);

            dtNetworkSorumlusu.Table = dsh.InitComboDataTable("NETWORKSORUMLUSU", _obj.Comp);
            createPK(dtNetworkSorumlusu.Table);


            cmbSehir.DataBind();
            cmbmusteriTemsilcisi.DataBind();
            cmbVD.DataBind();
            cmbSektor.DataBind();
            cmbUlke.DataBind();
            cmbnetworkSorumlusu.DataBind();
        }

        private void createPK(DataTable dt)
        {
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = dt.Columns["ID"];
            dt.PrimaryKey = pricols;
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();


        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsCallback | IsPostBack) { return; }
            preparepageFields();
            preparepageForms();


            dilgetir();


        }

        private void preparepageForms()
        {
            if (currentCardUniqueID != string.Empty)
            {
                using (SqlConnection conn = new SqlConnection(ConnString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        conn.Open();
                        cmd.Parameters.Clear();
                        cmd.CommandText = "GAC_NAV2.dbo.[sp_0WEB_FIRMA_KARTI_DUZENLE]";
                        SqlParameter _param = new SqlParameter("@adayno", currentCardUniqueID);
                        cmd.Parameters.Add(_param);
                        cmd.Parameters.AddWithValue("@comp", _obj.Comp.ToString());
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = conn;
                        SqlDataReader sqr = cmd.ExecuteReader();
                        string sorgu = "";

                        if (sqr.Read())
                        {
                            txtAdi.Text = sqr["ad"].ToString();
                            txtticariUnvan.Text = sqr["unvan"].ToString();
                            txtAdres1.Text = sqr["adres"].ToString();
                            txtAdres2.Text = sqr["adres2"].ToString();
                            txtTel.Text = sqr["tel"].ToString();
                            cmbSehir.Value = sqr["sehir"].ToString();
                            if (!string.IsNullOrEmpty(sqr["sehir"].ToString()))
                            {
                                //sorgu = "Select Code,Code+' - '+City as name from [Post Code] where Type=1 and [İl Kodu] ='" + sqr["sehir"].ToString() + "' Order by name";
                                sorgu = "SELECT  Code,Code+' - '+City as name   FROM  [GAC_NAV2].[dbo].[0C_00225_02_POST CODE]  WHERE " +
                                "  COMPANY='" + _obj.Comp.ToString() + "'  AND  [İl Kodu]='" + sqr["sehir"].ToString() + "'   Order by name";
                                f.FillDevExCombobox(cmbIlce, sorgu, "name", "Code");
                                cmbIlce.Value = sqr["ilce"].ToString();
                            }
                            cmbUlke.Value = sqr["countryRegionCode"].ToString();
                            cmbSektor.Value = sqr["sektor"].ToString();
                            cmbmusteriTemsilcisi.Value = sqr["satistemsilcisi"].ToString();
                            cmbUlke.Value = sqr["ulke"].ToString();
                            txtFax.Text = sqr["faks"].ToString();
                            txtMail.Text = sqr["mail"].ToString();
                            txtWeb.Text = sqr["website"].ToString();
                            cmbVD.Value = sqr["alankodu"].ToString();
                            if (sqr["alankodu"].ToString() == "999")
                                txtVDNo.Text = sqr["TCKimlikNo"].ToString();
                            else
                                txtVDNo.Text = sqr["vergino"].ToString();
                            txtFirmaTanitimi.Text = sqr["Firma Tanıtımı"].ToString();
                            if (!string.IsNullOrEmpty(sqr["musNo"].ToString()))
                            {
                                btnKaydet.Enabled = false;
                                btnKaydet.Style.Add("cursor","no-drop");
                                btnKaydet.ToolTip = "Bu firmanın müşteri kardı vardır, lütfen işleminizi müşteri kartından yapınız.";
                            }

                            if (sqr["alankodu"].ToString() == "998")
                                txtVDNo.Enabled = false;

                            txtCityText.Text = sqr["CityText"].ToString();
                            txtPostCodeText.Text = sqr["PostCodeText"].ToString();
                            if (sqr["ulke"].ToString() == "TR")
                            {
                                cmbSehir.Visible = true;
                                txtCityText.Visible = false;
                                cmbIlce.Visible = true;
                                txtPostCodeText.Visible = false;
                            }
                            else
                            {
                                cmbSehir.Visible = false;
                                txtCityText.Visible = true;
                                cmbIlce.Visible = false;
                                txtPostCodeText.Visible = true;
                            }

                        }
                        conn.Close();



                    }
                }
                string MusNo = f.tekbirsonucdonder("select [Müşteri No] from [GAC_NAV2].[dbo].[0C_05050_03_CONTACT] where COMPANY = '" + _obj.Comp.ToString() + "' and No_='" + currentCardUniqueID + "'");
                if (string.IsNullOrEmpty(MusNo))
                {
                    btnMusteriyeCevir.Visible = true;
                    btnMusteriyeGit.Visible = false;
                }
                else
                {
                    btnMusteriyeCevir.Visible = false;
                    btnMusteriyeGit.Visible = true;
                }
            }
            else
            {
                btnMusteriyeCevir.Visible = false;
                btnMusteriyeGit.Visible = false;
            }

        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        protected void btnKaydet_Click(object sender, EventArgs e)
        {
            if (currentCardUniqueID == string.Empty)
            {
                try
                {


                    string[] _params = { txtAdi.Text, txtticariUnvan.Text,
                                            cmbmusteriTemsilcisi.SelectedItem != null ? cmbmusteriTemsilcisi.SelectedItem.Value.ToString() : "",
                                             txtAdres1.Text, txtAdres2.Text, txtTel.Text,
                                             txtFax.Text, txtMail.Text, txtWeb.Text,
                                             cmbSehir.SelectedItem != null ? cmbSehir.SelectedItem.Value.ToString() : "",
                                             cmbIlce.SelectedItem != null ? cmbIlce.SelectedItem.Value.ToString() : ""
                                             , _obj.UserName.ToString(),
                                             cmbVD.SelectedItem != null ? cmbVD.SelectedItem.Value.ToString() : "",
                                             txtVDNo.Text, "",
                                             cmbSektor.SelectedItem != null ? cmbSektor.SelectedItem.Value.ToString() : "", txtVDNo.Text,
                                             "", "", "",
                                             txtFirmaTanitimi.Text,
                                               cmbnetworkSorumlusu.SelectedItem != null ? cmbnetworkSorumlusu.SelectedItem.Value.ToString() : "",
                                               cmbUlke.SelectedItem != null ? cmbUlke.SelectedItem.Value.ToString() : "", txtCityText.Text,txtPostCodeText.Text };


                    string AdayN = GetWebFonksiyonlari().ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "AdayOlustur§" + string.Join("§", _params), _obj.Comp);


                    string path = "./../OnePageList50.aspx?P=PG5004&P1=" + AdayN + "&GId=" + Request.QueryString["GId"];

                    if (!ClientScript.IsStartupScriptRegistered("JSNew"))
                    {
                        ClientScript.RegisterStartupScript(GetType(), "JSNew", @"<script type=""text/javascript""> "
                        + "window.parent.location='" + path + "';</script>");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox(ex.Message.ToString().Replace("'", " "));
                }
            }
            else
            {
                try
                {

                    string[] _params = { currentCardUniqueID, txtAdi.Text,
                                         txtticariUnvan.Text,
                                         cmbmusteriTemsilcisi.SelectedItem != null ? cmbmusteriTemsilcisi.SelectedItem.Value.ToString() : "",
                                         txtAdres1.Text, txtAdres2.Text, txtTel.Text,
                                         txtFax.Text, txtMail.Text, txtWeb.Text,
                                         cmbSehir.SelectedItem != null ? cmbSehir.SelectedItem.Value.ToString() : "",
                                         cmbIlce.SelectedItem != null ? cmbIlce.SelectedItem.Value.ToString() : "", _obj.UserName.ToString(),
                                         cmbVD.SelectedItem != null ? cmbVD.SelectedItem.Value.ToString() : "",
                                         txtVDNo.Text,
                                         "", cmbSektor.SelectedItem != null ? cmbSektor.SelectedItem.Value.ToString() : "",
                                         txtVDNo.Text, "", "", "", txtFirmaTanitimi.Text,
                                          cmbnetworkSorumlusu.SelectedItem != null ? cmbnetworkSorumlusu.SelectedItem.Value.ToString() : ""
                                          , cmbUlke.SelectedItem != null ? cmbUlke.SelectedItem.Value.ToString() : "" , txtCityText.Text,txtPostCodeText.Text};

                    string AdayN = GetWebFonksiyonlari().ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "AdayDuzenle§" + string.Join("§", _params), _obj.Comp);

                    if (!ClientScript.IsStartupScriptRegistered("JSSave"))
                    {
                        ClientScript.RegisterStartupScript(GetType(), "JSSave", @"<script type=""text/javascript""> "
                        + "window.parent.location='./../OnePageList50.aspx?P=PG5004&P1=" + currentCardUniqueID + "&GId=" + Request.QueryString["GId"] + "';</script>");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox(ex.Message.ToString().Replace("'", " "));
                }
            }
        }
        static void WriteLog(string ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex);
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            string path = HttpContext.Current.Server.MapPath("~/App_Data/Log.txt");
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(path, true))
            {
                writer.WriteLine(message);
                writer.Close();
            }
        }
        protected void btnDosyaGor_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(this.Page.GetType(), "dsygor", "window.open('../dosyagor.aspx?BelgeNo=" + currentCardUniqueID + "&Tur=26&dtur=26&GId=" + Request.QueryString["GId"] + "','" + Guid.NewGuid().ToString() + "','scrollbars=no,resizable=no,width=650,height=460,top=200,left=400');", true);
        }
        private Web_GenericFunction_PortClient GetWebFonksiyonlari()
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(_obj.Comp.ToString())));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }

        protected void btnMusteriyeGit_Click(object sender, EventArgs e)
        {
            string result = f.GetContactMusteriNo(currentCardUniqueID, _obj.Comp);

            if (!string.IsNullOrEmpty(result))
            {
                string _path = "/OnePageList50.aspx?P=PG5005&P1=" + f.GetContactMusteriNo(currentCardUniqueID, _obj.Comp) + "&GId=" + Request.QueryString["GId"];

                if (!ClientScript.IsStartupScriptRegistered("JSGoToCust"))
                {
                    ClientScript.RegisterStartupScript(GetType(), "JSGoToCust", @"<script type=""text/javascript""> "
                    + "window.open('" + _path + "','_blank');</script>");
                }
            }
            else
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "firmacard", "msg001"));
            }
        }

        protected void btnMusteriyeCevir_Click(object sender, EventArgs e)
        {
            string SehirVal = string.Empty;
            if (cmbSehir.SelectedItem != null)
            {
                if (string.IsNullOrEmpty(cmbSehir.Value.ToString()))
                    SehirVal = string.Empty;
                else
                    SehirVal = cmbSehir.Value.ToString();
            }

            if (f.MusteriyecevirKontrol("select [Müşteriye Çevirme] from [GAC_NAV2].[dbo].[0C_05050_03_CONTACT] where COMPANY = '" + _obj.Comp.ToString() + "' and No_ = '" + currentCardUniqueID + "'") == "0")
            {
                if (txtFirmaTanitimi.Text != string.Empty)
                {
                    if (cmbVD.Text == "YURTDIŞI")
                    {
                        string sorgu = "select Code from [GAC_NAV2].[dbo].[0C_00318_00_TAX AREA] where COMPANY = '" + _obj.Comp.ToString() + "' and Description = '" + cmbVD.Text + "'";
                        string vd = f.tekbirsonucdonder(sorgu);

                        string[] _params = { currentCardUniqueID, "", "", _obj.NameSurName, vd, txtVDNo.Text };


                        string mustno = GetWebFonksiyonlari().ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "AdayMusteriyeCevir§" + string.Join("§", _params), _obj.Comp);

                        //string mustno = GetWebFonksiyonlari().AdayMusteriyeCevir(currentCardUniqueID, "", "", _obj.NameSurName, vd, txtVDNo.Text);
                        if (mustno.Length > 15)
                        {
                            MessageBox(mustno);
                        }
                        else
                        {
                            if (!ClientScript.IsStartupScriptRegistered("JSMusCevir"))
                            {
                                ClientScript.RegisterStartupScript(GetType(), "JSMusteriNew", @"<script type=""text/javascript""> "
                                + "window.parent.location='./../OnePageList50.aspx?P=PG5005&P1=" + mustno + "&GId=" + Request.QueryString["GId"]+"';</script>");
                            }
                            //Response.Redirect("newpage_Musteribilgi.aspx?MusteriNo=" + mustno);
                        }

                    }
                    else
                    {
                        //if (!f.verginokontrol(txtVDNo.Text))
                        //{
                        string sorgu = "select Code from [GAC_NAV2].[dbo].[0C_00318_00_TAX AREA] where COMPANY = '" + _obj.Comp.ToString() + "' and Description = '" + cmbVD.Text + "'";

                        // string sorgu = "select Code from [Tax Area] where Description = '" + cmbVD.Text + "'";
                        string vd = f.tekbirsonucdonder(sorgu);

                        string[] _params = { currentCardUniqueID, "", "", _obj.NameSurName, vd, txtVDNo.Text };

                        string mustno = GetWebFonksiyonlari().ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "AdayMusteriyeCevir§" + string.Join("§", _params), _obj.Comp);
                        //string mustno = GetWebFonksiyonlari().AdayMusteriyeCevir(currentCardUniqueID, "", "", _obj.NameSurName, vd, txtVDNo.Text);


                        if (!ClientScript.IsStartupScriptRegistered("JSMusCevir"))
                        {
                            ClientScript.RegisterStartupScript(GetType(), "JSMusteriNew", @"<script type=""text/javascript""> "
                            + "window.parent.location='./../OnePageList50.aspx?P=PG5005&P1=" + mustno + "&GId=" + Request.QueryString["GId"]+"';</script>");
                        }
                        //}
                        //else
                        //{
                        //    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(),_obj.UserName.ToString(), "newpage_account_gir", "msg083"));
                        //}

                    }

                }
                else
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "firmacard", "msg002"));
                }
            }
            else
            {

                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "newpage_account_gir", "msg085"));
            }
        }

        protected void cmbIlce_Callback(object sender, CallbackEventArgsBase e)
        {
            if (string.IsNullOrEmpty(e.Parameter)) return;

            ASPxComboBox cmb = sender as ASPxComboBox;

            //string sorgu = "Select Code,Code+' - '+City as name from [Post Code] where Type=1 and [İl Kodu] ='" + e.Parameter + "' Order by name";
            string sorgu = "SELECT  Code,Code+' - '+City as name   FROM  [GAC_NAV2].[dbo].[0C_00225_02_POST CODE]  WHERE " +
                "  COMPANY='" + _obj.Comp.ToString() + "'  AND  [İl Kodu]='" + e.Parameter + "'   Order by name";
            f.FillDevExCombobox(cmb, sorgu, "name", "Code");

        }

        protected void cmbUlke_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbUlke.Value.ToString() == "TR")
            {
                cmbSehir.Visible = true;
                txtCityText.Visible = false;
                cmbIlce.Visible = true;
                txtPostCodeText.Visible = false;
            }
            else
            {
                cmbSehir.Visible = false;
                txtCityText.Visible = true;
                cmbIlce.Visible = false;
                txtPostCodeText.Visible = true;
            }
        }
    }
}