﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FirmaCard.aspx.cs" Inherits="MSS1.CRM.FirmaCard" %>
<%@ Register Src="~/controls/DataTableControlGAC.ascx" TagName="StDataTable" TagPrefix="STDT" %>

<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../css/GAC_MenuStyle.css" rel="stylesheet" />
    <link href="../css/GAC_NewStyle.css?v=V4" rel="stylesheet" />
    <link href="../css/GAC_CardStyle.css?v=V3" rel="stylesheet" />
    <style>
        .dxflGroup {
            padding: 0px 0px;
            width: 100%;
        }

            .dxflGroup tr:first-child > .dxflGroupCell > .dxflGroupBox, .dxflGroup > .dxflChildInFirstRowSys > .dxflGroupCell > .dxflGroupBox {
                margin-top: -5px;
                margin-bottom: 0px;
            }

        .dx-vam {
            color: white;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%; padding-top: 0px; margin-top: 0px; padding-bottom: 0px; margin-bottom: 0px">
            <table style="width: 100%; border-width: 0px;">
                <tr>
                    <td style="width: 35%;vertical-align:top">
                        <dx:ASPxFormLayout ID="Box1" runat="server" Width="100%">
                            <Items>
                                <dx:LayoutGroup ShowCaption="False">
                                    <Items>
                                        <dx:LayoutItem FieldName="Adi"
                                            Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtAdi" runat="server" Width="100%" Height="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="ticariUnvan" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer> 
                                                         <asp:TextBox ID="txtticariUnvan" runat="server" Width="100%" Height="100%"  
                                                       CssClass="carditemStyle" onkeyup="this.value = this.value.toUpperCase();"  onpaste="return false" ></asp:TextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="adres1" Height="50px" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <asp:TextBox ID="txtAdres1" runat="server" Width="100%" Height="100%"
                                                        TextMode="MultiLine" CssClass="carditemStyle" onkeyup="this.value = this.value.toUpperCase();"  onpaste="return false"></asp:TextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="adres2" Height="50px" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <asp:TextBox ID="txtAdres2" runat="server" Width="100%" Height="100%"
                                                        TextMode="MultiLine" CssClass="carditemStyle" onkeyup="this.value = this.value.toUpperCase();"  onpaste="return false"></asp:TextBox>

                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                         <dx:LayoutItem FieldName="ulke" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxComboBox ID="cmbUlke" runat="server" Width="50%" CssClass="carditemStyle"
                                                        ValueField="Code" TextField="Text" DataSourceID="dtUlke" OnSelectedIndexChanged="cmbUlke_SelectedIndexChanged" AutoPostBack="true">
                                                    </dx:ASPxComboBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="sehir" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxComboBox ID="cmbSehir" runat="server" Visible="false" Width="50%"
                                                        ValueField="Code" TextField="Text" DataSourceID="dtSehir"
                                                        CssClass="carditemStyle">
                                                        <ClientSideEvents SelectedIndexChanged="function(s,e){cmbIlce.PerformCallback(s.GetValue())}" />
                                                    </dx:ASPxComboBox>
                                                      <dx:ASPxTextBox ID="txtCityText" runat="server" Width="100%" Height="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>                                       
                                        <dx:LayoutItem FieldName="postaKodu" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                     <dx:ASPxComboBox ID="cmbIlce" ClientInstanceName="cmbIlce" Visible="false" runat="server" Width="50%" CssClass="carditemStyle"
                                                        ValueField="Code" TextField="Text" OnCallback="cmbIlce_Callback">
                                                    </dx:ASPxComboBox>
                                                       <dx:ASPxTextBox ID="txtPostCodeText" runat="server" Width="100%" Height="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="sektor" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxComboBox ID="cmbSektor" runat="server" Width="50%" DataSourceID="dtSektor"
                                                        ValueField="Code" TextField="Text" CssClass="carditemStyle">
                                                    </dx:ASPxComboBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>

                                    </Items>
                                    <GroupBoxStyle CssClass="cardBox" Caption-Paddings-Padding="0" Caption-Paddings-PaddingTop="0"></GroupBoxStyle>
                                    <ParentContainerStyle Paddings-PaddingTop="0"></ParentContainerStyle>
                                </dx:LayoutGroup>
                            </Items>
                        </dx:ASPxFormLayout>
                    </td>
                    <td style="width: 35%;vertical-align:top">
                        <dx:ASPxFormLayout ID="Box2" runat="server" Width="100%">
                            <Items>
                                <dx:LayoutGroup ShowCaption="False">
                                    <Items>
                                        <dx:LayoutItem FieldName="musteriTemsilcisi" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxComboBox ID="cmbmusteriTemsilcisi" runat="server" Width="100%"
                                                        ValueField="Code" TextField="Text" DataSourceID="dtMusteriTemsilcisi"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxComboBox>

                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="networkSorumlusu" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxComboBox ID="cmbnetworkSorumlusu" runat="server" Width="100%"
                                                        CssClass="carditemStyle"
                                                        ValueField="Code" TextField="Text" DataSourceID="dtNetworkSorumlusu">
                                                    </dx:ASPxComboBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                     


                                        <dx:LayoutItem FieldName="tel" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtTel" runat="server" Width="50%" CssClass="carditemStyle"></dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>



                                        <dx:LayoutItem FieldName="fax" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtFax" runat="server" Width="50%" CssClass="carditemStyle"></dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="email" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtMail" runat="server" Width="100%" Height="10%" CssClass="carditemStyle"></dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="web" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtWeb" runat="server" Width="100%" Height="10%" CssClass="carditemStyle"></dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="VD" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxComboBox ID="cmbVD" runat="server" Width="100%"
                                                        ValueField="Code" TextField="Text" DataSourceID="dtVD"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxComboBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>

                                        <dx:LayoutItem FieldName="VDNo" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtVDNo" runat="server" Width="50%" CssClass="carditemStyle"></dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                           <dx:LayoutItem FieldName="eFatura" ShowCaption="False" Height="50px">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                    </Items>
                                    <GroupBoxStyle CssClass="cardBox"></GroupBoxStyle>
                                    <ParentContainerStyle Paddings-Padding="0"></ParentContainerStyle>
                                </dx:LayoutGroup>
                            </Items>
                        </dx:ASPxFormLayout>
                    </td>
                    <td style="width: 30%; vertical-align: top">
                        <dx:ASPxFormLayout ID="Box3" runat="server" Width="100%">
                            <Items>
                                <dx:LayoutGroup ShowCaption="False" Name="firmaTanitimi" Height="261px">
                                    <Items>
                                        <dx:LayoutItem FieldName="firmaTanitimi" ShowCaption="True" CaptionSettings-Location="Top">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <asp:TextBox ID="txtFirmaTanitimi" runat="server" Width="100%" Height="210px" MaxLength="1000"
                                                        TextMode="MultiLine" CssClass="carditemStyle"></asp:TextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>

                                    </Items>

                                    <GroupBoxStyle CssClass="cardBox"></GroupBoxStyle>
                                    <ParentContainerStyle Paddings-Padding="0"></ParentContainerStyle>
                                </dx:LayoutGroup>
                            </Items>
                        </dx:ASPxFormLayout>
                    </td>

                </tr>
                <tr>
                    <td style="width: 35%"></td>
                    <td style="width: 35%"></td>
                    <td style="width: 30%">
                        <dx:ASPxButton ID="btnMusteriyeCevir" runat="server" CssClass="formglobalbuttonStyleCRM" Text="MÜŞTERİYE ÇEVİR"
                            OnClick="btnMusteriyeCevir_Click" Visible="False">
                        </dx:ASPxButton>
                        <dx:ASPxButton ID="btnKaydet" runat="server" CssClass="formglobalbuttonStyleCRM" OnClick="btnKaydet_Click"></dx:ASPxButton>
                        <dx:ASPxButton ID="btnMusteriyeGit" runat="server" CssClass="formglobalbuttonStyleCRM" OnClick="btnMusteriyeGit_Click"></dx:ASPxButton>
                        <dx:ASPxButton ID="btnDosyaGor" Visible="false" runat="server" CssClass="formglobalbuttonStyleCRM" OnClick="btnDosyaGor_Click"></dx:ASPxButton>
                    </td>
                </tr>
            </table>
        </div>
        <STDT:stdatatable id="dtSehir" runat="server" />
        <STDT:stdatatable id="dtMusteriGrubu" runat="server" />
        <STDT:stdatatable id="dtOdemeVadesi" runat="server" />
        <STDT:stdatatable id="dtMusteriTemsilcisi" runat="server" />
        <STDT:stdatatable id="dtVD" runat="server" />
        <STDT:stdatatable id="dtSektor" runat="server" />
        <STDT:stdatatable id="dtUlke" runat="server" />
        <STDT:stdatatable id="dtNetworkSorumlusu" runat="server" />

    </form>
</body>
</html>
