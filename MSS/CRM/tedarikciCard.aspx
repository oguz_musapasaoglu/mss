﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="tedarikciCard.aspx.cs" Inherits="MSS1.CRM.tedarikciCard" %>

<%@ Register Src="~/controls/DataTableControlGAC.ascx" TagName="StDataTable" TagPrefix="STDT" %>

<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../css/GAC_MenuStyle.css" rel="stylesheet" />
    <link href="../css/GAC_NewStyle.css?v=V1" rel="stylesheet" />
    <link href="../css/GAC_CardStyle.css?v=V1" rel="stylesheet" />
    <style>
        .dxflGroup {
            padding: 0px 5px;
            width: 100%;
        }

            .dxflGroup tr:first-child > .dxflGroupCell > .dxflGroupBox, .dxflGroup > .dxflChildInFirstRowSys > .dxflGroupCell > .dxflGroupBox {
                margin-top: -5px;
            }

        .dx-vam {
            color: white;
           
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%">
            <table style="width: 100%; border-width: 0px;">
                <tr>
                    <td style="width: 35%">
                        <dx:ASPxFormLayout ID="Box1" runat="server" Width="100%">
                            <Items>
                                <dx:LayoutGroup ShowCaption="False">
                                    <Items>
                                        <dx:LayoutItem FieldName="Adi" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtAdi" runat="server" Width="100%" Height="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="ticariUnvan" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer> 
                                                    <asp:TextBox ID="txtticariUnvan" runat="server" Width="100%" Height="100%"  
                                                       CssClass="carditemStyle" onkeyup="this.value = this.value.toUpperCase();"  onpaste="return false" ></asp:TextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="adres1" Height="50px" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <asp:TextBox ID="txtAdres1" runat="server" Width="100%" Height="100%" onkeyup="this.value = this.value.toUpperCase();"  onpaste="return false"
                                                        TextMode="MultiLine" CssClass="carditemStyle"></asp:TextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="adres2" Height="50px" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <asp:TextBox ID="txtAdres2" runat="server" Width="100%" Height="100%" onkeyup="this.value = this.value.toUpperCase();"  onpaste="return false"
                                                        TextMode="MultiLine" CssClass="carditemStyle"></asp:TextBox>

                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="sehir" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxComboBox ID="cmbSehir" runat="server" Width="50%"
                                                        ValueField="Code" TextField="Text" DataSourceID="dtSehir"
                                                        CssClass="carditemStyle">
                                                        <ClientSideEvents SelectedIndexChanged="function(s,e){cmbIlce.PerformCallback(s.GetValue())}" />
                                                    </dx:ASPxComboBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="ulke" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxComboBox ID="cmbUlke" runat="server" Width="50%" CssClass="carditemStyle" ValueField="Code" TextField="Text" DataSourceID="dtUlke"></dx:ASPxComboBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="postaKodu" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxComboBox ID="cmbIlce" ClientInstanceName="cmbIlce" runat="server" Width="50%" CssClass="carditemStyle"
                                                        ValueField="Code" TextField="Text" OnCallback="cmbIlce_Callback">
                                                    </dx:ASPxComboBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="musteriGrubu" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxComboBox ID="cmbMusteriGrubu" runat="server" Width="50%" DataSourceID="dtMusteriGrubu"
                                                        ValueField="Code" TextField="Text" CssClass="carditemStyle">
                                                    </dx:ASPxComboBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="odemeVadesi" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxComboBox ID="cmbOdemeVadesi" runat="server" Width="50%" CssClass="carditemStyle"
                                                        ValueField="Code" TextField="Text" DataSourceID="dtOdemeVadesi">
                                                    </dx:ASPxComboBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                    </Items>
                                    <GroupBoxStyle CssClass="cardBox"></GroupBoxStyle>
                                    <ParentContainerStyle Paddings-Padding="0"></ParentContainerStyle>
                                </dx:LayoutGroup>
                            </Items>
                        </dx:ASPxFormLayout>
                    </td>
                    <td style="width: 35%">
                        <dx:ASPxFormLayout ID="Box2" runat="server" Width="100%">
                            <Items>
                                <dx:LayoutGroup ShowCaption="False">
                                    <Items>
                                        <dx:LayoutItem FieldName="talepEden" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxComboBox ID="cmbtalepEden" runat="server" Width="100%"
                                                        ValueField="Code" TextField="Text" DataSourceID="dttalepEden"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxComboBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="ilgiliDepartman" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxComboBox ID="cmbilgiliDepartman" runat="server" Width="100%" CssClass="carditemStyle"
                                                        ValueField="Code" TextField="Text" DataSourceID="dtilgiliDepartman">
                                                    </dx:ASPxComboBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="mutabakatSorumlusu" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxComboBox ID="cmbMutSorumlusu" runat="server" Width="100%" CssClass="carditemStyle"
                                                        ValueField="Code" TextField="Text" DataSourceID="dtMutSorumlusu">
                                                    </dx:ASPxComboBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="TEDARİKÇİ TİPİ *" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                       <dx:ASPxComboBox ID="cmbTedarikciTipi" runat="server" Width="100%" CssClass="carditemStyle"
                                                        ValueField="Code" TextField="Text" DataSourceID="dtTedarikciTipi">
                                                    </dx:ASPxComboBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="eFatura" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxCheckBox ID="cheFatura" Enabled="false" runat="server"></dx:ASPxCheckBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="tel" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtTel" runat="server" Width="50%" Height="10%" CssClass="carditemStyle"></dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="fax" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtFax" runat="server" Width="50%" CssClass="carditemStyle"></dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="email" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtMail" runat="server" Width="100%" Height="10%" CssClass="carditemStyle"></dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="web" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtWeb" runat="server" Width="100%" Height="10%" CssClass="carditemStyle"></dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="VD" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxComboBox ID="cmbVD" runat="server" Width="100%"
                                                        ValueField="Code" TextField="Text" DataSourceID="dtVD"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxComboBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>

                                        <dx:LayoutItem FieldName="VDNo" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtVDNo" runat="server" Width="50%" CssClass="carditemStyle"></dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                    </Items>
                                    <GroupBoxStyle CssClass="cardBox"></GroupBoxStyle>
                                    <ParentContainerStyle Paddings-Padding="0"></ParentContainerStyle>
                                </dx:LayoutGroup>
                            </Items>
                        </dx:ASPxFormLayout>
                    </td>
                    <td style="width: 30%; vertical-align: top">
                        <dx:ASPxFormLayout ID="Box3" runat="server" Width="100%" Height="100%">
                            <Items>
                                <dx:LayoutGroup ShowCaption="False" Name="firmaTanitimi" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                    <Items>
                                        <dx:LayoutItem FieldName="acilmaNedeni" ShowCaption="True" CaptionSettings-Location="Top">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <asp:TextBox ID="txtAcilmaNedeni" runat="server" Width="100%" Height="200px" MaxLength="1000"
                                                        TextMode="MultiLine" CssClass="carditemStyle"></asp:TextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="firmaTanitimi" ShowCaption="True" CaptionSettings-Location="Top" Visible="false">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <asp:TextBox ID="txtFirmaTanitimi" runat="server" Width="100%" Height="100px" MaxLength="1000"
                                                        TextMode="MultiLine" CssClass="carditemStyle"></asp:TextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem> 
                                    </Items> 
                                    <GroupBoxStyle CssClass="cardBox"></GroupBoxStyle>
                                    <ParentContainerStyle Paddings-Padding="0"></ParentContainerStyle>
                                </dx:LayoutGroup>
                            </Items>
                        </dx:ASPxFormLayout>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%;" colspan="3">
                        <dx:ASPxButton ID="btnKaydet" runat="server" CssClass="formglobalbuttonStyleCRM" OnClick="btnKaydet_Click"></dx:ASPxButton>
                        <dx:ASPxButton ID="btndetay" runat="server" CssClass="formglobalbuttonStyleCRM" OnClick="btndetay_Click"></dx:ASPxButton>
                        <dx:ASPxButton ID="btnDosyaGor" runat="server" CssClass="formglobalbuttonStyleCRM" OnClick="btnDosyaGor_Click"></dx:ASPxButton>
                        <dx:ASPxButton ID="btntedarikciextre" runat="server" CssClass="formglobalbuttonStyleCRM" OnClick="btntedarikciextre_Click"></dx:ASPxButton>
                        <dx:ASPxButton ID="btnMutabakat" runat="server" CssClass="formglobalbuttonStyleCRM" OnClick="btnMutabakat_Click"></dx:ASPxButton>
                        <dx:ASPxButton ID="btnOnOnay" runat="server" CssClass="formglobalbuttonStyleCRM" OnClick="btnOnOnay_Click"></dx:ASPxButton>
                        <dx:ASPxButton ID="btnBloke" runat="server" CssClass="formglobalbuttonStyleCRM" OnClick="btnBloke_Click"></dx:ASPxButton>
                        <dx:ASPxButton ID="btnDepoCariOlustur" runat="server" CssClass="formglobalbuttonStyleCRM" OnClick="btnDepoCariOlustur_Click"></dx:ASPxButton>
                     </td>
                </tr>
            </table>


        </div>

        <STDT:stdatatable id="dtSehir" runat="server" />
        <STDT:stdatatable id="dtMusteriGrubu" runat="server" />
        <STDT:stdatatable id="dtOdemeVadesi" runat="server" />
        <STDT:stdatatable id="dtMusteriTemsilcisi" runat="server" />
        <STDT:stdatatable id="dtVD" runat="server" />
        <STDT:stdatatable id="dtilgiliDepartman" runat="server" />
        <STDT:stdatatable id="dttalepEden" runat="server" />
        <STDT:stdatatable id="dtMutSorumlusu" runat="server" />
        <STDT:stdatatable id="dtTedarikciTipi" runat="server" />
        <STDT:stdatatable id="dtUlke" runat="server" />

    </form>
</body>
</html>