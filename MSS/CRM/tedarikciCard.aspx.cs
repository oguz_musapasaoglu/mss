﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.WSGeneric;
using System.ServiceModel;
using System.Data.SqlClient;
using System.Configuration;
using MSS1.Codes;

namespace MSS1.CRM
{
    public partial class tedarikciCard : Bases
    {
        fonksiyonlar f = new fonksiyonlar();
        datasourceHandler dsh = new datasourceHandler();
        webObjectHandler woh = new webObjectHandler();
        Methods method = new Methods();
        ws_baglantı ws = new ws_baglantı();
        string ConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        SessionBase _obj;


        private bool currentCustOnOnayStatu
        {
            get
            {
                return f.GetTedarikciOnOnayKontrol(currentCardUniqueID, _obj.Comp);
            }
        }

        private bool currentCustBlokestatu
        {
            get
            {
                return f.GetTedarikciBlockedKontrol(currentCardUniqueID, _obj.Comp);
            }
        }
        private string currentCardUniqueID
        {
            get
            {
                string _cardId = string.Empty;
                try
                {
                    _cardId = Request.QueryString["CCUID"].ToString();
                }
                catch (Exception)
                {
                    _cardId = string.Empty;
                }
                return _cardId;
            }
        }

        private bool _DepoTedarikciKaydiVarmi
        {
            get
            {
                return f.GETDepoTedarikciKaydi(currentCardUniqueID, _obj.Comp);
            }
        }

        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        public void dilgetir()
        {
            Language l = new Language();
            Box1.FindItemByFieldName("Adi").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "R0011");
            Box1.FindItemByFieldName("ticariUnvan").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "R0012");
            Box1.FindItemByFieldName("adres1").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "R0013");
            Box1.FindItemByFieldName("adres2").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "R0014");
            Box1.FindItemByFieldName("sehir").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "R0015");
            Box1.FindItemByFieldName("ulke").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "R0017");
            Box1.FindItemByFieldName("postaKodu").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "R0018");
            Box1.FindItemByFieldName("musteriGrubu").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "R0029");
            Box1.FindItemByFieldName("odemeVadesi").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "R0030");

            Box2.FindItemByFieldName("talepEden").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "R0031");
            Box2.FindItemByFieldName("ilgiliDepartman").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "R0032");
            Box2.FindItemByFieldName("mutabakatSorumlusu").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "R0033");
            Box2.FindItemByFieldName("eFatura").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "R0034");
            Box2.FindItemByFieldName("tel").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "R0022");
            Box2.FindItemByFieldName("VD").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "R0026");
            Box2.FindItemByFieldName("fax").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "R0023");
            Box2.FindItemByFieldName("VDNo").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "R0027");
            Box2.FindItemByFieldName("email").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "R0024");
            Box2.FindItemByFieldName("web").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "R0025");

            Box3.FindItemByFieldName("acilmaNedeni").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "R0035");
            Box3.FindItemByFieldName("firmaTanitimi").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "R0036");

            btnKaydet.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "F1001");
            btndetay.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "F1002");
            btntedarikciextre.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "F1003");
            btnMutabakat.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "F1004");
            btnDosyaGor.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "F1005");
            btnOnOnay.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "F1006");
            btnBloke.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "F1007");
            btnDepoCariOlustur.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "PG5006_OJ5024", "F1008");
        }
        private void preparepageFields()
        {
            dtSehir.Table = dsh.InitComboDataTable("SEHIR", _obj.Comp);
            createPK(dtSehir.Table);

            dtMusteriGrubu.Table = dsh.InitComboDataTable("TEDARIKCIGRUBU", _obj.Comp);
            createPK(dtMusteriGrubu.Table);
            dtOdemeVadesi.Table = dsh.InitComboDataTable("ODEMEVADESI", _obj.Comp);
            createPK(dtOdemeVadesi.Table);
            dtVD.Table = dsh.InitComboDataTable("VD", _obj.Comp);
            createPK(dtVD.Table);

            dtUlke.Table = dsh.InitComboDataTable("ULKE", _obj.Comp);
            createPK(dtUlke.Table);

            dtMutSorumlusu.Table = dsh.InitComboDataTable("EMPLOYEE", _obj.Comp);
            createPK(dtMutSorumlusu.Table);

            dtilgiliDepartman.Table = dsh.InitComboDataTableV1("ILGILIDEPT", _obj.Comp);
            createPK(dtilgiliDepartman.Table);

            dttalepEden.Table = dsh.InitComboDataTableV1("TALEPEDEN", _obj.Comp);
            createPK(dttalepEden.Table);

            dtTedarikciTipi.Table = dsh.InitComboDataTableV1("TEDARIKCITIPI", _obj.Comp);
            createPK(dtTedarikciTipi.Table);

            cmbSehir.DataBind();
            cmbMusteriGrubu.DataBind();
            cmbOdemeVadesi.DataBind();
            cmbVD.DataBind();
            cmbMutSorumlusu.DataBind();
            cmbtalepEden.DataBind();
            cmbilgiliDepartman.DataBind();
            cmbTedarikciTipi.DataBind();
            cmbTedarikciTipi.SelectedIndex = 0;

            OnayBlokeKontrol();
            btnBloke.Enabled = f.YetkiKontrol(_obj.Comp, _obj.UserName, "tedarikcidegistirmuhasebe");
            btnOnOnay.Enabled = f.YetkiKontrol(_obj.Comp, _obj.UserName, "tedarikciolusturononay");
            btnDepoCariOlustur.Visible = f.YetkiKontrol(_obj.Comp, _obj.UserName, "tedarikcidegistirmuhasebe");
            if (string.IsNullOrEmpty(currentCardUniqueID))
                btnKaydet.Enabled = f.YetkiKontrol(_obj.Comp, _obj.UserName, "tedarikciolustur");
            else
                btnKaydet.Enabled = f.YetkiKontrol(_obj.Comp, _obj.UserName, "tedarikcidegistir");
            if (_obj.Comp == "GAC" | _obj.Comp == "LAM" | _obj.Comp == "LAMLOJ" | _obj.Comp == "MLH" | _obj.Comp == "SINOTRANS" | _obj.Comp == "LUCENT" | _obj.Comp == "STENA")
                btnMutabakat.Visible = true;
            else
                btnMutabakat.Visible = false;


            if (currentCardUniqueID != "")
            {
                ButtonInitilaizeForDepoMusteri();
            }

        }

        private void ButtonInitilaizeForDepoMusteri()
        {
            if (currentCardUniqueID.Substring(0, 2) == "VD")
            {
                btnDepoCariOlustur.Visible = false;
                btnKaydet.Visible = false;
                btnDosyaGor.Visible = false;
                btndetay.Visible = true;
                btnMutabakat.Visible = true;
                btnOnOnay.Visible = false;
                btntedarikciextre.Visible = true;
            }


            if (_DepoTedarikciKaydiVarmi)
                btnDepoCariOlustur.Visible = false;
        }

        private void createPK(DataTable dt)
        {
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = dt.Columns["ID"];
            dt.PrimaryKey = pricols;
        }
        protected void Page_Load(object sender, EventArgs e)
        {


            if (IsCallback | IsPostBack) { return; }
            preparepageFields();
            preparepageForms();

            dilgetir();


        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }
        private void OnayBlokeKontrol()
        {


            if (currentCustOnOnayStatu)
            {
                if (!currentCustBlokestatu)
                {
                    btnKaydet.Visible = true;
                    btndetay.Visible = true;
                    btnDosyaGor.Visible = true;
                    //   btntedarikciextre.Visible = true;
                    btnMutabakat.Visible = true;
                    btnOnOnay.Visible = false;
                    btnBloke.Visible = false;
                }
                else
                {
                    btnKaydet.Visible = true;
                    btndetay.Visible = true;
                    btnDosyaGor.Visible = true;
                    // btntedarikciextre.Visible = true;
                    btnMutabakat.Visible = true;
                    btnOnOnay.Visible = false;
                    btnBloke.Visible = true;
                }
            }
            else
            {
                btnKaydet.Visible = true;
                btndetay.Visible = false;
                btnDosyaGor.Visible = false;
                //btntedarikciextre.Visible = false;
                btnMutabakat.Visible = false;
                btnOnOnay.Visible = true;
                btnBloke.Visible = false;
            }

            if (currentCardUniqueID == string.Empty)
            {
                btnKaydet.Visible = true;
                cmbtalepEden.ClientEnabled = true;
                cmbilgiliDepartman.ClientEnabled = true;
                txtAcilmaNedeni.Enabled = true;
                cmbtalepEden.ClientEnabled = true;
                cmbilgiliDepartman.ClientEnabled = true;
            }
            else
            {
                cmbtalepEden.ClientEnabled = false;
                cmbilgiliDepartman.ClientEnabled = false;
                txtAcilmaNedeni.Enabled = false;
            }

            if (currentCardUniqueID == string.Empty)
            {
                btnKaydet.Visible = true;
                btndetay.Visible = false;
                btnDosyaGor.Visible = false;
                btnMutabakat.Visible = false;
                btnOnOnay.Visible = false;
                btnBloke.Visible = false;
                btntedarikciextre.Visible = false;
            }
        }

        private void preparepageForms()
        {
            if (currentCardUniqueID != string.Empty)
            {
                using (SqlConnection conn = new SqlConnection(ConnString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        conn.Open();
                        cmd.Parameters.Clear();


                        string cmdQuery = string.Empty;
                        if (currentCardUniqueID.Substring(0, 2) == "VD")
                            cmdQuery = "sp_0WEB_SATICI_KARTI_DUZENLE_DEPO";
                        else
                            cmdQuery = "sp_0WEB_SATICI_KARTI_DUZENLE";


                        cmd.CommandText = cmdQuery;
                        SqlParameter _param = new SqlParameter("@saticino", currentCardUniqueID);
                        cmd.Parameters.Add(_param);
                        cmd.Parameters.AddWithValue("@comp", _obj.Comp);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = conn;
                        SqlDataReader sqr = cmd.ExecuteReader();
                        string sorgu = "";
                        if (sqr.Read())
                        {
                            txtAdi.Text = sqr["ad"].ToString();
                            txtticariUnvan.Text = sqr["unvan"].ToString();
                            txtAdres1.Text = sqr["adres"].ToString();
                            txtAdres2.Text = sqr["adres2"].ToString();
                            txtTel.Text = sqr["tel"].ToString();
                            cmbSehir.Value = sqr["sehir"].ToString();
                            if (!string.IsNullOrEmpty(sqr["sehir"].ToString()))
                            {
                                sorgu = "SELECT  Code,Code+' - '+City as name   FROM  [GAC_NAV2].[dbo].[0C_00225_02_POST CODE]  WHERE " +
                                "  COMPANY='" + _obj.Comp + "'  AND  [İl Kodu]='" + sqr["sehir"].ToString() + "'   Order by name";
                                //sorgu = "Select Code,Code+' - '+City as name from [Post Code] where Type=1 and [İl Kodu] ='" + sqr["sehir"].ToString() + "' Order by name";
                                f.FillDevExCombobox(cmbIlce, sorgu, "name", "Code");
                                cmbIlce.Value = sqr["ilce"].ToString();
                            }
                            cmbUlke.Value = sqr["ulkekod"].ToString();
                            cmbMusteriGrubu.Value = sqr["sablon"].ToString();
                            cmbOdemeVadesi.Value = sqr["odemevade"].ToString();
                            cmbtalepEden.Value = sqr["TEP"].ToString();
                            cmbilgiliDepartman.Value = sqr["ilgiliDepartman"].ToString();
                            cmbMutSorumlusu.Value = sqr["MutabakatSorumlusu"].ToString();
                            cheFatura.Checked = Convert.ToBoolean(method.StringToInt(sqr["IsEf"].ToString()));
                            txtFax.Text = sqr["faks"].ToString();
                            txtTel.Text = sqr["tel"].ToString();
                            txtMail.Text = sqr["mail"].ToString();
                            txtWeb.Text = sqr["website"].ToString();
                            cmbVD.Value = sqr["vergidairekod"].ToString();
                            txtVDNo.Text = sqr["vergino"].ToString();
                            txtAcilmaNedeni.Text = sqr["acilmaNedeni"].ToString();
                            cmbtalepEden.Value = sqr["TEP"].ToString();
                            cmbilgiliDepartman.Value = sqr["ilgiliDepartman"].ToString();
                            cmbTedarikciTipi.Text = sqr["tedarikcitipi"].ToString();
                            //txtFirmaTanitimi.Text= sqr["FirmaTanitimi"].ToString();
                            if (sqr["vergidairekod"].ToString() == "998")
                                txtVDNo.Enabled = false;
                        }
                        conn.Close();



                    }
                }
            }
        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        protected void btnKaydet_Click(object sender, EventArgs e)
        {
            if (currentCardUniqueID == string.Empty)
            {
                try
                {
                    string[] _params = { txtAdi.Text, txtticariUnvan.Text,
                          cmbSehir.SelectedItem != null ? cmbSehir.SelectedItem.Value.ToString() : "",
                          cmbIlce.SelectedItem != null ? cmbIlce.SelectedItem.Value.ToString() : "",
                        txtTel.Text, txtFax.Text, txtWeb.Text, txtMail.Text, txtAdres1.Text, txtAdres2.Text,
                         cmbVD.SelectedItem != null ? cmbVD.SelectedItem.Value.ToString() : "",
                        txtVDNo.Text,
                         cmbOdemeVadesi.SelectedItem != null ? cmbOdemeVadesi.SelectedItem.Value.ToString() : "",
                       cmbMusteriGrubu.SelectedItem != null ? cmbMusteriGrubu.SelectedItem.Value.ToString() : ""
                       , _obj.UserName,
                        cmbMutSorumlusu.SelectedItem != null ? cmbMutSorumlusu.SelectedItem.Value.ToString() : "",
                        cmbUlke.SelectedItem != null ? cmbUlke.SelectedItem.Value.ToString() : "",
                        cmbtalepEden.SelectedItem != null ? cmbtalepEden.SelectedItem.Value.ToString() : "",
                        cmbilgiliDepartman.SelectedItem != null ? cmbilgiliDepartman.SelectedItem.Value.ToString() : "",
                        txtAcilmaNedeni.Text, cmbTedarikciTipi.SelectedItem.Value.ToString()

                    };

                    string saticino = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SaticiOlustur§" + string.Join("§", _params), _obj.Comp);
                    string path = "./../OnePageList50.aspx?P=PG5006&P1=" + saticino + "&GId=" + Request.QueryString["GId"];

                    if (!ClientScript.IsStartupScriptRegistered("JSNew"))
                    {
                        ClientScript.RegisterStartupScript(GetType(), "JSNew", @"<script type=""text/javascript""> "
                        + "window.parent.location='" + path + "';</script>");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox(ex.Message.ToString().Replace("'", " "));
                }
            }
            else
            {
                try
                {
                    string[] _params = { currentCardUniqueID, txtAdi.Text, txtticariUnvan.Text, txtAdres1.Text, txtAdres2.Text, txtTel.Text, txtFax.Text, txtWeb.Text, txtMail.Text,
                         cmbSehir.SelectedItem != null ? cmbSehir.SelectedItem.Value.ToString() : "",
                        cmbIlce.SelectedItem != null ? cmbIlce.SelectedItem.Value.ToString() : "",
                         cmbVD.SelectedItem != null ? cmbVD.SelectedItem.Value.ToString() : "",
                        txtVDNo.Text,
                        cmbOdemeVadesi.SelectedItem != null ? cmbOdemeVadesi.SelectedItem.Value.ToString() : "",
                        cmbMusteriGrubu.SelectedItem != null ? cmbMusteriGrubu.SelectedItem.Value.ToString() : "",
                        _obj.UserName,
                        cmbMutSorumlusu.SelectedItem != null ? cmbMutSorumlusu.SelectedItem.Value.ToString() : "",
                        cmbUlke.SelectedItem != null ? cmbUlke.SelectedItem.Value.ToString() : "",
                        cmbtalepEden.SelectedItem != null ? cmbtalepEden.SelectedItem.Value.ToString() : "",
                        cmbilgiliDepartman.SelectedItem != null ? cmbilgiliDepartman.SelectedItem.Value.ToString() : "",
                        txtAcilmaNedeni.Text, cmbTedarikciTipi.SelectedItem.Value.ToString()};

                    GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SaticiDuzenle§" + string.Join("§", _params), _obj.Comp);

                    string path = "./../OnePageList50.aspx?P=PG5006&P1=" + currentCardUniqueID + "&GId=" + Request.QueryString["GId"];

                    if (!ClientScript.IsStartupScriptRegistered("JSNew"))
                    {
                        ClientScript.RegisterStartupScript(GetType(), "JSNew", @"<script type=""text/javascript""> "
                        + "window.parent.location='" + path + "';</script>");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox(ex.Message.ToString().Replace("'", " "));
                }
            }
        }



        protected void btndetay_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "popupOpener", "var popup=window.open('../Raporlar/DynamicReport.aspx?ReportCode=RP100&CariNo=" + currentCardUniqueID + "&GId=" + Request.QueryString["GId"] + "','" + Guid.NewGuid().ToString() + "','scrollbars=no,resizable=no,width=1024,height=600,top=25,left=150');popup.focus();", true);

        }

        protected void btntedarikciextre_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "popupOpener", "var popup=window.open('../Raporlar/DynamicReport.aspx?ReportCode=RP103&CariNo=" + currentCardUniqueID + "&GId=" + Request.QueryString["GId"] + "','" + Guid.NewGuid().ToString() + "','scrollbars=no,resizable=no,width=1024,height=600,top=25,left=150');popup.focus();", true);
        }

        protected void btnMutabakat_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "popupOpener", "var popup=window.open('../Reconciliations.aspx?Type=2&CustomerNo=" + currentCardUniqueID + "&GId=" + Request.QueryString["GId"] + "','" + Guid.NewGuid().ToString() + "','scrollbars=no,resizable=no,width=1024,height=600,top=25,left=150');popup.focus();", true);

        }
        protected void btnDosyaGor_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(this.Page.GetType(), "dsygor", "window.open('../dosyagor.aspx?BelgeNo=" + currentCardUniqueID + "&Tur=26&dtur=26&GId=" + Request.QueryString["GId"] + "','" + Guid.NewGuid().ToString() + "','scrollbars=no,resizable=no,width=650,height=460,top=200,left=400');", true);
        }

        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }

        protected void btnBloke_Click(object sender, EventArgs e)
        {

            string[] _params = { currentCardUniqueID, _obj.UserName };

            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SaticiBlokeKaldır§" + string.Join("§", _params), _obj.Comp);

            Response.Redirect(Request.RawUrl);
        }

        protected void btnOnOnay_Click(object sender, EventArgs e)
        {
            string[] _params = { currentCardUniqueID, _obj.UserName };
            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SaticiOnayla§" + string.Join("§", _params), _obj.Comp);

            Response.Redirect(Request.RawUrl);
        }

        protected void cmbIlce_Callback(object sender, CallbackEventArgsBase e)
        {
            if (string.IsNullOrEmpty(e.Parameter)) return;

            ASPxComboBox cmb = sender as ASPxComboBox;

            // string sorgu = "Select Code,Code+' - '+City as name from [Post Code] where Type=1 and [İl Kodu] ='" + e.Parameter + "' Order by name";
            string sorgu = "SELECT  Code,Code+' - '+City as name   FROM  [GAC_NAV2].[dbo].[0C_00225_02_POST CODE]  WHERE " +
             "  COMPANY='" + _obj.Comp + "'  AND  [İl Kodu]='" + e.Parameter + "'   Order by name";
            f.FillDevExCombobox(cmb, sorgu, "name", "Code");

        }

        protected void btnDepoCariOlustur_Click(object sender, EventArgs e)
        {
            string _result = string.Empty;
            if (currentCardUniqueID != "")
            {
                string[] _params = { currentCardUniqueID };
                _result = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "TedarikciDepoCariOlustur§" + string.Join("§", _params), _obj.Comp);
            }



            string _path = "/OnePageList50.aspx?P=PG5013&P1=" + _result + "&GId=" + Request.QueryString["GId"];

            if (!ClientScript.IsStartupScriptRegistered("JSGoToCust"))
            {
                ClientScript.RegisterStartupScript(GetType(), "JSGoToCust", @"<script type=""text/javascript""> "
                + "window.open('" + _path + "','_blank');</script>");
            }
        }
    }
}