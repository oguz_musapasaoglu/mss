﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.WSGeneric;
using System.ServiceModel;
using System.Data.SqlClient;
using System.Configuration;
using MSS1.Codes;

namespace MSS1.CRM
{
    public partial class kontakCard : Bases
    {
        fonksiyonlar f = new fonksiyonlar();
        datasourceHandler dsh = new datasourceHandler();
        webObjectHandler woh = new webObjectHandler();
        ws_baglantı ws = new ws_baglantı();
        string ConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        SessionBase _obj;
        private string currentCardUniqueID
        {
            get
            {
                string _cardId = string.Empty;
                try
                {
                    _cardId = Request.QueryString["CCUID"].ToString();
                }
                catch (Exception)
                {
                    _cardId = string.Empty;
                }
                return _cardId;
            }
        }

       
        public void dilgetir()
        {
            Language l = new Language();
            Box1.FindItemByFieldName("adisoyadi").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "PG5007_OJ5025", "R0011");
            Box1.FindItemByFieldName("sirket").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "PG5007_OJ5025", "R0012");
            Box1.FindItemByFieldName("bolum").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "PG5007_OJ5025", "R0013");
            Box1.FindItemByFieldName("unvan").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "PG5007_OJ5025", "R0014");
            Box1.FindItemByFieldName("DogumTarihi").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "PG5007_OJ5025", "R0015");
            Box1.FindItemByFieldName("medeniDurum").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "PG5007_OJ5025", "R0017");

            Box2.FindItemByFieldName("telefon").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "PG5007_OJ5025", "R0018");
            Box2.FindItemByFieldName("CepTel").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "PG5007_OJ5025", "R0019");
            Box2.FindItemByFieldName("CepTel2").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "PG5007_OJ5025", "R0020");
            Box2.FindItemByFieldName("mail").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "PG5007_OJ5025", "R0021");
            Box2.FindItemByFieldName("mail2").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "PG5007_OJ5025", "R0022");

            btnKaydet.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "PG5007_OJ5025", "F1001");
            btnFirmayaGit.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "PG5007_OJ5025", "F1002");
            btnMusteriyeGit.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "PG5007_OJ5025", "F1003");
        }
        private void preparepageFields()
        {
        }

        private void createPK(DataTable dt)
        {
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = dt.Columns["ID"];
            dt.PrimaryKey = pricols;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsCallback | IsPostBack) { return; }

            preparepageForms();


            dilgetir();
            preparepageFields();
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }

        private void preparepageForms()
        {
            if (currentCardUniqueID != string.Empty)
            {
                using (SqlConnection conn = new SqlConnection(ConnString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        conn.Open();
                        cmd.Parameters.Clear();
                        cmd.CommandText = "[sp_0WEB_ILGILI_KISI_KARTI_DUZENLE]";
                        SqlParameter _param = new SqlParameter("@kisino", currentCardUniqueID);
                        cmd.Parameters.Add(_param);
                        cmd.Parameters.AddWithValue("@comp", _obj.Comp);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = conn;
                        SqlDataReader sqr = cmd.ExecuteReader();

                        if (sqr.Read())
                        {
                            txtAdisoyadi.Text = sqr["ad"].ToString();
                            txtSirket.Text = sqr["sirketadi"].ToString();
                            txtbolumu.Text = sqr["bolumu"].ToString();
                            txtUnvan.Text = sqr["unvan"].ToString();
                            txtDogumtarihi.Date = (DateTime)sqr["dogumTar"];
                            txtmedeniDurum.Text = sqr["MedeniDurum"].ToString();
                            txtTelefon.Text = sqr["tel"].ToString();
                            txtCepTelefon.Text = sqr["ceptel"].ToString();
                            txtCepTel2.Text = sqr["MobilNoIs"].ToString();
                            txtMail.Text = sqr["mail"].ToString();
                            txtMail2.Text = sqr["mail2"].ToString();
                        }
                        conn.Close();



                    }
                }
            }
        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        protected void btnKaydet_Click(object sender, EventArgs e)
        {
            if (currentCardUniqueID == string.Empty)
            {
                string[] _params = { txtAdisoyadi.Text, txtTelefon.Text, txtCepTelefon.Text, txtMail.Text, _obj.UserName, "", txtUnvan.Text, txtbolumu.Text, new Methods().DateToStringNAVType(txtDogumtarihi.Date.ToShortDateString()), txtmedeniDurum.Text, txtCepTel2.Text, txtMail2.Text };

                GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "IlgiliKisiOlustur§" + string.Join("§", _params), _obj.Comp);


            }
            else
            {

                string[] _params = { currentCardUniqueID, txtAdisoyadi.Text, txtTelefon.Text, txtCepTelefon.Text, txtMail.Text, _obj.UserName, "", txtUnvan.Text, txtbolumu.Text, new Methods().DateToStringNAVType(txtDogumtarihi.Date.ToShortDateString()), txtmedeniDurum.Text, txtCepTel2.Text, txtMail2.Text };

                GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "IlgiliKisiDuzenle§" + string.Join("§", _params), _obj.Comp);


            }
        }

        protected void btnMusteriyeGit_Click(object sender, EventArgs e)
        {
            string _path = "/OnePageList11.aspx?OJ1=PG5005;OJ5005&OJ2=PG5005;OJ5016&OJ3=PG5005;OJ5017&OJ4=PG5005;OJ9999&OJ5=PG5005;OJ5019&OJ6=PG5005;OJ5020&OJ7=PG5005;OJ5028&OJ8=PG5005;OJ9999&OJ9=PG5005;OJ9999&P1=" + f.GetContactMusteriNo(currentCardUniqueID, _obj.Comp) + "&GId=" + Request.QueryString["GId"];
            ClientScript.RegisterStartupScript(this.Page.GetType(), "dsygor", "window.open('" + _path + "','_blank');", true);
        }

        protected void btnFirmayaGit_Click(object sender, EventArgs e)
        {
            string _path = "/OnePageList11.aspx?OJ1=PG5004;OJ5004&OJ2=PG5004;OJ5010&OJ3=PG5004;OJ5011&OJ4=PG5004;OJ9999&OJ5=PG5004;OJ5014&OJ6=PG5004;OJ5015&OJ7=PG5004;OJ5028&OJ8=PG5004;OJ9999&OJ9=PG5004;OJ9999&P1=" + f.GetKontakSirketNo(currentCardUniqueID, _obj.Comp) + "&GId=" + Request.QueryString["GId"];
            ClientScript.RegisterStartupScript(this.Page.GetType(), "dsygor", "window.open('" + _path + "','_blank');", true);
        }

        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        { 
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }
    }
}