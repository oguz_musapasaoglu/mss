﻿//------------------------------------------------------------------------------
// <otomatik üretildi>
//     Bu kod bir araç tarafından oluşturuldu.
//
//     Bu dosyada yapılacak değişiklikler yanlış davranışa neden olabilir ve
//     kod tekrar üretildi. 
// </otomatik üretildi>
//------------------------------------------------------------------------------

namespace MSS1.CRM {
    
    
    public partial class FirmaCard {
        
        /// <summary>
        /// form1 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;
        
        /// <summary>
        /// Box1 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxFormLayout Box1;
        
        /// <summary>
        /// txtAdi denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxTextBox txtAdi;
        
        /// <summary>
        /// txtticariUnvan denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtticariUnvan;
        
        /// <summary>
        /// txtAdres1 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtAdres1;
        
        /// <summary>
        /// txtAdres2 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtAdres2;
        
        /// <summary>
        /// cmbUlke denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxComboBox cmbUlke;
        
        /// <summary>
        /// cmbSehir denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxComboBox cmbSehir;
        
        /// <summary>
        /// txtCityText denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxTextBox txtCityText;
        
        /// <summary>
        /// cmbIlce denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxComboBox cmbIlce;
        
        /// <summary>
        /// txtPostCodeText denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxTextBox txtPostCodeText;
        
        /// <summary>
        /// cmbSektor denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxComboBox cmbSektor;
        
        /// <summary>
        /// Box2 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxFormLayout Box2;
        
        /// <summary>
        /// cmbmusteriTemsilcisi denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxComboBox cmbmusteriTemsilcisi;
        
        /// <summary>
        /// cmbnetworkSorumlusu denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxComboBox cmbnetworkSorumlusu;
        
        /// <summary>
        /// txtTel denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxTextBox txtTel;
        
        /// <summary>
        /// txtFax denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxTextBox txtFax;
        
        /// <summary>
        /// txtMail denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxTextBox txtMail;
        
        /// <summary>
        /// txtWeb denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxTextBox txtWeb;
        
        /// <summary>
        /// cmbVD denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxComboBox cmbVD;
        
        /// <summary>
        /// txtVDNo denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxTextBox txtVDNo;
        
        /// <summary>
        /// Box3 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxFormLayout Box3;
        
        /// <summary>
        /// txtFirmaTanitimi denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtFirmaTanitimi;
        
        /// <summary>
        /// btnMusteriyeCevir denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxButton btnMusteriyeCevir;
        
        /// <summary>
        /// btnKaydet denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxButton btnKaydet;
        
        /// <summary>
        /// btnMusteriyeGit denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxButton btnMusteriyeGit;
        
        /// <summary>
        /// btnDosyaGor denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxButton btnDosyaGor;
        
        /// <summary>
        /// dtSehir denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::MSS1.Controls.DataTableControlGAC dtSehir;
        
        /// <summary>
        /// dtMusteriGrubu denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::MSS1.Controls.DataTableControlGAC dtMusteriGrubu;
        
        /// <summary>
        /// dtOdemeVadesi denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::MSS1.Controls.DataTableControlGAC dtOdemeVadesi;
        
        /// <summary>
        /// dtMusteriTemsilcisi denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::MSS1.Controls.DataTableControlGAC dtMusteriTemsilcisi;
        
        /// <summary>
        /// dtVD denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::MSS1.Controls.DataTableControlGAC dtVD;
        
        /// <summary>
        /// dtSektor denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::MSS1.Controls.DataTableControlGAC dtSektor;
        
        /// <summary>
        /// dtUlke denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::MSS1.Controls.DataTableControlGAC dtUlke;
        
        /// <summary>
        /// dtNetworkSorumlusu denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::MSS1.Controls.DataTableControlGAC dtNetworkSorumlusu;
    }
}
