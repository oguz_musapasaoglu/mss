﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"  CodeBehind="shipSec.aspx.cs" Inherits="MSS1.shipSec" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        #overlay {
            position: fixed;
            z-index: 3;
            top: 0px;
            left: 0px;
            background-color: #f8f8f8;
            width: 100%;
            height: 100%;
            filter: Alpha(Opacity=90);
            opacity: 0.9;
            -moz-opacity: 0.9;
            visibility: hidden;
        }

        #theprogress {
            background-color: #fff;
            border: 1px solid #ccc;
            padding: 10px;
            width: 300px;
            height: 70px;
            line-height: 30px;
            text-align: center;
            filter: Alpha(Opacity=100);
            opacity: 1;
            -moz-opacity: 1;
        }

        #modalprogress {
            position: absolute;
            top: 40%;
            left: 50%;
            margin: -11px 0 0 -150px;
            color: #990000;
            font-weight: bold;
            font-size: 14px;
        }

        .auto-style1 {
            width: 100%;
        }
    </style>

    <script type="text/javascript" language="JavaScript">

        function displayLoadingImage() {
            var imgLoading = document.getElementById("imgLoading");
            var overlay = document.getElementById("overlay");
            var lblyukleniyor = document.getElementById("ContentPlaceHolder1_lblyukleniyor");
            overlay.style.visibility = "visible";
            imgLoading.style.visibility = "visible";
            lblyukleniyor.style.visibility = "visible";
        }

        function kontrol(id) {
            if (id != '') {
                //                if (confirm("Müşteri Seçtiniz Onaylıyor musunuz ?") == true) {
                window.open("shipOlustur.aspx?ID=" + id, '_self');
                //                }
            }
        }

        function resetPosition(object, args) {

            var tb = object._element; // tb.id is the associated textbox ID in the grid.

            // Get the position with scrolling.
            var tbposition = [tb.offsetLeft + tb.offsetParent.offsetLeft, tb.offsetTop + tb.offsetParent.offsetTop];

            var xposition = tbposition[0] + 10;

            var yposition = tbposition[1] + 235;

            var ex = object._completionListElement;
            if (ex)

                $common.setLocation(ex, new Sys.UI.Point(xposition, yposition));
        } // End resetPosition
        function resetPosition2(object, args) {

            var tb = object._element; // tb.id is the associated textbox ID in the grid.

            // Get the position with scrolling.
            var tbposition = [tb.offsetLeft + tb.offsetParent.offsetLeft, tb.offsetTop + tb.offsetParent.offsetTop];

            var xposition = tbposition[0] + 10;

            var yposition = tbposition[1] + 610;

            var ex = object._completionListElement;
            if (ex)

                $common.setLocation(ex, new Sys.UI.Point(xposition, yposition));
        } // End resetPosition

        $(document).ready(function () {
            if ($.cookie('colWidth') != null) {
                var columns = $.cookie('colWidth').split(',');
                var i = 0;
                $('.GridViewStyle th').each(function () {
                    $(this).width(columns[i++]);
                });
            }

            $(".GridViewStyle").colResizable({
                liveDrag: true,
                gripInnerHtml: "<div class='grip'></div>",
                draggingClass: "dragging",
                onResize: onSampleResized
            });

        });

        var onSampleResized = function (e) {
            var columns = $(e.currentTarget).find("th");
            var msg = "";
            columns.each(function () { msg += $(this).width() + ","; })
            $.cookie("colWidth", msg);
        };

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table>
        <tr>
            <td height="50">
                <table cellspacing="8">
                    <tr>
                        <td colspan="2">
                            <h2>
                                <asp:Label ID="lblBaslik" runat="server"></asp:Label>
                            </h2>
                        </td>
                    </tr>
                    <tr>
                        <td width="100px">
                            <asp:Label ID="lblShipName" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtShipName" runat="server" AutoPostBack="True" CssClass="textbox" Width="207px" OnTextChanged="txtShipName_TextChanged"></asp:TextBox>
                            <cc1:AutoCompleteExtender ID="txtShipName_AutoCompleteExtender" runat="server" CompletionInterval="100" CompletionListCssClass="listMain" CompletionListHighlightedItemCssClass="itemsSelected" CompletionListItemCssClass="itemmain" CompletionSetCount="30" EnableCaching="false" FirstRowSelected="false" MinimumPrefixLength="1" OnClientShown="resetPosition" ServiceMethod="ShipAra" TargetControlID="txtShipName" UseContextKey="true">
                            </cc1:AutoCompleteExtender>
                        </td>
                    </tr>
                    <tr>
                        <td width="100px">
                            <asp:Label ID="lblImoNo" Text="IMO NO" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtImoNo" runat="server" CssClass="textbox" Width="207px"></asp:TextBox>

                        </td>
                    </tr>
                    <tr>
                        <td width="100px">
                            <asp:Label ID="lblFlag" Text="FLAG" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlFlag" AppendDataBoundItems="true" CssClass="textbox" Width="210px" runat="server">
                                <asp:ListItem Selected="True" Value="-1" Text=""> </asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td width="100px">
                            <asp:Label ID="lblPasif" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkPasif" runat="server" Width="207px"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="overlay">
                                <div id="modalprogress">
                                    <div id="theprogress">
                                        <img id="imgLoading" name="imgLoading" src="images/ajax-loader.gif" style="visibility: hidden;" />
                                        <br />
                                        <div id="yukleniyor" style="visibility: hidden;">
                                            <asp:Label ID="lblyukleniyor" runat="server" Text="Yükleniyor..."></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true" EnableScriptGlobalization="true"
                                EnableScriptLocalization="true">
                            </asp:ScriptManager>
                        </td>
                        <td align="right">
                            <asp:Button ID="btnAra" runat="server" CssClass="myButton" OnClick="btnAra_Click" />
                        </td>
                    </tr>
                </table>
            </td>
            <td height="50">&nbsp;</td>
        </tr>
    </table>
    <div>
        <asp:GridView ID="gvListe" runat="server" AutoGenerateColumns="False" CssClass="mGrid" AllowPaging="True" AllowSorting="True" PageSize="30" OnPageIndexChanging="gvListe_PageIndexChanging" OnRowDataBound="gvListe_RowDataBound" Font-Size="10pt" Width="500px" EmptyDataText="Gemi Bulunamadı.">
            <Columns>
                <asp:TemplateField HeaderText="id" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="lbl_id" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="GEMI NO">
                    <ItemTemplate>
                        <asp:Label ID="lblno" runat="server" Text='<%# Eval("no") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="70px" />
                    <ItemStyle Width="70px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="GEMI ADI">
                    <ItemTemplate>
                        <asp:Label ID="lblad" runat="server" Text='<%# Eval("ad") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="100px" />
                    <ItemStyle Width="100px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ESKİ GEMI ADI">
                    <ItemTemplate>
                        <asp:Label ID="lblexad" runat="server" Text='<%# Eval("exad") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="100px" />
                    <ItemStyle Width="100px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="IMO NO">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("imono") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="100px" />
                    <ItemStyle Width="100px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="FLAG">
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("flag") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Width="100px" />
                    <ItemStyle Width="100px" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>

</asp:Content>

