﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GenelGiderAylikParameter.aspx.cs" Inherits="MSS1.GenelGiderAylikParameter" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .burakalan {
            width: 240px;
        }

        .burakalan2 {
            width: 200px;
        }

        #overlay {
            position: fixed;
            z-index: 3;
            top: 0px;
            left: 0px;
            background-color: #f8f8f8;
            width: 100%;
            height: 100%;
            filter: Alpha(Opacity=90);
            opacity: 0.9;
            -moz-opacity: 0.9;
            visibility: hidden;
        }

        #theprogress {
            background-color: #fff;
            border: 1px solid #ccc;
            padding: 10px;
            width: 300px;
            height: 70px;
            line-height: 30px;
            text-align: center;
            filter: Alpha(Opacity=100);
            opacity: 1;
            -moz-opacity: 1;
        }

        #modalprogress {
            position: absolute;
            top: 40%;
            left: 50%;
            margin: -11px 0 0 -150px;
            color: #990000;
            font-weight: bold;
            font-size: 14px;
        }
    </style>

    <script type="text/javascript" language="javascript">
        function resetPosition(object, args) {

            var tb = object._element; // tb.id is the associated textbox ID in the grid.

            // Get the position with scrolling.
            var tbposition = [tb.offsetLeft + tb.offsetParent.offsetLeft, tb.offsetTop + tb.offsetParent.offsetTop];

            var xposition = tbposition[0] + 10;

            var yposition = tbposition[1] + 335;

            var ex = object._completionListElement;
            if (ex)

                $common.setLocation(ex, new Sys.UI.Point(xposition, yposition));
        } // End resetPosition
        function resetPosition2(object, args) {

            var tb = object._element; // tb.id is the associated textbox ID in the grid.

            // Get the position with scrolling.
            var tbposition = [tb.offsetLeft + tb.offsetParent.offsetLeft, tb.offsetTop + tb.offsetParent.offsetTop];

            var xposition = tbposition[0] + 10;

            var yposition = tbposition[1] + 690;

            var ex = object._completionListElement;
            if (ex)

                $common.setLocation(ex, new Sys.UI.Point(xposition, yposition));
        } // End resetPosition
        function resetPosition3(object, args) {

            var tb = object._element; // tb.id is the associated textbox ID in the grid.

            // Get the position with scrolling.
            var tbposition = [tb.offsetLeft + tb.offsetParent.offsetLeft, tb.offsetTop + tb.offsetParent.offsetTop];

            var xposition = tbposition[0] + 10;

            var yposition = tbposition[1] + 700;

            var ex = object._completionListElement;
            if (ex)

                $common.setLocation(ex, new Sys.UI.Point(xposition, yposition));
        } // End resetPosition
        function resetPositionItem(object, args) {

            var tb = object._element; // tb.id is the associated textbox ID in the grid.

            // Get the position with scrolling.
            var tbposition = [tb.offsetLeft + tb.offsetParent.offsetLeft, tb.offsetTop + tb.offsetParent.offsetTop];

            var xposition = tbposition[0] + 10;

            var yposition = tbposition[1] + 650;

            var ex = object._completionListElement;
            if (ex)

                $common.setLocation(ex, new Sys.UI.Point(xposition, yposition));
        } // End resetPosition
        function resetPositionserino(object, args) {

            var tb = object._element; // tb.id is the associated textbox ID in the grid.

            // Get the position with scrolling.
            var tbposition = [tb.offsetLeft + tb.offsetParent.offsetLeft, tb.offsetTop + tb.offsetParent.offsetTop];

            var xposition = tbposition[0] + 10;

            var yposition = tbposition[1] + 650;

            var ex = object._completionListElement;
            if (ex)

                $common.setLocation(ex, new Sys.UI.Point(xposition, yposition));
        } // End resetPosition
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <br />
            <h2>
                <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptLocalization="true"
                    EnableScriptGlobalization="true" EnablePageMethods="true">
                </asp:ScriptManager>
                <asp:Label ID="lblsayfaadi" runat="server"></asp:Label></h2>
            <table class="fixtable" cellspacing="1">
                <tr>
                    <td nowrap="nowrap" class="burakalan">
                        <asp:Label ID="lblSirket" runat="server"></asp:Label>
                    </td>
                    <td class="burakalan">


                        <asp:DropDownList ID="ddlSirket" runat="server" DataValueField="Code" DataTextField="Text"
                            CssClass="textbox" Height="22px" Width="180px"  DataSourceID="DsComp">                          
                        </asp:DropDownList>
                    </td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">
                        <asp:Label ID="lblAnaDepartman" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtanaDept" runat="server" CssClass="textbox" MaxLength="30" Width="180px" AutoPostBack="True" OnTextChanged="txtanaDept_TextChanged"></asp:TextBox>
                        <cc1:AutoCompleteExtender ServiceMethod="SearchAnaDept" UseContextKey="true" MinimumPrefixLength="1"
                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtanaDept"
                            ID="AutoCompleteExtender2" runat="server" FirstRowSelected="false" CompletionListCssClass="listMain"
                            CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected"
                            OnClientShown="resetPosition">
                        </cc1:AutoCompleteExtender>
                        <asp:Literal ID="ltrAnaDept" runat="server"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">
                        <asp:Label ID="lblRaporTuru" runat="server"></asp:Label>
                    </td>
                    <td class="burakalan">
                        <asp:DropDownList ID="ddlRaporTuru" runat="server" AppendDataBoundItems="True"
                            CssClass="textbox" Height="22px" Width="180px">
                            <asp:ListItem Value="TÜM VERİ GRUPLU" Text="TÜM VERİ GRUPLU"> </asp:ListItem>
                            <asp:ListItem Value="GENEL GİDER DETAY" Text="GENEL GİDER DETAY"> </asp:ListItem>

                        </asp:DropDownList>
                    </td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">
                        <asp:Label ID="lblBolge" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtBolge" runat="server" CssClass="textbox" MaxLength="30" Width="180px" OnTextChanged="txtBolge_TextChanged" AutoPostBack="True"></asp:TextBox>
                        <cc1:AutoCompleteExtender ServiceMethod="SearchBolge" UseContextKey="true" MinimumPrefixLength="1"
                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtBolge"
                            ID="AutoCompleteExtender1" runat="server" FirstRowSelected="false" CompletionListCssClass="listMain"
                            CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected"
                            OnClientShown="resetPosition">
                        </cc1:AutoCompleteExtender>
                        <asp:Literal ID="ltrBolge" runat="server"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">
                        <asp:Label ID="lblYil" runat="server"></asp:Label>
                    </td>
                    <td class="burakalan">
                        <asp:DropDownList ID="ddlYil" runat="server" AppendDataBoundItems="True"
                            CssClass="textbox" Height="22px" Width="180px">
                            <asp:ListItem Value="2017" Text="2017"> </asp:ListItem>
                            <asp:ListItem Value="2018" Text="2018"> </asp:ListItem>
                             <asp:ListItem Value="2019" Text="2019"> </asp:ListItem>
                             <asp:ListItem Value="2020" Text="2020"> </asp:ListItem>
                             <asp:ListItem Value="2021" Text="2021"> </asp:ListItem>
                             <asp:ListItem Value="2022" Text="2022"> </asp:ListItem>
                            <asp:ListItem Value="2023" Text="2023"> </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">
                        <asp:Label ID="lblDepartman" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtDept" runat="server" CssClass="textbox" MaxLength="30" Width="180px" OnTextChanged="txtDept_TextChanged" AutoPostBack="True"></asp:TextBox>
                        <cc1:AutoCompleteExtender ServiceMethod="SearchDept" UseContextKey="true" MinimumPrefixLength="1"
                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtDept"
                            ID="AutoCompleteExtender3" runat="server" FirstRowSelected="false" CompletionListCssClass="listMain"
                            CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected"
                            OnClientShown="resetPosition">
                        </cc1:AutoCompleteExtender>
                        <asp:Literal ID="ltrDept" runat="server"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">&nbsp;</td>
                    <td class="burakalan">&nbsp;</td>

                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">
                        <asp:Label ID="lblPersonel" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtPersonel" runat="server" CssClass="textbox" MaxLength="30" Width="180px" OnTextChanged="txtPersonel_TextChanged" AutoPostBack="True"></asp:TextBox>
                        <cc1:AutoCompleteExtender ServiceMethod="SearchPersonel" UseContextKey="true" MinimumPrefixLength="1"
                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtPersonel"
                            ID="AutoCompleteExtender4" runat="server" FirstRowSelected="false" CompletionListCssClass="listMain"
                            CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected"
                            OnClientShown="resetPosition">
                        </cc1:AutoCompleteExtender>
                        <asp:Literal ID="ltrPersonel" runat="server"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">&nbsp;</td>
                    <td class="burakalan">&nbsp;</td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">
                        <asp:Label ID="lblArac" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtArac" runat="server" CssClass="textbox" MaxLength="30" Width="180px" OnTextChanged="txtArac_TextChanged" AutoPostBack="True"></asp:TextBox>
                        <cc1:AutoCompleteExtender ServiceMethod="SearchArac" UseContextKey="true" MinimumPrefixLength="1"
                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtArac"
                            ID="AutoCompleteExtender5" runat="server" FirstRowSelected="false" CompletionListCssClass="listMain"
                            CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected"
                            OnClientShown="resetPosition">
                        </cc1:AutoCompleteExtender>
                        <asp:Literal ID="ltrArac" runat="server"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">&nbsp;</td>
                    <td class="burakalan">&nbsp;</td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">
                        <asp:Label ID="lblGGGrup" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtGGGrup" runat="server" CssClass="textbox" MaxLength="30" Width="180px" AutoPostBack="True" OnTextChanged="txtGGGrup_TextChanged"></asp:TextBox>
                        <cc1:AutoCompleteExtender ServiceMethod="SearchGGGrup" UseContextKey="true" MinimumPrefixLength="1"
                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtGGGrup"
                            ID="txtGGGrup_AutoCompleteExtender" runat="server" FirstRowSelected="false" CompletionListCssClass="listMain"
                            CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected"
                            OnClientShown="resetPosition">
                        </cc1:AutoCompleteExtender>

                        <asp:Literal ID="ltrGGGrup" runat="server" Visible="false"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">&nbsp;</td>
                    <td class="burakalan">&nbsp;</td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">
                        <asp:Label ID="lblGG" runat="server"></asp:Label>
                    </td>
                    <td style="display: none">
                        <asp:TextBox ID="txtGG" runat="server" CssClass="textbox" MaxLength="30" Width="180px" AutoPostBack="True"></asp:TextBox>
                        <cc1:AutoCompleteExtender ServiceMethod="SearchGG" UseContextKey="true" MinimumPrefixLength="1"
                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtGG"
                            ID="txtGG_AutoCompleteExtender" runat="server" FirstRowSelected="false" CompletionListCssClass="listMain"
                            CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected"
                            OnClientShown="resetPosition">
                        </cc1:AutoCompleteExtender>
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">&nbsp;</td>
                    <td class="burakalan">&nbsp;</td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">
                        <asp:Label ID="lblFirma" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtFirma" runat="server" CssClass="textbox" MaxLength="30" Width="180px" AutoPostBack="True" OnTextChanged="txtFirma_TextChanged"></asp:TextBox>
                        <cc1:AutoCompleteExtender ServiceMethod="SearchFirmalar" UseContextKey="true" MinimumPrefixLength="1"
                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtFirma"
                            ID="AutoCompleteExtender7" runat="server" FirstRowSelected="false" CompletionListCssClass="listMain"
                            CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected"
                            OnClientShown="resetPosition">
                        </cc1:AutoCompleteExtender>

                        <asp:Literal ID="ltrFirma" runat="server" Visible="False"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">&nbsp;</td>
                    <td class="burakalan">&nbsp;</td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">
                        <asp:Label ID="ltrGrup1" runat="server"></asp:Label>
                    </td>
                    <td class="burakalan">
                        <asp:DropDownList ID="ddlGrup1" runat="server" AppendDataBoundItems="True"
                            CssClass="textbox" Height="22px" Width="180px">
                            <asp:ListItem Value="ANA DEPARTMAN" Text="ANA DEPARTMAN"> </asp:ListItem>
                            <asp:ListItem Value="OP. BÖLGE" Text="OP. BÖLGE"> </asp:ListItem>
                            <asp:ListItem Value="DEPARTMAN" Text="DEPARTMAN"> </asp:ListItem>
                            <asp:ListItem Value="PERSONEL" Text="PERSONEL"> </asp:ListItem>
                            <asp:ListItem Value="ARAÇ PLAKA" Text="ARAÇ PLAKA"> </asp:ListItem>
                            <asp:ListItem Value="GELİR GİDER GRUBU" Text="GELİR GİDER GRUBU"> </asp:ListItem>
                            <asp:ListItem Value="GELİR GİDER DETAY" Text="GELİR GİDER DETAY"> </asp:ListItem>
                            <asp:ListItem Value="FİRMA" Text="FİRMA"> </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">
                        <asp:Label ID="ltrGrup2" runat="server"></asp:Label>
                    </td>
                    <td class="burakalan">
                        <asp:DropDownList ID="ddlGrup2" runat="server" AppendDataBoundItems="True"
                            CssClass="textbox" Height="22px" Width="180px">
                            <asp:ListItem Value="ANA DEPARTMAN" Text="ANA DEPARTMAN"> </asp:ListItem>
                            <asp:ListItem Value="OP. BÖLGE" Text="OP. BÖLGE"> </asp:ListItem>
                            <asp:ListItem Value="DEPARTMAN" Text="DEPARTMAN"> </asp:ListItem>
                            <asp:ListItem Value="PERSONEL" Text="PERSONEL"> </asp:ListItem>
                            <asp:ListItem Value="ARAÇ PLAKA" Text="ARAÇ PLAKA"> </asp:ListItem>
                            <asp:ListItem Value="GELİR GİDER GRUBU" Text="GELİR GİDER GRUBU"> </asp:ListItem>
                            <asp:ListItem Value="GELİR GİDER DETAY" Text="GELİR GİDER DETAY"> </asp:ListItem>
                            <asp:ListItem Value="FİRMA" Text="FİRMA"> </asp:ListItem>


                        </asp:DropDownList>
                    </td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">
                        <asp:Label ID="ltrGrup3" runat="server"></asp:Label></td>
                    <td class="burakalan">
                        <asp:DropDownList ID="ddlGrup3" runat="server" AppendDataBoundItems="True"
                            CssClass="textbox" Height="22px" Width="180px">
                            <asp:ListItem Value="" Text=""> </asp:ListItem>
                            <asp:ListItem Value="ANA DEPARTMAN" Text="ANA DEPARTMAN"> </asp:ListItem>
                            <asp:ListItem Value="OP. BÖLGE" Text="OP. BÖLGE"> </asp:ListItem>
                            <asp:ListItem Value="DEPARTMAN" Text="DEPARTMAN"> </asp:ListItem>
                            <asp:ListItem Value="PERSONEL" Text="PERSONEL"> </asp:ListItem>
                            <asp:ListItem Value="ARAÇ PLAKA" Text="ARAÇ PLAKA"> </asp:ListItem>
                            <asp:ListItem Value="GELİR GİDER GRUBU" Text="GELİR GİDER GRUBU"> </asp:ListItem>
                            <asp:ListItem Value="GELİR GİDER DETAY" Text="GELİR GİDER DETAY"> </asp:ListItem>
                            <asp:ListItem Value="FİRMA" Text="FİRMA"> </asp:ListItem>



                        </asp:DropDownList>
                    </td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">&nbsp;</td>
                    <td class="burakalan">&nbsp;</td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">&nbsp;</td>
                    <td class="burakalan">&nbsp;</td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="burakalan" nowrap="nowrap">
                        <asp:Button ID="btnRaporOlustur" runat="server" CssClass="myButton" OnClick="btnRaporOlustur_Click" />
                    </td>
                    <td class="burakalan"></td>
                    <td width="50px">
                        <div id="overlay">
                            <div id="modalprogress">
                                <div id="theprogress">
                                    <img src="images/ajax-loader.gif" name="imgLoading" id="imgLoading" style="visibility: hidden;">
                                    <br />
                                    <div id="yukleniyor" style="visibility: hidden;">
                                        <asp:Label ID="lblyukleniyor" runat="server" Text="Yükleniyor..."></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="burakalan2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
        <asp:SqlDataSource ID="DsComp" runat="server" ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    </form>
</body>
</html>
