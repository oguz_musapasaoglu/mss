﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.WSGeneric;
using MSS1.Codes;

namespace MSS1
{
    public partial class FaturaOnGiris : Bases
    {
        #region Page Fields
        string ConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        public ws_baglantı ws = new ws_baglantı();
        public Methods method = new Methods();
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        SessionBase _obj;
        private enum invoiceTypes { NONE, SATINALMA, SATIS_IADE };
        private enum renderTypes { Add, Edit };


        private string faturaSirketi
        {
            get
            {
                string procName = string.Empty;
                switch (Request.QueryString["FaturaStatu"].ToString())
                {
                    case "ALIMISLEMDE":
                        procName = "0_W50319";
                        break;
                    case "ALIMTAMAMLANDI":
                        procName = "0_W50329";
                        break;
                    case "IADEISLEMDE":
                        procName = "0_W50319";
                        break;
                    case "IADETAMAMLANDI":
                        procName = "0_W50349";
                        break;

                }




                return f.tekbirsonucdonder("select RCOMP from [" + procName + "] where RCOMP='" + _obj.Comp.ToString() + "' AND R0011='" + currentinvoiceNo + "'");
            }
        }
        private bool eFatura
        {
            get
            {
                bool _type = false;
                try
                {
                    string sorgu = "select [E-Fatura] from [0D_70004_01_ALIM_FATURA_ONAY_LISTE] where COMPANY='" + _obj.Comp.ToString() + "' AND [Fatura No_]='" + currentinvoiceNo + "'";
                    if (f.tekbirsonucdonder(sorgu) == "1")
                        _type = true;
                    else
                        _type = false;
                }
                catch (Exception)
                {
                    _type = false;
                }
                return _type;
            }
        }

        private decimal limitY
        {
            get
            {
                string limitY = f.tekbirsonucdonder("select LIMITY from [0_W50306] where FTNO='" + currentinvoiceNo + "' and KULLANICI='" + _obj.UserName.ToString() + "' and COMPANY='" + _obj.Comp.ToString() + "'");
                int durum = 0;
                int sayi = 0;
                if (int.TryParse(limitY, out durum))
                {
                    sayi = durum;
                }
                else
                {
                    sayi = 0;
                }
                return Convert.ToDecimal(sayi);
            }
        }
        private invoiceTypes invoiceType
        {
            get
            {
                invoiceTypes _pageType = invoiceTypes.NONE;

                if (!string.IsNullOrEmpty(Request.QueryString["InvoiceType"]))
                {
                    if (Request.QueryString["InvoiceType"].ToString() == "SATINALMA")
                        _pageType = invoiceTypes.SATINALMA;
                    else if (Request.QueryString["InvoiceType"].ToString() == "SATIS_IADE")
                        _pageType = invoiceTypes.SATIS_IADE;
                    else
                        _pageType = invoiceTypes.NONE;
                }
                return _pageType;
            }
        }
        private string invoiceStatu
        {
            get
            {
                return Request.QueryString["FaturaStatu"].ToString();
            }
        }




        private string invoiceTypeText
        {
            get
            {
                string _temp = string.Empty;
                if (invoiceType == invoiceTypes.SATINALMA)
                {
                    _temp = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblFaturaTuruAlim");
                }
                else
                {
                    _temp = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblFaturaTuruiade");
                }
                return _temp;
            }
        }

        public bool dosyaVarmi { get; set; }
        public string DosyaAdi { get; set; }
        public string DosyaPath { get; set; }

        private string currentinvoiceNo
        {
            get
            {
                string _invNo = string.Empty;
                try
                {
                    string[] _temp = Request.QueryString["invoiceNo"].ToString().Split('|');
                    _invNo = _temp[0];
                }
                catch (Exception)
                {
                    _invNo = string.Empty;
                }
                return _invNo;
            }
        }
        private string currentJobNo
        {
            get
            {
                string _jobNo = string.Empty;
                try
                {
                    _jobNo = Request.QueryString["ProjeNo"].ToString();
                }
                catch (Exception)
                {
                    _jobNo = string.Empty;
                }
                return _jobNo;
            }
        }


        private renderTypes renderType
        {
            get
            {
                renderTypes _rendtype = renderTypes.Add;
                if (string.IsNullOrEmpty(Request.QueryString["invoiceNo"]))
                {
                    _rendtype = renderTypes.Add;
                }
                else
                {
                    _rendtype = renderTypes.Edit;
                }
                return _rendtype;
            }
        }


        #endregion

        private void DilGetir()
        {

            Box1.FindItemByFieldName("lblFaturaTuru").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblFaturaTuru");
            if (invoiceType == invoiceTypes.SATINALMA)
            {
                Box1.FindItemByFieldName("lblVendCust").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblVend");

            }
            else
            {
                Box1.FindItemByFieldName("lblVendCust").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblCust");

            }
            Box1.FindItemByFieldName("lblReferansNo").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblReferansNo");
            Box1.FindItemByFieldName("lblSerino").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblSerino");
            Box1.FindItemByFieldName("lblHariciBelgeNo").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblHariciBelgeNo");
            Box1.FindItemByFieldName("lblBelgeTarihi").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblBelgeTarihi");
            Box1.FindItemByFieldName("lblDeftereNakilTarihi").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblDeftereNakilTarihi");

            Box2.FindItemByFieldName("lblDovizCinsi").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblDovizCinsi");
            Box2.FindItemByFieldName("lblDovizKuru").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblDovizKuru");

            Box2.FindItemByFieldName("lblMatrah").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblMatrah");
            Box2.FindItemByFieldName("lblKdvTutari").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblKdvTutari");
            Box2.FindItemByFieldName("lblToplamTutar").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblToplamTutar");
            Box2.FindItemByFieldName("lblToplamTutartl").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblToplamTutar") + " TL";
            Box2.FindItemByFieldName("lblFirmaOdemeVadesi").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblFirmaOdemeVadesi");
            Box2.FindItemByFieldName("lblFaturaOdemeVadesi").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblFaturaVadesi");



            Box3.FindItemByFieldName("lblNot").Caption = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblNot");

            btnKaydet.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "btnKaydet");
            btnDosyaBagla.Value = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "btnDosyaBagla");
            btnDosyaGor.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "btnDosyaGor");
            btnDosyaSil.Value = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "btnDosyaSil");
            btnKayitSil.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "btnKayitSil");
            btnFaturaOlustur.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "btnFaturaOlustur");

            lblSilmeMessage.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblSilmeMessage");
            lblconfirmOnayaGonderMessage.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "lblconfirmOnayaGonderMessage");
            btnReddet.Value = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGirisOnay", "btnReddet");
            btnOnayla.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGirisOnay", "btnOnayla");
            btnAciklamaKaydet.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGirisOnay", "btnAciklamaKaydet");
            txtodemeVadesiMessage.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGirisOnay", "txtodemeVadesiMessage");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            _obj = TabSessions.ControlState(this.Page, Request.QueryString["GId"].ToString());

            DosyaKontrol();
            if (IsPostBack || IsCallback)
            { return; }
            PreparePageFields();
            PreparePage();

        }

        private void PreparePage()
        {
            if (renderType == renderTypes.Edit)
            {
                FillFormObjectsForEdit();

            }
        }

        private void pageAuthority()
        {
            string invoiceStatu = f.tekbirsonucdonder("select Durum from [0D_70004_01_ALIM_FATURA_ONAY_LISTE] where COMPANY='" + _obj.Comp.ToString() + "' AND [Fatura No_]='" + currentinvoiceNo + "'");
            if (invoiceStatu == "AÇIK" || invoiceStatu == "REDDEDİLDİ")
            {
                btnDosyaSil.Style.Add("Display", "normal");
            }

        }

        private void FillFormObjectsForEdit()
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                conn.Open();

                string procName = string.Empty;
                switch (Request.QueryString["FaturaStatu"].ToString())
                {
                    case "ALIMISLEMDE":
                        procName = "SP_0_W50319";
                        break;
                    case "ALIMTAMAMLANDI":
                        procName = "SP_0_W51401";
                        break;
                    case "IADEISLEMDE":
                        procName = "SP_0_W50339";
                        break;
                    case "IADETAMAMLANDI":
                        procName = "SP_0_W51521";
                        break;

                }

                using (SqlCommand cmd = new SqlCommand(procName, conn))
                {

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@COMP", _obj.Comp.ToString());
                    cmd.Parameters.AddWithValue("@USER", _obj.UserName.ToString());
                    cmd.Parameters.AddWithValue("@YTKG", "AK97");
                    cmd.Parameters.AddWithValue("@YTK1", "");
                    cmd.Parameters.AddWithValue("@YTK2", "");
                    cmd.Parameters.AddWithValue("@YTK3", "");
                    cmd.Parameters.AddWithValue("@YTK4", "");
                    cmd.Parameters.AddWithValue("@YTK5", "");
                    cmd.Parameters.AddWithValue("@PRM1", Request.QueryString["invoiceNo"].ToString());
                    cmd.Parameters.AddWithValue("@PRM2", "");
                    cmd.Parameters.AddWithValue("@PRM3", "");


                    SqlDataReader sdr = cmd.ExecuteReader();

                    if (sdr.Read())
                    {
                        if (sdr["R0037"].ToString() != "1" & sdr["R0037"].ToString() != "4")
                            Response.Redirect("~/FaturaCard/Satinalma.aspx?FaturaStatu=" + invoiceStatu + "&InvoiceType=" + invoiceType + "&invoiceNo=" + currentinvoiceNo+ "&GId=" + Request.QueryString["GId"]);
                        HiddenDocIsSaved.Value = "1";
                        cmbVendCust.Value = sdr["R0013"];
                        txtReferansNo.Text = sdr["R0032"].ToString();
                        txtSeriNo.Text = sdr["R0033"].ToString();
                        txtHariciBelgeNo.Text = sdr["R0015"].ToString();
                        txtDeftereNakilTarihi.Date = Convert.ToDateTime(sdr["R0039"].ToString());
                        txtBelgeTarihi.Date = Convert.ToDateTime(sdr["R0016"].ToString());
                        cmbDovizCinsi.Value = sdr["R0017"].ToString();
                        txtDovizKuru.Text = string.Format("{0:N4}", sdr["R0018"]);
                        if (method.StringToDecimal(txtDovizKuru.Text) == 0)
                            txtDovizKuru.Text = "1";
                        txtMatrah.Text = string.Format("{0:N2}", sdr["R0034"]);
                        txtKdvTutari.Text = string.Format("{0:N2}", sdr["R0035"]);
                        txtToplamTutar.Text = string.Format("{0:N2}", sdr["R0019"]);

                        txtFirmaOdemeVadesiCode.Text = sdr["R0022"].ToString();
                        txtFirmaOdemeVadesi.Text = sdr["R0023"].ToString();
                        Session["OdemeVadesi"] = sdr["R0020"].ToString();


                        txtToplamTutartl.Text = string.Format("{0:N2}", sdr["R0044"]);


                        cmbFaturaVadesi.Value = sdr["R0020"].ToString();


                        txtNot.Text = sdr["R0029"].ToString();


                        if (eFatura)
                        {
                            cmbVendCust.Enabled = false;
                            txtSeriNo.Enabled = false;
                            txtHariciBelgeNo.Enabled = false;
                            txtBelgeTarihi.Enabled = false;
                            txtDeftereNakilTarihi.Enabled = false;

                            cmbDovizCinsi.Enabled = false;
                        }
                        txtSayfaTurveNo.Text = currentinvoiceNo + "-" + invoiceType.ToString();
                    }

                    conn.Close();
                }
            }
        }

        private void PreparePageFields()
        {
            txtFaturaTuru.Text = invoiceTypeText;

            DilGetir();
            pageAuthority();

            //InitDataTable(DTVendCust.Table);
            FillVendCust();

            InitDataTable(DTCurrency.Table);
            FillCurrency(DTCurrency.Table);

            InitDataTable(DTPaymentTerm.Table);
            FillPaymentTerms(DTPaymentTerm.Table);

            //InitDataTableNots(DTNots.Table);
            //FillNots(DTNots.Table);
            ButtonInitiliaze();

            lblGId.Text = Request.QueryString["GId"];
        }

        private void ButtonInitiliaze()
        {
            SqlConnection baglanti = new SqlConnection(ConnString);
            DataTable dt = new DataTable();
            try
            {
                string procName = "";
                switch (Request.QueryString["FaturaStatu"].ToString())
                {
                    case "ALIMISLEMDE":
                        procName = "SP_0_W50319";
                        break;
                    case "ALIMTAMAMLANDI":
                        procName = "SP_0_W50329";
                        break;
                    case "IADEISLEMDE":
                        procName = "SP_0_W50339";
                        break;
                    case "IADETAMAMLANDI":
                        procName = "SP_0_W50349";
                        break;

                }

                baglanti.Open();
                SqlCommand sorgu = new SqlCommand();

                sorgu.Connection = baglanti;
                sorgu.CommandType = CommandType.StoredProcedure;
                sorgu.CommandText = procName;
                SqlParameter p_sirket = new SqlParameter("@COMP", SqlDbType.VarChar, 20);
                p_sirket.Value = faturaSirketi;
                sorgu.Parameters.Add(p_sirket);
                SqlParameter p_adi = new SqlParameter("@USER", SqlDbType.VarChar, 20);
                p_adi.Value = _obj.UserName.ToString();
                sorgu.Parameters.Add(p_adi);
                SqlParameter p_bankakodu = new SqlParameter("@YTKG", SqlDbType.VarChar, 20);
                p_bankakodu.Value = "AK97";
                sorgu.Parameters.Add(p_bankakodu);
                SqlDataAdapter sorguDA = new SqlDataAdapter(sorgu);
                sorguDA.Fill(dt);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                baglanti.Close();
            }


            if (!string.IsNullOrEmpty(currentinvoiceNo))
            {
                if (dt.Select("R0011='" + currentinvoiceNo + "'").Length > 0)
                {

                    btnKaydet.Visible = dt.Select("R0011='" + currentinvoiceNo + "' AND RYTK1=1").Length > 0 ? true : false;
                    btnDosyaBagla.Visible = dt.Select("R0011='" + currentinvoiceNo + "' AND RYTK2=1").Length > 0 ? true : false;
                    btnFaturaOlustur.Visible = dt.Select("R0011='" + currentinvoiceNo + "' AND RYTK1=1").Length > 0 ? true : false;
                    btnOnayla.Visible = dt.Select("R0011='" + currentinvoiceNo + "' AND RYTK4=1").Length > 0 ? true : false;
                    btnReddet.Visible = dt.Select("R0011='" + currentinvoiceNo + "' AND RYTK4=1").Length > 0 ? true : false;
                    btnDosyaSil.Visible = dt.Select("R0011='" + currentinvoiceNo + "' AND RYTK3=1").Length > 0 ? true : false;
                    btnKayitSil.Visible = dt.Select("R0011='" + currentinvoiceNo + "' AND RYTK5=1").Length > 0 ? true : false;
                }
                else
                {
                    btnKaydet.Visible = false;
                    btnDosyaBagla.Visible = false;
                    btnFaturaOlustur.Visible = false;
                    btnOnayla.Visible = false;
                    btnReddet.Visible = false;
                    btnDosyaSil.Visible = false;
                    btnKayitSil.Visible = false;
                }


            }
            else
            {
                btnKaydet.Visible = true;
                btnDosyaBagla.Visible = true;
                btnFaturaOlustur.Visible = false;
                btnOnayla.Visible = false;
                btnReddet.Visible = false;
                btnDosyaSil.Visible = false;
                btnKayitSil.Visible = false;
            }

        }

        private void InitDataTableNots(DataTable dt)
        {
            dt.Columns.Add("notType", typeof(string));
            dt.Columns.Add("Tarih", typeof(DateTime));
            dt.Columns.Add("Saat", typeof(DateTime));
            dt.Columns.Add("Not", typeof(string));
            dt.Columns.Add("Kullanici", typeof(string));
        }

        private void FillNots(DataTable dt)
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select Tarih,Saat, case [Hareket Turu]  when 3 Then 'Red' else 'Not' End notType, Not1+' '+Not2 [Not], KullaniciAdi Kullanici from [vw_AlimOnayTarihce] where [Fatura No_]='" + currentinvoiceNo + "' and (Not1 <>'' or Not2<>'')  and COMPANY='" + _obj.Comp.ToString() + "' order by [Fatura No_], Tarih,Saat ";
                SqlCommand cmd = new SqlCommand(sorgu, conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                //gridNot.DataBind();
            }
        }

        private void openPDF()
        {
            if (HiddenDocIsSaved.Value == "1")
                ClientScript.RegisterStartupScript(this.Page.GetType(), "popupOpener", "var popup=window.open('" + CurrentFolderName(_obj.Comp) + "/Dosya/SatinAlma_Onay/" + currentinvoiceNo + "10000.pdf" + "');popup.focus();", true);
        }

        private void DosyaKontrol()
        {
            if (currentinvoiceNo != string.Empty)
            {
                string query = "select count(*) from [0C_50000_00_DOSYA BAGLAMA] where COMPANY='" + _obj.Comp.ToString() + "' and [Dosya No]='" + currentinvoiceNo + "'";
                dosyaVarmi = Convert.ToInt32(f.tekbirsonucdonder(query)) > 0 ? true : false;
                if (dosyaVarmi)
                {

                    btnDosyaSil.Style.Add("Display", "normal");

                }
                else
                {
                    btnDosyaBagla.Visible = true;
                    //   btnDosyaGor.Visible = false;
                    btnDosyaSil.Style.Add("Display", "none");
                }

            }
        }


        private void InitDataTable(DataTable dt)
        {
            dt.Columns.Add("Code", typeof(string));
            dt.Columns.Add("Text", typeof(string));
        }

        private void FillVendCust()
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select [No_] Code, Name Text, PAYMDESC from [0C_00018_00023_02_CUSTOMER_VENDOR]";
                switch (invoiceType)
                {
                    case invoiceTypes.SATINALMA:
                        sorgu += " where TYPE='VEND' and COMPANY='" + _obj.Comp.ToString() + "'";
                        break;
                    case invoiceTypes.SATIS_IADE:
                        sorgu += " where TYPE='CUST' and COMPANY='" + _obj.Comp.ToString() + "'";
                        break;
                }
                cmbVendCust.Items.Clear();
                DSVendCust.SelectCommand = sorgu;
                cmbVendCust.DataBind();
            }
        }

        private void FillCurrency(DataTable dt)
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select KOD Code, ACIKLAMA Text From [0_W10400] WHERE COMPANY='" + _obj.Comp.ToString() + "'";
                SqlCommand cmd = new SqlCommand(sorgu, conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                cmbDovizCinsi.DataBind();
            }
        }

        private void FillPaymentTerms(DataTable dt)
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select Code, Description Text from [0C_00003_00_PAYMENT_TERMS] where COMPANY='" + _obj.Comp.ToString() + "'";
                SqlCommand cmd = new SqlCommand(sorgu, conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                cmbFaturaVadesi.DataBind();
            }
        }

        private void FillOnayDepartmani(DataTable dt)
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select Code, Name Text from [0C_00349_00_DIMENSION VALUES] where Blocked=0 and [Dimension Code]='DEPARTMAN' and COMPANY='" + _obj.Comp.ToString() + "'";
                SqlCommand cmd = new SqlCommand(sorgu, conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                cmbFaturaVadesi.DataBind();
            }
        }
        private void FillJobTypes(DataTable dt)
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select [LineNo] Code, Adi Text from [0C_50055_13_LOOKUPS_AFO_JOB_TUR] where Tur='Job Tur' and COMPANY='" + _obj.Comp.ToString() + "'";
                SqlCommand cmd = new SqlCommand(sorgu, conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                cmbFaturaVadesi.DataBind();
            }
        }

        private void FillJobNo(DataTable dt)
        {
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                string sorgu = "select [No_] Code ,[GAC Proje No] Text from [0C_00167_02_JOB] where COMPANY='" + _obj.Comp.ToString() + "'";
                SqlCommand cmd = new SqlCommand(sorgu, conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }
        }
        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        protected void btnKaydet_Click(object sender, EventArgs e)
        {
            decimal S1 = method.StringToDecimal(txtToplamTutartl.Text),
                S2 = method.StringToDecimal(txtToplamTutar.Text) * method.StringToDecimal(txtDovizKuru.Text);
            if (string.Format("{0:N2}", S1) != string.Format("{0:N2}", S2))
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "msg007"));
                return;
            }
            if (txtFaturaTuru.Text != "" && cmbVendCust.SelectedItem != null && txtHariciBelgeNo.Text != "" &&
                txtBelgeTarihi.Text != "" &&
                txtDeftereNakilTarihi.Text != "" && txtToplamTutartl.Text != ""
                && (txtSeriNo.Text != "" || eFatura))
            {
                FaturaDuzenle(sender);
            }
            else
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "msg002"));
                return;
            }
        }
        private void FaturaDuzenle(object sender)
        {

            if (currentinvoiceNo == string.Empty)
            {
                string[] _params = {
                                     invoiceType == invoiceTypes.SATINALMA ? "1" : "2",
                                     cmbVendCust.SelectedItem != null ? cmbVendCust.SelectedItem.Value.ToString() : "",
                                     txtReferansNo.Text, txtSeriNo.Text, txtHariciBelgeNo.Text,
                                     method.DateToStringNAVType(txtBelgeTarihi.Text) ,
                                     cmbDovizCinsi.SelectedItem != null ? cmbDovizCinsi.SelectedItem.Value.ToString() : "",
                                     method.StringToDecimal(txtDovizKuru.Text).ToString(),
                                     method.StringToDecimal(txtMatrah.Text).ToString(), method.StringToDecimal(txtKdvTutari.Text).ToString(),
                                     method.StringToDecimal(txtToplamTutar.Text).ToString(),
                                     string.IsNullOrEmpty(txtFirmaOdemeVadesiCode.Text) == false ? txtFirmaOdemeVadesiCode.Text : "0",
                                     cmbFaturaVadesi.SelectedItem != null ? cmbFaturaVadesi.SelectedItem.Value.ToString() : "",
                                     "", "", "0",
                                     txtNot.Text, _obj.UserName.ToString(),
                                      method.DateToStringNAVType(txtDeftereNakilTarihi.Text) , Convert.ToInt32(chRM.Checked).ToString()
                                     ,method.StringToDecimal(txtToplamTutartl.Text).ToString()};

                string invoinceNo = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "SatınalmaOnayOlustur§" + string.Join("§", _params), _obj.Comp);


                if (invoinceNo.Length > 15)
                {
                    MessageBox(invoinceNo);
                }
                else
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "msg001"));
                    if (sender != null)
                    {
                        Response.Redirect("/FaturaOnGiris.aspx?FaturaStatu=" + invoiceStatu + "&InvoiceType=" + invoiceType.ToString() + "&invoiceNo=" + invoinceNo+ "&GId=" + Request.QueryString["GId"]);
                    }

                }

            }
            else
            {
                string[] _params = {this.currentinvoiceNo,
                                    invoiceType == invoiceTypes.SATINALMA ? "1" : "2",
                                    cmbVendCust.SelectedItem != null ? cmbVendCust.SelectedItem.Value.ToString() : "",
                                    txtReferansNo.Text, txtSeriNo.Text, txtHariciBelgeNo.Text,
                                    method.DateToStringNAVType(txtBelgeTarihi.Text) ,
                                    cmbDovizCinsi.SelectedItem != null ? cmbDovizCinsi.SelectedItem.Value.ToString() : "",
                                    method.StringToDecimal(txtDovizKuru.Text).ToString(),
                                    method.StringToDecimal(txtMatrah.Text).ToString(), method.StringToDecimal(txtKdvTutari.Text).ToString(),
                                    method.StringToDecimal(txtToplamTutar.Text).ToString(),
                                    string.IsNullOrEmpty(txtFirmaOdemeVadesiCode.Text) == false ? txtFirmaOdemeVadesiCode.Text : "0",
                                    cmbFaturaVadesi.SelectedItem != null ? cmbFaturaVadesi.SelectedItem.Value.ToString() : "",
                                       "", "", "0",
                                    txtNot.Text,
                                    _obj.UserName.ToString(),  method.DateToStringNAVType(txtDeftereNakilTarihi.Text) , Convert.ToInt32(chRM.Checked).ToString(),
                                    method.StringToDecimal(txtToplamTutartl.Text).ToString()};
                string invoinceNo = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "SatınalmaOnayDegisiklik§" + string.Join("§", _params), _obj.Comp);

                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "msg001"));
                if (sender != null)
                {
                    if (eFatura)
                        Response.Redirect("/FaturaOnGiris.aspx?FaturaStatu=" + invoiceStatu + "&InvoiceType=" + invoiceType.ToString() + "&invoiceNo=" + this.currentinvoiceNo + "&eFatura=1"+ "&GId=" + Request.QueryString["GId"]);
                    else
                        Response.Redirect("/FaturaOnGiris.aspx?FaturaStatu=" + invoiceStatu + "&InvoiceType=" + invoiceType.ToString() + "&invoiceNo=" + this.currentinvoiceNo+ "&GId=" + Request.QueryString["GId"]);
                }
            }



        }


        protected void btnKayitSil_Click(object sender, EventArgs e)
        {
            if (currentinvoiceNo != string.Empty)
            {

                string[] _params = { this.currentinvoiceNo, _obj.UserName.ToString() };
                string invoinceNo = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "SatınalmaOnaySil§" + string.Join("§", _params), _obj.Comp);

                if (!ClientScript.IsStartupScriptRegistered("JSFaturaAc"))
                {
                    ClientScript.RegisterStartupScript(GetType(), "JSFaturaAc", @"window.parent.location.href=window.parent.location.href", true);
                }
                //Response.Redirect("/FaturaOnGiris.aspx?InvoiceType=" + invoiceType.ToString());

            }
            Session.Remove("OdemeVadesi");
        }

        protected void btnDosyaBagla_Click(object sender, EventArgs e)
        {
            if (currentinvoiceNo != string.Empty)
            {
                Response.Write("<script>window.open('dosyabagla.aspx?BelgeNo=" + currentinvoiceNo + "&tur=28&dtur=28" + "&FaturaOnGiris=" + invoiceType.ToString() + "&GId=" + Request.QueryString["GId"]+"','List','scrollbars=no,resizable=no,width=650,height=460,top=200,left=400');</script>");
            }

        }

        protected void btnDosyaSil_Click(object sender, EventArgs e)
        {
            if (currentinvoiceNo != string.Empty)
            {
                Response.Write("<script>window.open('dosyagor.aspx?GId=" + Request.QueryString["GId"] +"&BelgeNo=" + currentinvoiceNo + "&tur=28&dtur=28&Silme=1','List','scrollbars=no,resizable=no,width=650,height=460,top=200,left=400');</script>");
            }
        }


        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }

        protected void callbackPanelDoviz_Callback(object sender, CallbackEventArgsBase e)
        {
            txtDovizKuru.Text = f.returnKur(txtBelgeTarihi.Text, cmbDovizCinsi.SelectedItem.Value.ToString(), _obj.Comp);

        }

        protected void btnDosyaGor_Click(object sender, EventArgs e)
        {
            if (currentinvoiceNo != string.Empty)
            {
                openPDF();
            }
        }



        protected void cmbVendCust_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtFirmaOdemeVadesi.Text = f.tekbirsonucdonder("select PAYMDESC from [0C_00018_00023_02_CUSTOMER_VENDOR] where " +
                "No_='" + cmbVendCust.SelectedItem.Value.ToString() + "' and COMPANY='" + _obj.Comp.ToString() + "'");
            txtFirmaOdemeVadesiCode.Text = f.tekbirsonucdonder("select [Payment Terms Code] from [0C_00018_00023_02_CUSTOMER_VENDOR] where " +
                "No_='" + cmbVendCust.SelectedItem.Value.ToString() + "' and COMPANY='" + _obj.Comp.ToString() + "'");
            //Session["OdemeVadesi"] = f.tekbirsonucdonder("select [Payment Terms Code] from [0C_00018_00023_02_CUSTOMER_VENDOR] where " +
            //    "No_='" + cmbVendCust.SelectedItem.Value.ToString() + "' and COMPANY='" + _obj.Comp.ToString() + "'");
        }


        protected void btnFaturaOlustur_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cmbVendCust.Text) | (string.IsNullOrEmpty(txtSeriNo.Text) & !eFatura) | string.IsNullOrEmpty(txtHariciBelgeNo.Text) | string.IsNullOrEmpty(txtBelgeTarihi.Text) |
                string.IsNullOrEmpty(txtDeftereNakilTarihi.Text) | string.IsNullOrEmpty(cmbDovizCinsi.Text) | string.IsNullOrEmpty(cmbFaturaVadesi.Text))
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnOnayListe", "msg004"));
                return;
            }
            if (!dosyaVarmi)
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnOnayListe", "msg005"));
                return;
            }

            decimal S1 = method.StringToDecimal(txtToplamTutartl.Text),
                S2 = method.StringToDecimal(txtToplamTutar.Text) * method.StringToDecimal(txtDovizKuru.Text);
            if (string.Format("{0:N2}", S1) != string.Format("{0:N2}", S2))
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnGiris", "msg007"));
                return;
            }


            btnKaydet_Click(null, null);

            string[] _params = { currentinvoiceNo, _obj.UserName.ToString() };
            string faturaNo = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "SatınalmaOnayFaturaOlustur§" + string.Join("§", _params), _obj.Comp);

            string dest = CurrentFolderName(_obj.Comp) + "/Dosya/SatinAlma_Onay/" + currentinvoiceNo + "10000.pdf";
            string folderPath = "";
            if (invoiceType == invoiceTypes.SATINALMA)
                folderPath = "SatinAlma";
            else
                folderPath = "IadeSatis";

            string Target = CurrentFolderName(_obj.Comp) + "/Dosya/" + folderPath + "/" + faturaNo + "10000.pdf";
            if (!File.Exists(Server.MapPath(Target)))
            {
                File.Copy(Server.MapPath(dest), Server.MapPath(Target));
            }

            MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "FaturaOnOnayListe", "msg001"));

            if (!ClientScript.IsStartupScriptRegistered("JSFaturaAc"))
            {
                ClientScript.RegisterStartupScript(GetType(), "JSFaturaAc", @"ReloadParentPage('" + currentinvoiceNo + "');", true);
            }
        }

        protected void btnOnayla_Click(object sender, EventArgs e)
        {
            string[] _params = { currentinvoiceNo, _obj.UserName.ToString(), "1", txtNot.Text, "", "0", limitY.ToString() };
            string message = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "SatınalmaOnayDurumDegistir§" + string.Join("§", _params), _obj.Comp);


            if (message != "1")
            {
                MessageBox(message);
            }

            if (!ClientScript.IsStartupScriptRegistered("JSFaturaAc"))
            {
                ClientScript.RegisterStartupScript(GetType(), "JSFaturaAc", @"ReloadParentPage('" + currentinvoiceNo + "');", true);
            }




        }

        protected void btnAciklamaKaydet_Click(object sender, EventArgs e)
        {
            if (txtAciklama.Text != string.Empty)
            {
                string[] _params = { currentinvoiceNo, _obj.UserName.ToString(), "2", txtAciklama.Text,
                   "", "0", limitY.ToString()};
                string message = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "SatınalmaOnayDurumDegistir§" + string.Join("§", _params), _obj.Comp);



                if (message != "1")
                {
                    MessageBox(message);
                }
                if (!ClientScript.IsStartupScriptRegistered("JSSaveA"))
                {
                    ClientScript.RegisterStartupScript(GetType(), "JSSaveA", @"<script type=""text/javascript""> "
                    + "window.parent.grid1.PerformCallback('ReloadWith');window.location.href = window.location.href;</script>");
                }
            }
        }
    }
}