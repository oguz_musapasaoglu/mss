﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.WSGeneric;
using System.ServiceModel;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DevExpress.Web;
using System.Web.UI.HtmlControls;
using System.IO;
using MSS1.Codes;

namespace MSS1
{
    public partial class newpage_documentEntrySelector : System.Web.UI.Page
    {
        #region Connection String & Service
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        
        public ws_baglantı ws = new ws_baglantı();
        SqlConnection baglanti;
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        string kontrol = string.Empty;
        DataTable DtFields = new DataTable();
        static SessionBase _obj;
        #endregion

        #region Servis Arama
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod(EnableSession = true)]
        public static List<string> SearchTaxarea(string prefixText, int count)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
            if (prefixText == "*")
            {
                string query = "Select Description as name from [0C_00318_00_TAX AREA] where" +
                    " COMPANY='" + _obj.Comp + "' order by Sıralama asc";
                SqlCommand cmd = new SqlCommand(query, conn);
                conn.Open();
                List<string> bul = new List<string>();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        bul.Add(sdr["name"].ToString());
                    }
                }
                conn.Close();

                return bul;
            }
            else
            {
                string query = "Select Description as name from [0C_00318_00_TAX AREA] where" +
                    " COMPANY='" + _obj.Comp + "' AND Description like '" + prefixText + "%'  order by Sıralama asc";
                SqlCommand cmd = new SqlCommand(query, conn);
                conn.Open();
                List<string> bul = new List<string>();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        bul.Add(sdr["name"].ToString());
                    }
                }
                conn.Close();

                return bul;
            }

        }
        #endregion

        #region Dil Getir
        public void dilgetir()
        {
            Language l = new Language();
            lblsayfaadi.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntrySelector", "lblsayfaadi");
            lblPersonel.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntrySelector", "lblPersonel");
            BtnGoTo.Value = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntrySelector", "BtnGoTo");

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }

            if (IsPostBack || IsCallback) return;
            dilgetir();

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }
    }
}