﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DynPopup.aspx.cs" Inherits="MSS1.Popup.DynPopup" %>

<%@ Register Src="~/controls/DataTableControlGAC.ascx" TagName="StDataTable" TagPrefix="STDT" %>
<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../css/GAC_MenuStyle.css?v=47" rel="stylesheet" />
    <link href="../css/GAC_NewStyle.css?v=47" rel="stylesheet" />
    <link href="../css/GAC_CardStyle.css?v=47" rel="stylesheet" />
    <style>
        .dxflGroup {
            padding: 0px 5px;
            width: 100%;
        }

            .dxflGroup tr:first-child > .dxflGroupCell > .dxflGroupBox, .dxflGroup > .dxflChildInFirstRowSys > .dxflGroupCell > .dxflGroupBox {
                margin-top: -5px;
            }

        .dx-vam {
            color: white;
        }
    </style>

    <script type="text/javascript" src="../js/popuputilities.js"></script>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%">
            <asp:Panel runat="server" ID="Pnl1" Visible="false">
                <table style="width: 500px; border-spacing: 0; border-collapse: collapse;">
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblTarih" runat="server" Text="TARİH SEÇ" CssClass="captionStyle"></asp:Label></td>
                        <td style="width: 400px">
                            <dx:ASPxDateEdit ID="txtBelgeTarihi" ClientInstanceName="txtBelgeTarihi" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt" EditFormatString="dd.MM.yyyy" DisplayFormatString="dd.MM.yyyy" runat="server" CssClass="DropDownList" Width="100%">
                            </dx:ASPxDateEdit>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblDovizCek" runat="server" Text="DÖVİZ" CssClass="captionStyle"></asp:Label></td>
                        <td style="width: 400px">
                            <dx:ASPxComboBox ID="cmbDovizCinsi" runat="server" CssClass="dropdownStyle" Width="100%" DataSourceID="DSDyn"
                                ValueField="Code" TextField="Text" ClientInstanceName="cmbDovizCinsi" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt"
                                AutoPostBack="false">
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblDovizKurSec" runat="server" Text="DÖVİZ KUR" CssClass="captionStyle"></asp:Label></td>
                        <td style="width: 400px">
                            <dx:ASPxSpinEdit ID="txtDovizKuru" ClientInstanceName="txtDovizKuru" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt" runat="server" CssClass="textboxStyle" Width="100%" SpinButtons-ClientVisible="false"
                                NumberType="Float" HorizontalAlign="Right">
                            </dx:ASPxSpinEdit>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Button runat="server" ID="btnCreateInv" Text="FATURA OLUŞTUR" CssClass="formglobalbuttonStyleFAT" OnClick="btnCreateInv_Click" /></td>
                        <td style="width: 400px"></td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel runat="server" ID="Pnl2" Visible="false">
                <table style="width: 500px; border-spacing: 0; border-collapse: collapse;">

                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblFaturaSec" runat="server" Text="FATURA SEÇ" CssClass="captionStyle"></asp:Label></td>
                        <td style="width: 400px">
                            <dx:ASPxComboBox ID="cmbChoosen" runat="server" CssClass="dropdownStyle" Width="100%" DataSourceID="DSDyn"
                                ValueField="No_" TextField="Text" ClientInstanceName="cmbChoosen" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt"
                                AutoPostBack="false">
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Button runat="server" ID="btnCreateInv2" Text="FATURA OLUŞTUR" CssClass="formglobalbuttonStyleFAT" OnClick="btnCreateInv_Click" /></td>
                        <td style="width: 400px"></td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel runat="server" ID="Pnl3" Visible="false">
                <table style="width: 500px; border-spacing: 0; border-collapse: collapse;">

                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblTaskSec" runat="server" Text="SELECT TASK" CssClass="captionStyle"></asp:Label></td>
                        <td style="width: 400px">
                            <dx:ASPxComboBox ID="cmbTask" runat="server" CssClass="dropdownStyle" Width="100%" DataSourceID="DSDyn"
                                ValueField="Code" TextField="Text" ClientInstanceName="cmbTask" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt"
                                AutoPostBack="false">
                                <ClientSideEvents SelectedIndexChanged="function(s,e){cmbBL.PerformCallback(s.GetValue());CmbDocument.PerformCallback(s.GetValue());}" />
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr runat="server" id="trBl">
                        <td style="width: 100px">
                            <asp:Label ID="Label12" runat="server" Text="SELECT BL" CssClass="captionStyle"></asp:Label></td>
                        <td style="width: 400px">
                            <dx:ASPxComboBox ID="cmbBL" runat="server" CssClass="dropdownStyle" Width="100%" DataSourceID="DSDyn3"
                                ValueField="Code" TextField="Text" EnableCallbackMode="true" CallbackPageSize="15" ClientInstanceName="cmbBL" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt"
                                OnCallback="cmbBL_Callback">
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblDocumentSec" runat="server" Text="SELECT DOCUMENT" CssClass="captionStyle"></asp:Label></td>
                        <td style="width: 400px">
                            <dx:ASPxComboBox ID="CmbDocument" runat="server" CssClass="dropdownStyle" Width="100%" DataSourceID="DSDyn2"
                                ValueField="Code" TextField="Text" EnableCallbackMode="true" CallbackPageSize="15" ClientInstanceName="CmbDocument" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt"
                                AutoPostBack="true" OnSelectedIndexChanged="CmbDocument_SelectedIndexChanged" OnCallback="CmbDocument_Callback">
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="Label10" runat="server" Text="LANGUAGE" CssClass="captionStyle"></asp:Label></td>
                        <td style="width: 400px">
                            <dx:ASPxComboBox ID="cmbLang" runat="server" CssClass="dropdownStyle" Width="100%"
                                ValueField="Code" TextField="Text" ClientInstanceName="cmbLang" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt">
                                <Items>
                                    <dx:ListEditItem Value="0" Text="" />
                                    <dx:ListEditItem Value="1" Text="TR" Selected="true" />
                                    <dx:ListEditItem Value="2" Text="EN" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="Label11" runat="server" Text="NOTES" CssClass="captionStyle"></asp:Label></td>
                        <td style="width: 400px">
                            <dx:ASPxMemo ID="txtNotes" runat="server" Theme="Glass" Width="100%" Height="60px">
                            </dx:ASPxMemo>
                        </td>
                    </tr>
                    <tr runat="server" id="trATA" visible="false">
                        <td style="width: 100px">
                            <asp:Label ID="Label1" runat="server" Text="ETA" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxDateEdit ID="txtATA" runat="server" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt" Theme="Glass" EditFormatString="dd.MM.yyyy" DisplayFormatString="dd.MM.yyyy"></dx:ASPxDateEdit>
                        </td>
                    </tr>
                    <tr runat="server" id="trATD" visible="false">
                        <td style="width: 100px">
                            <asp:Label ID="Label2" runat="server" Text="ATD" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxDateEdit ID="txtATD" runat="server" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt" Theme="Glass" EditFormatString="dd.MM.yyyy" DisplayFormatString="dd.MM.yyyy"></dx:ASPxDateEdit>
                        </td>
                    </tr>
                    <tr runat="server" id="trETA" visible="false">
                        <td style="width: 100px">
                            <asp:Label ID="Label3" runat="server" Text="ETA" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxDateEdit ID="txtETA" runat="server" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt" Theme="Glass" EditFormatString="dd.MM.yyyy" DisplayFormatString="dd.MM.yyyy"></dx:ASPxDateEdit>
                        </td>
                    </tr>
                    <tr runat="server" id="trETD" visible="false">
                        <td style="width: 100px">
                            <asp:Label ID="Label4" runat="server" Text="ETD" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxDateEdit ID="txtETD" runat="server" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt" Theme="Glass" EditFormatString="dd.MM.yyyy" DisplayFormatString="dd.MM.yyyy"></dx:ASPxDateEdit>
                        </td>
                    </tr>
                    <tr runat="server" id="trInst__Wh__Cut_off_Time" visible="false">
                        <td style="width: 100px">
                            <asp:Label ID="Label5" runat="server" Text="Inst Wh Cut off Time" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxTextBox ID="txtInst__Wh__Cut_off_Time" runat="server" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt" Theme="Glass"></dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr runat="server" id="trInst_Wh_Cut_off_Date" visible="false">
                        <td style="width: 100px">
                            <asp:Label ID="Label6" runat="server" Text="Inst Wh Cut off Date" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxDateEdit ID="txtInst_Wh_Cut_off_Date" runat="server" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt" Theme="Glass" EditFormatString="dd.MM.yyyy" DisplayFormatString="dd.MM.yyyy"></dx:ASPxDateEdit>
                        </td>
                    </tr>
                    <tr runat="server" id="trLCNumber" visible="false">
                        <td style="width: 100px">
                            <asp:Label ID="Label7" runat="server" Text="LC Number" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxTextBox ID="txtLCNumber" runat="server" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt" Theme="Glass"></dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr runat="server" id="trOrdinoAdress" visible="false">
                        <td style="width: 100px">
                            <asp:Label ID="Label8" runat="server" Text="Ordino Adress" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxComboBox ID="cmbOrdinoAdress" runat="server" CssClass="dropdownStyle" Width="100%" DataSourceID="DSDyn4"
                                ValueField="Code" TextField="Code" EnableCallbackMode="true" CallbackPageSize="15" ClientInstanceName="cmbOrdinoAdress" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt">
                            </dx:ASPxComboBox>
                            <%-- <dx:ASPxTextBox ID="txtOrdinoAdress" runat="server" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt" Theme="Glass"></dx:ASPxTextBox>--%>
                        </td>
                    </tr>
                    <tr runat="server" id="trSiraNo" visible="false">
                        <td style="width: 100px">
                            <asp:Label ID="Label14" runat="server" Text="Sıra" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxComboBox ID="cmbOrder" runat="server" CssClass="dropdownStyle" Width="100%"
                                ClientInstanceName="cmbOrder" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt">
                                <Items>
                                    <dx:ListEditItem Selected="true" Value="1" Text="1" />
                                    <dx:ListEditItem Value="2" Text="2" />
                                </Items>
                            </dx:ASPxComboBox>
                            <%-- <dx:ASPxTextBox ID="txtOrdinoAdress" runat="server" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt" Theme="Glass"></dx:ASPxTextBox>--%>
                        </td>
                    </tr>
                    <tr runat="server" id="tropFoy" visible="false">
                        <td style="width: 100px">
                            <asp:Label ID="Label18" runat="server" Text="Föy Seç" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxComboBox ID="cmbopFoy" runat="server" CssClass="dropdownStyle" Width="100%"
                                ClientInstanceName="cmbopFoy" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt">
                                <Items>
                                    <dx:ListEditItem Value="0" Selected="true" Text=" " />
                                    <dx:ListEditItem Value="1" Text="FÖY 1" />
                                    <dx:ListEditItem Value="2" Text="FÖY 2" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr runat="server" id="trAmbar" visible="false">
                        <td style="width: 100px">
                            <asp:Label ID="Label15" runat="server" Text="Ambar" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxTextBox ID="txtAmbar" runat="server" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt" Theme="Glass"></dx:ASPxTextBox>
                        </td>
                    </tr>

                    <tr runat="server" id="trQuantity" visible="false">
                        <td style="width: 100px">
                            <asp:Label ID="Label9" runat="server" Text="Quantity" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxTextBox ID="txtQuantity" runat="server" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt" Theme="Glass"></dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr runat="server" id="trFile" visible="false">
                        <td style="width: 100px">
                            <asp:Label ID="Label13" runat="server" Text="SELECT FILE" CssClass="captionStyle"></asp:Label></td>
                        <td style="width: 400px; text-align: right">
                            <asp:FileUpload ID="FileUpload1" runat="server" />
                        </td>
                    </tr>
                    <tr runat="server" id="trAlici" visible="false">
                        <td style="width: 100px">
                            <asp:Label ID="Label19" runat="server" Text="Alıcı" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                             <dx:ASPxComboBox ID="cmbAlici" runat="server" CssClass="dropdownStyle" Width="100%" 
                                 ClientInstanceName="cmbAlici" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt"
                                AutoPostBack="false">
                                 <Items>
                                     <dx:ListEditItem Text="" Value="" Selected="true" />
                                     <dx:ListEditItem Text="CONSIGNEE" Value="CONSIGNEE" />
                                     <dx:ListEditItem Text="NOTIFY" Value="NOTIFY" />
                                     <dx:ListEditItem Text="MANUEL" Value="MANUEL" />
                                 </Items>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr runat="server" id="trDamgaVergisi" visible="false">
                        <td style="width: 100px">
                            <asp:Label ID="Label20" runat="server" Text="Damga Vergisi" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxTextBox ID="txtDamgaVergisi" runat="server" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt" Theme="Glass"></dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr runat="server" id="trDefterSiraNo" visible="false">
                        <td style="width: 100px">
                            <asp:Label ID="Label21" runat="server" Text="Defter Sıra No" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxTextBox ID="txtDefterSiraNo" runat="server" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt" Theme="Glass"></dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr runat="server" id="trManuelAlici" visible="false">
                        <td style="width: 100px">
                            <asp:Label ID="Label22" runat="server" Text="Manuel Alıcı" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxTextBox ID="txtManuelAlici" runat="server" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt" Theme="Glass"></dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px"></td>
                        <td style="width: 400px; text-align: right">
                            <asp:Button runat="server" ID="btnCreateDocument" Text="CREATE DOCUMENT" CssClass="formglobalbuttonStyleFAT" OnClick="btnCreateInv_Click" /></td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel runat="server" ID="Pnl4" Visible="false">
                <table style="width: 500px; border-spacing: 0; border-collapse: collapse;">

                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblTransactionType" runat="server" Text="TRANSACTION TYPE" CssClass="captionStyle"></asp:Label></td>
                        <td style="width: 400px">
                            <dx:ASPxComboBox ID="cmbTransactionType" runat="server" CssClass="dropdownStyle" Width="100%" DataSourceID="DSDyn"
                                ValueField="Code" TextField="Text" ClientInstanceName="cmbTransactionType" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt"
                                AutoPostBack="false">
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr1">
                        <td style="width: 100px">
                            <asp:Label ID="lblStartingDate" runat="server" Text="STARTING DATE" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxDateEdit ID="txtStartingDate" runat="server" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt" Theme="Glass" EditFormatString="dd.MM.yyyy" DisplayFormatString="dd.MM.yyyy"></dx:ASPxDateEdit>
                        </td>
                    </tr>
                    <tr runat="server" id="tr2">
                        <td style="width: 100px">
                            <asp:Label ID="lblEndingDate" runat="server" Text="ENDING DATE" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxDateEdit ID="txtEndDate" runat="server" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt" Theme="Glass" EditFormatString="dd.MM.yyyy" DisplayFormatString="dd.MM.yyyy"></dx:ASPxDateEdit>
                        </td>
                    </tr>
                    <tr runat="server" id="tr3">
                        <td style="width: 100px">
                            <asp:Label ID="lblFrom" runat="server" Text="FROM PORT" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxComboBox ID="cmbFromPort" runat="server" CssClass="dropdownStyle" Width="100%"
                                ValueField="R0011" TextField="customText" OnItemRequestedByValue="Cmb_ItemRequestedByValue" OnItemsRequestedByFilterCondition="Cmb_ItemsRequestedByFilterCondition" ClientInstanceName="cmbFromPort" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt"
                                AutoPostBack="false">
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr4">
                        <td style="width: 100px">
                            <asp:Label ID="lblFromPlace" runat="server" Text="FROM PLACE" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxComboBox ID="cmbFromPlace" runat="server" CssClass="dropdownStyle" Width="100%"
                                ValueField="R0011" TextField="customText" ClientInstanceName="cmbFromPlace" OnItemRequestedByValue="Cmb_ItemRequestedByValue" OnItemsRequestedByFilterCondition="Cmb_ItemsRequestedByFilterCondition" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt"
                                EnableCallbackMode="true" CallbackPageSize="15" IncrementalFilteringMode="Contains"
                                AutoPostBack="false">
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr5">
                        <td style="width: 100px">
                            <asp:Label ID="lblToPort" runat="server" Text="TO PORT" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxComboBox ID="cmbToPort" runat="server" CssClass="dropdownStyle" Width="100%"
                                ValueField="R0011" TextField="customText" ClientInstanceName="cmbToPort" OnItemRequestedByValue="Cmb_ItemRequestedByValue" OnItemsRequestedByFilterCondition="Cmb_ItemsRequestedByFilterCondition" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt"
                                EnableCallbackMode="true" CallbackPageSize="15" IncrementalFilteringMode="Contains"
                                AutoPostBack="false">
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr6">
                        <td style="width: 100px">
                            <asp:Label ID="lblToPlace" runat="server" Text="TO PLACE" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxComboBox ID="cmbToPlace" runat="server" CssClass="dropdownStyle" Width="100%"
                                ValueField="R0011" TextField="customText" ClientInstanceName="cmbToPlace" OnItemRequestedByValue="Cmb_ItemRequestedByValue" OnItemsRequestedByFilterCondition="Cmb_ItemsRequestedByFilterCondition" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt"
                                EnableCallbackMode="true" CallbackPageSize="15" IncrementalFilteringMode="Contains"
                                AutoPostBack="false">
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr7">
                        <td style="width: 100px">
                            <asp:Label ID="lblActionPlaceType" runat="server" Text="ACTION TYPE" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxComboBox ID="cmbActionType" runat="server" CssClass="dropdownStyle" Width="100%"
                                ClientInstanceName="cmbActionType" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt"
                                AutoPostBack="false">
                                <Items>
                                    <dx:ListEditItem Value="0" Text="" />
                                    <dx:ListEditItem Value="1" Text="CUSTOMER" />
                                    <dx:ListEditItem Value="2" Text="PORT" />
                                    <dx:ListEditItem Value="3" Text="DEPOT" />
                                    <dx:ListEditItem Value="4" Text="VENDOR" />
                                </Items>
                                <ClientSideEvents SelectedIndexChanged="function(s,e){cmbActionPlace.PerformCallback(s.GetValue());}" />
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr8">
                        <td style="width: 100px">
                            <asp:Label ID="lblActionPlace" runat="server" Text="ACTION PLACE" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxComboBox ID="cmbActionPlace" runat="server" CssClass="dropdownStyle" Width="100%"
                                ValueField="R0011" TextField="customText" ClientInstanceName="cmbActionPlace"
                                EnableCallbackMode="true" CallbackPageSize="15" IncrementalFilteringMode="Contains"
                                OnItemRequestedByValue="Cmb_ItemRequestedByValueCasc" OnItemsRequestedByFilterCondition="Cmb_ItemsRequestedByFilterConditionCasc"
                                OnCallback="cmbActionPlace_Callback" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt"
                                AutoPostBack="false">
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr runat="server" id="tr12">
                        <td style="width: 100px">
                            <asp:Label ID="lblDeliveryDate" runat="server" Text="DELIVERY DATE" CssClass="captionStyle"></asp:Label>
                        </td>
                        <td style="width: 400px">
                            <dx:ASPxDateEdit ID="txtDeliveryDate" runat="server" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt" Theme="Glass" EditFormatString="dd.MM.yyyy" DisplayFormatString="dd.MM.yyyy"></dx:ASPxDateEdit>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px"></td>
                        <td style="width: 400px; text-align: right">
                            <asp:Button runat="server" ID="btnAddMovement" Text="ADD TO MOVEMENT" CssClass="formglobalbuttonStyleFAT" OnClick="btnCreateInv_Click" /></td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel runat="server" ID="Pnl5" Visible="false">
                <table style="width: 500px; border-spacing: 0; border-collapse: collapse;">
                    <tr>
                        <td style="width: 100%">
                            <asp:Label ID="lblApprovalCaption" runat="server" Text="Faturalar Onaylanıyor..." CssClass="captionStyle"></asp:Label></td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel runat="server" ID="Pnl6" Visible="false">
                <table style="width: 500px; border-spacing: 0; border-collapse: collapse;">
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="Label16" runat="server" Text="SELECT VOYAGE" CssClass="captionStyle"></asp:Label></td>
                        <td style="width: 400px">
                            <dx:ASPxComboBox ID="CmbVoyage" runat="server" CssClass="dropdownStyle" Width="100%" DataSourceID="DSDyn2"
                                ValueField="R0012" TextField="R0019" EnableCallbackMode="true" CallbackPageSize="15"
                                Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt" ClientInstanceName="CmbVoyage">
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px"></td>
                        <td style="width: 400px; text-align: right">
                            <asp:Button runat="server" ID="btnCreateInv3" Text="CREATE DOCUMENT" CssClass="formglobalbuttonStyleFAT" OnClick="btnCreateInv_Click" /></td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel runat="server" ID="Pnl7" Visible="false">
                <table style="width: 500px; border-spacing: 0; border-collapse: collapse;">

                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="Label17" runat="server" Text="MÜŞTERİ SEÇ" CssClass="captionStyle"></asp:Label></td>
                        <td style="width: 400px">
                            <dx:ASPxComboBox ID="cmbCustomer" runat="server" CssClass="dropdownStyle" Width="100%"
                                ValueField="No_" TextField="customText" ClientInstanceName="cmbCustomer" OnItemRequestedByValue="Cmb_ItemRequestedByValue" OnItemsRequestedByFilterCondition="Cmb_ItemsRequestedByFilterCondition" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt"
                                EnableCallbackMode="true" CallbackPageSize="15" IncrementalFilteringMode="Contains"
                                AutoPostBack="false">
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px"></td>
                        <td style="width: 400px; text-align: right">
                            <asp:Button runat="server" ID="btnCreateShipMent" Text="CREATE SHIPMENT" CssClass="formglobalbuttonStyleFAT" OnClick="btnCreateInv_Click" /></td>
                    </tr>
                </table>
            </asp:Panel>
            <div style="display: none">
                <asp:SqlDataSource ID="DSDyn" runat="server" ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
                <asp:SqlDataSource ID="DSDyn2" runat="server" ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
                <asp:SqlDataSource ID="DSDyn3" runat="server" ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
                <asp:SqlDataSource ID="DSDyn4" runat="server" ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
                <asp:TextBox ID="txtMusts" runat="server" Text=""></asp:TextBox>
                <asp:TextBox ID="txtWithoutBL" runat="server" Text=""></asp:TextBox>
            </div>
        </div>
    </form>
</body>
</html>

