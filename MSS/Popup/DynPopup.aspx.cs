﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.WSGeneric;
using System.Web.UI.HtmlControls;
using MSS1.Codes;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace MSS1.Popup
{
    public partial class DynPopup : Bases
    {
        string ConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        public ws_baglantı ws = new ws_baglantı();
        public Methods method = new Methods();
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        SessionBase _obj;

        #region Page Params
        private enum Types
        {
            NONE, SALESINV, PURCHMEMO, PURCHINV, LOJCREATEDOC, LOJATTACHDOC, CONTAINERMOVEMENT
            , INVAPPROVE, RPF22, LOJATTACHDOCMLH, CREATESHIPMENTJOB, EMANIFEST, LOADINGlST
        };
        private Types _Type
        {
            get
            {
                Types _pageType = Types.NONE;

                if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    if (Request.QueryString["Type"].ToString() == "SALESINV")
                        _pageType = Types.SALESINV;
                    else if (Request.QueryString["Type"].ToString() == "PURCHMEMO")
                        _pageType = Types.PURCHMEMO;
                    else if (Request.QueryString["Type"].ToString() == "PURCHINV")
                        _pageType = Types.PURCHINV;
                    else if (Request.QueryString["Type"].ToString() == "LOJCREATEDOC")
                        _pageType = Types.LOJCREATEDOC;
                    else if (Request.QueryString["Type"].ToString() == "LOJATTACHDOC")
                        _pageType = Types.LOJATTACHDOC;
                    else if (Request.QueryString["Type"].ToString() == "LOJATTACHDOCMLH")
                        _pageType = Types.LOJATTACHDOCMLH;
                    else if (Request.QueryString["Type"].ToString() == "CONTAINERMOVEMENT")
                        _pageType = Types.CONTAINERMOVEMENT;
                    else if (Request.QueryString["Type"].ToString() == "INVAPPROVE")
                        _pageType = Types.INVAPPROVE;
                    else if (Request.QueryString["Type"].ToString() == "RPF22")
                        _pageType = Types.RPF22;
                    else if (Request.QueryString["Type"].ToString() == "CREATESHIPMENTJOB")
                        _pageType = Types.CREATESHIPMENTJOB;
                    else if (Request.QueryString["Type"].ToString() == "EMANIFEST")
                        _pageType = Types.EMANIFEST;
                    else if (Request.QueryString["Type"].ToString() == "LOADINGlST")
                        _pageType = Types.LOADINGlST;
                    else
                        _pageType = Types.NONE;
                }
                return _pageType;
            }
        }
        private string JobNo
        {
            get
            {
                return Request.QueryString["JobNo"].ToString();
            }
        }

        private string VoyageNo
        {
            get
            {
                return Request.QueryString["VoyageNo"].ToString();
            }
        }

        private string Company
        {
            get
            {
                return Request.QueryString["Company"].ToString();
            }
        }
        private string Typedesc
        {
            get
            {
                return Request.QueryString["Type"].ToString();
            }
        }
        private string IslemId
        {
            get
            {
                return Request.QueryString["IslemId"].ToString();
            }
        }
        private string sirket
        {
            get
            {
                return _obj.Comp;
            }
        }

        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }



        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }

            if (IsPostBack || IsCallback) return;

            DilGetir();
            PreparePageFields();


        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }

        private void DilGetir()
        {

            if (_Type == Types.CONTAINERMOVEMENT)
            {
                btnCreateDocument.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "btnCreateDocument");
                lblTransactionType.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "lblTransactionType");
                lblStartingDate.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "lblStartingDate");
                lblEndingDate.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "lblEndingDate");
                lblFrom.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "lblFrom");
                lblFromPlace.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "lblFromPlace");
                lblToPort.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "lblToPort");
                lblToPlace.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "lblToPlace");
                lblActionPlaceType.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "lblActionPlaceType");
                lblActionPlace.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "lblActionPlace");
                lblDeliveryDate.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "lblDeliveryDate");
                btnAddMovement.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "btnAddMovement");
            }
            else
            {
                lblTarih.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "lblTarih");
                lblDovizCek.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "lblDovizCek");
                lblDovizKurSec.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "lblDovizKurSec");
                btnCreateInv.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "btnCreateInv");
                lblFaturaSec.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "lblFaturaSec");
                btnCreateInv2.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "btnCreateInv2");
                lblTaskSec.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "lblTaskSec");
                Label12.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "Label12");
                lblDocumentSec.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "lblDocumentSec");
                Label10.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "Label10");
                Label11.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "Label11");
                Label1.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "Label1");
                Label2.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "Label2");
                Label3.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "Label3");
                Label4.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "Label4");
                Label5.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "Label5");
                Label6.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "Label6");
                Label7.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "Label7");
                Label8.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "Label8");
                Label14.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "Label14");
                Label9.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "Label9");
                Label13.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "DynPopup", "Label13");
            }

        }

        private void PreparePageFields()
        {

            switch (_Type)
            {
                case Types.SALESINV:
                case Types.PURCHMEMO:
                    Pnl1.Visible = true;
                    FillSources();
                    txtBelgeTarihi.Date = DateTime.Now;
                    string Dovizcinsi = GetWebTopluIslemDovizCinsi();
                    cmbDovizCinsi.Value = Dovizcinsi;
                    txtDovizKuru.Text = f.returnKur(string.Format("{0:dd.MM.yyyy}", DateTime.Now), Dovizcinsi, _obj.Comp);
                    cmbDovizCinsi.ReadOnly = true;
                    break;
                case Types.CONTAINERMOVEMENT:
                    Pnl4.Visible = true;
                    FillSources();
                    break;
                case Types.PURCHINV:
                    Pnl2.Visible = true;
                    FillSources();
                    break;
                case Types.LOJCREATEDOC:
                case Types.LOJATTACHDOC:
                    if (_Type == Types.LOJATTACHDOC)
                    {
                        if (Request.Url.AbsoluteUri.Contains("ENo="))
                        {
                            Label13.Visible = false;
                            FileUpload1.Visible = false;
                        }
                        SetComponentVisibility("trFile", true);
                        btnCreateDocument.Text = "ATTACH DOCUMENT";
                    }
                    else
                        btnCreateDocument.Text = "CREATE DOCUMENT";
                    Pnl3.Visible = true;
                    FillSources();
                    break;
                case Types.LOJATTACHDOCMLH:
                    SetComponentVisibility("trFile", true);
                    btnCreateDocument.Text = "ATTACH DOCUMENT";
                    Pnl3.Visible = true;
                    cmbTask.ClientSideEvents.SelectedIndexChanged = "function(s,e){CmbDocument.PerformCallback(s.GetValue());}";
                    FillSources();
                    trBl.Visible = false;
                    break;
                case Types.INVAPPROVE:
                    Pnl5.Visible = true;
                    InvApprove();
                    break;
                case Types.RPF22:
                    Pnl6.Visible = true;
                    FillSources();
                    break;
                case Types.CREATESHIPMENTJOB:
                case Types.EMANIFEST:
                case Types.LOADINGlST:
                    Pnl7.Visible = true;
                    FillSources();
                    break;
            }

        }

        void InvApprove()
        {
            string _result = GetWebFonksiyonlari(sirket).ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatinalmaOnayTopluOnaySELECTED§" + IslemId + "§" + _obj.UserName, _obj.Comp);

            if (_result.ToLower().Contains("ok"))
            {

                string sorgu = "select [Primary Key 1],[Additional Field 1] from " + _obj.Comp + ".dbo.[Web Toplu Islem] where [Islem ID]='" + IslemId + "' and [Additional Field 1]!='' ";
                using (DataTable dt = f.datatablegonder(sorgu))
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        OnOnayFileCreate(row["Primary Key 1"].ToString(), row["Additional Field 1"].ToString());
                    }
                }

                ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: proceed3('" + Request.QueryString["ObjectId"] + "');", true);
            }
            else
            {
                lblApprovalCaption.Text = _result;
            }
        }

        private void OnOnayFileCreate(string _FatNo, string _AfNo)
        {
            if (_AfNo.Length < 3) return;

            string reportPath = f.GetReportPath("ALIM ONAY TARIHCE.rpt", _obj.Comp);
            using (ReportDocument crystalReport = new ReportDocument())
            {
                crystalReport.Load(reportPath);
                crystalReport.ReportOptions.EnableSaveDataWithReport = false;
                crystalReport.SetParameterValue("@COMP", _obj.Comp);

                crystalReport.SetParameterValue("@FATURA", _FatNo);

                string folder = string.Empty;
                if (_AfNo.Substring(0, 2).ToLower() == "af")
                {
                    folder = "SatinAlma";
                }
                else
                {
                    folder = "IadeSatis";
                }

                string foldPath = "~/" + CurrentFolderName(_obj.Comp) + "/Dosya/" + folder + "/" + _AfNo + "10000.pdf";
                if (File.Exists(Server.MapPath(foldPath)))
                {
                    File.Delete(Server.MapPath(foldPath));
                }

                crystalReport.ExportToDisk(ExportFormatType.PortableDocFormat, Server.MapPath(foldPath));


            }

        }


        private string GetWebTopluIslemDovizCinsi()
        {
            string sorgu = "select top 1 [Primary Key 2] from [0T_70013_00_WEB_TOPLU_ISLEM] where [CompanyCode]='" + _obj.Comp + "' and [Islem ID]='" + IslemId + "' ";
            return f.tekbirsonucdonder(sorgu);

        }

        private string GetWebTopluIslemVendorAndCurrency()
        {
            string sorgu = "select top 1 [Primary Key 1]+'|'+[Primary Key 2] from " + _obj.Comp + ".dbo.[Web Toplu Islem] where [Islem ID]='" + IslemId + "' ";
            return f.tekbirsonucdonder(sorgu);

        }

        private void FillSources()
        {
            string sorgu = string.Empty;
            switch (_Type)
            {
                case Types.SALESINV:
                case Types.PURCHMEMO:
                    sorgu = "select KOD Code, ACIKLAMA Text From [0_W10400] WHERE COMPANY='" + _obj.Comp + "'";
                    cmbDovizCinsi.Items.Clear();
                    DSDyn.SelectCommand = sorgu;
                    cmbDovizCinsi.DataBind();
                    break;
                case Types.CONTAINERMOVEMENT:
                    sorgu = "select R0011 Code, R0012 Text From [0_W10185] WHERE RCOMP='" + _obj.Comp + "'";
                    cmbTransactionType.Items.Clear();
                    DSDyn.SelectCommand = sorgu;
                    cmbTransactionType.DataBind();
                    cmbFromPort.NullText = "0_W10020#RCOMP#R0011#R0012";
                    cmbFromPort.NullTextStyle.ForeColor = System.Drawing.Color.White;
                    cmbFromPlace.NullText = "0_W10042#RCOMP#R0011#R0012";
                    cmbFromPlace.NullTextStyle.ForeColor = System.Drawing.Color.White;
                    cmbToPort.NullText = "0_W10020#RCOMP#R0011#R0012";
                    cmbToPort.NullTextStyle.ForeColor = System.Drawing.Color.White;
                    cmbToPlace.NullText = "0_W10042#RCOMP#R0011#R0012";
                    cmbToPlace.NullTextStyle.ForeColor = System.Drawing.Color.White;
                    cmbActionPlace.NullText = "CascColumn1#0_W00243#RCOMP#R0011#R0013#RTYPE#OWN";
                    cmbActionPlace.NullTextStyle.ForeColor = System.Drawing.Color.White;
                    break;
                case Types.PURCHINV:
                    string vendandcur = GetWebTopluIslemVendorAndCurrency();
                    string curcode = vendandcur.Split('|')[1];
                    string vendcode = vendandcur.Split('|')[0];
                    if (curcode == "TL") curcode = "";
                    sorgu = "EXEC SP_0_W12142 '" + curcode + "','" + vendcode + "','" + sirket + "'";
                    cmbChoosen.Items.Clear();
                    DSDyn.SelectCommand = sorgu;
                    cmbChoosen.DataBind();
                    break;
                case Types.LOJCREATEDOC:
                case Types.LOJATTACHDOC:
                    sorgu = "select R0012 Code, R0013 [Text] FROM [dbo].[0_W12205] where R0011='" + JobNo + "' and RCOMP='" + Company + "'";
                    if (Request.QueryString["TaskNo"] != null)
                        sorgu += " and R0012 like '" + Request.QueryString["TaskNo"].ToString() + "%'";
                    cmbTask.Items.Clear();
                    DSDyn.SelectCommand = sorgu;
                    cmbTask.DataBind();
                    if (Request.QueryString["TaskNo"] != null)
                    {
                        cmbTask.SelectedIndex = 0;
                        cmbTask.ReadOnly = true;

                        DSDyn3.SelectCommand = "SELECT R0015+'|'+CAST(R0013 as varchar) Code, R0015 [Text] FROM [0_W12165] where RCOMP='" + sirket + "' and R0012='" + Request.QueryString["TaskNo"].ToString() + "'";
                        cmbBL.Items.Clear();
                        cmbBL.DataBind();

                        if (cmbBL.Items.Count == 1)
                        {
                            cmbBL.SelectedIndex = 0;
                            cmbBL.ReadOnly = true;
                        }

                        switch (_Type)
                        {
                            case Types.LOJCREATEDOC:
                                DSDyn2.SelectCommand = "SELECT R0013 Code, R0014 [Text] FROM [0_W12173] where R0016=1 and RCOMP='" + sirket + "' and R0011='" + cmbTask.Value.ToString().Split('|')[1] + "'";
                                break;
                            case Types.LOJATTACHDOC:
                                DSDyn2.SelectCommand = "SELECT R0013 Code, R0014 [Text] FROM [0_W12173] where R0016=2 and RCOMP='" + sirket + "' and R0011='" + cmbTask.Value.ToString().Split('|')[1] + "'";
                                break;
                        }
                        CmbDocument.Items.Clear();
                        CmbDocument.DataBind();

                    }

                    sorgu = "SELECT [COMPANY],[Code] FROM [0C_00222_00_SHIP_TO_ADDRESS] where COMPANY='" + Company + "'";
                    cmbOrdinoAdress.Items.Clear();
                    DSDyn4.SelectCommand = sorgu;
                    cmbOrdinoAdress.DataBind();
                    break;
                case Types.LOJATTACHDOCMLH:
                    sorgu = "select R0012 Code, R0013 [Text] FROM [dbo].[0_W74105] where R0011='" + JobNo + "' and RCOMP='" + Company + "'";
                    if (Request.QueryString["TaskNo"] != null)
                        sorgu += " and R0012 like '" + Request.QueryString["TaskNo"].ToString() + "%'";
                    cmbTask.Items.Clear();
                    DSDyn.SelectCommand = sorgu;
                    cmbTask.DataBind();
                    if (Request.QueryString["TaskNo"] != null)
                    {
                        cmbTask.SelectedIndex = 0;
                        cmbTask.ReadOnly = true;




                        DSDyn2.SelectCommand = "SELECT R0013 Code, R0014 [Text] FROM [0_W12173] where R0016=2 and RCOMP='" + sirket + "' and R0011='" + cmbTask.Value.ToString().Split('|')[1] + "'";


                        CmbDocument.Items.Clear();
                        CmbDocument.DataBind();

                    }

                    sorgu = "SELECT [COMPANY],[Code] FROM [0C_00222_00_SHIP_TO_ADDRESS] where COMPANY='" + Company + "'";
                    cmbOrdinoAdress.Items.Clear();
                    DSDyn4.SelectCommand = sorgu;
                    cmbOrdinoAdress.DataBind();
                    trBl.Visible = false;
                    break;
                case Types.RPF22:
                    sorgu = "Exec SP_0_W75596 '" + Company + "','" + JobNo + "'";
                    DSDyn2.SelectCommand = sorgu;
                    CmbVoyage.DataBind();
                    CmbVoyage.SelectedIndex = -1;
                    break;
                case Types.CREATESHIPMENTJOB:
                case Types.EMANIFEST:
                case Types.LOADINGlST:
                    cmbCustomer.NullText = "0_W50130#COMPANY#No_#Name";
                    cmbCustomer.NullTextStyle.ForeColor = System.Drawing.Color.White;
                    break;

            }



        }

        protected void Cmb_ItemRequestedByValue(object source, ListEditItemRequestedByValueEventArgs e)
        {
            try
            {
                if (e.Value == null) return;
                if (string.IsNullOrEmpty(e.Value.ToString())) return;
                ASPxComboBox cmb = (ASPxComboBox)source;
                cmb.CallbackPageSize = 15;
                cmb.Items.Clear();
                string[] _params = null;
                if (cmb.NullText.Contains("#"))
                {
                    _params = cmb.NullText.Split('#');
                }
                else return;

                var connstr = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                var sds = new SqlDataSource();
                sds.ConnectionString = connstr;
                sds.SelectCommand = DynamicUtilsV1.FillComboboxV2(_params[0], _params[1], _params[2], _params[3], _obj.Comp, _obj.UserName);
                sds.SelectParameters.Clear();
                sds.SelectParameters.Add("ID", e.Value.ToString());
                cmb.DataSource = sds;
                cmb.DataBind();
            }
            catch
            {


            }
        }

        protected void Cmb_ItemsRequestedByFilterCondition(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
        {
            ASPxComboBox cmb = (ASPxComboBox)source;
            cmb.CallbackPageSize = 15;


            string[] _params = null;
            if (cmb.NullText.Contains("#"))
            {
                _params = cmb.NullText.Split('#');
            }
            else return;

            // string sss = string.Empty;
            //throw new Exception(FillComboboxV1(_params[0], _params[1], _params[2], _params[3], HttpContext.Current._obj.Comp, HttpContext.Current._obj.UserName));
            try
            {
                var connstr = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                var sds = new SqlDataSource();
                sds.ConnectionString = connstr;
                cmb.Items.Clear();
                sds.SelectCommand = DynamicUtilsV1.FillComboboxV1(_params[0], _params[1], _params[2], _params[3], _obj.Comp, _obj.UserName);
                //sss = sds.SelectCommand;

                sds.SelectParameters.Clear();
                sds.SelectParameters.Add("filter", TypeCode.String, string.Format("%{0}%", e.Filter));
                sds.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
                sds.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

                cmb.DataSource = sds;
                cmb.DataBind();


            }
            catch (Exception ex)
            {
                //throw;
                //throw new Exception(ex.Message);
            }
        }

        protected void Cmb_ItemRequestedByValueCasc(object source, ListEditItemRequestedByValueEventArgs e)
        {
            try
            {
                if (e.Value == null) return;
                if (string.IsNullOrEmpty(e.Value.ToString())) return;
                ASPxComboBox cmb = (ASPxComboBox)source;
                cmb.CallbackPageSize = 15;
                cmb.Items.Clear();
                string[] _params = null;
                if (cmb.NullText.Contains("#"))
                {
                    _params = cmb.NullText.Split('#');
                }
                else return;

                var connstr = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                var sds = new SqlDataSource();
                sds.ConnectionString = connstr;
                sds.SelectCommand = DynamicUtilsV1.FillComboboxV2(_params[1], _params[2], _params[3], _params[4], _obj.Comp, _obj.UserName);
                sds.SelectParameters.Clear();
                sds.SelectParameters.Add("ID", e.Value.ToString());
                cmb.DataSource = sds;
                cmb.DataBind();
            }
            catch
            {


            }

        }

        protected void Cmb_ItemsRequestedByFilterConditionCasc(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
        {
            ASPxComboBox cmb = (ASPxComboBox)source;
            cmb.CallbackPageSize = 15;
            string[] _params = null;
            if (cmb.NullText.Contains("#"))
            {
                _params = cmb.NullText.Split('#');
            }
            else return;

            string valpar = string.Empty;

            try
            {
                if (_params[0] == "CascColumn2")
                    valpar = HttpContext.Current.Session["AddValPar2"].ToString();
                if (_params[0] == "CascColumn1")
                    valpar = HttpContext.Current.Session["AddValPar1"].ToString();
            }
            catch { }

            // string sss = string.Empty;
            //throw new Exception(FillComboboxV1(_params[0], _params[1], _params[2], _params[3], HttpContext.Current._obj.Comp, HttpContext.Current._obj.UserName));
            try
            {
                var connstr = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                var sds = new SqlDataSource();
                sds.ConnectionString = connstr;
                cmb.Items.Clear();
                sds.SelectCommand = DynamicUtilsV1.FillComboboxV3(_params[1], _params[2], _params[3], _params[4], _obj.Comp, _obj.UserName, valpar);
                //sss = sds.SelectCommand;


                sds.SelectParameters.Clear();
                sds.SelectParameters.Add("filter", TypeCode.String, string.Format("%{0}%", e.Filter));
                sds.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
                sds.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

                cmb.DataSource = sds;
                cmb.DataBind();




            }
            catch (Exception ex)
            {
                //throw;
                //throw new Exception(ex.Message);
            }
        }

        void SetComponentVisibility(string _FieldId, bool _visible)
        {
            try
            {
                var control = this.Page.FindControl(_FieldId);
                if (control == null) return;
                if (control.GetType() == typeof(HtmlTableRow))
                    control.Visible = _visible;
            }
            catch { }

        }

        protected void btnCreateInv_Click(object sender, EventArgs e)
        {
            string _result = string.Empty;

            switch (_Type)
            {
                case Types.SALESINV:
                case Types.PURCHMEMO:
                    string _Date = method.DateToStringNAVType(txtBelgeTarihi.Text);
                    string kur = txtDovizKuru.Text;
                    _result = GetWebFonksiyonlari(sirket).ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "ExecFunctionFromDynPopup§" + Typedesc + "§" + IslemId + "§" + _Date + ";" + kur, sirket);
                    if (_result.Contains("OK|"))
                    {
                        ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: alert('" + _result.Substring(3).Replace("'", string.Empty) + "'); proceed3('" + Request.QueryString["ObjectId"] + "');", true);
                    }
                    else
                        ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: alert('" + _result.Replace("'", string.Empty) + "');", true);
                    break;
                case Types.PURCHINV:
                    string faturano = cmbChoosen.Value.ToString();
                    _result = GetWebFonksiyonlari(sirket).ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "ExecFunctionFromDynPopup§" + Typedesc + "§" + IslemId + "§" + faturano, sirket);


                    if (_result.Contains("OK|"))
                    {
                        ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: alert('" + _result.Substring(3).Replace("'", string.Empty) + "'); proceed3('" + Request.QueryString["ObjectId"] + "');", true);
                    }
                    else
                        ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: alert('" + _result.Replace("'", string.Empty) + "');", true);
                    break;
                case Types.LOJCREATEDOC:
                    CreateDocument();
                    break;
                case Types.LOJATTACHDOC:
                    AttachDocument();
                    break;
                case Types.LOJATTACHDOCMLH:
                    AttachDocument();
                    break;
                case Types.RPF22:
                    CreateVoyageDocument();
                    break;
                case Types.CONTAINERMOVEMENT:
                    _result = GetWebFonksiyonlari(sirket).ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "ExecFunctionFromDynPopup§" + Typedesc + "§" + IslemId + "§" + "", sirket);

                    if (_result.Contains("OK|"))
                    {
                        ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: alert('" + _result.Substring(3).Replace("'", string.Empty) + "'); proceed3('" + Request.QueryString["ObjectId"] + "');", true);
                    }
                    else
                        ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: alert('" + _result.Replace("'", string.Empty) + "');", true);
                    break;
                case Types.CREATESHIPMENTJOB:
                    _result = GetWebFonksiyonlari(sirket).ExecGenericWebFunctionAllComp(_obj.UserName, "50030", "CreateLinerXMLToShipment§" + VoyageNo + "§" + cmbCustomer.Value.ToString() + "§" + _obj.UserName, sirket);

                    if (_result.Contains("OK|"))
                    {
                        ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: alert('" + _result.Substring(3).Replace("'", string.Empty) + "'); proceed3('" + Request.QueryString["ObjectId"] + "');", true);
                    }
                    else
                        ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: alert('" + _result.Replace("'", string.Empty) + "');", true);
                    break;
                case Types.EMANIFEST:
                    _result = GetWebFonksiyonlari(sirket).ExecGenericWebFunctionAllComp(_obj.UserName, "50030", "CreateFeederShipment§" + VoyageNo + "§" + cmbCustomer.Value.ToString() + "§" + _obj.UserName, sirket);

                    if (_result.Contains("OK|"))
                    {
                        ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: alert('" + _result.Substring(3).Replace("'", string.Empty) + "'); proceed3('" + Request.QueryString["ObjectId"] + "');", true);
                    }
                    else
                        ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: alert('" + _result.Replace("'", string.Empty) + "');", true);
                    break;
                case Types.LOADINGlST:
                    _result = GetWebFonksiyonlari(sirket).ExecGenericWebFunctionAllComp(_obj.UserName, "50030", "CreateLoadListToShipment§" + VoyageNo + "§" + cmbCustomer.Value.ToString() + "§" + _obj.UserName, sirket);

                    if (_result.Contains("OK|"))
                    {
                        ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: alert('" + _result.Substring(3).Replace("'", string.Empty) + "'); proceed3('" + Request.QueryString["ObjectId"] + "');", true);
                    }
                    else
                        ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: alert('" + _result.Replace("'", string.Empty) + "');", true);
                    break;
            }

        }

        void CreateVoyageDocument()
        {
            string _TaskNo = string.Empty, _MainType = string.Empty, _LojTaskNo = "0"
                , BlType = string.Empty, BlNo = string.Empty;



            DataTable dt = f.datatablegonder("SELECT *  FROM [GAC_NAV2].[dbo].[0_W12173] where RCOMP='" + sirket + "' and R0014='" + Request.QueryString["Type"] + "'");


            string DocNameTr = dt.Rows[0]["R0014"].ToString()
                , DocNameEn = dt.Rows[0]["R0015"].ToString()
                , F1 = dt.Rows[0]["R0020"].ToString()
                , F2 = dt.Rows[0]["R0021"].ToString()
                , F3 = dt.Rows[0]["R0022"].ToString()
                , ReportCode = dt.Rows[0]["R0014"].ToString()
                , DocumentCode = dt.Rows[0]["R0013"].ToString();
            int folder3 = Convert.ToInt32(dt.Rows[0]["R0022"].ToString().Replace("AIR", "1").Replace("LAND", "2").Replace("OCEAN", "3"));

            string FileName = GetVoyageReportName("1", JobNo, _TaskNo, Convert.ToInt32(_LojTaskNo), DocumentCode, CmbVoyage.Text);

            string _docvers = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50014", "DosyaNoAl§" + this.JobNo + "§" + _TaskNo + "§" + DocumentCode + "§" + "" + "§1", sirket);


            int version = Convert.ToInt32(_docvers);


            CreateDoc(FileName, "", 1, _TaskNo, DocumentCode, F2, folder3, ReportCode
                , BlNo, DocNameEn, DocNameTr, Convert.ToInt32(_LojTaskNo), version, "1");


            string _link = "./../Raporlar/DynamicReport.aspx?JobTaskNo=" + _LojTaskNo + "&JobNo=" + JobNo + "&Language=1&Company=" + this.sirket + "&FolderName=" + F2
                + "&Type=" + folder3.ToString() + "&ReportCode=" + ReportCode + "&SaveName=" + FileName + "&Version="
                + version + "&state=3" + "&VoyageNo=&blNo=" + BlNo + "&SaveMethod=save&SIRA=&Folder1=" + F1 + "&Folder2=" + F2 + "&Folder3=" + F3 + "&Foy=" + cmbopFoy.Value.ToString() + "&Agent=" + CmbVoyage.Text + "&GId=" + Request.QueryString["GId"];

            ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: window.opener.OpenPopupLink('" + _link + "'); proceed3('" + Request.QueryString["ObjectId"] + "');", true);

        }

        void CreateDocument()
        {
            string _TaskNo = string.Empty, _MainType = string.Empty, _LojTaskNo = string.Empty
                , BlType = string.Empty, BlNo = string.Empty;
            try
            {
                string[] _array = cmbTask.Value.ToString().Split('|');
                _TaskNo = _array[0];
                _MainType = _array[1];
                _LojTaskNo = _array[2];
            }
            catch
            {

                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "dynpopup", "msg001"));

                return;
            }

            if (txtWithoutBL.Text != "1")
            {

                try
                {
                    string[] _array3 = cmbBL.Value.ToString().Split('|');
                    BlNo = _array3[0];
                    BlType = _array3[1];
                }
                catch
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "dynpopup", "msg002"));
                    return;
                }
            }

            if (string.IsNullOrEmpty(_TaskNo))
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "dynpopup", "msg003"));
                return;
            }

            if (string.IsNullOrEmpty(_MainType))
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "dynpopup", "msg004"));
                return;
            }

            if (string.IsNullOrEmpty(_LojTaskNo))
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "dynpopup", "msg005"));
                return;
            }

            if (!string.IsNullOrEmpty(txtMusts.Text))
            {
                string[] _array2 = txtMusts.Text.Split(',');
                foreach (string str in _array2)
                {
                    var control = this.Page.FindControl("txt" + str);
                    if (control == null) continue;
                    if (control.GetType() == typeof(ASPxTextBox))
                    {
                        if (string.IsNullOrEmpty(((ASPxTextBox)control).Text))
                        {
                            MessageBox("Please fill the mandatory fiels [" + str + "]");
                            return;
                        }
                    }
                    else if (control.GetType() == typeof(ASPxDateEdit))
                    {
                        if (string.IsNullOrEmpty(((ASPxDateEdit)control).Text))
                        {
                            MessageBox("Please fill the mandatory fiels [" + str + "]");
                            return;
                        }
                    }
                }
            }

            string FileName = GetReportName(cmbLang.Value.ToString(), JobNo, _TaskNo, Convert.ToInt32(_LojTaskNo), CmbDocument.Value.ToString());

            DataTable dt = f.datatablegonder("SELECT *  FROM [GAC_NAV2].[dbo].[0_W12173] where RCOMP='" + sirket + "' and R0013='" + CmbDocument.Value.ToString() + "'");


            string DocNameTr = dt.Rows[0]["R0014"].ToString()
                , DocNameEn = dt.Rows[0]["R0015"].ToString()
                , F1 = dt.Rows[0]["R0020"].ToString()
                , F2 = dt.Rows[0]["R0021"].ToString()
                , F3 = dt.Rows[0]["R0022"].ToString()
                , ReportCode = dt.Rows[0]["R0018"].ToString();
            int folder3 = Convert.ToInt32(dt.Rows[0]["R0022"].ToString().Replace("AIR", "1").Replace("LAND", "2").Replace("OCEAN", "3"));


            string _docvers = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50014", "DosyaNoAl§" + this.JobNo + "§" + _TaskNo + "§" + CmbDocument.Value.ToString() + "§" + "" + "§" + cmbLang.Value.ToString(), sirket);


            int version = Convert.ToInt32(_docvers);




            CreateDoc(FileName, txtNotes.Text, 1, _TaskNo, CmbDocument.Value.ToString(), F2, folder3, ReportCode
                , BlNo, DocNameEn, DocNameTr, Convert.ToInt32(_LojTaskNo), version, BlType);


            string _link = "./../Raporlar/DynamicReport.aspx?JobTaskNo=" + _LojTaskNo + "&JobNo=" + JobNo + "&Language=" + cmbLang.Value.ToString() + "&Company=" + this.sirket + "&FolderName=" + F2
                + "&Type=" + folder3.ToString() + "&ReportCode=" + ReportCode + "&SaveName=" + FileName + "&Version="
                + version + "&state=3" + "&VoyageNo=&blNo=" + BlNo + "&SaveMethod=save&SIRA=" + cmbOrder.Value.ToString() + txtAmbar.Text + "&Folder1=" + F1 + "&Folder2=" + F2 + "&Folder3=" + F3 + "&Foy=" + cmbopFoy.Value.ToString() + "&GId=" + Request.QueryString["GId"];

            string _alici = string.Empty;
            try
            {
                _alici = cmbAlici.SelectedItem.Value.ToString();
            }
            catch
            {


            }

            if (trAlici.Visible) _link += "&Alici=" + _alici;
            if (trDamgaVergisi.Visible) _link += "&DamgaVergisi=" + txtDamgaVergisi.Text;
            if (trDefterSiraNo.Visible) _link += "&DefterSiraNo=" + txtDefterSiraNo.Text;
            if (trManuelAlici.Visible) _link += "&ManuelAlici=" + txtManuelAlici.Text;


            ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: window.opener.OpenPopupLink('" + _link + "'); proceed3('" + Request.QueryString["ObjectId"] + "');", true);

        }

        void AttachDocument()
        {
            if (Request.Url.AbsoluteUri.Contains("ENo="))
            {
                string _TaskNo = string.Empty, _MainType = string.Empty, _LojTaskNo = string.Empty
                             , BlType = string.Empty, BlNo = string.Empty;
                try
                {
                    string[] _array = cmbTask.Value.ToString().Split('|');
                    _TaskNo = _array[0];
                    _MainType = _array[1];
                    _LojTaskNo = _array[2];
                }
                catch
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "dynpopup", "msg001"));
                    return;
                }

                if (_Type != Types.LOJATTACHDOCMLH)
                {
                    if (txtWithoutBL.Text != "1")
                    {
                        try
                        {
                            string[] _array3 = cmbBL.Value.ToString().Split('|');
                            BlNo = _array3[0];
                            BlType = _array3[1];
                        }
                        catch
                        {
                            MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "dynpopup", "msg002"));
                            return;
                        }
                    }
                }

                if (string.IsNullOrEmpty(_TaskNo))
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "dynpopup", "msg003"));
                    return;
                }

                if (string.IsNullOrEmpty(_MainType))
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "dynpopup", "msg004"));
                    return;
                }

                if (string.IsNullOrEmpty(_LojTaskNo))
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "dynpopup", "msg005"));
                    return;
                }

                if (!string.IsNullOrEmpty(txtMusts.Text))
                {
                    string[] _array2 = txtMusts.Text.Split(',');
                    foreach (string str in _array2)
                    {
                        var control = this.Page.FindControl("txt" + str);
                        if (control == null) continue;
                        if (control.GetType() == typeof(ASPxTextBox))
                        {
                            if (string.IsNullOrEmpty(((ASPxTextBox)control).Text))
                            {
                                MessageBox("Please fill the mandatory fiels [" + str + "]");
                                return;
                            }
                        }
                        else if (control.GetType() == typeof(ASPxDateEdit))
                        {
                            if (string.IsNullOrEmpty(((ASPxDateEdit)control).Text))
                            {
                                MessageBox("Please fill the mandatory fiels [" + str + "]");
                                return;
                            }
                        }
                    }
                }

               // string[] dizi = FileUpload1.PostedFile.FileName.Split('.');
               // string dosyaUzantisi = dizi[1];

               // string FileName = GetReportName(cmbLang.Value.ToString(), JobNo, _TaskNo, Convert.ToInt32(_LojTaskNo), CmbDocument.Value.ToString()) + "." + dosyaUzantisi;
                string EntryNo = "#" + Request.QueryString["ENo"];

                DataTable dt = f.datatablegonder("SELECT *  FROM [GAC_NAV2].[dbo].[0_W12173] where RCOMP='" + sirket + "' and R0013='" + CmbDocument.Value.ToString() + "'");


                string DocNameTr = dt.Rows[0]["R0014"].ToString()
                    , DocNameEn = dt.Rows[0]["R0015"].ToString()
                    , F2 = dt.Rows[0]["R0021"].ToString()
                    , F3 = dt.Rows[0]["R0022"].ToString()
                    , ReportCode = dt.Rows[0]["R0018"].ToString()
                , F1 = dt.Rows[0]["R0020"].ToString();
                int folder3 = Convert.ToInt32(dt.Rows[0]["R0022"].ToString().Replace("AIR", "1").Replace("LAND", "2").Replace("OCEAN", "3"));


                string _docvers = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50014", "DosyaNoAl§" + this.JobNo + "§" + _TaskNo + "§" + CmbDocument.Value.ToString() + "§" + "" + "§" + cmbLang.Value.ToString(), sirket);


                int version = Convert.ToInt32(_docvers);


              //  if (!string.IsNullOrEmpty(F1) && !string.IsNullOrEmpty(F2) && !string.IsNullOrEmpty(F3))
              //  {
              //      FileUpload1.SaveAs(Server.MapPath("~/" + CurrentFolderName(_obj.Comp) + "/RAPORLAR OP/" + F1 + "/" + F2 + "/" + F3 + "/") + FileName);
              //  }

                if (string.IsNullOrEmpty(BlType)) BlType = "1";

                CreateDoc(EntryNo, txtNotes.Text, 0, _TaskNo, CmbDocument.Value.ToString(), F2, folder3, ReportCode
                    , BlNo, DocNameEn, DocNameTr, Convert.ToInt32(_LojTaskNo), version, BlType);



                ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: proceed3('" + Request.QueryString["ObjectId"] + "');", true);
            }
            else
            {
                string _TaskNo = string.Empty, _MainType = string.Empty, _LojTaskNo = string.Empty
                               , BlType = string.Empty, BlNo = string.Empty;
                try
                {
                    string[] _array = cmbTask.Value.ToString().Split('|');
                    _TaskNo = _array[0];
                    _MainType = _array[1];
                    _LojTaskNo = _array[2];
                }
                catch
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "dynpopup", "msg001"));
                    return;
                }

                if (!FileUpload1.HasFile)
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "dynpopup", "msg006"));
                    return;
                }
                if (_Type != Types.LOJATTACHDOCMLH)
                {
                    if (txtWithoutBL.Text != "1")
                    {
                        try
                        {
                            string[] _array3 = cmbBL.Value.ToString().Split('|');
                            BlNo = _array3[0];
                            BlType = _array3[1];
                        }
                        catch
                        {
                            MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "dynpopup", "msg002"));
                            return;
                        }
                    }
                }

                if (string.IsNullOrEmpty(_TaskNo))
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "dynpopup", "msg003"));
                    return;
                }

                if (string.IsNullOrEmpty(_MainType))
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "dynpopup", "msg004"));
                    return;
                }

                if (string.IsNullOrEmpty(_LojTaskNo))
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "dynpopup", "msg005"));
                    return;
                }

                if (!string.IsNullOrEmpty(txtMusts.Text))
                {
                    string[] _array2 = txtMusts.Text.Split(',');
                    foreach (string str in _array2)
                    {
                        var control = this.Page.FindControl("txt" + str);
                        if (control == null) continue;
                        if (control.GetType() == typeof(ASPxTextBox))
                        {
                            if (string.IsNullOrEmpty(((ASPxTextBox)control).Text))
                            {
                                MessageBox("Please fill the mandatory fiels [" + str + "]");
                                return;
                            }
                        }
                        else if (control.GetType() == typeof(ASPxDateEdit))
                        {
                            if (string.IsNullOrEmpty(((ASPxDateEdit)control).Text))
                            {
                                MessageBox("Please fill the mandatory fiels [" + str + "]");
                                return;
                            }
                        }
                    }
                }

                string[] dizi = FileUpload1.PostedFile.FileName.Split('.');
                string dosyaUzantisi = dizi[1];

                string FileName = GetReportName(cmbLang.Value.ToString(), JobNo, _TaskNo, Convert.ToInt32(_LojTaskNo), CmbDocument.Value.ToString()) + "." + dosyaUzantisi;

                DataTable dt = f.datatablegonder("SELECT *  FROM [GAC_NAV2].[dbo].[0_W12173] where RCOMP='" + sirket + "' and R0013='" + CmbDocument.Value.ToString() + "'");


                string DocNameTr = dt.Rows[0]["R0014"].ToString()
                    , DocNameEn = dt.Rows[0]["R0015"].ToString()
                    , F2 = dt.Rows[0]["R0021"].ToString()
                    , F3 = dt.Rows[0]["R0022"].ToString()
                    , ReportCode = dt.Rows[0]["R0018"].ToString()
                , F1 = dt.Rows[0]["R0020"].ToString();
                int folder3 = Convert.ToInt32(dt.Rows[0]["R0022"].ToString().Replace("AIR", "1").Replace("LAND", "2").Replace("OCEAN", "3"));


                string _docvers = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50014", "DosyaNoAl§" + this.JobNo + "§" + _TaskNo + "§" + CmbDocument.Value.ToString() + "§" + "" + "§" + cmbLang.Value.ToString(), sirket);


                int version = Convert.ToInt32(_docvers);


                if (!string.IsNullOrEmpty(F1) && !string.IsNullOrEmpty(F2) && !string.IsNullOrEmpty(F3))
                {
                    FileUpload1.SaveAs(Server.MapPath("~/" + CurrentFolderName(_obj.Comp) + "/RAPORLAR OP/" + F1 + "/" + F2 + "/" + F3 + "/") + FileName);

                }

                if (string.IsNullOrEmpty(BlType)) BlType = "1";

                CreateDoc(FileName, txtNotes.Text, 0, _TaskNo, CmbDocument.Value.ToString(), F2, folder3, ReportCode
                    , BlNo, DocNameEn, DocNameTr, Convert.ToInt32(_LojTaskNo), version, BlType);



                ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: proceed3('" + Request.QueryString["ObjectId"] + "');", true);
            }
           

        }

        private string GetReportName(string lang, string jobNo, string TaskNo, int LojTaskNo, string DocumentCode)
        {
            string reportName = string.Empty;
            Random rnd = new Random();
            int random = rnd.Next(0, 999);


            string _docvers = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50014", "DosyaNoAl§" + this.JobNo + "§" + TaskNo + "§" + DocumentCode + "§" + "" + "§" + cmbLang.Value.ToString(), sirket);


            int documentVersion = Convert.ToInt32(_docvers);

            int jobTaskCount = LojTaskNo.ToString().Count();

            if (jobTaskCount == 5)
            {
                reportName = jobNo + "0" + LojTaskNo.ToString().Replace("0000", "") + documentVersion + lang + random.ToString();
            }
            else if (jobTaskCount >= 6)
            {
                reportName = jobNo + LojTaskNo.ToString().Replace("0000", "") + documentVersion + lang + random.ToString();
            }
            //  MessageBox("getreportname "+reportName);
            return reportName;
        }

        private string GetVoyageReportName(string lang, string jobNo, string TaskNo, int LojTaskNo, string DocumentCode, string Agent)
        {
            string reportName = string.Empty;
            Random rnd = new Random();
            int random = rnd.Next(0, 999);


            string _docvers = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50014", "DosyaNoAl§" + this.JobNo + "§" + TaskNo + "§" + DocumentCode + "§" + "" + "§1", sirket);


            int documentVersion = Convert.ToInt32(_docvers);



            reportName = jobNo + documentVersion + Agent + random.ToString();

            return reportName;
        }

        private void CreateDoc(string FileName, string notes, int type, string TaskNo, string DocumentCode, string DocFolderName,
            int FolderCode, string ReportCode, string BlNo, string DocumentNameEn, string DocumentNameTr, int LojTaskNo, int version
            , string BlType)
        {
            if (type == 1)
            {
                FileName = FileName + ".PDF";
            }

            string createdTime = DateTime.Now.ToLongTimeString();

            string notes1 = "";
            string notes2 = "";
            if (notes.Length > 250)
            {
                if (notes.Length <= 500)
                {
                    notes1 = notes.Substring(0, 250);
                    notes2 = notes.Substring(250, notes.Length - 250);
                }
                else
                {
                    notes1 = notes.Substring(0, 250);
                }
            }
            else
            {
                notes1 = notes;
            }

            string description1 = "";
            string description2 = "";


            string shippingMarks1 = "";
            string shippingMarks2 = "";

            string HBLNavNo = "";
            string MBLNavNo = "";
            if (BlType == "1")
                HBLNavNo = BlNo;
            else
                MBLNavNo = BlNo;



            int intComp = 1;

            if (this.sirket == "LAM")
                intComp = 2;





            string[] _params1 = {
             method.DateToStringNAVType(txtETD.Text),
             method.DateToStringNAVType(txtETA.Text),
             method.DateToStringNAVType(txtATD.Text),
             method.DateToStringNAVType(txtATA.Text),
                "", "", "",
                notes1, "", "", DocumentCode, _obj.UserName, intComp.ToString(),
                DocFolderName, FolderCode.ToString(), ReportCode, version.ToString(),
                this.JobNo, TaskNo, createdTime,
             method.DateToStringNAVType(txtInst_Wh_Cut_off_Date.Text),
                "", "", "", "", "",
                "", "", "",
                "0", description1, "", MBLNavNo, HBLNavNo,
                "0", method.StringToDecimal(txtQuantity.Text).ToString(), "",
                "", "", "", "", "", "",
                "", "", BlNo, "", DocumentNameEn,
                DocumentNameTr, FileName, LojTaskNo.ToString(), "0", txtInst__Wh__Cut_off_Time.Text,
                "",
                "", notes2, shippingMarks1, shippingMarks2,
                "0", description2, cmbLang.Value.ToString(), txtLCNumber.Text, _obj.UserName
                , "1", cmbOrdinoAdress.SelectedItem != null ? cmbOrdinoAdress.SelectedItem.Value.ToString() : "", BlType };

            string _docvers = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50014", "CreateDocument§" + string.Join("§", _params1), Session["Sirket"].ToString());



        }



        protected void CmbDocument_Callback(object sender, CallbackEventArgsBase e)
        {
            if (string.IsNullOrEmpty(e.Parameter)) return;

            ASPxComboBox cmb = sender as ASPxComboBox;
            switch (_Type)
            {
                case Types.LOJCREATEDOC:
                    DSDyn2.SelectCommand = "SELECT R0013 Code, R0014 [Text] FROM [0_W12173] where R0016=1 and RCOMP='" + sirket + "' and R0011='" + e.Parameter.Split('|')[1] + "'";
                    break;
                case Types.LOJATTACHDOC:
                    DSDyn2.SelectCommand = "SELECT R0013 Code, R0014 [Text] FROM [0_W12173] where R0016=2 and RCOMP='" + sirket + "' and R0011='" + e.Parameter.Split('|')[1] + "'";
                    break;
                case Types.LOJATTACHDOCMLH:
                    DSDyn2.SelectCommand = "SELECT R0013 Code, R0014 [Text] FROM [0_W12173] where R0016=2 and RCOMP='" + sirket + "' and R0011='" + e.Parameter.Split('|')[1] + "'";
                    break;
            }
            cmb.Items.Clear();
            cmb.DataBind();

        }

        protected void CmbDocument_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtWithoutBL.Text = "";
            ASPxComboBox cmb = sender as ASPxComboBox;
            if (cmb.Value == null) return;

            if (_Type != Types.LOJATTACHDOCMLH)
            {
                string _WithoutBl = f.tekbirsonucdonder("SELECT R0023 FROM [0_W12173] where  RCOMP = '" + sirket + "' and R0013='" + cmb.Value.ToString() + "'");
                if (_WithoutBl == "1")
                {
                    txtWithoutBL.Text = "1";
                    trBl.Visible = false;
                }
                else
                    trBl.Visible = true;
            }
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    clearValues();
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT *  FROM [0_W12174] where RCOMP = '" + sirket + "' and R0011='" + cmb.Value.ToString() + "'  order by R0011";



                    using (SqlDataAdapter adptr = new SqlDataAdapter(cmd))
                    {
                        using (DataSet ds = new DataSet())
                        {
                            adptr.Fill(ds);
                            bool ilk = true;
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                SetComponentVisibility("tr" + row["R0012"].ToString(), true);
                                if ("tr" + row["R0012"].ToString() == "trOrdinoAdress")
                                {
                                    SetComponentVisibility("trSiraNo", true);
                                    SetComponentVisibility("trAmbar", true);
                                }
                                if (row["R0012"].ToString() == "ManuelAlici") continue;
                                if (ilk)
                                {
                                    txtMusts.Text = row["R0012"].ToString();
                                    ilk = false;
                                }
                                else
                                    txtMusts.Text = "," + row["R0012"].ToString();
                            }
                        }

                    }
                }

            }
        }

        void clearValues()
        {
            txtATA.Text = string.Empty;
            txtATD.Text = string.Empty;
            txtETA.Text = string.Empty;
            txtETD.Text = string.Empty;
            txtInst__Wh__Cut_off_Time.Text = string.Empty;
            txtInst_Wh_Cut_off_Date.Text = string.Empty;
            txtLCNumber.Text = string.Empty;
            cmbOrdinoAdress.SelectedIndex = -1;
            txtQuantity.Text = string.Empty;
            txtMusts.Text = string.Empty;
            cmbAlici.SelectedIndex = 0;
            txtDamgaVergisi.Text = string.Empty;
            txtDefterSiraNo.Text = string.Empty;
            txtManuelAlici.Text = string.Empty;
            SetComponentVisibility("trATA", false);
            SetComponentVisibility("trATD", false);
            SetComponentVisibility("trETA", false);
            SetComponentVisibility("trETD", false);
            SetComponentVisibility("trETD", false);
            SetComponentVisibility("trInst_Wh_Cut_off_Date", false);
            SetComponentVisibility("trInst__Wh__Cut_off_Time", false);
            SetComponentVisibility("trLCNumber", false);
            SetComponentVisibility("trOrdinoAdress", false);
            SetComponentVisibility("trQuantity", false);
            SetComponentVisibility("trAlici", false);
            SetComponentVisibility("trDamgaVergisi", false);
            SetComponentVisibility("trDefterSiraNo", false);
            SetComponentVisibility("trManuelAlici", false);

        }

        protected void cmbBL_Callback(object sender, CallbackEventArgsBase e)
        {
            if (string.IsNullOrEmpty(e.Parameter)) return;

            ASPxComboBox cmb = sender as ASPxComboBox;

            DSDyn3.SelectCommand = "SELECT R0015+'|'+CAST(R0013 as varchar) Code, R0015 [Text] FROM [0_W12165] where RCOMP='" + sirket + "' and R0012='" + e.Parameter.Split('|')[0] + "'";
            cmb.Items.Clear();
            cmb.DataBind();

            if (cmb.Items.Count == 1) cmb.SelectedIndex = 0;
        }

        protected void cmbActionPlace_Callback(object sender, CallbackEventArgsBase e)
        {
            if (string.IsNullOrEmpty(e.Parameter)) return;
            ASPxComboBox cmb = (ASPxComboBox)sender;
            string[] PMarray = cmb.NullText.Split('#');
            string DxCascFilter = PMarray[5];


            Session["AddValPar1"] = " and " + DxCascFilter + "='" + e.Parameter + "'";
        }
    }
}