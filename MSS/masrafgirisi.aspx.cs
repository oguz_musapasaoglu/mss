﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using MSS1.WSGeneric;
using System.ServiceModel;
using System.Data;
using MSS1.Codes;

namespace MSS1
{
    public partial class masrafgirisi : Bases
    {
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        SqlConnection baglanti;
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        
        public ws_baglantı ws = new ws_baglantı();
        Methods method = new Methods();
        SessionBase _obj;
        static SessionBase _objStatic;
        #region Dil Getir
        public void dilgetir()
        {
            lblBaslik.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblBaslik");
            lblAlacakHesapB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblAlacakHesapB");
            lblAlacakliHesapB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblAlacakliHesapB");
            lblBelgeNoB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblBelgeNoB");
            lblNakilTarihiB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblNakilTarihiB");
            lblMatrahB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblMatrahB");
            lblKDVB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblKDVB");
            lblToplamB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblToplamB");
            btnonayla.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "btnonayla");
            btnsatirsil.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "btnsatirsil");
            btndosya.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "btndosya");
            btndosyagor.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "btndosyagor");
            btnfaturasatirkopyala.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "btnfaturasatirkopyala");
            lblAlacakHesapB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblAlacakHesapB");
            lblBelgeTarihiB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblBelgeTarihiB");
            lblFirmaAdiB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblFirmaAdiB");
            lblHariciBelgeB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblHariciBelgeB");
            lblVergiNoB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblVergiNoB");
            lblParaBirimiB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblParaBirimiB");
            lblGununKuruB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblGununKuruB");
            lblKDVHaricB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblKDVHaricB");
            lblKDVOranB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblKDVOranB");
            lblKDVTutarB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblKDVTutarB");
            lblKDVDahilTutarB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblKDVDahilTutarB");
            lblTutarTLB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblTutarTLB");
            lblGGT1B.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblGGT1B");
            lblGGT2B.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblGGT2B");
            lblGGT3B.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblGGT3B");
            lblKaynakAdiB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblKaynakAdiB");
            lblIsGACNoB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblIsGACNoB");
            lblIsGorevAdiB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblIsGorevAdiB");
            lblFinalDAKuruB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblFinalDAKuruB");
            lblFinalDADovizCinsiB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblFinalDADovizCinsiB");
            lblAracPlakaB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblAracPlakaB");
            lblPersonelB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblPersonelB");
            lblBolgeB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblBolgeB");
            lblDepartmanB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblDepartmanB");
            lblFaaliyetB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblFaaliyetB");
            lblSeriNoB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "lblSeriNoB");
        }
        #endregion


        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            dilgetir();
            Page.Form.DefaultButton = btnekle.UniqueID;

            if (!IsPostBack)
            {
                // f.DropDownDoldurIlkSatirEkle("Select Code name, Code kod From Currency Order By Code", ddlfinaldadoviz, true, "TL");
                ddlfinaldadoviz.SelectedIndex = 0;
                #region belgenoal

                string[] _params = { _obj.UserName };
                lblbelgeno.Text = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "MasrafSonBelgeNoAl§" + string.Join("§", _params), _obj.Comp);

                #endregion
                f.BolgeDoldur(_obj.Comp, ddlbolge);
                f.HizmetDoldur(_obj.Comp, ddlfaaliyet);
                f.DepartmanDoldur(_obj.Comp, ddldepartman);
                f.AracPlakaDoldur(_obj.Comp, ddlarac);
                f.PersonelDoldur(_obj.Comp, ddlpersonel);
                AutoCompleteExtender1.ContextKey = _obj.Comp;
                txtprojegorev_AutoCompleteExtender.ContextKey = _obj.Comp + ":" + lblprojeno.Text + ":0";
                masrafgirdoldur(_obj.Comp, _obj.UserName, lblbelgeno.Text, grdmasraf, txtdnakil, ddlalacakhesabı);
                chper_CheckedChanged(chper, EventArgs.Empty);
                //if (ddlalacakhesabı.SelectedValue == string.Empty)
                //{
                //    chper.Visible = false;
                //}
                ddlalacakhesabı_SelectedIndexChanged(ddlalacakhesabı, EventArgs.Empty);
                if (grdmasraf.Rows.Count > 0)
                {
                    txtdnakil.Enabled = false;
                    ddlalacakhesabı.Enabled = false;
                    chper.Enabled = false;
                }
                lbltoplamsatir.Text = toplamsecilisatir().ToString();
                f.KDVUrunNakilGrubuDoldur(_obj.Comp, ddlkdvoran, 0);
                f.tur1doldur(_obj.Comp, ddltur1);
                f.tur2doldur(_obj.Comp, ddltur1.SelectedValue, ddltur2);
                f.tur3doldur(_obj.Comp, ddltur1.SelectedValue, ddltur2.SelectedValue, ddltur3);
                //if (Request.QueryString["ProjeNo"] != null && Request.QueryString["ProjeGorevNo"] != null)
                //{ 
                lblprojeno.Text = Request.QueryString["ProjeNo"];
                lblprojegorev.Text = Request.QueryString["ProjeGorevNo"];
                f.projeadidoldur(_obj.Comp, lblprojeno.Text, txtprojeadi);
                txtprojeadi_TextChanged(txtprojeadi, EventArgs.Empty);
                f.projegorevadidoldur(_obj.Comp, lblprojeno.Text, lblprojegorev.Text, txtprojegorev);
                txtprojegorev_TextChanged(txtprojegorev, EventArgs.Empty);
                //}
                dosyagetir(_obj.Comp);
                Toplamsatırtutarı();


            }

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());
            _objStatic = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());
        }
        private void dosyagetir(string sirket)
        {

            baglanti = new SqlConnection(strConnString);
            baglanti.Open();
            SqlCommand sorgu = new SqlCommand("Select [Dosya Path] as DosyaPath,[Dosya İsmi] as dosyaismi from [0C_50000_00_DOSYA BAGLAMA] where COMPANY='" + sirket + "' and BelgeNo='" + lblbelgeno.Text + "'", baglanti);
            SqlDataReader sqr;
            sqr = sorgu.ExecuteReader();
            if (sqr.Read())
            {
                if (sqr["dosyaismi"] != DBNull.Value)
                {
                    btndosyagoster.Visible = true;
                    btndosyagoster.Text = sqr["dosyaismi"].ToString();
                    lbldosyapath.Text = sqr["DosyaPath"].ToString();
                    lblbaglanmısdosya.Visible = true;

                }
                else
                {
                    lblbaglanmısdosya.Visible = false;
                    btndosyagoster.Visible = false;
                }
            }
            else
            {
                lblbaglanmısdosya.Visible = false;
                btndosyagoster.Visible = false;
            }

            sqr.Close();
        }
        private void Toplamsatırtutarı()
        {
            decimal toplamtutar = 0.00m, toplamtutarkdv = 0.00m;
            foreach (GridViewRow item in grdmasraf.Rows)
            {
                Label lblkdvharic = (Label)grdmasraf.Rows[item.RowIndex].FindControl("lblkdvharic");
                Label lblkdahiltutar = (Label)grdmasraf.Rows[item.RowIndex].FindControl("lblkdahiltutar");
                Label lblkur = (Label)grdmasraf.Rows[item.RowIndex].FindControl("lblkur");
                Label lbldovizcinsi = (Label)grdmasraf.Rows[item.RowIndex].FindControl("lbldovizcinsi");
                if (lbldovizcinsi.Text == string.Empty)
                {
                    toplamtutarkdv += Convert.ToDecimal(lblkdahiltutar.Text);
                    toplamtutar += Convert.ToDecimal(lblkdvharic.Text);
                }
                else
                {
                    toplamtutarkdv += Convert.ToDecimal(lblkdahiltutar.Text) * Convert.ToDecimal(lblkur.Text);
                    toplamtutar += Convert.ToDecimal(lblkdvharic.Text) * Convert.ToDecimal(lblkur.Text);
                }
                lblsatirtoplamtl.Text = toplamtutar.ToString("N");
                lblsatirtoplamtlkdv.Text = toplamtutarkdv.ToString("N");
            }
            if (toplamtutarkdv > 0 && toplamtutar > 0)
            {
                lblfarktl.Text = (Convert.ToDecimal(lblsatirtoplamtlkdv.Text) - Convert.ToDecimal(lblsatirtoplamtl.Text)).ToString("N");
            }
            if (grdmasraf.Rows.Count == 0)
            {
                lblfarktl.Text = "0,00";
                lblsatirtoplamtlkdv.Text = "0,00";
                lblsatirtoplamtl.Text = "0,00";
            }

        }
        private int toplamsecilisatir()
        {
            int toplam = 0;
            foreach (GridViewRow grdRow in grdmasraf.Rows)
            {
                if (((CheckBox)grdRow.FindControl("Chksec")).Checked)
                {
                    toplam++;
                }
            }
            return toplam;
        }
        #region Servisler
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchCustomer(string prefixText, int count)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                string query = "Select Name from [0C_00018_01_CUSTOMER]";

                if (prefixText != "*")
                {
                    query += " Where Upper(Name) like '" + prefixText.ToUpper() + "%'";
                }
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    conn.Open();
                    List<string> Cust = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Cust.Add(sdr["Name"].ToString());
                        }
                    }
                    conn.Close();
                    return Cust;
                }
            }

        }
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchResource(string prefixText, int count, string contextKey)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                string query = "SELECT Name FROM [0C_00156_00_RESOURCE] where COMPANY ='" + _objStatic.Comp + "' and [Tur Kodu]='" + contextKey.ToUpper() + "'";
                if (prefixText != "*")
                {
                    query += " and  Upper(Name)  like '" + prefixText.ToUpper() + "%'";
                }

                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    cmd.Connection = conn;
                    conn.Open();
                    List<string> Resource = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Resource.Add(sdr["Name"].ToString());
                        }
                    }
                    conn.Close();

                    return Resource;
                }

            }

        }
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchJob(string prefixText, int count, string contextKey)
        {

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_0WEB_PROJE_ADI_ARA_OPEN";
                    cmd.Parameters.AddWithValue("@sirket", contextKey);
                    cmd.Parameters.AddWithValue("@p_adi", prefixText.ToUpper());


                    cmd.Connection = conn;
                    conn.Open();
                    List<string> Job = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {

                            Job.Add(sdr["Description"].ToString());
                        }
                    }
                    conn.Close();

                    return Job;
                }

            }

        }
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchJobTask(string prefixText, int count, string contextKey)
        {
            string[] splitter = contextKey.Split(':');
            string query = "SELECT TOP 100 Description+' | '+[Liman Adı] +' | '+ [Job Task No_] Description from [0C_01001_00_JOB_TASK] where COMPANY='" + _objStatic.Comp + "' and [Job No_]='" + splitter[1] + "'";
            if (prefixText != "*")
            {
                query += " and Upper(Description) like '" + prefixText.ToUpper() + "%'";
            }

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    conn.Open();
                    List<string> Jobtask = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Jobtask.Add(sdr["Description"].ToString());
                        }
                    }
                    conn.Close();

                    return Jobtask;
                }

            }

        }
        #endregion
        protected void txtprojeadi_TextChanged(object sender, EventArgs e)
        {
            txtprojegorev.Text = "";
            f.projeKoduDoldur(_obj.Comp, txtprojeadi.Text, lblprojeno);
            txtprojegorev_AutoCompleteExtender.ContextKey = _obj.Comp + ":" + lblprojeno.Text + ":0";
            if (txtdnakil.Text != string.Empty)
            {
                string tarih;
                int tarihkarakter = txtdnakil.Text.Length - 4;
                tarih = txtdnakil.Text.Substring(tarihkarakter, 4);
                if ((tarih == "2015") && ((lblprojeno.Text == "G1400001") || (lblprojeno.Text == "L1400001")))
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg454"));
                    txtprojeadi.Text = string.Empty;
                }
                if ((tarih == "2015") && ((lblprojeno.Text == "G130001") || (lblprojeno.Text == "L130001")))
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg456"));
                    txtprojeadi.Text = string.Empty;
                }
                if ((tarih == "2014") && ((lblprojeno.Text == "G1500001") || (lblprojeno.Text == "L1500001")))
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg454"));
                    txtprojeadi.Text = string.Empty;
                }
                if ((tarih == "2014") && ((lblprojeno.Text == "G130001") || (lblprojeno.Text == "L130001")))
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg224"));
                    txtprojeadi.Text = string.Empty;
                }

                if ((tarih == "2013") && ((lblprojeno.Text == "G1400001") || (lblprojeno.Text == "L1400001")))
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg225"));
                    txtprojeadi.Text = string.Empty;
                }
            }
        }
        protected void ddltur_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtkaynak.Text = string.Empty;
        }
        protected void txtprojegorev_TextChanged(object sender, EventArgs e)
        {
            f.projegorevKoduDoldur(_obj.Comp, lblprojeno.Text, txtprojegorev.Text, 0, lblprojegorev);
            f.projeboyutdoldur(_obj.Comp, lblprojeno.Text, lblprojegorev.Text, ddlbolge, ddldepartman, ddlfaaliyet, ddlpersonel, ddlarac);
        }
        public bool donder(string kul, string sirket, string kontrol)
        {
            bool sonuc = false;
            string hangiyetki = f.tekbirsonucdonder("select AK53 from [0C_50005_00_YETKILENDIRME] where [Kullanıcı Adı] = '" + _obj.UserName + "' and Sirket='" + _obj.Comp + "'");
            if (hangiyetki == "10")
            {
                sonuc = true;
            }
            else if (hangiyetki == "00")
            {
                sonuc = false;
            }
            else if (hangiyetki == "21" || hangiyetki == "22" || hangiyetki == "23")
            {
                SqlConnection conn = new SqlConnection(strConnString);
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "sp_departmandolduryetki";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = conn;
                cmd.Parameters.AddWithValue("@kad", kul);
                cmd.Parameters.AddWithValue("@sirket", sirket);
                cmd.Parameters.AddWithValue("@kontrol", kontrol);
                string birlesik = cmd.ExecuteScalar().ToString();
                string[] filtre = birlesik.Split('-');
                string[] depfiltre = filtre[0].Split('|');
                string[] bolfiltre = filtre[1].Split('|');

                #region Kullanıcı Yetkisi
                if (depfiltre[0].ToString().Trim() != "" || depfiltre[0].ToString().Trim() != string.Empty)
                {
                    for (int i = 0; i < depfiltre.Length; i++)
                    {
                        if (ddldepartman.SelectedValue.Substring(0, depfiltre[i].ToString().Length) == depfiltre[i].ToString())
                        {
                            sonuc = true;
                            break;
                        }
                        else
                        {
                            sonuc = false;
                        }
                    }
                }
                else
                {
                    sonuc = true;
                }

                if ((bolfiltre[0].ToString().Trim() != "" || bolfiltre[0].ToString().Trim() != string.Empty) && sonuc == true)
                {
                    for (int i = 0; i < bolfiltre.Length; i++)
                    {
                        if (ddlbolge.SelectedValue.Substring(0, bolfiltre[i].ToString().Length) == bolfiltre[i].ToString())
                        {
                            sonuc = true;
                            break;
                        }
                        else
                        {
                            sonuc = false;
                        }
                    }
                }
                #endregion


            }
            return sonuc;

        }

        protected void btnekle_Click(object sender, EventArgs e)
        {
            if (ddltur1.SelectedValue + ddltur2.SelectedValue + ddltur3.SelectedValue != "6200801" && ddltur1.SelectedValue + ddltur2.SelectedValue + ddltur3.SelectedValue != "6160402")
            {
                eklemeislemiyap();
            }
            else
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg227"));
            }



        }
        public void eklemeislemiyap()
        {
            string[] kont_params = { lblprojeno.Text };
            string ProjeKontrol = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "ProjeKontrol§" + string.Join("§", kont_params), _obj.Comp);


            string[] kont_params2 = { lblprojeno.Text }; 
            string ProjeGorevKontrol = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "ProjeGorevKontrol§" + string.Join("§", kont_params2), _obj.Comp);

            if (txtdnakil.Text != string.Empty && ProjeKontrol=="True" && ProjeGorevKontrol == "True" && txtkaynak.Text != string.Empty && txtnettutar.Text != string.Empty && ddlalacakhesabı.SelectedValue != string.Empty && txtbelgetarihi.Text != string.Empty && ddlkdvoran.SelectedValue != string.Empty && ddlbolge.SelectedValue != string.Empty && ddldepartman.SelectedValue != string.Empty && ddlfaaliyet.SelectedValue != string.Empty && txtSeriNo.Text != string.Empty && txtharicibelgeno.Text != string.Empty)
            {
                if (((ddltur1.SelectedValue + ddltur2.SelectedValue + ddltur3.SelectedValue) == "7400601" && ddlkdvoran.SelectedValue == "0") || (ddlkdvoran.SelectedValue == "0" && (ddltur1.SelectedValue + ddltur2.SelectedValue + ddltur3.SelectedValue) == "7700601") || ((ddltur1.SelectedValue + ddltur2.SelectedValue + ddltur3.SelectedValue) != "7400601" && (ddltur1.SelectedValue + ddltur2.SelectedValue + ddltur3.SelectedValue) != "7700601"))
                {
                    string[] tcevir = txtbelgetarihi.Text.Split('.');
                    string[] t1cevir = txtdnakil.Text.Split('.');
                    DateTime dt1 = new DateTime(Convert.ToInt32(tcevir[2]), Convert.ToInt32(tcevir[1]), Convert.ToInt32(tcevir[0]));
                    DateTime dt2 = new DateTime(Convert.ToInt32(t1cevir[2]), Convert.ToInt32(t1cevir[1]), Convert.ToInt32(t1cevir[0]));
                    if (dt1 < DateTime.Now && dt2 < DateTime.Now)
                    {
                        string[] _params = { lblprojeno.Text  }; 
                        string drm  = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "ProjeKontrol§" + string.Join("§", _params), _obj.Comp);


                        if (drm == "True")
                        {

                           
                            string[] _params2 = { _obj.UserName };
                            string masrafsonno = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "MasrafSonNoAl§" + string.Join("§", _params2), _obj.Comp);



                            string[] _params3 = { _obj.UserName, lblkaynak.Text, masrafsonno, "1", "", "", ddldepartman.SelectedValue }; 
                            string drm3 = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "KurulumKontrol§" + string.Join("§", _params3), _obj.Comp);



                            //  Response.Write(_obj.UserName + "--" + lblkaynak.Text + "--" + record.MasrafSonNoAl(_obj.UserName) + "--" + 1 + "--" + "" + "--" + "" + "--" + ddldepartman.SelectedValue);
                            if (drm3 == "True")//type =1 "masraf"
                            {

                                string[] _params4 = { _obj.UserName };
                                string masrafsonno1 = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "MasrafSonNoAl§" + string.Join("§", _params4), _obj.Comp);




                                string[] _params5 = { _obj.UserName, t1cevir[1] + t1cevir[0] + t1cevir[2], lblprojeno.Text, lblprojegorev.Text,
                                    lblkaynak.Text, ddlkdvoran.SelectedValue, ddlalacakhesabı.SelectedValue, masrafsonno1,
                                    lblbelgeno.Text, _obj.NameSurName, ddlparabirimi.SelectedValue, txtharicibelgeno.Text, txtfirma.Text,
                                    txtkdvdahil.Text, ddltur1.SelectedValue, ddltur2.SelectedValue, ddltur3.SelectedValue, txtkaynak.Text,
                                    tcevir[1] + tcevir[0] + tcevir[2],txtkur.Text, ddlbolge.SelectedValue, ddldepartman.SelectedValue,
                                    ddlfaaliyet.SelectedValue, ddlpersonel.SelectedValue, ddlarac.SelectedValue, "0", txtvergino.Text,
                                    Convert.ToInt32(chper.Checked).ToString(), txtfinaldakur.Text, ddlfinaldadoviz.SelectedValue, txtSeriNo.Text,
                                    GetDescription() };
                                GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "MasrafGirisOlustur§" + string.Join("§", _params5), _obj.Comp);

                                txtdnakil.Enabled = false;
                                temizle();
                                masrafgirdoldur(_obj.Comp, _obj.UserName, lblbelgeno.Text, grdmasraf, txtdnakil, ddlalacakhesabı);
                                if (grdmasraf.Rows.Count > 0)
                                {
                                    txtdnakil.Enabled = false;
                                    ddlalacakhesabı.Enabled = false;
                                    chper.Enabled = false;
                                }

                                lblkaynak.Text = string.Empty;
                            }
                            else
                            {
                                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg228"));

                            }

                        }
                        else
                        {
                            MessageBox(txtprojeadi.Text + l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg229"));
                        }
                    }
                    else
                    {
                        MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg230"));
                    }
                }
                else
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg231"));
                }
            }
            else
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg232"));
            }

            lbltoplamsatir.Text = toplamsecilisatir().ToString();
            Toplamsatırtutarı();
        }

        private string GetDescription()
        {
            string firma = txtfirma.Text;
            string kaynak = txtkaynak.Text;

            string firma1 = string.Empty;
            string kaynak1 = string.Empty;

            string firmakaynak = string.Empty;

            if (txtkaynak.Text != string.Empty && txtfirma.Text != string.Empty)
            {
                if (firma.Length < 20)
                {
                    firma1 = firma.Substring(0, firma.Length);
                }
                else if (firma.Length >= 19)
                {
                    firma1 = firma.Substring(0, 19);
                }

                if (kaynak.Length < 30)
                {
                    kaynak1 = kaynak.Substring(0, kaynak.Length);
                }
                else if (kaynak.Length >= 30)
                {
                    kaynak1 = kaynak.Substring(0, 30);
                }

                firmakaynak = firma1 + " " + kaynak1;
            }
            return firmakaynak;
        }



        void temizle()
        {
            txtbelgetarihi.Text = string.Empty;
            txtkaynak.Text = string.Empty;
            txtprojeadi.Text = string.Empty;
            txtprojegorev.Text = string.Empty;
            ddlkdvoran.SelectedIndex = -1;
            txtnettutar.Text = string.Empty;
            txtkdvtutar.Text = string.Empty;
            txtharicibelgeno.Text = string.Empty;
            txtfirma.Text = string.Empty;
            txtkdvdahil.Text = string.Empty;
            txtkdvtutar.Text = string.Empty;
            ddltur1.SelectedIndex = -1;
            ddltur2.SelectedIndex = -1;
            ddltur3.SelectedIndex = -1;
            ddlparabirimi.SelectedIndex = -1;
            txtkur.Text = "0.00";
            lbltutartl.Text = "0.00";
            ddlbolge.SelectedIndex = -1;
            ddldepartman.SelectedIndex = -1;
            ddlfaaliyet.SelectedIndex = -1;
            ddlpersonel.SelectedIndex = -1;
            ddlarac.SelectedIndex = -1;
            txtvergino.Text = string.Empty;
            txtSeriNo.Text = string.Empty;
        }
        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        protected void grdmasraf_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                CheckBox chksec = (CheckBox)grdmasraf.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("Chksec");
                if (chksec.Checked)
                {
                    chksec.Checked = false;
                    lbltoplamsatir.Text = toplamsecilisatir().ToString();
                }
                else
                {
                    chksec.Checked = true;
                    lbltoplamsatir.Text = toplamsecilisatir().ToString();
                }


            }
        }
        protected void grdmasraf_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                e.Row.Attributes["onmouseover"] = "javascript:setMouseOverColor(this);";
                e.Row.Attributes["onmouseout"] = "javascript:setMouseOutColor(this);";
                e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(this.grdmasraf, "Select$" + e.Row.RowIndex);
                if (e.Row.RowState == DataControlRowState.Selected)
                {
                    CheckBox chksec = (CheckBox)e.Row.FindControl("Chksec");
                    chksec.Checked = true;
                    lbltoplamsatir.Text = toplamsecilisatir().ToString();
                }
            }
        }
        protected void ddlalacakhesabı_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (grdmasraf.Rows.Count > 0)
            {
                txtdnakil.Enabled = false;
                ddlalacakhesabı.Enabled = false;
            }
            lblkhesapadi.Text = ddlalacakhesabı.SelectedIndex.ToString();

            if (ddlalacakhesabı.SelectedIndex > 0)
            {
                chper.Enabled = false;
            }
            string sorgu = "SELECT CASE WHEN [Currency Code]='' then 'TL' ELSE [Currency Code] END  , * FROM  [0C_00270_00_BANK ACCOUNTS] where COMPANY='" + _obj.Comp + "' AND No_='" + ddlalacakhesabı.SelectedValue + "'";
            string _birim = f.tekbirsonucdonder(sorgu);
            ddlparabirimi.SelectedValue = _birim.Replace("TL", "");
            if (txtbelgetarihi.Text != "" & txtdnakil.Text != "")
                f.kurgetir(_obj.Comp, txtkur, txtbelgetarihi.Text, ddlparabirimi.SelectedValue);
        }
        protected void txtnettutar_TextChanged(object sender, EventArgs e)
        {
            if (ddlkdvoran.SelectedValue != string.Empty)
            {
                decimal kdvdahil = (Convert.ToDecimal(txtnettutar.Text) * Convert.ToDecimal(ddlkdvoran.SelectedValue)) / 100;
                txtkdvtutar.Text = kdvdahil.ToString();
                txtkdvdahil.Text = (Convert.ToDecimal(txtnettutar.Text) + kdvdahil).ToString();
            }
            else
            {
                txtkdvdahil.Text = txtnettutar.Text;
            }
            if (txtkur.Text != "0.00")
            {
                lbltutartl.Text = Convert.ToString(Convert.ToDecimal(txtkur.Text) * Convert.ToDecimal(txtnettutar.Text));
            }
            else
            {
                lbltutartl.Text = txtnettutar.Text;
            }


        }
        protected void ddlkdvoran_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((ddltur1.SelectedValue + ddltur2.SelectedValue + ddltur3.SelectedValue) == "6160401")
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg233"));
                ddlkdvoran.SelectedValue = "0";
            }
            else if (txtnettutar.Text != string.Empty && ddlkdvoran.SelectedValue != string.Empty)
            {
                decimal kdvdahil = (Convert.ToDecimal(txtnettutar.Text) * Convert.ToDecimal(ddlkdvoran.SelectedValue)) / 100;
                txtkdvtutar.Text = kdvdahil.ToString();
                txtkdvdahil.Text = (Convert.ToDecimal(txtnettutar.Text) + kdvdahil).ToString();
            }
            if (lblkaynak.Text != string.Empty)
            {
                if (f.KKEGbul(_obj.Comp, lblkaynak.Text) == 1)
                {
                    if (ddlkdvoran.SelectedValue == "1" || ddlkdvoran.SelectedValue == "18" || ddlkdvoran.SelectedValue == "8")
                    {
                        MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg234"));
                        ddlkdvoran.SelectedIndex = -1;
                    }
                }
            }

            if (((ddltur1.SelectedValue + ddltur2.SelectedValue + ddltur3.SelectedValue) == "7409090") || ((ddltur1.SelectedValue + ddltur2.SelectedValue + ddltur3.SelectedValue) == "7709090"))
            {
                ddlkdvoran.SelectedValue = "0";
                txtkdvtutar.Text = "0";
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg235"));
            }

            if ((ddltur1.SelectedValue + ddltur2.SelectedValue + ddltur3.SelectedValue) == "6160401")
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg236"));
                ddlkdvoran.SelectedValue = "0";
            }

        }
        protected void btnsatirsil_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow grdRow in grdmasraf.Rows)
            {
                if (((CheckBox)grdRow.FindControl("Chksec")).Checked)
                {
                    Label lblsatirno = (Label)grdmasraf.Rows[grdRow.RowIndex].FindControl("lblsatirno");

                    string[] _params= { _obj.UserName, lblsatirno.Text, _obj.NameSurName };
                    NavServicesContents serviceContent = new NavServicesContents();
                    string[] _pars = serviceContent.NavServiceParams("MasrafSatırSil", _params);
                    GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "AdayOlustur§" + string.Join("§", _params), _obj.Comp);

                }
            }
            masrafgirdoldur(_obj.Comp, _obj.UserName, lblbelgeno.Text, grdmasraf, txtdnakil, ddlalacakhesabı);
            lblkaynak.Text = string.Empty;
            if (grdmasraf.Rows.Count > 0)
            {
                txtdnakil.Enabled = false;
                ddlalacakhesabı.Enabled = false;
                chper.Enabled = false;
            }
            else
            {
                txtdnakil.Enabled = true;
                ddlalacakhesabı.Enabled = true;
                chper.Enabled = true;
            }
            lbltoplamsatir.Text = toplamsecilisatir().ToString();
            Toplamsatırtutarı();
        }
        protected void ddltur1_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtkaynak.Text = string.Empty;
            f.tur2doldur(_obj.Comp, ddltur1.SelectedValue, ddltur2);
            f.tur3doldur(_obj.Comp, ddltur1.SelectedValue, ddltur2.SelectedValue, ddltur3);
            f.KaynakKoduDoldur(_obj.Comp, ddltur1.SelectedValue, ddltur2.SelectedValue, ddltur3.SelectedValue, lblkaynak, txtkaynak);

            if (ddltur1.SelectedItem.ToString().Substring(0, 1) == "6")
            {
                AutoCompleteExtender1.ContextKey = AutoCompleteExtender1.ContextKey + ":Genelis";
            }
            else
            {
                AutoCompleteExtender1.ContextKey = AutoCompleteExtender1.ContextKey.Replace(":Genelis", "");
            }
        }
        protected void ddltur2_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtkaynak.Text = string.Empty;
            f.tur3doldur(_obj.Comp, ddltur1.SelectedValue, ddltur2.SelectedValue, ddltur3);
            f.KaynakKoduDoldur(_obj.Comp, ddltur1.SelectedValue, ddltur2.SelectedValue, ddltur3.SelectedValue, lblkaynak, txtkaynak);
        }
        protected void ddltur3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (f.KaynakKodKontrol(_obj.Comp, ddltur1.SelectedValue.ToString(), ddltur2.SelectedValue.ToString(), ddltur3.SelectedValue.ToString()))
            {
                txtkaynak.Enabled = true;
                txtkaynak.Text = string.Empty;
                if ((ddltur1.SelectedValue + ddltur2.SelectedValue + ddltur3.SelectedValue) == "6160402")
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg237"));
                    ddltur3.SelectedIndex = -1;
                }

                if ((ddltur1.SelectedValue + ddltur2.SelectedValue + ddltur3.SelectedValue) == "6160401")
                {
                    ddlkdvoran.SelectedValue = "0";
                    txtkdvtutar.Text = "0";
                    if (txtnettutar.Text != string.Empty && ddlkdvoran.SelectedValue != string.Empty)
                    {
                        decimal kdvdahil = (Convert.ToDecimal(txtnettutar.Text) * Convert.ToDecimal(ddlkdvoran.SelectedValue)) / 100;
                        txtkdvtutar.Text = kdvdahil.ToString();
                        txtkdvdahil.Text = (Convert.ToDecimal(txtnettutar.Text) + kdvdahil).ToString();
                    }

                }

                if (((ddltur1.SelectedValue + ddltur2.SelectedValue + ddltur3.SelectedValue) == "7409090") || ((ddltur1.SelectedValue + ddltur2.SelectedValue + ddltur3.SelectedValue) == "7709090"))
                {
                    ddlkdvoran.SelectedValue = "0";
                    txtkdvtutar.Text = "0";
                    txtkdvdahil.Text = txtnettutar.Text;
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg238"));
                }

                f.KaynakKoduDoldur(_obj.Comp, ddltur1.SelectedValue, ddltur2.SelectedValue, ddltur3.SelectedValue, lblkaynak, txtkaynak);
            }
            else
            {
                ddltur3.SelectedValue = "";
                txtkaynak.Text = string.Empty;
                txtkaynak.Enabled = false;
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg068"));
            }






        }
        protected void txtkdvdahil_TextChanged(object sender, EventArgs e)
        {
            if (ddlkdvoran.SelectedValue != string.Empty && txtkdvdahil.Text != string.Empty)
            {
                decimal kdvoranı = 1 + (Convert.ToDecimal(ddlkdvoran.SelectedValue) / 100);
                decimal kdvharic = Math.Round((Convert.ToDecimal(txtkdvdahil.Text) / kdvoranı), 2);
                txtnettutar.Text = kdvharic.ToString();
                txtkdvtutar.Text = Math.Round((Convert.ToDecimal(txtkdvdahil.Text) - kdvharic), 2).ToString();
            }
            else
            {
                txtnettutar.Text = txtkdvdahil.Text;
            }
            if (txtkdvdahil.Text == string.Empty)
            {
                txtnettutar.Text = string.Empty;
                txtkdvdahil.Text = string.Empty;
                ddlkdvoran.SelectedIndex = -1;
            }
        }
        protected void btndosya_Click(object sender, EventArgs e)
        {
            Response.Write("<script>window.open('dosyabagla.aspx?BelgeNo=" + lblbelgeno.Text + "&Tur=2&dtur=2&GId=" + Request.QueryString["GId"]+"','List','scrollbars=no,resizable=no,width=650,height=460,top=200,left=400');</script>");
        }
        protected void btndosyagor_Click(object sender, EventArgs e)
        {
            Response.Write("<script>window.open('dosyagor.aspx?BelgeNo=" + lblbelgeno.Text + "&Tur=2&dtur=2&GId=" + Request.QueryString["GId"]+"','List','scrollbars=no,resizable=no,width=650,height=460,top=200,left=400');</script>");
        }
        protected void btnonayla_Click(object sender, EventArgs e)
        {
            if (!btndosyagoster.Visible)
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg239"));
                return;
            }
            if (grdmasraf.Rows.Count > 0)
            {

                string[] t1cevir = txtdnakil.Text.Split('.');

                string[] _params = { _obj.UserName, lblbelgeno.Text, t1cevir[1] + t1cevir[0] + t1cevir[2], ddlalacakhesabı.SelectedValue, "0" };
                GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "MasrafMuhasebeyeGonder§" + string.Join("§", _params), _obj.Comp);


                if (grdmasraf.Rows.Count > 0)
                {
                    txtdnakil.Enabled = false;
                }

                string[] _params2= { _obj.UserName }; 
                lblbelgeno.Text = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "MasrafSonBelgeNoAl§" + string.Join("§", _params), _obj.Comp);

                masrafgirdoldur(_obj.Comp, _obj.UserName, lblbelgeno.Text, grdmasraf, txtdnakil, ddlalacakhesabı);
                lbltoplamsatir.Text = toplamsecilisatir().ToString();
                ddlalacakhesabı.SelectedIndex = -1;
                txtdnakil.Enabled = true;
                ddlalacakhesabı.Enabled = true;
                chper.Enabled = true;
                txtdnakil.Text = string.Empty;
                lblbaglanmısdosya.Visible = false;
                btndosyagoster.Visible = false;
                chper.Checked = false;

            }
            else
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg240"));
            }

        }
        protected void ddlparabirimi_SelectedIndexChanged(object sender, EventArgs e)
        {
            f.kurgetir(_obj.Comp, txtkur, txtbelgetarihi.Text, ddlparabirimi.SelectedValue);
            if (ddlparabirimi.SelectedValue == "")
            {
                txtkur.Enabled = false;
            }
            else
            {
                txtkur.Enabled = true;
            }
        }
        protected void btndosyagoster_Click(object sender, EventArgs e)
        {
            string path = "";
            path = "." + lbldosyapath.Text;
            path = path.Replace("..", "");
            path = path.Replace("\\", "\\\\");
            if (Request.Url.ToString().Contains(ws.dısip))
            {

                Response.Write("<script>window.open('" + ws.dısip + path + "','List','scrollbars=no,resizable=no,width=1000,height=460,top=0,left=0');</script>");
            }
            else
            {
                Response.Write("<script>window.open('" + ws.icip + path + "','List','scrollbars=no,resizable=no,width=1000,height=460,top=0,left=0');</script>");
            }
        }
        public void masrafgirdoldur(string sirket, string kadi, string belgeno, GridView grd, TextBox txt, DropDownList ddl)
        {
            baglanti = new SqlConnection(strConnString);
            baglanti.Open();
            SqlCommand sorgu = new SqlCommand();

            sorgu.Connection = baglanti;
            sorgu.CommandType = CommandType.StoredProcedure;
            sorgu.CommandText = "sp_0WEB_MASRAF_GIR_DOLDUR";
            SqlParameter p_sirket = new SqlParameter("@sirket", SqlDbType.VarChar, 20);
            p_sirket.Value = sirket;
            sorgu.Parameters.Add(p_sirket);
            SqlParameter bno = new SqlParameter("@bno", SqlDbType.VarChar, 20);
            bno.Value = belgeno;
            sorgu.Parameters.Add(bno);
            SqlDataAdapter sorguDA = new SqlDataAdapter(sorgu);
            DataTable sorgugridDT = new DataTable();
            sorguDA.Fill(sorgugridDT);
            SqlDataReader sqr;
            sqr = sorgu.ExecuteReader();
            if (sqr.Read())
            {
                txt.Text = sqr["dftarih"].ToString();

                chper.Checked = Convert.ToBoolean(sqr["saticimi"].ToString());
                if (sqr["karsihesap"].ToString() != "" || sqr["karsihesap"].ToString() != null)
                {
                    ddlalacakhesabı.SelectedValue = sqr["karsihesap"].ToString();
                }
            }
            grd.DataSource = sorgugridDT;
            grd.DataBind();
            sqr.Close();
            sqr = null;

        }
        protected void chper_CheckedChanged(object sender, EventArgs e)
        {
            if (chper.Checked)
            {
                ddlalacakhesabı.Items.Clear();
                //f.alacakhesabıdoldur(_obj.Comp, ddlalacakhesabı, 3, _obj.UserName);
                f.bankalarigetir2(_obj.Comp, _obj.UserName, ddlalacakhesabı);
            }
            else
            {
                ddlalacakhesabı.Items.Clear();
                f.alacakhesabıdoldur(_obj.Comp, ddlalacakhesabı, 0, _obj.UserName);
            }
        }
        protected void btnfaturasatirkopyala_Click(object sender, EventArgs e)
        {
            int sayac = 0;
            int index = 0;
            foreach (GridViewRow grdRow in grdmasraf.Rows)
            {

                if (((CheckBox)grdRow.FindControl("Chksec")).Checked)
                {
                    index = grdRow.RowIndex;
                    sayac++;
                }
            }
            if (sayac == 1)
            {
                foreach (GridViewRow grdRow in grdmasraf.Rows)
                {

                    if (((CheckBox)grdRow.FindControl("Chksec")).Checked)
                    {
                        Label lblkhesapadisatir = (Label)grdmasraf.Rows[grdRow.RowIndex].FindControl("lblkhesapadisatir");
                        Label lblbtarih = (Label)grdmasraf.Rows[grdRow.RowIndex].FindControl("lblbtarih");
                        Label lblfirma = (Label)grdmasraf.Rows[grdRow.RowIndex].FindControl("lblfirma");
                        Label lblhbelgeno = (Label)grdmasraf.Rows[grdRow.RowIndex].FindControl("lblhbelgeno");
                        Label lblvergino = (Label)grdmasraf.Rows[grdRow.RowIndex].FindControl("lblvergino");
                        Label lbldovizcinsi = (Label)grdmasraf.Rows[grdRow.RowIndex].FindControl("lbldovizcinsi");
                        Label lblkur = (Label)grdmasraf.Rows[grdRow.RowIndex].FindControl("lblkur");
                        Label lblkdvharic = (Label)grdmasraf.Rows[grdRow.RowIndex].FindControl("lblkdvharic");
                        Label lblkdvoran = (Label)grdmasraf.Rows[grdRow.RowIndex].FindControl("lblkdvoran");
                        Label lblkdvtutari = (Label)grdmasraf.Rows[grdRow.RowIndex].FindControl("lblkdvtutari");
                        Label lblkdahiltutar = (Label)grdmasraf.Rows[grdRow.RowIndex].FindControl("lblkdahiltutar");
                        Label lbltutartlg = (Label)grdmasraf.Rows[grdRow.RowIndex].FindControl("lbltutartl");
                        Label lblbolgekod = (Label)grdmasraf.Rows[grdRow.RowIndex].FindControl("lblbolgekod");
                        Label lbldepartmankod = (Label)grdmasraf.Rows[grdRow.RowIndex].FindControl("lbldepartmankod");
                        Label lblfaaliyetkod = (Label)grdmasraf.Rows[grdRow.RowIndex].FindControl("lblfaaliyetkod");
                        Label lblpersonelkod = (Label)grdmasraf.Rows[grdRow.RowIndex].FindControl("lblpersonelkod");
                        Label lblarackod = (Label)grdmasraf.Rows[grdRow.RowIndex].FindControl("lblarackod");
                        Label lbltur1kod = (Label)grdmasraf.Rows[grdRow.RowIndex].FindControl("lbltur1kod");
                        Label lbltur2kod = (Label)grdmasraf.Rows[grdRow.RowIndex].FindControl("lbltur2kod");
                        Label lbltur3kod = (Label)grdmasraf.Rows[grdRow.RowIndex].FindControl("lbltur3kod");
                        Label lblisadi = (Label)grdmasraf.Rows[grdRow.RowIndex].FindControl("lblisadi");
                        Label lblisgorevadi = (Label)grdmasraf.Rows[grdRow.RowIndex].FindControl("lblisgorevadi");
                        lblkhesapadi.Text = lblkhesapadisatir.Text;
                        txtbelgetarihi.Text = lblbtarih.Text;
                        txtfirma.Text = lblfirma.Text;
                        txtharicibelgeno.Text = lblhbelgeno.Text;
                        txtvergino.Text = lblvergino.Text;
                        ddlparabirimi.SelectedValue = lbldovizcinsi.Text;
                        ddlparabirimi_SelectedIndexChanged(ddlparabirimi, EventArgs.Empty);
                        txtkur.Text = lblkur.Text;
                        txtnettutar.Text = lblkdvharic.Text;
                        txtnettutar_TextChanged(txtnettutar, EventArgs.Empty);
                        ddlkdvoran.SelectedValue = lblkdvoran.Text;
                        ddlkdvoran_SelectedIndexChanged(ddlkdvoran, EventArgs.Empty);
                        txtkdvtutar.Text = lblkdvtutari.Text;
                        txtkdvdahil.Text = lblkdahiltutar.Text;
                        txtkdvdahil_TextChanged(txtkdvdahil, EventArgs.Empty);
                        lbltutartl.Text = lbltutartlg.Text;
                        ddlbolge.SelectedValue = lblbolgekod.Text;
                        ddldepartman.SelectedValue = lbldepartmankod.Text;
                        ddlfaaliyet.SelectedValue = lblfaaliyetkod.Text;
                        ddlpersonel.SelectedValue = lblpersonelkod.Text;
                        ddlarac.SelectedValue = lblarackod.Text;
                        ddltur1.SelectedValue = lbltur1kod.Text;
                        ddltur1_SelectedIndexChanged(ddltur1, EventArgs.Empty);
                        ddltur2.SelectedValue = lbltur2kod.Text;
                        ddltur2_SelectedIndexChanged(ddltur2, EventArgs.Empty);
                        ddltur3.SelectedValue = lbltur3kod.Text;
                        ddltur3_SelectedIndexChanged(ddltur3, EventArgs.Empty);
                        txtprojeadi.Text = lblisadi.Text;
                        txtprojeadi_TextChanged(txtprojeadi, EventArgs.Empty);
                        txtprojegorev.Text = lblisgorevadi.Text;
                        txtprojegorev_TextChanged(txtprojegorev, EventArgs.Empty);
                    }
                }
            }
            else if (sayac == 0)
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg241"));
            }
            else
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg242"));
            }
        }
        protected void txtkaynak_TextChanged(object sender, EventArgs e)
        {
            if (lblkaynak.Text != string.Empty)
            {
                if (f.KKEGbul(_obj.Comp, lblkaynak.Text) == 1)
                {
                    if (ddlkdvoran.SelectedValue == "1" || ddlkdvoran.SelectedValue == "18" || ddlkdvoran.SelectedValue == "8")
                    {
                        MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg243"));
                        ddlkdvoran.SelectedIndex = -1;
                    }
                }
            }
        }
        protected void txtdnakil_TextChanged(object sender, EventArgs e)
        {
            if (txtdnakil.Text != "" && txtbelgetarihi.Text != "")
            {
                if (Convert.ToDateTime(txtdnakil.Text) < Convert.ToDateTime(txtbelgetarihi.Text))
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg459"));
                    txtdnakil.Text = "";
                    return;
                }
            }


            if (txtprojeadi.Text != string.Empty)
            {
                string tarih;
                int tarihkarakter = txtdnakil.Text.Length - 4;
                tarih = txtdnakil.Text.Substring(tarihkarakter, 4);
                if ((tarih == "2014") && (lblprojeno.Text == "G130001"))
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg244"));
                    txtdnakil.Text = string.Empty;
                }

                if ((tarih == "2013") && (lblprojeno.Text == "G1400001"))
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg245"));
                    txtdnakil.Text = string.Empty;
                }
            }
        }
        protected void ddlarac_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlpersonel.SelectedIndex = 0;
            ddldepartman.SelectedIndex = 0;
            ddlbolge.SelectedIndex = 0;
            DataTable dt = f.datatablegonder("select [Personel Kodu],[Departman Kodu],[Bolge Kodu] from [0C_50029_00_HAT_ARAC_KAYIT] where COMPANY='" + _obj.Comp + "' AND [Arac Boyutu] ='" + ddlarac.SelectedItem.ToString() + "' and  [Arac Boyutu]!= '' ");

            try
            {
                ddlpersonel.SelectedValue = dt.Rows[0][0].ToString();
            }
            catch (Exception)
            {
            }
            try
            {
                ddldepartman.SelectedValue = dt.Rows[0][1].ToString();
            }
            catch (Exception)
            {

            }

            try
            {
                ddlbolge.SelectedValue = dt.Rows[0][2].ToString();
            }
            catch (Exception)
            {
            }
            if (ddlbolge.SelectedIndex > 0)
            {
                ddlbolge.Enabled = false;
            }
            if (ddldepartman.SelectedIndex > 0)
            {
                ddldepartman.Enabled = false;
            }
            if (ddlpersonel.SelectedIndex > 0)
            {
                ddlpersonel.Enabled = false;
            }

            if (ddlarac.SelectedItem.ToString() == "YOK")
            {
                ddlpersonel.Enabled = true;
                ddldepartman.Enabled = true;
                ddlbolge.Enabled = true;
            }
        }
        protected void ddldepartman_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (ddldepartman.SelectedValue)
            {
                case "1088":
                    ddlbolge.SelectedValue = "113110";
                    break;
                case "2088":
                    ddlbolge.SelectedValue = "123410";
                    break;
                case "3088":
                    ddlbolge.SelectedValue = "123420";
                    break;
                case "4088":
                    ddlbolge.SelectedValue = "123410";
                    break;
            }
        }
        protected void ddlpersonel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((ddlarac.SelectedIndex == 0 || ddlarac.SelectedItem.ToString() == "YOK") && (ddlpersonel.SelectedIndex > 0 && ddlpersonel.SelectedItem.ToString() != "YOK"))
            {
                ddlbolge.SelectedValue = f.tekbirsonucdonder("select BLG from [0C_05200_00_EMPLOYEE] where COMPANY='" + _obj.Comp + "' and  No_ ='" + ddlpersonel.SelectedValue + "'");
                ddldepartman.SelectedValue = f.tekbirsonucdonder("select DEPT from [0C_05200_00_EMPLOYEE] where COMPANY='" + _obj.Comp + "' and No_ ='" + ddlpersonel.SelectedValue + "'");
                ddlbolge.Enabled = false;
                ddldepartman.Enabled = false;
            }
            else if (ddlpersonel.SelectedIndex == 0 && (ddlarac.SelectedIndex == 0 || ddlarac.SelectedItem.ToString() == "YOK"))
            {
                ddlbolge.Enabled = true;
                ddldepartman.Enabled = true;
                ddlbolge.SelectedIndex = 0;
                ddldepartman.SelectedIndex = 0;
            }
        }

        protected void txtkur_TextChanged(object sender, EventArgs e)
        {
            decimal sonuc, sonuc2, sayi, sayi2, deger;

            kurgetir2(lbldeger2, txtbelgetarihi.Text, ddlparabirimi.SelectedValue);

            deger = Convert.ToDecimal(lbldeger2.Text);

            sayi = Convert.ToDecimal("0.9");
            sayi2 = Convert.ToDecimal("1.1");

            sonuc = deger * sayi;
            sonuc2 = deger * sayi2;

            if (Convert.ToDecimal(txtkur.Text.Replace(",", ".")) < sonuc)
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg069"));
                txtkur.Text = string.Empty;
            }
            else if (Convert.ToDecimal(txtkur.Text.Replace(",", ".")) > sonuc2)
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg069"));
                txtkur.Text = string.Empty;
            }
        }

        public void kurgetir2(Label kur, string tarih, string dovizcinsi)
        {
            baglanti = new SqlConnection(strConnString);
            try
            {
                string tarihcevir = "";
                if (tarih.Length == 10)
                {
                    tarihcevir = tarih;
                    string[] tcevir = tarihcevir.Split('.');

                    baglanti.Open();
                    SqlCommand sorgu = new SqlCommand("Select CONVERT(decimal(10,4),[Relational Exch_ Rate Amount]) as kur from [00_EXCHANGE RATES]  where COMPANY='" + _obj.Comp + "' and [Starting Date]='" + tcevir[1] + "." + tcevir[0] + "." + tcevir[2] + "' and [Currency Code]='" + dovizcinsi + "'", baglanti);
                    SqlDataReader sqr;
                    sqr = sorgu.ExecuteReader();
                    if (sqr.Read())
                    {
                        kur.Text = sqr["kur"].ToString();
                    }
                    else
                    {
                        kur.Text = "0,00";
                    }

                    sqr.Close();
                }
                else
                {
                    kur.Text = "0,00";
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                baglanti.Close();
            }



        }

        protected void txtbelgetarihi_TextChanged(object sender, EventArgs e)
        {

            string[] tcevir = txtbelgetarihi.Text.Split('.');
            string[] tcevir2 = txtdnakil.Text.Split('.');
            DateTime dt1 = new DateTime(Convert.ToInt32(tcevir[2]), Convert.ToInt32(tcevir[1]), Convert.ToInt32(tcevir[0]));
            DateTime dt2 = new DateTime(Convert.ToInt32(tcevir2[2]), Convert.ToInt32(tcevir2[1]), Convert.ToInt32(tcevir2[0]));
            if (txtbelgetarihi.Text != "" && txtdnakil.Text != "")
            {
                if (dt1 > dt2)
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "masrafgirisi", "msg071"));
                    txtbelgetarihi.Text = "";
                }


            }
            f.kurgetir(_obj.Comp, txtkur, txtbelgetarihi.Text, ddlparabirimi.SelectedValue);
        }
    }
}