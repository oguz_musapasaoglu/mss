﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Text;
using MSS1.Codes;

namespace MSS1
{
    public partial class SalesReportDocuments : Bases
    {
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        SessionBase _obj;
        protected void Page_Load(object sender, EventArgs e)
        {
            _obj = TabSessions.ControlState(this.Page, Request.QueryString["GId"].ToString());
        }

        protected void btndocTopla_Click(object sender, EventArgs e)
        {
            Array.ForEach(Directory.GetFiles(Server.MapPath(CurrentFolderName(_obj.Comp) + @"\KDV Belgeleri\Dosyalar")), File.Delete);
            string TargetPath = "", FilePath = "";

            DataTable dt = Getir();
            List<ListItem> ErrorList = new List<ListItem>();
            ListItem li = new ListItem();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                try
                {
                    //FilePath = Server.MapPath(CurrentFolderName(_obj.Comp) + dt.Rows[i]["FATURAPATH"].ToString());
                    FilePath = dt.Rows[i]["FATURAPATH"].ToString();
                    TargetPath = Server.MapPath(CurrentFolderName(_obj.Comp) + @"/KDV Belgeleri\Dosyalar\" + Path.GetFileName(FilePath));

                    if (File.Exists(FilePath))
                    {
                        if (!File.Exists(TargetPath))
                        {
                            try
                            {
                                File.Copy(FilePath, TargetPath);
                            }
                            catch (Exception)
                            {
                                li.Text = FilePath;
                                li.Value = "";
                                ErrorList.Add(li);
                            }

                        }
                        else
                        {
                        }
                    }
                    else
                    {
                        if (FilePath != Server.MapPath(CurrentFolderName(_obj.Comp) + ""))
                        {
                            li.Text = FilePath;
                            li.Value = "";
                            ErrorList.Add(li);
                        }

                    }
                }
                catch (Exception)
                {
                    li.Text = FilePath;
                    li.Value = "";
                    ErrorList.Add(li);
                }




                try
                {
                    //FilePath = Server.MapPath(CurrentFolderName(_obj.Comp) + dt.Rows[i]["BLPATH"].ToString());


                    FilePath = dt.Rows[i]["BLPATH"].ToString();
                    TargetPath = Server.MapPath(CurrentFolderName(_obj.Comp) + @"/KDV Belgeleri\Dosyalar\" + Path.GetFileName(FilePath));

                    if (File.Exists(FilePath))
                    {
                        if (!File.Exists(TargetPath))
                        {
                            try
                            {
                                File.Copy(FilePath, TargetPath);
                            }
                            catch (Exception)
                            {
                                li.Text = FilePath;
                                li.Value = "";
                                ErrorList.Add(li);
                            }
                        }
                    }
                    else
                    {
                        if (FilePath != Server.MapPath(CurrentFolderName(_obj.Comp) + ""))
                        {
                            li.Text = FilePath;
                            li.Value = "";
                            ErrorList.Add(li);
                        }

                    }
                }
                catch (Exception)
                {
                    li.Text = FilePath;
                    li.Value = "";
                    ErrorList.Add(li);
                }


            }

            FileInfo fi = new FileInfo(Server.MapPath(CurrentFolderName(_obj.Comp) + @"/KDV Belgeleri\ErrorList.txt"));
            using (StreamWriter sw = new StreamWriter(fi.Open(FileMode.Truncate)))
            {

                sw.WriteLine("                           ********** ERROR LİST **********");
                foreach (ListItem item in ErrorList.ToList())
                {
                    sw.WriteLine("Hedef Adreste Bulunamadı : " + item.Text + "...");
                }

            }

            ExcelExport();

        }

        private void ExcelExport()
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=List.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            Response.ContentEncoding = System.Text.Encoding.Unicode;
            Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                {
                    rptSource.RenderControl(hw);
                    Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                }
            }


        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        public DataTable Getir()
        {
            SqlConnection conn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand("sp_0WEB_KDVLI_FATURA_DOKUMANLARI_00", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@COMPANY", Session["Sirket"].ToString());
            cmd.Parameters.AddWithValue("@USER", Session["kulad"].ToString());
            cmd.Parameters.AddWithValue("@AY", ddlAy.SelectedValue);
            cmd.Parameters.AddWithValue("@YIL", ddlYil.SelectedValue);
            cmd.Parameters.AddWithValue("@DURUM", ddlKdvDurum.SelectedValue);
            cmd.Parameters.AddWithValue("@ANADEPT", ddlDepartman.SelectedValue);

            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adp.Fill(dt);
            rptSource.DataSource = dt;
            rptSource.DataBind();
            return dt;
        }
    }
}