﻿<%@ Application Language="C#" %>

<script RunAt="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup
        
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs

    }

    void Application_BeginRequest(object sender, EventArgs e)
    {
        try
        {
          if (HttpContext.Current.Request.Url != null)
            {
                if (HttpContext.Current.Request.Url.ToString().Contains("PDF"))
                {
                    if(HttpContext.Current.Session["uyedurum"]!="tamam")
                    {
                         HttpContext.Current.Response.Redirect("~/Default.aspx");
                    }
                }
            }
        
        }
        catch
        {
            HttpContext.Current.Response.Redirect("~/Default.aspx");
        }              
    
    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>
