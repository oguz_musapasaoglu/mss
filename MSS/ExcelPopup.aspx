﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExcelPopup.aspx.cs" Inherits="MSS1.ExcelPopup" %>

<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpreadsheet.v19.1" Namespace="DevExpress.Web.ASPxSpreadsheet" TagPrefix="dx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function OnExceptionOccurred(s, e) {
            e.handled = true;
            alert(e.message);
            window.location.reload();
        }
        function MenuItemClick(e) {
            var i = e.item.name;
            if (i == "Save")
                document.getElementById('txtfocus').focus();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField runat="server" ID="HiddenBlNo1" />
         <asp:HiddenField runat="server" ID="HiddenBlNo2" />
        <div>
            <div style="display:none">
                <asp:TextBox runat="server" ID="txtfocus"></asp:TextBox>
                <asp:TextBox runat="server" ID="txtLineNos"></asp:TextBox>
            </div>
            <dx:ASPxMenu ID="menu1" ClientInstanceName="menu1" runat="server" AutoSeparators="RootOnly"
                Theme="Glass"
                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                ShowSubMenuShadow="False" OnItemClick="menu1_ItemClick">
                <SubMenuStyle GutterWidth="0px" />
                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                </SubMenuItemStyle>
                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                <Items>
                    <dx:MenuItem Name="Save" Text="Save" Image-Url="~/images/addfileandsave.png">
                    </dx:MenuItem>

                </Items>
                <ClientSideEvents ItemClick="function(s, e) { MenuItemClick(e); }" />
            </dx:ASPxMenu>
            <dx:ASPxSpreadsheet ID="Spreadsheet" ClientInstanceName="Spreadsheet" RibbonMode="None" ShowFormulaBar="false" runat="server" Width="100%" Height="600px" ActiveTabIndex="0" ShowConfirmOnLosingChanges="false">
                <clientsideevents callbackerror="OnExceptionOccurred"></clientsideevents>
            </dx:ASPxSpreadsheet>
        </div>
    </form>
</body>
</html>

