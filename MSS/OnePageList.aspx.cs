﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.Data;
using System.Collections;
using System.IO;
using MSS1.WSGeneric;
using System.ServiceModel;
using MSS1.Codes;


namespace MSS1
{
    public partial class OnePageList : Bases
    {

        Hashtable copiedValues = null;
        SessionBase _obj;
        static string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;

        public string currentP1 { get; set; }

        public string currentP2 { get; set; }
        public string OBJ1_pageId { get; set; }
        public string OBJ2_pageId { get; set; }
        public string OBJ3_pageId { get; set; }
        public string OBJ4_pageId { get; set; }
        public string OBJ5_pageId { get; set; }
        public string OBJ6_pageId { get; set; }
        public string OBJ1_fieldId { get; set; }
        public string OBJ2_fieldId { get; set; }
        public string OBJ3_fieldId { get; set; }
        public string OBJ4_fieldId { get; set; }
        public string OBJ5_fieldId { get; set; }
        public string OBJ6_fieldId { get; set; }

        private void QueryStringParameterDesing()
        {
            string[] obj1, obj2, obj3, obj4, obj5, obj6 = { };

            obj1 = Request.QueryString["OJ1"].ToString().Split(';');
            obj2 = Request.QueryString["OJ2"].ToString().Split(';');
            obj3 = Request.QueryString["OJ3"].ToString().Split(';');
            obj4 = Request.QueryString["OJ4"].ToString().Split(';');
            obj5 = Request.QueryString["OJ5"].ToString().Split(';');
            obj6 = Request.QueryString["OJ6"].ToString().Split(';');


            try
            {
                OBJ1_pageId = obj1[0].ToString();
                OBJ1_fieldId = obj1[1].ToString();

                OBJ2_pageId = obj2[0].ToString();
                OBJ2_fieldId = obj2[1].ToString();

                OBJ3_pageId = obj3[0].ToString();
                OBJ3_fieldId = obj3[1].ToString();

                OBJ4_pageId = obj4[0].ToString();
                OBJ4_fieldId = obj4[1].ToString();

                OBJ5_pageId = obj5[0].ToString();
                OBJ5_fieldId = obj5[1].ToString();

                OBJ6_pageId = obj6[0].ToString();
                OBJ6_fieldId = obj6[1].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["P1"]))
                    HiddenP1.Value = Request.QueryString["P1"].ToString();

                if (!string.IsNullOrEmpty(Request.QueryString["P2"]))
                    HiddenP2.Value = Request.QueryString["P2"].ToString();

            }
            catch (Exception)
            {
                OBJ1_pageId = string.Empty;
                OBJ1_fieldId = string.Empty;
                OBJ2_pageId = string.Empty;
                OBJ2_fieldId = string.Empty;
                OBJ3_pageId = string.Empty;
                OBJ3_fieldId = string.Empty;
                OBJ4_pageId = string.Empty;
                OBJ4_fieldId = string.Empty;
                OBJ5_pageId = string.Empty;
                OBJ5_fieldId = string.Empty;
                OBJ6_pageId = string.Empty;
                OBJ6_fieldId = string.Empty;
            }



            grid2.JSProperties["cpIsUpdated"] = false;
            grid3.JSProperties["cpIsUpdated"] = false;
            grid4.JSProperties["cpIsUpdated"] = false;
            grid5.JSProperties["cpIsUpdated"] = false;
            grid6.JSProperties["cpIsUpdated"] = false;

        }

        webObjectHandler woh = new webObjectHandler();
        ServiceUtilities services = new ServiceUtilities();
        fonksiyonlar f = new fonksiyonlar();
        Methods method = new Methods(); 
        public ws_baglantı ws = new ws_baglantı();

        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            QueryStringParameterDesing();

            if (IsPostBack || IsCallback) return;



            GetParameters();
            CollapseDisplay();

            CallGrid1Data();
            CallGrid2Data("RCOMP='AA'");
            CallGrid3Data("RCOMP='AA'");
            CallGrid4Data("RCOMP='AA'");
            CallGrid5Data("RCOMP='AA'");
            CallGrid6Data("RCOMP='AA'");

            Session["OPL10Grid1Rows"] = webObjectHandler.GetDynamicWebPageObjects(OBJ1_pageId, OBJ1_fieldId, _obj.Comp).Tables[4];
            Session["OPL10Grid2Rows"] = webObjectHandler.GetDynamicWebPageObjects(OBJ2_pageId, OBJ2_fieldId, _obj.Comp).Tables[4];
            Session["OPL10Grid3Rows"] = webObjectHandler.GetDynamicWebPageObjects(OBJ3_pageId, OBJ3_fieldId, _obj.Comp).Tables[4];
            Session["OPL10Grid4Rows"] = webObjectHandler.GetDynamicWebPageObjects(OBJ4_pageId, OBJ4_fieldId, _obj.Comp).Tables[4];
            Session["OPL10Grid5Rows"] = webObjectHandler.GetDynamicWebPageObjects(OBJ5_pageId, OBJ5_fieldId, _obj.Comp).Tables[4];
            Session["OPL10Grid6Rows"] = webObjectHandler.GetDynamicWebPageObjects(OBJ6_pageId, OBJ6_fieldId, _obj.Comp).Tables[4];

            FillDS1((DataTable)Session["OPL10Grid1Rows"]);
            //grid1.DataBind();

            FillDS2((DataTable)Session["OPL10Grid2Rows"]);
            grid2.DataBind();

            FillDS3((DataTable)Session["OPL10Grid3Rows"]);
            grid3.DataBind();

            FillDS4((DataTable)Session["OPL10Grid4Rows"]);
            grid4.DataBind();

            FillDS5((DataTable)Session["OPL10Grid5Rows"]);
            grid5.DataBind();

            FillDS6((DataTable)Session["OPL10Grid6Rows"]);
            grid6.DataBind();

            RadioListrOder();

            if (Request.QueryString["P1"] != null)
            {
                HiddenP1.Value = Request.QueryString["P1"].ToString();
                if (Request.QueryString["P2"] != null)
                    HiddenP2.Value = Request.QueryString["P2"].ToString();
                string link = HiddenpageType.Value;
                link = link.Replace("XXXXX", HiddenP1.Value);
                link = link.Replace("YYYYY", HiddenP2.Value);
                link = link.Replace("undefined", "TOTAL");
                this.iframeView.Attributes.Add("src", link);

                txtValues.Text = Request.QueryString["P1"].ToString();
                if (Request.QueryString["P2"] != null)
                    txtValues.Text += "," + Request.QueryString["P2"].ToString();
            }

            PreparePage();

            if (!ClientScript.IsStartupScriptRegistered("JSLoadMainData"))
            {
                ClientScript.RegisterStartupScript(GetType(), "JSLoadMainData", @"grid1.PerformCallback('LoadMainData');", true);
            }

        }

        private void PreparePage()
        {
            lblGId.Text = Request.QueryString["GId"];
            if (!string.IsNullOrEmpty(HiddenP1.Value))
            {
                string _link = HiddenpageType.Value.Replace("XXXXX", HiddenP1.Value);
                _link = _link.Replace("YYYYY", HiddenP2.Value);
                _link = _link.Replace("undefined", "TOTAL");
                iframeView.Attributes.Add("src", _link);

                //string script = string.Empty;
                //if (!string.IsNullOrEmpty(HiddenP2.Value))
                //    script = "OnGetRowValuesFilter('" + HiddenP1.Value + "','" + HiddenP2.Value + "')";
                //else
                //    script = "OnGetRowValuesFilter('" + HiddenP1.Value + "')";
                //ClientScript.RegisterStartupScript(this.Page.GetType(), "fnct", script, true);

                rpMain.Collapsed = false;

                string LP1 = string.Empty, LP2 = string.Empty;

                if (HiddendirectParam.Value.Contains(";"))
                {
                    LP1 = HiddendirectParam.Value.Split(';')[0];
                    LP2 = HiddendirectParam.Value.Split(';')[1];
                    grid1.FilterExpression = LP1 + "='" + HiddenP1.Value + "' And " + LP2 + " = '" + HiddenP2.Value + "'";
                }
                else
                {
                    LP1 = HiddendirectParam.Value;
                    grid1.FilterExpression = LP1 + "='" + HiddenP1.Value + "'";
                }

                grid1.DataSourceID = "dtgrid1";
                grid1.DataBind();
            }
        }

        void GetParameters()
        {
            string Pages = OBJ1_pageId + "," + OBJ2_pageId + "," + OBJ3_pageId + "," + OBJ4_pageId + "," + OBJ5_pageId + "," + OBJ6_pageId;
            string objects = OBJ1_fieldId + "," + OBJ2_fieldId + "," + OBJ3_fieldId + "," + OBJ4_fieldId + "," + OBJ5_fieldId + "," + OBJ6_fieldId;
            using (DataSet ds = f.GetDs(Pages, objects, _obj.Comp))
            {
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        txtlink1.Text = ds.Tables[0].Rows[0]["LINK"].ToString();
                        txtlinkField1.Text = ds.Tables[0].Rows[0]["LINK FIELD"].ToString();
                        HiddendirectParam.Value = ds.Tables[0].Rows[0]["PrimaryField"].ToString();
                        txtCatchParameters1.Text = ds.Tables[0].Rows[0]["Transferred Fields"].ToString();
                        rpMain.Collapsed = ds.Tables[0].Rows[0]["Default Collapse State"].ToString() == "1" ? false : true;
                    }

                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        txtlink2.Text = ds.Tables[1].Rows[0]["LINK"].ToString();
                        txtlinkField2.Text = ds.Tables[1].Rows[0]["LINK FIELD"].ToString();
                        txtCatchParameters2.Text = ds.Tables[1].Rows[0]["Transferred Fields"].ToString();
                        rpCargoDetails.Collapsed = ds.Tables[1].Rows[0]["Default Collapse State"].ToString() == "1" ? false : true;
                    }

                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        txtlink3.Text = ds.Tables[2].Rows[0]["LINK"].ToString();
                        txtlinkField3.Text = ds.Tables[2].Rows[0]["LINK FIELD"].ToString();
                        txtCatchParameters3.Text = ds.Tables[2].Rows[0]["Transferred Fields"].ToString();
                        rpCostSales.Collapsed = ds.Tables[2].Rows[0]["Default Collapse State"].ToString() == "1" ? false : true;
                    }

                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        txtlink4.Text = ds.Tables[3].Rows[0]["LINK"].ToString();
                        txtlinkField4.Text = ds.Tables[3].Rows[0]["LINK FIELD"].ToString();
                        txtCatchParameters4.Text = ds.Tables[3].Rows[0]["Transferred Fields"].ToString();
                        rpDocuments.Collapsed = ds.Tables[3].Rows[0]["Default Collapse State"].ToString() == "1" ? false : true;
                    }

                    if (ds.Tables[4].Rows.Count > 0)
                    {
                        txtlink5.Text = ds.Tables[4].Rows[0]["LINK"].ToString();
                        txtlinkField5.Text = ds.Tables[4].Rows[0]["LINK FIELD"].ToString();
                        txtCatchParameters5.Text = ds.Tables[4].Rows[0]["Transferred Fields"].ToString();
                        rpComments.Collapsed = ds.Tables[4].Rows[0]["Default Collapse State"].ToString() == "1" ? false : true;
                    }

                    if (ds.Tables[5].Rows.Count > 0)
                    {
                        txtlink6.Text = ds.Tables[5].Rows[0]["LINK"].ToString();
                        txtlinkField6.Text = ds.Tables[5].Rows[0]["LINK FIELD"].ToString();
                        txtCatchParameters6.Text = ds.Tables[5].Rows[0]["Transferred Fields"].ToString();
                        rpTasks.Collapsed = ds.Tables[5].Rows[0]["Default Collapse State"].ToString() == "1" ? false : true;
                    }

                }
            }

            Session["grid1Cloned"] = "0";
            Session["grid2Cloned"] = "0";
            Session["grid3Cloned"] = "0";
            Session["grid4Cloned"] = "0";
            Session["grid5Cloned"] = "0";
            Session["grid6Cloned"] = "0";

            //txtlink1.Text = f.tekbirsonucdonder("select LINK from WebPages where [Web Page ID]='" + OBJ1_pageId + "' and [Web Object ID]='" + OBJ1_fieldId + "'");
            //txtlinkField1.Text = f.tekbirsonucdonder("select [LINK FIELD] from WebPages where [Web Page ID]='" + OBJ1_pageId + "' and [Web Object ID]='" + OBJ1_fieldId + "'");
            //txtlink2.Text = f.tekbirsonucdonder("select LINK from WebPages where [Web Page ID]='" + OBJ2_pageId + "' and [Web Object ID]='" + OBJ2_fieldId + "'");
            //txtlinkField2.Text = f.tekbirsonucdonder("select [LINK FIELD] from WebPages where [Web Page ID]='" + OBJ2_pageId + "' and [Web Object ID]='" + OBJ2_fieldId + "'");
            //txtlink3.Text = f.tekbirsonucdonder("select LINK from WebPages where [Web Page ID]='" + OBJ3_pageId + "' and [Web Object ID]='" + OBJ3_fieldId + "'");
            //txtlinkField3.Text = f.tekbirsonucdonder("select [LINK FIELD] from WebPages where [Web Page ID]='" + OBJ3_pageId + "' and [Web Object ID]='" + OBJ3_fieldId + "'");
            //txtlink4.Text = f.tekbirsonucdonder("select LINK from WebPages where [Web Page ID]='" + OBJ4_pageId + "' and [Web Object ID]='" + OBJ4_fieldId + "'");
            //txtlinkField4.Text = f.tekbirsonucdonder("select [LINK FIELD] from WebPages where [Web Page ID]='" + OBJ4_pageId + "' and [Web Object ID]='" + OBJ4_fieldId + "'");
            //txtlink5.Text = f.tekbirsonucdonder("select LINK from WebPages where [Web Page ID]='" + OBJ5_pageId + "' and [Web Object ID]='" + OBJ5_fieldId + "'");
            //txtlinkField5.Text = f.tekbirsonucdonder("select [LINK FIELD] from WebPages where [Web Page ID]='" + OBJ5_pageId + "' and [Web Object ID]='" + OBJ5_fieldId + "'");
            //txtlink6.Text = f.tekbirsonucdonder("select LINK from WebPages where [Web Page ID]='" + OBJ6_pageId + "' and [Web Object ID]='" + OBJ6_fieldId + "'");
            //txtlinkField6.Text = f.tekbirsonucdonder("select [LINK FIELD] from WebPages where [Web Page ID]='" + OBJ6_pageId + "' and [Web Object ID]='" + OBJ6_fieldId + "'");
        }

        void FillDS1(DataTable dtRows)
        {
            int sayac = 1;
            string query = string.Empty;
            for (int i = 0; i < dtRows.Rows.Count; i++)
            {
                switch (dtRows.Rows[i]["DX Type1"].ToString())
                {
                    case "DATA":
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Source"].ToString()))
                        {
                            query = webObjectHandler.FillCombobox(dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(),
                                dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), _obj.Comp, _obj.UserName);
                            switch (sayac)
                            {
                                case 1:
                                    DS11.Table = f.datatablegonder(query);
                                    break;
                                case 2:
                                    DS12.Table = f.datatablegonder(query);
                                    break;
                                case 3:
                                    DS13.Table = f.datatablegonder(query);
                                    break;
                                case 4:
                                    DS14.Table = f.datatablegonder(query);
                                    break;
                                case 5:
                                    DS15.Table = f.datatablegonder(query);
                                    break;
                                case 6:
                                    DS16.Table = f.datatablegonder(query);
                                    break;
                                case 7:
                                    DS17.Table = f.datatablegonder(query);
                                    break;
                                case 8:
                                    DS18.Table = f.datatablegonder(query);
                                    break;
                                case 9:
                                    DS19.Table = f.datatablegonder(query);
                                    break;
                                case 10:
                                    DS110.Table = f.datatablegonder(query);
                                    break;
                                case 11:
                                    DS111.Table = f.datatablegonder(query);
                                    break;
                                case 12:
                                    DS112.Table = f.datatablegonder(query);
                                    break;
                                case 13:
                                    DS113.Table = f.datatablegonder(query);
                                    break;
                                case 14:
                                    DS114.Table = f.datatablegonder(query);
                                    break;
                                case 15:
                                    DS115.Table = f.datatablegonder(query);
                                    break;
                                case 16:
                                    DS116.Table = f.datatablegonder(query);
                                    break;
                                case 17:
                                    DS117.Table = f.datatablegonder(query);
                                    break;
                                case 18:
                                    DS118.Table = f.datatablegonder(query);
                                    break;
                                case 19:
                                    DS119.Table = f.datatablegonder(query);
                                    break;
                                case 20:
                                    DS120.Table = f.datatablegonder(query);
                                    break;
                            }
                            sayac++;
                        }
                        break;
                }
            }
        }

        void FillDS2(DataTable dtRows)
        {
            int sayac = 1;
            string query = string.Empty;
            for (int i = 0; i < dtRows.Rows.Count; i++)
            {
                switch (dtRows.Rows[i]["DX Type1"].ToString())
                {
                    case "DATA":
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Source"].ToString()))
                        {
                            query = webObjectHandler.FillCombobox(dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(),
                                dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), _obj.Comp, _obj.UserName);
                            switch (sayac)
                            {
                                case 1:
                                    DS21.Table = f.datatablegonder(query);
                                    break;
                                case 2:
                                    DS22.Table = f.datatablegonder(query);
                                    break;
                                case 3:
                                    DS23.Table = f.datatablegonder(query);
                                    break;
                                case 4:
                                    DS24.Table = f.datatablegonder(query);
                                    break;
                                case 5:
                                    DS25.Table = f.datatablegonder(query);
                                    break;
                                case 6:
                                    DS26.Table = f.datatablegonder(query);
                                    break;
                                case 7:
                                    DS27.Table = f.datatablegonder(query);
                                    break;
                                case 8:
                                    DS28.Table = f.datatablegonder(query);
                                    break;
                                case 9:
                                    DS29.Table = f.datatablegonder(query);
                                    break;
                                case 10:
                                    DS210.Table = f.datatablegonder(query);
                                    break;
                                case 11:
                                    DS211.Table = f.datatablegonder(query);
                                    break;
                                case 12:
                                    DS212.Table = f.datatablegonder(query);
                                    break;
                                case 13:
                                    DS213.Table = f.datatablegonder(query);
                                    break;
                                case 14:
                                    DS214.Table = f.datatablegonder(query);
                                    break;
                                case 15:
                                    DS215.Table = f.datatablegonder(query);
                                    break;
                                case 16:
                                    DS216.Table = f.datatablegonder(query);
                                    break;
                                case 17:
                                    DS217.Table = f.datatablegonder(query);
                                    break;
                                case 18:
                                    DS218.Table = f.datatablegonder(query);
                                    break;
                                case 19:
                                    DS219.Table = f.datatablegonder(query);
                                    break;
                                case 20:
                                    DS220.Table = f.datatablegonder(query);
                                    break;
                            }
                            sayac++;
                        }
                        break;
                }
            }
        }

        void FillDS3(DataTable dtRows)
        {
            int sayac = 1;
            string query = string.Empty;
            for (int i = 0; i < dtRows.Rows.Count; i++)
            {
                switch (dtRows.Rows[i]["DX Type1"].ToString())
                {
                    case "DATA":
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Source"].ToString()))
                        {
                            query = webObjectHandler.FillCombobox(dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(),
                                dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), _obj.Comp, _obj.UserName);
                            switch (sayac)
                            {
                                case 1:
                                    DS31.Table = f.datatablegonder(query);
                                    break;
                                case 2:
                                    DS32.Table = f.datatablegonder(query);
                                    break;
                                case 3:
                                    DS33.Table = f.datatablegonder(query);
                                    break;
                                case 4:
                                    DS34.Table = f.datatablegonder(query);
                                    break;
                                case 5:
                                    DS35.Table = f.datatablegonder(query);
                                    break;
                                case 6:
                                    DS36.Table = f.datatablegonder(query);
                                    break;
                                case 7:
                                    DS37.Table = f.datatablegonder(query);
                                    break;
                                case 8:
                                    DS38.Table = f.datatablegonder(query);
                                    break;
                                case 9:
                                    DS39.Table = f.datatablegonder(query);
                                    break;
                                case 10:
                                    DS310.Table = f.datatablegonder(query);
                                    break;
                                case 11:
                                    DS311.Table = f.datatablegonder(query);
                                    break;
                                case 12:
                                    DS312.Table = f.datatablegonder(query);
                                    break;
                                case 13:
                                    DS313.Table = f.datatablegonder(query);
                                    break;
                                case 14:
                                    DS314.Table = f.datatablegonder(query);
                                    break;
                                case 15:
                                    DS315.Table = f.datatablegonder(query);
                                    break;
                                case 16:
                                    DS316.Table = f.datatablegonder(query);
                                    break;
                                case 17:
                                    DS317.Table = f.datatablegonder(query);
                                    break;
                                case 18:
                                    DS318.Table = f.datatablegonder(query);
                                    break;
                                case 19:
                                    DS319.Table = f.datatablegonder(query);
                                    break;
                                case 20:
                                    DS320.Table = f.datatablegonder(query);
                                    break;
                            }
                            sayac++;
                        }
                        break;
                }
            }
        }

        void FillDS4(DataTable dtRows)
        {
            int sayac = 1;
            string query = string.Empty;
            for (int i = 0; i < dtRows.Rows.Count; i++)
            {
                switch (dtRows.Rows[i]["DX Type1"].ToString())
                {
                    case "DATA":
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Source"].ToString()))
                        {
                            query = webObjectHandler.FillCombobox(dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(),
                                dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), _obj.Comp, _obj.UserName);
                            switch (sayac)
                            {
                                case 1:
                                    DS41.Table = f.datatablegonder(query);
                                    break;
                                case 2:
                                    DS42.Table = f.datatablegonder(query);
                                    break;
                                case 3:
                                    DS43.Table = f.datatablegonder(query);
                                    break;
                                case 4:
                                    DS44.Table = f.datatablegonder(query);
                                    break;
                                case 5:
                                    DS45.Table = f.datatablegonder(query);
                                    break;
                                case 6:
                                    DS46.Table = f.datatablegonder(query);
                                    break;
                                case 7:
                                    DS47.Table = f.datatablegonder(query);
                                    break;
                                case 8:
                                    DS48.Table = f.datatablegonder(query);
                                    break;
                                case 9:
                                    DS49.Table = f.datatablegonder(query);
                                    break;
                                case 10:
                                    DS410.Table = f.datatablegonder(query);
                                    break;
                                case 11:
                                    DS411.Table = f.datatablegonder(query);
                                    break;
                                case 12:
                                    DS412.Table = f.datatablegonder(query);
                                    break;
                                case 13:
                                    DS413.Table = f.datatablegonder(query);
                                    break;
                                case 14:
                                    DS414.Table = f.datatablegonder(query);
                                    break;
                                case 15:
                                    DS415.Table = f.datatablegonder(query);
                                    break;
                                case 16:
                                    DS416.Table = f.datatablegonder(query);
                                    break;
                                case 17:
                                    DS417.Table = f.datatablegonder(query);
                                    break;
                                case 18:
                                    DS418.Table = f.datatablegonder(query);
                                    break;
                                case 19:
                                    DS419.Table = f.datatablegonder(query);
                                    break;
                                case 20:
                                    DS420.Table = f.datatablegonder(query);
                                    break;
                            }
                            sayac++;
                        }
                        break;
                }
            }
        }

        void FillDS5(DataTable dtRows)
        {
            int sayac = 1;
            string query = string.Empty;
            for (int i = 0; i < dtRows.Rows.Count; i++)
            {
                switch (dtRows.Rows[i]["DX Type1"].ToString())
                {
                    case "DATA":
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Source"].ToString()))
                        {
                            query = webObjectHandler.FillCombobox(dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(),
                                dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), _obj.Comp, _obj.UserName);
                            switch (sayac)
                            {
                                case 1:
                                    DS51.Table = f.datatablegonder(query);
                                    break;
                                case 2:
                                    DS52.Table = f.datatablegonder(query);
                                    break;
                                case 3:
                                    DS53.Table = f.datatablegonder(query);
                                    break;
                                case 4:
                                    DS54.Table = f.datatablegonder(query);
                                    break;
                                case 5:
                                    DS55.Table = f.datatablegonder(query);
                                    break;
                                case 6:
                                    DS56.Table = f.datatablegonder(query);
                                    break;
                                case 7:
                                    DS57.Table = f.datatablegonder(query);
                                    break;
                                case 8:
                                    DS58.Table = f.datatablegonder(query);
                                    break;
                                case 9:
                                    DS59.Table = f.datatablegonder(query);
                                    break;
                                case 10:
                                    DS510.Table = f.datatablegonder(query);
                                    break;
                                case 11:
                                    DS511.Table = f.datatablegonder(query);
                                    break;
                                case 12:
                                    DS512.Table = f.datatablegonder(query);
                                    break;
                                case 13:
                                    DS513.Table = f.datatablegonder(query);
                                    break;
                                case 14:
                                    DS514.Table = f.datatablegonder(query);
                                    break;
                                case 15:
                                    DS515.Table = f.datatablegonder(query);
                                    break;
                                case 16:
                                    DS516.Table = f.datatablegonder(query);
                                    break;
                                case 17:
                                    DS517.Table = f.datatablegonder(query);
                                    break;
                                case 18:
                                    DS518.Table = f.datatablegonder(query);
                                    break;
                                case 19:
                                    DS519.Table = f.datatablegonder(query);
                                    break;
                                case 20:
                                    DS520.Table = f.datatablegonder(query);
                                    break;
                            }
                            sayac++;
                        }
                        break;
                }
            }
        }

        void FillDS6(DataTable dtRows)
        {
            int sayac = 1;
            string query = string.Empty;
            for (int i = 0; i < dtRows.Rows.Count; i++)
            {
                switch (dtRows.Rows[i]["DX Type1"].ToString())
                {
                    case "DATA":
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Source"].ToString()))
                        {
                            query = webObjectHandler.FillCombobox(dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(),
                                dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), _obj.Comp, _obj.UserName);
                            switch (sayac)
                            {
                                case 1:
                                    DS61.Table = f.datatablegonder(query);
                                    break;
                                case 2:
                                    DS62.Table = f.datatablegonder(query);
                                    break;
                                case 3:
                                    DS63.Table = f.datatablegonder(query);
                                    break;
                                case 4:
                                    DS64.Table = f.datatablegonder(query);
                                    break;
                                case 5:
                                    DS65.Table = f.datatablegonder(query);
                                    break;
                                case 6:
                                    DS66.Table = f.datatablegonder(query);
                                    break;
                                case 7:
                                    DS67.Table = f.datatablegonder(query);
                                    break;
                                case 8:
                                    DS68.Table = f.datatablegonder(query);
                                    break;
                                case 9:
                                    DS69.Table = f.datatablegonder(query);
                                    break;
                                case 10:
                                    DS610.Table = f.datatablegonder(query);
                                    break;
                                case 11:
                                    DS611.Table = f.datatablegonder(query);
                                    break;
                                case 12:
                                    DS612.Table = f.datatablegonder(query);
                                    break;
                                case 13:
                                    DS613.Table = f.datatablegonder(query);
                                    break;
                                case 14:
                                    DS614.Table = f.datatablegonder(query);
                                    break;
                                case 15:
                                    DS615.Table = f.datatablegonder(query);
                                    break;
                                case 16:
                                    DS616.Table = f.datatablegonder(query);
                                    break;
                                case 17:
                                    DS617.Table = f.datatablegonder(query);
                                    break;
                                case 18:
                                    DS618.Table = f.datatablegonder(query);
                                    break;
                                case 19:
                                    DS619.Table = f.datatablegonder(query);
                                    break;
                                case 20:
                                    DS620.Table = f.datatablegonder(query);
                                    break;
                            }
                            sayac++;
                        }
                        break;
                }
            }
        }
        private void RadioListrOder()
        {
            if (pageOrderType == "QUOTE")
            {
                RdList.Items[0].Text = "QUOTATION";
            }
            else
            {
                RdList.Items[0].Text = "VOYAGE";
                rpMain.HeaderText = "VOYAGE";
            }
            HiddenpageType.Value = postURL + "&" + postURLParameter + "=XXXXX&" + postURLParameter2 + "=YYYYY";


            if (OBJ1_fieldId == "O140001" || OBJ1_fieldId == "O140001")
            {
                RdList.SelectedIndex = 0;

            }
            else if (OBJ1_fieldId == "O140002" || OBJ1_fieldId == "O140002")
            {
                RdList.SelectedIndex = 1;
            }

            if (OBJ1_fieldId == "O140002" || OBJ1_fieldId == "O140001")
            {
                rpMain.Height = Unit.Percentage(30);
            }





        }
        private void CollapseDisplay()
        {
            if (OBJ2_fieldId == "OJ9999")
                rpCargoDetails.Visible = false;
            if (OBJ3_fieldId == "OJ9999")
                rpCostSales.Visible = false;
            if (OBJ4_fieldId == "OJ9999")
                rpDocuments.Visible = false;
            if (OBJ5_fieldId == "OJ9999")
                rpComments.Visible = false;


        }
        private void CallGrid1Data()
        {
            if (OBJ1_pageId.Length > 0 & OBJ1_fieldId.Length > 0)
            {
                dtgrid1.Table = woh.webObjectHandlerExtracted(grid1, menu1, txtinsertparams1, txteditparams1, txtdeleteparams1,
                    _obj.UserName, _obj.Comp, OBJ1_pageId, OBJ1_fieldId,
                    HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString(),
                    HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), 1).Copy();
                DataColumn[] pricols = new DataColumn[1];
                pricols[0] = dtgrid1.Table.Columns["ID"];
                dtgrid1.Table.PrimaryKey = pricols;


            }
            else
            {
                grid1.Visible = false;
            }
        }
        private void CallGrid2Data(string filters)
        {
            if (OBJ2_fieldId == "OJ9999") return;

            if (OBJ2_pageId.Length > 0 & OBJ2_fieldId.Length > 0)
            {
                dtgrid2.Table = woh.webObjectHandlerExtractedSub(grid2, menu2, txtinsertparams2, txteditparams2, txtdeleteparams2, _obj.UserName, _obj.Comp,
                    OBJ2_pageId, OBJ2_fieldId, HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString(),
                    HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), 2).Copy();
                DataColumn[] pricols = new DataColumn[1];
                pricols[0] = dtgrid2.Table.Columns["ID"];
                dtgrid2.Table.PrimaryKey = pricols;
                grid2.DataSourceID = "dtgrid2";
                grid2.KeyFieldName = "ID";

            }
            else
            {
                grid2.Visible = false;
            }

        }
        private void CallGrid3Data(string filters)
        {

            if (OBJ3_fieldId == "OJ9999") return;
            if (OBJ3_pageId.Length > 0 & OBJ3_fieldId.Length > 0)
            {
                dtgrid3.Table = woh.webObjectHandlerExtractedSub(grid3, menu3, txtinsertparams3, txteditparams3, txtdeleteparams3, _obj.UserName, _obj.Comp,
                    OBJ3_pageId, OBJ3_fieldId, HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString(),
                    HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), 3).Copy();
                DataColumn[] pricols = new DataColumn[1];
                pricols[0] = dtgrid3.Table.Columns["ID"];
                dtgrid3.Table.PrimaryKey = pricols;
                grid3.DataSourceID = "dtgrid3";
                grid3.KeyFieldName = "ID";
            }
            else
            {
                grid3.Visible = false;
            }
        }
        private void CallGrid4Data(string filters)
        {
            if (OBJ4_fieldId == "OJ9999") return;
            if (OBJ4_pageId.Length > 0 & OBJ4_fieldId.Length > 0)
            {
                dtgrid4.Table = woh.webObjectHandlerExtractedSub(grid4, menu4, txtinsertparams4, txteditparams4, txtdeleteparams4, _obj.UserName, _obj.Comp,
                    OBJ4_pageId, OBJ4_fieldId, HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString(),
                    HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), 4).Copy();
                DataColumn[] pricols = new DataColumn[1];
                pricols[0] = dtgrid4.Table.Columns["ID"];
                dtgrid4.Table.PrimaryKey = pricols;
                grid4.DataSourceID = "dtgrid4";
                grid4.KeyFieldName = "ID";
            }
            else
            {
                grid4.Visible = false;
            }
        }
        private void CallGrid5Data(string filters)
        {
            if (OBJ5_fieldId == "OJ9999") return;
            if (OBJ5_pageId.Length > 0 & OBJ5_fieldId.Length > 0)
            {
                dtgrid5.Table = woh.webObjectHandlerExtractedSub(grid5, menu5, txtinsertparams5, txteditparams4, txtdeleteparams4, _obj.UserName, _obj.Comp,
                    OBJ5_pageId, OBJ5_fieldId, HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString(),
                    HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), 5).Copy();
                DataColumn[] pricols = new DataColumn[1];
                pricols[0] = dtgrid5.Table.Columns["ID"];
                dtgrid5.Table.PrimaryKey = pricols;
                grid5.DataSourceID = "dtgrid5";
                grid5.KeyFieldName = "ID";
            }
            else
            {
                grid5.Visible = false;
            }
        }
        private void CallGrid6Data(string filters)
        {
            if (OBJ6_fieldId == "OJ9999") return;

            if (OBJ6_pageId.Length > 0 & OBJ6_fieldId.Length > 0)
            {
                dtgrid6.Table = woh.webObjectHandlerExtractedSub(grid6, menu6, txtinsertparams6, txteditparams6, txtdeleteparams6, _obj.UserName, _obj.Comp,
                    OBJ6_pageId, OBJ6_fieldId, HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString(),
                    HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), 6).Copy();
                DataColumn[] pricols = new DataColumn[1];
                pricols[0] = dtgrid6.Table.Columns["ID"];
                dtgrid6.Table.PrimaryKey = pricols;
                grid6.DataSourceID = "dtgrid6";
                grid6.KeyFieldName = "ID";
            }
            else
            {
                grid6.Visible = false;
            }
        }
        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }
        protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            switch (gridI.ID)
            {
                case "grid1":
                    if (txtinsertparams1.Text.Length > 0)
                    {
                        string[] _temp = txtinsertparams1.Text.Split(',');
                        string[] _array = new string[txtinsertparams1.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else if (string.IsNullOrEmpty(_temp[i].ToString()))
                            {
                                _array[i - 1] = "";
                            }
                            else if (_temp[i].ToString().Substring(0, 1) == "$")
                            {
                                try { _array[i - 1] = _temp[i].ToString().Substring(1, _temp[i].ToString().Length - 1); } catch { }
                            }
                            else
                            {
                                if (e.NewValues[_temp[i].ToString()] != null)
                                {
                                    if (e.NewValues[_temp[i].ToString()].ToString().Contains('|'))
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString().Split('|')[e.NewValues[_temp[i].ToString()].ToString().Split('|').Length - 1];
                                    else
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                                }
                                else
                                    _array[i - 1] = "";

                            }
                        }

                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;
                case "grid2":
                    if (txtinsertparams2.Text.Length > 0)
                    {
                        string[] _temp = txtinsertparams2.Text.Split(',');
                        string[] _array = new string[txtinsertparams2.Text.Split(',').Length - 1];
                        string[] arr = null;
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else if (string.IsNullOrEmpty(_temp[i].ToString()))
                            {
                                _array[i - 1] = "";
                            }
                            else if (_temp[i].ToString().Substring(0, 1) == "$")
                            {
                                try { _array[i - 1] = _temp[i].ToString().Substring(1, _temp[i].ToString().Length - 1); } catch { }
                            }
                            else
                            {
                                if (e.NewValues[_temp[i].ToString()] != null)
                                {
                                    if (e.NewValues[_temp[i].ToString()].ToString().Contains('|'))
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString().Split('|')[e.NewValues[_temp[i].ToString()].ToString().Split('|').Length - 1];
                                    else
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                                }
                                else
                                    _array[i - 1] = "";

                            }
                        }

                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;
                case "grid3":
                    if (txtinsertparams3.Text.Length > 0)
                    {
                        string[] _temp = txtinsertparams3.Text.Split(',');
                        string[] _array = new string[txtinsertparams3.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else if (string.IsNullOrEmpty(_temp[i].ToString()))
                            {
                                _array[i - 1] = "";
                            }
                            else if (_temp[i].ToString().Substring(0, 1) == "$")
                            {
                                try { _array[i - 1] = _temp[i].ToString().Substring(1, _temp[i].ToString().Length - 1); } catch { }
                            }
                            else
                            {
                                if (e.NewValues[_temp[i].ToString()] != null)
                                {
                                    if (e.NewValues[_temp[i].ToString()].ToString().Contains('|'))
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString().Split('|')[e.NewValues[_temp[i].ToString()].ToString().Split('|').Length - 1];
                                    else
                                    {
                                        if (_temp[i].ToString() == "R0037")
                                        {
                                            if (e.NewValues[_temp[i].ToString()].ToString() == "COST")
                                                _array[i - 1] = "1";
                                            else if (e.NewValues[_temp[i].ToString()].ToString() == "SALES")
                                                _array[i - 1] = "2";
                                            else if (e.NewValues[_temp[i].ToString()].ToString() == "")
                                                _array[i - 1] = "0";
                                            else
                                                _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();

                                        }
                                        else
                                            _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                                    }
                                }
                                else
                                    _array[i - 1] = "";

                            }
                        }

                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;
                case "grid4":
                    if (txtinsertparams4.Text.Length > 0)
                    {
                        string[] _temp = txtinsertparams4.Text.Split(',');
                        string[] _array = new string[txtinsertparams4.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else if (string.IsNullOrEmpty(_temp[i].ToString()))
                            {
                                _array[i - 1] = "";
                            }
                            else if (_temp[i].ToString().Substring(0, 1) == "$")
                            {
                                try { _array[i - 1] = _temp[i].ToString().Substring(1, _temp[i].ToString().Length - 1); } catch { }
                            }
                            else
                            {
                                if (e.NewValues[_temp[i].ToString()] != null)
                                {
                                    if (e.NewValues[_temp[i].ToString()].ToString().Contains('|'))
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString().Split('|')[e.NewValues[_temp[i].ToString()].ToString().Split('|').Length - 1];
                                    else
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                                }
                                else
                                    _array[i - 1] = "";

                            }
                        }

                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;
                case "grid5":
                    if (txtinsertparams5.Text.Length > 0)
                    {
                        string[] _temp = txtinsertparams5.Text.Split(',');
                        string[] _array = new string[txtinsertparams5.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else if (string.IsNullOrEmpty(_temp[i].ToString()))
                            {
                                _array[i - 1] = "";
                            }
                            else if (_temp[i].ToString().Substring(0, 1) == "$")
                            {
                                try { _array[i - 1] = _temp[i].ToString().Substring(1, _temp[i].ToString().Length - 1); } catch { }
                            }
                            else
                            {
                                if (e.NewValues[_temp[i].ToString()] != null)
                                {
                                    if (e.NewValues[_temp[i].ToString()].ToString().Contains('|'))
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString().Split('|')[e.NewValues[_temp[i].ToString()].ToString().Split('|').Length - 1];
                                    else
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                                }
                                else
                                    _array[i - 1] = "";

                            }
                        }

                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    grid5_CustomCallback(sender, null);
                    break;
                case "grid6":
                    if (txtinsertparams6.Text.Length > 0)
                    {
                        string[] _temp = txtinsertparams6.Text.Split(',');
                        string[] _array = new string[txtinsertparams6.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else if (string.IsNullOrEmpty(_temp[i].ToString()))
                            {
                                _array[i - 1] = "";
                            }
                            else if (_temp[i].ToString().Substring(0, 1) == "$")
                            {
                                try { _array[i - 1] = _temp[i].ToString().Substring(1, _temp[i].ToString().Length - 1); } catch { }
                            }
                            else
                            {
                                if (e.NewValues[_temp[i].ToString()] != null)
                                {
                                    if (e.NewValues[_temp[i].ToString()].ToString().Contains('|'))
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString().Split('|')[e.NewValues[_temp[i].ToString()].ToString().Split('|').Length - 1];
                                    else
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                                }
                                else
                                    _array[i - 1] = "";

                            }
                        }
                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;
                default:
                    break;
            }
            Session[gridI.ID + "Cloned"] = "0";
            gridI.JSProperties["cpIsUpdated"] = true;

        }
        protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            switch (gridI.ID)
            {
                case "grid1":
                    if (txteditparams1.Text.Length > 0)
                    {
                        string[] _temp = txteditparams1.Text.Split(',');
                        string[] _array = new string[txteditparams1.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else if (string.IsNullOrEmpty(_temp[i].ToString()))
                            {
                                _array[i - 1] = "";
                            }
                            else if (_temp[i].ToString().Substring(0, 1) == "$")
                            {
                                try { _array[i - 1] = _temp[i].ToString().Substring(1, _temp[i].ToString().Length - 1); } catch { }
                            }
                            else
                            {
                                if (e.NewValues[_temp[i].ToString()] != null)
                                {
                                    if (e.NewValues[_temp[i].ToString()].ToString().Contains('|'))
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString().Split('|')[e.NewValues[_temp[i].ToString()].ToString().Split('|').Length - 1];
                                    else
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                                }
                                else
                                    _array[i - 1] = "";

                            }
                        }

                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;
                case "grid2":
                    if (txteditparams2.Text.Length > 0)
                    {
                        string[] _temp = txteditparams2.Text.Split(',');
                        string[] _array = new string[txteditparams2.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else if (string.IsNullOrEmpty(_temp[i].ToString()))
                            {
                                _array[i - 1] = "";
                            }
                            else if (_temp[i].ToString().Substring(0, 1) == "$")
                            {
                                try { _array[i - 1] = _temp[i].ToString().Substring(1, _temp[i].ToString().Length - 1); } catch { }
                            }
                            else
                            {
                                if (e.NewValues[_temp[i].ToString()] != null)
                                {
                                    if (e.NewValues[_temp[i].ToString()].ToString().Contains('|'))
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString().Split('|')[e.NewValues[_temp[i].ToString()].ToString().Split('|').Length - 1];
                                    else
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                                }
                                else
                                    _array[i - 1] = "";

                            }
                        }

                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;
                case "grid3":
                    string values = string.Empty;
                    if (txteditparams3.Text.Length > 0)
                    {
                        string[] _temp = txteditparams3.Text.Split(',');
                        string[] _array = new string[txteditparams3.Text.Split(',').Length - 1];

                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                                values = AddValues(values, _obj.Comp);
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                                values = AddValues(values, _obj.UserName);
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                                values = AddValues(values, this.HiddenP1.Value);
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                                values = AddValues(values, this.HiddenP2.Value);
                            }
                            else if (string.IsNullOrEmpty(_temp[i].ToString()))
                            {
                                _array[i - 1] = "";
                                values = AddValues(values, "");
                            }
                            else if (_temp[i].ToString().Substring(0, 1) == "$")
                            {
                                try { _array[i - 1] = _temp[i].ToString().Substring(1, _temp[i].ToString().Length - 1); values = AddValues(values, _temp[i].ToString().Substring(1, _temp[i].ToString().Length - 1)); } catch { }
                            }
                            else
                            {
                                if (e.NewValues[_temp[i].ToString()] != null)
                                {
                                    if (e.NewValues[_temp[i].ToString()].ToString().Contains('|'))
                                    {
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString().Split('|')[e.NewValues[_temp[i].ToString()].ToString().Split('|').Length - 1];
                                        values = AddValues(values, e.NewValues[_temp[i].ToString()].ToString().Split('|')[e.NewValues[_temp[i].ToString()].ToString().Split('|').Length - 1]);
                                    }
                                    else
                                    {


                                        if (_temp[i].ToString() == "R0037")
                                        {
                                            if (e.NewValues[_temp[i].ToString()].ToString() == "COST")
                                                _array[i - 1] = "1";
                                            else if (e.NewValues[_temp[i].ToString()].ToString() == "SALES")
                                                _array[i - 1] = "2";
                                            else if (e.NewValues[_temp[i].ToString()].ToString() == "")
                                                _array[i - 1] = "0";
                                            else
                                                _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();

                                        }
                                        else
                                            _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();

                                        values = AddValues(values, e.NewValues[_temp[i].ToString()].ToString());
                                    }
                                }
                                else
                                {
                                    _array[i - 1] = "";
                                    values = AddValues(values, "");
                                }

                            }
                        }
                        // throw new Exception(values);
                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;
                case "grid4":
                    if (txteditparams4.Text.Length > 0)
                    {
                        string[] _temp = txteditparams4.Text.Split(',');
                        string[] _array = new string[txteditparams4.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else if (string.IsNullOrEmpty(_temp[i].ToString()))
                            {
                                _array[i - 1] = "";
                            }
                            else if (_temp[i].ToString().Substring(0, 1) == "$")
                            {
                                try { _array[i - 1] = _temp[i].ToString().Substring(1, _temp[i].ToString().Length - 1); } catch { }
                            }
                            else
                            {
                                if (e.NewValues[_temp[i].ToString()] != null)
                                {
                                    if (e.NewValues[_temp[i].ToString()].ToString().Contains('|'))
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString().Split('|')[e.NewValues[_temp[i].ToString()].ToString().Split('|').Length - 1];
                                    else
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                                }
                                else
                                    _array[i - 1] = "";

                            }
                        }

                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;
                case "grid5":
                    if (txteditparams5.Text.Length > 0)
                    {
                        string[] _temp = txteditparams5.Text.Split(',');
                        string[] _array = new string[txteditparams5.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else if (string.IsNullOrEmpty(_temp[i].ToString()))
                            {
                                _array[i - 1] = "";
                            }
                            else if (_temp[i].ToString().Substring(0, 1) == "$")
                            {
                                try { _array[i - 1] = _temp[i].ToString().Substring(1, _temp[i].ToString().Length - 1); } catch { }
                            }
                            else
                            {
                                if (e.NewValues[_temp[i].ToString()] != null)
                                {
                                    if (e.NewValues[_temp[i].ToString()].ToString().Contains('|'))
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString().Split('|')[e.NewValues[_temp[i].ToString()].ToString().Split('|').Length - 1];
                                    else
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                                }
                                else
                                    _array[i - 1] = "";

                            }
                        }

                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;
                case "grid6":
                    if (txteditparams6.Text.Length > 0)
                    {
                        string[] _temp = txteditparams6.Text.Split(',');
                        string[] _array = new string[txteditparams6.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else if (string.IsNullOrEmpty(_temp[i].ToString()))
                            {
                                _array[i - 1] = "";
                            }
                            else if (_temp[i].ToString().Substring(0, 1) == "$")
                            {
                                try { _array[i - 1] = _temp[i].ToString().Substring(1, _temp[i].ToString().Length - 1); } catch { }
                            }
                            else
                            {
                                if (e.NewValues[_temp[i].ToString()] != null)
                                {
                                    if (e.NewValues[_temp[i].ToString()].ToString().Contains('|'))
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString().Split('|')[e.NewValues[_temp[i].ToString()].ToString().Split('|').Length - 1];
                                    else
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                                }
                                else
                                    _array[i - 1] = "";

                            }
                        }

                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;
                default:
                    break;
            }
            Session[gridI.ID + "Cloned"] = "0";
            gridI.JSProperties["cpIsUpdated"] = true;
        }
        protected void grid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            switch (gridI.ID)
            {
                case "grid1":
                    if (txtdeleteparams1.Text.Length > 0)
                    {
                        string[] _temp = txtdeleteparams1.Text.Split(',');
                        string[] _array = new string[txtdeleteparams1.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else if (string.IsNullOrEmpty(_temp[i].ToString()))
                            {
                                _array[i - 1] = "";
                            }
                            else if (_temp[i].ToString().Substring(0, 1) == "$")
                            {
                                try { _array[i - 1] = _temp[i].ToString().Substring(1, _temp[i].ToString().Length - 1); } catch { }
                            }
                            else
                            {
                                if (e.Values[_temp[i].ToString()] != null)
                                {
                                    if (e.Values[_temp[i].ToString()].ToString().Contains('|'))
                                        _array[i - 1] = e.Values[_temp[i].ToString()].ToString().Split('|')[e.Values[_temp[i].ToString()].ToString().Split('|').Length - 1];
                                    else
                                        _array[i - 1] = e.Values[_temp[i].ToString()].ToString();
                                }
                                else
                                    _array[i - 1] = "";

                            }
                        }
                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;
                case "grid2":
                    if (txtdeleteparams2.Text.Length > 0)
                    {
                        string[] _temp = txtdeleteparams2.Text.Split(',');
                        string[] _array = new string[txtdeleteparams2.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else if (string.IsNullOrEmpty(_temp[i].ToString()))
                            {
                                _array[i - 1] = "";
                            }
                            else if (_temp[i].ToString().Substring(0, 1) == "$")
                            {
                                try { _array[i - 1] = _temp[i].ToString().Substring(1, _temp[i].ToString().Length - 1); } catch { }
                            }
                            else
                            {
                                if (e.Values[_temp[i].ToString()] != null)
                                {
                                    if (e.Values[_temp[i].ToString()].ToString().Contains('|'))
                                        _array[i - 1] = e.Values[_temp[i].ToString()].ToString().Split('|')[e.Values[_temp[i].ToString()].ToString().Split('|').Length - 1];
                                    else
                                        _array[i - 1] = e.Values[_temp[i].ToString()].ToString();
                                }
                                else
                                    _array[i - 1] = "";

                            }
                        }
                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;
                case "grid3":
                    if (txtdeleteparams3.Text.Length > 0)
                    {
                        string[] _temp = txtdeleteparams3.Text.Split(',');
                        string[] _array = new string[txtdeleteparams3.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else if (string.IsNullOrEmpty(_temp[i].ToString()))
                            {
                                _array[i - 1] = "";
                            }
                            else if (_temp[i].ToString().Substring(0, 1) == "$")
                            {
                                try { _array[i - 1] = _temp[i].ToString().Substring(1, _temp[i].ToString().Length - 1); } catch { }
                            }
                            else
                            {
                                if (e.Values[_temp[i].ToString()] != null)
                                {
                                    if (e.Values[_temp[i].ToString()].ToString().Contains('|'))
                                        _array[i - 1] = e.Values[_temp[i].ToString()].ToString().Split('|')[e.Values[_temp[i].ToString()].ToString().Split('|').Length - 1];
                                    else
                                        _array[i - 1] = e.Values[_temp[i].ToString()].ToString();
                                }
                                else
                                    _array[i - 1] = "";

                            }
                        }
                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;
                case "grid4":
                    if (txtdeleteparams4.Text.Length > 0)
                    {
                        string[] _temp = txtdeleteparams4.Text.Split(',');
                        string[] _array = new string[txtdeleteparams4.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else if (string.IsNullOrEmpty(_temp[i].ToString()))
                            {
                                _array[i - 1] = "";
                            }
                            else if (_temp[i].ToString().Substring(0, 1) == "$")
                            {
                                try { _array[i - 1] = _temp[i].ToString().Substring(1, _temp[i].ToString().Length - 1); } catch { }
                            }
                            else
                            {
                                if (e.Values[_temp[i].ToString()] != null)
                                {
                                    if (e.Values[_temp[i].ToString()].ToString().Contains('|'))
                                        _array[i - 1] = e.Values[_temp[i].ToString()].ToString().Split('|')[e.Values[_temp[i].ToString()].ToString().Split('|').Length - 1];
                                    else
                                        _array[i - 1] = e.Values[_temp[i].ToString()].ToString();
                                }
                                else
                                    _array[i - 1] = "";

                            }
                        }
                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;
                case "grid5":
                    if (txtdeleteparams5.Text.Length > 0)
                    {
                        string[] _temp = txtdeleteparams5.Text.Split(',');
                        string[] _array = new string[txtdeleteparams5.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else if (string.IsNullOrEmpty(_temp[i].ToString()))
                            {
                                _array[i - 1] = "";
                            }
                            else if (_temp[i].ToString().Substring(0, 1) == "$")
                            {
                                try { _array[i - 1] = _temp[i].ToString().Substring(1, _temp[i].ToString().Length - 1); } catch { }
                            }
                            else
                            {
                                if (e.Values[_temp[i].ToString()] != null)
                                {
                                    if (e.Values[_temp[i].ToString()].ToString().Contains('|'))
                                        _array[i - 1] = e.Values[_temp[i].ToString()].ToString().Split('|')[e.Values[_temp[i].ToString()].ToString().Split('|').Length - 1];
                                    else
                                        _array[i - 1] = e.Values[_temp[i].ToString()].ToString();
                                }
                                else
                                    _array[i - 1] = "";

                            }
                        }
                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;
                case "grid6":
                    if (txtdeleteparams6.Text.Length > 0)
                    {
                        string[] _temp = txtdeleteparams6.Text.Split(',');
                        string[] _array = new string[txtdeleteparams6.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else if (string.IsNullOrEmpty(_temp[i].ToString()))
                            {
                                _array[i - 1] = "";
                            }
                            else if (_temp[i].ToString().Substring(0, 1) == "$")
                            {
                                try { _array[i - 1] = _temp[i].ToString().Substring(1, _temp[i].ToString().Length - 1); } catch { }
                            }
                            else
                            {
                                if (e.Values[_temp[i].ToString()] != null)
                                {
                                    if (e.Values[_temp[i].ToString()].ToString().Contains('|'))
                                        _array[i - 1] = e.Values[_temp[i].ToString()].ToString().Split('|')[e.Values[_temp[i].ToString()].ToString().Split('|').Length - 1];
                                    else
                                        _array[i - 1] = e.Values[_temp[i].ToString()].ToString();
                                }
                                else
                                    _array[i - 1] = "";

                            }
                        }
                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;

            }
            Session[gridI.ID + "Cloned"] = "0";
            gridI.JSProperties["cpIsUpdated"] = true;
        }
        protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            if (copiedValues == null) return;
            string Sessionn = "OPL10Grid" + gridI.ID.Substring(gridI.ID.Length - 1) + "Rows";
            DataRow[] rows = ((DataTable)Session[Sessionn]).Select("[DX Clone] >0");
            if (rows.Length == 0) return;
            foreach (DataRow row in rows)
            {
                e.NewValues[row["DX Field ID"].ToString()] = copiedValues[row["DX Field ID"].ToString()];
            }
        }
        protected void grid_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            if (e.ButtonID == "Clone")
            {
                string Sessionn = "OPL10Grid" + gridI.ID.Substring(gridI.ID.Length - 1) + "Rows";
                DataRow[] rows = ((DataTable)Session[Sessionn]).Select("[DX Clone]>0");
                if (rows.Length > 0)
                {
                    copiedValues = new Hashtable();
                    foreach (DataRow row in rows)
                    {
                        copiedValues[row["DX Field ID"].ToString()] = gridI.GetRowValues(e.VisibleIndex, row["DX Field ID"].ToString());
                    }
                }
                Session[gridI.ID + "Cloned"] = "1";
                gridI.AddNewRow();
            }
            else if (e.ButtonID == "SelectAll")
            {
                gridI.Selection.SelectAll();
            }
            else if (e.ButtonID == "UnSelectAll")
            {
                gridI.Selection.UnselectAll();
            }
        }
        #region Grid1

        protected void grid1_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView xgrid = (ASPxGridView)sender;
            string _Session = "OPL10Grid1Rows";
            if (xgrid.IsNewRowEditing)
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.Visible = false;
                    else
                        e.Editor.Visible = true;
                }

                if (Session[xgrid.ID + "Cloned"].ToString() == "1")
                {
                    DataRow[] clonedRows = ((DataTable)Session[_Session]).Select("[DX Clone]>0");
                    if (clonedRows.Length > 0)
                    {
                        foreach (DataRow row in clonedRows)
                        {
                            if (e.Column.FieldName == row["DX Field ID"].ToString())
                            {
                                if (Convert.ToInt32(row["DX Clone  Readonly"]) > 0)
                                    e.Editor.ClientEnabled = false;
                                else if (Convert.ToInt32(row["DX Clone Hidden"]) > 0)
                                    e.Editor.Visible = false;
                            }
                        }
                    }
                }

            }
            else
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.Visible = false;
                    else
                        e.Editor.Visible = true;
                }
            }



        }
        protected void menu1_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport1.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                webObjectHandler.ExportPdf(gridExport1);
            }
            else if (e.Item.Name.Equals("btninsert"))
            {
                this.HiddenP1.Value = "AXYZT";
                this.HiddenP2.Value = "";
                this.iframeView.Attributes.Add("src", postURL);
                HiddenMainStatu.Value = "99";
                try { grid2_CustomCallback(grid2, null); } catch { }
                try { grid3_CustomCallback(grid3, null); } catch { }
                try { grid6_CustomCallback(grid6, null); } catch { }
            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(OBJ1_pageId, OBJ1_fieldId, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string[] filter = rows[0]["DX Filter"].ToString().Split(',');
                if (filter.Contains("DATE"))
                {
                    if (string.IsNullOrEmpty(txtDate.Date.ToString()))
                    {
                        txtCurGridId.Text = "1";
                        txtCurGridFieldId.Text = FieldId;
                        txtCurGridObjecId.Text = OBJ1_fieldId;
                        txtCurGridPageId.Text = OBJ1_pageId;
                        DatePopup.ShowOnPageLoad = true;
                    }
                    return;
                }
                ServiceUtilities.ProcSelectedRows(grid1, rows, _obj.UserName, _obj.Comp);

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }
        #endregion

        #region Grid2

        protected void grid2_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView xgrid = (ASPxGridView)sender;
            string _Session = "OPL10Grid2Rows";
            if (xgrid.IsNewRowEditing)
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.Visible = false;
                    else
                        e.Editor.Visible = true;
                }

                if (Session[xgrid.ID + "Cloned"].ToString() == "1")
                {
                    DataRow[] clonedRows = ((DataTable)Session[_Session]).Select("[DX Clone]>0");
                    if (clonedRows.Length > 0)
                    {
                        foreach (DataRow row in clonedRows)
                        {
                            if (e.Column.FieldName == row["DX Field ID"].ToString())
                            {
                                if (Convert.ToInt32(row["DX Clone  Readonly"]) > 0)
                                    e.Editor.ClientEnabled = false;
                                else if (Convert.ToInt32(row["DX Clone Hidden"]) > 0)
                                    e.Editor.Visible = false;
                            }
                        }
                    }
                }


            }
            else
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.Visible = false;
                    else
                        e.Editor.Visible = true;
                }
            }

        }
        protected void Grid_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;
            string _sessionname = "OPL10Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";
            switch (e.ButtonType)
            {
                case ColumnCommandButtonType.Edit:

                    DataRow[] EditRows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='EDIT' and [DX Display Order]>0");
                    if (EditRows.Length > 0)
                    {
                        string _col = EditRows[0]["DX Permision Field"].ToString();
                        if (grid.GetRowValues(e.VisibleIndex, _col) != null)
                        {
                            if (grid.GetRowValues(e.VisibleIndex, _col).ToString() == "0")
                            {
                                e.Visible = false;
                            }
                        }
                    }
                    else
                        e.Visible = false;
                    break;
                case ColumnCommandButtonType.Delete:
                    DataRow[] DeleteRows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='DELETE' and [DX Display Order]>0");
                    if (DeleteRows.Length > 0)
                    {
                        string _col = DeleteRows[0]["DX Permision Field"].ToString();
                        if (grid.GetRowValues(e.VisibleIndex, _col) != null)
                        {
                            if (grid.GetRowValues(e.VisibleIndex, _col).ToString() == "0")
                            {
                                e.Visible = false;
                            }
                        }
                    }
                    else
                        e.Visible = false;
                    break;
            }
        }
        protected void menu2_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport2.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                webObjectHandler.ExportPdf(gridExport2);
            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(OBJ2_pageId, OBJ2_fieldId, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string[] filter = rows[0]["DX Filter"].ToString().Split(',');
                if (filter.Contains("DATE"))
                {
                    if (string.IsNullOrEmpty(txtDate.Date.ToString()))
                    {
                        txtCurGridId.Text = "2";
                        txtCurGridFieldId.Text = FieldId;
                        txtCurGridObjecId.Text = OBJ2_fieldId;
                        txtCurGridPageId.Text = OBJ2_pageId;
                        DatePopup.ShowOnPageLoad = true;
                    }
                    return;
                }
                ServiceUtilities.ProcSelectedRows(grid2, rows, _obj.UserName, _obj.Comp);

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }
        #endregion

        #region Grid3

        protected void grid3_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView xgrid = (ASPxGridView)sender;
            string _Session = "OPL10Grid3Rows";
            if (xgrid.IsNewRowEditing)
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.Visible = false;
                    else
                        e.Editor.Visible = true;
                }

                if (Session[xgrid.ID + "Cloned"].ToString() == "1")
                {
                    DataRow[] clonedRows = ((DataTable)Session[_Session]).Select("[DX Clone]>0");
                    if (clonedRows.Length > 0)
                    {
                        foreach (DataRow row in clonedRows)
                        {
                            if (e.Column.FieldName == row["DX Field ID"].ToString())
                            {
                                if (Convert.ToInt32(row["DX Clone  Readonly"]) > 0)
                                    e.Editor.ClientEnabled = false;
                                else if (Convert.ToInt32(row["DX Clone Hidden"]) > 0)
                                    e.Editor.Visible = false;
                            }
                        }
                    }
                }


            }
            else
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.Visible = false;
                    else
                        e.Editor.Visible = true;
                }
            }

            string[] linkFields = txtlinkField1.Text.Split(';');

            if (e.Column.FieldName == "RTYP2")
            {
                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP1");
                if (_val != null)
                {
                    if (!string.IsNullOrEmpty(_val.ToString()))
                    {
                        string sorgu = "select NEWID() PK, * from VW0111_2 where R0011='" + _val.ToString() + "'";
                        f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                    }
                }
                cmb.Callback += Cmb_Callback;
            }
            else if (e.Column.FieldName == "RTYP3")
            {
                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                object _val1 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP1");
                object _val2 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP2");
                if (_val1 != null & _val2 != null)
                {
                    if (!string.IsNullOrEmpty(_val1.ToString()) & !string.IsNullOrEmpty(_val2.ToString()))
                    {
                        string sorgu = "select NEWID() PK, * from VW0111_3 where R0011='" + _val1.ToString() + "' and R0012='" + _val2.ToString() + "'";
                        f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                    }
                }
                cmb.Callback += Cmb2_Callback;
            }
            else if (e.Column.FieldName == "R0042")
            {
                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, "R0037");
                if (_val != null)
                {
                    if (!string.IsNullOrEmpty(_val.ToString()))
                    {
                        string CustVend = string.Empty;
                        if (_val.ToString() == "1" | _val.ToString() == "COST")
                            CustVend = "VENDOR";
                        else if (_val.ToString() == "2" | _val.ToString() == "SALES")
                            CustVend = "CUSTOMER";
                        string sorgu = "select NEWID() PK, * from VW0110 where R0011='" + CustVend + "'";
                        f.FillAspxCombobox(cmb, sorgu, "R0013", "R0012");
                    }
                }
                cmb.Callback += Cmb3_Callback;
            }



            if (linkFields.Length == 2)
            {
                if (e.Column.FieldName == linkFields[0])
                {
                    e.Editor.Value = HiddenP1.Value;
                }
                else if (e.Column.FieldName == linkFields[1])
                {
                    e.Editor.Value = HiddenP2.Value;
                }
            }
            else if (linkFields.Length == 1)
            {
                if (e.Column.FieldName == linkFields[0])
                {
                    e.Editor.Value = HiddenP1.Value;

                }
            }

        }


        private void Cmb_Callback(object sender, CallbackEventArgsBase e)
        {
            if (!string.IsNullOrEmpty(e.Parameter))
            {
                ASPxComboBox cmb = (ASPxComboBox)sender;
                string sorgu = "select NEWID() PK, * from VW0111_2 where R0011='" + e.Parameter + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                Session["OPL10R0011"] = e.Parameter.ToString();

            }

        }
        private void Cmb2_Callback(object sender, CallbackEventArgsBase e)
        {
            if (!string.IsNullOrEmpty(e.Parameter))
            {
                ASPxComboBox cmb = (ASPxComboBox)sender;
                string sorgu = "select NEWID() PK, * from VW0111_3 where R0011='" + Session["OPL10R0011"].ToString() + "' and R0012='" + e.Parameter + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");


            }

        }
        private void Cmb3_Callback(object sender, CallbackEventArgsBase e)
        {
            //throw new Exception(e.Parameter.ToString());
            if (!string.IsNullOrEmpty(e.Parameter))
            {
                ASPxComboBox cmb = (ASPxComboBox)sender;
                string CustVend = string.Empty;
                if (e.Parameter.ToString() == "1")
                    CustVend = "VENDOR";
                else if (e.Parameter.ToString() == "2")
                    CustVend = "CUSTOMER";
                string sorgu = "select NEWID() PK, * from VW0110 where R0011='" + CustVend + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0013", "R0012");
            }

        }
        protected void menu3_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport3.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                webObjectHandler.ExportPdf(gridExport3);
            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(OBJ3_pageId, OBJ3_fieldId, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string[] filter = rows[0]["DX Filter"].ToString().Split(',');
                if (filter.Contains("DATE"))
                {
                    if (string.IsNullOrEmpty(txtDate.Date.ToString()))
                    {
                        txtCurGridId.Text = "3";
                        txtCurGridFieldId.Text = FieldId;
                        txtCurGridObjecId.Text = OBJ3_fieldId;
                        txtCurGridPageId.Text = OBJ3_pageId;
                        DatePopup.ShowOnPageLoad = true;
                    }
                    return;
                }
                ServiceUtilities.ProcSelectedRows(grid3, rows, _obj.UserName, _obj.Comp);

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }
        #endregion

        #region Grid4

        protected void grid4_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView xgrid = (ASPxGridView)sender;
            string _Session = "OPL10Grid4Rows";
            if (xgrid.IsNewRowEditing)
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.Visible = false;
                    else
                        e.Editor.Visible = true;
                }

                if (Session[xgrid.ID + "Cloned"].ToString() == "1")
                {
                    DataRow[] clonedRows = ((DataTable)Session[_Session]).Select("[DX Clone]>0");
                    if (clonedRows.Length > 0)
                    {
                        foreach (DataRow row in clonedRows)
                        {
                            if (e.Column.FieldName == row["DX Field ID"].ToString())
                            {
                                if (Convert.ToInt32(row["DX Clone  Readonly"]) > 0)
                                    e.Editor.ClientEnabled = false;
                                else if (Convert.ToInt32(row["DX Clone Hidden"]) > 0)
                                    e.Editor.Visible = false;
                            }
                        }
                    }
                }


            }
            else
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.Visible = false;
                    else
                        e.Editor.Visible = true;
                }
            }

        }
        protected void menu4_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport4.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                webObjectHandler.ExportPdf(gridExport4);
            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(OBJ4_pageId, OBJ4_fieldId, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string[] filter = rows[0]["DX Filter"].ToString().Split(',');
                if (filter.Contains("DATE"))
                {
                    if (string.IsNullOrEmpty(txtDate.Date.ToString()))
                    {
                        txtCurGridId.Text = "4";
                        txtCurGridFieldId.Text = FieldId;
                        txtCurGridObjecId.Text = OBJ4_fieldId;
                        txtCurGridPageId.Text = OBJ4_pageId;
                        DatePopup.ShowOnPageLoad = true;
                    }
                    return;
                }
                ServiceUtilities.ProcSelectedRows(grid4, rows, _obj.UserName, _obj.Comp);

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }
        #endregion

        #region Grid5

        protected void grid5_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView xgrid = (ASPxGridView)sender;
            string _Session = "OPL10Grid5Rows";
            if (xgrid.IsNewRowEditing)
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.Visible = false;
                    else
                        e.Editor.Visible = true;
                }

                if (Session[xgrid.ID + "Cloned"].ToString() == "1")
                {
                    DataRow[] clonedRows = ((DataTable)Session[_Session]).Select("[DX Clone]>0");
                    if (clonedRows.Length > 0)
                    {
                        foreach (DataRow row in clonedRows)
                        {
                            if (e.Column.FieldName == row["DX Field ID"].ToString())
                            {
                                if (Convert.ToInt32(row["DX Clone  Readonly"]) > 0)
                                    e.Editor.ClientEnabled = false;
                                else if (Convert.ToInt32(row["DX Clone Hidden"]) > 0)
                                    e.Editor.Visible = false;
                            }
                        }
                    }
                }


            }
            else
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.Visible = false;
                    else
                        e.Editor.Visible = true;
                }
            }


        }
        protected void menu5_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport5.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                webObjectHandler.ExportPdf(gridExport5);
            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(OBJ5_pageId, OBJ5_fieldId, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string[] filter = rows[0]["DX Filter"].ToString().Split(',');
                if (filter.Contains("DATE"))
                {
                    if (string.IsNullOrEmpty(txtDate.Date.ToString()))
                    {
                        txtCurGridId.Text = "5";
                        txtCurGridFieldId.Text = FieldId;
                        txtCurGridObjecId.Text = OBJ5_fieldId;
                        txtCurGridPageId.Text = OBJ5_pageId;
                        DatePopup.ShowOnPageLoad = true;
                    }
                    return;
                }
                ServiceUtilities.ProcSelectedRows(grid5, rows, _obj.UserName, _obj.Comp);

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }
        #endregion

        #region Grid6

        protected void grid6_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView xgrid = (ASPxGridView)sender;
            string _Session = "OPL10Grid6Rows";
            if (e.Column.FieldName == "R0027")
            {
                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, "R0015");
                if (_val != null)
                {
                    if (!string.IsNullOrEmpty(_val.ToString()))
                    {
                        string sorgu = "select NEWID() PK, t1.* from [0_W10081] t1 left join VW0064 t2 on t1.RTASK=t2.RTASK where t1.RTASK=t2.RTASK " +
                        "and t2.R0011='" + _val.ToString() + "'";
                        f.FillAspxCombobox(cmb, sorgu, "R0014", "R0012");
                    }
                }
                cmb.Callback += Cmb_Callback1;
            }
            else if (e.Column.FieldName == "R0031")
            {
                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, "R0015");
                if (_val != null)
                {
                    if (!string.IsNullOrEmpty(_val.ToString()))
                    {
                        string sorgu = "select NEWID() PK, t1.* from [0_W10031] t1 left join VW0064 t2 on t1.RTASK=t2.RTASK where t1.RTASK=t2.RTASK " +
                        "and t2.R0011='" + _val.ToString() + "'";
                        f.FillAspxCombobox(cmb, sorgu, "R0012", "R0011");
                    }
                }
                cmb.Callback += Cmb_Callback2;
            }
            else if (e.Column.FieldName == "R0033")
            {
                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, "R0015");
                if (_val != null)
                {
                    if (!string.IsNullOrEmpty(_val.ToString()))
                    {
                        string sorgu = "select NEWID() PK, t1.* from [0_W10031] t1 left join VW0064 t2 on t1.RTASK=t2.RTASK where t1.RTASK = t2.RTASK " +
                        "and t2.R0011='" + _val.ToString() + "'";
                        f.FillAspxCombobox(cmb, sorgu, "R0012", "R0011");
                    }
                }
                cmb.Callback += Cmb_Callback3;
            }
            if (xgrid.IsNewRowEditing)
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.Visible = false;
                    else
                        e.Editor.Visible = true;
                }

                if (Session[xgrid.ID + "Cloned"].ToString() == "1")
                {
                    DataRow[] clonedRows = ((DataTable)Session[_Session]).Select("[DX Clone]>0");
                    if (clonedRows.Length > 0)
                    {
                        foreach (DataRow row in clonedRows)
                        {
                            if (e.Column.FieldName == row["DX Field ID"].ToString())
                            {
                                if (Convert.ToInt32(row["DX Clone  Readonly"]) > 0)
                                    e.Editor.ClientEnabled = false;
                                else if (Convert.ToInt32(row["DX Clone Hidden"]) > 0)
                                    e.Editor.Visible = false;
                            }
                        }
                    }
                }


            }
            else
            {
                DataRow[] rows = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.Visible = false;
                    else
                        e.Editor.Visible = true;
                }
            }



        }
        private void Cmb_Callback3(object sender, CallbackEventArgsBase e)
        {
            if (e.Parameter.ToString() != "")
            {
                ASPxComboBox cmb = (ASPxComboBox)sender;

                string sorgu = "select NEWID() PK, t1.* from [0_W10031] t1 left join VW0064 t2 on t1.RTASK=t2.RTASK where t1.RTASK = t2.RTASK " +
                     "and t2.R0011='" + e.Parameter.ToString() + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0012", "R0011");

            }
        }
        private void Cmb_Callback2(object sender, CallbackEventArgsBase e)
        {
            if (e.Parameter.ToString() != "")
            {
                ASPxComboBox cmb = (ASPxComboBox)sender;

                string sorgu = "select NEWID() PK, t1.* from [0_W10031] t1 left join VW0064 t2 on t1.RTASK=t2.RTASK where t1.RTASK = t2.RTASK " +
                    "and t2.R0011='" + e.Parameter.ToString() + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0012", "R0011");

            }
        }
        private void Cmb_Callback1(object sender, CallbackEventArgsBase e)
        {
            if (e.Parameter.ToString() != "")
            {
                ASPxComboBox cmb = (ASPxComboBox)sender;

                string sorgu = "select NEWID() PK, t1.* from [0_W10081] t1 left join VW0064 t2 on t1.RTASK=t2.RTASK where t1.RTASK = t2.RTASK " +
                    "and t2.R0011='" + e.Parameter.ToString() + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0014", "R0012");

            }
        }
        protected void menu6_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport6.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                webObjectHandler.ExportPdf(gridExport6);
            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(OBJ6_pageId, OBJ6_fieldId, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string[] filter = rows[0]["DX Filter"].ToString().Split(',');
                if (filter.Contains("DATE"))
                {
                    if (string.IsNullOrEmpty(txtDate.Date.ToString()))
                    {
                        txtCurGridId.Text = "6";
                        txtCurGridFieldId.Text = FieldId;
                        txtCurGridObjecId.Text = OBJ6_fieldId;
                        txtCurGridPageId.Text = OBJ6_pageId;
                        DatePopup.ShowOnPageLoad = true;
                    }
                    return;
                }
                ServiceUtilities.ProcSelectedRows(grid6, rows, _obj.UserName, _obj.Comp);

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }
        #endregion
        protected void grid1_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(OBJ1_pageId, OBJ1_fieldId, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
            }
            this.dtgrid1.Table = webObjectHandler.fillMainGrid(OBJ1_pageId, OBJ1_fieldId, _obj.Comp, _obj.UserName);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid1.Table.Columns["ID"];
            this.dtgrid1.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid1";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL10Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";
            grid.FilterExpression = webObjectHandler.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;
            grid.DataBind();
            grid.FilterExpression = _filterexpression;



        }
        protected void grid2_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(OBJ2_pageId, OBJ2_fieldId, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
            if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid2.Table = webObjectHandler.fillSubGrid(OBJ2_pageId, OBJ2_fieldId, _obj.Comp, _obj.UserName, OBJ1_pageId, OBJ1_fieldId, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid2.Table.Columns["ID"];
            this.dtgrid2.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid2";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL10Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";

            DataRow[] FilterEditRows = ((DataTable)Session[_sessionname]).Select("[FILTER EDIT]>0");
            if (FilterEditRows.Length > 0)
            {
                using (DataTable dt = webObjectHandler.GetSubGridInsertPermission(OBJ2_pageId, OBJ2_fieldId, _obj.Comp, _obj.UserName, OBJ1_pageId, OBJ1_fieldId, values))
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtSubInsertPer2.Text = dt.Rows[0]["REDIT"].ToString();
                    }
                    else
                        txtSubInsertPer2.Text = "0";
                }
            }
            else
                txtSubInsertPer2.Text = "1";

            grid.FilterExpression = webObjectHandler.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;
            grid.DataBind();
            grid.FilterExpression = _filterexpression;



        }
        protected void grid3_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(OBJ3_pageId, OBJ3_fieldId, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
            if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid3.Table = webObjectHandler.fillSubGrid(OBJ3_pageId, OBJ3_fieldId, _obj.Comp, _obj.UserName, OBJ1_pageId, OBJ1_fieldId, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid3.Table.Columns["ID"];
            this.dtgrid3.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid3";
            grid.KeyFieldName = "ID";
            string _sessionname = "OPL10Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";
            DataRow[] FilterEditRows = ((DataTable)Session[_sessionname]).Select("[FILTER EDIT]>0");
            if (FilterEditRows.Length > 0)
            {
                using (DataTable dt = webObjectHandler.GetSubGridInsertPermission(OBJ3_pageId, OBJ3_fieldId, _obj.Comp, _obj.UserName, OBJ1_pageId, OBJ1_fieldId, values))
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtSubInsertPer3.Text = dt.Rows[0]["REDIT"].ToString();
                    }
                    else
                        txtSubInsertPer3.Text = "0";
                }
            }
            else
                txtSubInsertPer3.Text = "1";

            grid.FilterExpression = webObjectHandler.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;
            grid.DataBind();
            grid.FilterExpression = _filterexpression;

        }
        protected void grid4_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(OBJ4_pageId, OBJ4_fieldId, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
            if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid4.Table = webObjectHandler.fillSubGrid(OBJ4_pageId, OBJ4_fieldId, _obj.Comp, _obj.UserName, OBJ1_pageId, OBJ1_fieldId, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid4.Table.Columns["ID"];
            this.dtgrid4.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid4";
            grid.KeyFieldName = "ID";
            string _sessionname = "OPL10Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";
            DataRow[] FilterEditRows = ((DataTable)Session[_sessionname]).Select("[FILTER EDIT]>0");
            if (FilterEditRows.Length > 0)
            {
                using (DataTable dt = webObjectHandler.GetSubGridInsertPermission(OBJ4_pageId, OBJ4_fieldId, _obj.Comp, _obj.UserName, OBJ1_pageId, OBJ1_fieldId, values))
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtSubInsertPer4.Text = dt.Rows[0]["REDIT"].ToString();
                    }
                    else
                        txtSubInsertPer4.Text = "0";
                }
            }
            else
                txtSubInsertPer4.Text = "1";

            grid.FilterExpression = webObjectHandler.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;
            grid.DataBind();
            grid.FilterExpression = _filterexpression;

        }
        protected void grid5_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {

            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(OBJ5_pageId, OBJ5_fieldId, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
            if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid5.Table = webObjectHandler.fillSubGrid(OBJ5_pageId, OBJ5_fieldId, _obj.Comp, _obj.UserName, OBJ1_pageId, OBJ1_fieldId, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid5.Table.Columns["ID"];
            this.dtgrid5.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid5";
            grid.KeyFieldName = "ID";
            string _sessionname = "OPL10Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";
            DataRow[] FilterEditRows = ((DataTable)Session[_sessionname]).Select("[FILTER EDIT]>0");
            if (FilterEditRows.Length > 0)
            {
                using (DataTable dt = webObjectHandler.GetSubGridInsertPermission(OBJ5_pageId, OBJ5_fieldId, _obj.Comp, _obj.UserName, OBJ1_pageId, OBJ1_fieldId, values))
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtSubInsertPer5.Text = dt.Rows[0]["REDIT"].ToString();
                    }
                    else
                        txtSubInsertPer5.Text = "0";
                }
            }
            else
                txtSubInsertPer5.Text = "1";

            grid.FilterExpression = webObjectHandler.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;
            grid.DataBind();
            grid.FilterExpression = _filterexpression;


        }
        protected void grid6_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(OBJ6_pageId, OBJ6_fieldId, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
            if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid6.Table = webObjectHandler.fillSubGrid(OBJ6_pageId, OBJ6_fieldId, _obj.Comp, _obj.UserName, OBJ1_pageId, OBJ1_fieldId, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid6.Table.Columns["ID"];
            this.dtgrid6.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid6";
            grid.KeyFieldName = "ID";
            string _sessionname = "OPL10Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";
            DataRow[] FilterEditRows = ((DataTable)Session[_sessionname]).Select("[FILTER EDIT]>0");
            if (FilterEditRows.Length > 0)
            {
                using (DataTable dt = webObjectHandler.GetSubGridInsertPermission(OBJ6_pageId, OBJ6_fieldId, _obj.Comp, _obj.UserName, OBJ1_pageId, OBJ1_fieldId, values))
                {
                    if (dt.Rows.Count > 0)
                    {
                        txtSubInsertPer6.Text = dt.Rows[0]["REDIT"].ToString();
                    }
                    else
                        txtSubInsertPer6.Text = "0";
                }
            }
            else
                txtSubInsertPer6.Text = "1";

            grid.FilterExpression = webObjectHandler.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;
            grid.DataBind();
            grid.FilterExpression = _filterexpression;


        }
        protected void iframecallbackPanel_Callback(object sender, CallbackEventArgsBase e)
        {
            iframeView.Attributes.Add("src", postURL + "?" + postURLParameter + "=" + e.Parameter);

        }

        protected void btnhat_Click(object sender, EventArgs e)
        {
            throw new Exception(txttask.Text);
        }

        protected void grid_RowValidating(object sender, ASPxDataValidationEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            string sessionname = string.Empty;
            switch (gridI.ID)
            {
                case "grid1":
                    sessionname = "OPL10Grid1Rows";
                    break;
                case "grid2":
                    sessionname = "OPL10Grid2Rows";
                    break;
                case "grid3":
                    sessionname = "OPL10Grid3Rows";
                    break;
                case "grid4":
                    sessionname = "OPL10Grid4Rows";
                    break;
                case "grid5":
                    sessionname = "OPL10Grid5Rows";
                    break;
                case "grid6":
                    sessionname = "OPL10Grid6Rows";
                    break;
                default:
                    break;
            }

            if (e.NewValues["REDIT"] == null)
                e.NewValues["REDIT"] = 0;

            //DataRow[] rows2 = ((DataTable)Session[sessionname]).Select("[DX Type1]='DATA'");
            //foreach (DataRow row in rows2)
            //{
            //    if (e.NewValues[row["DX Field ID"].ToString()] == null)
            //        e.NewValues[row["DX Field ID"].ToString()] = "";
            //}

            DataRow[] rows1 = ((DataTable)Session[sessionname]).Select("[DX Type1]='DECIMAL' OR [DX Type2]='INTEGER'");
            foreach (DataRow row in rows1)
            {
                if (e.NewValues[row["DX Field ID"].ToString()] == null)
                    e.NewValues[row["DX Field ID"].ToString()] = 0;
            }





        }

        protected void grid_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;
            GridViewCommandColumn col = grid.Columns["cmnd"] as GridViewCommandColumn;
            string perm = "0";
            switch (grid.ID)
            {
                case "grid2":
                    perm = txtSubInsertPer2.Text;
                    break;
                case "grid3":
                    perm = txtSubInsertPer3.Text;
                    break;
                case "grid4":
                    perm = txtSubInsertPer4.Text;
                    break;
                case "grid5":
                    perm = txtSubInsertPer5.Text;
                    break;
                case "grid6":
                    perm = txtSubInsertPer6.Text;
                    break;

            }

            if (perm == "0")
            {
                try
                {
                    if (col.ShowNewButtonInHeader)
                        col.ShowNewButtonInHeader = false;
                }
                catch { }
                try
                {
                    foreach (GridViewCommandColumnCustomButton item in col.CustomButtons)
                    {
                        if (item.ID == "Clone") col.CustomButtons.Remove(item);

                    }
                }
                catch { }
            }

        }

        string AddValues(string _val1, string _val2)
        {
            if (string.IsNullOrEmpty(_val1))
                _val1 = _val2;
            else
                _val1 += "," + _val2;
            return _val1;
        }

        protected void Grid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            ASPxGridView Igrid = sender as ASPxGridView;
            string _sessionname = "OPL10Grid" + Igrid.ID.Substring(Igrid.ID.Length - 1) + "Rows";

            DataRow[] rows = ((DataTable)Session[_sessionname]).Select("[DX MouseOver Text]<>'' and [DX Field ID]='" + e.DataColumn.FieldName + "'");
            if (rows.Length > 0)
            {
                try
                {
                    string _val = Igrid.GetRowValues(e.VisibleIndex, rows[0]["DX MouseOver Text"].ToString()).ToString();
                    e.Cell.ToolTip = _val;
                }
                catch { e.Cell.ToolTip = string.Format("{0}", e.CellValue); }
            }
            else
                e.Cell.ToolTip = string.Format("{0}", e.CellValue);

            try
            {
                if (e.DataColumn is GridViewDataHyperLinkColumn)
                {
                    if (string.IsNullOrEmpty(e.CellValue.ToString()))
                        e.Cell.Controls[0].Visible = false;

                }
            }
            catch
            {

            }
        }

        

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string[] splitter = txtValues.Text.Split(',');

            string d1 = string.Empty;
            if (!string.IsNullOrEmpty(txtInvDate.Text))
                d1 = string.Format("{0:dd.MM.yyyy}", txtInvDate.Date);
             
            GetWebFonksiyonlari(_obj.Comp).ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "KTFCreateInvoice§" + splitter[0] + "§" + splitter[1] + "§" + method.DateToStringNAVType(d1) + "§" + _obj.UserName, _obj.Comp);
            MessageBox("Faturalar Oluşturuldu.");



        }


        protected void callbackKTFMessage_Callback(object sender, CallbackEventArgsBase e)
        {
            txtKTFmessage.Text = GetWebFonksiyonlari(_obj.Comp).ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "KTFInvoicesPreview§" + HiddenP1.Value + "§" + HiddenP2.Value + "§" + _obj.UserName, _obj.Comp);
             
        }

        protected void RdList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string _url = "";
            switch (RdList.SelectedItem.Value.ToString())
            {
                case "A":
                    _url = "~/OnePageList.aspx?OJ1=P140010;O140001&OJ2=P140010;O140005&OJ3=P140010;O140006&OJ4=P140010;OJ9999&OJ5=P140010;OJ9999&OJ6=P140010;O140004&GId=" + Request.QueryString["GId"];

                    break;
                case "B":
                    _url = "~/OnePageList.aspx?OJ1=P140010;O140002&OJ2=P140010;O140005&OJ3=P140010;O140006&OJ4=P140010;OJ9999&OJ5=P140010;OJ9999&OJ6=P140010;O140004&GId=" + Request.QueryString["GId"];

                    break;

            }
            Response.Redirect(_url);
        }

        public string pageOrderType
        {
            get
            {
                string _type = "";
                if (OBJ1_pageId == "P140010")
                {
                    _type = "VOYAGE";
                }
                else
                {
                    _type = "VOYAGE";
                }

                return _type;
            }
        }

        public string postURL
        {
            get
            {
                string _url = "";
                if (pageOrderType == "VOYAGE")
                {
                    _url = "/Cards/VoyageCard.aspx?GId=" + Request.QueryString["GId"];
                }
                else
                {
                    _url = "/Cards/VoyageCard.aspx?GId=" + Request.QueryString["GId"];
                }
                return _url;
            }
        }
        public string postURLParameter
        {
            get
            {
                string _url = "";
                if (pageOrderType == "VOYAGE")
                {
                    _url = "currentNo";
                }
                else
                {
                    _url = "currentNo";
                }
                return _url;
            }
        }
        public string postURLParameter2
        {
            get
            {
                string _url = "";
                if (pageOrderType == "VOYAGE")
                {
                    _url = "currentTaskNo";
                }
                else
                {
                    _url = "currentTaskNo";
                }
                return _url;
            }
        }

        protected void BtnProc_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtDate.Date.ToString()))
            {
                MessageBox("Lütfen Tarih alanını doldurunuz.");
                return;
            }
            string d1 = string.Empty;
            if (!string.IsNullOrEmpty(txtDate.Text))
                d1 = string.Format("{0:dd.MM.yyyy}", txtDate.Date);



            DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(txtCurGridPageId.Text, txtCurGridObjecId.Text, _obj.Comp).Tables[0].Select("[DX Field ID]='" + txtCurGridFieldId.Text + "'");

            switch (txtCurGridId.Text)
            {
                case "1":
                    ServiceUtilities.ProcSelectedRows(grid1, rows, _obj.UserName, _obj.Comp );
                    break;
                case "2":
                    ServiceUtilities.ProcSelectedRows(grid2, rows, _obj.UserName, _obj.Comp );
                    break;
                case "3":
                    ServiceUtilities.ProcSelectedRows(grid3, rows, _obj.UserName, _obj.Comp );
                    break;
                case "4":
                    ServiceUtilities.ProcSelectedRows(grid4, rows, _obj.UserName, _obj.Comp );
                    break;
                case "5":
                    ServiceUtilities.ProcSelectedRows(grid5, rows, _obj.UserName, _obj.Comp );
                    break;
                case "6":
                    ServiceUtilities.ProcSelectedRows(grid6, rows, _obj.UserName, _obj.Comp );
                    break;
            }

            txtDate.Text = string.Empty;

            if (!Request.RawUrl.Contains("P1="))
                Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
            else
                Response.Redirect(Request.RawUrl);


        }
    }
}