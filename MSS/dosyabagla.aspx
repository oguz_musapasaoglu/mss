﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="dosyabagla.aspx.cs" Inherits="MSS1.dosyabagla" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />


    <script type="text/javascript">
        function proceed(sipno, tur, dtur, pageTur) {
            if (dtur == 12) {
                window.opener.alert('Dosyanız başarıyla eklenmiştir.');
                self.close();
                return;
            }
            window.opener.location.href = window.opener.location.href;
            self.close();

        }

        function proceed3(fieldId, ObjectId) {
            if (ObjectId == 1)
                window.opener.grid1.PerformCallback('SelectButton|' + fieldId);
            else if (ObjectId == 2)
                window.opener.grid2.PerformCallback('SelectButton|' + fieldId);
            else if (ObjectId == 3)
                window.opener.grid3.PerformCallback('SelectButton|' + fieldId);
            else if (ObjectId == 4)
                window.opener.grid4.PerformCallback('SelectButton|' + fieldId);
            else if (ObjectId == 5)
                window.opener.grid5.PerformCallback('SelectButton|' + fieldId);
            else if (ObjectId == 6)
                window.opener.grid6.PerformCallback('SelectButton|' + fieldId);
            else if (ObjectId == 7)
                window.opener.grid7.PerformCallback('SelectButton|' + fieldId);
            else if (ObjectId == 8)
                window.opener.grid8.PerformCallback('SelectButton|' + fieldId);
            else if (ObjectId == 9)
                window.opener.grid9.PerformCallback('SelectButton|' + fieldId);
            else if (ObjectId == 10)
                window.opener.grid10.PerformCallback('SelectButton|' + fieldId);
            else if (ObjectId == 11)
                window.opener.grid11.PerformCallback('SelectButton|' + fieldId);
            else if (ObjectId == 12)
                window.opener.grid12.PerformCallback('SelectButton|' + fieldId);
            else if (ObjectId == 13)
                window.opener.grid13.PerformCallback('SelectButton|' + fieldId);
            else if (ObjectId == 14)
                window.opener.grid14.PerformCallback('SelectButton|' + fieldId);
            else if (ObjectId == 15)
                window.opener.grid15.PerformCallback('SelectButton|' + fieldId);
            self.close();

        }

    </script>

    <script type="text/javascript" src="../js/popuputilities2.js?v=1"></script>

    <link href="style.css" rel="stylesheet" type="text/css" />
    <link href="tools.css" rel="stylesheet" type="text/css" />
    <link href="table.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #overlay {
            position: fixed;
            z-index: 3;
            top: 0px;
            left: 0px;
            background-color: #f8f8f8;
            width: 100%;
            height: 100%;
            filter: Alpha(Opacity=90);
            opacity: 0.9;
            -moz-opacity: 0.9;
            visibility: hidden;
        }

        #theprogress {
            background-color: #fff;
            border: 1px solid #ccc;
            padding: 10px;
            width: 300px;
            height: 70px;
            line-height: 30px;
            text-align: center;
            filter: Alpha(Opacity=100);
            opacity: 1;
            -moz-opacity: 1;
        }

        #modalprogress {
            position: absolute;
            top: 40%;
            left: 50%;
            margin: -11px 0 0 -150px;
            color: #990000;
            font-weight: bold;
            font-size: 14px;
        }

        .style1 {
            text-align: left;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function displayLoadingImage() {
            var imgLoading = document.getElementById("imgLoading");
            var overlay = document.getElementById("overlay");
            var lblyukleniyor = document.getElementById("lblyukleniyor");
            overlay.style.visibility = "visible";
            imgLoading.style.visibility = "visible";
            lblyukleniyor.style.visibility = "visible";
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scriptManager" runat="server" EnablePageMethods="true" />
        <br />
        <table>
            <tr>
                <td class="style3">
                    <asp:Label ID="lblDosyaNo" runat="server"></asp:Label>
                </td>
                <td class="style1">
                    <asp:Label ID="lblno" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Label ID="lblDosyaAdi" runat="server"></asp:Label>
                </td>
                <td class="style1">
                    <asp:TextBox ID="txtdosyaadi" CssClass="textbox" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Label ID="lblDosyaSec" runat="server"></asp:Label>
                    &nbsp;</td>
                <td class="style1">
                    <asp:FileUpload ID="FileUpload1" CssClass="textbox" runat="server" />
                </td>
            </tr>
            <tr runat="server" id="trcust" visible="false">
                <td class="style3">
                    <asp:Label ID="lblcomp" runat="server" Text="CUSTOMER / VENDOR"></asp:Label>
                    &nbsp;</td>
                <td class="style1">
                    <dx:ASPxComboBox ID="cmbCustomer" runat="server" CssClass="dropdownStyle" Width="100%" DataSourceID="DSDyn"
                        ValueField="No_" TextField="Name" ClientInstanceName="cmbCustomer" Font-Bold="false" Font-Names="Arial, sans-serif" Font-Size="8pt"
                        AutoPostBack="false">
                    </dx:ASPxComboBox>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Label ID="lblYorum" runat="server"></asp:Label>
                </td>
                <td class="style1">
                    <asp:TextBox ID="txtyorum" CssClass="textbox" runat="server" Height="60px" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <div id="overlay">
                        <div id="modalprogress">
                            <div id="theprogress">
                                <img src="images/ajax-loader.gif" name="imgLoading" id="imgLoading" style="visibility: hidden;">
                                <br />
                                <div id="yukleniyor" style="visibility: hidden;">
                                    <asp:Label ID="lblyukleniyor" runat="server" Text="Yükleniyor..."></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:Label ID="lblTayfaID" runat="server"></asp:Label>
                </td>
                <td class="style1">
                    <asp:Button ID="btndosyayukle" runat="server" CssClass="myButton"
                        OnClick="btndosyayukle_Click" />
                </td>
            </tr>
        </table>
    </form>
    <div style="display: none">
        <asp:SqlDataSource ID="DSDyn" runat="server" ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
        <asp:Label runat="server" ID="lblLineNo"></asp:Label>
        <asp:Label runat="server" ID="lblFileName"></asp:Label>
    </div>
</body>
</html>
