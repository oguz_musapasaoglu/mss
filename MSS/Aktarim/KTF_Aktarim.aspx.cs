﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.IO;
using System.Data.OleDb;
using System.Data;
using System.Data.Odbc;
using DevExpress.Spreadsheet;
using DevExpress.Web.Office;
using MSS1.WSGeneric;
using System.ServiceModel;
using DevExpress.Spreadsheet.Export;
using System.Data.SqlClient;
using MSS1.Codes;

namespace MSS1.Aktarim
{
    public partial class KTF_Aktarim : Bases
    {
        Language l = new Language();
        public ws_baglantı ws = new ws_baglantı();
        fonksiyonlar f = new fonksiyonlar();
        Methods method = new Methods();
        SessionBase _obj;

        string FilePath
        {
            get { return Session["ExcelFilePath"] == null ? String.Empty : Session["ExcelFilePath"].ToString(); }
            set { Session["ExcelFilePath"] = value; }
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (IsPostBack || IsCallback) return;
            FilePath = String.Empty;

        }
        protected void Page_Load(object sender, EventArgs e)
        {
             
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }



        protected void Upload_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
        {

            if (System.IO.File.Exists(Server.MapPath("~/" + CurrentFolderName(_obj.Comp) + "/Aktarim/") + e.UploadedFile.FileName))
                System.IO.File.Delete(Server.MapPath("~/" + CurrentFolderName(_obj.Comp) + "/Aktarim/") + e.UploadedFile.FileName);


            String uploadedFilePath = Server.MapPath("~/" + CurrentFolderName(_obj.Comp) + "/Aktarim/") + e.UploadedFile.FileName;
            e.UploadedFile.SaveAs(uploadedFilePath);
            if (!String.IsNullOrEmpty(uploadedFilePath))
                DocumentManager.CloseDocument(uploadedFilePath);
            FilePath = uploadedFilePath;
            Lst.Visible = false;
        }

        protected void Spreadsheet_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(FilePath))
            {
                Spreadsheet.Open(FilePath);
            }
        }

        protected void btnAktar_Click(object sender, EventArgs e)
        { 
            string aktarimNo = GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "KTFGetAktarimNo", _obj.Comp);

            Worksheet Ws = Spreadsheet.Document.Worksheets.ActiveWorksheet;
            int oRow = Ws.Rows.LastUsedIndex;
            Range range = Ws.GetDataRange();
            DataTable dt = Ws.CreateDataTable(range, true);
            DataTableExporter exp = Ws.CreateDataTableExporter(range, dt, true);
            exp.Export();
            DataColumn clm = new DataColumn("AktarimNo", typeof(string));
            clm.DefaultValue = aktarimNo;
            dt.Columns.Add(clm);
            datatableEdit(dt);

            if (dt.Rows.Count == 0)
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "ktfaktarim", "msg001"));
                return;
            }

            using (SqlConnection conn = method.returnConnection())
            {
                conn.Open();
                using (SqlBulkCopy bulk = new SqlBulkCopy(conn))
                { 
                    bulk.DestinationTableName = "LAMKARA.[dbo].[LAMKARA$KTF Transactions Temp]"; 
                    bulk.ColumnMappings.Add("Voyage", "Voyage"); 
                    bulk.ColumnMappings.Add("PointTo / Point From", "Point To _ From"); 
                    bulk.ColumnMappings.Add("BL No", "BkgRef _ BL No"); 
                    bulk.ColumnMappings.Add("Carrier (KUMPANYA)", "Carrier"); 
                    bulk.ColumnMappings.Add("Type (IMP /EXP)", "Type"); 
                    bulk.ColumnMappings.Add("Vessel", "Vessel"); 
                    bulk.ColumnMappings.Add("POD", "POD"); 
                    bulk.ColumnMappings.Add("POL", "POL"); 
                    bulk.ColumnMappings.Add("Container No", "Container"); 
                    bulk.ColumnMappings.Add("ContSizeType", "ContSizeType _ Pack"); 
                    bulk.ColumnMappings.Add("Shipping Type", "FE _ Shipping Type"); 
                    bulk.ColumnMappings.Add("3rd Party", "3rd Party"); 
                    bulk.ColumnMappings.Add("AktarimNo", "Aktarim No_"); 
                    bulk.ColumnMappings.Add("Commodity", "Commodity"); 
                    bulk.ColumnMappings.Add("Cnee / Bkg Party", "Cnee _ Bkg Party"); 
                    bulk.ColumnMappings.Add("Notify", "Notify"); 
                    bulk.ColumnMappings.Add("SOC Manuel ", "SOC"); 
                    bulk.WriteToServer(dt); 
                    bulk.Close();

                    string message = GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "KTFExcelAktar" + "§" + aktarimNo + "§" + _obj.UserName, _obj.Comp);
                    MessageBox(message);
                    string truncate = "Truncate Table LAMKARA.[dbo].[LAMKARA$KTF Transactions Temp];";
                    f.updateyap(truncate);

                }
            }  
        }
        private void datatableEdit(DataTable dt)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i][0] = dt.Rows[i][0] == DBNull.Value ? "" : dt.Rows[i][0];
                dt.Rows[i][1] = dt.Rows[i][1] == DBNull.Value ? "" : dt.Rows[i][1];
                dt.Rows[i][2] = dt.Rows[i][2] == DBNull.Value ? "" : dt.Rows[i][2];
                dt.Rows[i][3] = dt.Rows[i][3] == DBNull.Value ? "" : dt.Rows[i][3];
                dt.Rows[i][4] = dt.Rows[i][4] == DBNull.Value ? "" : dt.Rows[i][4];
                dt.Rows[i][5] = dt.Rows[i][5] == DBNull.Value ? "" : dt.Rows[i][5];
                dt.Rows[i][6] = dt.Rows[i][6] == DBNull.Value ? "" : dt.Rows[i][6];
                dt.Rows[i][7] = dt.Rows[i][7] == DBNull.Value ? "" : dt.Rows[i][7];
                dt.Rows[i][8] = dt.Rows[i][8] == DBNull.Value ? "" : dt.Rows[i][8];
                dt.Rows[i][9] = dt.Rows[i][9] == DBNull.Value ? "" : dt.Rows[i][9];
                dt.Rows[i][10] = dt.Rows[i][10] == DBNull.Value ? "" : dt.Rows[i][10];
                dt.Rows[i][11] = dt.Rows[i][11] == DBNull.Value ? "" : dt.Rows[i][11];
                dt.Rows[i][12] = dt.Rows[i][12] == DBNull.Value ? "" : dt.Rows[i][12];
                dt.Rows[i][13] = dt.Rows[i][13] == DBNull.Value ? "" : dt.Rows[i][13];
                dt.Rows[i][14] = dt.Rows[i][14] == DBNull.Value ? "" : dt.Rows[i][14];
                dt.Rows[i][15] = dt.Rows[i][15] == DBNull.Value ? "" : dt.Rows[i][15];
                dt.Rows[i][16] = dt.Rows[i][16] == DBNull.Value ? "" : dt.Rows[i][16];



            }
        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            Web_GenericFunction_PortClient webFonksiyonlari;
            BasicHttpBinding _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            EndpointAddress _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }
    }
}