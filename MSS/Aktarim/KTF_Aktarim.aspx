﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="KTF_Aktarim.aspx.cs" Inherits="MSS1.Aktarim.KTF_Aktarim" 
     EnableEventValidation="false"
    MaintainScrollPositionOnPostback="true"%>

<%@ Register Assembly="DevExpress.Web.ASPxSpreadsheet.v19.1" Namespace="DevExpress.Web.ASPxSpreadsheet" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function OnFileUploadComplete(s, e) {
            Spreadsheet.PerformCallback();
        }
        function UploadCompleteMessage() {

            alert($('#<%= lblFileSuccess.ClientID %>').val());

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table border="0" style="width: 950px">
        <tr>
            <td style="text-align: left; width: 250px">

                <dx:ASPxUploadControl ID="UploadCallbackSpreadsheet" Width="450px" runat="server" ShowUploadButton="false" OnFileUploadComplete="Upload_FileUploadComplete">
                    <ValidationSettings AllowedFileExtensions=".csv,.xlsx,.xls" NotAllowedFileExtensionErrorText="Sadece csv ve xlsx dosyaları">
                    </ValidationSettings>
                    <ClientSideEvents FileUploadComplete="OnFileUploadComplete" />
                    <ClientSideEvents TextChanged="function(s, e) {
                          s.SetText(e.inputIndex);
                        s.Upload();
                        }" />
                </dx:ASPxUploadControl>


            </td>
            <td style="text-align: left; width: 700px" colspan="2">
                <asp:Button runat="server" ID="btnAktar" Text="AKTAR" CssClass="btn btn-primary" OnClick="btnAktar_Click" />
            </td>

        </tr>
        <tr>
            <td colspan="2">
                <asp:ListBox ID="Lst" Width="500px" runat="server" Visible="false"></asp:ListBox>
            </td>
        </tr>
    </table>



    <div style="display: none">
        <asp:Label ID="lblFileSuccess" Text="Yükleme Tamam." runat="server"></asp:Label>
        <asp:Label ID="lblSuccess" Text="Aktarım Tamamlandı" runat="server"></asp:Label>
        <asp:Label ID="lblUnSuccess" Text="Aktarım esnasında hata" runat="server"></asp:Label>
        <asp:Label ID="lblPleaseSelectFile" Text="Lütfen bir dosya seçiniz" runat="server"></asp:Label>


        <dx:ASPxSpreadsheet ID="Spreadsheet" OnLoad="Spreadsheet_Load" ClientInstanceName="Spreadsheet" Width="300px" Height="150px" runat="server">
            <ClientSideEvents EndCallback="function(s,e){ UploadCompleteMessage();  }" />
        </dx:ASPxSpreadsheet>
    </div>
</asp:Content>


