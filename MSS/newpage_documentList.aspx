﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" EnableViewState="true" CodeBehind="newpage_documentList.aspx.cs" Inherits="MSS1.newpage_documentList" %>

<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Src="~/Controls/DataTableControlGAC.ascx" TagName="StDataTable" TagPrefix="STDT" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .TD {
            padding: 0px;
            color: black;
            font-weight: 400;
            font-style: normal;
            text-decoration: none;
            font-family: Calibri, sans-serif;
            text-align: center;
            vertical-align: bottom;
            border: none;
        }

        html body {
            width: 98%;
            height: 98%;
        }
    </style>
    <script type="text/javascript">
        function OnSplitterPaneResized(s, e) {
            var name = e.pane.name;
            //if (name == 'listBoxContainer')
            //    ResizeControl(LstGroups, e.pane);
            if (name == 'gridContainer')
                ResizeControl(grid, e.pane);
        }
        function ResizeControl(control, splitterPane) {
            control.SetWidth(splitterPane.GetClientWidth());
            control.SetHeight(splitterPane.GetClientHeight());
        }
        function OnGridEndCallback() {
            sampleSplitter.GetPaneByName("gridContainer").RaiseResizedEvent();
        }
        function GoTo(values, Company) {
            window.open("newpage_documentCard.aspx?EntryNo=" + values + "&Company=" + Company, "_self");
        }
        function openInNewTab(url) {
            var win = window.open(url, '_blank');
            win.focus();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <asp:Label ID="lblsayfaadi" Text="DOKÜMAN TAKİP" CssClass="SayfaBaslik" runat="server"></asp:Label></h2>
    <dx:ASPxSplitter ID="ASPxSplitter1" Theme="iOS" runat="server" Height="450px" Width="100%" ClientInstanceName="sampleSplitter">
        <Panes>
            <dx:SplitterPane ShowCollapseBackwardButton="True" Size="20%" Name="listBoxContainer" ScrollBars="Vertical">
                <ContentCollection>
                    <dx:SplitterContentControl ID="SplitterContentControl1" runat="server">
                        <dx:ASPxListBox runat="server" ID="LstGroups" SelectionMode="Single" Theme="DevEx" ClientInstanceName="LstGroups"
                            SelectedIndex="0" Height="100%" Width="100%">
                            <Border BorderWidth="0px" />
                            <ItemStyle Cursor="pointer" />
                            <ClientSideEvents ValueChanged="function(s,e){grid.PerformCallback('A|'+s.GetValue());}" />
                        </dx:ASPxListBox>
                    </dx:SplitterContentControl>
                </ContentCollection>
            </dx:SplitterPane>
            <dx:SplitterPane MinSize="350px">
                <Panes>
                    <dx:SplitterPane Size="80%" Name="gridContainer" ScrollBars="Vertical">
                        <ContentCollection>
                            <dx:SplitterContentControl ID="SplitterContentControl2" runat="server">
                                <dx:ASPxMenu ID="menu" runat="server" AutoSeparators="RootOnly"
                                    Theme="Glass"
                                    ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                    ShowSubMenuShadow="False" OnItemClick="menu_ItemClick">
                                    <SubMenuStyle GutterWidth="0px" />
                                    <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                    <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                    </SubMenuItemStyle>
                                    <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12"
                                        X="2" Y="-12" />
                                    <Items>
                                        <dx:MenuItem Name="ExportExcel" ToolTip="Excel'e Aktar" Text="">
                                            <Image Url="~/images/excelico.png" />
                                        </dx:MenuItem>
                                        <dx:MenuItem Name="ExportPdf" ToolTip="Pdf'e Aktar" Text="">
                                            <Image Url="~/images/pdfico.png" />
                                        </dx:MenuItem>
                                    </Items>
                                </dx:ASPxMenu>
                                <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False" Width="100%" Theme="Glass"
                                    KeyFieldName="ID" DataSourceID="STDT1" EnableRowsCache="false" 
                                    OnCustomCallback="grid_CustomCallback" OnAfterPerformCallback="grid_AfterPerformCallback" 
                                    OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared">
                                    <Settings ShowStatusBar="Visible" ShowPreview="true" ShowTitlePanel="true" ShowFilterRow="true" ShowGroupPanel="true" VerticalScrollBarMode="Visible" VerticalScrollableHeight="600" HorizontalScrollBarMode="Visible" />
                                    <SettingsBehavior ColumnResizeMode="Control" AutoExpandAllGroups="false" />
                                    <SettingsPager Mode="ShowPager" PageSize="100"></SettingsPager>
                                    <SettingsText GroupPanel="Gruplamak istediğiniz kolonu bu alana sürükleyebilirsiniz." EmptyDataRow="#" />

                                    <Border BorderWidth="0px" />
                                    <SettingsCommandButton>
                                        <ClearFilterButton Image-ToolTip="Clear Filter" Image-Url="~/images/clear_filter.png"></ClearFilterButton>
                                    </SettingsCommandButton>
                                    <Styles>
                                        <Header ForeColor="White" CssClass="grid1CaptionStyle"></Header>
                                        <HeaderPanel ForeColor="White" CssClass="grid1CaptionStyle"></HeaderPanel>
                                        <GroupPanel Border-BorderStyle="None"></GroupPanel>
                                    </Styles>
                                    <ClientSideEvents EndCallback="function(s, e) {  OnGridEndCallback(); }" />
                                    <Columns>
                                        <dx:GridViewCommandColumn ButtonType="Image" Width="80px" ShowClearFilterButton="true" ShowSelectCheckbox="true" VisibleIndex="0">
                                            <HeaderTemplate>
                                                <input id="Button1" type="button" style="font-size: 16px; width: 20px" onclick="grid.PerformCallback('B|true');" value="+"
                                                    title="Tümünü Seç" />
                                                <input id="Button2" type="button" style="font-size: 16px; width: 20px" onclick="grid.PerformCallback('B|false');" value="-"
                                                    title="Tümünü Seçme" />
                                            </HeaderTemplate>
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataColumn Visible="false" FieldName="ID" VisibleIndex="0">
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn CellStyle-HorizontalAlign="Right" Caption=" " Width="30px" VisibleIndex="1">
                                            <DataItemTemplate>
                                                <img src="images/Attach.png" alt="Dosyayı Aç" style="cursor: pointer"
                                                    onclick="openInNewTab('<%#Eval("FilePath") %>')" />
                                            </DataItemTemplate>
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn Caption="ENTRY NO" FieldName="EntryNo" Width="80px" VisibleIndex="2">
                                            <DataItemTemplate>
                                                <dx:ASPxHyperLink ID="IssueLink" runat="server" ForeColor="#000066" NavigateUrl=<%#"JavaScript:GoTo('"+Eval("EntryNo")+"','"+Eval("DocCompanyInt")+"');"%>
                                                    Text='<%#Eval("EntryNo")%>'>
                                                </dx:ASPxHyperLink>
                                            </DataItemTemplate>
                                            <Settings AutoFilterCondition="Contains" />
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn Caption="ŞİRKET" Width="65px" FieldName="DocCompany" VisibleIndex="3">
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn Caption="NO" Width="100px" FieldName="RelatedDocNo" VisibleIndex="4">
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn Caption="ADI" Width="120px" FieldName="RelatedDocName" VisibleIndex="5">
                                            <CellStyle Wrap="False"></CellStyle>
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn Caption="DOKÜMAN GRUBU" Width="100px" FieldName="DocGrup" VisibleIndex="6">
                                            <HeaderCaptionTemplate>
                                                DOKÜMAN<br />
                                                GRUBU
                                            </HeaderCaptionTemplate>
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn Caption="DOKÜMAN TİPİ" Width="100px" FieldName="DocType" VisibleIndex="7">
                                            <HeaderCaptionTemplate>
                                                DOKÜMAN<br />
                                                TİPİ
                                            </HeaderCaptionTemplate>
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn Caption="BELGE ADI" GroupIndex="0" Width="100px" FieldName="DocName" VisibleIndex="8">
                                            <HeaderCaptionTemplate>
                                                BELGE<br />
                                                ADI
                                            </HeaderCaptionTemplate>
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataColumn Caption="VERİLDİĞİ YER ADI" Width="100px" FieldName="DocCreatedDepName" VisibleIndex="9">
                                            <HeaderCaptionTemplate>
                                                VERİLDİĞİ YER<br />
                                                ADI
                                            </HeaderCaptionTemplate>
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataDateColumn Width="80px" Caption="BELGE TARİHİ" FieldName="DocDate" VisibleIndex="10">
                                            <HeaderCaptionTemplate>
                                                BELGE<br />
                                                TARİHİ
                                            </HeaderCaptionTemplate>
                                            <PropertiesDateEdit DisplayFormatString="dd.MM.yyyy" DateOnError="Null" MinDate="01.01.2000" EditFormatString="dd.MM.yyyy"></PropertiesDateEdit>
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataDateColumn Width="80px" Caption="GEÇERLİLİK TARİHİ" FieldName="DocValidityDate" VisibleIndex="11">
                                            <HeaderCaptionTemplate>
                                                GEÇERLİLİK<br />
                                                TARİHİ
                                            </HeaderCaptionTemplate>
                                            <PropertiesDateEdit DisplayFormatString="dd.MM.yyyy" DateOnError="Null" MinDate="01.01.2000" EditFormatString="dd.MM.yyyy"></PropertiesDateEdit>
                                        </dx:GridViewDataDateColumn>
                                        <dx:GridViewDataColumn Caption="DURUM" Width="100px" FieldName="DocStatus" VisibleIndex="10">
                                        </dx:GridViewDataColumn>
                                        <dx:GridViewDataDateColumn Width="80px" Caption="KAYIT TARİHİ" FieldName="DocCreatedDate" VisibleIndex="12">
                                            <HeaderCaptionTemplate>
                                                KAYIT<br />
                                                TARİHİ
                                            </HeaderCaptionTemplate>
                                            <PropertiesDateEdit DisplayFormatString="dd.MM.yyyy" DateOnError="Null" MinDate="01.01.2000" EditFormatString="dd.MM.yyyy"></PropertiesDateEdit>
                                        </dx:GridViewDataDateColumn>
                                    </Columns>
                                </dx:ASPxGridView>
                                <dx:ASPxGridViewExporter ID="gridExport" ExportedRowType="Selected" runat="server" GridViewID="grid"></dx:ASPxGridViewExporter>
                            </dx:SplitterContentControl>
                        </ContentCollection>
                    </dx:SplitterPane>
                    <dx:SplitterPane Name="editorsContainer" MinSize="200px" Collapsed="true" ShowCollapseForwardButton="True">
                        <ContentCollection>
                            <dx:SplitterContentControl runat="server">
                                <table id="editorsTable">
                                    <tr>
                                        <td style="white-space: nowrap; text-align: right">
                                            <dx:ASPxLabel ID="lblMailAdress" Theme="Glass" runat="server" Text="MAIL ADRESLERİ :">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td style="width: 100%">
                                            <dx:ASPxTextBox ID="txtMailAdresses" Theme="Glass" runat="server" Width="100%" ClientInstanceName="textBoxContactName">
                                            </dx:ASPxTextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="white-space: nowrap; text-align: right">
                                            <dx:ASPxLabel ID="lblSubject" Theme="Glass" runat="server" Text="KONU :">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td>
                                            <dx:ASPxTextBox ID="txtSubject" runat="server" Width="100%" ClientInstanceName="textBoxCompanyName">
                                            </dx:ASPxTextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: text-top; white-space: nowrap; padding-top: 2px; text-align: right">
                                            <dx:ASPxLabel ID="lblMailBody" Theme="Glass" runat="server" Text="MAIL BODY :">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td id="memoCell">
                                            <dx:ASPxMemo ID="txtMailBody" runat="server" Theme="Glass" Height="71px" Width="100%" Style="padding: 0px 3px 0px 3px;" ClientInstanceName="memoStuff">
                                            </dx:ASPxMemo>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left">&nbsp;
                                        </td>
                                        <td style="text-align: left">
                                            <asp:Button runat="server" CssClass="formglobalbuttonStyle" OnClick="btnSendSelectedRows_Click" Text="SEÇİLENLERİ MAIL GÖNDER" ID="btnSendSelectedRows" />
                                        </td>
                                    </tr>
                                </table>
                                <br />
                            </dx:SplitterContentControl>
                        </ContentCollection>
                    </dx:SplitterPane>
                </Panes>
            </dx:SplitterPane>

        </Panes>
        <ClientSideEvents PaneResized="OnSplitterPaneResized" />
    </dx:ASPxSplitter>
    <div style="display: none">
        <STDT:StDataTable ID="STDT1" runat="server" />
        <STDT:StDataTable ID="DTGroups" runat="server" />
        <asp:Label ID="lblvergidaireno" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblKontrol" runat="server" Visible="False"></asp:Label>
    </div>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true" EnableScriptGlobalization="true"
        EnableScriptLocalization="true">
    </asp:ScriptManager>




</asp:Content>

