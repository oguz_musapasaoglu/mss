﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.WSGeneric;
using System.ServiceModel;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using MSS1.Codes;

namespace MSS1
{
    public partial class iletisimgirisiV2 : Bases
    {
        #region Connection String & Service
        fonksiyonlar f = new fonksiyonlar();
        Language l = new Language();

        public ws_baglantı ws = new ws_baglantı();
        SqlConnection baglanti;
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        int optionselectedSirket;
        ListItem katilimcili = new ListItem();
        #endregion


        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        SessionBase _obj;
        static SessionBase _objStatic;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }

        #region Service Arama


        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> searchRequester(string prefixText, int count)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
            string query = "Select [First Name]+' '+[Last Name] +'|'+ No_ Name from [0C_05200_00_EMPLOYEE]";
            if (prefixText != "")
            {
                query = query + " where COMPANY='" + _objStatic.Comp.ToString() + "' and [First Name]+' '+[Last Name] like '" + prefixText.ToUpper() + "%'";
            }
            SqlCommand cmd = new SqlCommand(query, conn);
            conn.Open();
            List<string> bul = new List<string>();
            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
                while (sdr.Read())
                {
                    bul.Add(sdr["Name"].ToString());
                }
            }
            conn.Close();

            return bul;


        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchContact(string prefixText, int count)
        {

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_aday_ara";
                    cmd.Parameters.AddWithValue("@ad", prefixText.Replace("*", "").ToUpper());
                    cmd.Parameters.AddWithValue("@unvan", "");
                    cmd.Parameters.AddWithValue("@stemsil", "");
                    cmd.Parameters.AddWithValue("@aday", "BOS");
                    cmd.Parameters.AddWithValue("@sehir", "");
                    cmd.Parameters.AddWithValue("@sirket", _objStatic.Comp.ToString());
                    cmd.Parameters.AddWithValue("@kad", _objStatic.UserName.ToString());
                    cmd.Connection = conn;
                    conn.Open();
                    List<string> Jobtask = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Jobtask.Add(sdr["Name"].ToString());
                        }
                    }
                    conn.Close();
                    return Jobtask;
                }
            }
        }
        #endregion

        private void dilgetir()
        {
            Language l = new Language();

            if (Request.QueryString["EntryNo"] != null)
            {
                lblsayfaadi.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "newpage_iletisimgirisi", "lblsayfaadiDuzenle");
                btnOlustur.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "newpage_iletisimgirisi", "btnDuzenle");
            }
            else
            {
                lblsayfaadi.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "newpage_iletisimgirisi", "lblsayfaadiOlustur");
                btnOlustur.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "newpage_iletisimgirisi", "btnOlustur");
            }


            lblFirmaB.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "newpage_iletisimgirisi", "lblFirmaB");
            lblIlgiliKisiB.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "newpage_iletisimgirisi", "lblIlgiliKisiB");
            lblMusTemB.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "newpage_iletisimgirisi", "lblMusTemB");
            lblKatilimciB.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "newpage_iletisimgirisi", "lblKatilimciB");
            lblIletisimDetB.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "newpage_iletisimgirisi", "lblIletisimDetB");
            btnDosyaBagla.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "newpage_iletisimgirisi", "btnDosyaBagla");
            btndosyagoryeni.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "newpage_iletisimgirisi", "btndosyagoryeni");




            lblKaydetMessage.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "iletisimgir", "msg189");
        }


        public string iletisimcode()
        {
            string striletisimcode = string.Empty;
            try
            {
                striletisimcode = Request.QueryString["EntryNo"].ToString();
            }
            catch (Exception)
            {
                string sorgu = "SELECT max([Entry No_]) From [0C_05065_00_INTERACTION_LOG_ENTRY] where COMPANY='" + _obj.Comp.ToString() + "'";
                striletisimcode = f.tekbirsonucdonder(sorgu);
                striletisimcode = (Convert.ToInt32(striletisimcode) + 1).ToString();
            }
            return striletisimcode;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _obj = TabSessions.ControlState(this.Page, Request.QueryString["GId"].ToString());
            _objStatic = TabSessions.ControlState(this.Page, Request.QueryString["GId"].ToString());
            btnOlustur.Attributes.Add("onClick", "displayLoadingImage()");
            if (Request.QueryString["Tur"].ToString() == "1")
            {
                if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == "move")
                {
                    int idx = lbkatilimci.SelectedIndex;
                    ListItem item = lbkatilimci.SelectedItem;
                    lbkatilimci.Items.Remove(item);
                }
                lbkatilimci.Attributes.Add("ondblclick", ClientScript.GetPostBackEventReference(lbkatilimci, "move"));


                if (Session["uyedurum"] == null)
                {
                    Response.Redirect("Default.aspx");
                }

                if (!IsPostBack)
                {
                    dilgetir();
                    optionselectedSirket = _obj.Comp.ToString() == "GAC" ? 0 : 1;

                    f.SatısTemsilcisiDoldur(ddlSatısTemsilcisi, _obj.Comp);
                    f.iletisimturudoldur(0, ddliletisimturu, _obj.Comp);
                    f.iletisimturudoldur(1, ddliletisimnedeni, _obj.Comp);
                    f.iletisimturudoldur(2, ddliletisimsonucu, _obj.Comp);
                    if (Request.QueryString["EntryNo"] != null)
                    {
                        iletisimdoldur(Request.QueryString["EntryNo"].ToString());
                        katilimcidoldur(Request.QueryString["EntryNo"].ToString(), lbkatilimci);
                    }
                    else
                    {
                        txtiletisimtarihi.Text = string.Format("{0:dd.MM.yyyy}", DateTime.Now);
                        btnDosyaBagla.Visible = false;
                        btndosyagoryeni.Visible = false;
                    }

                    if (Request.QueryString["CustomerNo"] != null)
                    {
                        lblcontactno.Text = f.tekbirsonucdonder("select [Contact No_] from [0C_05054_00_CONTACT_BUSINESS_RELATION] where COMPANY='" + _obj.Comp.ToString() + "' and No_ = '" + Request.QueryString["CustomerNo"].ToString() + "'");
                        txtCompany.Text = f.tekbirsonucdonder("select UPPER(Name) from [0C_05050_01_CONTACT]   where COMPANY='" + _obj.Comp.ToString() + "' and Type = 0 and No_='" + lblcontactno.Text + "'");
                        txtCompany_TextChanged(null, null);
                        txtCompany.Enabled = false;
                    }
                    else if (Request.QueryString["ContactNo"] != null)
                    {
                        lblcontactno.Text = Request.QueryString["ContactNo"].ToString();
                        txtCompany.Text = f.tekbirsonucdonder("select UPPER(Name) from [0C_05050_01_CONTACT] where COMPANY='" + _obj.Comp.ToString() + "' and Type = 0 and No_='" + lblcontactno.Text + "'");
                        txtCompany_TextChanged(null, null);
                        txtCompany.Enabled = false;
                    }
                    else if (Request.QueryString["KontakNo"] != null)
                    {
                        lblcontactno.Text = Request.QueryString["KontakNo"].ToString();
                        txtCompany.Text = f.tekbirsonucdonder("select  R0027    from [0_W20400]  where RCOMP='" + _obj.Comp.ToString() + "' and RFCNT='" + lblcontactno.Text + "'");
                        txtCompany_TextChanged(null, null);
                        txtCompany.Enabled = false;

                    }




                }

            }
            else
            {
                if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == "move")
                {
                    int idx = lbkatilimci.SelectedIndex;
                    ListItem item = lbkatilimci.SelectedItem;
                    lbkatilimci.Items.Remove(item);
                }
                lbkatilimci.Attributes.Add("ondblclick", ClientScript.GetPostBackEventReference(lbkatilimci, "move"));


                if (Session["uyedurum"] == null)
                {
                    Response.Redirect("Default.aspx");
                }

                if (!IsPostBack)
                {
                    f.SatısTemsilcisiDoldur(ddlSatısTemsilcisi, _obj.Comp);
                    f.iletisimturudoldur(0, ddliletisimturu, _obj.Comp);
                    f.iletisimturudoldur(1, ddliletisimnedeni, _obj.Comp);
                    f.iletisimturudoldur(2, ddliletisimsonucu, _obj.Comp);

                    btnOlustur.Text = "Oluştur";
                    lblsayfaadi.Text = "TELEFON İLETİŞİM GİR";
                    txtiletisimtarihi.Text = string.Format("{0:dd.MM.yyyy}", DateTime.Now);

                    txtBasSaati.Text = DateTime.Now.ToString("HH:mm");
                    txtBitSaati.Text = DateTime.Now.ToString("HH:mm");
                    ddlSatısTemsilcisi.SelectedValue = f.tekbirsonucdonder("select CASE WHEN  [Satıs Sorumlu Kod]='' THEN 'YOK' ELSE [Satıs Sorumlu Kod] end from [0C_50001_00_KULLANICILAR] where COMPANY='" + _obj.Comp.ToString() + "' and [Kullanıcı Adı] ='" + _obj.UserName.ToString() + "'");
                    ddliletisimturu.SelectedValue = ddliletisimturu.Items.FindByText("Telefon").Value;


                    btnkatilimciekle.Enabled = false;
                    lbkatilimci.Enabled = false;
                    ddlkatilimci.Enabled = false;
                    ddliletisimturu.Enabled = false;
                }


            }
        }




        public void katilimcidoldur(string iletisimno, ListBox lb)
        {
            SqlConnection conn = new SqlConnection(strConnString);
            SqlDataAdapter ad = new SqlDataAdapter("Select [Katılımcı No] as katilimcino,Description from [0C_05068_00_SALUTATION_KATILIMCI] where COMPANY='" + _obj.Comp.ToString() + "' and  Code= '" + iletisimno + "'", conn);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            lb.DataSource = ds;
            lb.DataTextField = "Description";
            lb.DataValueField = "katilimcino";
            lb.DataBind();
        }
        protected void txtCompany_TextChanged(object sender, EventArgs e)
        {
            ddlilgilikisi.Items.Clear();
            //lblcontactno.Text = f.tekbirsonucdonder("select No_ from [0C_05050_01_CONTACT] where COMPANY='" + _obj.Comp.ToString() + "' and  Type = 0 and [Company No_]='"+ Request.QueryString["ContactNo"].ToString() + "'");
            //txtFirmaTanitimi.Text = f.tekbirsonucdonder("select [Firma Tanıtımı] from Contact where  Type = 0 and UPPER(Name)=UPPER('" + txtCompany.Text + "')");
            //f.KisiContactDoldur(txtCompany.Text, lblcontactno);

            f.SirketKisiDoldur(lblcontactno.Text, ddlilgilikisi, _obj.Comp);
            f.SirketKisiveTemsilciDoldur(lblcontactno.Text, ddlkatilimci, _obj.Comp);
            f.iletisimContactTemsilciDoldur(lblcontactno.Text, ddlSatısTemsilcisi, _obj.Comp);

            if (txtCompany.Text == string.Empty)
            {
                lblcontactno.Text = string.Empty;
                ddlilgilikisi.Items.Clear();
            }

        }

        protected void Duzenleme()
        {
            string istek = txtDetay.Text;
            string istek1;
            string istek2;

            if (istek.Length > 249)
            {
                istek1 = istek.Substring(0, 250);
                istek2 = istek.Substring(250, istek.Length - 250);
            }
            else
            {
                istek1 = istek;
                istek2 = "";
            }
            string[] tcevir = txtiletisimtarihi.Text.Split('.');

            if (lblcontactno.Text != string.Empty & ddlilgilikisi.SelectedValue != string.Empty & txtiletisimtarihi.Text != string.Empty & ddliletisimnedeni.SelectedValue != string.Empty /* & txtyapilacaktarih.Text != string.Empty */ & txtBasSaati.Text != string.Empty & txtBitSaati.Text != string.Empty & ddlSatısTemsilcisi.SelectedValue != string.Empty & ddliletisimturu.SelectedValue != string.Empty & ddliletisimsonucu.SelectedValue != string.Empty & txtDetay.Text != string.Empty)
            {
                string[] _params = { Convert.ToInt32(Request.QueryString["EntryNo"]).ToString(), ddlilgilikisi.SelectedValue, lblcontactno.Text, tcevir[1] + tcevir[0] + tcevir[2], txtBasSaati.Text, txtBitSaati.Text, ddliletisimnedeni.SelectedValue, ddliletisimturu.SelectedValue, ddliletisimsonucu.SelectedValue, istek1, ddlSatısTemsilcisi.SelectedValue, _obj.NameSurName.ToString(), optionselectedSirket.ToString(), istek2, "" };
                GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "IletisimDuzenle§" + string.Join("§", _params), _obj.Comp);

            }
            else
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "iletisimgir", "msg188"));
            }




        }

        protected void Olusturma()
        {
            #region Normal İletişim Oluştur
            if (lblcontactno.Text != string.Empty & ddlilgilikisi.SelectedValue != string.Empty & txtiletisimtarihi.Text != string.Empty & ddliletisimnedeni.SelectedValue != string.Empty /* & txtyapilacaktarih.Text != string.Empty */ & txtBasSaati.Text != string.Empty & txtBitSaati.Text != string.Empty & ddlSatısTemsilcisi.SelectedValue != string.Empty & ddliletisimturu.SelectedValue != string.Empty & ddliletisimsonucu.SelectedValue != string.Empty & txtDetay.Text != string.Empty)
            {
                string[] tcevir = txtiletisimtarihi.Text.Split('.');

                string EntryNo = string.Empty;
                string istek = txtDetay.Text;
                string istek1;
                string istek2;

                if (istek.Length > 249)
                {
                    istek1 = istek.Substring(0, 250);
                    istek2 = istek.Substring(250, istek.Length - 250);
                }
                else
                {
                    istek1 = istek;
                    istek2 = "";
                }

                string[] _params = { ddlilgilikisi.SelectedValue, lblcontactno.Text, tcevir[1] + tcevir[0] + tcevir[2], txtBasSaati.Text, txtBitSaati.Text, ddliletisimnedeni.SelectedValue, ddliletisimturu.SelectedValue, ddliletisimsonucu.SelectedValue, istek1, ddlSatısTemsilcisi.SelectedValue, _obj.NameSurName.ToString(), optionselectedSirket.ToString(), istek2, "" };

                EntryNo = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "IletisimGir§" + string.Join("§", _params), _obj.Comp);

                if (lbkatilimci.Items.Count > 0)
                {

                    for (int i = 0; i < lbkatilimci.Items.Count; i++)
                    {
                        katilimciekle(EntryNo, lbkatilimci.Items[i].Value, lbkatilimci.Items[i].Text);

                    }

                }

                string[] _params2 = { };
                GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "EntryNoDoldur§" + string.Join("§", _params2), _obj.Comp);


                string alert = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "iletisimgir", "msg192");
                string url = "/iletisimgirisiV2.aspx?EntryNo=" + EntryNo + "&Tur=" + Request.QueryString["Tur"].ToString() + "&GId=" + Request.QueryString["GId"];

                if (Request.QueryString["ObjectId"] != null)
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + alert + "');proceed3('" + Request.QueryString["ObjectId"] + "');", true);
                else
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + alert + "');window.location.href='" + url + "';", true);





            }
            else
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "iletisimgir", "msg188"));

            }
            #endregion
        }

        protected void btnOlustur_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["EntryNo"] == "" || Request.QueryString["EntryNo"] == null)
            {
                Olusturma();
            }
            else
            {
                Duzenleme();
            }

        }
        private void katilimciekle(string iletisimno, string katilimcino, string katilimciadi)
        {

            string[] _params = { iletisimno, katilimcino, katilimciadi, lblcontactno.Text };
            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "KatilimciEkle§" + string.Join("§", _params), _obj.Comp);

        }
        private void iletisimdoldur(string entryno)
        {
            baglanti = new SqlConnection(strConnString);
            baglanti.Open();
            SqlCommand sorgu = new SqlCommand();

            sorgu.Connection = baglanti;
            sorgu.CommandType = CommandType.StoredProcedure;
            sorgu.CommandText = "sp_0WEB_ILETISIM_DOLDUR";
            SqlParameter pentryno = new SqlParameter("@entryno", SqlDbType.VarChar, 20);
            pentryno.Value = entryno;
            sorgu.Parameters.Add(pentryno);

            SqlParameter psirket = new SqlParameter("@COMP", SqlDbType.VarChar, 20);
            psirket.Value = _obj.Comp.ToString();
            sorgu.Parameters.Add(psirket);

            SqlDataAdapter sorguDA = new SqlDataAdapter(sorgu);
            DataTable sorgugridDT = new DataTable();
            sorguDA.Fill(sorgugridDT);
            SqlDataReader sqr;
            sqr = sorgu.ExecuteReader();
            if (sqr.Read())
            {
                lblcontactno.Text = sqr["sirketno"].ToString();
                txtCompany.Text = sqr["sirketadi"].ToString();
                txtCompany_TextChanged(txtCompany, EventArgs.Empty);
                ddlilgilikisi.SelectedValue = sqr["kisino"].ToString();
                txtiletisimtarihi.Text = sqr["etkilesimtarih"].ToString();


                ddliletisimnedeni.SelectedValue = sqr["iletisimnedeni"].ToString();
                ddliletisimturu.SelectedValue = sqr["iletisimturu"].ToString();
                ddliletisimsonucu.SelectedValue = sqr["iletisimsonucu"].ToString();
                txtBasSaati.Text = sqr["bassaat"].ToString();
                txtBitSaati.Text = sqr["bitsaat"].ToString();

                txtDetay.Text = sqr["iletisimdetay"].ToString() + sqr["iletisimdetay2"].ToString();
                if (sqr["satistemsilcisi"].ToString() != "")
                {
                    ddlSatısTemsilcisi.SelectedValue = sqr["satistemsilcisi"].ToString();
                }


            }
            sqr.Close();
        }
        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }
        protected void btnkatilimciekle_Click(object sender, EventArgs e)
        {
            ListItem li = new ListItem();
            li.Text = ddlkatilimci.SelectedItem.Text.ToString();
            li.Value = ddlkatilimci.SelectedValue;
            lbkatilimci.Items.Add(li);
            katilimcili = li;

        }

        protected void ddliletisimturu_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddliletisimturu.SelectedValue == "Ofisimizde Ziyaret" || ddliletisimturu.SelectedValue == "Yerinde Ziyaret"
                || ddliletisimturu.SelectedValue == "Dışarıda Görüşme")
                txtBitSaati.Enabled = true;
            else
            {
                txtBitSaati.Text = string.Empty;
                txtBitSaati.Enabled = false;
            }
        }






        private bool yenisorumluekle(string yapilacak, DateTime yapilacaktarih, string Sorumlu)
        {
            int a = 1;
            int b = 0;
            bool sonuc = false;
            string bosdeger = string.Empty;
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString);
            string icode = iletisimcode();

            if (icode != "" || icode != null)
            {
                SqlCommand cmd = new SqlCommand("INSERT INTO Salutation ([Company No],Silinen,[Katılımcı No],Code,Description,Tur,Yapılacak, [Yapılacak Tarih],Sorumlu) values (@CompanyNo,@sil, @katilimci, @Code, @Descr, @Tur,@Yapılacak, @YapılacakTarih,@Sorumlu) ", conn);
                cmd.Parameters.AddWithValue("@CompanyNo", lblcontactno.Text);
                cmd.Parameters.AddWithValue("@sil", b);
                cmd.Parameters.AddWithValue("@katilimci", bosdeger);
                cmd.Parameters.AddWithValue("@Code", icode);
                cmd.Parameters.AddWithValue("@Descr", bosdeger);
                cmd.Parameters.AddWithValue("@Tur", a);
                cmd.Parameters.AddWithValue("@Yapılacak", yapilacak);
                cmd.Parameters.AddWithValue("@YapılacakTarih", yapilacaktarih);
                cmd.Parameters.AddWithValue("@Sorumlu", Sorumlu);
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                try
                {

                    sonuc = Convert.ToBoolean(cmd.ExecuteNonQuery());
                }
                catch (SqlException ex)
                {


                }
                finally
                {
                    conn.Close();
                }
            }
            else
            {

                SqlCommand cmd = new SqlCommand("INSERT INTO Salutation ([Company No],Silinen,[Katılımcı No],Code,Description,Tur,Yapılacak, [Yapılacak Tarih],Sorumlu) values (@CompanyNo,@sil, @katilimci, @Code, @Descr, @Tur,@Yapılacak, @YapılacakTarih,@Sorumlu) ", conn);
                cmd.Parameters.AddWithValue("@CompanyNo", lblcontactno.Text);
                cmd.Parameters.AddWithValue("@sil", b);
                cmd.Parameters.AddWithValue("@katilimci", bosdeger);
                cmd.Parameters.AddWithValue("@Code", Convert.ToInt32(icode + 1).ToString());
                cmd.Parameters.AddWithValue("@Descr", bosdeger);
                cmd.Parameters.AddWithValue("@Tur", a);
                cmd.Parameters.AddWithValue("@Yapılacak", yapilacak);
                cmd.Parameters.AddWithValue("@YapılacakTarih", yapilacaktarih);
                cmd.Parameters.AddWithValue("@Sorumlu", Sorumlu);
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                try
                {

                    sonuc = Convert.ToBoolean(cmd.ExecuteNonQuery());
                }
                catch (SqlException ex)
                {


                }
                finally
                {
                    conn.Close();
                }
            }





            return sonuc;
        }
        protected void btnPotansiyelGir_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(lblcontactno.Text))
            {
                Response.Redirect("~/newpage_PotansiyelGirisi.aspx?iletisimNo=" + lblcontactno.Text + "&GId=" + Request.QueryString["GId"]);
            }
            else
            {

            }

        }

        protected void btnyeniiletisim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/iletisimgirisiV2.aspx?Tur=1&GId=" + Request.QueryString["GId"]);
        }

        protected void btnFirmaAra_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/newpage_Firmagor.aspx?GId=" + Request.QueryString["GId"]);
        }

        protected void btnDosyaBagla_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/newpage_documentEntry.aspx?DocType=5&DocNo=" + Request.QueryString["EntryNo"].ToString() + "&GId=" + Request.QueryString["GId"]);
            //ClientScript.RegisterStartupScript(this.Page.GetType(), "dsygor", "window.open('dosyabagla.aspx?BelgeNo=" + Request.QueryString["EntryNo"].ToString() + "&Tur=27&pageTur=1&dtur=27','List','scrollbars=no,resizable=no,width=650,height=460,top=200,left=400');", true);
        }

        protected void btnDosyaGor_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(this.Page.GetType(), "dsygor", "window.open('dosyagor.aspx?BelgeNo=" + Request.QueryString["EntryNo"].ToString() + "&Tur=27&dtur=27&GId=" + Request.QueryString["GId"] + "','List','scrollbars=no,resizable=no,width=650,height=460,top=200,left=400');", true);
        }
        protected void btndosyagoryeni_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/newpage_documentList.aspx?DocType=5&DocNo=" + Request.QueryString["EntryNo"].ToString() + "&GId=" + Request.QueryString["GId"]);
        }
    }
}