﻿using MSS1.Codes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using MSS1.WSGeneric;
using System.ServiceModel;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace MSS1
{
    public partial class JobFaturaOlustur : Bases
    {
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        SessionBase _obj;
        public ws_baglantı ws = new ws_baglantı();

        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }


        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }

            _obj = TabSessions.ControlState(this.Page, Request.QueryString["GId"].ToString());
            if (!IsPostBack)
            {
                fillcomboxes();

            }
        }

        void BindRepeater()
        {

            using (SqlConnection conn = new SqlConnection())
            {

                string[] Split = ddlMonthYear.SelectedValue.Split('/');
                string _date = Split[0].Trim() + "-" + Split[1].Trim() + "-" + "01";

                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_0WEB_LUCENT_JOB_INVOICE_LINES";
                    cmd.Parameters.AddWithValue("@year", Split[0].Trim());
                    cmd.Parameters.AddWithValue("@month", Split[1].Trim());
                    cmd.Parameters.AddWithValue("@sirket", _obj.Comp.ToString());
                    cmd.Parameters.AddWithValue("@firma", ddlsirket.SelectedValue);

                    cmd.Connection = conn;
                    conn.Open();
                    DataSet ds = new DataSet();
                    using (SqlDataAdapter adpt = new SqlDataAdapter(cmd))
                    {
                        adpt.Fill(ds);
                    }


                    rptFatura.DataSource = ds.Tables[0];
                    rptFatura.DataBind();

                    CheckBox chSec;
                    foreach (RepeaterItem rptItem in rptFatura.Items)
                    {
                        chSec = (CheckBox)rptItem.FindControl("chSecTypeSales");
                        if (chSec.Checked)
                        {
                            chSec.Checked = false;
                        }
                    }

                    //try
                    //{
                    if (ds.Tables[1].Rows[0]["AgencyFee"] != null && ds.Tables[3].Rows[0]["ProcessingFee"] != null)
                    {
                        if (!String.IsNullOrEmpty(ds.Tables[1].Rows[0]["AgencyFee"].ToString()))
                            txtAgency.Text = String.Format("{0:n2}", Convert.ToDouble(ds.Tables[1].Rows[0]["AgencyFee"]));
                        else
                            txtAgency.Text = String.Format("{0:n2}", 0);
                        if (!String.IsNullOrEmpty(ds.Tables[3].Rows[0]["ProcessingFee"].ToString()))
                            txtProcessing.Text = String.Format("{0:n2}", Convert.ToDouble(ds.Tables[3].Rows[0]["ProcessingFee"]));
                        else
                            txtProcessing.Text = String.Format("{0:n2}", 0);
                        if (!String.IsNullOrEmpty(ds.Tables[4].Rows[0]["Recruit Fee"].ToString()))
                            txtRecruit.Text = String.Format("{0:n2}", Convert.ToDouble(ds.Tables[4].Rows[0]["Recruit Fee"]));
                        else
                            txtRecruit.Text = String.Format("{0:n2}", 0);
                    }
                    else
                    {
                        txtAgency.Text = String.Format("{0:n2}", 0);
                        txtProcessing.Text = String.Format("{0:n2}", 0);
                        txtRecruit.Text = String.Format("{0:n2}", 0);
                    }

                    double x = 0, y = 0;

                    if (ds.Tables[2].Rows[0]["CrewCharges"] != null)
                    {
                        if (!String.IsNullOrEmpty(ds.Tables[2].Rows[0]["CrewCharges"].ToString()))
                            x = Convert.ToDouble(ds.Tables[2].Rows[0]["CrewCharges"]);
                    }

                    y = x + Convert.ToDouble(txtAgency.Text) + Convert.ToDouble(txtProcessing.Text) + Convert.ToDouble(txtRecruit.Text);

                    txtCrewcharges.Text = String.Format("{0:n2}", x);
                    txtTotal.Text = String.Format("{0:n2}", y);
                    //}
                    //catch { }

                    conn.Close();
                }
            }
        }

        void LoadForm()
        {
            if (ddlsirket.SelectedValue == "-1")
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "jobfaturaolustur", "msg001"));
                return;
            }
          

            string[] Split = ddlMonthYear.SelectedValue.Split('/');
            string _date = Split[0].Trim() + "-" + Split[1].Trim() + "-" + "01";



            string[] _params = { ddlsirket.SelectedValue, _date };
            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50007", "FaturaAraTabloDoldurSP§" + string.Join("§", _params), _obj.Comp);


            BindRepeater();
        }

        void fillcomboxes()
        {
            List<string> months = new List<string>();

            for (int i = DateTime.Now.Year; i >= 2000; i--)
            {
                for (int j = 1; j <= 12; j++)
                {
                    months.Add(i.ToString() + " / " + j.ToString().PadLeft(2, '0'));
                }
            }

            ddlMonthYear.Items.Clear();
            ddlMonthYear.DataSource = months;
            ddlMonthYear.DataBind();

            ddlMonthYear.SelectedValue = DateTime.Now.Year.ToString() + " / " + DateTime.Now.Month.ToString().PadLeft(2, '0');

            using (SqlConnection baglanti = new SqlConnection(strConnString))
            {
                baglanti.Open();
                using (SqlCommand sorgu = new SqlCommand())
                {
                    sorgu.Connection = baglanti;

                    sorgu.CommandText = "select No_,Name from [0C_00018_01_CUSTOMER] where COMPANY='" + _obj.Comp.ToString() + "' Order By Name";

                    using (SqlDataAdapter sorguDA = new SqlDataAdapter(sorgu))
                    {
                        using (DataTable sorgugridDT = new DataTable())
                        {
                            sorguDA.Fill(sorgugridDT);

                            ddlsirket.DataTextField = "Name";
                            ddlsirket.DataValueField = "No_";
                            ddlsirket.DataSource = sorgugridDT;
                            ddlsirket.DataBind();
                            ddlsirket.SelectedValue = "-1";
                        }
                    }

                }
            }


        }

        protected void btnOlustur_Click(object sender, EventArgs e)
        {
            
            string[] Split = ddlMonthYear.SelectedValue.Split('/');
            string _date = Split[0].Trim() + "-" + Split[1].Trim() + "-" + "01";

            string[] _params = { ddlsirket.SelectedValue, _date };
            string _return = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50007", "LucentFaturaOlustur§" + string.Join("§", _params), _obj.Comp);


            if (!string.IsNullOrEmpty(_return))
                Response.Redirect("OnePageList15.aspx?OJ1=P50050;J50050&OJ2=P50050;J50051&OJ3=P50050;OJ9999&OJ4=P50050;OJ9999&OJ5=P50050;OJ9999&OJ6=P50050;OJ9999&P1=" + _return+ "&GId=" + Request.QueryString["GId"]);
            else
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "jobfaturaolustur", "msg002"));



        }
        protected void btnGetInvoiceLines_Click(object sender, EventArgs e)
        {
            LoadForm();
        }
        protected void btnCalcLines_Click(object sender, EventArgs e)
        {
           

            CheckBox chSec;
            int checkBoxCount = 0;
            Literal ltrEntryNo;
            TextBox txtFaturaUsd;
            decimal x = 0;
            foreach (RepeaterItem rptItem in rptFatura.Items)
            {
                chSec = (CheckBox)rptItem.FindControl("chSecTypeSales");
                if (chSec.Checked)
                {
                    checkBoxCount++;
                    ltrEntryNo = (Literal)rptItem.FindControl("ltrentryno");
                    txtFaturaUsd = (TextBox)rptItem.FindControl("txtFaturaUsd");

                    x = Convert.ToDecimal(txtFaturaUsd.Text.Replace(",", "."));




                    string[] _params = {ltrEntryNo.Text, x.ToString() };
                    GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50007", "FaturaAraTabloGuncelle§" + string.Join("§", _params), _obj.Comp);



                }
            }
            if (checkBoxCount == 0)
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "jobfaturaolustur", "msg003"));
                return;
            }

            BindRepeater();
        }
        protected void btnGetLastWorkedList_Click(object sender, EventArgs e)
        {
            BindRepeater();
        }
    }
}