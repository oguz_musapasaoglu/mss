﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TayfaCard.aspx.cs" Inherits="MSS1.Tayfa.TayfaCard" %>

<%@ Register Src="~/controls/DataTableControlGAC.ascx" TagName="StDataTable" TagPrefix="STDT" %>

<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../css/GAC_MenuStyle.css" rel="stylesheet" />
    <link href="../css/GAC_NewStyle.css?v=V1" rel="stylesheet" />
    <link href="../css/GAC_CardStyle.css?v=V1" rel="stylesheet" />
    <style>
        .dxflGroup {
            padding: 0px 5px;
            width: 100%;
        }

            .dxflGroup tr:first-child > .dxflGroupCell > .dxflGroupBox, .dxflGroup > .dxflChildInFirstRowSys > .dxflGroupCell > .dxflGroupBox {
                margin-top: -5px;
            }

        .dx-vam {
            color: white;
        }

        .myImage {
            max-height: 225px;
            max-width: 200px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%">
            <table style="width: 100%; border-width: 0px;">
                <tr>
                    <td style="width: 40%; vertical-align: top">
                        <dx:ASPxFormLayout ID="Box1" runat="server" Width="100%">
                            <Items>
                                <dx:LayoutGroup ShowCaption="False">
                                    <Items>
                                        <dx:LayoutItem FieldName="Name" Caption="NAME" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtName" runat="server" Width="100%" Height="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="SurName" Caption="SURNAME" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtSurName" runat="server" Width="100%" Height="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="NATIONALITY" Caption="NATIONALITY / SEX" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <table border="0" style="width: 100%; height: 100%" cellspacing="2">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxComboBox ID="cmbNationality" runat="server" Width="100%" CssClass="carditemStyle"
                                                                    ValueField="Code" TextField="Text" DataSourceID="dtUlke">
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxComboBox ID="cmbSex" runat="server" Width="100%" ValueType="System.Int32" CssClass="carditemStyle">
                                                                    <Items>
                                                                        <dx:ListEditItem Selected="true" Text="" Value="0" />
                                                                        <dx:ListEditItem Text="MALE" Value="1" />
                                                                        <dx:ListEditItem Text="FEMALE" Value="2" />
                                                                    </Items>
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="INDENITYNO" Caption="IDENTITY NO" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtIdentityNo" runat="server" Width="100%" Height="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="BIRTHDATE" Caption="DATE - BIRTH / PLACE" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <table border="0" style="width: 100%; height: 100%" cellspacing="2">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxDateEdit runat="server" ID="txtBirthDate" Width="100%" DisplayFormatString="dd.MM.yyyy" EditFormatString="dd.MM.yyyy"></dx:ASPxDateEdit>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox ID="txtPlaceOfBirth" runat="server" Width="100%"
                                                                    CssClass="carditemStyle">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="PHONENO" Caption="PHONE NO" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtPhoneNo" runat="server" Width="100%" Height="10%" CssClass="carditemStyle"></dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="EMAIL" Caption="EMAIL" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtEmail" runat="server" Width="100%" Height="10%" CssClass="carditemStyle"></dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="ADRESS" Height="50px" Caption="ADRESS" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="0">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtAdress" runat="server" Width="100%" Height="100%"
                                                        TextMode="MultiLine" CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="CITY" Caption="CITY" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="0">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtCity" runat="server" Width="100%" Height="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="COUNTRY" Caption="COUNTRY" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxComboBox ID="cmbUlke" runat="server" Width="100%" CssClass="carditemStyle"
                                                        ValueField="Code" TextField="Text" DataSourceID="dtUlke">
                                                    </dx:ASPxComboBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                    </Items>
                                    <GroupBoxStyle CssClass="cardBox"></GroupBoxStyle>
                                    <ParentContainerStyle Paddings-Padding="0"></ParentContainerStyle>
                                </dx:LayoutGroup>
                            </Items>
                        </dx:ASPxFormLayout>
                    </td>
                    <td style="width: 30%; vertical-align: top">
                        <dx:ASPxFormLayout ID="Box2" runat="server" Width="100%">
                            <Items>
                                <dx:LayoutGroup ShowCaption="False">
                                    <Items>
                                        <dx:LayoutItem FieldName="STATUS" Border-BorderColor="DarkBlue" Caption="STATUS" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxComboBox ID="cmbstatus" runat="server" Width="50%"
                                                        ValueField="Code" TextField="Text" DataSourceID="dtstatus"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxComboBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="CURRENTSTATUS" Border-BorderColor="DarkBlue" Caption="CURRENT STATUS" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtCurrentStatus" ReadOnly="true" runat="server" Width="100%" Height="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="READINESSDATE" Caption="READINESS DATE" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxDateEdit runat="server" ID="txtReadinessDate" DisplayFormatString="dd.MM.yyyy" EditFormatString="dd.MM.yyyy"></dx:ASPxDateEdit>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="COC" Caption="C.O.C" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxComboBox ID="cmbCoc" runat="server" Width="50%"
                                                        ValueField="Code" TextField="Text" DataSourceID="dtLucentRank"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxComboBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="LUCENTRANK" Caption="RANK / TIME IN RANK" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <table border="0" style="width: 100%; height: 100%" cellspacing="2">
                                                        <tr>
                                                            <td style="width: 65%">
                                                                <dx:ASPxComboBox ID="cmbLucentRank" runat="server" Width="100%"
                                                                    ValueField="Code" TextField="Text" DataSourceID="dtLucentRank"
                                                                    CssClass="carditemStyle">
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                            <td style="width: 35%">
                                                                <dx:ASPxTextBox ID="txtTimeInRank" runat="server" Width="100%"
                                                                    CssClass="carditemStyle">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="PASSPORTNO" Caption="PASSPORT NO" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtPassportNo" ReadOnly="true" runat="server" Width="100%" Height="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="PASISSUEDATE" Caption="PAS. ISSUE DATE" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxDateEdit runat="server" ID="txtPassIssueDate" ReadOnly="true" DisplayFormatString="dd.MM.yyyy" EditFormatString="dd.MM.yyyy"></dx:ASPxDateEdit>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="PassEXDATE" Caption="PAS. EXPIRY DATE" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxDateEdit runat="server" ID="txtPassExDate" ReadOnly="true" DisplayFormatString="dd.MM.yyyy" EditFormatString="dd.MM.yyyy"></dx:ASPxDateEdit>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="SEAMANSBOOKNO" Caption="SEAMANS BOOK NO" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxTextBox ID="txtSeamanBookNo" ReadOnly="true" runat="server" Width="100%" Height="100%"
                                                        CssClass="carditemStyle">
                                                    </dx:ASPxTextBox>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="SEAMANSBOOKISSUEDATE" Caption="S.B. ISSUE DATE" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxDateEdit runat="server" ID="txtSeamanBookIssueDate" ReadOnly="true" DisplayFormatString="dd.MM.yyyy"
                                                        EditFormatString="dd.MM.yyyy">
                                                    </dx:ASPxDateEdit>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                        <dx:LayoutItem FieldName="SEAMANSBOOKEXPIRYDATE" Caption="S.B. EXPIRY DATE" Border-BorderColor="DarkBlue" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                            <LayoutItemNestedControlCollection>
                                                <dx:LayoutItemNestedControlContainer>
                                                    <dx:ASPxDateEdit runat="server" ID="txtSeamanBookExDate" ReadOnly="true" DisplayFormatString="dd.MM.yyyy" EditFormatString="dd.MM.yyyy"></dx:ASPxDateEdit>
                                                </dx:LayoutItemNestedControlContainer>
                                            </LayoutItemNestedControlCollection>
                                        </dx:LayoutItem>
                                    </Items>
                                    <GroupBoxStyle CssClass="cardBox"></GroupBoxStyle>
                                    <ParentContainerStyle Paddings-Padding="0"></ParentContainerStyle>
                                </dx:LayoutGroup>
                            </Items>
                        </dx:ASPxFormLayout>
                    </td>
                    <td style="width: 30%; vertical-align: top">
                        <table style="width: 100%; vertical-align: top">
                            <tr>
                                <td>
                                    <dx:ASPxFormLayout ID="Box3" Visible="true" runat="server" Width="100%">
                                        <Items>
                                            <dx:LayoutGroup ShowCaption="true" Caption="PHOTO" Name="firmaTanitimi" Height="261px">
                                                <Items>
                                                    <dx:LayoutItem FieldName="Photo" ShowCaption="false" CaptionSettings-Location="Top">
                                                        <LayoutItemNestedControlCollection>
                                                            <dx:LayoutItemNestedControlContainer>
                                                                <table style="width: 100%; table-layout: fixed">
                                                                    <tr>
                                                                        <td style="text-align: center">
                                                                            <dx:ASPxImage ID="ImgPhoto" CssClass="myImage" runat="server">
                                                                            </dx:ASPxImage>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </dx:LayoutItemNestedControlContainer>
                                                        </LayoutItemNestedControlCollection>
                                                    </dx:LayoutItem>
                                                </Items>
                                                <GroupBoxStyle CssClass="cardBox"></GroupBoxStyle>
                                                <ParentContainerStyle Paddings-Padding="0"></ParentContainerStyle>
                                            </dx:LayoutGroup>
                                        </Items>
                                    </dx:ASPxFormLayout>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="width: 100%; padding-left: 5px">
                                        <tr>
                                            <td>
                                                <dx:ASPxButton ID="btnKaydet" Text="SAVE / EDIT" runat="server" CssClass="formglobalbuttonStyleCRM" OnClick="btnKaydet_Click"></dx:ASPxButton>
                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btnSil" Text="DELETE" runat="server" CssClass="formglobalbuttonStyleCRM" OnClick="btnSil_Click"></dx:ASPxButton>
                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btnImageUpload" Text="UPDATE PICTURE" runat="server" CssClass="formglobalbuttonStyleCRM" OnClick="btnImageUpload_Click"></dx:ASPxButton>
                                            </td>
                                        </tr>

                                    </table>



                                </td>
                            </tr>
                        </table>


                    </td>
                </tr>

            </table>
        </div>
        <STDT:StDataTable ID="dtstatus" runat="server" />
        <STDT:StDataTable ID="dtLucentRank" runat="server" />
        <STDT:StDataTable ID="dtUlke" runat="server" />
    </form>
</body>
</html>
