﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="iadesatinalmayazdırma.aspx.cs" Inherits="MSS1.iadesatinalmayazdırma" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function proceed() {
            opener.location.reload(true);
            opener.location = 'iadesatinalma.aspx';
            self.close();
        }
        function nakletsoru() {
            if (confirm('Deftere Naklet/Yazdırmak istiyor musunuz ?')) {
                return true;
            }
            else {
                alert("Nakil İşlemi İptal Edildi !");
                return false;
            }

        }
        function earsivsoru() {
            if (confirm('E-Arşive göndermek istiyor musunuz ?')) {
                return true;
            }
            else {
                alert("E-Arşiv İşlemi İptal Edildi !");
                return false;
            }

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table cellspacing="10">
            <tr>
                <td class="style2">
                    <asp:Label ID="lblFaturaTuruB" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlft1" CssClass="textbox" runat="server" Width="155px" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlft1_SelectedIndexChanged">
                        <asp:ListItem Selected="True"></asp:ListItem>
                        <asp:ListItem Value="1">1 - DETAYLI</asp:ListItem>
                        <asp:ListItem Value="2">2 - TOPLU</asp:ListItem>
                        <asp:ListItem Value="3">3 - GRUPLU</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;<asp:Label ID="lblTopFtMetni1B" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtftmetni1" CssClass="textbox" runat="server" Enabled="False" MaxLength="80" OnTextChanged="txtftmetni1_TextChanged"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="lblTopFtMetni2B" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtftmetni2" CssClass="textbox" runat="server" Enabled="False" MaxLength="60"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="lblTopFtMetni3B" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtftmetni3" CssClass="textbox" runat="server" Enabled="False" MaxLength="60"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;<asp:Label ID="lblFtEkMetinB" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtftekmetin" CssClass="textbox" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;<asp:Label ID="lblFtTarihiB" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtfaturatarihi" CssClass="textbox" runat="server" Style="text-align: justify"
                        ValidationGroup="MKE" 
                        ontextchanged="txtfaturatarihi_TextChanged" AutoPostBack=true />
                    <cc1:CalendarExtender ID="txtfaturatarihi_CalendarExtender" runat="server" TargetControlID="txtfaturatarihi"
                        Format="dd.MM.yyyy" />
                    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptLocalization="true"
                        EnableScriptGlobalization="true" EnablePageMethods="true">
                    </asp:ScriptManager>
                </td>
            </tr>
        </table>
        <br />
        <table cellspacing="10">
            <tr>
                <td class="style3">
                    <asp:Label ID="lblSeriNoB" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtSeriNo" CssClass="textbox" runat="server" MaxLength="10"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Label ID="lblHariciBNoB" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txthbno" CssClass="textbox" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Label ID="lblKonsNoB" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtawb" CssClass="textbox" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style3">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style3">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        <div id="altmenuyazdir">
            <asp:Button ID="btnönizleme" runat="server" CssClass="myButton" ViewStateMode="Enabled"
                OnClick="btnönizleme_Click" />
            <asp:Button ID="btnyazdır" runat="server" CssClass="myButton"
                OnClick="btnyazdır_Click" ViewStateMode="Enabled" OnClientClick="javascript: return nakletsoru()" />
               <asp:Button ID="btnearsiv" runat="server" CssClass="myButton" Visible="false"
                    OnClick="btnearsiv_Click" Text="E-Arşive Gönder" ViewStateMode="Enabled"
                    OnClientClick="javascript: return earsivsoru()" />
              <asp:Button ID="btnearsivonizleme" runat="server" CssClass="myButton" Visible="false" ViewStateMode="Enabled"
                    OnClick="btnearsivonizleme_Click" />
            <asp:Button ID="btnEFaturaYazdir" runat="server" CssClass="myButton"
                    OnClick="btnEFaturaYazdir_Click"  ViewStateMode="Enabled"  />
        
        </div>
    </div>
    </form>
</body>
</html>
