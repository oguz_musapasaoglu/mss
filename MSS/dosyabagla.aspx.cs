﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.ServiceModel;
using System.Configuration;
using System.Data.SqlClient;
using MSS1.WSGeneric;
using System.Text;
using System.Threading;
using DevExpress.Pdf;
using DevExpress.XtraPrinting.Export.Pdf;
using System.Drawing;
using MSS1.Codes;

namespace MSS1
{
    public partial class dosyabagla : Bases
    {
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        SqlConnection baglanti;
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;

        public ws_baglantı ws = new ws_baglantı();

        SessionBase _obj;

        #region Dil Getir
        public void dilgetir()
        {
            lblDosyaSec.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "dosyabagla", "lblDosyaSec");
            lblDosyaNo.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "dosyabagla", "lblDosyaNo");
            lblDosyaAdi.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "dosyabagla", "lblDosyaAdi");
            lblYorum.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "dosyabagla", "lblYorum");
            btndosyayukle.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "dosyabagla", "btndosyayukle");
        }
        #endregion


        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            _obj = TabSessions.ControlState(this.Page, Request.QueryString["GId"].ToString());
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            dilgetir();
            if (!IsPostBack)
            {
                if (Request.QueryString["NAVTypeAfter"] != null)
                {
                    string _Type = string.Empty;
                    if (Request.QueryString["NAVTypeAfter"].ToLower().Contains(";c") | Request.QueryString["NAVTypeAfter"].ToLower().Contains(";v"))
                    {
                        trcust.Visible = true;
                        if (Request.QueryString["NAVTypeAfter"].ToLower().Contains(";c"))
                            _Type = "CUSTOMER";
                        else
                            _Type = "VENDOR";
                        string sorgu = "SELECT No_,Name FROM [0_W50130] where TYPE='" + _Type + "' and COMPANY='" + _obj.Comp + "'";
                        DSDyn.SelectCommand = sorgu;
                    }
                }

                    if (Request.QueryString["tayfaId"] != null)
                    lblTayfaID.Text = Request.QueryString["tayfaId"];
                lblno.Text = Request.QueryString["BelgeNo"];
                if (Request.QueryString["NavType"] != null)
                {

                    string[] _params = { _obj.UserName.ToString(), lblno.Text, Convert.ToInt32(Request.QueryString["NavType"]).ToString(), Convert.ToInt32(Request.QueryString["tur"]).ToString(), Convert.ToInt32(Request.QueryString["dtur"]).ToString() };
                    lblno.Text = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "DosyaBelgeNoAl§" + string.Join("§", _params), _obj.Comp);
                    //MessageBox(_obj.UserName.ToString() + "," + lblno.Text + "," + Request.QueryString["NavType"] + "," + Request.QueryString["tur"] + "," + Request.QueryString["dtur"] + "," + lblno.Text);
                }
                if (Request.QueryString["tur"].ToString() == "28")
                {
                    txtyorum.Visible = false;
                    lblYorum.Visible = false;
                }
            }
            btndosyayukle.Attributes.Add("onClick", "displayLoadingImage()");
        }
        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }
        protected void btndosyayukle_Click(object sender, EventArgs e)
        {
            if (txtdosyaadi.Text != string.Empty)
            {

                int tur = Convert.ToInt32(Request.QueryString["Tur"]);
                string extn = "", save_path = "", dosyaupload = "", inputfilename = "", outputfilename = "", sirket = _obj.Comp.ToString();

                if (FileUpload1.HasFile)
                {
                    extn = System.IO.Path.GetExtension(FileUpload1.FileName);
                    if (extn.ToLower() == ".bmp" || extn.ToLower() == ".jpg" || extn.ToLower() == ".jpeg" || extn.ToLower() == ".png" || extn.ToLower() == ".gif" ||
                        extn.ToLower() == ".docx" || extn.ToLower() == ".doc" || extn.ToLower() == ".xlsx" || extn.ToLower() == ".xls" || extn.ToLower() == ".txt" ||
                        extn.ToLower() == ".pdf" || extn.ToLower() == ".xml" || extn.ToLower()==".csv")
                    {


                        lblLineNo.Text = GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "DosyaSatirNoAl§" + tur.ToString() + "§" + lblno.Text, _obj.Comp);

                        lblFileName.Text = lblno.Text + lblLineNo.Text;

                        switch (tur)
                        {

                            case 34:
                                string sfilename = txtdosyaadi.Text + string.Format("{0:ddMMyyyyHHmmss}", DateTime.Now) + extn;
                                dosyaupload = Server.MapPath(CurrentFolderName(_obj.Comp) + "\\" + Session["Upd" + Request.QueryString["FId"] + "FilePath"].ToString().Replace(@"\", "\\")) + sfilename;
                                Session["Upd" + Request.QueryString["FId"] + "FileName"] = sfilename;
                                Session["Upd" + Request.QueryString["FId"] + "FilePath2"] = CurrentFolderName(_obj.Comp) + "\\" + Session["Upd" + Request.QueryString["FId"] + "FilePath"].ToString().Replace(@"\", "\\");
                                FileUpload1.PostedFile.SaveAs(dosyaupload.Trim());
                                break;
                            default:
                                using (DataTable dt = new DataTable())
                                {
                                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
                                    {
                                        using (SqlCommand cmd = new SqlCommand())
                                        {
                                            cmd.CommandText = "[sp_0WEB_DOSYA_BAGLA]";
                                            cmd.Connection = conn;
                                            cmd.CommandTimeout = 500;
                                            cmd.CommandType = CommandType.StoredProcedure;
                                            cmd.Parameters.Clear();
                                            cmd.Parameters.AddWithValue("@COMP", _obj.Comp.ToString());
                                            cmd.Parameters.AddWithValue("@PRM1", tur.ToString());

                                            using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                                            {
                                                adp.Fill(dt);
                                            }
                                            if (dt.Rows.Count > 0)
                                            {
                                                save_path = dt.Rows[0]["PATH"].ToString() + lblFileName.Text + extn;
                                                dosyaupload = Server.MapPath(dt.Rows[0]["PATH"].ToString()) + lblFileName.Text + extn;
                                                if (dt.Rows[0]["DOSYATABLO"].ToString() == "1")
                                                {

                                                    lblLineNo.Text = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "DosyaOlustur§" + tur.ToString() + "§" + lblno.Text + "§" + lblno.Text + "§"
                                                        + save_path + "§" + txtyorum.Text + "§" + extn + "§" + txtdosyaadi.Text + "§" + lblLineNo.Text + "§" + _obj.NameSurName.ToString(), _obj.Comp);

                                                }
                                                FileUpload1.PostedFile.SaveAs(dosyaupload.Trim());
                                                if (tur == 28)
                                                {
                                                    if (extn.ToLower() == ".pdf")
                                                    {
                                                        inputfilename = dosyaupload.Trim();
                                                        outputfilename = Server.MapPath(CurrentFolderName(_obj.Comp) + "\\Dosya\\SatinAlma_Onay\\images\\") + lblFileName.Text + ".png";
                                                        CreateThumbnailsFromPDF(inputfilename, outputfilename);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                                break;


                        }

                    }

                }

                if (Request.QueryString["NAVTypeAfter"] != null)
                {
                    string _result = string.Empty;

                    string[] _params = { _obj.UserName.ToString(), Request.QueryString["NAVTypeAfter"].ToString(), dosyaupload.Trim()
                        , FileUpload1.FileName, lblno.Text, Convert.ToInt32(Request.QueryString["tur"]).ToString()
                        , Convert.ToInt32(Request.QueryString["dtur"]).ToString(), txtdosyaadi.Text, txtyorum.Text, cmbCustomer.SelectedItem != null ? cmbCustomer.SelectedItem.Value.ToString() : "" };
                    
                    _result = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "AfterWebDocImport§" + string.Join("§", _params), _obj.Comp);

                    ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: proceed4('" + Request.QueryString["ObjectId"] + "','" + _result + "');", true);

                }
                else
                {

                    if (Request.QueryString["ProcType"] != null)
                    {
                        if (Request.QueryString["ProcType"].ToString() == "INSERTFILE")
                            ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: proceed3('" + Request.QueryString["FId"] + "'," + Request.QueryString["ObjectId"] + ");", true);

                    }
                    else
                    {
                        if (Request.QueryString["ObjeId"] != null)
                            ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: proceed2('" + Request.QueryString["BelgeNo"] + "','" + Request.QueryString["tur"] + "','" + Request.QueryString["dtur"] + "','" + Request.QueryString["FaturaOnGiris"] + "'," + Request.QueryString["ObjeId"] + ");", true);
                        else
                            ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: proceed('" + Request.QueryString["BelgeNo"] + "','" + Request.QueryString["tur"] + "','" + Request.QueryString["dtur"] + "','" + Request.QueryString["FaturaOnGiris"] + "');", true);
                    }
                }
                //onsubmit="proceed()"
            }
            else
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "dosyabagla", "msg126"));
            }

            txtdosyaadi.Text = string.Empty;
            txtyorum.Text = string.Empty;
        }

        void CreateThumbnailsFromPDF(string inputfilename, string outputfilename)
        {
            using (PdfDocumentProcessor processor = new PdfDocumentProcessor())
            {
                processor.LoadDocument(inputfilename);
                
                int _count = 1;
                string FileNWithoutExt = outputfilename.Replace("10000.png", string.Empty);
                foreach (DevExpress.Pdf.PdfPage _page in processor.Document.Pages)
                {
                    if (_count == 1)
                    {
                        using (Bitmap pageBitmap = processor.CreateBitmap(_count, 1800))
                        {
                            try { if (File.Exists(outputfilename)) File.Delete(outputfilename); } catch { }
                            pageBitmap.Save(outputfilename);
                        }
                    }
                    else
                    {
                        using (Bitmap pageBitmap = processor.CreateBitmap(_count, 1800))
                        {
                            try { if (File.Exists(FileNWithoutExt + (_count * 10000).ToString() + ".png")) File.Delete(FileNWithoutExt + (_count * 10000).ToString() + ".png"); } catch { }
                            pageBitmap.Save(FileNWithoutExt + (_count * 10000).ToString() + ".png");
                        }
                    }
                    _count++;
                }


                string[] _params = { Request.QueryString["BelgeNo"], (_count - 1).ToString() };
                GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "DosyaSayisiUpdate§" + string.Join("§", _params), _obj.Comp);


            }

        }
    }
}