﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"  CodeBehind="JobFaturaOlustur.aspx.cs" Inherits="MSS1.JobFaturaOlustur"
    EnableEventValidation="false"
    MaintainScrollPositionOnPostback="true"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .burakalan {
            width: 240px;
        }

        .burakalan2 {
            width: 200px;
        }

        .tablestyle table tr td {
            padding: 2px;
            font-size: 10px;
            border: 1px solid navy;
        }

        .table > thead > tr > td {
            padding: 0px;
            border: 1px solid navy;
        }

        .table > thead > tr > th {
            font-weight: 600;
            color: navy;
            background-color: #DCE6F1;
            padding: 0px;
            font-size: 10px;
            vertical-align: top;
            border: 1px solid navy;
            text-align: center;
        }

        .table-striped > tbody > tr:nth-of-type(odd) {
            background-color: #f9f9f9;
        }

        .rightAlign {
            text-align: right;
        }

        #overlay {
            position: fixed;
            z-index: 3;
            top: 0px;
            left: 0px;
            background-color: #f8f8f8;
            width: 100%;
            height: 100%;
            filter: Alpha(Opacity=90);
            opacity: 0.9;
            -moz-opacity: 0.9;
            visibility: hidden;
        }

        #theprogress {
            background-color: #fff;
            border: 1px solid #ccc;
            padding: 10px;
            width: 300px;
            height: 70px;
            line-height: 30px;
            text-align: center;
            filter: Alpha(Opacity=100);
            opacity: 1;
            -moz-opacity: 1;
        }

        #modalprogress {
            position: absolute;
            top: 40%;
            left: 50%;
            margin: -11px 0 0 -150px;
            color: #990000;
            font-weight: bold;
            font-size: 14px;
        }
    </style>
    <script type="text/javascript" language="JavaScript">

        function displayLoadingImage() {
            var imgLoading = document.getElementById("imgLoading");
            var overlay = document.getElementById("overlay");
            var lblyukleniyor = document.getElementById("ContentPlaceHolder1_lblyukleniyor");
            overlay.style.visibility = "visible";
            imgLoading.style.visibility = "visible";
            lblyukleniyor.style.visibility = "visible";
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table cellpadding="3" cellspacing="3" style="width: 850px">
        <tr>
            <td colspan="4">
                <h2>
                    <asp:Label ID="lblBaslik" Text="LUCENT FATURA OLUŞTURMA" runat="server"></asp:Label></h2>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="left" valign="top">
                <table cellspacing="3" style="width: 315px">
                    <tr>
                        <td width="150">
                            <asp:Label ID="lblMounthYear" runat="server">YIL / AY</asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlMonthYear" CssClass="textbox" Width="163px" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td width="150">
                            <asp:Label ID="lblCustomer" Text="MÜŞTERİ" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlsirket" AppendDataBoundItems="true" CssClass="textbox" Width="165px" runat="server">
                                <asp:ListItem Selected="True" Text="" Value="-1" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td width="300px">&nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td width="300px"></td>
                        <td>
                            <table style="width: 300px">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnGetInvoiceLines" runat="server" Text="SATIRLARI GETİR" CssClass="myButton" OnClick="btnGetInvoiceLines_Click" /></td>
                                    <td>
                                        <asp:Button ID="btnGetLastWorkedList" runat="server" Text="KAYDEDİLENLERİ GETİR" CssClass="myButton" OnClick="btnGetLastWorkedList_Click" /></td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
            </td>
            <td align="right" valign="top">
                <table cellspacing="3" style="width: 350px">
                    <tr>
                        <td width="150">
                            <asp:Label ID="lblProcessing0" Text="RECRUIT" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtRecruit" ReadOnly="true" runat="server" CssClass="Dectextbox" Width="149px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="150">
                            <asp:Label ID="lblProcessing" Text="PROCESSING" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtProcessing" ReadOnly="true" runat="server" CssClass="Dectextbox" Width="149px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="150">
                            <asp:Label ID="lblAgency" Text="AGENCY" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtAgency" ReadOnly="true" runat="server" CssClass="Dectextbox" Width="149px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="150">
                            <asp:Label ID="lblCrewcharges" Text="CREW CHARGES" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCrewcharges" ReadOnly="true" runat="server" CssClass="Dectextbox" Width="149px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="150">
                            <asp:Label ID="lblTotal" Text="TOTAL" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtTotal" ReadOnly="true" runat="server" CssClass="Dectextbox" Width="149px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td align="right">&nbsp;</td>
            <td align="right">&nbsp;</td>
            <td align="right">
                <div id="overlay">
                    <div id="modalprogress">
                        <div id="theprogress">
                            <img src="images/ajax-loader.gif" name="imgLoading" id="imgLoading" style="visibility: hidden;">
                            <br />
                            <div id="yukleniyor" style="visibility: hidden;">
                                <asp:Label ID="lblyukleniyor" runat="server" Text="Yükleniyor..."></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>

            </td>
        </tr>
    </table>
    <hr />
    <div id="divMandatory" runat="server" class="tablestyle" style="margin-top: 20px; width: 1310px">
        <table class="table table-striped" style="width: 100%">
            <thead>
                <tr>
                    <th class="lblBaslikS" style="border: 1px solid navy; width: 80px">İŞ NO</th>
                    <th class="lblBaslikS" style="border: 1px solid navy; width: 80px">GEMİ<br />
                        ADI
                    </th>
                    <th class="lblBaslikS" style="border: 1px solid navy; width: 80px">TAYFA<br />
                        ADI</th>
                    <th class="lblBaslikS" style="border: 1px solid navy; width: 70px">NAV<br />
                        BELGE NO
                    </th>
                    <th class="lblBaslikS" style="border: 1px solid navy; width: 150px">FİRMA
                    </th>
                    <th class="lblBaslikS" style="border: 1px solid navy; width: 70px">HARİCİ<br />
                        BELGE NO
                    </th>
                    <th class="lblBaslikS" style="border: 1px solid navy; width: 80px">KAYIT<br />
                        TARİHİ
                    </th>
                    <th class="lblBaslikS" style="border: 1px solid navy; width: 70px">KAYNAK<br />
                        NO
                    </th>
                    <th class="lblBaslikS" style="border: 1px solid navy; width: 150px">KAYNAK
                    </th>
                    <th class="lblBaslikS" style="border: 1px solid navy; width: 70px">TUTAR<br />
                        TL
                    </th>
                    <th class="lblBaslikS" style="border: 1px solid navy; width: 70px">KDV<br />
                        TL</th>
                    <th class="lblBaslikS" style="border: 1px solid navy; width: 80px">TOPLAM<br />
                        TL</th>
                    <th class="lblBaslikS" style="border: 1px solid navy; width: 70px">KUR</th>
                    <th class="lblBaslikS" style="border: 1px solid navy; width: 80px">TOPLAM<br />
                        USD</th>
                    <th class="lblBaslikS" style="border: 1px solid navy; width: 100px">FATURA<br />
                        USD</th>
                    <th class="lblBaslikS" style="border: 1px solid navy; width: 50px">SEÇ</th>

                </tr>
            </thead>
            <asp:Repeater ID="rptFatura" runat="server">
                <ItemTemplate>
                    <tr runat="server" id="rowTr">
                        <td>
                            <asp:Literal ID="ltrentryno" runat="server" Visible="false" Text='<%# Eval("EntryNo") %>'></asp:Literal>
                            <%# Eval("IsNo") %>
                        </td>
                        <td>
                            <%# Eval("Gemi") %>
                        </td>
                        <td>
                            <%# Eval("Tayfa") %>
                        </td>
                        <td>
                            <%# Eval("NavBelgeNo") %>
                        </td>
                        <td>
                            <%# Eval("Firma") %>
                        </td>
                        <td>
                            <%# Eval("HariciBelgeNo") %>
                        </td>
                        <td>
                            <%# String.Format("{0:dd.MM.yyyy}", Eval("KayitTarihi")) %>
                        </td>
                        <td>
                            <%# Eval("KaynakNo") %>
                        </td>
                        <td>
                            <%# Eval("Kaynak") %>
                        </td>
                        <td align="right">
                            <%# Eval("TutarTL") %>
                        </td>
                        <td align="right">
                            <%# Eval("KdvTL") %>
                        </td>
                        <td align="right">
                            <%# Eval("ToplamTL") %>
                        </td>
                        <td align="right">
                            <%# Eval("Kur") %>
                        </td>
                        <td align="right">
                            <%# Eval("ToplamUSD") %>
                        </td>
                        <td style="width: 100px">
                            <asp:TextBox runat="server" ID="txtFaturaUsd" Width="90%" CssClass="Dectextbox" Text='<%# Eval("FaturaUSD") %>'></asp:TextBox>
                        </td>
                        <td style="background-color: white; border: none; text-align: center; width: 50px">
                            <asp:CheckBox ID="chSecTypeSales" runat="server" /></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
        <table class="table table-striped" style="width: 100%">
            <tr>
                <td align="right">
                    <asp:Button ID="btnOlustur" runat="server" Text="FATURA OLUŞTUR" CssClass="myButton" OnClick="btnOlustur_Click" />
                    <asp:Button ID="btnCalcLines" runat="server" Text="SEÇİLEN SATIRLARI GÜNCELLE" CssClass="myButton" OnClick="btnCalcLines_Click" /></td>
            </tr>
        </table>
    </div>
</asp:Content>

