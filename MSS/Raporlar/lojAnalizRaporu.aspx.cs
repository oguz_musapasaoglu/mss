﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Web;
using MSS1.Codes;

namespace MSS1.Raporlar
{
    public partial class lojAnalizRaporu : System.Web.UI.Page
    {
        #region Son Versiyon Session & Report Directory

        bool status = false;
        ReportDocument objrptdoc;
        fonksiyonlar f = new fonksiyonlar();
        SessionBase _obj;
        private string Pagetype
        {
            get
            {
                try
                {
                    return Request.QueryString["Tur"].ToString();
                }
                catch (Exception)
                {
                    return "";
                }


            }
        }
        protected override void OnInit(EventArgs e)
        {
            SetSessionValues();
            show();
        }


        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
         _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            { 
                if (Pagetype == "1")
                {
                    objrptdoc = new ReportDocument();
                    string reportPath = f.GetReportPath("LOJISTIK ANALIZ RAPOR YETKİ 652 WEB.rpt", _obj.Comp);
                    objrptdoc.Load(reportPath);
                    objrptdoc.SetParameterValue("KULLANICI", _obj.UserName);
                }
                else
                {
                    objrptdoc = new ReportDocument(); 
                    string reportPath = f.GetReportPath("LOJISTIK ANALIZ RAPOR YETKİ 652 WEB.rpt", _obj.Comp);
                    objrptdoc.Load(reportPath); 
                    objrptdoc.SetParameterValue("@USER", _obj.UserName);
                    objrptdoc.SetParameterValue("@COMP", _obj.Comp);
                  //  objrptdoc.SetParameterValue("ŞİRKET", fonksiyonlar.lojanalizRaporParameters[1].ToString());
                    objrptdoc.SetParameterValue("RAPOR YILI", fonksiyonlar.lojanalizRaporParameters[2].ToString());
                    objrptdoc.SetParameterValue("@YEAR", fonksiyonlar.lojanalizRaporParameters[2].ToString());
                    objrptdoc.SetParameterValue("RAPOR TÜRÜ", fonksiyonlar.lojanalizRaporParameters[3].ToString());
                   // objrptdoc.SetParameterValue("DÖNEM", fonksiyonlar.lojanalizRaporParameters[4].ToString());
                    objrptdoc.SetParameterValue("@OPDEPT", fonksiyonlar.lojanalizRaporParameters[5].ToString());
                    objrptdoc.SetParameterValue("@OPBÖLGE", fonksiyonlar.lojanalizRaporParameters[6].ToString());
                    objrptdoc.SetParameterValue("@MTDEPT", fonksiyonlar.lojanalizRaporParameters[7].ToString());
                    objrptdoc.SetParameterValue("@MT", fonksiyonlar.lojanalizRaporParameters[8].ToString());
                    objrptdoc.SetParameterValue("@MUSTERI", fonksiyonlar.lojanalizRaporParameters[9].ToString());
                    objrptdoc.SetParameterValue("@CARRIER", fonksiyonlar.lojanalizRaporParameters[10].ToString());
                    objrptdoc.SetParameterValue("@POLLIMAN", fonksiyonlar.lojanalizRaporParameters[11].ToString());
                    objrptdoc.SetParameterValue("@PODLIMAN", fonksiyonlar.lojanalizRaporParameters[12].ToString());
                    objrptdoc.SetParameterValue("@POLCOUNTRY", fonksiyonlar.lojanalizRaporParameters[13].ToString());
                    objrptdoc.SetParameterValue("@PODCOUNTRY", fonksiyonlar.lojanalizRaporParameters[14].ToString());
                    objrptdoc.SetParameterValue("@POLREGION", fonksiyonlar.lojanalizRaporParameters[15].ToString());
                    objrptdoc.SetParameterValue("@PODREGION", fonksiyonlar.lojanalizRaporParameters[16].ToString());
                    objrptdoc.SetParameterValue("@AGENT", fonksiyonlar.lojanalizRaporParameters[17].ToString());
                   // objrptdoc.SetParameterValue("RAPOR GRUP", fonksiyonlar.lojanalizRaporParameters[18].ToString());
                    //objrptdoc.SetParameterValue("TOP10 GRUP", fonksiyonlar.lojanalizRaporParameters[19].ToString());
                   // objrptdoc.SetParameterValue("TOP10 KONU", fonksiyonlar.lojanalizRaporParameters[20].ToString());
                    objrptdoc.SetParameterValue("OPTYPE", fonksiyonlar.lojanalizRaporParameters[21].ToString());
                    objrptdoc.SetParameterValue("JOB CUST TYPE", fonksiyonlar.lojanalizRaporParameters[22].ToString());
                    objrptdoc.SetParameterValue("GID", Request.QueryString["GId"].ToString());

                    //  objrptdoc.SetParameterValue("@INCOME", fonksiyonlar.lojanalizRaporParameters[23].ToString());
                    //  objrptdoc.SetParameterValue("@EXPENSE", fonksiyonlar.lojanalizRaporParameters[24].ToString());

                }

        
            }

            show();
        }
        public void show()
        {

            if (string.IsNullOrEmpty(txtId.Text))
            {
                txtId.Text = Guid.NewGuid().ToString();
                objrptdoc = new ReportDocument();
                if (Pagetype == "1")
                {
                    string reportPath = f.GetReportPath("LOJISTIK ANALIZ RAPOR YETKİ 652 WEB.rpt", _obj.Comp);
                    objrptdoc.Load(reportPath);
                    objrptdoc.SetDatabaseLogon("sa", fonksiyonlar.password);
                    objrptdoc.SetParameterValue("KULLANICI", _obj.UserName);
                }
                else
                {
                    //new Methods().WriteLog("Log show 1"); 
                    string reportPath = f.GetReportPath("LOJISTIK ANALIZ RAPOR YETKİ 652 WEB.rpt", _obj.Comp);
                    //new Methods().WriteLog("Log show 2");
                    objrptdoc.Load(reportPath);
                    //new Methods().WriteLog("Log show 3");
                    objrptdoc.SetParameterValue("@USER", _obj.UserName);
                    objrptdoc.SetParameterValue("@COMP", _obj.Comp);
                    //  objrptdoc.SetParameterValue("ŞİRKET", fonksiyonlar.lojanalizRaporParameters[1].ToString());
                    objrptdoc.SetParameterValue("RAPOR YILI", fonksiyonlar.lojanalizRaporParameters[2].ToString());
                    objrptdoc.SetParameterValue("@YEAR", fonksiyonlar.lojanalizRaporParameters[2].ToString());
                    objrptdoc.SetParameterValue("RAPOR TÜRÜ", fonksiyonlar.lojanalizRaporParameters[3].ToString());
                    // objrptdoc.SetParameterValue("DÖNEM", fonksiyonlar.lojanalizRaporParameters[4].ToString());
                    objrptdoc.SetParameterValue("@OPDEPT", fonksiyonlar.lojanalizRaporParameters[5].ToString());
                    objrptdoc.SetParameterValue("@OPBÖLGE", fonksiyonlar.lojanalizRaporParameters[6].ToString());
                    objrptdoc.SetParameterValue("@MTDEPT", fonksiyonlar.lojanalizRaporParameters[7].ToString());
                    objrptdoc.SetParameterValue("@MT", fonksiyonlar.lojanalizRaporParameters[8].ToString());
                    objrptdoc.SetParameterValue("@MUSTERI", fonksiyonlar.lojanalizRaporParameters[9].ToString());
                    objrptdoc.SetParameterValue("@CARRIER", fonksiyonlar.lojanalizRaporParameters[10].ToString());
                    objrptdoc.SetParameterValue("@POLLIMAN", fonksiyonlar.lojanalizRaporParameters[11].ToString());
                    objrptdoc.SetParameterValue("@PODLIMAN", fonksiyonlar.lojanalizRaporParameters[12].ToString());
                    objrptdoc.SetParameterValue("@POLCOUNTRY", fonksiyonlar.lojanalizRaporParameters[13].ToString());
                    objrptdoc.SetParameterValue("@PODCOUNTRY", fonksiyonlar.lojanalizRaporParameters[14].ToString());
                    objrptdoc.SetParameterValue("@POLREGION", fonksiyonlar.lojanalizRaporParameters[15].ToString());
                    objrptdoc.SetParameterValue("@PODREGION", fonksiyonlar.lojanalizRaporParameters[16].ToString());
                    objrptdoc.SetParameterValue("@AGENT", fonksiyonlar.lojanalizRaporParameters[17].ToString());
                    // objrptdoc.SetParameterValue("RAPOR GRUP", fonksiyonlar.lojanalizRaporParameters[18].ToString());
                    //objrptdoc.SetParameterValue("TOP10 GRUP", fonksiyonlar.lojanalizRaporParameters[19].ToString());
                    // objrptdoc.SetParameterValue("TOP10 KONU", fonksiyonlar.lojanalizRaporParameters[20].ToString());
                    objrptdoc.SetParameterValue("OPTYPE", fonksiyonlar.lojanalizRaporParameters[21].ToString());
                    objrptdoc.SetParameterValue("JOB CUST TYPE", fonksiyonlar.lojanalizRaporParameters[22].ToString());
                    //  objrptdoc.SetParameterValue("@INCOME", fonksiyonlar.lojanalizRaporParameters[23].ToString());
                    //  objrptdoc.SetParameterValue("@EXPENSE", fonksiyonlar.lojanalizRaporParameters[24].ToString());
                    objrptdoc.SetParameterValue("GID", Request.QueryString["GId"].ToString());
                    //new Methods().WriteLog("Log show 4");
                }
                Session["Objrptdoc" + txtId.Text] = objrptdoc;
                crycagir();
            }
            else
                crycagir();


        }
        public void crycagir()
        {
            ReportDocument rd = (ReportDocument)Session["Objrptdoc" + txtId.Text];
            CrystalReportViewer1.DisplayGroupTree = false;
            CrystalReportViewer1.DisplayToolbar = true;
            CrystalReportViewer1.EnableDatabaseLogonPrompt = false;
            CrystalReportViewer1.GroupTreeStyle.ShowLines = false;
            CrystalReportViewer1.ToolPanelView = ToolPanelViewType.None;
            CrystalReportViewer1.EnableDrillDown = true;
            CrystalReportViewer1.EnableParameterPrompt = true;
            CrystalReportViewer1.HasCrystalLogo = false;
            CrystalReportViewer1.HasDrillUpButton = true;
            CrystalReportViewer1.HasGotoPageButton = false;
            CrystalReportViewer1.HasPrintButton = true;
            CrystalReportViewer1.HasRefreshButton = true;
            CrystalReportViewer1.HasSearchButton = true;
            CrystalReportViewer1.HasToggleGroupTreeButton = false;
            CrystalReportViewer1.HasToggleParameterPanelButton = false;
            CrystalReportViewer1.HasZoomFactorList = true;
            CrystalReportViewer1.HasExportButton = true;
            CrystalReportViewer1.HyperlinkTarget = "_blank";
            CrystalReportViewer1.PageZoomFactor = 100;
            CrystalReportViewer1.HasDrilldownTabs = true;
            CrystalReportViewer1.ReuseParameterValuesOnRefresh = true;
            CrystalReportViewer1.DisplayPage = true;
            CrystalReportViewer1.ReportSource = rd;
        }


        #endregion
    }
}