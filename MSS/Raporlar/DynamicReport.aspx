﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DynamicReport.aspx.cs" Inherits="MSS1.Raporlar.DynamicReport" EnableViewState="true" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../style.css" rel="stylesheet" type="text/css" />
    <%--        <script src="../js/RightClick.js" type="text/javascript"></script>--%>

    <script type="text/javascript" src="../js/popuputilities.js"></script>
    <style type="text/css">
        #CrystalReportViewer1_toptoolbar_refresh{
            display:none !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <cr:crystalreportviewer id="CrystalReportViewer1" onunload="CrystalReportViewer1_Unload" runat="server" autodatabind="True"
                grouptreeimagesfolderurl=""
                height="50px" reportsourceid="CrystalReportSource1" toolbarimagesfolderurl=""
                toolpanelwidth="200px" width="350px" />
            <cr:crystalreportsource id="CrystalReportSource1" runat="server">
            <Report FileName="Raporlar\INVOICE.rpt">
            </Report>
        </cr:crystalreportsource>
        </div>
        <div style="display: none">
            <asp:TextBox runat="server" ID="txtId"></asp:TextBox>
        </div>
    </form>
</body>
</html>
