﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Web;
using MSS1.Codes;

namespace MSS1.Raporlar
{
    public partial class iadesatinalmarapor : System.Web.UI.Page
    {
        #region Son Versiyon Session & Report Directory

        bool status = false;
        ReportDocument objrptdoc;
        SessionBase _obj;
        protected override void OnInit(EventArgs e)
        {
            SetSessionValues();
            show();
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();


        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                fonksiyonlar f = new fonksiyonlar();
                string reportPath = f.GetReportPath("INVOICE IADE TUM GRUP.rpt",_obj.Comp);
                objrptdoc = new ReportDocument();
                objrptdoc.Load(reportPath);
                objrptdoc.SetParameterValue("@DOCNO", Request.QueryString["BelgeNo"]);
                objrptdoc.SetParameterValue("@COMP", _obj.Comp);
                objrptdoc.SetParameterValue("DETAY TOPLU", Request.QueryString["FT1"]);
                objrptdoc.SetParameterValue("NORMALPROFORMA", 1);
                objrptdoc.SetParameterValue("TOPLU FATURA METNİ", Request.QueryString["FTM"]);
                objrptdoc.SetParameterValue("EK METİN", Request.QueryString["FTEM"]);
                objrptdoc.SetParameterValue("ÖN İZLEME", 2);
                show();
            }
        }
        public void show()
        {
            if (!this.IsPostBack)
                Session["Objrptdoc"] = null;

            if (Session["Objrptdoc"] != null)
            {
                crycagir();
            }
            else
            {
                fonksiyonlar f = new fonksiyonlar();
                Session["Objrptdoc"] = objrptdoc;
                string reportPath = f.GetReportPath("INVOICE IADE TUM GRUP.rpt", _obj.Comp);
                crycagir();
            }
        }
        public void crycagir()
        {
            ReportDocument rd = (ReportDocument)Session["Objrptdoc"];
            CrystalReportViewer1.DisplayGroupTree = false;
            CrystalReportViewer1.DisplayToolbar = true;
            CrystalReportViewer1.EnableDatabaseLogonPrompt = false;
            CrystalReportViewer1.GroupTreeStyle.ShowLines = false;
            CrystalReportViewer1.ToolPanelView = ToolPanelViewType.None;
            CrystalReportViewer1.EnableDrillDown = true;
            CrystalReportViewer1.EnableParameterPrompt = true;
            CrystalReportViewer1.HasCrystalLogo = false;
            CrystalReportViewer1.HasDrillUpButton = true;
            CrystalReportViewer1.HasGotoPageButton = false;
            CrystalReportViewer1.HasPrintButton = true;
            CrystalReportViewer1.HasRefreshButton = true;
            CrystalReportViewer1.HasSearchButton = true;
            CrystalReportViewer1.HasToggleGroupTreeButton = false;
            CrystalReportViewer1.HasToggleParameterPanelButton = false;
            CrystalReportViewer1.HasZoomFactorList = true;
            CrystalReportViewer1.HasExportButton = true;
            CrystalReportViewer1.HyperlinkTarget = "_blank";
            CrystalReportViewer1.PageZoomFactor = 100;
            CrystalReportViewer1.HasDrilldownTabs = true;
            CrystalReportViewer1.ReuseParameterValuesOnRefresh = true;
            CrystalReportViewer1.DisplayPage = true;
            CrystalReportViewer1.ReportSource = rd;
        }

        #endregion
    }
}