﻿//------------------------------------------------------------------------------
// <otomatik üretildi>
//     Bu kod bir araç tarafından oluşturuldu.
//
//     Bu dosyada yapılacak değişiklikler yanlış davranışa neden olabilir ve
//     kod tekrar üretildi. 
// </otomatik üretildi>
//------------------------------------------------------------------------------

namespace MSS1.Raporlar
{


    public partial class DynamicReport
    {

        /// <summary>
        /// form1 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;

        /// <summary>
        /// CrystalReportViewer1 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::CrystalDecisions.Web.CrystalReportViewer CrystalReportViewer1;

        /// <summary>
        /// CrystalReportSource1 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::CrystalDecisions.Web.CrystalReportSource CrystalReportSource1;

        /// <summary>
        /// txtId denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtId;
    }
}
