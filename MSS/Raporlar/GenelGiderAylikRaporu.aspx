﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GenelGiderAylikRaporu.aspx.cs" Inherits="MSS1.Raporlar.GenelGiderAylikRaporu" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../style.css" rel="stylesheet" type="text/css" />
<%--        <script src="../js/RightClick.js" type="text/javascript"></script>--%>
<style type="text/css">
        #CrystalReportViewer1_toptoolbar_refresh{
            display:none !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="True"
         GroupTreeImagesFolderUrl="" 
            Height="50px" ReportSourceID="CrystalReportSource1" ToolbarImagesFolderUrl="" 
            ToolPanelWidth="200px" Width="350px" />
        <CR:CrystalReportSource ID="CrystalReportSource1" runat="server">
            <Report FileName="Raporlar\INVOICE.rpt">
            </Report>
        </CR:CrystalReportSource>
    </div>
    </form>
</body>
</html>
