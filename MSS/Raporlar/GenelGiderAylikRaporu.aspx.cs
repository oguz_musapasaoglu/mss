﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Web;
using MSS1.Codes;

namespace MSS1.Raporlar
{
    public partial class GenelGiderAylikRaporu : System.Web.UI.Page
    {
        #region Son Versiyon Session & Report Directory

        ReportDocument objrptdoc;
        fonksiyonlar f = new fonksiyonlar();

        private string _comp
        {
            get
            {
                string _GId = Request.QueryString["GId"];
                return TabSessions._SessionBase.Where(x => x.GuidKey == _GId).FirstOrDefault().Comp;
            }
        }

        private string _user
        {
            get
            {
                string _GId = Request.QueryString["GId"];
                return TabSessions._SessionBase.Where(x => x.GuidKey == _GId).FirstOrDefault().UserName;
            }
        }
        protected override void OnInit(EventArgs e)
        {

            show();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["Objrptdoc"] = null;
                objrptdoc = new ReportDocument();
                string reportPath = f.GetReportPath("JOBREPORT GENELIS YETKİ 65.rpt", _comp);
                objrptdoc.Load(reportPath);
                objrptdoc.SetParameterValue("ŞİRKET", fonksiyonlar.genelGiderAylikRaporParameters[0].ToString());
                objrptdoc.SetParameterValue("RAPOR TÜRÜ", fonksiyonlar.genelGiderAylikRaporParameters[1].ToString());
                objrptdoc.SetParameterValue("GRUP1", fonksiyonlar.genelGiderAylikRaporParameters[2].ToString());
                objrptdoc.SetParameterValue("GRUP2", fonksiyonlar.genelGiderAylikRaporParameters[3].ToString());
                objrptdoc.SetParameterValue("GRUP3", fonksiyonlar.genelGiderAylikRaporParameters[4].ToString());
                objrptdoc.SetParameterValue("ANADEPT", fonksiyonlar.genelGiderAylikRaporParameters[5].ToString());
                objrptdoc.SetParameterValue("@COMP", fonksiyonlar.genelGiderAylikRaporParameters[6].ToString());
                objrptdoc.SetParameterValue("@USER", fonksiyonlar.genelGiderAylikRaporParameters[7].ToString());
                objrptdoc.SetParameterValue("@YIL", fonksiyonlar.genelGiderAylikRaporParameters[8].ToString());
                objrptdoc.SetParameterValue("@DEPT", fonksiyonlar.genelGiderAylikRaporParameters[9].ToString());
                objrptdoc.SetParameterValue("@BÖLGE", fonksiyonlar.genelGiderAylikRaporParameters[10].ToString());
                objrptdoc.SetParameterValue("@PERS", fonksiyonlar.genelGiderAylikRaporParameters[11].ToString());
                objrptdoc.SetParameterValue("@ARAC", fonksiyonlar.genelGiderAylikRaporParameters[12].ToString());
                objrptdoc.SetParameterValue("@FIRMA", fonksiyonlar.genelGiderAylikRaporParameters[13].ToString());
                objrptdoc.SetParameterValue("@GGRUP", fonksiyonlar.genelGiderAylikRaporParameters[14].ToString());
                Session["Objrptdoc"] = objrptdoc;
                show();
            }
            
        }
        public void show()
        {
            if (!this.IsPostBack)
            {
                ReportDocument _dynreport = new ReportDocument();
                Session["Objrptdoc"] = _dynreport;
                Session["Objrptdoc"] = null;

            }

            if (Session["Objrptdoc"] != null)
            {
                crycagir();
            }
            else
            {
                //objrptdoc = new ReportDocument();
                //string reportPath = f.GetReportPath("JOBREPORT GENELIS YETKİ 65.rpt", _comp);
                //objrptdoc.Load(reportPath);
                //objrptdoc.SetParameterValue("ŞİRKET", fonksiyonlar.genelGiderAylikRaporParameters[0].ToString());
                //objrptdoc.SetParameterValue("RAPOR TÜRÜ", fonksiyonlar.genelGiderAylikRaporParameters[1].ToString());
                //objrptdoc.SetParameterValue("GRUP1", fonksiyonlar.genelGiderAylikRaporParameters[2].ToString());
                //objrptdoc.SetParameterValue("GRUP2", fonksiyonlar.genelGiderAylikRaporParameters[3].ToString());
                //objrptdoc.SetParameterValue("GRUP3", fonksiyonlar.genelGiderAylikRaporParameters[4].ToString());
                //objrptdoc.SetParameterValue("ANADEPT", fonksiyonlar.genelGiderAylikRaporParameters[5].ToString());
                //objrptdoc.SetParameterValue("@COMP", fonksiyonlar.genelGiderAylikRaporParameters[6].ToString());
                //objrptdoc.SetParameterValue("@USER", fonksiyonlar.genelGiderAylikRaporParameters[7].ToString());
                //objrptdoc.SetParameterValue("@YIL", fonksiyonlar.genelGiderAylikRaporParameters[8].ToString());
                //objrptdoc.SetParameterValue("@DEPT", fonksiyonlar.genelGiderAylikRaporParameters[9].ToString());
                //objrptdoc.SetParameterValue("@BÖLGE", fonksiyonlar.genelGiderAylikRaporParameters[10].ToString());
                //objrptdoc.SetParameterValue("@PERS", fonksiyonlar.genelGiderAylikRaporParameters[11].ToString());
                //objrptdoc.SetParameterValue("@ARAC", fonksiyonlar.genelGiderAylikRaporParameters[12].ToString());
                //objrptdoc.SetParameterValue("@FIRMA", fonksiyonlar.genelGiderAylikRaporParameters[13].ToString());
                //objrptdoc.SetParameterValue("@GGRUP", fonksiyonlar.genelGiderAylikRaporParameters[14].ToString());
                Session["Objrptdoc"] = objrptdoc;
                crycagir();

            }
        }
        public void crycagir()
        {
            ReportDocument rd = (ReportDocument)Session["Objrptdoc"];
            CrystalReportViewer1.DisplayGroupTree = false;
            CrystalReportViewer1.DisplayToolbar = true;
            CrystalReportViewer1.EnableDatabaseLogonPrompt = false;
            CrystalReportViewer1.GroupTreeStyle.ShowLines = false;
            CrystalReportViewer1.ToolPanelView = ToolPanelViewType.None;
            CrystalReportViewer1.EnableDrillDown = true;
            CrystalReportViewer1.EnableParameterPrompt = true;
            CrystalReportViewer1.HasCrystalLogo = false;
            CrystalReportViewer1.HasDrillUpButton = true;
            CrystalReportViewer1.HasGotoPageButton = false;
            CrystalReportViewer1.HasPrintButton = true;
            CrystalReportViewer1.HasRefreshButton = true;
            CrystalReportViewer1.HasSearchButton = true;
            CrystalReportViewer1.HasToggleGroupTreeButton = false;
            CrystalReportViewer1.HasToggleParameterPanelButton = false;
            CrystalReportViewer1.HasZoomFactorList = true;
            CrystalReportViewer1.HasExportButton = true;
            CrystalReportViewer1.HyperlinkTarget = "_blank";
            CrystalReportViewer1.PageZoomFactor = 100;
            CrystalReportViewer1.HasDrilldownTabs = true;
            CrystalReportViewer1.ReuseParameterValuesOnRefresh = false;
            CrystalReportViewer1.DisplayPage = true;
            CrystalReportViewer1.ReportSource = rd;
        }


        #endregion
    }
}