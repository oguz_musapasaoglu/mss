﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Web;
using MSS1.Codes;

namespace MSS1.Raporlar
{
    public partial class lojAylikRaporu : System.Web.UI.Page
    {
        #region Son Versiyon Session & Report Directory

        bool status = false;
        ReportDocument objrptdoc;
        fonksiyonlar f = new fonksiyonlar();
        SessionBase _obj;
        private string Pagetype
        {
            get
            {
                try
                {
                    return Request.QueryString["Tur"].ToString();
                }
                catch (Exception)
                {
                    return "";
                }


            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();
            show();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {


                if (Pagetype == "1")
                {
                    objrptdoc = new ReportDocument();
                    string reportPath = f.GetReportPath("LOJISTIK AYLIK RAPOR YETKİ 65.rpt", _obj.Comp);
                    objrptdoc.Load(reportPath);
                    objrptdoc.SetParameterValue("KULLANICI", _obj.UserName);
                }
                else
                {
                    objrptdoc = new ReportDocument();
                    string reportPath = f.GetReportPath("LOJISTIK AYLIK RAPOR YETKİ 652.rpt", _obj.Comp);
                    objrptdoc.Load(reportPath);
                    objrptdoc.SetParameterValue("@USER", _obj.UserName);
                    objrptdoc.SetParameterValue("@COMP", _obj.Comp);
                    objrptdoc.SetParameterValue("ŞİRKET", fonksiyonlar.lojaylikRaporParameters[1].ToString());
                    objrptdoc.SetParameterValue("RAPOR YILI", fonksiyonlar.lojaylikRaporParameters[2].ToString());
                    objrptdoc.SetParameterValue("RAPOR TÜRÜ", fonksiyonlar.lojaylikRaporParameters[3].ToString());
                    objrptdoc.SetParameterValue("DÖNEM", fonksiyonlar.lojaylikRaporParameters[4].ToString());
                    objrptdoc.SetParameterValue("@OPDEPT", fonksiyonlar.lojaylikRaporParameters[5].ToString());
                    objrptdoc.SetParameterValue("@OPBÖLGE", fonksiyonlar.lojaylikRaporParameters[6].ToString());
                    objrptdoc.SetParameterValue("@MTDEPT", fonksiyonlar.lojaylikRaporParameters[7].ToString());
                    objrptdoc.SetParameterValue("@MT", fonksiyonlar.lojaylikRaporParameters[8].ToString());
                    objrptdoc.SetParameterValue("@MUSTERI", fonksiyonlar.lojaylikRaporParameters[9].ToString());
                    objrptdoc.SetParameterValue("@CARRIER", fonksiyonlar.lojaylikRaporParameters[10].ToString());
                    objrptdoc.SetParameterValue("@POLLIMAN", fonksiyonlar.lojaylikRaporParameters[11].ToString());
                    objrptdoc.SetParameterValue("@PODLIMAN", fonksiyonlar.lojaylikRaporParameters[12].ToString());
                    objrptdoc.SetParameterValue("@POLCOUNTRY", fonksiyonlar.lojaylikRaporParameters[13].ToString());
                    objrptdoc.SetParameterValue("@PODCOUNTRY", fonksiyonlar.lojaylikRaporParameters[14].ToString());
                    objrptdoc.SetParameterValue("@POLREGION", fonksiyonlar.lojaylikRaporParameters[15].ToString());
                    objrptdoc.SetParameterValue("@PODREGION", fonksiyonlar.lojaylikRaporParameters[16].ToString());
                    objrptdoc.SetParameterValue("@AGENT", fonksiyonlar.lojaylikRaporParameters[17].ToString());
                    objrptdoc.SetParameterValue("RAPOR GRUP", fonksiyonlar.lojaylikRaporParameters[18].ToString());
                    objrptdoc.SetParameterValue("TOP10 GRUP", fonksiyonlar.lojaylikRaporParameters[19].ToString());
                    objrptdoc.SetParameterValue("TOP10 KONU", fonksiyonlar.lojaylikRaporParameters[20].ToString());
                    objrptdoc.SetParameterValue("OPTYPE", fonksiyonlar.lojaylikRaporParameters[21].ToString());
                    objrptdoc.SetParameterValue("JOB CUST TYPE", fonksiyonlar.lojaylikRaporParameters[22].ToString());
                    objrptdoc.SetParameterValue("@INCOME", fonksiyonlar.lojaylikRaporParameters[23].ToString());
                    objrptdoc.SetParameterValue("@EXPENSE", fonksiyonlar.lojaylikRaporParameters[24].ToString());
                }

                show();
            }
        }
        public void show()
        {
            if (!this.IsPostBack)
                Session["ObjrptdocLojAy"] = null;

            if (Session["ObjrptdocLojAy"] != null)
            {
                crycagir();
            }
            else
            {
                objrptdoc = new ReportDocument();

                if (Pagetype == "1")
                {
                    string reportPath = f.GetReportPath("LOJISTIK AYLIK RAPOR YETKİ 65.rpt",_obj.Comp);
                    objrptdoc.Load(reportPath);
                    objrptdoc.SetDatabaseLogon("sa", fonksiyonlar.password);
                    objrptdoc.SetParameterValue("KULLANICI", _obj.UserName);
                }
                else
                {
                    string reportPath = f.GetReportPath("LOJISTIK AYLIK RAPOR YETKİ 652.rpt", _obj.Comp);
                    objrptdoc.Load(reportPath);
                    objrptdoc.SetParameterValue("@USER",  _obj.UserName);
                    objrptdoc.SetParameterValue("@COMP",  _obj.Comp);
                    objrptdoc.SetParameterValue("ŞİRKET", fonksiyonlar.lojaylikRaporParameters[1].ToString());
                    objrptdoc.SetParameterValue("RAPOR YILI", fonksiyonlar.lojaylikRaporParameters[2].ToString());
                    objrptdoc.SetParameterValue("RAPOR TÜRÜ", fonksiyonlar.lojaylikRaporParameters[3].ToString());
                    objrptdoc.SetParameterValue("DÖNEM", fonksiyonlar.lojaylikRaporParameters[4].ToString());
                    objrptdoc.SetParameterValue("@OPDEPT", fonksiyonlar.lojaylikRaporParameters[5].ToString());
                    objrptdoc.SetParameterValue("@OPBÖLGE", fonksiyonlar.lojaylikRaporParameters[6].ToString());
                    objrptdoc.SetParameterValue("@MTDEPT", fonksiyonlar.lojaylikRaporParameters[7].ToString());
                    objrptdoc.SetParameterValue("@MT", fonksiyonlar.lojaylikRaporParameters[8].ToString());
                    objrptdoc.SetParameterValue("@MUSTERI", fonksiyonlar.lojaylikRaporParameters[9].ToString());
                    objrptdoc.SetParameterValue("@CARRIER", fonksiyonlar.lojaylikRaporParameters[10].ToString());
                    objrptdoc.SetParameterValue("@POLLIMAN", fonksiyonlar.lojaylikRaporParameters[11].ToString());
                    objrptdoc.SetParameterValue("@PODLIMAN", fonksiyonlar.lojaylikRaporParameters[12].ToString());
                    objrptdoc.SetParameterValue("@POLCOUNTRY", fonksiyonlar.lojaylikRaporParameters[13].ToString());
                    objrptdoc.SetParameterValue("@PODCOUNTRY", fonksiyonlar.lojaylikRaporParameters[14].ToString());
                    objrptdoc.SetParameterValue("@POLREGION", fonksiyonlar.lojaylikRaporParameters[15].ToString());
                    objrptdoc.SetParameterValue("@PODREGION", fonksiyonlar.lojaylikRaporParameters[16].ToString());
                    objrptdoc.SetParameterValue("@AGENT", fonksiyonlar.lojaylikRaporParameters[17].ToString());
                    objrptdoc.SetParameterValue("RAPOR GRUP", fonksiyonlar.lojaylikRaporParameters[18].ToString());
                    objrptdoc.SetParameterValue("TOP10 GRUP", fonksiyonlar.lojaylikRaporParameters[19].ToString());
                    objrptdoc.SetParameterValue("TOP10 KONU", fonksiyonlar.lojaylikRaporParameters[20].ToString());
                    objrptdoc.SetParameterValue("OPTYPE", fonksiyonlar.lojaylikRaporParameters[21].ToString());
                    objrptdoc.SetParameterValue("JOB CUST TYPE", fonksiyonlar.lojaylikRaporParameters[22].ToString());
                    objrptdoc.SetParameterValue("@INCOME", fonksiyonlar.lojaylikRaporParameters[23].ToString());
                    objrptdoc.SetParameterValue("@EXPENSE", fonksiyonlar.lojaylikRaporParameters[24].ToString());

                }

                Session["ObjrptdocLojAy"] = objrptdoc;
                crycagir();

            }
        }
        public void crycagir()
        {
            ReportDocument rd = (ReportDocument)Session["ObjrptdocLojAy"];
            CrystalReportViewer1.DisplayGroupTree = false;
            CrystalReportViewer1.DisplayToolbar = true;
            CrystalReportViewer1.EnableDatabaseLogonPrompt = false;
            CrystalReportViewer1.GroupTreeStyle.ShowLines = false;
            CrystalReportViewer1.ToolPanelView = ToolPanelViewType.None;
            CrystalReportViewer1.EnableDrillDown = true;
            CrystalReportViewer1.EnableParameterPrompt = true;
            CrystalReportViewer1.HasCrystalLogo = false;
            CrystalReportViewer1.HasDrillUpButton = true;
            CrystalReportViewer1.HasGotoPageButton = false;
            CrystalReportViewer1.HasPrintButton = true;
            CrystalReportViewer1.HasRefreshButton = true;
            CrystalReportViewer1.HasSearchButton = true;
            CrystalReportViewer1.HasToggleGroupTreeButton = false;
            CrystalReportViewer1.HasToggleParameterPanelButton = false;
            CrystalReportViewer1.HasZoomFactorList = true;
            CrystalReportViewer1.HasExportButton = true;
            CrystalReportViewer1.HyperlinkTarget = "_blank";
            CrystalReportViewer1.PageZoomFactor = 100;
            CrystalReportViewer1.HasDrilldownTabs = true;
            CrystalReportViewer1.ReuseParameterValuesOnRefresh = true;
            CrystalReportViewer1.DisplayPage = true;
            CrystalReportViewer1.ReportSource = rd;
        }


        #endregion
    }
}