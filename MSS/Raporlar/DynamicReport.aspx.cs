﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using System.ServiceModel;
using MSS1.WSGeneric;
using MSS1.Codes;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System.IO;


namespace MSS1.Raporlar
{
    public partial class DynamicReport : Bases
    {
        bool status = false;
        ReportDocument objrptdoc;
        string ConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        public ws_baglantı ws = new ws_baglantı();
        public Methods method = new Methods();
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private string[] RPost
        {
            get
            {
                string[] split = Request.QueryString["RPost"].ToString().Split('|');
                return split;
            }
        }

        private string _comp
        {
            get
            {
                string _GId = Request.QueryString["GId"];
                return TabSessions._SessionBase.Where(x => x.GuidKey == _GId).FirstOrDefault().Comp;
            }
        }

        private string _user
        {
            get
            {
                string _GId = Request.QueryString["GId"];
                return TabSessions._SessionBase.Where(x => x.GuidKey == _GId).FirstOrDefault().UserName;
            }
        }

        private string _nameusername
        {
            get
            {
                string _GId = Request.QueryString["GId"];
                return TabSessions._SessionBase.Where(x => x.GuidKey == _GId).FirstOrDefault().NameSurName;
            }

        }

        private string _selectedlang
        {
            get
            {
                string _GId = Request.QueryString["GId"];
                return TabSessions._SessionBase.Where(x => x.GuidKey == _GId).FirstOrDefault().SelectedLanguage;
            }
        }

        protected override void OnInit(EventArgs e)
        {

            show();
        }
        private Web_GenericFunction_PortClient GetWebFonksiyonlari()
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(_comp)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                if (Request.QueryString["ReportCode"] == "MERGE")
                {
                    CrystalReportViewer1.Visible = false;
                    MergePDF();
                    return;
                }

                JobPrint();
                KasaYazdir();

                if (Request.QueryString["IslemId"] != null)
                {
                    if (Request.QueryString["LOJ"] == null)
                        LoadReportLoj(_comp);
                    else
                        LoadReport(_comp, Request.QueryString["ReportCode"].ToString());
                }
                else
                {
                    if (Request.QueryString["CoverPagePrinted"] != null && Request.QueryString["LOJ"] != null)
                    {
                        string[] _params = { Request.QueryString["jobno"].ToString() };

                        string drm = GetWebFonksiyonlari().ExecGenericWebFunctionAllComp(Session["kulad"].ToString(), "50014", "JobpreviewUpdate§" + string.Join("§", _params), Session["Sirket"].ToString());
                        if (drm == "OK")
                        {
                            LoadReport(_comp, Request.QueryString["ReportCode"].ToString());
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(GetType(), "JSFaturaAcik", @"window.opener.parent.PreviewError();self.close();", true);
                        }
                    }
                    else
                    {
                        LoadReport(_comp, Request.QueryString["ReportCode"].ToString());
                    }
                }

                KasaDeftereNaklet();

                if (Request.QueryString["ObjectId"] != null & Request.QueryString["PageClose"] != null)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "proceed3('" + Request.QueryString["ObjectId"] + "');", true);
                }

            }
            //else
            //    show();


        }

        private void MergePDF()
        {
            DataTable dt = new DataTable();

            string query = "select * from [0C_00167_80_JOB_DOCUMENTS] where [Job no]='" + Request.QueryString["jobno"].ToString() + "'";
            dt = f.datatablegonder(query);


            if (dt.Rows.Count > 0)
            {
                string[] pdfs = new string[dt.Rows.Count];
                string _filepath = string.Empty;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _filepath = Server.MapPath("~") + CurrentFolderName(_comp) + "/" + dt.Rows[i]["DocPATH2"].ToString();
                    if (File.Exists(_filepath))
                        pdfs[i] = _filepath;
                }

                if (pdfs.Length == 0)
                {
                    Response.Write("<span><h3>Pdf files has not in specified folder</h3></span>");
                    return;
                }


                using (PdfDocument targetDoc = new PdfDocument())
                {
                    foreach (string pdf in pdfs)
                    {
                        try
                        {
                            using (PdfDocument pdfDoc = PdfReader.Open(pdf, PdfDocumentOpenMode.Import))
                            {

                                for (int i = 0; i < pdfDoc.PageCount; i++)
                                {
                                    targetDoc.AddPage(pdfDoc.Pages[i]);
                                }
                            }
                        }
                        catch
                        {
                        }



                    }

                    string tName = @"/Dosya/Job Pdfs/" + _user + "_" + DateTime.Now.ToFileTime().ToString() + "_pdffile.pdf";
                    string targetPath = Server.MapPath("~") + CurrentFolderName(_comp) + tName;
                    if (File.Exists(targetPath))
                    {
                        File.Delete(targetPath);
                    }

                    targetDoc.Save(targetPath);

                    Response.Redirect("~/" + CurrentFolderName(_comp) + tName);
                }

            }
            else
            {
                Response.Write("<span><h3>Pdf file not found</h3></span>");
            }






        }

        void AddPdf(string _CreatedFile, string _AppendFile)
        {
            string[] pdfs = { _CreatedFile, _AppendFile };
            using (PdfDocument targetDoc = new PdfDocument())
            {
                foreach (string pdf in pdfs)
                {
                    try
                    {
                        using (PdfDocument pdfDoc = PdfReader.Open(pdf, PdfDocumentOpenMode.Import))
                        {

                            for (int i = 0; i < pdfDoc.PageCount; i++)
                            {
                                targetDoc.AddPage(pdfDoc.Pages[i]);
                            }
                        }
                    }
                    catch
                    {
                    }

                    if (File.Exists(_CreatedFile))
                    {
                        File.Delete(_CreatedFile);
                    }

                    targetDoc.Save(_CreatedFile);

                }
            }
        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        void JobPrint()
        {
            if (Request.QueryString["JobPrinted"] != null)
            {
                if (Request.QueryString["JobPrinted"].ToString() == "1")
                {

                    string[] _params = { Request.QueryString["jobno"].ToString(), _user };
                    GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_user, "50000", "MuhJobCoverPagePrintedUpdate§" + string.Join("§", _params), _comp);

                }
            }
        }

        private void KasaYazdir()
        {


            if (Request.QueryString["KasaYazdir"] != null)
            {


                string SatirNo = Request.QueryString["SatirNo"].ToString()
                       , Jtn = RPost[0].ToString()
                       , Jtb = RPost[1].ToString()
                       , WebSatirNo = RPost[2].ToString()
                       , BelgeNo = RPost[3].ToString()
                       , KarsiHesapNo = RPost[4].ToString()
                       , SecilenKasaNo = RPost[5].ToString();

                if (KarsiHesapNo.Trim().Length > 0 || KarsiHesapNo.Trim().Length > 0)
                {
                    string[] _params = { Jtn, Jtb, SatirNo, "2", "" };
                    GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_user, "50000", "KasaGirisFonksiyon§" + string.Join("§", _params), _comp);

                }
                else
                {
                    string[] _params = { Jtn, Jtb, SatirNo, "2", SecilenKasaNo };
                    GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_user, "50000", "KasaGirisFonksiyon§" + string.Join("§", _params), _comp);

                }


            }
        }

        private void KasaDeftereNaklet()
        {
            if (Request.QueryString["KasaYazdir"] != null)
            {

                string SatirNo = Request.QueryString["SatirNo"].ToString()
                       , Jtn = RPost[0].ToString()
                       , Jtb = RPost[1].ToString()
                       , WebSatirNo = RPost[2].ToString()
                       , BelgeNo = RPost[3].ToString()
                       , KarsiHesapNo = RPost[4].ToString()
                       , SecilenKasaNo = RPost[5].ToString();



                string[] _params = { Jtn, Jtb, SatirNo };
                string drm = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_user, "50000", "KasaSatırSilKontrol§" + string.Join("§", _params), _comp);

                if (drm == "False")
                {
                    if (KarsiHesapNo.Trim().Length > 0)
                    {
                        string[] _params2 = { _user, SatirNo, WebSatirNo, _nameusername, "" };
                        GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_user, "50000", "KasaDeftereNaklet§" + string.Join("§", _params2), _comp);

                    }
                    else
                    {
                        string[] _params3 = { _user, SatirNo, "0", _nameusername, BelgeNo };
                        GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_user, "50000", "KasaDeftereNaklet§" + string.Join("§", _params3), _comp);

                    }
                }
                else
                {
                    MessageBox(l.dilgetir(_selectedlang, _comp, _user, "kasagiris", "msg212"));
                }
                Response.Redirect("../" + CurrentFolderName(_comp) + "/Makbuz/" + BelgeNo + ".pdf");
            }

        }

        void LoadReport(string Company, string ReportCode)
        {
            string _aspx = string.Empty, _crystal = string.Empty, _SessionVal = string.Empty;
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
            {

                using (DataTable dt = new DataTable())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "Select * from [0C_50409_01_REPORT_DIRECTORY] where COMPANY=@Company and  ReportCode=@ReportCode";
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 1000;
                        cmd.Connection = conn;
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@Company", Company);
                        cmd.Parameters.AddWithValue("@ReportCode", ReportCode);
                        using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                        {
                            adp.Fill(dt);

                            if (dt.Rows.Count > 0)
                            {

                                _aspx = dt.Rows[0]["ASPX1"].ToString() + dt.Rows[0]["ASPX2"].ToString();
                                _crystal = dt.Rows[0]["CRYSTAL1"].ToString() + dt.Rows[0]["CRYSTAL2"].ToString();
                                string[] _arr1 = _aspx.Split(',');
                                string[] _arr2 = _crystal.Split(',');
                                int _sayac = 0;
                                if (_arr1.Length != _arr2.Length)
                                {
                                    MessageBox(l.dilgetir(_selectedlang, _comp, _user, "kasagiris", "msg216"));
                                    return;
                                }
                                objrptdoc = new ReportDocument();
                                objrptdoc.Load(dt.Rows[0]["Report Path"].ToString());
                                foreach (string item in _arr1)
                                {
                                    if (string.IsNullOrEmpty(item)) continue;
                                    if (item.Substring(0, 1) == "$")
                                    {
                                        if (item.Replace("$", string.Empty).ToLower() == "sirket")
                                            _SessionVal = _comp;
                                        else if (item.Replace("$", string.Empty).ToLower() == "kulad")
                                            _SessionVal = _user;
                                        else
                                            _SessionVal = Session[item.Replace("$", string.Empty)].ToString();
                                        objrptdoc.SetParameterValue(_arr2[_sayac], _SessionVal);
                                    }
                                    else if (item.Substring(0, 1) == "#")
                                        objrptdoc.SetParameterValue(_arr2[_sayac], item.Replace("#", string.Empty));
                                    else
                                        objrptdoc.SetParameterValue(_arr2[_sayac], Request.QueryString[item].ToString());
                                    _sayac++;
                                }
                                if (Request.QueryString["TalepNo"] != null)
                                {
                                    objrptdoc.ExportToDisk(ExportFormatType.PortableDocFormat, Server.MapPath("../" + CurrentFolderName(_comp) + "/PDF/" + Request.QueryString["TalepNo"] + ".PDF"));
                                }
                                if (Request.QueryString["KasaYazdir"] != null)
                                {
                                    objrptdoc.ExportToDisk(ExportFormatType.PortableDocFormat, Server.MapPath("../" + CurrentFolderName(_comp) + "/Makbuz/" + RPost[3] + ".PDF"));
                                }
                                string Folder1 = string.Empty, Folder2 = string.Empty, Folder3 = string.Empty;
                                if (Request.QueryString["SaveName"] != null & Request.QueryString["FolderName"] != null && Request.QueryString["state"] != null)
                                {

                                    if (Request.QueryString["SaveMethod"] == "save" && Request.QueryString["state"] == "3")
                                    {


                                        objrptdoc.ExportToDisk(ExportFormatType.PortableDocFormat, Server.MapPath("../" + CurrentFolderName(_comp) + "/RAPORLAR OP/" + Request.QueryString["Folder1"] + "/" + Request.QueryString["Folder2"] + "/" + Request.QueryString["Folder3"] + "/" + Request.QueryString["SaveName"] + ".PDF"));
                                    }
                                    else if (Request.QueryString["state"] == "BLSELECT")
                                        objrptdoc.ExportToDisk(ExportFormatType.PortableDocFormat, Server.MapPath("../" + CurrentFolderName(_comp) + "/RAPORLAR OP/" + Request.QueryString["Company"] + "/" + Request.QueryString["FolderName"] + "/" + Request.QueryString["Type"] + "/" + Request.QueryString["SaveName"] + ".PDF"));

                                }


                                show();
                            }
                            else
                                MessageBox(l.dilgetir(_selectedlang, _comp, _user, "kasagiris", "msg217"));
                        }
                    }
                }

            }
        }

        void LoadReportLoj(string Company)
        {
            string PipeString = GetPipeString(Request.QueryString["IslemId"].ToString());
            if (string.IsNullOrEmpty(PipeString)) return;
            string[] PipeArray = PipeString.Split('|');
            string _aspx = string.Empty, _crystal = string.Empty, ReportCode = PipeArray[8];
            if (string.IsNullOrEmpty(ReportCode))
            {
                MessageBox(l.dilgetir(_selectedlang, _comp, _user, "kasagiris", "msg218"));
                return;
            }


            //GAC|G1801119|G124961|H1222|GH008190|1||20000|RPT14||PREVIEW
            int companycode = 1,
            LojTaskNo = Convert.ToInt32(PipeArray[7]), buttontype = -1
            , BlType = Convert.ToInt32(PipeArray[5]);
            string sirket = PipeArray[0]
                , JobNo = PipeArray[1]
                , TaskNo = PipeArray[2]
                , DocumentCode = string.Empty
                , MainType = PipeArray[3]
                , VoyageNo = PipeArray[6]
                , DocNameTr = PipeArray[9]
                , BlTypeDesc = BlType == 1 ? "HBL" : "MBL"
                , BlNo = PipeArray[4]
                , Folder1 = string.Empty
                , Folder2 = string.Empty
                , Folder3 = string.Empty
            , HouseBlNo = BlType == 1 ? BlNo : ""
            , MasterBlNo = BlType == 2 ? BlNo : "";

            string sorgu = "SELECT  * FROM [GAC_NAV2].[dbo].[0C_50041_00_DOCUMENTS]  WHERE [Silindi] = 0  and COMPANY = '" + Company + "' "
            + " and [Main Type] = '" + MainType + "' and [Report Code] = '" + ReportCode + "' and [Document Name Tr] = '" + DocNameTr + "'";
            DataTable dt1 = f.datatablegonder(sorgu);
            if (dt1.Rows.Count > 0)
            {
                Folder1 = dt1.Rows[0]["Folder 1"].ToString();
                Folder2 = dt1.Rows[0]["Folder 2"].ToString();
                Folder3 = dt1.Rows[0]["Folder 3"].ToString();
                DocumentCode = dt1.Rows[0]["Document Code"].ToString();
                companycode = Convert.ToInt32(dt1.Rows[0][2]);
            }

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
            {

                using (DataTable dt = new DataTable())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "Select * from [0C_50409_01_REPORT_DIRECTORY] where COMPANY=@Company and  ReportCode=@ReportCode";
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 1000;
                        cmd.Connection = conn;
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@Company", Company);
                        cmd.Parameters.AddWithValue("@ReportCode", ReportCode);
                        using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                        {
                            adp.Fill(dt);

                            if (dt.Rows.Count > 0)
                            {

                                _aspx = dt.Rows[0]["ASPX1"].ToString() + dt.Rows[0]["ASPX2"].ToString();
                                _crystal = dt.Rows[0]["CRYSTAL1"].ToString() + dt.Rows[0]["CRYSTAL2"].ToString();
                                string[] _arr1 = _aspx.Split(',');
                                string[] _arr2 = _crystal.Split(',');
                                if (_arr1.Length != _arr2.Length)
                                {
                                    MessageBox(l.dilgetir(_selectedlang, _comp, _user, "kasagiris", "msg216"));
                                    return;
                                }
                                objrptdoc = new ReportDocument();
                                objrptdoc.Load(dt.Rows[0]["Report Path"].ToString());


                                switch (ReportCode)
                                {
                                    case "RPT12":
                                    case "RPT13":
                                    case "RPT14":
                                    case "RPL13":
                                        objrptdoc.SetParameterValue("@COMP", Company);
                                        objrptdoc.SetParameterValue("@BLNO", BlNo);
                                        objrptdoc.SetParameterValue("@BLTYPE", Request.QueryString["BLTYPE"].ToString());
                                        break;
                                    case "RPT15":
                                        objrptdoc.SetParameterValue("@COMPANY", Company);
                                        objrptdoc.SetParameterValue("@BL", BlNo);
                                        break;
                                }

                                switch (PipeArray[10])
                                {
                                    case "BL":
                                        buttontype = 4;

                                        string SaveName = GetReportName(JobNo, LojTaskNo, TaskNo, DocumentCode, VoyageNo) + ".PDF";


                                        CreateDocument(JobNo, TaskNo, LojTaskNo, DocumentCode, VoyageNo, companycode, SaveName, Folder2, MainType, ReportCode,
                                            BlType, BlTypeDesc, HouseBlNo, MasterBlNo, buttontype);

                                        string F1 = string.Empty;
                                        if (_comp == "GAC" | _comp == "LAM")
                                            F1 = Folder1 == "1" ? "GAC" : "LAM";
                                        else if (_comp == "LAMLOJ")
                                            F1 = Folder1.Replace("1", "LAMLOJ");
                                        else if (_comp == "SINOTRANS")
                                            F1 = Folder1.Replace("1", "SINOTRANS");
                                        else if (_comp == "MLH")
                                            F1 = Folder1.Replace("1", "MLH");


                                        objrptdoc.ExportToDisk(ExportFormatType.PortableDocFormat, Server.MapPath("../" + CurrentFolderName(_comp) + "/RAPORLAR OP/" + F1 + "/" + Folder2 + "/" + Folder3.Replace("1", "AIR").Replace("2", "LAND").Replace("3", "OCEAN") + "/" + SaveName));
                                        if (Request.QueryString["BackPage"] != null)
                                        {

                                            string _createdPrfLoc = Server.MapPath("../" + CurrentFolderName(_comp) + "/RAPORLAR OP/" + F1 + "/" + Folder2 + "/" + Folder3.Replace("1", "AIR").Replace("2", "LAND").Replace("3", "OCEAN") + "/" + SaveName);
                                            string _AppendFile = Server.MapPath("../" + CurrentFolderName(_comp) + "/RAPORLAR OP/Back Pages/"
                                            + Request.QueryString["BackPage"].ToString() + ".pdf");
                                            AddPdf(_createdPrfLoc, _AppendFile);


                                        }
                                        break;
                                    case "MANIFEST":
                                        buttontype = 4;

                                        SaveName = GetReportName(JobNo, LojTaskNo, TaskNo, DocumentCode, VoyageNo) + ".PDF";


                                        CreateDocument(JobNo, TaskNo, LojTaskNo, DocumentCode, VoyageNo, companycode, SaveName, Folder2, MainType, ReportCode,
                                            BlType, BlTypeDesc, HouseBlNo, MasterBlNo, buttontype);

                                        F1 = string.Empty;
                                        if (_comp == "GAC" | _comp == "LAM")
                                            F1 = Folder1 == "1" ? "GAC" : "LAM";
                                        else if (_comp == "LAMLOJ")
                                            F1 = Folder1.Replace("1", "LAMLOJ");
                                        else if (_comp == "SINOTRANS")
                                            F1 = Folder1.Replace("1", "SINOTRANS");
                                        else if (_comp == "MLH")
                                            F1 = Folder1.Replace("1", "MLH");


                                        objrptdoc.ExportToDisk(ExportFormatType.PortableDocFormat, Server.MapPath("../" + CurrentFolderName(_comp) + "/RAPORLAR OP/" + F1 + "/" + Folder2 + "/" + Folder3.Replace("1", "AIR").Replace("2", "LAND").Replace("3", "OCEAN") + "/" + SaveName));

                                        break;
                                    case "DRAFT":
                                        buttontype = 2;

                                        SaveName = GetReportName(JobNo, LojTaskNo, TaskNo, DocumentCode, VoyageNo) + ".PDF";

                                        CreateDocument(JobNo, TaskNo, LojTaskNo, DocumentCode, VoyageNo, companycode, SaveName, Folder2, MainType, ReportCode,
                                            BlType, BlTypeDesc, HouseBlNo, MasterBlNo, buttontype);

                                        F1 = string.Empty;
                                        if (_comp == "GAC" | _comp == "LAM")
                                            F1 = Folder1 == "1" ? "GAC" : "LAM";
                                        else if (_comp == "LAMLOJ")
                                            F1 = Folder1.Replace("1", "LAMLOJ");
                                        else if (_comp == "SINOTRANS")
                                            F1 = Folder1.Replace("1", "SINOTRANS");
                                        else if (_comp == "MLH")
                                            F1 = Folder1.Replace("1", "MLH");


                                        objrptdoc.ExportToDisk(ExportFormatType.PortableDocFormat, Server.MapPath("../" + CurrentFolderName(_comp) + "/RAPORLAR OP/" + F1 + "/" + Folder2 + "/" + Folder3.Replace("1", "AIR").Replace("2", "LAND").Replace("3", "OCEAN") + "/" + SaveName));
                                        if (Request.QueryString["BackPage"] != null)
                                        {

                                            string _createdPrfLoc = Server.MapPath("../" + CurrentFolderName(_comp) + "/RAPORLAR OP/" + F1 + "/" + Folder2 + "/" + Folder3.Replace("1", "AIR").Replace("2", "LAND").Replace("3", "OCEAN") + "/" + SaveName);
                                            string _AppendFile = Server.MapPath("../" + CurrentFolderName(_comp) + "/RAPORLAR OP/Back Pages/"
                                            + Request.QueryString["BackPage"].ToString() + ".pdf");
                                            AddPdf(_createdPrfLoc, _AppendFile);

                                        }
                                        break;
                                }

                                show();


                            }
                            else
                                MessageBox(l.dilgetir(_selectedlang, _comp, _user, "kasagiris", "msg217"));
                        }
                    }
                }

            }
        }

        private string GetPipeString(string IslemId)
        {
            string sorgu = "select top 1 [Primary Key 1] from " + DynamicUtilsV1.dbName(_comp) + "[Web Toplu Islem] where [Islem ID]='" + IslemId + "' ";
            return f.tekbirsonucdonder(sorgu);

        }

        private string GetReportName(string jobNo, int IntTaskNo, string jobTaskNo, string documentCode, string VoyageNo)
        {
            string reportName = string.Empty;

            string[] _params1 = { jobNo, jobTaskNo, documentCode, VoyageNo, "1" };
            string _docvers = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_user, "50014", "DosyaNoAl§" + string.Join("§", _params1), _comp);

            int documentVersion = Convert.ToInt32(_docvers);

            int jobTaskCount = IntTaskNo.ToString().Count();

            if (jobTaskCount == 5)
            {
                reportName = jobNo + "0" + IntTaskNo.ToString().Replace("0000", "") + documentVersion;
            }
            else if (jobTaskCount >= 6)
            {
                reportName = jobNo + IntTaskNo.ToString().Replace("0000", "") + documentVersion;
            }

            return reportName;
        }

        void CreateDocument(string JobNo, string TaskNo, int LojTaskNo, string DocumentCode, string VoyageNo, int companyCode
           , string SaveName, string folder2, string MainType, string reportCode, int BlType, string BlTypeDesc, string HouseBlNo, string MasterBlNo
           , int buttontype)
        {
            string createdTime = DateTime.Now.ToShortTimeString();

            string[] _params1 = { JobNo, TaskNo, DocumentCode, VoyageNo, "1" };
            string _docvers = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_user, "50014", "DosyaNoAl§" + string.Join("§", _params1), _comp);

            int documentVersion = Convert.ToInt32(_docvers);

            string[] _params = {
                "",
                "",
                "",
                null,
                "",
                "",
                "",
                "",
                "",
                "",
                DocumentCode,
                _user,
                companyCode.ToString(),
                folder2,
                getFolderCode(MainType).ToString(),
                reportCode,
                documentVersion.ToString(),
                JobNo,
                TaskNo,
                createdTime,
                "",
                VoyageNo,
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "0",
                "",
                "",
                MasterBlNo,
                HouseBlNo,
                "0",
                "0",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                BlType == 2 ? MasterBlNo : HouseBlNo,
                "",
                "",
                "",
                SaveName,
                LojTaskNo.ToString(),
                "0",
                "",
                "",
                "",
                "",
                "",
                "", buttontype.ToString(), "", "1", "", _user
                , "0", "", BlTypeDesc };

            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_user, "50014", "CreateDocument§" + string.Join("§", _params), _comp);

        }

        protected int getFolderCode(string MainType)
        {
            int code = -1;
            if (MainType.Substring(3, 1) == "1")
                code = 3;
            else if (MainType.Substring(3, 1) == "2")
                code = 1;
            else if (MainType.Substring(3, 1) == "3")
                code = 2;

            return code;
        }

        public void show()
        {
            string _sessionName = setSessionName();


            if (!this.IsPostBack)
            {
                ReportDocument _dynreport = new ReportDocument();
                Session[_sessionName] = _dynreport;
                Session[_sessionName] = null;

            }

            if (Session[_sessionName] != null)
            {
                crycagir();
            }
            else
            {
                Session[_sessionName] = objrptdoc;
                crycagir();
            }


        }
        public void crycagir()
        {
            string _sessionName = setSessionName();


            ReportDocument rd = (ReportDocument)Session[_sessionName];
            CrystalReportViewer1.DisplayGroupTree = false;
            CrystalReportViewer1.DisplayToolbar = true;
            CrystalReportViewer1.EnableDatabaseLogonPrompt = false;
            CrystalReportViewer1.GroupTreeStyle.ShowLines = false;
            CrystalReportViewer1.ToolPanelView = ToolPanelViewType.None;
            CrystalReportViewer1.EnableDrillDown = true;
            CrystalReportViewer1.EnableParameterPrompt = true;
            CrystalReportViewer1.HasCrystalLogo = false;
            CrystalReportViewer1.HasDrillUpButton = true;
            CrystalReportViewer1.HasGotoPageButton = false;
            CrystalReportViewer1.HasPrintButton = true;
            CrystalReportViewer1.HasRefreshButton = true;
            CrystalReportViewer1.HasSearchButton = true;
            CrystalReportViewer1.HasToggleGroupTreeButton = false;
            CrystalReportViewer1.HasToggleParameterPanelButton = false;
            CrystalReportViewer1.HasZoomFactorList = true;
            CrystalReportViewer1.HasExportButton = true;
            CrystalReportViewer1.HyperlinkTarget = "_blank";
            CrystalReportViewer1.PageZoomFactor = 100;
            CrystalReportViewer1.HasDrilldownTabs = true;
            CrystalReportViewer1.ReuseParameterValuesOnRefresh = false;
            CrystalReportViewer1.DisplayPage = true;
            CrystalReportViewer1.ReportSource = rd;

        }

        string setSessionName()
        {

            string _sessionName = string.Empty;
            if (Request.QueryString["IslemId"] != null & Request.QueryString["LOJ"] == null)
            {
                string PipeString = GetPipeString(Request.QueryString["IslemId"].ToString());
                string[] PipeArray = PipeString.Split('|');
                _sessionName = PipeArray[8] + PipeArray[1];

            }
            else
            {
                _sessionName = Request.QueryString["ReportCode"].ToString();
                string _params = f.tekbirsonucdonder("Select [Crystal Session] from [0C_50409_01_REPORT_DIRECTORY] where COMPANY='" + _comp + "' and  ReportCode='" + Request.QueryString["ReportCode"].ToString() + "'");
                if (!string.IsNullOrEmpty(_params))
                {
                    foreach (string str in _params.Split(','))
                    {
                        if (Request.QueryString[str] != null)
                            _sessionName += Request.QueryString[str];
                    }
                }
            }



            return _sessionName;
        }



        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            Web_GenericFunction_PortClient webFonksiyonlari;
            BasicHttpBinding _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            EndpointAddress _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }

        protected void Page_UnLoad(object sender, EventArgs e)
        {
            CrystalReportViewer1.Dispose();
            GC.Collect();
            GC.WaitForPendingFinalizers();

            //if (objrptdoc != null)
            //{
            //    objrptdoc.Close();
            //    objrptdoc.Dispose();
            //}
        }
        protected void CrystalReportViewer1_Unload(object sender, EventArgs e)
        {
            this.CrystalReportViewer1.ReportSource = null;

            CrystalReportViewer1.Dispose();

            //if (objrptdoc != null)
            //{

            //    objrptdoc.Close();

            //    objrptdoc.Dispose();

            //    objrptdoc = null;

            //}

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
    }
}