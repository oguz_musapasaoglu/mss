﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="belgekopyala.aspx.cs" Inherits="MSS1.belgekopyala" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 500px;
        }
    </style>
    <script type="text/javascript">
        function proceed(sipno, tur) {
            opener.location.reload(true);
            if (tur == 1) {
                opener.location = 'iadesatis_sip.aspx?SipNo=' + sipno;
            } else if (tur == 2) {
                opener.location = 'iadesatinalma.aspx?SipNo=' + sipno;
            } else {
                opener.location = 'satis_sip.aspx?SipNo=' + sipno;
            }

            self.close();
        }

    </script>
    <link href="css/tools.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <table style="border-style:solid;border-width:thick;width:100%" >
        <tr>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr>
                   <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
        <tr>
                   <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
        </tr>
    </table>



    <br />
    <form id="form1" runat="server">
    <h2>
        <asp:Label ID="lblBaslik" runat="server"></asp:Label>
        </h2>
    <div>
        <table class="style1" cellspacing="10">
            <tr>
                <td>
                    <asp:Label ID="lblBelgeNoB" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblbelgeno" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblBelgeSecB" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox CssClass="textbox" Width="300px" ID="txtbelge" runat="server" Height="21px"
                        MaxLength="20" AutoPostBack="True" OnTextChanged="txtbelge_TextChanged"></asp:TextBox>
                    <cc1:AutoCompleteExtender ServiceMethod="belgegetir" UseContextKey="true" MinimumPrefixLength="1"
                        CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtbelge"
                        ID="autobelge" runat="server" FirstRowSelected="false" CompletionListCssClass="listMain"
                        CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected">
                    </cc1:AutoCompleteExtender>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptLocalization="true"
                        EnableScriptGlobalization="true" EnablePageMethods="true">
                    </asp:ScriptManager>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblsipno" runat="server" Visible="False"></asp:Label>
                </td>
                <td>
                    <asp:Button ID="btnkopyala" runat="server" CssClass="myButton" OnClick="btnkopyala_Click" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
