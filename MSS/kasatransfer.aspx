﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="kasatransfer.aspx.cs" Inherits="MSS1.kasatransfer" %>

<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style1 {
            width: 900px;
            table-layout: fixed;
        }
    </style>
    <script>
        function proceed() {
            opener.location.reload(true);
            opener.location = 'kasatransfer.aspx';
            self.close();
        }

        function checkTextAreaMaxLength(textBox, e, length) {

            var mLen = textBox["MaxLength"];
            if (null == mLen)
                mLen = length;

            var maxLength = parseInt(mLen);
            if (!checkSpecialKeys(e)) {
                if (textBox.value.length > maxLength - 1) {
                    if (window.event)//IE
                    {
                        e.returnValue = false;
                        return false;
                    }
                    else//Firefox
                        e.preventDefault();
                }
            }
        }

        function checkSpecialKeys(e) {
            if (e.keyCode != 8 && e.keyCode != 46 && e.keyCode != 35 && e.keyCode != 36 && e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40)
                return false;
            else
                return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <h1>KASA TRANSFER</h1>
    <br />
    <table class="style1">
        <tr>
            <td>PARA BİRİMİ
            </td>
            <td>TESLİM EDİLEN KİŞİ
            </td>
            <td>TRANSFER TUTARI
            </td>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList ID="ddlcurrency" runat="server" AppendDataBoundItems="True" CssClass="textbox"
                    Width="120px" AutoPostBack="True" OnSelectedIndexChanged="ddlcurrency_SelectedIndexChanged">
                    <asp:ListItem Selected="True" Text="" Value=""> </asp:ListItem>
                    <asp:ListItem Text="USD" Value="USD"> </asp:ListItem>
                    <asp:ListItem Text="EURO" Value="EURO"> </asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="txtteslimkisi" runat="server" Width="200px" onkeyDown="return checkTextAreaMaxLength(this,event,'100');"
                    CssClass="textbox" AutoPostBack="True" OnTextChanged="txtteslimkisi_TextChanged"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtTutar" CssClass="textbox" runat="server" AutoPostBack="True"
                    OnTextChanged="txtTutar_TextChanged"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="btnOnizle" runat="server" CssClass="myButtonV" Text="Ön İzleme" OnClick="btnOnizle_Click" />
                <asp:Button ID="btnTransfer" runat="server" CssClass="myButtonV" Text="Transfer Et"
                    OnClick="btnTransfer_Click" Width="112px" Visible="False" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblbelgeno" runat="server" Visible="False"></asp:Label>
            </td>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
        </tr>
    </table>
    <br />
    <br />
    <dx:ASPxGridView ID="MusteriGrid" ClientInstanceName="MusteriGrid" Caption="" runat="server" AutoGenerateColumns="true" EnableRowsCache="false"
        Theme="iOS" EnableTheming="false" KeyFieldName="documentno" Width="100%">
        <Settings ShowFilterRow="true" ShowGroupPanel="true" VerticalScrollBarMode="Visible" VerticalScrollableHeight="600" />
        <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" AllowFocusedRow="false" />
        <SettingsEditing Mode="EditForm" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
        <SettingsPager Mode="EndlessPaging" PageSize="30" ShowSeparators="True"></SettingsPager>
        <SettingsText EmptyDataRow=" " />
        <SettingsCommandButton>
            <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
            </CancelButton>
            <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
            </UpdateButton>
        </SettingsCommandButton>
        <SettingsCookies Enabled="true" />
        <SettingsEditing Mode="EditForm" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
        <Styles>


            <Header ForeColor="White"></Header>
            <HeaderPanel ForeColor="White"></HeaderPanel>
            <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
        </Styles>
        <SettingsCookies StoreColumnsVisiblePosition="false" StoreColumnsWidth="false" StoreControlWidth="false" StoreFiltering="false"
            StoreGroupingAndSorting="false" StorePaging="false" StoreSearchPanelFiltering="false" Enabled="false" />
    </dx:ASPxGridView>
    <%--  <asp:GridView ID="MusteriGrid" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" BorderStyle="None" CellPadding="0" CssClass="mGrid GridViewStyle"
        EmptyDataText="Herhangi bir muhasebe hareketi bulunamadı." Font-Bold="False"
        Font-Size="Small" GridLines="Horizontal" OnPageIndexChanging="MusteriGrid_PageIndexChanging"
        PagerSettings-Mode="NumericFirstLast" PageSize="30" Width="700px">
        <RowStyle HorizontalAlign="left" />
        <PagerSettings Mode="NumericFirstLast" />
        <PagerStyle CssClass="pgr" ForeColor="White" HorizontalAlign="Left" />
        <Columns>
            <asp:BoundField DataField="postingdate" HeaderText="Nakil Tarihi" />
            <asp:BoundField DataField="documentno" HeaderText="Belge No" />
            <asp:BoundField DataField="amount" HeaderText="Tutar">
                <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
            <asp:BoundField DataField="currency" HeaderText="Döviz" />
            <asp:BoundField DataField="aciklama" HeaderText="Açıklama" />
            <asp:BoundField DataField="olusturankullanici" HeaderText="Oluşturan Kullanıcı" />
            <asp:BoundField DataField="olusturmatarihi" HeaderText="Oluşturma Tarihi" />
        </Columns>
    </asp:GridView>--%>
</asp:Content>
