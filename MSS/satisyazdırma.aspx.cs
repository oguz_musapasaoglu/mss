﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.ServiceModel;
using MSS1.WSGeneric;
using MSS1.Codes;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace MSS1
{
    public partial class satisyazdırma : Bases
    {
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        SqlConnection baglanti;
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        SessionBase _obj;

        public ws_baglantı ws = new ws_baglantı();
        public int Onizleme = 0;

        #region Dil Getir
        public void dilgetir()
        {
            lblFtTuru1.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "lblFtTuru1");
            lblFtTuru2.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "lblFtTuru2");
            lblTopFtMetni1.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "lblTopFtMetni1");
            lblTopFtMetni2.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "lblTopFtMetni2");
            lblTopFtMetni3.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "lblTopFtMetni3");
            lblAciklama1.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "lblAciklama1");
            lblAciklama2.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "lblAciklama2");
            lblAciklama3.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "lblAciklama3");
            lblFaturaEkMetinB.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "lblFaturaEkMetinB");
            lblFaturaEkMetin2B.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "lblFaturaEkMetin2B");
            lblFtTarihiB.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "lblFtTarihiB");
            lblticari.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "lblticari");
            lblHaBeNoB.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "lblHaBeNoB");
            lblLimanB.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "lblLimanB");
            lblTanimB.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "lblTanimB");
            lblTasiyiciB.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "lblTasiyiciB");
            lblKonsB.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "lblKonsB");
            lblKapB.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "lblKapB");
            lblKargoB.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "lblKargoB");
            lblBirimB.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "lblBirimB");
            btnönizleme.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "btnönizleme");
            btnearsivonizleme.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "btnönizleme");
            btnyazdır.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "btnyazdır");
            lblSeriNoB.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "lblSeriNoB");
            btneFaturaYazdir.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "btneFaturaYazdir");
        }
        #endregion

        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            dilgetir();
            if (!IsPostBack)
            {
                txtfaturatarihi.Text = Request.QueryString["Tarih"];
                ticariunvankontrol(_obj.Comp, Request.QueryString["BelgeNo"].ToString());
                if (kagitfaturakontrol(Request.QueryString["BelgeNo"].ToString()))
                {
                    btnearsiv.Visible = false;
                    btnyazdır.Visible = true;
                    btneFaturaYazdir.Visible = false;
                    btnearsivonizleme.Visible = false;
                    btnönizleme.Visible = true;
                }
                else
                {
                    efaturakontrol(_obj.Comp, Request.QueryString["BelgeNo"].ToString());
                }

                FillBLInfos();
                FillForm();

                string sorgu = "Exec sp_0WEB_LOOKUPS_FATURA_TURU '" + _obj.Comp + "'";
                ddlft1.Items.Clear();
                DSFt.SelectCommand = sorgu;
                ddlft1.DataBind();
                ddlft1.SelectedIndex = 0;
            }

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }
        void FillBLInfos()
        {
          

            string BlinfoQuery = "SELECT * FROM [GAC_NAV2].[dbo].[0D_00036_60_SALES HEADER_INFO] where COMPANY='" + _obj.Comp + "' and [Document No_]='" + Request.QueryString["BelgeNo"] + "'";
            baglanti = new SqlConnection(strConnString);
            baglanti.Open();
            SqlCommand sorgu = new SqlCommand(BlinfoQuery, baglanti);
            SqlDataAdapter sorguDA = new SqlDataAdapter(sorgu);
            SqlDataReader sqr;
            sqr = sorgu.ExecuteReader();
            if (sqr.Read())
            {
                if (sqr["SEFER"].ToString() != "" & sqr["GEMI"].ToString() != "")
                    txtseferno.Text = sqr["GEMI"].ToString() + " / " + sqr["SEFER"].ToString();
                else
                    txtseferno.Text = sqr["SEFER"].ToString();

                txtawb.Text = sqr["KONSIMENTO"].ToString();
                if (sqr["KAPNEVI"].ToString() != "" & sqr["KAP"].ToString() != "")
                    txtadet.Text = sqr["KAPNEVI"].ToString() + " / " + sqr["KAP"].ToString();
                else
                    txtadet.Text = sqr["KAPNEVI"].ToString();

                txtagırlık.Text = sqr["KARGOAGIRLIK"].ToString();
                txtbirim.Text = sqr["BIRIM"].ToString();
            }
            sqr.Close();

        }
        private void FillForm()
        {
            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select * from [0D_00036_00_SALES HEADER] where COMPANY=@Company and No_=@BelgeNo";
                    cmd.Connection = conn;
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@Company", _obj.Comp);
                    cmd.Parameters.AddWithValue("@BelgeNo", Request.QueryString["BelgeNo"]);
                    using (DataTable dt = new DataTable())
                    {
                        using (SqlDataAdapter adpt = new SqlDataAdapter())
                        {
                            adpt.SelectCommand = cmd;
                            adpt.Fill(dt);

                            if (dt.Rows.Count > 0)
                            {
                                txtliman.Text = dt.Rows[0]["Liman"].ToString();
                                // txtseferno.Text = dt.Rows[0]["Taşıyıcı Adı _ Sefer No"].ToString();

                                if (Convert.ToInt32(dt.Rows[0]["EF_Kaydi"]) == 1 & Convert.ToInt32(dt.Rows[0]["EF_Temel"]) == 1)
                                {
                                    txtSeriNo.Enabled = false;
                                    txthbno.Enabled = false;
                                }
                            }

                        }
                    }

                }
            }
            // txtliman.Text = f.tekbirsonucdonder("select  Liman   from [0D_00036_00_SALES HEADER] where COMPANY='" + _obj.Comp + "' and No_='" + Request.QueryString["BelgeNo"] + "'");
            //txtliman.Enabled = false;
            //txtseferno.Text = f.tekbirsonucdonder("select  [Taşıyıcı Adı _ Sefer No]   from [0D_00036_00_SALES HEADER] where COMPANY='" + _obj.Comp + "' and No_='" + Request.QueryString["BelgeNo"] + "'");
            //txtseferno.Enabled = false;
        }

        private void efaturakontrol(string sirket, string sipno)
        {
            string efaturamuk = f.tekbirsonucdonder("select SH.EF_Kaydi from [0D_00036_00_SALES HEADER] SH where COMPANY='" + _obj.Comp + "' and  SH.No_='" + sipno + "'");

            if (efaturamuk == "0" || efaturamuk == "NULL" || efaturamuk == string.Empty)
            {
                string earsiv = f.tekbirsonucdonder("select [E-Arsiv] from [0C_70043_00_COMP_INFO2] where COMPANY='" + _obj.Comp + "'");
                if (earsiv == "0" || earsiv == "NULL" || earsiv == string.Empty)
                {
                    btnearsiv.Visible = false;
                    btnyazdır.Visible = true;
                    btneFaturaYazdir.Visible = false;
                }
                else
                {
                    btnearsiv.Visible = true;
                    btnyazdır.Visible = false;
                    btneFaturaYazdir.Visible = false;
                    btnönizleme.Visible = false;
                    btnearsivonizleme.Visible = true;
                }
            }
            else
            {

                btnyazdır.Visible = false;
                btneFaturaYazdir.Visible = true;
            }

        }
        private void ticariunvankontrol(string sirket, string sipno)
        {
            baglanti = new SqlConnection(strConnString);
            baglanti.Open();
            SqlCommand sorgu = new SqlCommand("select C.[Fatura Ticari Ünvan] as ticari,C.[Ticari Unvan] as ticariicerik from [0C_00018_02_CUSTOMER] C where COMPANY='" + _obj.Comp + "' and C.No_= (select SH.[Bill-to Customer No_] from [0D_00036_00_SALES HEADER] SH WHERE SH.COMPANY='" + sirket + "' and SH.No_='" + sipno + "')", baglanti);
            SqlDataAdapter sorguDA = new SqlDataAdapter(sorgu);
            SqlDataReader sqr;
            sqr = sorgu.ExecuteReader();
            if (sqr.Read())
            {
                txtticariunvan.Enabled = Convert.ToBoolean(sqr["ticari"]);
                if (txtticariunvan.Enabled == true)
                {
                    txtticariunvan.Text = sqr["ticariicerik"].ToString();
                }

            }
            sqr.Close();
        }
        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        public Boolean kagitfaturakontrol(string faturano)
        {
            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                Boolean _result = false;
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[SP_0WEB_KAGIT_FATURA_KONTROL]";
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@PRM1", _obj.Comp);
                    cmd.Parameters.AddWithValue("@PRM2", _obj.UserName);
                    cmd.Parameters.AddWithValue("@PRM3", faturano);

                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        _result = true;
                    }

                }
                conn.Close();
                return _result;
            }
        }
        protected void btnyazdır_Click(object sender, EventArgs e)
        {
            string No_ = string.Empty;
            No_ = f.tekbirsonucdonder("select No_ from [0D_00036_00_SALES HEADER] where No_='" + Request.QueryString["BelgeNo"] + "'");
            if (string.IsNullOrEmpty(No_))
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "msg001"));
                return;
            }

            if (ddlft1.SelectedValue != string.Empty && ddlft2.SelectedValue != string.Empty && txthbno.Text != string.Empty && txtfaturatarihi.Text != string.Empty && txtSeriNo.Text != string.Empty)
            {
                string[] tcevir = txtfaturatarihi.Text.Split('.');

                string[] _params = { txthbno.Text };
                string drm = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatisHariciKontrol§" + string.Join("§", _params), _obj.Comp);

                if (drm == "True")
                {
                    
                    string[] ticaridizi = new string[4];
                    int uzunluk = txtticariunvan.Text.Length - 50;
                    if (txtticariunvan.Text.Length <= 50)
                    {
                        ticaridizi[0] = txtticariunvan.Text.Substring(0, txtticariunvan.Text.Length);
                        ticaridizi[1] = "";
                    }
                    else
                    {
                        ticaridizi[0] = txtticariunvan.Text.Substring(0, 50);
                        ticaridizi[1] = txtticariunvan.Text.Substring(50, txtticariunvan.Text.Length - 50);
                    }

                    string[] _params2 = { Request.QueryString["BelgeNo"], txthbno.Text, txtliman.Text, txttanim.Text, txtseferno.Text,
                        txtawb.Text, txtadet.Text, txtagırlık.Text, txtbirim.Text,
                        ddlft2.SelectedValue, tcevir[1] + "." + tcevir[0] + "." + tcevir[2],  ddlft1.SelectedValue , txtftmetni1.Text,
                        txtftmetni2.Text, txtftmetni3.Text, txtgrup1a.Text, txtgrup2a.Text, txtgrup3a.Text, ticaridizi[0], ticaridizi[1], txtSeriNo.Text };
                    GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatisSipYazdır§" + string.Join("§", _params2), _obj.Comp);

                    ReportDocument crystalReport;
                    fonksiyonlar f = new fonksiyonlar();

                    crystalReport = new ReportDocument();
                    string reportPath = f.GetReportPath("INVOICETUM GRUP.rpt", _obj.Comp);
                    crystalReport.Load(reportPath);

                    crystalReport.ReportOptions.EnableSaveDataWithReport = false;
                    crystalReport.SetParameterValue("@DOCNO", Request.QueryString["BelgeNo"]);
                    crystalReport.SetParameterValue("@COMP", _obj.Comp);
                    crystalReport.SetParameterValue("DETAY TOPLU", ddlft1.SelectedValue);
                    crystalReport.SetParameterValue("TOPLU FATURA METNİ", txtftmetni1.Text);
                    crystalReport.SetParameterValue("NORMALPROFORMA", ddlft2.SelectedValue);
                    crystalReport.SetParameterValue("EK METİN", txtftekmetin.Text);
                    crystalReport.SetParameterValue("EK METİN 2", txtftekmetin2.Text);
                    crystalReport.SetParameterValue("ÖN İZLEME", 1);
                    crystalReport.ExportToDisk(ExportFormatType.PortableDocFormat, Server.MapPath(CurrentFolderName(_obj.Comp) + "/PDF/" + Request.QueryString["BelgeNo"] + ".pdf"));
                    ClientScript.RegisterStartupScript(this.Page.GetType(), "popupOpener", "var popup=window.open('" + CurrentFolderName(_obj.Comp) + "/PDF/" + Request.QueryString["BelgeNo"] + ".pdf');popup.focus();", true);

                    if (crystalReport != null)
                    {

                        crystalReport.Close();

                        crystalReport.Dispose();

                        crystalReport = null;

                    }
                    if (ddlft2.SelectedIndex == 1)
                    {

                        string[] _params3 = { Request.QueryString["BelgeNo"], _obj.NameSurName };
                        GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatisDefNaklet§" + string.Join("§", _params3), _obj.Comp);


                        if (!ClientScript.IsStartupScriptRegistered("JSFaturaAcik"))
                        {
                            ClientScript.RegisterStartupScript(GetType(), "JSFaturaAcik", @"window.opener.parent.RefreshAllCollapse();self.close();", true);
                        }
                        // ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: proceed();", true);
                    }
                    else
                    {
                        MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "msg426"));
                    }

                }
                else
                {
                    MessageBox(txthbno.Text + l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "msg427"));
                }

            }
            else
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "msg428"));
            }


        }
        protected void btnönizleme_Click(object sender, EventArgs e)
        {
            Onizleme = 1;
            string emukellefmi = f.tekbirsonucdonder("SELECT EF_Kaydi FROM  [0D_00036_00_SALES HEADER] where COMPANY='" + _obj.Comp + "' and No_='" + Request.QueryString["BelgeNo"].ToString() + "'");


            if (ddlft1.SelectedValue != string.Empty && ddlft2.SelectedValue != string.Empty && txtfaturatarihi.Text != string.Empty && txtSeriNo.Text != string.Empty)
            {
                string[] ticaridizi = new string[4];
                int uzunluk = txtticariunvan.Text.Length - 50;
                if (txtticariunvan.Text.Length <= 50)
                {
                    ticaridizi[0] = txtticariunvan.Text.Substring(0, txtticariunvan.Text.Length);
                    ticaridizi[1] = "";
                }
                else
                {
                    ticaridizi[0] = txtticariunvan.Text.Substring(0, 50);
                    ticaridizi[1] = txtticariunvan.Text.Substring(50, txtticariunvan.Text.Length - 50);
                }


                string[] tcevir = txtfaturatarihi.Text.Split('.');

                string[] _params = { Request.QueryString["BelgeNo"], txthbno.Text, txtliman.Text, txttanim.Text, txtseferno.Text, txtawb.Text, txtadet.Text,
                    txtagırlık.Text, txtbirim.Text, ddlft2.SelectedValue, tcevir[1] + "." + tcevir[0] + "." + tcevir[2],
                    ddlft1.SelectedValue, txtftmetni1.Text, txtftmetni2.Text, txtftmetni3.Text, txtgrup1a.Text, txtgrup2a.Text, txtgrup3a.Text,
                    ticaridizi[0], ticaridizi[1], txtSeriNo.Text };
                GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatisSipYazdır§" + string.Join("§", _params), _obj.Comp);
                this.ClientScript.RegisterClientScriptBlock(this.GetType(), "popupOpener", "var popup=window.open('Raporlar/Default.aspx?BelgeNo=" + Request.QueryString["BelgeNo"] + "&FT1=" + ddlft1.SelectedValue + "&FT2=" + ddlft2.SelectedValue + "&FTM=" + txtftmetni1.Text + "&FTEM=" + txtftekmetin.Text + "&FTEM2=" + txtftekmetin2.Text + "&GId=" + Request.QueryString["GId"] + "');popup.focus();", true);
            }
            else
            {
                if (emukellefmi == "1")
                {
                    string[] ticaridizi = new string[4];
                    int uzunluk = txtticariunvan.Text.Length - 50;
                    if (txtticariunvan.Text.Length <= 50)
                    {
                        ticaridizi[0] = txtticariunvan.Text.Substring(0, txtticariunvan.Text.Length);
                        ticaridizi[1] = "";
                    }
                    else
                    {
                        ticaridizi[0] = txtticariunvan.Text.Substring(0, 50);
                        ticaridizi[1] = txtticariunvan.Text.Substring(50, txtticariunvan.Text.Length - 50);
                    }
                    string[] tcevir = txtfaturatarihi.Text.Split('.');

                    string[] _params = { Request.QueryString["BelgeNo"], txthbno.Text, txtliman.Text, txttanim.Text, txtseferno.Text, txtawb.Text, txtadet.Text,
                        txtagırlık.Text, txtbirim.Text, ddlft2.SelectedValue, tcevir[1] + "." + tcevir[0] + "." + tcevir[2],
                        ddlft1.SelectedValue, txtftmetni1.Text, txtftmetni2.Text, txtftmetni3.Text, txtgrup1a.Text, txtgrup2a.Text,
                        txtgrup3a.Text, ticaridizi[0], ticaridizi[1], txtSeriNo.Text };
                    GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatisSipYazdır§" + string.Join("§", _params), _obj.Comp);
                    this.ClientScript.RegisterClientScriptBlock(this.GetType(), "popupOpener", "var popup=window.open('Raporlar/Default.aspx?BelgeNo=" + Request.QueryString["BelgeNo"] + "&FT1=" + ddlft1.SelectedValue + "&FT2=" + ddlft2.SelectedValue + "&FTM=" + txtftmetni1.Text + "&FTEM=" + txtftekmetin.Text + "&FTEM2=" + txtftekmetin2.Text + "&GId=" + Request.QueryString["GId"] + "');popup.focus();", true);
                }
                else
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "satisyazdırma", "msg429"));
                }

            }
            Onizleme = 0;

        }
        protected void ddlft1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlft1.SelectedIndex == 2)
            {
                txtftmetni1.Enabled = true;
                txtftmetni2.Enabled = true;
                txtftmetni3.Enabled = true;
            }
            else
            {
                txtftmetni1.Enabled = false;
                txtftmetni2.Enabled = false;
                txtftmetni3.Enabled = false;
            }
            if (ddlft1.SelectedIndex == 4)
            {
                txtgrup1a.Enabled = true;
                txtgrup2a.Enabled = true;
                txtgrup3a.Enabled = true;
            }
            else
            {
                txtgrup1a.Enabled = false;
                txtgrup2a.Enabled = false;
                txtgrup3a.Enabled = false;
            }
        }
        protected void ddlft2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlft2.SelectedValue == "2")
            {
                string[] _params = { Request.QueryString["BelgeNo"] };
                GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "ProformaNoGetir§" + string.Join("§", _params), _obj.Comp);

                txthbno.Enabled = false;
            }
            else
            {
                txthbno.Text = string.Empty;
                txthbno.Enabled = true;
            }
        }
        protected void btneFaturaYazdir_Click(object sender, EventArgs e)
        {
            string ftmetni1 = "";
            string ftmetni2 = "";
            string ftmetni3 = "";
            if (ddlft1.SelectedValue == "2")
            {
                ftmetni1 = txtftmetni1.Text;
                ftmetni2 = txtftmetni2.Text;
                ftmetni3 = txtftmetni3.Text;
            }

            string[] _params = {
                                Request.QueryString["BelgeNo"].ToString(),
                                txtliman.Text,
                                txttanim.Text,
                                txtseferno.Text,
                                txtawb.Text,
                                txtadet.Text,
                                txtagırlık.Text,
                                txtbirim.Text,
                                _obj.NameSurName, txtftekmetin.Text, txtftekmetin2.Text, ddlft1.SelectedValue ,
                                ftmetni1, ftmetni2, ftmetni3 };

            string message = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "EF_GidenBaslik§" + string.Join("§", _params), _obj.Comp);



            MessageBox(message);
        }

        protected void btnearsiv_Click(object sender, EventArgs e)
        {
            string ftmetni1 = "";
            string ftmetni2 = "";
            string ftmetni3 = "";
            if (ddlft1.SelectedValue == "2")
            {
                ftmetni1 = txtftmetni1.Text;
                ftmetni2 = txtftmetni2.Text;
                ftmetni3 = txtftmetni3.Text;
            }

            string[] _params = {
                                Request.QueryString["BelgeNo"].ToString(),
                                txtliman.Text,
                                txttanim.Text,
                                txtseferno.Text,
                                txtawb.Text,
                                txtadet.Text,
                                txtagırlık.Text,
                                txtbirim.Text,
                                _obj.NameSurName, txtftekmetin.Text, txtftekmetin2.Text, ddlft1.SelectedValue ,
                                ftmetni1, ftmetni2, ftmetni3 };

            string message = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "EA_GidenBaslik§" + string.Join("§", _params), _obj.Comp);



            MessageBox(message);
        }

        protected void btnearsivonizleme_Click(object sender, EventArgs e)
        {
            string[] ticaridizi = new string[4];
            int uzunluk = txtticariunvan.Text.Length - 50;
            if (txtticariunvan.Text.Length <= 50)
            {
                ticaridizi[0] = txtticariunvan.Text.Substring(0, txtticariunvan.Text.Length);
                ticaridizi[1] = "";
            }
            else
            {
                ticaridizi[0] = txtticariunvan.Text.Substring(0, 50);
                ticaridizi[1] = txtticariunvan.Text.Substring(50, txtticariunvan.Text.Length - 50);
            }
            string[] tcevir = txtfaturatarihi.Text.Split('.');

            string[] _params = { Request.QueryString["BelgeNo"], txthbno.Text, txtliman.Text, txttanim.Text, txtseferno.Text, txtawb.Text, txtadet.Text,
                        txtagırlık.Text, txtbirim.Text, ddlft2.SelectedValue, tcevir[1] + "." + tcevir[0] + "." + tcevir[2],
                        ddlft1.SelectedValue, txtftmetni1.Text, txtftmetni2.Text, txtftmetni3.Text, txtgrup1a.Text, txtgrup2a.Text,
                        txtgrup3a.Text, ticaridizi[0], ticaridizi[1], txtSeriNo.Text };
            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SatisSipYazdır§" + string.Join("§", _params), _obj.Comp);
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "popupOpener", "var popup=window.open('Raporlar/Default.aspx?BelgeNo=" +
                Request.QueryString["BelgeNo"] + "&FT1=" + ddlft1.SelectedValue + "&FT2=" + ddlft2.SelectedValue + "&FTM=" +
                txtftmetni1.Text + "&FTEM=" + txtftekmetin.Text + "&FTEM2=" + txtftekmetin2.Text + "&GId=" + Request.QueryString["GId"] + "');popup.focus();", true);
        }
    }
}