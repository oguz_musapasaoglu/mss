﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.WSGeneric;
using System.ServiceModel;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using MSS1.Codes;

namespace MSS1
{
    public partial class shipSec : Bases
    {
        static SessionBase _obj;

        #region Connection String & Service
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar(); 
        public ws_baglantı ws = new ws_baglantı();

        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;

        [System.Web.Services.WebMethod]
        [System.Web.Script.Services.ScriptMethod]
        public static List<string> ShipAra(string prefixText, int count)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_ship_goster";
                    cmd.Parameters.AddWithValue("@ad", prefixText.ToUpper());
                    cmd.Parameters.AddWithValue("@sirket", _obj.Comp);
                    cmd.Connection = conn;
                    conn.Open();
                    List<string> Ship = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Ship.Add(sdr["ShipName"].ToString());
                        }
                    }
                    conn.Close();
                    return Ship;
                }
            }
        }
        #endregion

        public void dilgetir()
        {
            Language l = new Language();
            lblBaslik.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "shipSec", "lblBaslik");
            lblShipName.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "shipSec", "lblShipName");
            btnAra.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "shipSec", "btnAra");
            lblPasif.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "shipOlustur", "lblPasif");
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (IsPostBack) return;
            dilgetir();
            fillcombox();
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }

        void fillcombox()
        {
            using (SqlConnection baglanti = new SqlConnection(strConnString))
            {
                baglanti.Open();
                SqlCommand sorgu = new SqlCommand();

                sorgu.Connection = baglanti;
                sorgu.CommandText = "Select Code,Name From [0C_00009_00_COUNTRY] where COMPANY='" + _obj.Comp + "' Order By Name";

                SqlDataAdapter sorguDA = new SqlDataAdapter(sorgu);
                DataTable sorgugridDT = new DataTable();
                sorguDA.Fill(sorgugridDT);

                ddlFlag.DataTextField = "Name";
                ddlFlag.DataValueField = "Code";
                ddlFlag.DataSource = sorgugridDT;
                ddlFlag.DataBind();
                ddlFlag.SelectedValue = "-1";

            }
        }

        private void shipBul(string sirket, string imono, string flagId, string ad, GridView gv)
        {
            using (SqlConnection baglanti = new SqlConnection(strConnString))
            {
                baglanti.Open();
                SqlCommand sorgu = new SqlCommand();
                sorgu.Connection = baglanti;
                sorgu.CommandType = CommandType.StoredProcedure;
                sorgu.CommandText = "sp_0WEB_LUCENT_GEMI_ARA";
                SqlParameter pSirket = new SqlParameter("@sirket", SqlDbType.NVarChar, 30);
                pSirket.Value = sirket;
                sorgu.Parameters.Add(pSirket);
                SqlParameter pAd = new SqlParameter("@ad", SqlDbType.NVarChar, 30);
                pAd.Value = ad;
                sorgu.Parameters.Add(pAd);
                SqlParameter pimono = new SqlParameter("@imono", SqlDbType.NVarChar, 30);
                pimono.Value = imono;
                sorgu.Parameters.Add(pimono);
                SqlParameter pflag = new SqlParameter("@flag", SqlDbType.NVarChar, 30);
                pflag.Value = flagId;
                sorgu.Parameters.Add(pflag);
                SqlParameter pPasif = new SqlParameter("@Pasif", SqlDbType.TinyInt);
                pPasif.Value = chkPasif.Checked ? 1 : 0;
                sorgu.Parameters.Add(pPasif);
                SqlDataAdapter sorguDA = new SqlDataAdapter(sorgu);
                DataTable sorgugridDT = new DataTable();
                sorguDA.Fill(sorgugridDT);
                gv.DataSource = sorgugridDT;
                gv.DataBind();
                gv.Visible = true;
                SqlDataReader sqr;
                sqr = sorgu.ExecuteReader();
                sqr.Read();
                sqr.Close();
                sqr = null;
            }

        }
        protected void gvListe_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            shipBul(_obj.Comp, txtImoNo.Text, ddlFlag.SelectedValue, txtShipName.Text, gvListe);
            gvListe.DataSource = SortDataTable(gvListe.DataSource as DataTable, true);
            gvListe.PageIndex = e.NewPageIndex;
            gvListe.DataBind();
        }
        protected DataView SortDataTable(DataTable dtable, bool isPageIndexChanging)
        {
            if (dtable != null)
            {
                DataView dview = new DataView(dtable);
                if (GridViewSortExpression != string.Empty)
                {
                    if (isPageIndexChanging)
                    {
                        dview.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                    }
                    else
                    {
                        dview.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                    }
                }
                return dview;
            }
            else
            {
                return new DataView();
            }
        }
        private string GridViewSortExpression
        {

            get { return ViewState["SortExpression"] as string ?? string.Empty; }

            set { ViewState["SortExpression"] = value; }

        }
        private string GridViewSortDirection
        {

            get { return ViewState["SortDirection"] as string ?? "ASC"; }

            set { ViewState["SortDirection"] = value; }

        }
        private string GetSortDirection()
        {

            switch (GridViewSortDirection)
            {

                case "ASC":

                    GridViewSortDirection = "DESC";

                    break;

                case "DESC":

                    GridViewSortDirection = "ASC";

                    break;
            }

            return GridViewSortDirection;

        }
        protected void btnAra_Click(object sender, EventArgs e)
        {
            shipBul(_obj.Comp, txtImoNo.Text, ddlFlag.SelectedValue, txtShipName.Text, gvListe);
        }
        protected void gvListe_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lbl_id = (Label)e.Row.FindControl("lbl_id");
                e.Row.Style.Add(HtmlTextWriterStyle.Cursor, "pointer");
                e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='DeepSkyBlue'");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='transparent'");
                e.Row.Attributes.Add("onclick", "return kontrol('" + lbl_id.Text + "')");
            }
        }
        protected void txtShipName_TextChanged(object sender, EventArgs e)
        {
            txtShipName.Text = txtShipName.Text.Replace("ı", "i");
            txtShipName.Text = txtShipName.Text.Replace("ğ", "g");
            txtShipName.Text = txtShipName.Text.Replace("ü", "u");
            txtShipName.Text = txtShipName.Text.Replace("ş", "s");
            txtShipName.Text = txtShipName.Text.Replace("ö", "o");
            txtShipName.Text = txtShipName.Text.Replace("ç", "c");
            txtShipName.Text = txtShipName.Text.Replace("İ", "I");
            txtShipName.Text = txtShipName.Text.Replace("Ğ", "G");
            txtShipName.Text = txtShipName.Text.Replace("Ü", "U");
            txtShipName.Text = txtShipName.Text.Replace("Ş", "S");
            txtShipName.Text = txtShipName.Text.Replace("Ö", "O");
            txtShipName.Text = txtShipName.Text.Replace("Ç", "C");
        }
    }
}