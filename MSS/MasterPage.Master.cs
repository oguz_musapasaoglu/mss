﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.HtmlControls;
using MSS1.Codes;
using MSS1.WSGeneric;
using System.ServiceModel;

namespace MSS1
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        fonksiyonlar f = new fonksiyonlar();
        Language l = new Language();
        ws_baglantı ws = new ws_baglantı();
        SessionBase _obj;
        Guid _Guid = new Guid();


        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _obj = TabSessions.ControlState(this.Page, Request.QueryString["GId"].ToString());
            if (Session["changePass"] != null)
            {
                if (Session["changePass"].ToString() == "1")
                {
                    Response.Redirect("sifre.aspx?GId=" + Request.QueryString["GId"].ToString());
                }
            }
            Page.Title = _obj.Comp + " PORTAL";

            if (Request.Url.ToString().Contains("ONMG=1"))
            {
                tblUstMenu.Attributes.Add("class", "hiddenpopup");
                divSpecial.Attributes.Add("class", "hiddenpopup");
            }

            if (Page.IsPostBack | Page.IsCallback) return;

            LoadMenu();
            LoadForm();
        }

        void LoadForm()
        {
            if (Session["Sirket"] == null)
                Response.Redirect("~/Default.aspx");

            FillLang();
            FillComp();
            if (string.IsNullOrEmpty(_obj.SelectedLangVal))
                cmbLangNew.SelectedValue = f.tekbirsonucdonder("select [LineNo] from [0_0000002] where [Kullanıcı Adı]='" + _obj.UserName + "' and COMPANY='" + _obj.Comp + "'");
            else
            {
                if (!string.IsNullOrEmpty(_obj.SelectedLangVal))
                {
                    cmbLangNew.SelectedValue = _obj.SelectedLangVal;
                }
            }


            Session["SelectedLanguage"] = cmbLangNew.SelectedItem.Text;
            _obj.SelectedLanguage = cmbLangNew.SelectedItem.Text;

            lblMesaj.Text = Session["kulad"].ToString();


            LoadReportMenu(_obj.UserName);
            //LoadTopMenu();
            //menusecili();
            //menuolustur();

            if (fonksiyonlar.evrakkayitaltlogo == false)
            {
                lblMesaj.Visible = false;
            }

            if (_obj.Comp == "LAM")
            {
                string DepBoyut = f.tekbirsonucdonder("select LEFT([DEPT],1) from [0C_50001_00_KULLANICILAR] where COMPANY='LAM' AND [Kullanıcı Adı]='" + _obj.UserName + "'");
                if (DepBoyut == "3" || DepBoyut == "4")
                    Image1.ImageUrl = "~/images/thumbs/LAMK.png";
                else
                    Image1.ImageUrl = "~/images/thumbs/LAMB.png";
            }
            else
                Image1.ImageUrl = "~/images/thumbs/" + _obj.Comp + ".png";


        }

        void LoadMenu()
        {
            hrefLogoLink.HRef = "/Home.aspx?GId=" + Request.QueryString["GId"].ToString();
            ltrlMenu2.Text = "";
            string curUrl = Request.Url.PathAndQuery;
            string pageid = "";
            if (curUrl.Contains("&GId=") || curUrl.ToString().Contains("?GId=") || curUrl.ToString().Contains("&ONMG=1") || curUrl.ToString().Contains("?ONMG=1") || curUrl.ToString().Contains("?ONT=1") || curUrl.ToString().Contains("&ONT=1"))
            {
                curUrl = curUrl.Replace("&GId=" + Request.QueryString["GId"].ToString(), "");
                curUrl = curUrl.Replace("?GId=" + Request.QueryString["GId"].ToString(), "");
                curUrl = curUrl.Replace("&ONMG=1", "");
                curUrl = curUrl.Replace("?ONMG=1", "");
                curUrl = curUrl.Replace("?ONT=1", "");
                curUrl = curUrl.Replace("&ONT=1", "");
            }
            if (curUrl.Contains("&P1="))
            {
                int ilkDeger = curUrl.IndexOf("&P1");
                string curUrlSil = curUrl.Substring(ilkDeger);
                curUrl = curUrl.Replace(curUrlSil, "");
            }

            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[SP_0_WPAGE_MENU]";
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@COMP", _obj.Comp);
                    cmd.Parameters.AddWithValue("@USER", _obj.UserName);
                    cmd.Parameters.AddWithValue("@YTKG", "");
                    cmd.Parameters.AddWithValue("@YTK1", "");
                    cmd.Parameters.AddWithValue("@YTK2", "");
                    cmd.Parameters.AddWithValue("@YTK3", "");
                    cmd.Parameters.AddWithValue("@YTK4", "");
                    cmd.Parameters.AddWithValue("@YTK5", "");
                    cmd.Parameters.AddWithValue("@PRM1", "");
                    cmd.Parameters.AddWithValue("@PRM2", "");
                    cmd.Parameters.AddWithValue("@PRM3", "");
                    string anamenu = "";
                    string anamenu2 = "";
                    string altmenu = "";
                    ltrlAgacMenu.Text = "";

                    using (SqlDataAdapter adptr = new SqlDataAdapter(cmd))
                    {
                        using (DataSet ds = new DataSet())
                        {
                            adptr.Fill(ds);

                            _obj.DynMenu = ds.Tables[0];
                            DataRow[] MainRows = ds.Tables[0].Select("[DX Type2]='L1' and [DX Display Order]>0", "[DX Display Order] ASC");
                            string labeltext = string.Empty, postbackurl = string.Empty;
                            string threemenutree = "";
                            string threemenutwo = "";
                            string threemenuone = "";

                            foreach (DataRow row in MainRows)
                            {
                                string curmenu = "";
                                string curmenu2 = "";
                                altmenu = "";
                                curmenu = "<td class=\"menuUstButtonsTDStyle\"><a href=\"#\" class=\"menuUstButtons1\">";
                                curmenu2 = "<div class=\"menuUstButtonsTDStyle\"><a href=\"#\" class=\"menuUstButtons1\">";
                                if (curUrl == "/" + row["LINK"].ToString())
                                {
                                    curmenu = "<td class=\"menuUstButtonsTDStyle SelectedMenu\"><a href=\"#\" class=\"menuUstButtons1\">";
                                    curmenu2 = "<div class=\"menuUstButtonsTDStyle SelectedMenu\"><a href=\"#\" class=\"menuUstButtons1\">";

                                }


                                DataRow[] SubMenuRows = _obj.DynMenu.Select("[DX Type2]='L2' and [DX Obje Zone]='" + row["DX Page ID"].ToString() + "' and [DX Display Order]>0", "[DX Display Order] ASC");
                                foreach (var subrow in SubMenuRows)
                                {

                                    string link = "";
                                    string linktype = "";
                                    if (subrow["LINK"].ToString().Contains("http"))
                                    {
                                        link = subrow["LINK"].ToString();
                                        linktype = "target=\"_Blank\"";
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(subrow["LINK"].ToString()))
                                        {

                                        }
                                        else
                                        {
                                            if (subrow["LINK"].ToString().Contains("?"))
                                                link = "/" + subrow["LINK"].ToString() + "&GId=" + Request.QueryString["GId"].ToString();
                                            else
                                                link = "/" + subrow["LINK"].ToString() + "?GId=" + Request.QueryString["GId"].ToString();
                                        }
                                    }
                                    string ilkKarakter = link.Substring(0, 1);
                                    if (ilkKarakter == "/")
                                    {
                                        link = link.Substring(1);
                                    }
                                    if (curUrl == "/" + subrow["LINK"].ToString() || curUrl == subrow["LINK"].ToString())
                                    {
                                        curmenu = "<td class=\"menuUstButtonsTDStyle SelectedMenu\"><a href=\"#\" class=\"menuUstButtons1\">";
                                        curmenu2 = "<div class=\"menuUstButtonsTDStyle SelectedMenu\"><a href=\"#\" class=\"menuUstButtons1\">";
                                        altmenu += "<li class=\"SelectedMenu\"><a href=\"" + link + "\" " + linktype + "><div>" + subrow["" + Session["SelectedLanguage"].ToString() + ""].ToString() + "</div></a></li>";
                                        ltrlAgacMenu.Text += row["" + Session["SelectedLanguage"].ToString() + ""].ToString() + " > " + subrow["" + Session["SelectedLanguage"].ToString() + ""].ToString();
                                        Page.Title += " | " + row["" + Session["SelectedLanguage"].ToString() + ""].ToString() + " > " + subrow["" + Session["SelectedLanguage"].ToString() + ""].ToString();
                                    }
                                    else
                                    {
                                        altmenu += "<li><a href=\"" + link + "\" " + linktype + "><div>" + subrow["" + Session["SelectedLanguage"].ToString() + ""].ToString() + "</div></a></li>";
                                    }
                                }
                                anamenu += curmenu + row["" + Session["SelectedLanguage"].ToString() + ""].ToString() + "</a> <ul class=\"acilirMenu\">" + altmenu + "</ul></td>";
                                string menuIsmi = "";
                                if (string.IsNullOrEmpty(row["DX Path2"].ToString()))
                                {
                                    menuIsmi = row["" + Session["SelectedLanguage"].ToString() + ""].ToString();
                                }
                                else
                                {
                                    menuIsmi = "<i class=\"" + row["DX Path2"].ToString() + "\" title=\""+ row["" + Session["SelectedLanguage"].ToString() + ""].ToString() + "\"></i>";
                                }
                                anamenu2 += curmenu2 + menuIsmi + "</a> <ul class=\"acilirMenu\">" + altmenu + "</ul></div>";
                            }
                            if (ltrlAgacMenu.Text == "")
                            {
                                if (curUrl.Contains("OJ1="))
                                {
                                    string[] pgid = Request.QueryString["OJ1"].ToString().Split(';');
                                    pageid = pgid[0];
                                }
                                if (curUrl.Contains("P="))
                                {
                                    pageid = Request.QueryString["P"].ToString();
                                }
                                if (pageid != "")
                                {
                                    DataRow[] ThreeMenuRows = _obj.DynMenu.Select("[DX Type2]='L3' and [LINK] LIKE '%" + pageid + "%'");
                                    foreach (var threemenu in ThreeMenuRows)
                                    {
                                        threemenutree = threemenu["" + Session["SelectedLanguage"].ToString() + ""].ToString();
                                        DataRow[] ThreeMenuRowsTwo = _obj.DynMenu.Select("[DX Type2]='L2' and [DX Page ID]='" + threemenu["DX Obje Zone"].ToString() + "'");
                                        foreach (var twomenurows in ThreeMenuRowsTwo)
                                        {
                                            threemenutwo = twomenurows["" + Session["SelectedLanguage"].ToString() + ""].ToString();
                                            DataRow[] ThreeMenuRowsOne = _obj.DynMenu.Select("[DX Type2]='L1' and [DX Page ID]='" + twomenurows["DX Obje Zone"].ToString() + "'");
                                            foreach (var onemenurows in ThreeMenuRowsOne)
                                            {
                                                threemenuone = onemenurows["" + Session["SelectedLanguage"].ToString() + ""].ToString();
                                            }
                                        }
                                    }
                                    ltrlAgacMenu.Text = threemenuone + " > " + threemenutwo + " > " + threemenutree;
                                }
                            }
                            ltrlMenu2.Text = anamenu2;
                        }
                    }
                }
            }

        }


        private void FillComp()
        {
            DsCompany.SelectCommand = "select DISTINCT COMPANY Code, COMPNAME [Text]  from [0_0000002_01] where [Kullanıcı Adı]='" + _obj.UserName.ToUpper() + "'";
            cmbCompanyNew.Items.Clear();
            cmbCompanyNew.DataBind();
            cmbCompanyNew.SelectedValue = _obj.Comp;
        }


        private void FillLang()
        {
            string sorgu = "select [LineNo] Code, Adi [Text] From [0C_50055_40_LOOKUPS_LANGUAGES] WHERE COMPANY='" + _obj.Comp + "'";
            cmbLangNew.Items.Clear();
            DsLang.SelectCommand = sorgu;
            cmbLangNew.DataBind();
        }


        /*  ESKİ MENÜ YAPISI YENi MENÜ YAPISI İÇİN UĞURCAN ÖZCAN TARAFINDAN COMMITLENDI, İSTEK TİMÜR BEY (SELMAN BEY ONAYLI)
        public void menusecili()
        {
            var control = this.FindControl("td" + _obj.NewPage.ToString());
            if (control.GetType() == typeof(HtmlTableCell))
             {
                 ((HtmlTableCell)control).Attributes.Add("class", "menuUstButtonsSelectedTDStyle");
             }

             for (int i = 1; i < 10; i++)
             {
                 if (i == _obj.NewPage) continue;
                 control = this.FindControl("td" + i.ToString());
                 if (control.GetType() == typeof(HtmlTableCell))
                 {
                     ((HtmlTableCell)control).Attributes.Add("class", "menuUstButtonsTDStyle");
                 }
             }

         }

         public void menuolustur()
         {

             AltMenu.Items.Clear();
             string _ObjeZone = string.Empty;
             var control = this.FindControl("btn" + _obj.NewPage.ToString());
             if (control.GetType() == typeof(Button))
             {
                 _ObjeZone = ((Button)control).CommandArgument.Split('|')[1];
             }

             DataRow[] SubMenuRows = _obj.DynMenu.Select("[DX Type2]='L2' and [DX Obje Zone]='" + _ObjeZone + "' and [DX Display Order]>0", "[DX Display Order] ASC");
             foreach (var row in SubMenuRows)
             {
                 DevExpress.Web.MenuItem submenu = new DevExpress.Web.MenuItem();
                 if (row["LINK"].ToString().Contains("http"))
                 {
                     submenu.NavigateUrl = row["LINK"].ToString();
                     submenu.Target = "_Blank";
                 }
                 else
                 {
                     if (string.IsNullOrEmpty(row["LINK"].ToString()))
                         submenu.Enabled = false;
                     else
                     {
                         if (row["LINK"].ToString().Contains("?"))
                             submenu.NavigateUrl = "~/" + row["LINK"].ToString() + "&GId=" + Request.QueryString["GId"].ToString();
                         else
                             submenu.NavigateUrl = "~/" + row["LINK"].ToString() + "?GId=" + Request.QueryString["GId"].ToString();
                     }
                 }

                 submenu.Text = row[_obj.SelectedLanguage].ToString();
                 AltMenu.Items.Add(submenu);
             }



         }
        */

        private void getMainAspx()
        {
            Response.Redirect("~/Home.aspx?GId=" + Request.QueryString["GId"].ToString());
        }



        protected void AltMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            try
            {
                Session.Remove("REPORT_KEY");
                Session.Remove("REPORT_NAME");
                Session.Remove("myObjrptdoc");
                Session.Remove("syObjrptdoc");
                Session.Remove("mpObjrptdoc");
                Session.Remove("PAGObjrptdoc");
                Session.Remove("isrObjrptdoc");

            }
            catch
            {

            }
        }

        protected void AltMenu_ItemClick(object source, DevExpress.Web.MenuItemEventArgs e)
        {
            try
            {
                Session.Remove("REPORT_KEY");
                Session.Remove("REPORT_NAME");
                Session.Remove("myObjrptdoc");
                Session.Remove("syObjrptdoc");
                Session.Remove("mpObjrptdoc");
                Session.Remove("PAGObjrptdoc");
                Session.Remove("isrObjrptdoc");
            }
            catch (Exception)
            {
                ;
            }
        }
        //ESKİ MENÜ YAPISI YENİ MENÜ YAPISI İÇİN UĞURCAN ÖZCAN TARAFINDAN COMMITLENDI, İSTEK TİMÜR BEY (SELMAN BEY ONAYLI)
        /*protected void btnFlashCrm_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("newpage_iletisimgirisi.aspx?Tur=2?GId=" + Request.QueryString["GId"].ToString());
        }*/
        protected void cmbLang_SelectedIndexChanged(object sender, EventArgs e)
        {



        }
        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            EndpointAddress _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }

        void SetTdVisibility(string _FieldId, bool _visible)
        {
            try
            {
                var control = this.FindControl(_FieldId);
                if (control == null) return;
                if (control.GetType() == typeof(HtmlTableCell))
                    control.Visible = _visible;
            }
            catch { }

        }

        void LoadTopMenu()
        {
            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[SP_0_WPAGE_MENU]";
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@COMP", _obj.Comp);
                    cmd.Parameters.AddWithValue("@USER", _obj.UserName);
                    cmd.Parameters.AddWithValue("@YTKG", "");
                    cmd.Parameters.AddWithValue("@YTK1", "");
                    cmd.Parameters.AddWithValue("@YTK2", "");
                    cmd.Parameters.AddWithValue("@YTK3", "");
                    cmd.Parameters.AddWithValue("@YTK4", "");
                    cmd.Parameters.AddWithValue("@YTK5", "");
                    cmd.Parameters.AddWithValue("@PRM1", "");
                    cmd.Parameters.AddWithValue("@PRM2", "");
                    cmd.Parameters.AddWithValue("@PRM3", "");

                    using (SqlDataAdapter adptr = new SqlDataAdapter(cmd))
                    {
                        using (DataSet ds = new DataSet())
                        {
                            adptr.Fill(ds);

                            _obj.DynMenu = ds.Tables[0];
                            DataRow[] MainRows = ds.Tables[0].Select("[DX Type2]='L1' and [DX Display Order]>0", "[DX Display Order] ASC");
                            string labeltext = string.Empty, postbackurl = string.Empty;
                            int sayac = 1;
                            foreach (DataRow row in MainRows)
                            {
                                SetTdVisibility("td" + sayac, true);
                                var control = this.FindControl("btn" + sayac);
                                if (control.GetType() == typeof(Button))
                                {
                                    ((Button)control).CommandArgument += "|" + row["DX Page ID"].ToString();

                                    ((Button)control).Text = row[cmbLangNew.SelectedItem.Text].ToString();
                                }
                                sayac++;
                            }

                        }
                    }
                }
            }
        }

        void LoadReportMenu(string UserName)
        {
            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[SP_0WEB_GET_WIZARD_DETAILS]";
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@UserName", UserName);
                    cmd.Parameters.AddWithValue("@Company", _obj.Comp);


                    using (SqlDataAdapter adptr = new SqlDataAdapter(cmd))
                    {
                        using (DataSet ds = new DataSet())
                        {
                            adptr.Fill(ds);

                            // Report
                            string labeltext = string.Empty, postbackurl = string.Empty;
                            foreach (DataRow row in ds.Tables[1].Rows)
                            {
                                try
                                {
                                    if (string.IsNullOrEmpty(row["Path"].ToString()))
                                    {
                                        labeltext = "Kayıtta Path Yok";
                                        postbackurl = "JavaScript: return false;";
                                        SetLabelAndLinks(Convert.ToInt32(row["Alan 1"].ToString().Replace("R", string.Empty).Replace("L", string.Empty)), postbackurl, labeltext);

                                        continue;
                                    }
                                    //javascript:popuac('Raporlar/PersonelAracGider.aspx');
                                    string path = ws.icip + row["Path"].ToString();
                                    if (path.Contains("?"))
                                        path += "&GId=" + Request.QueryString["GId"];
                                    else
                                        path += "?GId=" + Request.QueryString["GId"];
                                    labeltext = row["Ad TR"].ToString();
                                    postbackurl = "javascript:popuac('" + path + "');return false;";
                                    SetLabelAndLinks(Convert.ToInt32(row["Alan 1"].ToString().Replace("R", string.Empty).Replace("L", string.Empty)), postbackurl, labeltext);

                                }
                                catch
                                {
                                    labeltext = "Kayıtta Hata Var";
                                    postbackurl = "JavaScript: return false;";
                                    SetLabelAndLinks(Convert.ToInt32(row["Alan 1"].ToString().Replace("R", string.Empty).Replace("L", string.Empty)), postbackurl, labeltext);
                                }


                            }

                        }
                    }
                }
            }
        }

        void SetLabelAndLinks(int sayac, string PostBackUrl, string LabelLinkText)
        {
            try
            {
                switch (sayac)
                {
                    case 1:
                        LinkButton1.OnClientClick = PostBackUrl;
                        LinkButton1.Text = LabelLinkText;
                        break;
                    case 2:
                        LinkButton2.OnClientClick = PostBackUrl;
                        LinkButton2.Text = LabelLinkText;
                        break;
                    case 3:
                        LinkButton3.OnClientClick = PostBackUrl;
                        LinkButton3.Text = LabelLinkText;
                        break;
                    case 4:
                        LinkButton4.OnClientClick = PostBackUrl;
                        LinkButton4.Text = LabelLinkText;
                        break;
                    case 5:
                        LinkButton5.OnClientClick = PostBackUrl;
                        LinkButton5.Text = LabelLinkText;
                        break;
                    case 6:
                        LinkButton6.OnClientClick = PostBackUrl;
                        LinkButton6.Text = LabelLinkText;
                        break;
                    case 7:
                        LinkButton7.OnClientClick = PostBackUrl;
                        LinkButton7.Text = LabelLinkText;
                        break;
                    case 8:
                        LinkButton8.OnClientClick = PostBackUrl;
                        LinkButton8.Text = LabelLinkText;
                        break;
                    case 9:
                        LinkButton9.OnClientClick = PostBackUrl;
                        LinkButton9.Text = LabelLinkText;
                        break;
                    case 10:
                        LinkButton10.OnClientClick = PostBackUrl;
                        LinkButton10.Text = LabelLinkText;
                        break;
                    case 11:
                        LinkButton11.OnClientClick = PostBackUrl;
                        LinkButton11.Text = LabelLinkText;
                        break;
                    case 12:
                        LinkButton12.OnClientClick = PostBackUrl;
                        LinkButton12.Text = LabelLinkText;
                        break;
                    case 13:
                        LinkButton13.OnClientClick = PostBackUrl;
                        LinkButton13.Text = LabelLinkText;
                        break;
                    case 14:
                        LinkButton14.OnClientClick = PostBackUrl;
                        LinkButton14.Text = LabelLinkText;
                        break;
                    case 15:
                        LinkButton15.OnClientClick = PostBackUrl;
                        LinkButton15.Text = LabelLinkText;
                        break;
                    case 16:
                        LinkButton16.OnClientClick = PostBackUrl;
                        LinkButton16.Text = LabelLinkText;
                        break;
                    case 17:
                        LinkButton17.OnClientClick = PostBackUrl;
                        LinkButton17.Text = LabelLinkText;
                        break;
                    case 18:
                        LinkButton18.OnClientClick = PostBackUrl;
                        LinkButton18.Text = LabelLinkText;
                        break;
                    case 19:
                        LinkButton19.OnClientClick = PostBackUrl;
                        LinkButton19.Text = LabelLinkText;
                        break;
                    case 20:
                        LinkButton20.OnClientClick = PostBackUrl;
                        LinkButton20.Text = LabelLinkText;
                        break;
                    default:
                        break;
                }
            }
            catch { }
        }

        /*protected void btn1_Click(object sender, EventArgs e)
        {

            Button btn = sender as Button;
            int sayac = Convert.ToInt32(btn.CommandArgument.Split('|')[0]);
            if (sayac == 1)
                fonksiyonlar.Home = true;
            else
                fonksiyonlar.Home = false;
            _obj.NewPage = sayac;
            menusecili();
            menuolustur();
            getMainAspx();
        }*/

        protected void cmbCompany_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void cmbCompanyNew_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (SqlConnection connect = new SqlConnection(strConnString))
            {
                using (SqlCommand query = new SqlCommand("Select İsim,[Kullanıcı Adı] as kulad,[Personel ID] as perid,DEPT as departman,BLG as bolge,[İlk Giriş] as ilkgiris from [0C_50001_00_KULLANICILAR] where COMPANY =@sirket and UPPER([Kullanıcı Adı])=@kullanici", connect))
                {
                    query.Parameters.AddWithValue("@Kullanici", _obj.UserName.ToUpper());
                    query.Parameters.AddWithValue("@sirket", cmbCompanyNew.SelectedValue.ToString());
                    connect.Open();

                    SqlDataReader reader = query.ExecuteReader();
                    if (reader.Read())
                    {
                        Session["uyedurum"] = "tamam";
                        string kad = reader["İsim"].ToString();
                        Session["İsim"] = kad;
                        Session["Sirket"] = cmbCompanyNew.SelectedValue.ToString();
                        Session["kulad"] = reader["kulad"].ToString();
                        Session["perid"] = reader["perid"].ToString();
                        Session["bolge"] = reader["bolge"].ToString();
                        Session["departman"] = reader["departman"].ToString();
                        Session["ilkgiris"] = reader["ilkgiris"].ToString();
                        Session.Timeout = 120;
                        fonksiyonlar.sirket = cmbCompanyNew.SelectedValue.ToString();
                        Session["SelectedLanguage"] = f.tekbirsonucdonder("select Adi from [0_0000002] where [Kullanıcı Adı]='" + _obj.UserName + "' and COMPANY='" + _obj.Comp + "'");
                        _Guid = Guid.NewGuid();
                        TabSessions.Set(_Guid.ToString(), cmbCompanyNew.SelectedValue.ToString(), reader["kulad"].ToString(), reader["İsim"].ToString(), Session["SelectedLanguage"].ToString(), "");
                        string path = "./Home.aspx?GId=" + _Guid.ToString();

                        if (!Page.ClientScript.IsStartupScriptRegistered("JSNew" + cmbCompanyNew.SelectedValue.ToString()))
                        {
                            Page.ClientScript.RegisterStartupScript(GetType(), "JSNew" + cmbCompanyNew.SelectedValue.ToString(), @"<script type=""text/javascript""> "
                            + "window.open('" + path + "');</script>");
                        }

                        // Response.Redirect("~/Home.aspx?GId=" + _Guid.ToString());
                        cmbCompanyNew.SelectedItem.Value = _obj.Comp;
                    }
                    else
                    {
                        MessageBox("This user does not have access to the specified company");
                    }

                    reader.Dispose();
                    reader.Close();
                    connect.Close();
                }
            }
        }

        protected void cmbLangNew_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["SelectedLanguage"] = cmbLangNew.SelectedItem.Text;
            Session["SelectedLangVal"] = cmbLangNew.SelectedValue.ToString();
            _obj.SelectedLanguage = cmbLangNew.SelectedItem.Text;
            _obj.SelectedLangVal = cmbLangNew.SelectedValue.ToString();
            string[] _params = { Session["kulad"].ToString(), cmbLangNew.SelectedItem.Text };
            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "UpdateWebLanguage§" + string.Join("§", _params), _obj.Comp);

            getMainAspx();
        }
    }
}