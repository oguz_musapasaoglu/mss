﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.WSGeneric;
using MSS1.Codes;

namespace MSS1
{
    public partial class LogisticInvoiceSelector :Bases
    {
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        SqlConnection baglanti;
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        SessionBase _obj;
        public ws_baglantı ws = new ws_baglantı();

        #region Dil Getir
        public void dilgetir()
        {
            lblDovCinsB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "satis_sip", "lblDovCinsB");
            btnList.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "LogisticInvoiceSelector", "btnList");
            btnCreate.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "LogisticInvoiceSelector", "btnCreate");
            btnSelectAll.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "LogisticInvoiceSelector", "btnSelectAll");
            btnUnSelectAll.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "LogisticInvoiceSelector", "btnUnSelectAll");
        }
        #endregion

        #region Page Sub Methods
        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }
        #endregion


        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            dilgetir();
            if (IsPostBack || IsCallback) return;

            f.DropDownDoldurIlkSatirEkle("Select Code name, Code kod From [0C_00004_00_CURRENCY] where COMPANY ='" + _obj.Comp + "'  Order By Code", ddldoviz, true, "TL");
            ddldoviz.SelectedIndex = 0;

            if (Request.QueryString["CustomerNo"] != null && Request.QueryString["CurrencyCode"] != null)
            {
                ddldoviz.SelectedValue = Request.QueryString["CurrencyCode"].ToString();
                LoadGrid(Request.QueryString["CustomerNo"].ToString(), Request.QueryString["CurrencyCode"].ToString());
            }

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }

        void LoadGrid(string CustomerNo, string CurrencyCode)
        {

            try
            {

                if (Request.QueryString["Page"].ToString() == "iadesatis_sip")
                    GrdLines.Columns[1].HeaderText = "CUSTOMER NO";

                using (SqlConnection conn = new SqlConnection(strConnString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {

                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "SP_0WEB_PLANLAMA_SATIR_ARA";
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@COMP", _obj.Comp);
                        cmd.Parameters.AddWithValue("@MENU", Request.QueryString["Page"].ToString());
                        cmd.Parameters.AddWithValue("@CARINO", CustomerNo);
                        cmd.Parameters.AddWithValue("@CurrencyCode", ddldoviz.SelectedValue);
                        cmd.Parameters.AddWithValue("@MBLNO", txtMBLNo.Text);
                        cmd.Parameters.AddWithValue("@HBLNO", txtHBLNo.Text);
                        cmd.Parameters.AddWithValue("@ContainerNo", txtContainerNo.Text);
                        cmd.Parameters.AddWithValue("@JobNo", txtJobNo.Text);
                        cmd.Parameters.AddWithValue("@Cass", Convert.ToInt32(chCass.Checked));

                        using (SqlDataAdapter adptr = new SqlDataAdapter(cmd))
                        {
                            DataTable dt = new DataTable();
                            adptr.Fill(dt);
                            GrdLines.DataSource = dt;
                            GrdLines.DataBind();
                        }
                    }
                }



            }
            catch (Exception ex)
            {
                MessageBox("Error : " + ex.Message.Replace("'", " "));
            }

        }



        protected void btnList_Click(object sender, EventArgs e)
        {

            if (Request.QueryString["CustomerNo"] != null && Request.QueryString["CurrencyCode"] != null)
            {

                ddldoviz.SelectedValue = Request.QueryString["CurrencyCode"].ToString();
                LoadGrid(Request.QueryString["CustomerNo"].ToString(), Request.QueryString["CurrencyCode"].ToString());
            }
        }
        protected void btnCreate_Click(object sender, EventArgs e)
        {
            //  return; // unutma coşku.

            string InvoiceInfos = string.Empty, _InvoiceNumber = string.Empty;
            bool ilk = true; 

            string _vendorNo = string.Empty;
            foreach (GridViewRow row in GrdLines.Rows)
            {
                CheckBox chk = (CheckBox)row.FindControl("chksec");


                if (chk != null & chk.Checked)
                {
                    Literal ltrJobContractEntryNo = (Literal)row.FindControl("ltrJobContractEntryNo");
                    Literal ltrGacJobNo = (Literal)row.FindControl("ltrGacJobNo");
                    Literal ltrJobNo = (Literal)row.FindControl("ltrJobNo");
                    Literal ltrVendorNo = (Literal)row.FindControl("ltrVendorNo");
                    _vendorNo = ltrVendorNo.Text;
                    if (String.IsNullOrEmpty(ltrGacJobNo.Text))
                    {
                        MessageBox("Lütfen önce, seçilen işlerin Job Track No’sunu doldurunuz. Job Numarası : " + ltrJobNo.Text);
                        return;
                    }
                    if (ilk)
                    {
                        InvoiceInfos = ltrJobContractEntryNo.Text;
                        ilk = false;
                    }
                    else
                        InvoiceInfos += ";" + ltrJobContractEntryNo.Text;

                }
            }


            if (Request.QueryString["Page"].ToString() == "1")
            {
                string sipNo = Request.QueryString["SipNo"].ToString();


                _InvoiceNumber = GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "OperasyonAF" + "§" + _vendorNo + "§" + Request["CurrencyCode"].ToString()
                    + "§" + Request["UserName"].ToString() + "§" + Request["Date"].ToString() + "§" + 
                    Convert.ToDecimal(Request["Currency"].ToString().Replace(",", ".")).ToString()
                    + "§" + Request["VendInvNo"].ToString() + "§" + InvoiceInfos + "§" + sipNo + "§" + "1", _obj.Comp);

                 

                if (!(_InvoiceNumber.StartsWith("AF")))
                {
                    MessageBox("Fatura oluşturulamadı." + _InvoiceNumber);
                    return;
                }
                if (!ClientScript.IsStartupScriptRegistered("JSFaturaAcik"))
                {
                    ClientScript.RegisterStartupScript(GetType(), "JSFaturaAcik", @"window.opener.parent.RefreshAllCollapse();self.close();", true);
                }
            }
            else if (Request.QueryString["Page"].ToString() == "1")
            {
                string sipNo = Request.QueryString["SipNo"].ToString();




                _InvoiceNumber = GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "OperasyonAF" + "§" + _vendorNo + "§" + Request["CurrencyCode"].ToString()
                    + "§" + Request["UserName"].ToString() + "§" + Request["Date"].ToString() + "§" +
                    Convert.ToDecimal(Request["Currency"].ToString().Replace(",", ".")).ToString()
                    + "§" + Request["VendInvNo"].ToString() + "§" + InvoiceInfos + "§" + sipNo + "§" + "1", _obj.Comp);
                 

                if (!(_InvoiceNumber.StartsWith("AF")))
                {
                    MessageBox("Fatura oluşturulamadı." + _InvoiceNumber);
                    return;
                }
                if (!ClientScript.IsStartupScriptRegistered("JSFaturaAcik"))
                {
                    ClientScript.RegisterStartupScript(GetType(), "JSFaturaAcik", @"window.opener.parent.RefreshAllCollapse();self.close();", true);
                }
            }
            else if (Request.QueryString["Page"].ToString() == "2")
            {
                string sipNo = Request.QueryString["SipNo"].ToString();

                _InvoiceNumber = GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "OperasyonAF" + "§" + _vendorNo + "§" + Request["CurrencyCode"].ToString()
                  + "§" + Request["UserName"].ToString() + "§" + Request["Date"].ToString() + "§" +
                  Convert.ToDecimal(Request["Currency"].ToString().Replace(",", ".")).ToString()
                  + "§" + Request["VendInvNo"].ToString() + "§" + InvoiceInfos + "§" + sipNo + "§" + "2", _obj.Comp);
                 

                if (!(_InvoiceNumber.StartsWith("AİF")))
                {
                    MessageBox("Fatura oluşturulamadı." + _InvoiceNumber);
                    return;
                }
                if (!ClientScript.IsStartupScriptRegistered("JSFaturaAcik"))
                {
                    ClientScript.RegisterStartupScript(GetType(), "JSFaturaAcik", @"window.opener.parent.RefreshAllCollapse();self.close();", true);
                }
                //Response.Write("<script>window.opener.location.href='iadesatinalma.aspx?SipNo=" + _InvoiceNumber + "&tur=0';</script>");
                //Response.Write("<script>window.close();</script>");
            }
            else if (Request.QueryString["Page"].ToString() == "4")
            {
                string sipNo = Request.QueryString["SipNo"].ToString();

                _InvoiceNumber = GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "OperasyonAF" + "§" + Request["CustomerNo"].ToString() + "§" + Request["CurrencyCode"].ToString()
                  + "§" + Request["UserName"].ToString() + "§" + Request["Date"].ToString() + "§" +
                  Convert.ToDecimal(Request["Currency"].ToString().Replace(",", ".")).ToString()
                  + "§" + Request["VendInvNo"].ToString() + "§" + InvoiceInfos + "§" + sipNo + "§" + "2", _obj.Comp);
                 

                if (!(_InvoiceNumber.StartsWith("SİF")))
                {
                    MessageBox("Fatura oluşturulamadı." + _InvoiceNumber);
                    return;
                }
                if (!ClientScript.IsStartupScriptRegistered("JSFaturaAcik"))
                {
                    ClientScript.RegisterStartupScript(GetType(), "JSFaturaAcik", @"window.opener.parent.RefreshAllCollapse();self.close();", true);
                }
            }
            else if (Request.QueryString["Page"].ToString() == "3")
            {
                string sipNo = Request.QueryString["SipNo"].ToString();


                _InvoiceNumber = GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "OperasyonAF" + "§" + Request["CustomerNo"].ToString() + "§" + Request["CurrencyCode"].ToString()
                  + "§" + Request["UserName"].ToString() + "§" + Request["Date"].ToString() + "§" +
                  Convert.ToDecimal(Request["Currency"].ToString().Replace(",", ".")).ToString()
                  + "§" + Request["VendInvNo"].ToString() + "§" + InvoiceInfos + "§" + sipNo + "§" + "1", _obj.Comp);
                 

                if (!(_InvoiceNumber.StartsWith("SF")))
                {
                    MessageBox("Fatura oluşturulamadı." + _InvoiceNumber);
                    return;
                }
                if (!ClientScript.IsStartupScriptRegistered("JSFaturaAcik"))
                {
                    ClientScript.RegisterStartupScript(GetType(), "JSFaturaAcik", @"window.opener.parent.RefreshAllCollapse();self.close();", true);
                }
                //Response.Write("<script>window.opener.location.href='satis_sip.aspx?SipNo=" + _InvoiceNumber + "&tur=0';</script>");
                //Response.Write("<script>window.close();</script>");
            }
        }
        protected void btnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GrdLines.Rows)
            {
                CheckBox chk = (CheckBox)row.FindControl("chksec");
                if (chk != null)
                {
                    chk.Checked = true;
                }
            }
        }
        protected void btnUnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GrdLines.Rows)
            {
                CheckBox chk = (CheckBox)row.FindControl("chksec");
                if (chk != null)
                {
                    chk.Checked = false;
                }
            }
        }


    }
}