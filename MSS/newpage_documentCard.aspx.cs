﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.WSGeneric;
using System.ServiceModel;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DevExpress.Web;
using System.Web.UI.HtmlControls;
using System.IO;
using MSS1.Codes;

namespace MSS1
{
    public partial class newpage_documentCard : Bases
    {
        #region Connection String & Service
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
    
        public ws_baglantı ws = new ws_baglantı();
        SqlConnection baglanti;
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        string kontrol = string.Empty;
        DataTable DtFields = new DataTable();
        static SessionBase _obj;
        #endregion

        #region Servis Arama
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod(EnableSession = true)]
        public static List<string> SearchTaxarea(string prefixText, int count)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
            if (prefixText == "*")
            {
                string query = "Select Description as name from [0C_00318_00_TAX AREA] where COMPANY='" + _obj.Comp + "' order by Sıralama asc";
                SqlCommand cmd = new SqlCommand(query, conn);
                conn.Open();
                List<string> bul = new List<string>();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        bul.Add(sdr["name"].ToString());
                    }
                }
                conn.Close();

                return bul;
            }
            else
            {
                string query = "Select Description as name from [0C_00318_00_TAX AREA] where COMPANY='" + _obj.Comp + "' AND Description like '" + prefixText + "%'  order by Sıralama asc";
                SqlCommand cmd = new SqlCommand(query, conn);
                conn.Open();
                List<string> bul = new List<string>();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        bul.Add(sdr["name"].ToString());
                    }
                }
                conn.Close();

                return bul;
            }

        }
        #endregion

        #region Dil Getir
        public void dilgetir()
        {
            Language l = new Language();
            lblsayfaadi.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentCard", "lblsayfaadi");
            lblCompany.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentCard", "lblCompany");
            lblDocumentGroup.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentCard", "lblDocumentGroup");
            lblDocumentType.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentCard", "lblDocumentType");
            btnDosyaBagla.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentCard", "btnDosyaBagla");
            lblDurum.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentCard", "lblDurum");
            btnDelete.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentCard", "btnDelete");
            Hiddenmsg04.Value = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentCard", "Hiddenmsg04");

        }
        #endregion


        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        { 
              _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
              _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
          
            if (IsPostBack || IsCallback) return;
            dilgetir();
            InitDTComponents(STDT1.Table);
            InitDTComments(DTComments.Table);

            cmbCompany.Text = _obj.Comp.ToUpper();
            cmbCompany.Enabled = false;

            int EntryNo = Convert.ToInt32(Request.QueryString["EntryNo"].ToString());
            if (Request.QueryString["UpdatedDoc"] != null)
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentCard", "msg03"));
            LoadDocument(EntryNo);



        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }

        void FillDocumentType(int Group)
        {
            cmbDocumenType.Items.Clear();
            DSDocumentType.SelectCommand = "select [Belge No] Code, [Belge Adi] Adi from [0C_70000_00_DOCUMENTS GENEL] where [Dokuman Grubu]=" + Group.ToString() + "  order by [Belge Adi]";
            cmbDocumenType.DataBind();
        }

        void LoadDocument(int EntryNo)
        {
            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[SP_0WEB_DOKUMAN_DETAYI_AL]";
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@EntryNo", EntryNo);
                    cmd.Parameters.AddWithValue("@Company", Convert.ToInt32(Request.QueryString["Company"].ToString()));
                    using (SqlDataAdapter adptr = new SqlDataAdapter(cmd))
                    {
                        using (DataSet ds = new DataSet())
                        {
                            adptr.Fill(ds);
                            int CIndex = 0;
                            string LookupsSorgu = string.Empty;
                            if (ds.Tables[0].Rows.Count > 0)
                            {

                                cmbCompany.Value = ds.Tables[0].Rows[0]["Sirket"].ToString();
                                cmbDocumentGroup.Value = ds.Tables[0].Rows[0]["Dokuman Grubu"].ToString();
                                cmbStatus.Value = ds.Tables[0].Rows[0]["Durum"].ToString();
                                FillDocumentType(Convert.ToInt32(ds.Tables[0].Rows[0]["Dokuman Grubu"].ToString()));
                                cmbDocumenType.Value = ds.Tables[0].Rows[0]["Belge No"].ToString();
                                lblno.Text = ds.Tables[0].Rows[0]["Dosya Ismi"].ToString();
                                cmbDocumenType_SelectedIndexChanged(null, null);
                                cmbCompany.Enabled = false;
                                cmbDocumentGroup.Enabled = false;
                                cmbStatus.Enabled = false;
                                cmbDocumenType.Enabled = false;

                                string FieldType, FieldNavNo;
                                DateTime dt1 = DateTime.Now.AddYears(-100);
                                foreach (DataRow row in STDT1.Table.Rows)
                                {
                                    CIndex++;
                                    if (row["FieldType"].ToString() == "TEX" || row["FieldType"].ToString() == "COD")
                                        FieldType = "TEX";
                                    else
                                        FieldType = row["FieldType"].ToString();

                                    FieldNavNo = row["FieldNavNo"].ToString();
                                    if (FieldType == "TAR")
                                    {
                                        try
                                        {
                                            dt1 = Convert.ToDateTime(ds.Tables[0].Rows[0][row["FieldNavName"].ToString()].ToString());
                                        }
                                        catch { }
                                        if (dt1 > new DateTime(2000, 1, 1))
                                            ((ASPxDateEdit)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Date = dt1;
                                    }
                                    else if (FieldType == "TEX")
                                    {
                                        if (row["IsLookups"].ToString() == "0")
                                            ((ASPxTextBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Text = ds.Tables[0].Rows[0][row["FieldNavName"].ToString()].ToString();
                                        else if (row["IsLookups"].ToString() == "1")
                                        {
                                            LookupsSorgu = "Select [LineNo] Code, Adi from [0C_50055_00_LOOKUPS] where " +
                                                "COMPANY='" + _obj.Comp + "' AND Tur='" + row["Lookups_Tur"].ToString() + "' Order By [LineNo]";
                                            ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).SelectCommand = LookupsSorgu;
                                            ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).DataBind();

                                            ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).ValueField = "Adi";
                                            ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).TextField = "Adi";
                                            ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).DataSource = ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString()));
                                            ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).DataBind();
                                            ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Value = ds.Tables[0].Rows[0][row["FieldNavName"].ToString()].ToString();
                                        }
                                        else if (row["IsLookups"].ToString() == "2")
                                        {
                                            LookupsSorgu = "Select [" + row["Kaynak_Tablo_Alan_Adi"].ToString().Split(',')[0] + "] Code, [" + row["Kaynak_Tablo_Alan_Adi"].ToString().Split(',')[1] + "] Adi from " + row["Kaynak_Tablo"].ToString() + "  Order By [" + row["Kaynak_Tablo_Alan_Adi"].ToString().Split(',')[1] + "]";
                                            ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).SelectCommand = LookupsSorgu;
                                            ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).DataBind();

                                            ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).ValueField = "Code";
                                            ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).TextField = "Adi";
                                            ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).DataSource = ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString()));
                                            ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).EnableCallbackMode = true;
                                            ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).CallbackPageSize = 20;
                                            ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).DataBind();
                                            ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Value = ds.Tables[0].Rows[0][row["FieldNavName"].ToString()].ToString();
                                        }
                                    }
                                    else if (FieldType == "INT")
                                    {
                                        LookupsSorgu = "Select [LineNo] Code, Adi from [0C_50055_00_LOOKUPS] where " +
                                            "COMPANY='" + _obj.Comp + "' AND Tur='" + row["Lookups_Tur"].ToString() + "' Order By [LineNo]";
                                        ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).SelectCommand = LookupsSorgu;
                                        ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).DataBind();

                                        ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).ValueField = "Code";
                                        ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).TextField = "Adi";
                                        ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).DataSource = ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString()));
                                        ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).DataBind();
                                        ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Value = ds.Tables[0].Rows[0][row["FieldNavName"].ToString()].ToString();
                                    }
                                    else if (FieldType == "BOO")
                                    {
                                        try
                                        {
                                            if (ds.Tables[0].Rows[0][row["FieldNavName"].ToString()].ToString() == "1")
                                                ((ASPxCheckBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Checked = true;
                                            else if (ds.Tables[0].Rows[0][row["FieldNavName"].ToString()].ToString() == "0")
                                                ((ASPxCheckBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Checked = false;
                                            else
                                            {
                                                ((ASPxCheckBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Checked
                                                    = Convert.ToBoolean(ds.Tables[0].Rows[0][row["FieldNavName"].ToString()]);
                                            }
                                        }
                                        catch { ((ASPxCheckBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Checked = false; }
                                    }
                                    else if (FieldType == "DEC")
                                    {
                                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0][row["FieldNavName"].ToString()].ToString()))
                                        {
                                            decimal ss = Convert.ToDecimal(ds.Tables[0].Rows[0][row["FieldNavName"].ToString()]);
                                            ((ASPxTextBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Text =
                                              string.Format("{0:0.00}", ss);
                                        }


                                    }

                                }


                            }
                            else
                            {
                                cmbCompany.Enabled = false;
                                cmbDocumentGroup.Enabled = false;
                                cmbStatus.Enabled = false;
                                cmbDocumenType.Enabled = false;
                            }
                        }
                    }
                }
            }
        }

        int GetDahiliNextNo()
        {
            string sorgu = "select count(*) from [0C_70002_00_DOCUMENTS DETAILS GENEL] where COMPANY='" + _obj.Comp + "' AND [Dokuman Grubu]=8";
            int DahiliNextNo = Convert.ToInt32(f.tekbirsonucdonder(sorgu)) + 1;
            return DahiliNextNo;
        }

        private void InitDTComponents(DataTable dt)
        {
            dt.Columns.Add("FieldId", typeof(string));
            dt.Columns.Add("FieldType", typeof(string));
            dt.Columns.Add("FieldNavNo", typeof(string));
            dt.Columns.Add("FieldNavName", typeof(string));
            dt.Columns.Add("IsMandatory", typeof(int));
            dt.Columns.Add("FieldName", typeof(string));
            dt.Columns.Add("IsLookups", typeof(int));
            dt.Columns.Add("Kaynak_Tablo", typeof(string));
            dt.Columns.Add("Lookups_Tur", typeof(string));
            dt.Columns.Add("Kaynak_Tablo_Alan_Adi", typeof(string));


        }

        private void InitDTComments(DataTable dt)
        {
            dt.Columns.Add("LineNo", typeof(string));
            dt.Columns.Add("Comment", typeof(string));
            dt.Columns.Add("Tarih", typeof(DateTime));
            dt.Columns.Add("Saat", typeof(DateTime));
            dt.Columns.Add("Kullanici", typeof(string));

        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }


        protected void cmbDocumenType_SelectedIndexChanged(object sender, EventArgs e)
        {

            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    clearValues();
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "select t1.*,t2.TR from [0C_70001_00_DOCUMENTS FIELDS GENEL] t1"
                    + " left outer join [0C_50032_01_WEB PAGE CAPTIONS] t2 on t1.COMPANY=t2.COMPANY AND t1.[Field ID]=t2.FieldID and t2.PageID='newpage_documentEntry'"
                    + " where t1.COMPANY='" + _obj.Comp + "' AND [Belge No]=@BelgeNo Order By t1.[Web alan sira no]";
                    cmd.Parameters.Clear();
                    if (!string.IsNullOrEmpty(this.cmbDocumenType.Text))
                        cmd.Parameters.AddWithValue("@BelgeNo", this.cmbDocumenType.Value.ToString());
                    else
                        cmd.Parameters.AddWithValue("@BelgeNo", string.Empty);

                    for (int i = 1; i <= 25; i++)
                    {
                        SetComponentVisibility("tr" + i.ToString(), false);
                    }
                    STDT1.Table.Rows.Clear();
                    using (SqlDataAdapter adptr = new SqlDataAdapter(cmd))
                    {
                        using (DataSet ds = new DataSet())
                        {
                            adptr.Fill(ds);

                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                int CIndex = 1, Lang = 0;
                                string _Lan = f.tekbirsonucdonder("select Case When Language=0 Then 'TR' When Language=1 Then 'EN' When Language=2 Then 'FR' Else 'TR' END " +
                                    "from [0C_50005_00_YETKILENDIRME] where [Kullanıcı Adı]='" + _obj.UserName + "' and COMPANY ='" + _obj.Comp + "'");
                                if (_Lan == "TR")
                                    Lang = 0;
                                else if (_Lan == "EN")
                                    Lang = 1;
                                else if (_Lan == "FR")
                                    Lang = 2;
                                else
                                    Lang = 0;
                                string LookupsSorgu = string.Empty;
                                foreach (DataRow row in ds.Tables[0].Rows)
                                {
                                    SetComponentVisibility("tr" + CIndex.ToString(), true);
                                    Label lbl = GetLabelFromDynamicPanel("Label" + CIndex.ToString());
                                    if (lbl != null) lbl.Text = Lang == 0 ? row["TR"].ToString() : Lang == 1 ? row["EN"].ToString() : Lang == 2 ? row["FR"].ToString() : row["TR"].ToString();

                                    if (row["Zorunlu"].ToString() == "1")
                                        lbl.Text = lbl.Text + "*";

                                    switch (row["Alan Turu"].ToString().Substring(0, 3))
                                    {
                                        case "TAR":
                                            DataRow dr = this.STDT1.Table.NewRow();
                                            dr["FieldId"] = "Tar" + CIndex.ToString();
                                            dr["FieldType"] = "TAR";
                                            dr["FieldNavNo"] = row["Alan No NAV"].ToString();
                                            dr["IsMandatory"] = row["Zorunlu"].ToString() == "1" ? 1 : 0;
                                            dr["FieldName"] = lbl.Text;
                                            dr["FieldNavName"] = row["Alan NAV Adi"].ToString();
                                            dr["IsLookups"] = 0;
                                            dr["Kaynak_Tablo"] = row["Kaynak Tablo"].ToString();
                                            dr["Lookups_Tur"] = row["Lookups Tur"].ToString();
                                            dr["Kaynak_Tablo_Alan_Adi"] = row["Kaynak Tablo Alan Adi"].ToString();
                                            this.STDT1.Table.Rows.Add(dr);
                                            ((ASPxDateEdit)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tar" + CIndex.ToString())).Visible = true;
                                            ((ASPxTextBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tex" + CIndex.ToString())).Visible = false;
                                            ((ASPxCheckBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Chk" + CIndex.ToString())).Visible = false;
                                            ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).Visible = false;
                                            break;
                                        case "TEX":
                                            if (!string.IsNullOrEmpty(row["Kaynak Tablo"].ToString()))
                                            {
                                                DataRow dr1 = this.STDT1.Table.NewRow();
                                                dr1["FieldId"] = "Cmb" + CIndex.ToString();
                                                dr1["FieldType"] = "TEX";
                                                dr1["FieldNavNo"] = row["Alan No NAV"].ToString();
                                                dr1["IsMandatory"] = row["Zorunlu"].ToString() == "1" ? 1 : 0;
                                                dr1["FieldName"] = lbl.Text;
                                                dr1["FieldNavName"] = row["Alan NAV Adi"].ToString();
                                                dr1["Kaynak_Tablo"] = row["Kaynak Tablo"].ToString();
                                                dr1["Lookups_Tur"] = row["Lookups Tur"].ToString();
                                                dr1["Kaynak_Tablo_Alan_Adi"] = row["Kaynak Tablo Alan Adi"].ToString();
                                                if (row["Kaynak Tablo"].ToString() == "Lookups")
                                                {

                                                    dr1["IsLookups"] = 1;
                                                    this.STDT1.Table.Rows.Add(dr1);
                                                    LookupsSorgu = "Select [LineNo] Code, Adi from [0C_50055_00_LOOKUPS] where " +
                                                        "COMPANY='" + _obj.Comp + "' AND Tur='" + row["Lookups Tur"].ToString() + "' Order By [LineNo]";
                                                    ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).SelectCommand = LookupsSorgu;
                                                    ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).DataBind();

                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).ValueField = "Adi";
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).TextField = "Adi";
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).DataSource = ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString()));
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).DataBind();

                                                }
                                                else
                                                {
                                                    dr1["IsLookups"] = 2;
                                                    this.STDT1.Table.Rows.Add(dr1);
                                                    LookupsSorgu = "Select [" + row["Kaynak Tablo Alan Adi"].ToString().Split(',')[0] + "] Code, [" + row["Kaynak Tablo Alan Adi"].ToString().Split(',')[1] + "] Adi from " + row["Kaynak Tablo"].ToString() + "  Order By [" + row["Kaynak Tablo Alan Adi"].ToString().Split(',')[1] + "]";
                                                    ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).SelectCommand = LookupsSorgu;
                                                    ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).DataBind();

                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).ValueField = "Code";
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).TextField = "Adi";
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).DataSource = ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString()));
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).EnableCallbackMode = true;
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).CallbackPageSize = 20;
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).DataBind();
                                                }

                                                ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).Visible = true;
                                                ((ASPxDateEdit)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tar" + CIndex.ToString())).Visible = false;
                                                ((ASPxCheckBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Chk" + CIndex.ToString())).Visible = false;
                                                ((ASPxTextBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tex" + CIndex.ToString())).Visible = false;
                                            }
                                            else
                                            {
                                                DataRow dr1 = this.STDT1.Table.NewRow();
                                                dr1["FieldId"] = "Tex" + CIndex.ToString();
                                                dr1["FieldType"] = "TEX";
                                                dr1["FieldNavNo"] = row["Alan No NAV"].ToString();
                                                dr1["IsMandatory"] = row["Zorunlu"].ToString() == "1" ? 1 : 0;
                                                dr1["FieldName"] = lbl.Text;
                                                dr1["FieldNavName"] = row["Alan NAV Adi"].ToString();
                                                dr1["IsLookups"] = 0;
                                                dr1["Kaynak_Tablo"] = row["Kaynak Tablo"].ToString();
                                                dr1["Lookups_Tur"] = row["Lookups Tur"].ToString();
                                                dr1["Kaynak_Tablo_Alan_Adi"] = row["Kaynak Tablo Alan Adi"].ToString();
                                                this.STDT1.Table.Rows.Add(dr1);
                                                ((ASPxTextBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tex" + CIndex.ToString())).Visible = true;
                                                ((ASPxDateEdit)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tar" + CIndex.ToString())).Visible = false;
                                                ((ASPxCheckBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Chk" + CIndex.ToString())).Visible = false;
                                                ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).Visible = false;
                                            }
                                            break;
                                        case "COD":
                                            if (!string.IsNullOrEmpty(row["Kaynak Tablo"].ToString()))
                                            {
                                                DataRow dr2 = this.STDT1.Table.NewRow();
                                                dr2["FieldId"] = "Tex" + CIndex.ToString();
                                                dr2["FieldType"] = "TEX";
                                                dr2["FieldNavNo"] = row["Alan No NAV"].ToString();
                                                dr2["IsMandatory"] = row["Zorunlu"].ToString() == "1" ? 1 : 0;
                                                dr2["FieldName"] = lbl.Text;
                                                dr2["FieldNavName"] = row["Alan NAV Adi"].ToString();
                                                dr2["Kaynak_Tablo"] = row["Kaynak Tablo"].ToString();
                                                dr2["Lookups_Tur"] = row["Lookups Tur"].ToString();
                                                dr2["Kaynak_Tablo_Alan_Adi"] = row["Kaynak Tablo Alan Adi"].ToString();

                                                if (row["Kaynak Tablo"].ToString() == "Lookups")
                                                {
                                                    dr2["IsLookups"] = 1;
                                                    this.STDT1.Table.Rows.Add(dr2);
                                                    LookupsSorgu = "Select [LineNo] Code, Adi from [0C_50055_00_LOOKUPS] where " +
                                                        "COMPANY='" + _obj.Comp + "' AND Tur='" + row["Lookups Tur"].ToString() + "' Order By [LineNo]";
                                                    ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).SelectCommand = LookupsSorgu;
                                                    ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).DataBind();

                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).ValueField = "Adi";
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).TextField = "Adi";
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).DataSource = ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString()));
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).DataBind();

                                                }
                                                else
                                                {
                                                    dr2["IsLookups"] = 2;
                                                    this.STDT1.Table.Rows.Add(dr2);
                                                    LookupsSorgu = "Select [" + row["Kaynak Tablo Alan Adi"].ToString().Split(',')[0] + "] Code, [" + row["Kaynak Tablo Alan Adi"].ToString().Split(',')[1] + "] Adi from " + row["Kaynak Tablo"].ToString() + "  Order By [" + row["Kaynak Tablo Alan Adi"].ToString().Split(',')[1] + "]";
                                                    ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).SelectCommand = LookupsSorgu;
                                                    ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).DataBind();

                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).ValueField = "Code";
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).TextField = "Adi";
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).DataSource = ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString()));
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).EnableCallbackMode = true;
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).CallbackPageSize = 20;
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).DataBind();
                                                }

                                                ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).Visible = true;
                                                ((ASPxDateEdit)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tar" + CIndex.ToString())).Visible = false;
                                                ((ASPxCheckBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Chk" + CIndex.ToString())).Visible = false;
                                                ((ASPxTextBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tex" + CIndex.ToString())).Visible = false;
                                            }
                                            else
                                            {
                                                DataRow dr2 = this.STDT1.Table.NewRow();
                                                dr2["FieldId"] = "Tex" + CIndex.ToString();
                                                dr2["FieldType"] = "TEX";
                                                dr2["FieldNavNo"] = row["Alan No NAV"].ToString();
                                                dr2["IsMandatory"] = row["Zorunlu"].ToString() == "1" ? 1 : 0;
                                                dr2["FieldName"] = lbl.Text;
                                                dr2["FieldNavName"] = row["Alan NAV Adi"].ToString();
                                                dr2["IsLookups"] = 0;
                                                dr2["Kaynak_Tablo"] = row["Kaynak Tablo"].ToString();
                                                dr2["Lookups_Tur"] = row["Lookups Tur"].ToString();
                                                dr2["Kaynak_Tablo_Alan_Adi"] = row["Kaynak Tablo Alan Adi"].ToString();
                                                this.STDT1.Table.Rows.Add(dr2);
                                                ((ASPxTextBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tex" + CIndex.ToString())).Visible = true;
                                                ((ASPxDateEdit)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tar" + CIndex.ToString())).Visible = false;
                                                ((ASPxCheckBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Chk" + CIndex.ToString())).Visible = false;
                                                ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).Visible = false;

                                            }
                                            break;
                                        case "INT":
                                            if (!string.IsNullOrEmpty(row["Kaynak Tablo"].ToString()))
                                            {
                                                DataRow dr3 = this.STDT1.Table.NewRow();
                                                dr3["FieldId"] = "Cmb" + CIndex.ToString();
                                                dr3["FieldType"] = "INT";
                                                dr3["FieldNavNo"] = row["Alan No NAV"].ToString();
                                                dr3["IsMandatory"] = row["Zorunlu"].ToString() == "1" ? 1 : 0;
                                                dr3["FieldName"] = lbl.Text;
                                                dr3["FieldNavName"] = row["Alan NAV Adi"].ToString();
                                                dr3["IsLookups"] = 1;
                                                dr3["Kaynak_Tablo"] = row["Kaynak Tablo"].ToString();
                                                dr3["Lookups_Tur"] = row["Lookups Tur"].ToString();
                                                dr3["Kaynak_Tablo_Alan_Adi"] = row["Kaynak Tablo Alan Adi"].ToString();
                                                this.STDT1.Table.Rows.Add(dr3);

                                                if (row["Kaynak Tablo"].ToString() == "Lookups")
                                                {
                                                    LookupsSorgu = "Select [LineNo] Code, Adi from [0C_50055_00_LOOKUPS] where " +
                                                        "COMPANY='" + _obj.Comp + "' AND Tur='" + row["Lookups Tur"].ToString() + "' Order By [LineNo]";
                                                    ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).SelectCommand = LookupsSorgu;
                                                    ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString())).DataBind();

                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).ValueField = "Code";
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).TextField = "Adi";
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).DataSource = ((SqlDataSource)this.Master.FindControl("ContentPlaceHolder1").FindControl("Ds" + CIndex.ToString()));
                                                    ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).DataBind();

                                                }
                                                ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).Visible = true;
                                                ((ASPxDateEdit)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tar" + CIndex.ToString())).Visible = false;
                                                ((ASPxCheckBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Chk" + CIndex.ToString())).Visible = false;
                                                ((ASPxTextBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tex" + CIndex.ToString())).Visible = false;
                                            }
                                            break;
                                        case "BOO":
                                            DataRow dr4 = this.STDT1.Table.NewRow();
                                            dr4["FieldId"] = "Chk" + CIndex.ToString();
                                            dr4["FieldType"] = "BOO";
                                            dr4["FieldNavNo"] = row["Alan No NAV"].ToString();
                                            dr4["IsMandatory"] = row["Zorunlu"].ToString() == "1" ? 1 : 0;
                                            dr4["FieldName"] = lbl.Text;
                                            dr4["FieldNavName"] = row["Alan NAV Adi"].ToString();
                                            dr4["IsLookups"] = 0;
                                            dr4["Kaynak_Tablo"] = row["Kaynak Tablo"].ToString();
                                            dr4["Lookups_Tur"] = row["Lookups Tur"].ToString();
                                            dr4["Kaynak_Tablo_Alan_Adi"] = row["Kaynak Tablo Alan Adi"].ToString();
                                            this.STDT1.Table.Rows.Add(dr4);
                                            ((ASPxCheckBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Chk" + CIndex.ToString())).Visible = true;
                                            ((ASPxDateEdit)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tar" + CIndex.ToString())).Visible = false;
                                            ((ASPxTextBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tex" + CIndex.ToString())).Visible = false;
                                            ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).Visible = false;
                                            break;
                                        case "DEC":
                                            DataRow dr5 = this.STDT1.Table.NewRow();
                                            dr5["FieldId"] = "Tex" + CIndex.ToString();
                                            dr5["FieldType"] = "DEC";
                                            dr5["FieldNavNo"] = row["Alan No NAV"].ToString();
                                            dr5["IsMandatory"] = row["Zorunlu"].ToString() == "1" ? 1 : 0;
                                            dr5["FieldName"] = lbl.Text;
                                            dr5["FieldNavName"] = row["Alan NAV Adi"].ToString();
                                            dr5["IsLookups"] = 0;
                                            dr5["Kaynak_Tablo"] = row["Kaynak Tablo"].ToString();
                                            dr5["Lookups_Tur"] = row["Lookups Tur"].ToString();
                                            dr5["Kaynak_Tablo_Alan_Adi"] = row["Kaynak Tablo Alan Adi"].ToString();
                                            this.STDT1.Table.Rows.Add(dr5);
                                            ((ASPxTextBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tex" + CIndex.ToString())).Visible = true;
                                            ((ASPxDateEdit)this.Master.FindControl("ContentPlaceHolder1").FindControl("Tar" + CIndex.ToString())).Visible = false;
                                            ((ASPxCheckBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Chk" + CIndex.ToString())).Visible = false;
                                            ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl("Cmb" + CIndex.ToString())).Visible = false;
                                            break;
                                    }
                                    CIndex++;
                                }
                                this.STDT1.Table.AcceptChanges();

                            }
                            else
                            {

                            }
                        }

                    }
                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "[SP_0WEB_DOKUMAN_GRUBU_YETKI_ARA]";
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@DocNo", this.cmbDocumenType.Value.ToString());
                    cmd.Parameters.AddWithValue("@Company", _obj.Comp);
                    cmd.Parameters.AddWithValue("@UserName", _obj.UserName);

                    using (SqlDataAdapter adptr = new SqlDataAdapter(cmd))
                    {
                        using (DataSet ds = new DataSet())
                        {
                            adptr.Fill(ds);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                if (Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) > 0)
                                {
                                    btnDosyaBagla.Enabled = true;
                                    btnAddComment.Enabled = true;
                                    btnDelete.Enabled = true;
                                    btnShowComments.OnClientClick = "return BindCommentGrid()";
                                }
                                else
                                {
                                    btnDosyaBagla.Enabled = false;
                                    btnAddComment.Enabled = false;
                                    btnDelete.Enabled = false;
                                    btnShowComments.Enabled = false;
                                    btnShowComments.OnClientClick = "";
                                }
                            }
                        }
                    }

                }
            }
        }

        void clearValues()
        {
            Tex1.Text = string.Empty;
            Tex2.Text = string.Empty;
            Tex3.Text = string.Empty;
            Tex4.Text = string.Empty;
            Tex5.Text = string.Empty;
            Tex6.Text = string.Empty;
            Tex7.Text = string.Empty;
            Tex8.Text = string.Empty;
            Tex9.Text = string.Empty;
            Tex10.Text = string.Empty;
            Tex11.Text = string.Empty;
            Tex12.Text = string.Empty;
            Tex13.Text = string.Empty;
            Tex14.Text = string.Empty;
            Tex15.Text = string.Empty;
            Tex16.Text = string.Empty;
            Tex17.Text = string.Empty;
            Tex18.Text = string.Empty;
            Tex19.Text = string.Empty;
            Tex20.Text = string.Empty;
            Tex21.Text = string.Empty;
            Tex22.Text = string.Empty;
            Tex23.Text = string.Empty;
            Tex24.Text = string.Empty;
            Tex25.Text = string.Empty;

            Tar1.Text = string.Empty;
            Tar2.Text = string.Empty;
            Tar3.Text = string.Empty;
            Tar4.Text = string.Empty;
            Tar5.Text = string.Empty;
            Tar6.Text = string.Empty;
            Tar7.Text = string.Empty;
            Tar8.Text = string.Empty;
            Tar9.Text = string.Empty;
            Tar10.Text = string.Empty;
            Tar11.Text = string.Empty;
            Tar12.Text = string.Empty;
            Tar13.Text = string.Empty;
            Tar14.Text = string.Empty;
            Tar15.Text = string.Empty;
            Tar16.Text = string.Empty;
            Tar17.Text = string.Empty;
            Tar18.Text = string.Empty;
            Tar19.Text = string.Empty;
            Tar20.Text = string.Empty;
            Tar21.Text = string.Empty;
            Tar22.Text = string.Empty;
            Tar23.Text = string.Empty;
            Tar24.Text = string.Empty;
            Tar25.Text = string.Empty;

            Cmb1.SelectedIndex = -1;
            Cmb2.SelectedIndex = -1;
            Cmb3.SelectedIndex = -1;
            Cmb4.SelectedIndex = -1;
            Cmb5.SelectedIndex = -1;
            Cmb6.SelectedIndex = -1;
            Cmb7.SelectedIndex = -1;
            Cmb8.SelectedIndex = -1;
            Cmb9.SelectedIndex = -1;
            Cmb10.SelectedIndex = -1;
            Cmb11.SelectedIndex = -1;
            Cmb12.SelectedIndex = -1;
            Cmb13.SelectedIndex = -1;
            Cmb14.SelectedIndex = -1;
            Cmb15.SelectedIndex = -1;
            Cmb16.SelectedIndex = -1;
            Cmb17.SelectedIndex = -1;
            Cmb18.SelectedIndex = -1;
            Cmb19.SelectedIndex = -1;
            Cmb20.SelectedIndex = -1;
            Cmb21.SelectedIndex = -1;
            Cmb22.SelectedIndex = -1;
            Cmb23.SelectedIndex = -1;
            Cmb24.SelectedIndex = -1;
            Cmb25.SelectedIndex = -1;

            Chk1.Checked = false;
            Chk2.Checked = false;
            Chk3.Checked = false;
            Chk4.Checked = false;
            Chk5.Checked = false;
            Chk6.Checked = false;
            Chk7.Checked = false;
            Chk8.Checked = false;
            Chk9.Checked = false;
            Chk10.Checked = false;
            Chk11.Checked = false;
            Chk12.Checked = false;
            Chk13.Checked = false;
            Chk14.Checked = false;
            Chk15.Checked = false;
            Chk16.Checked = false;
            Chk17.Checked = false;
            Chk18.Checked = false;
            Chk19.Checked = false;
            Chk20.Checked = false;
            Chk21.Checked = false;
            Chk22.Checked = false;
            Chk23.Checked = false;
            Chk24.Checked = false;
            Chk25.Checked = false;

        }

        void SetComponentVisibility(string _FieldId, bool _visible)
        {
            try
            {
                var container = this.Master.FindControl("ContentPlaceHolder1");
                var control = container.FindControl(_FieldId);
                if (control == null) return;
                if (control.GetType() == typeof(HtmlTableRow))
                    control.Visible = _visible;
            }
            catch { }

        }

        private Label GetLabelFromDynamicPanel(string _FieldId)
        {
            try
            {
                var container = this.Master.FindControl("ContentPlaceHolder1");
                var control = container.FindControl(_FieldId);
                if (control == null) return null;
                if (control.GetType() == typeof(Label))
                {
                    return (Label)control;
                }

            }
            catch { return null; }
            return null;
        }
        private HtmlTableCell GetTableCellFromDynamicPanel(string _FieldId)
        {
            try
            {
                var container = this.Master.FindControl("ContentPlaceHolder1");
                var control = container.FindControl(_FieldId);
                if (control == null) return null;
                if (control.GetType() == typeof(HtmlTableCell))
                {
                    return (HtmlTableCell)control;
                }

            }
            catch { return null; }
            return null;
        }


        protected void btnDosyaBagla_Click(object sender, EventArgs e)
        {

            int Company = 0, DocumentGroup = 0;
            string DocumentType = string.Empty;
            string DocumentTarget = string.Empty;

            if (string.IsNullOrEmpty(cmbCompany.Text))
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg01"));
                return;
            }
            if (string.IsNullOrEmpty(cmbDocumentGroup.Text))
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg02"));
                return;
            }
            if (string.IsNullOrEmpty(cmbDocumenType.Text))
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg03"));
                return;
            }

            try
            {
                Company = Convert.ToInt32(f.tekbirsonucdonder("select top 1 [LineNo] from [0C_50055_00_LOOKUPS] where " +
                    "COMPANY='" + _obj.Comp + "' AND Tur='Izin Talep Company' and Adi='" + cmbCompany.Text + "'"));
            }
            catch
            {
                Company = 0;
            }

            try
            {
                DocumentGroup = Convert.ToInt32(f.tekbirsonucdonder("select top 1 [LineNo] from [0C_50055_00_LOOKUPS] where " +
                    "COMPANY='" + _obj.Comp + "' AND Tur='Dokuman Grubu' and Adi='" + cmbDocumentGroup.Text + "'"));
            }
            catch
            {
                DocumentGroup = 0;
            }

            try
            {
                DocumentType = f.tekbirsonucdonder("select top 1 [Belge No] from [0C_70000_00_DOCUMENTS GENEL]  " +
                    "Where COMPANY='" + _obj.Comp + "' AND [Dokuman Grubu]=" + DocumentGroup.ToString() + " and  [Belge Adi]='" + cmbDocumenType.Text + "'");
            }
            catch
            {
                DocumentType = string.Empty;
            }

            try
            {

                using (DataTable dt = new DataTable())
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.CommandText = "[sp_0WEB_DOSYA_BAGLA_YENI]";
                            cmd.Connection = conn;
                            cmd.CommandTimeout = 500;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Clear();
                            cmd.Parameters.AddWithValue("@COMP", _obj.Comp);
                            cmd.Parameters.AddWithValue("@PRM1", DocumentGroup.ToString());

                            using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                            {
                                adp.Fill(dt);
                            }
                            if (dt.Rows.Count > 0)
                                DocumentTarget = dt.Rows[0]["PATH"].ToString();
                            else
                                DocumentTarget = string.Empty;
                        }
                    }

                }
            }
            catch
            {
                DocumentTarget = string.Empty;
            }


            foreach (DataRow row in STDT1.Table.Rows)
            {
                if (row["IsMandatory"].ToString() == "0") continue;
                var container = this.Master.FindControl("ContentPlaceHolder1");
                var control = container.FindControl(row["FieldId"].ToString());
                if (control == null) continue;
                if (row["FieldType"].ToString() == "TEX")
                {
                    if (control.GetType() == typeof(ASPxTextBox))
                    {
                        if (string.IsNullOrEmpty(((ASPxTextBox)control).Text))
                        {
                            MessageBox(row["FieldName"].ToString() + " " + l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg04"));
                            return;
                        }
                    }
                    else if (control.GetType() == typeof(ASPxComboBox))
                    {
                        if (string.IsNullOrEmpty(((ASPxComboBox)control).Text))
                        {
                            MessageBox(row["FieldName"].ToString() + " " + l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg04"));
                            return;
                        }
                    }
                }
                else if (row["FieldType"].ToString() == "COD")
                {
                    if (control.GetType() == typeof(ASPxTextBox))
                    {
                        if (string.IsNullOrEmpty(((ASPxTextBox)control).Text))
                        {
                            MessageBox(row["FieldName"].ToString() + " " + l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg04"));
                            return;
                        }
                    }
                    else if (control.GetType() == typeof(ASPxComboBox))
                    {
                        if (string.IsNullOrEmpty(((ASPxComboBox)control).Text))
                        {
                            MessageBox(row["FieldName"].ToString() + " " + l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg04"));
                            return;
                        }
                    }
                }
                else if (row["FieldType"].ToString() == "TAR")
                {
                    if (control.GetType() == typeof(ASPxDateEdit))
                    {
                        if (string.IsNullOrEmpty(((ASPxDateEdit)control).Text))
                        {
                            MessageBox(row["FieldName"].ToString() + " " + l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg04"));
                            return;
                        }
                    }
                }
                else if (row["FieldType"].ToString() == "INT")
                {
                    if (control.GetType() == typeof(ASPxComboBox))
                    {
                        if (string.IsNullOrEmpty(((ASPxComboBox)control).Text))
                        {
                            MessageBox(row["FieldName"].ToString() + " " + l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg04"));
                            return;
                        }
                    }
                }
                else if (row["FieldType"].ToString() == "BOO")
                {
                    if (control.GetType() == typeof(ASPxCheckBox))
                    {
                        if (!((ASPxCheckBox)control).Checked)
                        {
                            MessageBox(row["FieldName"].ToString() + " " + l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg04"));
                            return;
                        }
                    }
                }
                else if (row["FieldType"].ToString() == "DEC")
                {
                    if (control.GetType() == typeof(ASPxTextBox))
                    {
                        if (string.IsNullOrEmpty(((ASPxTextBox)control).Text))
                        {
                            MessageBox(row["FieldName"].ToString() + " " + l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg04"));
                            return;
                        }
                    }
                }


            }

            string extn = string.Empty;
            string save_path = string.Empty, dosyaupload = string.Empty;
            int tur = Convert.ToInt32(cmbDocumentGroup.Value.ToString());
            int _NextEntryNo = Convert.ToInt32(Request.QueryString["EntryNo"].ToString());
            if (FileUpload1.HasFile)
            {
                extn = System.IO.Path.GetExtension(FileUpload1.FileName);
                if (extn.ToLower() == ".pdf")
                {

                    if (!Directory.Exists(Server.MapPath(DocumentTarget)))
                        Directory.CreateDirectory(Server.MapPath(DocumentTarget));
                    save_path = ".\\" + DocumentTarget + lblno.Text + _NextEntryNo.ToString() + extn;
                    dosyaupload = Server.MapPath(DocumentTarget) + lblno.Text + _NextEntryNo.ToString() + extn;
                    if (File.Exists(dosyaupload.Trim()))
                        File.Delete(dosyaupload.Trim());
                    FileUpload1.PostedFile.SaveAs(dosyaupload.Trim());

                    // Musa 23.02.2019 Target folder dinamik hale getirilerek Lookup tablosundan okunmaya başlandı.
                    //switch (tur)
                    //{
                    //    case 1:
                    //        if (!Directory.Exists(Server.MapPath("Dosya\\MUSTERI\\")))
                    //            Directory.CreateDirectory(Server.MapPath("Dosya\\MUSTERI\\"));
                    //        save_path = ".\\Dosya\\MUSTERI\\" + lblno.Text + _NextEntryNo.ToString() + extn;
                    //        dosyaupload = Server.MapPath("Dosya\\MUSTERI\\") + lblno.Text + _NextEntryNo.ToString() + extn;
                    //        if (File.Exists(dosyaupload.Trim()))
                    //            File.Delete(dosyaupload.Trim());
                    //        FileUpload1.PostedFile.SaveAs(dosyaupload.Trim());
                    //        break;
                    //    case 2:
                    //        if (!Directory.Exists(Server.MapPath("Dosya\\TEDARIKCI\\")))
                    //            Directory.CreateDirectory(Server.MapPath("Dosya\\TEDARIKCI\\"));
                    //        save_path = ".\\Dosya\\TEDARIKCI\\" + lblno.Text + _NextEntryNo.ToString() + extn;
                    //        dosyaupload = Server.MapPath("Dosya\\TEDARIKCI\\") + lblno.Text + _NextEntryNo.ToString() + extn;
                    //        if (File.Exists(dosyaupload.Trim()))
                    //            File.Delete(dosyaupload.Trim());
                    //        FileUpload1.PostedFile.SaveAs(dosyaupload.Trim());
                    //        break;
                    //    case 3:
                    //        if (!Directory.Exists(Server.MapPath("Dosya\\ADAY\\")))
                    //            Directory.CreateDirectory(Server.MapPath("Dosya\\ADAY\\"));
                    //        save_path = ".\\Dosya\\ADAY\\" + lblno.Text + _NextEntryNo.ToString() + extn;
                    //        dosyaupload = Server.MapPath("Dosya\\ADAY\\") + lblno.Text + _NextEntryNo.ToString() + extn;
                    //        if (File.Exists(dosyaupload.Trim()))
                    //            File.Delete(dosyaupload.Trim());
                    //        FileUpload1.PostedFile.SaveAs(dosyaupload.Trim());
                    //        break;
                    //    case 4:
                    //        if (!Directory.Exists(Server.MapPath("Dosya\\JOB\\")))
                    //            Directory.CreateDirectory(Server.MapPath("Dosya\\JOB\\"));
                    //        save_path = ".\\Dosya\\JOB\\" + lblno.Text + _NextEntryNo.ToString() + extn;
                    //        dosyaupload = Server.MapPath("Dosya\\JOB\\") + lblno.Text + _NextEntryNo.ToString() + extn;
                    //        if (File.Exists(dosyaupload.Trim()))
                    //            File.Delete(dosyaupload.Trim());
                    //        FileUpload1.PostedFile.SaveAs(dosyaupload.Trim());
                    //        break;
                    //    case 5:
                    //        if (!Directory.Exists(Server.MapPath("Dosya\\ILETISIM\\")))
                    //            Directory.CreateDirectory(Server.MapPath("Dosya\\ILETISIM\\"));
                    //        save_path = ".\\Dosya\\ILETISIM\\" + lblno.Text + _NextEntryNo.ToString() + extn;
                    //        dosyaupload = Server.MapPath("Dosya\\ILETISIM\\") + lblno.Text + _NextEntryNo.ToString() + extn;
                    //        if (File.Exists(dosyaupload.Trim()))
                    //            File.Delete(dosyaupload.Trim());
                    //        FileUpload1.PostedFile.SaveAs(dosyaupload.Trim());
                    //        break;
                    //    case 6:
                    //        if (!Directory.Exists(Server.MapPath("Dosya\\TAYFA\\")))
                    //            Directory.CreateDirectory(Server.MapPath("Dosya\\TAYFA\\"));
                    //        save_path = ".\\Dosya\\TAYFA\\" + lblno.Text + _NextEntryNo.ToString() + extn;
                    //        dosyaupload = Server.MapPath("Dosya\\TAYFA\\") + lblno.Text + _NextEntryNo.ToString() + extn;
                    //        if (File.Exists(dosyaupload.Trim()))
                    //            File.Delete(dosyaupload.Trim());
                    //        FileUpload1.PostedFile.SaveAs(dosyaupload.Trim());
                    //        break;
                    //    case 7:
                    //        if (!Directory.Exists(Server.MapPath("Dosya\\PERSONEL\\")))
                    //            Directory.CreateDirectory(Server.MapPath("Dosya\\PERSONEL\\"));
                    //        save_path = ".\\Dosya\\PERSONEL\\" + lblno.Text + _NextEntryNo.ToString() + extn;
                    //        dosyaupload = Server.MapPath("Dosya\\PERSONEL\\") + lblno.Text + _NextEntryNo.ToString() + extn;
                    //        if (File.Exists(dosyaupload.Trim()))
                    //            File.Delete(dosyaupload.Trim());
                    //        FileUpload1.PostedFile.SaveAs(dosyaupload.Trim());
                    //        break;
                    //    case 8:
                    //        if (!Directory.Exists(Server.MapPath("Dosya\\DAHILI\\")))
                    //            Directory.CreateDirectory(Server.MapPath("Dosya\\DAHILI\\"));
                    //        save_path = ".\\Dosya\\DAHILI\\" + lblno.Text + _NextEntryNo.ToString() + extn;
                    //        dosyaupload = Server.MapPath("Dosya\\DAHILI\\") + lblno.Text + _NextEntryNo.ToString() + extn;
                    //        if (File.Exists(dosyaupload.Trim()))
                    //            File.Delete(dosyaupload.Trim());
                    //        FileUpload1.PostedFile.SaveAs(dosyaupload.Trim());
                    //        break;
                    //    case 9:
                    //        if (!Directory.Exists(Server.MapPath("Dosya\\MUTABAKAT\\")))
                    //            Directory.CreateDirectory(Server.MapPath("Dosya\\MUTABAKAT\\"));
                    //        save_path = ".\\Dosya\\MUTABAKAT\\" + lblno.Text + _NextEntryNo.ToString() + extn;
                    //        dosyaupload = Server.MapPath("Dosya\\MUTABAKAT\\") + lblno.Text + _NextEntryNo.ToString() + extn;
                    //        if (File.Exists(dosyaupload.Trim()))
                    //            File.Delete(dosyaupload.Trim());
                    //        FileUpload1.PostedFile.SaveAs(dosyaupload.Trim());
                    //        break;
                    //}
                }
                else
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentEntry", "msg06"));
                    return;
                }
            }
            else
            {
                save_path = string.Empty;
                lblno.Text = string.Empty;
            }


 

            string FieldsAndValues = string.Empty, FieldValue = string.Empty, FieldType = string.Empty;
            bool ilk = true;
            foreach (DataRow row in STDT1.Table.Rows)
            {
                if (row["FieldType"].ToString() == "TEX" || row["FieldType"].ToString() == "COD")
                    FieldType = "TEX";
                else
                    FieldType = row["FieldType"].ToString();

                if (FieldType == "TAR")
                    FieldValue = string.Format("{0:MM.dd.yyyy}", ((ASPxDateEdit)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Date);
                else if (FieldType == "TEX")
                {
                    if (row["IsLookups"].ToString() == "0")
                        FieldValue = ((ASPxTextBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Text;
                    else if (row["IsLookups"].ToString() == "1")
                        FieldValue = ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Text;
                    else if (row["IsLookups"].ToString() == "2")
                        FieldValue = ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Value.ToString();
                }
                else if (FieldType == "INT")
                    FieldValue = ((ASPxComboBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Value.ToString();
                else if (FieldType == "BOO")
                    FieldValue = ((ASPxCheckBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Checked ? "YES" : "NO";
                else if (FieldType == "DEC")
                    FieldValue = ((ASPxTextBox)this.Master.FindControl("ContentPlaceHolder1").FindControl(row["FieldId"].ToString())).Text.Replace(",", ".");
                if (ilk)
                {
                    FieldsAndValues = row["FieldNavNo"].ToString() + ";" + FieldValue.Replace(";", string.Empty).Replace(",", string.Empty) + ";" + FieldType;
                    ilk = false;
                }
                else
                    FieldsAndValues += "," + row["FieldNavNo"].ToString() + ";" + FieldValue.Replace(";", string.Empty).Replace(",", string.Empty) + ";" + FieldType;
            }

            string[] _params = { Convert.ToInt32(Request.QueryString["EntryNo"].ToString()).ToString(), Company.ToString(), DocumentGroup.ToString(), DocumentType, FieldsAndValues, _obj.UserName, save_path, lblno.Text };
            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "DocDetailsGenelDuzenle§" + string.Join("§", _params), _obj.Comp);


            Response.Redirect(Request.RawUrl + "&UpdatedDoc=1");



        }


        protected void btnDelete_Click(object sender, EventArgs e)
        {

            string[] _params = { Request.QueryString["EntryNo"].ToString(), _obj.UserName };
            NavServicesContents serviceContent = new NavServicesContents();
            string[] _pars = serviceContent.NavServiceParams("DocDetailsGenelSil", _params);
            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "DocDetailsGenelSil§" + string.Join("§", _params), _obj.Comp);




            int DocumentGroup = 0;
            try
            {
                DocumentGroup = Convert.ToInt32(f.tekbirsonucdonder("select top 1 [LineNo] from [0C_50055_00_LOOKUPS] where " +
                    "COMPANY='" + _obj.Comp + "' AND Tur='Dokuman Grubu' and Adi='" + cmbDocumentGroup.Text + "'"));
            }
            catch
            {
                DocumentGroup = 0;
            }

            Response.Redirect("~/newpage_documentList.aspx?DeletedDoc=1&DocType=" + DocumentGroup.ToString() + "&DocNo=&GId=" + Request.QueryString["GId"]);
        }
        protected void btnAddComment_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtComment.Text))
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp, _obj.UserName, "newpage_documentCard", "msg02"));
                return;
            }

            string[] _params = { Convert.ToInt32(Request.QueryString["EntryNo"].ToString()).ToString(), txtComment.Text, _obj.UserName };
            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "DocDetailsYorumEkle§" + string.Join("§", _params), _obj.Comp);

            Response.Redirect(Request.RawUrl.Replace("&UpdatedDoc=1", string.Empty));
        }

        void BindComments(string EntryNo)
        {
            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "select t1.No_,t1.[Line No_] LnNo,t1.Date Tarih,t1.[Comment Time] Saat," +
                        "t1.Comment,t1.[Olusturan Kullanıcı] Kullanici from [0D_00097_00_COMMENT_LINE] t1 where COMPANY='" + _obj.Comp + "' AND t1.No_=@EntryNo ";
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@EntryNo", EntryNo);

                    using (SqlDataAdapter adptr = new SqlDataAdapter(cmd))
                    {
                        DataTable dt = new DataTable();
                        adptr.Fill(dt);

                        DTComments.Table.Rows.Clear();
                        DTComments.Table.AcceptChanges();
                        foreach (DataRow row in dt.Rows)
                        {
                            DataRow dr = this.DTComments.Table.NewRow();
                            dr["LineNo"] = row["LnNo"];
                            dr["Tarih"] = row["Tarih"];
                            dr["Saat"] = row["Saat"];
                            dr["Comment"] = row["Comment"];
                            dr["Kullanici"] = row["Kullanici"];
                            DTComments.Table.Rows.Add(dr);
                        }
                        DTComments.Table.AcceptChanges();
                        grid.DataBind();
                    }
                }
            }
        }

        protected void grid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            BindComments(Request.QueryString["EntryNo"].ToString());
        }
    }
}