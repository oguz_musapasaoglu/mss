﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="shipOlustur.aspx.cs" Inherits="MSS1.shipOlustur" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        #overlay {
            position: fixed;
            z-index: 3;
            top: 0px;
            left: 0px;
            background-color: #f8f8f8;
            width: 100%;
            height: 100%;
            filter: Alpha(Opacity=90);
            opacity: 0.9;
            -moz-opacity: 0.9;
            visibility: hidden;
        }

        #theprogress {
            background-color: #fff;
            border: 1px solid #ccc;
            padding: 10px;
            width: 300px;
            height: 70px;
            line-height: 30px;
            text-align: center;
            filter: Alpha(Opacity=100);
            opacity: 1;
            -moz-opacity: 1;
        }

        #modalprogress {
            position: absolute;
            top: 40%;
            left: 50%;
            margin: -11px 0 0 -150px;
            color: #990000;
            font-weight: bold;
            font-size: 14px;
        }
    </style>

    <script type="text/javascript" language="JavaScript">

        function displayLoadingImage() {
            var imgLoading = document.getElementById("imgLoading");
            var overlay = document.getElementById("overlay");
            var lblyukleniyor = document.getElementById("ContentPlaceHolder1_lblyukleniyor");
            overlay.style.visibility = "visible";
            imgLoading.style.visibility = "visible";
            lblyukleniyor.style.visibility = "visible";
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table cellpadding="8" cellspacing="8">
        <tr>
            <td colspan="3">
                <table cellspacing="8">
                    <tr>
                        <td colspan="2">
                            <h2>
                                <asp:Label ID="lblBaslik" runat="server"></asp:Label></h2>
                        </td>
                    </tr>
                    <tr>
                        <td width="150">
                            <asp:Label ID="lblShipName" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtShipName" runat="server" CssClass="textbox" Width="207px"></asp:TextBox>
                        </td>
                    </tr>
                     <tr>
                        <td width="150">
                            <asp:Label ID="lblImoNo" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtImoNo" runat="server" CssClass="textbox" Width="207px"></asp:TextBox>
                        </td>
                    </tr>
                     <tr>
                        <td width="150">
                            <asp:Label ID="lblFlag" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlFlag" AppendDataBoundItems="true" CssClass="textbox" Width="210px" runat="server">
                                <asp:ListItem Selected="True" Value="-1" Text=""> </asp:ListItem>
                            </asp:DropDownList>
                           <%-- <asp:TextBox ID="txtFlag" runat="server" CssClass="textbox"></asp:TextBox>--%>
                        </td>
                    </tr>
                    <tr>
                        <td width="150">
                            <asp:Label ID="lblExName" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtExName" runat="server" CssClass="textbox" Width="207px"></asp:TextBox>
                        </td>
                    </tr>
                     <tr>
                        <td width="150">
                            <asp:Label ID="lblPasif" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkPasif" runat="server"  Width="207px"></asp:CheckBox>
                        </td>
                    </tr>
                </table>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td align="right">&nbsp;</td>
            <td align="right">
                <asp:Button ID="btnOlustur" runat="server" CssClass="myButton" OnClick="btnOlustur_Click" />
            </td>
            <td>
                <div id="overlay">
                    <div id="modalprogress">
                        <div id="theprogress">
                            <img src="images/ajax-loader.gif" name="imgLoading" id="imgLoading" style="visibility: hidden;">
                            <br />
                            <div id="yukleniyor" style="visibility: hidden;">
                                <asp:Label ID="lblyukleniyor" runat="server" Text="Yükleniyor..."></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>

