﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="dosyagor.aspx.cs" Inherits="MSS1.dosyagor" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">


</script>
    <style type="text/css">
        #overlay {
            position: fixed;
            z-index: 3;
            top: 0px;
            left: 0px;
            background-color: #f8f8f8;
            width: 100%;
            height: 100%;
            filter: Alpha(Opacity=90);
            opacity: 0.9;
            -moz-opacity: 0.9;
            visibility: hidden;
        }

        #theprogress {
            background-color: #fff;
            border: 1px solid #ccc;
            padding: 10px;
            width: 300px;
            height: 70px;
            line-height: 30px;
            text-align: center;
            filter: Alpha(Opacity=100);
            opacity: 1;
            -moz-opacity: 1;
        }

        #modalprogress {
            position: absolute;
            top: 40%;
            left: 50%;
            margin: -11px 0 0 -150px;
            color: #990000;
            font-weight: bold;
            font-size: 14px;
        }
    </style>
    <link href="css/tools.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function displayLoadingImage() {
            var imgLoading = document.getElementById("imgLoading");
            var overlay = document.getElementById("overlay");
            var lblyukleniyor = document.getElementById("lblyukleniyor");
            overlay.style.visibility = "visible";
            imgLoading.style.visibility = "visible";
            lblyukleniyor.style.visibility = "visible";
        }
        function open_URL(_val) {
            var _GId = document.getElementById("HdGId").value;
            var _url = "./dosyaupload.aspx?DosyaPath=" + _val + "&GId=" + _GId;
            window.open(_url, '_blank');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="HdBelgeNo" runat="server" />
        <asp:HiddenField ID="HdGId" runat="server" />
        <asp:ScriptManager ID="scriptManager" runat="server" EnablePageMethods="true" />
        <asp:Label ID="lblbaslik" runat="server"></asp:Label>

        <br />
        <asp:GridView ID="grideklenti" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" BorderStyle="None" CellPadding="0" CssClass="mGrid"
            EmptyDataText="Herhangi bir Kayıt bulunamadı." Font-Bold="False" Font-Size="Small"
            ForeColor="#333333" GridLines="Horizontal" OnPageIndexChanging="grideklenti_PageIndexChanging"
            PagerSettings-Mode="NumericFirstLast" Width="600px" OnRowCommand="grideklenti_RowCommand"
            OnRowDataBound="grideklenti_RowDataBound">
            <RowStyle HorizontalAlign="left" Width="100px" />
            <PagerSettings Mode="NumericFirstLast" />
            <PagerStyle CssClass="pgr" ForeColor="White" HorizontalAlign="Left" />
            <Columns>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblTur" runat="server" Text='<%# Eval("Tur")%>' Visible="false"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lbldosyasno" runat="server" Text='<%# Eval("dosyasno")%>' Visible="false"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblexe" runat="server" Text='<%# Eval("exe")%>' Visible="false"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblDosyaPath" runat="server" Text='<%# Eval("DosyaPath")%>' ></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Dosyatur" HeaderText="Dosya Turu" />
                <asp:TemplateField HeaderText="Dosya No">
                    <ItemTemplate>
                        <asp:Label ID="lblDosyano" runat="server" Text='<%# Eval("Dosyano")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Dosyaadı" HeaderText="Dosya Adı" />
                <asp:BoundField DataField="DosyaYorum" HeaderText="Yorum" />
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label ID="lblCapt" runat="server" Text="Eklenti Durumu" ForeColor="White"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:HyperLink runat="server" ID="hlPlus" Text="İNDİR" class="eklenti"></asp:HyperLink>
                        <%-- <a onclick="open_URL('<%# Eval("DosyaPath") %>','<%# Eval("exe") %>');" class="eklenti">İNDİR</a>--%>
                    </ItemTemplate>
                    <ItemStyle Width="75px" VerticalAlign="Middle" HorizontalAlign="Center" />
                </asp:TemplateField>
                <%-- <asp:HyperLinkField ControlStyle-CssClass="eklenti" DataNavigateUrlFields="DosyaPath,exe"
                    DataNavigateUrlFormatString="javascript:open_URL('+{0}+','+{1}+');" DataTextField="eklenti"
                    HeaderText="Eklenti Durumu" ItemStyle-Font-Underline="false" Target="_blank" NavigateUrl="javascript:open_URL('+{0}+','+{1}+');"
                    Text="">
                    <ControlStyle CssClass="eklenti"></ControlStyle>
                    <ItemStyle Font-Underline="False"></ItemStyle>                    
                </asp:HyperLinkField>--%>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:Label ID="lblCapt" runat="server" Text="Silinecek Dosya Seç" ForeColor="White"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="Chksec" runat="server" />
                    </ItemTemplate>
                    <ItemStyle Width="75px" VerticalAlign="Middle" HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <br />
        <asp:Button ID="btndosyasil" runat="server" CssClass="myButton"
            OnClick="btndosyasil_Click" />
    </form>
</body>
</html>
