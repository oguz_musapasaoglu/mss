﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.Data;
using System.Collections;
using MSS1.Codes;

namespace MSS1
{
    public partial class OnePageList12 : System.Web.UI.Page
    {
        Hashtable copiedValues = null;
        SessionBase _obj;
        public string OBJ1_pageId { get; set; }

        public string OBJ1_fieldId { get; set; }

        private void QueryStringParameterDesing()
        {
            string[] obj1 = { };

            obj1 = Request.QueryString["OJ1"].ToString().Split(';');


            try
            {
                OBJ1_pageId = obj1[0].ToString();
                OBJ1_fieldId = obj1[1].ToString();
            }
            catch (Exception)
            {
                OBJ1_pageId = string.Empty;
                OBJ1_fieldId = string.Empty;
            }


            txtlink.Text = f.tekbirsonucdonder("select LINK from WebPages where [Web Page ID]='" + OBJ1_pageId + "' and [Web Object ID]='" + OBJ1_fieldId + "'");
            txtlinkField.Text = f.tekbirsonucdonder("select [LINK FIELD] from WebPages where [Web Page ID]='" + OBJ1_pageId + "' and [Web Object ID]='" + OBJ1_fieldId + "'");


            grid1.JSProperties["cpIsUpdated"] = false;
        }


        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();
        }



        webObjectHandler woh = new webObjectHandler();
        ServiceUtilities services = new ServiceUtilities();
        fonksiyonlar f = new fonksiyonlar();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");

            QueryStringParameterDesing();


            if (IsCallback || IsPostBack) return;
            lblGId.Text = Request.QueryString["GId"];
            CallGrid1Data();
            GetParameters();
            Session["OPL12Grid1Rows"] = webObjectHandler.GetDynamicWebPageObjects(OBJ1_pageId, OBJ1_fieldId, _obj.Comp).Tables[4];
            FillDS1((DataTable)Session["OPL12Grid1Rows"]);
            grid1.DataSourceID = "dtgrid1";
            grid1.DataBind();
            RadioListOrder();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }

        private void RadioListOrder()
        {
            if (OBJ1_pageId.Length > 0 & OBJ1_fieldId.Length > 0)
            {
                DynamicUtils.preparePageRadioList(rblDynamicList, OBJ1_pageId, OBJ1_fieldId,_obj.Comp, _obj.UserName,_obj.SelectedLanguage);
            }
        }
        private void GetParameters()
        {
            Session["Grid2RTYP1"] = string.Empty;
            Session["Grid2RTYP2"] = string.Empty;
            Session["Grid2RTYP3"] = string.Empty;
            Session["Grid2R0014"] = string.Empty;
            Session["Grid2R0037"] = string.Empty;
        }


        void FillDS1(DataTable dtRows)
        {
            int sayac = 1;
            string query = string.Empty;
            for (int i = 0; i < dtRows.Rows.Count; i++)
            {
                switch (dtRows.Rows[i]["DX Type1"].ToString())
                {
                    case "DATA":
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Source"].ToString()))
                        {
                            query = webObjectHandler.FillCombobox(dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(),
                                dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), _obj.Comp, _obj.UserName);
                            switch (sayac)
                            {
                                case 1:
                                    DS11.Table = f.datatablegonder(query);
                                    break;
                                case 2:
                                    DS12.Table = f.datatablegonder(query);
                                    break;
                                case 3:
                                    DS13.Table = f.datatablegonder(query);
                                    break;
                                case 4:
                                    DS14.Table = f.datatablegonder(query);
                                    break;
                                case 5:
                                    DS15.Table = f.datatablegonder(query);
                                    break;
                                case 6:
                                    DS16.Table = f.datatablegonder(query);
                                    break;
                                case 7:
                                    DS17.Table = f.datatablegonder(query);
                                    break;
                                case 8:
                                    DS18.Table = f.datatablegonder(query);
                                    break;
                                case 9:
                                    DS19.Table = f.datatablegonder(query);
                                    break;
                                case 10:
                                    DS110.Table = f.datatablegonder(query);
                                    break;
                                case 11:
                                    DS111.Table = f.datatablegonder(query);
                                    break;
                                case 12:
                                    DS112.Table = f.datatablegonder(query);
                                    break;
                                case 13:
                                    DS113.Table = f.datatablegonder(query);
                                    break;
                                case 14:
                                    DS114.Table = f.datatablegonder(query);
                                    break;
                                case 15:
                                    DS115.Table = f.datatablegonder(query);
                                    break;
                                case 16:
                                    DS116.Table = f.datatablegonder(query);
                                    break;
                                case 17:
                                    DS117.Table = f.datatablegonder(query);
                                    break;
                                case 18:
                                    DS118.Table = f.datatablegonder(query);
                                    break;
                                case 19:
                                    DS119.Table = f.datatablegonder(query);
                                    break;
                                case 20:
                                    DS120.Table = f.datatablegonder(query);
                                    break;
                            }
                            sayac++;
                        }
                        break;
                }
            }
        }

        private void CallGrid1Data()
        {
            if (OBJ1_pageId.Length > 0 & OBJ1_fieldId.Length > 0)
            {
                dtgrid1.Table = woh.webObjectHandlerExtracted(grid1, menu1, txtinsertparams1, txteditparams1, txtdeleteparams1,
                    _obj.UserName, _obj.Comp, OBJ1_pageId, OBJ1_fieldId,
                    HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString(),
                    HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), 1).Copy();
                DataColumn[] pricols = new DataColumn[1];
                pricols[0] = dtgrid1.Table.Columns["ID"];
                dtgrid1.Table.PrimaryKey = pricols;
                grid1.DataSourceID = "dtgrid1";

            }
            else
            {
                grid1.Visible = false;
            }
        }




        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            switch (gridI.ID)
            {
                case "grid1":
                    if (txtinsertparams1.Text.Length > 0)
                    {
                        string[] _temp = txtinsertparams1.Text.Split(',');
                        string[] _array = new string[txtinsertparams1.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }

                            else if (string.IsNullOrEmpty(_temp[i].ToString()))
                            {
                                _array[i - 1] = "";
                            }
                            else if (_temp[i].ToString().Substring(0, 1) == "$")
                            {
                                try { _array[i - 1] = _temp[i].ToString().Substring(1, _temp[i].ToString().Length - 1); } catch { }
                            }
                            else
                            {
                                if (e.NewValues[_temp[i].ToString()] != null)
                                {
                                    if (e.NewValues[_temp[i].ToString()].ToString().Contains('|'))
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString().Split('|')[e.NewValues[_temp[i].ToString()].ToString().Split('|').Length - 1];
                                    else
                                        _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                                }
                                else
                                    _array[i - 1] = "";

                            }
                        }

                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;

                default:
                    break;
            }

            Session[gridI.ID + "Cloned"] = "0";
            gridI.JSProperties["cpIsUpdated"] = true;
        }

        protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            switch (gridI.ID)
            {
                case "grid1":
                    if (txteditparams1.Text.Length > 0)
                    {
                        string[] _temp = txteditparams1.Text.Split(',');
                        string[] _array = new string[txteditparams1.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "RCOMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "RUSER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else
                            {
                                if (e.NewValues[_temp[i].ToString()] != null)
                                    _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                                else
                                    _array[i - 1] = _temp[i].ToString();
                            }
                        }

                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;

                default:
                    break;
            }
            Session[gridI.ID + "Cloned"] = "0";
            gridI.JSProperties["cpIsUpdated"] = true;
        }

        protected void grid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            switch (gridI.ID)
            {
                case "grid1":
                    if (txtdeleteparams1.Text.Length > 0)
                    {
                        string[] _temp = txtdeleteparams1.Text.Split(',');
                        string[] _array = new string[txtdeleteparams1.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "RCOMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "RUSER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else
                            {
                                if (e.Values[_temp[i].ToString()] != null)
                                    _array[i - 1] = e.Values[_temp[i].ToString()].ToString();
                                else
                                    _array[i - 1] = _temp[i].ToString();
                            }
                        }
                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;

                default:
                    break;
            }
            Session[gridI.ID + "Cloned"] = "0";
            gridI.JSProperties["cpIsUpdated"] = true;
        }

        #region Grid1

        protected void grid1_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView xgrid = (ASPxGridView)sender;


            if (xgrid.IsNewRowEditing)
            {
                DataRow[] rows = ((DataTable)Session["OPL12Grid1Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session["OPL12Grid1Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session["OPL12Grid1Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.Visible = false;
                    else
                        e.Editor.Visible = true;
                }
                if (e.Column.FieldName == "R0012")
                {
                    ASPxDateEdit de = e.Editor as ASPxDateEdit;
                    de.Date = DateTime.Now;
                }

            }
            else
            {
                DataRow[] rows = ((DataTable)Session["OPL12Grid1Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session["OPL12Grid1Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session["OPL12Grid1Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.Visible = false;
                    else
                        e.Editor.Visible = true;
                }


            }

            if (e.Column.FieldName == "RTYP2")
            {

                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP1");
                if (!string.IsNullOrEmpty(Session["Grid2RTYP1"].ToString()))
                {

                    _val = Session["Grid2RTYP1"].ToString();
                }

                if (_val != null)
                {

                    if (!string.IsNullOrEmpty(_val.ToString()))
                    {
                        Session["OPL10R0011_GRID2"] = _val.ToString();
                        string sorgu = "select NEWID() PK, * from [0_W51162] where RCOMP='" + _obj.Comp + "' and R0011='" + _val.ToString() + "'";
                        f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                    }
                }

                cmb.Callback += CmbChargeType1_Callback;
            }
            else if (e.Column.FieldName == "RTYP3")
            {
                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                object _val1 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP1");
                object _val2 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP2");
                if (!string.IsNullOrEmpty(Session["Grid2RTYP1"].ToString()))
                {
                    _val1 = Session["Grid2RTYP1"].ToString();
                    _val2 = Session["Grid2RTYP2"].ToString();
                }
                if (_val1 != null & _val2 != null)
                {
                    if (!string.IsNullOrEmpty(_val1.ToString()) & !string.IsNullOrEmpty(_val2.ToString()))
                    {
                        string sorgu = "select NEWID() PK, * from [0_W51163] where RCOMP='" + _obj.Comp + "' and  R0011='" + _val1.ToString() + "' and R0012='" + _val2.ToString() + "'";
                        f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                    }
                }
                cmb.Callback += CmbChargeType2_Callback;

            }
            else if (e.Column.FieldName == "R0016")
            {
                ASPxComboBox cmb = e.Editor as ASPxComboBox;
                object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, "R0014");
                if (!string.IsNullOrEmpty(Session["Grid2R0014"].ToString()))
                {
                    _val = Session["Grid2R0014"].ToString();
                }
                if (_val != null)
                {
                    if (!string.IsNullOrEmpty(_val.ToString()))
                    {
                        string sorgu = "select NEWID() PK, * from  [0_W51172] where RCOMP='" + _obj.Comp + "' and R0011='" + _val.ToString() + "'";
                        f.FillAspxCombobox(cmb, sorgu, "R0013", "R0012");
                    }
                }
                cmb.Callback += CmbJobisNo_Callback;

            }
            else if (e.Column.FieldName == "RTEMP")
            {
                ASPxComboBox cmb = e.Editor as ASPxComboBox;

                object _val1 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP1");
                object _val2 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP2");
                object _val3 = xgrid.GetRowValuesByKeyValue(e.KeyValue, "RTYP3");
                if (!string.IsNullOrEmpty(Session["Grid2RTYP3"].ToString()))
                {
                    _val1 = Session["Grid2RTYP1"].ToString();
                    _val2 = Session["Grid2RTYP2"].ToString();
                    _val3 = Session["Grid2RTYP3"].ToString();
                }
                if (_val1 != null & _val2 != null & _val3 != null)
                {
                    if (!string.IsNullOrEmpty(_val1.ToString()) & !string.IsNullOrEmpty(_val2.ToString()) & !string.IsNullOrEmpty(_val3.ToString()))
                    {
                        string sorgu = "select NEWID() PK, * from [0_W51164] where RCOMP='" + _obj.Comp + "' and  R0013='" + _val1.ToString() + "|" + _val2.ToString() + "|" + _val3.ToString() + "'";
                        f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                    }
                }
                cmb.Callback += RTEMP_Callback;

            }
            else if (e.Column.FieldName == "R0033")
            {
                string query = "SELECT DEPT  FROM  [0C_50001_02_KULLANICILAR] WHERE COMPANY='" + _obj.Comp + "' AND [Kullanıcı Adı] ='" + _obj.UserName + "'";
                string _res = f.tekbirsonucdonder(query);
                if (!string.IsNullOrEmpty(_res))
                {
                    ASPxComboBox cmb = e.Editor as ASPxComboBox;
                    cmb.SelectedItem = cmb.Items.FindByValue(_res);
                }
            }
            else if (e.Column.FieldName == "R0034")
            {
                string query = "SELECT BLG  FROM  [0C_50001_02_KULLANICILAR] WHERE COMPANY='" + _obj.Comp + "' AND [Kullanıcı Adı] ='" + _obj.UserName + "'";
                string _res = f.tekbirsonucdonder(query);
                if (!string.IsNullOrEmpty(_res))
                {
                    ASPxComboBox cmb = e.Editor as ASPxComboBox;
                    cmb.SelectedItem = cmb.Items.FindByValue(_res);
                }
            }

        }

        private void RTEMP_Callback(object sender, CallbackEventArgsBase e)
        {
            ASPxComboBox cmb = (ASPxComboBox)sender;

            if (!string.IsNullOrEmpty(Session["Grid2RTYP3"].ToString()))
            {
                string sorgu = "select NEWID() PK, * from [0_W51164] where RCOMP='" + _obj.Comp + "' and  R0013='" + Session["Grid2RTYP1"].ToString() + "|" + Session["Grid2RTYP2"].ToString() + "|" + Session["Grid2RTYP3"].ToString() + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                Session["Grid2RTYP3"] = string.Empty;
                cmb.SelectedIndex = 1;
            }
            else
            {
                if (!string.IsNullOrEmpty(e.Parameter))
                {
                    string sorgu = "select NEWID() PK, * from [0_W51164] where RCOMP='" + _obj.Comp + "' and  R0013='" + e.Parameter + "'";
                    f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                    cmb.SelectedIndex = 1;
                }
            }

        }
        private void CmbJobisNo_Callback(object sender, CallbackEventArgsBase e)
        {

            if (!string.IsNullOrEmpty(Session["Grid2R0014"].ToString()))
            {
                ASPxComboBox cmb = (ASPxComboBox)sender;
                string sorgu = "select NEWID() PK, * from [0_W51172] where RCOMP='" + _obj.Comp + "' and R0011='" + Session["Grid2R0014"].ToString() + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0013", "R0012");
                Session["Grid2R0014"] = string.Empty;
            }
            else
            {
                if (!string.IsNullOrEmpty(e.Parameter))
                {
                    ASPxComboBox cmb = (ASPxComboBox)sender;
                    string sorgu = "select NEWID() PK, * from [0_W51172] where RCOMP='" + _obj.Comp + "' and R0011='" + e.Parameter + "'";
                    f.FillAspxCombobox(cmb, sorgu, "R0013", "R0012");
                }
            }

        }

        private void CmbChargeType1_Callback(object sender, CallbackEventArgsBase e)
        {

            if (!string.IsNullOrEmpty(Session["Grid2RTYP1"].ToString()))
            {
                ASPxComboBox cmb = (ASPxComboBox)sender;
                string sorgu = "select NEWID() PK, * from [0_W51162] where RCOMP='" + _obj.Comp + "' and  R0011='" + Session["Grid2RTYP1"].ToString() + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                Session["OPL10R0011_GRID2"] = Session["Grid2RTYP1"].ToString();
                Session["Grid2RTYP1"] = string.Empty;
            }
            else
            {
                if (!string.IsNullOrEmpty(e.Parameter))
                {

                    ASPxComboBox cmb = (ASPxComboBox)sender;
                    string sorgu = "select NEWID() PK, * from [0_W51162] where RCOMP='" + _obj.Comp + "' and  R0011='" + e.Parameter + "'";
                    f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                    Session["OPL10R0011_GRID2"] = e.Parameter.ToString();

                }
            }

        }
        private void CmbChargeType2_Callback(object sender, CallbackEventArgsBase e)
        {
            if (!string.IsNullOrEmpty(Session["Grid2RTYP2"].ToString()))
            {
                ASPxComboBox cmb = (ASPxComboBox)sender;
                string sorgu = "select NEWID() PK, * from [0_W51163] where RCOMP='" + _obj.Comp + "' and  R0011='" + Session["OPL10R0011_GRID2"].ToString() + "' and R0012='" + Session["Grid2RTYP2"].ToString() + "'";
                f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");
                Session["Grid2RTYP2"] = string.Empty;



            }
            else
            {
                if (!string.IsNullOrEmpty(e.Parameter))
                {
                    ASPxComboBox cmb = (ASPxComboBox)sender;
                    string sorgu = "select NEWID() PK, * from [0_W51163] where RCOMP='" + _obj.Comp + "' and  R0011='" + Session["OPL10R0011_GRID2"].ToString() + "' and R0012='" + e.Parameter + "'";
                    f.FillAspxCombobox(cmb, sorgu, "R0015", "R0013");


                }
            }

        }


        protected void menu1_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport1.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                webObjectHandler.ExportPdf(gridExport1);
            }
            else if (e.Item.Name.Equals("btninsert"))
            {
                try { grid1_CustomCallback(grid1, null); } catch { }
            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(OBJ1_pageId, OBJ1_fieldId, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid1, rows, _obj.UserName, _obj.Comp);

                try { grid1_CustomCallback(grid1, null); } catch { }



            }
            else
            {
                this.Response.StatusCode = 500;
                this.Response.End();
            }
        }
        #endregion


        protected void grid1_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(OBJ1_pageId, OBJ1_fieldId, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);

            }
            this.dtgrid1.Table = webObjectHandler.fillMainGrid(OBJ1_pageId, OBJ1_fieldId, _obj.Comp, _obj.UserName);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid1.Table.Columns["ID"];
            this.dtgrid1.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid1";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL12Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";
            grid.FilterExpression = webObjectHandler.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;
            grid.DataBind();
            grid.FilterExpression = _filterexpression;

        }

        protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            if (copiedValues == null) return;
            string Sessionn = "OPL12Grid" + gridI.ID.Substring(gridI.ID.Length - 1) + "Rows";
            DataRow[] rows = ((DataTable)Session[Sessionn]).Select("[DX Clone] >0");
            if (rows.Length == 0) return;
            foreach (DataRow row in rows)
            {
                e.NewValues[row["DX Field ID"].ToString()] = copiedValues[row["DX Field ID"].ToString()];
            }
        }



        protected void Grid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            ASPxGridView Igrid = sender as ASPxGridView;
            string _sessionname = "OPL12Grid" + Igrid.ID.Substring(Igrid.ID.Length - 1) + "Rows";

            DataRow[] rows = ((DataTable)Session[_sessionname]).Select("[DX MouseOver Text]<>'' and [DX Field ID]='" + e.DataColumn.FieldName + "'");
            if (rows.Length > 0)
            {
                try
                {
                    string _val = Igrid.GetRowValues(e.VisibleIndex, rows[0]["DX MouseOver Text"].ToString()).ToString();
                    e.Cell.ToolTip = _val;
                }
                catch { e.Cell.ToolTip = string.Format("{0}", e.CellValue); }
            }
            else
                e.Cell.ToolTip = string.Format("{0}", e.CellValue);

            try
            {
                if (e.DataColumn is GridViewDataHyperLinkColumn)
                {
                    if (string.IsNullOrEmpty(e.CellValue.ToString()))
                        e.Cell.Controls[0].Visible = false;

                }
            }
            catch
            {

            }
        }

        protected void grid_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {

            try
            {

                if (e.RowType == GridViewRowType.Data)
                {

                    ASPxGridView gridI = sender as ASPxGridView;
                    object rfont = e.GetValue("RFONT")
                                , rback = e.GetValue("RBACK")
                                , rftsz = e.GetValue("RFTSZ")
                                , rftwt = e.GetValue("RFTWT")
                                , rftst = e.GetValue("RFTST");
                    if (rfont == null) return;



                    try
                    {
                        for (int columnIndex = 0; columnIndex < e.Row.Cells.Count; columnIndex++)
                        {
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.Color, rfont.ToString()); } catch { }
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.BackgroundColor, rback.ToString()); } catch { }
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.FontSize, rftsz.ToString() + "px"); } catch { }
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.FontWeight, Convert.ToInt32(rftwt.ToString()) == 0 ? "normal" : "bold"); } catch { }
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.FontStyle, Convert.ToInt32(rftst.ToString()) == 0 ? "normal" : "italic"); } catch { }
                        }

                    }
                    catch { }



                }
            }
            catch { }
        }

        protected void Grid_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;
            string _sessionname = "OPL12Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";
            switch (e.ButtonType)
            {
                case ColumnCommandButtonType.Edit:

                    DataRow[] EditRows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='EDIT' and [DX Display Order]>0");
                    if (EditRows.Length > 0)
                    {
                        string _col = EditRows[0]["DX Permision Field"].ToString();
                        if (grid.GetRowValues(e.VisibleIndex, _col) != null)
                        {
                            if (grid.GetRowValues(e.VisibleIndex, _col).ToString() == "0")
                            {
                                e.Visible = false;
                            }
                        }
                    }
                    else
                        e.Visible = false;
                    break;
                case ColumnCommandButtonType.Delete:
                    DataRow[] DeleteRows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='DELETE' and [DX Display Order]>0");
                    if (DeleteRows.Length > 0)
                    {
                        string _col = DeleteRows[0]["DX Permision Field"].ToString();
                        if (grid.GetRowValues(e.VisibleIndex, _col) != null)
                        {
                            if (grid.GetRowValues(e.VisibleIndex, _col).ToString() == "0")
                            {
                                e.Visible = false;
                            }
                        }
                    }
                    else
                        e.Visible = false;
                    break;
                case ColumnCommandButtonType.SelectCheckbox:
                case ColumnCommandButtonType.Select:
                    DataRow[] selectrows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='SELECT' and [DX Display Order]>0");
                    string _selcol = selectrows[0]["DX Permision Field"].ToString();
                    if (!string.IsNullOrEmpty(_selcol))
                    {
                        if (grid.GetRowValues(e.VisibleIndex, _selcol).ToString() == "0")
                            e.Enabled = false;
                    }
                    break;
            }
        }

        protected void grid_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            string _sessionname = "OPL12Grid" + gridI.ID.Substring(gridI.ID.Length - 1) + "Rows";
            if (e.ButtonID == "Clone")
            {
                string Sessionn = "OPL12Grid" + gridI.ID.Substring(gridI.ID.Length - 1) + "Rows";
                DataRow[] rows = ((DataTable)Session[Sessionn]).Select("[DX Clone]>0");
                if (rows.Length > 0)
                {
                    copiedValues = new Hashtable();
                    foreach (DataRow row in rows)
                    {
                        copiedValues[row["DX Field ID"].ToString()] = gridI.GetRowValues(e.VisibleIndex, row["DX Field ID"].ToString());
                    }
                }
                Session[gridI.ID + "Cloned"] = "1";
                gridI.AddNewRow();
            }
            else if (e.ButtonID == "SelectAll")
            {
                DataRow[] selectrows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='SELECT' and [DX Display Order]>0");
                string _selcol = selectrows[0]["DX Permision Field"].ToString();
                if (!string.IsNullOrEmpty(_selcol))
                {
                    for (int counter = 0; counter < gridI.VisibleRowCount; counter++)
                    {
                        if (Convert.ToInt32(gridI.GetRowValues(counter, _selcol)) == 1)
                            gridI.Selection.SelectRow(counter);
                    }
                }
                else
                    gridI.Selection.SelectAll();
            }
            else if (e.ButtonID == "UnSelectAll")
            {
                gridI.Selection.UnselectAll();
            }

        }


        protected void rblDynamicList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string _url = "";
            _url = rblDynamicList.SelectedItem.Value.ToString();
            if (_url.Contains("?"))
                Response.Redirect(_url + "&GId=" + lblGId.Text);
            else
                Response.Redirect(_url + "?GId=" + lblGId.Text);
        }
    }
}