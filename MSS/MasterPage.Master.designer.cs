﻿//------------------------------------------------------------------------------
// <otomatik üretildi>
//     Bu kod bir araç tarafından oluşturuldu.
//
//     Bu dosyada yapılacak değişiklikler yanlış davranışa neden olabilir ve
//     kod tekrar üretildi. 
// </otomatik üretildi>
//------------------------------------------------------------------------------

namespace MSS1
{


    public partial class MasterPage
    {

        /// <summary>
        /// head denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ContentPlaceHolder head;

        /// <summary>
        /// form1 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;

        /// <summary>
        /// divSpecial denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl divSpecial;

        /// <summary>
        /// tblUstMenu denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl tblUstMenu;

        /// <summary>
        /// hrefLogoLink denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlAnchor hrefLogoLink;

        /// <summary>
        /// Image1 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image Image1;

        /// <summary>
        /// lblMesaj denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox lblMesaj;

        /// <summary>
        /// cmbCompanyNew denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList cmbCompanyNew;

        /// <summary>
        /// cmbLangNew denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList cmbLangNew;

        /// <summary>
        /// ltrlAgacMenu denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrlAgacMenu;

        /// <summary>
        /// ltrlMenu2 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrlMenu2;

        /// <summary>
        /// ltrlNotifSayac denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrlNotifSayac;

        /// <summary>
        /// ltrlNotifElements denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ltrlNotifElements;

        /// <summary>
        /// ContentPlaceHolder1 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ContentPlaceHolder ContentPlaceHolder1;

        /// <summary>
        /// tblTodo denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTable tblTodo;

        /// <summary>
        /// LinkButton1 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton1;

        /// <summary>
        /// LinkButton2 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton2;

        /// <summary>
        /// LinkButton3 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton3;

        /// <summary>
        /// LinkButton4 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton4;

        /// <summary>
        /// LinkButton5 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton5;

        /// <summary>
        /// LinkButton6 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton6;

        /// <summary>
        /// LinkButton7 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton7;

        /// <summary>
        /// LinkButton8 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton8;

        /// <summary>
        /// LinkButton9 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton9;

        /// <summary>
        /// LinkButton10 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton10;

        /// <summary>
        /// LinkButton11 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton11;

        /// <summary>
        /// LinkButton12 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton12;

        /// <summary>
        /// LinkButton13 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton13;

        /// <summary>
        /// LinkButton14 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton14;

        /// <summary>
        /// LinkButton15 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton15;

        /// <summary>
        /// LinkButton16 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton16;

        /// <summary>
        /// LinkButton17 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton17;

        /// <summary>
        /// LinkButton18 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton18;

        /// <summary>
        /// LinkButton19 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton19;

        /// <summary>
        /// LinkButton20 denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton20;

        /// <summary>
        /// DsLang denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource DsLang;

        /// <summary>
        /// DsCompany denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource DsCompany;

        /// <summary>
        /// LoadingPanel denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::DevExpress.Web.ASPxLoadingPanel LoadingPanel;
    }
}
