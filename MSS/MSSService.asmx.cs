﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using MSS1.Codes;

namespace MSS1
{
    /// <summary>
    /// Summary description for MSSService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MSSService : System.Web.Services.WebService
    {
        fonksiyonlar f = new fonksiyonlar();
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public string RemoveCache(string _comp, string _pageId, string IslemId)
        {
            string _result = string.Empty;
            try
            {
                string _servicecode = f.tekbirsonucdonder("select [Web Service Kod] from GAC_NAV2.dbo.[GAC$Company Information]");
                if (string.IsNullOrEmpty(_servicecode)) return "Web Service Kod boş olamaz";
                if (string.IsNullOrEmpty(IslemId)) return "İşlem ID boş olamaz";
                if (IslemId != _servicecode) return "";
                string[] _PIds = _pageId.Split(',');
                foreach (var _PId in _PIds)
                {
                    List<CacheObject> _objList = CacheManager.GetDynamicCachesAt(_comp, _pageId);
                    foreach (var _obj in _objList)
                    {
                        CacheManager._cacheObjects.Remove(_obj);
                    }

                    if (_objList.Count > 0) _result = "OK";
                }              

            }
            catch
            {

               
            }

            return _result;
        }
    }
}
