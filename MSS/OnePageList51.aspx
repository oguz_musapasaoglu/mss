﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OnePageList51.aspx.cs" EnableViewState="true" Inherits="MSS1.OnePageList51" %>

<%@ Register Src="~/controls/DataTableControlGAC.ascx" TagName="StDataTable" TagPrefix="STDT" %>
<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="css/GAC_NewStyle.css?v=43" rel="stylesheet" />
    <link href="css/style.css?v=43" rel="stylesheet" type="text/css" />
    <link href="css/tools.css?v=43" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/default.css?v=43" />
    <link rel="stylesheet" type="text/css" href="css/component.css?v=43" />
    <script type="text/javascript" src="js/modernizr.custom.js?v=43"></script>
    <script type="text/javascript" src="js/classie.js?v=43"></script>
    <script type="text/javascript" src="../../js/jquery.js?v=43"></script>
    <style type="text/css">
        .dxpcModalBackLite, .dxdpModalBackLite {
            background-color: #777777 !important;
            opacity: 1 !important;
        }

        .dxgvCSD {
            display: none !important;
        }
    </style>
    <script type="text/javascript">

        function OnBeginCallback(s, e) {
            if (e.command == 'CANCELEDIT')
                parent.DevexPopup.Hide();
        }
        function OnRefreshAllObjects(s, e, i) {
            if (s.cpIsUpdated) {


                if (!s.cpIsLinked) {
                    try {

                        var _val3 = s.cpMsg;
                        if (_val3.length > 0)
                            alert(_val3.replace("'", " "));


                    } catch (e) {

                    }
                }
                else {
                    try {

                        var _val3 = s.cpMsg;
                        if (_val3.length > 0 & _val3 != "OK")
                            alert(_val3.replace("'", " "));


                    } catch (e) {

                    }
                }

                var ferobjs = document.getElementById('txtRef1').value;

                var cpvalarray = ferobjs.split(';');
                var _val = '';

                for (var j = 0; j < 16; j++) {

                    checkRefVal = j.toString();
                    _val = cpvalarray.find(checkRefObjs);
                    if (_val != null) {
                        if (_val.length > 0) {
                            if (j == 0)
                                try {
                                    if (objISCardVal == "1") parent.document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rp1_iframeView').src = parent.document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rp1_iframeView').src;
                                } catch (err) { }
                            else if (j == 1)
                                try { parent.grid1.PerformCallback(''); } catch (err) { }
                            else if (j == 2)
                                try { parent.grid2.PerformCallback(''); } catch (err) { }
                            else if (j == 3)
                                try { parent.grid3.PerformCallback(''); } catch (err) { }
                            else if (j == 4)
                                try { parent.grid4.PerformCallback(''); } catch (err) { }
                            else if (j == 5)
                                try { parent.grid5.PerformCallback(''); } catch (err) { }
                            else if (j == 6)
                                try { parent.grid6.PerformCallback(''); } catch (err) { }
                            else if (j == 7)
                                try { parent.grid7.PerformCallback(''); } catch (err) { }
                            else if (j == 8)
                                try { parent.grid8.PerformCallback(''); } catch (err) { }
                            else if (j == 9)
                                try { parent.grid9.PerformCallback(''); } catch (err) { }
                            else if (j == 10)
                                try { parent.grid10.PerformCallback(''); } catch (err) { }
                            else if (j == 11)
                                try { parent.grid11.PerformCallback(''); } catch (err) { }
                            else if (j == 12)
                                try { parent.grid12.PerformCallback(''); } catch (err) { }
                            else if (j == 13)
                                try { parent.grid13.PerformCallback(''); } catch (err) { }
                            else if (j == 14)
                                try { parent.grid14.PerformCallback(''); } catch (err) { }
                            else if (j == 15)
                                try { parent.grid15.PerformCallback(''); } catch (err) { }


                        }
                    }


                }

                if (!s.cpIsLinked)
                    parent.DevexPopup.Hide();

            }
            if (s.cpIsLinked) {
                if (s.cpQueryStrings == 'PDFMERGE') {
                    var _resarray = s.cpLink.split('|');
                    if (_resarray[0] == "OK")
                        window.open('./' + _resarray[1]);
                    else
                        alert(_resarray[1]);
                }
                else {
                    if (s.cpProcType != '') {
                        var _modearray = s.cpProcType.split('|');
                        if (_modearray[0] == 'OPENMODE') {

                            OpenSelectDirect(s.cpLink, s.cpQueryStrings, s.cpIslemId, 1);
                        }
                        else if (_modearray[0] == 'POSTMODE') {
                            if (_modearray[1] != 'OK') {
                                alert(_modearray[1]);
                                return;
                            }
                            if (_modearray[3] != 'OK') {
                                alert(_modearray[3]);
                                return;
                            }
                            window.open('./' + s.cpLink);
                        }
                    }
                    else
                        OpenSelectDirect(s.cpLink, s.cpQueryStrings, s.cpIslemId, 0);
                }

                parent.DevexPopup.Hide();
            }



        }

        var checkval = '';
        var checkRefVal = '';

        function checkRefObjs(params) {
            return params.startsWith(checkRefVal);
        }

        function OpenSelectDirect(link, filterobjs, IslemId, IsRepProc) {
            var array1 = filterobjs.split(',');
            var sayac = 1;
            var sayval = '';
            var _val = '';
            var P1 = document.getElementById('HiddenP1').value;
            var P2 = document.getElementById('HiddenP2').value;
            var cpvals = document.getElementById('txtCatchParamsAndValues').value;
            var cpvalarray = cpvals.split('|');
            //alert(cpvals);
            array1.forEach(function (element) {
                _val = '';
                sayval = ReturnSayacVal(sayac);
                if (element == 'P1')
                    link = link.replace(sayval, P1);
                else if (element == 'P2')
                    link = link.replace(sayval, P2);
                else {
                    //alert(element);
                    checkval = element.replace("#", "");
                    _val = cpvalarray.find(checkParamVal);
                    //alert(_val);
                    link = link.replace(sayval, _val.split(';')[1]);
                }

                sayac = sayac + 1;
            });

            link = link + "&IslemId=" + IslemId;

            if (IsRepProc == 1)
                link = link + "&LOJ=1";

            var popwin = window.open(AdGIdToLink(link), '_blank');
            popwin.focus();
        }

        function ReturnSayacVal(sayac) {
            if (sayac == 1)
                return "XXXXX";
            else if (sayac == 2)
                return "YYYYY";
            else if (sayac == 3)
                return "ZZZZZ";
            else if (sayac == 4)
                return "TTTTT";
            else if (sayac == 5)
                return "PPPPP";
        }

        function checkParamVal(params) {
            return params.startsWith(checkval);
        }

        function AdGIdToLink(_val) {
            if (_val.includes("GId=")) return _val;
            if (_val.includes("?"))
                _val = _val + "&GId=" + $("#lblGId").text();
            else
                _val = _val + "?GId=" + $("#lblGId").text();

            return _val;

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <dx:ASPxGridView ID="grid1" ClientInstanceName="grid1" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                Theme="Glass" OnCustomCallback="grid1_CustomCallback" OnCellEditorInitialize="grid_CellEditorInitialize"
                EnableTheming="false" DataSourceID="dtgrid1" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnRowValidating="grid_RowValidating"
                OnRowInserting="grid_RowInserting" SettingsText-PopupEditFormCaption=" ">
                <ClientSideEvents BeginCallback="function(s, e) {OnBeginCallback(s,e)}" EndCallback="function(s, e) {OnRefreshAllObjects(s,e,1)}" />
                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" AllowFocusedRow="true" />
                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                <SettingsText EmptyDataRow=" " />
                <SettingsCommandButton>
                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                    </CancelButton>
                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                    </UpdateButton>
                </SettingsCommandButton>
                <Styles>
                    <Header ForeColor="White"></Header>
                    <HeaderPanel ForeColor="White"></HeaderPanel>
                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                </Styles>
            </dx:ASPxGridView>
        </div>
        <div style="display: none">
            <STDT:StDataTable ID="dtgrid1" runat="server" />
            <asp:HiddenField runat="server" ID="HiddenLoadqueryText" Value="AA" />
            <asp:HiddenField runat="server" ID="HiddenP1" Value="" />
            <asp:HiddenField runat="server" ID="HiddenP2" Value="" />
            <asp:HiddenField runat="server" ID="HiddenP3" Value="" />
            <asp:HiddenField runat="server" ID="HiddenP4" Value="" />
            <asp:HiddenField runat="server" ID="HiddenP5" Value="" />
            <asp:HiddenField runat="server" ID="HiddenMainStatu" Value="" />
            <asp:HiddenField runat="server" ID="HiddenpageType" Value="" />
            <asp:HiddenField runat="server" ID="HiddenRpath" Value="" />
            <asp:HiddenField runat="server" ID="HiddendirectParam" />
            <asp:HiddenField runat="server" ID="hndBos" />
            <asp:HiddenField runat="server" ID="HiddenPageTitle" />
            <asp:Label ID="lblden" runat="server"></asp:Label>
            <asp:TextBox ID="txtOpenNewWindow" Text="0" runat="server"></asp:TextBox>
            <asp:Label ID="lblGId" runat="server"></asp:Label>
            <asp:TextBox ID="txtlink1" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtlinkField1" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtinsertparams1" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtCloneParameters1" runat="server"></asp:TextBox>
            <asp:TextBox ID="txteditparams1" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtdeleteparams1" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtCatchParameters1" runat="server" Text=""></asp:TextBox>
            <asp:TextBox ID="txtCatchParamsAndValues" runat="server" Text=""></asp:TextBox>
            <asp:TextBox ID="txtCatchValues1" runat="server" Text=""></asp:TextBox>

            <asp:TextBox ID="txtTransferedParameters1" runat="server" Text=""></asp:TextBox>

            <asp:TextBox ID="txtCollapseHeader1" runat="server" Text=""></asp:TextBox>
            <asp:TextBox ID="txtCollapseStatu1" runat="server" Text=""></asp:TextBox>
            <asp:TextBox ID="txtDHT1" runat="server" Text=""></asp:TextBox>
            <asp:TextBox ID="txtRef1" runat="server" Text=""></asp:TextBox>
            <asp:TextBox ID="txtOBJP1" runat="server" Text=""></asp:TextBox>
            <asp:TextBox ID="txtOBJF1" runat="server" Text=""></asp:TextBox>
            <asp:TextBox ID="txtBos" runat="server" Text=""></asp:TextBox>
            <asp:TextBox ID="txtOnayStatus" runat="server" Text=""></asp:TextBox>
            <asp:TextBox ID="txtlinkType" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtCurLinkFields" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtFilterText" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtcurrentId" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtMessage" runat="server"></asp:TextBox>
            <dx:ASPxTextBox ID="txttask" ClientInstanceName="txttask" runat="server"></dx:ASPxTextBox>
            <asp:TextBox ID="txtPostDurum" runat="server" Text="First"></asp:TextBox>
            <asp:TextBox ID="txtIsCard" runat="server" Text="1"></asp:TextBox>
            <asp:TextBox ID="txtInsertComboSource" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtInsertComboParams" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtHeaderSource" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtHeaderParams" runat="server"></asp:TextBox>
        </div>
    </form>
</body>
</html>
