﻿using DevExpress.Web;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.Codes;

namespace MSS1
{
    public partial class OnePageList7 : System.Web.UI.Page
    {
        Hashtable copiedValues = null;
        SessionBase _obj;
        public string OBJ1_pageId { get; set; }
        public string OBJ2_pageId { get; set; }
        public string OBJ3_pageId { get; set; }
        public string OBJ1_fieldId { get; set; }
        public string OBJ2_fieldId { get; set; }
        public string OBJ3_fieldId { get; set; }


        private void QueryStringParameterDesing()
        {
            string[] obj1 = Request.QueryString["OJ1"].ToString().Split(';');
            string[] obj2 = Request.QueryString["OJ2"].ToString().Split(';');
            string[] obj3 = Request.QueryString["OJ3"].ToString().Split(';');
            OBJ1_pageId = obj1[0].ToString();
            OBJ1_fieldId = obj1[1].ToString();

            OBJ2_pageId = obj2[0].ToString();
            OBJ2_fieldId = obj2[1].ToString();

            OBJ3_pageId = obj3[0].ToString();
            OBJ3_fieldId = obj3[1].ToString();

            txtlink.Text = f.tekbirsonucdonder("select LINK from WebPages where [Web Page ID]='" + OBJ2_pageId + "' and [Web Object ID]='" + OBJ2_fieldId + "'");

            txtlinkField.Text = f.tekbirsonucdonder("select [LINK FIELD] from WebPages where [Web Page ID]='" + OBJ2_pageId + "' and [Web Object ID]='" + OBJ2_fieldId + "'");
        }

        webObjectHandler woh = new webObjectHandler();
        ServiceUtilities services = new ServiceUtilities();
        fonksiyonlar f = new fonksiyonlar();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }

            if (IsCallback || IsPostBack) return;
            Response.AppendHeader("Pragma", "no-cache");
            Response.AppendHeader("Cache-Control", "no-cache");
            Response.CacheControl = "no-cache";
            Response.Expires = -1;
            lblGId.Text = Request.QueryString["GId"];

            QueryStringParameterDesing();
            dtgrid1.Table = woh.webObjectHandlerExtracted(grid1, menu1, txtinsertparams1, txteditparams1, txtdeleteparams1, _obj.UserName, _obj.Comp,
                OBJ1_pageId, OBJ1_fieldId, HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString(), HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), 1).Copy();

            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = dtgrid1.Table.Columns["ID"];
            dtgrid1.Table.PrimaryKey = pricols;



            //dtgrid2.Table = woh.webObjectHandlerExtracted(grid2, menu2, txtinsertparams2, txteditparams2, txtdeleteparams2, _obj.UserName, _obj.Comp, OBJ2_pageId, OBJ2_fieldId, DS1, DS2, DS3, DS4, DS5, DS6, DS7, DS8, DS9, DS10, DS11, DS12, DS13, DS14, DS15, DS16, DS17, DS18, DS19, DS20, HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString(), HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString()).Copy();
            //DataColumn[] pricols2 = new DataColumn[1];
            //pricols2[0] = dtgrid2.Table.Columns["ID"];
            //dtgrid2.Table.PrimaryKey = pricols2;
            //grid2.DataBind();

            dtgrid3.Table = woh.webObjectHandlerExtracted(grid3, menu3, txtinsertparams3, txteditparams3, txtdeleteparams3, _obj.UserName, _obj.Comp,
                OBJ3_pageId, OBJ3_fieldId, HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString(), HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), 2).Copy();

            DataColumn[] pricols3 = new DataColumn[1];
            pricols3[0] = dtgrid3.Table.Columns["ID"];
            dtgrid3.Table.PrimaryKey = pricols3;


            Session["OPL10Grid1Rows"] = webObjectHandler.GetDynamicWebPageObjects(OBJ1_pageId, OBJ1_fieldId, _obj.Comp).Tables[4];
            FillDS1((DataTable)Session["OPL10Grid1Rows"]);
            //Session["OPL10Grid2Rows"] = webObjectHandler.GetDynamicWebPageObjects(OBJ2_pageId, OBJ2_fieldId).Tables[4];
            Session["OPL10Grid3Rows"] = webObjectHandler.GetDynamicWebPageObjects(OBJ3_pageId, OBJ3_fieldId, _obj.Comp).Tables[4];
            FillDS2((DataTable)Session["OPL10Grid3Rows"]);

            grid1.DataBind();
            grid3.DataBind();
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }

        void FillDS1(DataTable dtRows)
        {
            int sayac = 1;
            string query = string.Empty;
            for (int i = 0; i < dtRows.Rows.Count; i++)
            {
                switch (dtRows.Rows[i]["DX Type1"].ToString())
                {
                    case "DATA":
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Source"].ToString()))
                        {
                            query = webObjectHandler.FillCombobox(dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(),
                                dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), _obj.Comp, _obj.UserName);
                            switch (sayac)
                            {
                                case 1:
                                    DS11.Table = f.datatablegonder(query);
                                    break;
                                case 2:
                                    DS12.Table = f.datatablegonder(query);
                                    break;
                                case 3:
                                    DS13.Table = f.datatablegonder(query);
                                    break;
                                case 4:
                                    DS14.Table = f.datatablegonder(query);
                                    break;
                                case 5:
                                    DS15.Table = f.datatablegonder(query);
                                    break;
                                case 6:
                                    DS16.Table = f.datatablegonder(query);
                                    break;
                                case 7:
                                    DS17.Table = f.datatablegonder(query);
                                    break;
                                case 8:
                                    DS18.Table = f.datatablegonder(query);
                                    break;
                                case 9:
                                    DS19.Table = f.datatablegonder(query);
                                    break;
                                case 10:
                                    DS110.Table = f.datatablegonder(query);
                                    break;
                                case 11:
                                    DS111.Table = f.datatablegonder(query);
                                    break;
                                case 12:
                                    DS112.Table = f.datatablegonder(query);
                                    break;
                                case 13:
                                    DS113.Table = f.datatablegonder(query);
                                    break;
                                case 14:
                                    DS114.Table = f.datatablegonder(query);
                                    break;
                                case 15:
                                    DS115.Table = f.datatablegonder(query);
                                    break;
                                case 16:
                                    DS116.Table = f.datatablegonder(query);
                                    break;
                                case 17:
                                    DS117.Table = f.datatablegonder(query);
                                    break;
                                case 18:
                                    DS118.Table = f.datatablegonder(query);
                                    break;
                                case 19:
                                    DS119.Table = f.datatablegonder(query);
                                    break;
                                case 20:
                                    DS120.Table = f.datatablegonder(query);
                                    break;
                            }
                            sayac++;
                        }
                        break;
                }
            }
        }

        void FillDS2(DataTable dtRows)
        {
            int sayac = 1;
            string query = string.Empty;
            for (int i = 0; i < dtRows.Rows.Count; i++)
            {
                switch (dtRows.Rows[i]["DX Type1"].ToString())
                {
                    case "DATA":
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Source"].ToString()))
                        {
                            query = webObjectHandler.FillCombobox(dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(),
                                dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), _obj.Comp, _obj.UserName);
                            switch (sayac)
                            {
                                case 1:
                                    DS21.Table = f.datatablegonder(query);
                                    break;
                                case 2:
                                    DS22.Table = f.datatablegonder(query);
                                    break;
                                case 3:
                                    DS23.Table = f.datatablegonder(query);
                                    break;
                                case 4:
                                    DS24.Table = f.datatablegonder(query);
                                    break;
                                case 5:
                                    DS25.Table = f.datatablegonder(query);
                                    break;
                                case 6:
                                    DS26.Table = f.datatablegonder(query);
                                    break;
                                case 7:
                                    DS27.Table = f.datatablegonder(query);
                                    break;
                                case 8:
                                    DS28.Table = f.datatablegonder(query);
                                    break;
                                case 9:
                                    DS29.Table = f.datatablegonder(query);
                                    break;
                                case 10:
                                    DS210.Table = f.datatablegonder(query);
                                    break;
                                case 11:
                                    DS211.Table = f.datatablegonder(query);
                                    break;
                                case 12:
                                    DS212.Table = f.datatablegonder(query);
                                    break;
                                case 13:
                                    DS213.Table = f.datatablegonder(query);
                                    break;
                                case 14:
                                    DS214.Table = f.datatablegonder(query);
                                    break;
                                case 15:
                                    DS215.Table = f.datatablegonder(query);
                                    break;
                                case 16:
                                    DS216.Table = f.datatablegonder(query);
                                    break;
                                case 17:
                                    DS217.Table = f.datatablegonder(query);
                                    break;
                                case 18:
                                    DS218.Table = f.datatablegonder(query);
                                    break;
                                case 19:
                                    DS219.Table = f.datatablegonder(query);
                                    break;
                                case 20:
                                    DS220.Table = f.datatablegonder(query);
                                    break;
                            }
                            sayac++;
                        }
                        break;
                }
            }
        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }
        protected void grid1_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            if (txtinsertparams1.Text.Length > 0)
            {
                string[] _temp = txtinsertparams1.Text.Split(',');
                string[] _array = new string[txtinsertparams1.Text.Split(',').Length - 1];
                for (int i = 1; i < _temp.Length; i++)
                {
                    if (_temp[i].ToString() == "COMP")
                    {
                        _array[i - 1] = _obj.Comp;
                    }
                    else if (_temp[i].ToString() == "USER")
                    {
                        _array[i - 1] = _obj.UserName;
                    }
                    else
                    {
                        if (e.NewValues[_temp[i].ToString()] != null)
                            _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                        else
                            _array[i - 1] = _temp[i].ToString();
                    }
                }

                services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
            }
        }

        protected void grid1_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            if (txteditparams1.Text.Length > 0)
            {
                string[] _temp = txteditparams1.Text.Split(',');
                string[] _array = new string[txteditparams1.Text.Split(',').Length - 1];
                for (int i = 1; i < _temp.Length; i++)
                {
                    if (_temp[i].ToString() == "COMP")
                    {
                        _array[i - 1] = _obj.Comp;
                    }
                    else if (_temp[i].ToString() == "USER")
                    {
                        _array[i - 1] = _obj.UserName;
                    }
                    else
                    {
                        if (e.NewValues[_temp[i].ToString()] != null)
                            _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                        else
                            _array[i - 1] = _temp[i].ToString();
                    }
                }
                services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
            }
        }

        protected void grid1_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            if (txtdeleteparams1.Text.Length > 0)
            {
                string[] _temp = txtdeleteparams1.Text.Split(',');
                string[] _array = new string[txtdeleteparams1.Text.Split(',').Length - 1];
                for (int i = 1; i < _temp.Length; i++)
                {
                    if (_temp[i].ToString() == "COMP")
                    {
                        _array[i - 1] = _obj.Comp;
                    }
                    else if (_temp[i].ToString() == "USER")
                    {
                        _array[i - 1] = _obj.UserName;
                    }
                    else
                    {
                        if (e.Values[_temp[i].ToString()] != null)
                            _array[i - 1] = e.Values[_temp[i].ToString()].ToString();
                        else
                            _array[i - 1] = _temp[i].ToString();
                    }
                }
                services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
            }
        }


        protected void grid2_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            if (txtinsertparams2.Text.Length > 0)
            {
                string[] _temp = txtinsertparams2.Text.Split(',');
                string[] _array = new string[txtinsertparams2.Text.Split(',').Length - 1];
                for (int i = 1; i < _temp.Length; i++)
                {
                    if (_temp[i].ToString() == "COMP")
                    {
                        _array[i - 1] = _obj.Comp;
                    }
                    else if (_temp[i].ToString() == "USER")
                    {
                        _array[i - 1] = _obj.UserName;
                    }
                    else
                    {
                        if (e.NewValues[_temp[i].ToString()] != null)
                            _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                        else
                            _array[i - 1] = _temp[i].ToString();
                    }
                }

                services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
            }
        }

        protected void grid2_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            if (txteditparams2.Text.Length > 0)
            {
                string[] _temp = txteditparams2.Text.Split(',');
                string[] _array = new string[txteditparams2.Text.Split(',').Length - 1];
                for (int i = 1; i < _temp.Length; i++)
                {
                    if (_temp[i].ToString() == "COMP")
                    {
                        _array[i - 1] = _obj.Comp;
                    }
                    else if (_temp[i].ToString() == "USER")
                    {
                        _array[i - 1] = _obj.UserName;
                    }
                    else
                    {
                        if (e.NewValues[_temp[i].ToString()] != null)
                            _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                        else
                            _array[i - 1] = _temp[i].ToString();
                    }
                }
                services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
            }
        }

        protected void grid2_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            if (txtdeleteparams2.Text.Length > 0)
            {
                string[] _temp = txtdeleteparams2.Text.Split(',');
                string[] _array = new string[txtdeleteparams2.Text.Split(',').Length - 1];
                for (int i = 1; i < _temp.Length; i++)
                {
                    if (_temp[i].ToString() == "COMP")
                    {
                        _array[i - 1] = _obj.Comp;
                    }
                    else if (_temp[i].ToString() == "USER")
                    {
                        _array[i - 1] = _obj.UserName;
                    }
                    else
                    {
                        if (e.Values[_temp[i].ToString()] != null)
                            _array[i - 1] = e.Values[_temp[i].ToString()].ToString();
                        else
                            _array[i - 1] = _temp[i].ToString();
                    }
                }
                services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
            }
        }


        protected void grid3_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            if (txtinsertparams3.Text.Length > 0)
            {
                string[] _temp = txtinsertparams3.Text.Split(',');
                string[] _array = new string[txtinsertparams3.Text.Split(',').Length - 1];
                for (int i = 1; i < _temp.Length; i++)
                {
                    if (_temp[i].ToString() == "COMP")
                    {
                        _array[i - 1] = _obj.Comp;
                    }
                    else if (_temp[i].ToString() == "USER")
                    {
                        _array[i - 1] = _obj.UserName;
                    }
                    else
                    {
                        if (e.NewValues[_temp[i].ToString()] != null)
                            _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                        else
                            _array[i - 1] = _temp[i].ToString();

                    }
                }

                services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
            }
        }

        protected void grid3_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            if (txteditparams3.Text.Length > 0)
            {
                string[] _temp = txteditparams3.Text.Split(',');
                string[] _array = new string[txteditparams3.Text.Split(',').Length - 1];
                for (int i = 1; i < _temp.Length; i++)
                {
                    if (_temp[i].ToString() == "COMP")
                    {
                        _array[i - 1] = _obj.Comp;
                    }
                    else if (_temp[i].ToString() == "USER")
                    {
                        _array[i - 1] = _obj.UserName;
                    }
                    else
                    {
                        if (e.NewValues[_temp[i].ToString()] != null)
                            _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                        else
                            _array[i - 1] = _temp[i].ToString();

                    }
                }
                services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
            }
        }

        protected void grid3_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            if (txtdeleteparams3.Text.Length > 0)
            {
                string[] _temp = txtdeleteparams3.Text.Split(',');
                string[] _array = new string[txtdeleteparams3.Text.Split(',').Length - 1];
                for (int i = 1; i < _temp.Length; i++)
                {
                    if (_temp[i].ToString() == "COMP")
                    {
                        _array[i - 1] = _obj.Comp;
                    }
                    else if (_temp[i].ToString() == "USER")
                    {
                        _array[i - 1] = _obj.UserName;
                    }
                    else
                    {

                        if (e.Values[_temp[i].ToString()] != null)
                            _array[i - 1] = e.Values[_temp[i].ToString()].ToString();
                        else
                            _array[i - 1] = _temp[i].ToString();

                    }
                }
                services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
            }
        }




        protected void menu1_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport1.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                webObjectHandler.ExportPdf(gridExport1);
            }
            else
            {
                this.Response.StatusCode = 500;
                this.Response.End();
            }
        }

        protected void menu2_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport2.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                webObjectHandler.ExportPdf(gridExport2);
            }
            else
            {
                this.Response.StatusCode = 500;
                this.Response.End();
            }
        }
        protected void menu3_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport3.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                webObjectHandler.ExportPdf(gridExport3);
            }
            else
            {
                this.Response.StatusCode = 500;
                this.Response.End();
            }
        }



        protected void grid1_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView xgrid = (ASPxGridView)sender;
            if (xgrid.IsNewRowEditing)
            {
                DataRow[] rows = ((DataTable)Session["OPL10Grid1Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session["OPL10Grid1Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session["OPL10Grid1Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.Visible = false;
                    else
                        e.Editor.Visible = true;
                }


            }
            else
            {
                DataRow[] rows = ((DataTable)Session["OPL10Grid1Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session["OPL10Grid1Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session["OPL10Grid1Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.Visible = false;
                    else
                        e.Editor.Visible = true;
                }
            }
        }

        protected void grid2_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView xgrid = (ASPxGridView)sender;
            if (xgrid.IsNewRowEditing)
            {
                DataRow[] rows = ((DataTable)Session["OPL10Grid2Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session["OPL10Grid2Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session["OPL10Grid2Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.Visible = false;
                    else
                        e.Editor.Visible = true;
                }


            }
            else
            {
                DataRow[] rows = ((DataTable)Session["OPL10Grid2Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session["OPL10Grid2Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session["OPL10Grid2Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.Visible = false;
                    else
                        e.Editor.Visible = true;
                }
            }
        }

        protected void grid3_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView xgrid = (ASPxGridView)sender;
            if (xgrid.IsNewRowEditing)
            {
                DataRow[] rows = ((DataTable)Session["OPL10Grid3Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session["OPL10Grid3Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session["OPL10Grid3Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.Visible = false;
                    else
                        e.Editor.Visible = true;
                }


            }
            else
            {
                DataRow[] rows = ((DataTable)Session["OPL10Grid3Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (rows.Length > 0)
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session["OPL10Grid3Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                    DataRow[] RowsHidden = ((DataTable)Session["OPL10Grid3Rows"]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Hidden]=1");
                    if (RowsHidden.Length > 0)
                        e.Editor.Visible = false;
                    else
                        e.Editor.Visible = true;
                }
            }
        }

        protected void Grid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            e.Cell.ToolTip = string.Format("{0}", e.CellValue);

            try
            {
                if (e.DataColumn is GridViewDataHyperLinkColumn)
                {
                    if (string.IsNullOrEmpty(e.CellValue.ToString()))
                        e.Cell.Controls[0].Visible = false;

                }
            }
            catch
            {

            }
        }

        protected void grid_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {

            try
            {


                if (e.RowType == GridViewRowType.Data)
                {

                    ASPxGridView gridI = sender as ASPxGridView;
                    object rfont = e.GetValue("RFONT")
                                , rback = e.GetValue("RBACK")
                                , rftsz = e.GetValue("RFTSZ")
                                , rftwt = e.GetValue("RFTWT")
                                , rftst = e.GetValue("RFTST");
                    if (rfont == null) return;



                    try
                    {
                        for (int columnIndex = 0; columnIndex < e.Row.Cells.Count; columnIndex++)
                        {
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.Color, rfont.ToString()); } catch { }
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.BackgroundColor, rback.ToString()); } catch { }
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.FontSize, rftsz.ToString() + "px"); } catch { }
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.FontWeight, Convert.ToInt32(rftwt.ToString()) == 0 ? "normal" : "bold"); } catch { }
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.FontStyle, Convert.ToInt32(rftst.ToString()) == 0 ? "normal" : "italic"); } catch { }
                        }

                    }
                    catch { }



                }
            }
            catch { }
        }


        protected void Grid_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;
            string _sessionname = "OPL10Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";
            switch (e.ButtonType)
            {
                case ColumnCommandButtonType.Edit:

                    DataRow[] EditRows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='EDIT' and [DX Display Order]>0");
                    if (EditRows.Length > 0)
                    {
                        string _col = EditRows[0]["DX Permision Field"].ToString();
                        if (grid.GetRowValues(e.VisibleIndex, _col) != null)
                        {
                            if (grid.GetRowValues(e.VisibleIndex, _col).ToString() == "0")
                            {
                                e.Visible = false;
                            }
                        }
                    }
                    else
                        e.Visible = false;
                    break;
                case ColumnCommandButtonType.Delete:
                    DataRow[] DeleteRows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='DELETE' and [DX Display Order]>0");
                    if (DeleteRows.Length > 0)
                    {
                        string _col = DeleteRows[0]["DX Permision Field"].ToString();
                        if (grid.GetRowValues(e.VisibleIndex, _col) != null)
                        {
                            if (grid.GetRowValues(e.VisibleIndex, _col).ToString() == "0")
                            {
                                e.Visible = false;
                            }
                        }
                    }
                    else
                        e.Visible = false;
                    break;
                case ColumnCommandButtonType.SelectCheckbox:
                case ColumnCommandButtonType.Select:
                    DataRow[] selectrows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='SELECT' and [DX Display Order]>0");
                    string _selcol = selectrows[0]["DX Permision Field"].ToString();
                    if (!string.IsNullOrEmpty(_selcol))
                    {
                        if (grid.GetRowValues(e.VisibleIndex, _selcol).ToString() == "0")
                            e.Enabled = false;
                    }
                    break;
            }
        }

        protected void grid_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            string _sessionname = "OPL10Grid" + gridI.ID.Substring(gridI.ID.Length - 1) + "Rows";
            if (e.ButtonID == "Clone")
            {
                string Sessionn = "OPL10Grid" + gridI.ID.Substring(gridI.ID.Length - 1) + "Rows";
                DataRow[] rows = ((DataTable)Session[Sessionn]).Select("[DX Clone]>0");
                if (rows.Length > 0)
                {
                    copiedValues = new Hashtable();
                    foreach (DataRow row in rows)
                    {
                        copiedValues[row["DX Field ID"].ToString()] = gridI.GetRowValues(e.VisibleIndex, row["DX Field ID"].ToString());
                    }
                }
                Session[gridI.ID + "Cloned"] = "1";
                gridI.AddNewRow();
            }
            else if (e.ButtonID == "SelectAll")
            {
                DataRow[] selectrows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='SELECT' and [DX Display Order]>0");
                string _selcol = selectrows[0]["DX Permision Field"].ToString();
                if (!string.IsNullOrEmpty(_selcol))
                {
                    for (int counter = 0; counter < gridI.VisibleRowCount; counter++)
                    {
                        if (Convert.ToInt32(gridI.GetRowValues(counter, _selcol)) == 1)
                            gridI.Selection.SelectRow(counter);
                    }
                }
                else
                    gridI.Selection.SelectAll();
            }
            else if (e.ButtonID == "UnSelectAll")
            {
                gridI.Selection.UnselectAll();
            }

        }
    }
}