﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.Data;
using System.Collections;
using System.IO;
using System.Threading;
using MSS1.WSGeneric;
using System.ServiceModel;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Diagnostics;
using MSS1.Codes;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System.Web.UI.HtmlControls;

namespace MSS1
{
    public partial class OnePageList50 : Bases
    {
        Language l = new Language();
        Hashtable copiedValues = null, viewedValues = null;
        ServiceUtilities services = new ServiceUtilities();
        fonksiyonlar f = new fonksiyonlar();
        static string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        SessionBase _obj;
        public string currentP1 { get; set; }
        public string currentP2 { get; set; }
        public string currentP3 { get; set; }
        public string currentP4 { get; set; }

        public string OBJ1_pageId { get; set; }
        public string OBJ2_pageId { get; set; }
        public string OBJ3_pageId { get; set; }
        public string OBJ4_pageId { get; set; }
        public string OBJ5_pageId { get; set; }
        public string OBJ6_pageId { get; set; }
        public string OBJ7_pageId { get; set; }
        public string OBJ8_pageId { get; set; }
        public string OBJ9_pageId { get; set; }
        public string OBJ10_pageId { get; set; }
        public string OBJ11_pageId { get; set; }
        public string OBJ12_pageId { get; set; }
        public string OBJ13_pageId { get; set; }
        public string OBJ14_pageId { get; set; }
        public string OBJ15_pageId { get; set; }
        public string OBJ1_fieldId { get; set; }
        public string OBJ2_fieldId { get; set; }
        public string OBJ3_fieldId { get; set; }
        public string OBJ4_fieldId { get; set; }
        public string OBJ5_fieldId { get; set; }
        public string OBJ6_fieldId { get; set; }
        public string OBJ7_fieldId { get; set; }
        public string OBJ8_fieldId { get; set; }
        public string OBJ9_fieldId { get; set; }
        public string OBJ10_fieldId { get; set; }
        public string OBJ11_fieldId { get; set; }
        public string OBJ12_fieldId { get; set; }
        public string OBJ13_fieldId { get; set; }
        public string OBJ14_fieldId { get; set; }
        public string OBJ15_fieldId { get; set; }
        public string pageOrderType
        {
            get
            {
                string _type = "";
                if (OBJ2_pageId == "PG3006")
                {
                    _type = "PREBOOKING";
                }
                else if (OBJ2_pageId == "PG3003")
                {
                    _type = "QUOTE";
                }
                else if (OBJ2_pageId == "P41010" | OBJ2_pageId == "P41020")
                {
                    _type = "SHIP";
                }
                else if (OBJ2_pageId == "P50010" | OBJ2_pageId == "P50020" | OBJ2_pageId == "P50025")
                {
                    _type = "SATINALMAONAY";
                }
                else if (OBJ2_pageId == "P50050" | OBJ2_pageId == "P50060" | OBJ2_pageId == "P50065")
                {
                    _type = "SATIS";
                }
                else if (OBJ2_pageId == "P50030" | OBJ2_pageId == "P50040" | OBJ2_pageId == "P50045")
                {
                    _type = "SATIS_IADE";
                }
                else if (OBJ1_fieldId == "J50070" | OBJ1_fieldId == "J50080" | OBJ1_fieldId == "J50080" | OBJ1_fieldId == "J50085")
                {
                    _type = "ALIMIADE";
                }
                else if (OBJ1_fieldId == "J50110" | OBJ1_fieldId == "J50120" | OBJ1_fieldId == "J50125")
                {
                    _type = "BORCDEKONT";
                }
                else if (OBJ1_fieldId == "J50130" | OBJ1_fieldId == "J50140" | OBJ1_fieldId == "J50145")
                {
                    _type = "ALIMDEKONT";
                }
                else if (OBJ1_pageId == "P155100" & OBJ1_fieldId == "J155100")
                    _type = "TAYFA";
                else if (OBJ1_pageId == "P13000" & OBJ1_fieldId == "J13000")
                    _type = "EMPLOOYE";
                else if (OBJ1_pageId == "P256100" & OBJ1_fieldId == "J256100")
                    _type = "JOBS";
                else if (OBJ1_pageId == "P161110" | OBJ1_pageId == "P161120" | OBJ1_pageId == "P161130")
                    _type = "BOOKING";
                else if (OBJ1_pageId == "PG5005" | OBJ1_pageId == "PG5012")
                    _type = "MUSTERI";
                else if (OBJ1_pageId == "PG5006" | OBJ1_pageId == "PG5013")
                    _type = "TEDARIKCI";
                else if (OBJ1_pageId == "PG5004")
                    _type = "FIRMA";
                return _type;
            }
        }
        public string pageFaturaStatu
        {
            get
            {
                string _result = string.Empty;
                if (OBJ1_fieldId == "J50010") //Satınalma İşlemde
                {
                    _result = "ALIMISLEMDE";
                }
                else if (OBJ1_fieldId == "J50020") //Satınalma Tamamlandı
                {
                    _result = "ALIMTAMAMLANDI";
                }
                else if (OBJ1_fieldId == "J50025") //Satınalma Tamamlandı
                {
                    _result = "ALIMTAMAMLANDITUM";
                }
                else if (OBJ1_fieldId == "J50030") //SATIŞ IADE ISLEMDE 
                {
                    _result = "IADEISLEMDE";
                }
                else if (OBJ1_fieldId == "J50040") //SATIŞ IADE ISLEMDE 
                {
                    _result = "IADETAMAMLANDI";
                }
                else if (OBJ1_fieldId == "J50045") //Satınalma Tamamlandı
                {
                    _result = "IADETAMAMLANDITUM";
                }
                else if (OBJ1_fieldId == "J50050") //Satış İşlemde
                {
                    _result = "SATISISLEMDE";
                }
                else if (OBJ1_fieldId == "J50060") //Satış Tamamlandı
                {
                    _result = "SATISTAMAMLANDI";
                }
                else if (OBJ1_fieldId == "J50065") //Satış Tamamlandı
                {
                    _result = "SATISTAMAMLANDITUM";
                }
                else if (OBJ1_fieldId == "J50070") //SATINALMA  IADE ISLEMDE 
                {
                    _result = "ALIMIADEISLEMDE";
                }
                else if (OBJ1_fieldId == "J50080") //SATINALMA IADE ISLEMDE 
                {
                    _result = "ALIMIADETAMAMLANDI";
                }
                else if (OBJ1_fieldId == "J50085") //SATINALMA IADE ISLEMDE 
                {
                    _result = "ALIMIADETAMAMLANDITUM";
                }
                else if (OBJ1_fieldId == "J50110") //BORÇ DEKONTU ISLEMDE 
                {
                    _result = "DEKONTISLEMDE";
                }
                else if (OBJ1_fieldId == "J50120") //BORÇ DEKONTU TAMAMLANAN
                {
                    _result = "DEKONTTAMAMLANDI";
                }
                else if (OBJ1_fieldId == "J50125") //BORÇ DEKONTU TAMAMLANAN
                {
                    _result = "DEKONTTAMAMLANDITUM";
                }
                else if (OBJ1_fieldId == "J50130") //ALIM DEKONTU ISLEMDE 
                {
                    _result = "DEKONTISLEMDE";
                }
                else if (OBJ1_fieldId == "J50140") //ALIM DEKONTU TAMAMLANAN
                {
                    _result = "ALIMDEKONTTAMAMLANDI";
                }
                else if (OBJ1_fieldId == "J50145") //ALIM DEKONTU TAMAMLANAN
                {
                    _result = "ALIMDEKONTTAMAMLANDITUM";
                }
                return _result;
            }
        }
        public string postURL
        {
            get
            {
                string _url = "";
                if (pageOrderType == "QUOTE")
                {
                    _url = "/Quotations/mainQuotations.aspx";
                }
                else if (pageOrderType == "PREBOOKING")
                {
                    _url = "/Prebookings/mainPrebookings.aspx";
                }
                else if (pageOrderType == "SATINALMAONAY" | pageOrderType == "SATIS_IADE")
                {
                    if ((OBJ1_pageId == "P50010" & OBJ1_fieldId == "J50010") | (OBJ1_pageId == "P50030" & OBJ1_fieldId == "J50030"))
                        _url = "/FaturaOnGiris.aspx";
                    else
                        _url = "/FaturaCard/Satinalma.aspx";
                }
                else if (pageOrderType == "SATIS")
                {
                    _url = "/FaturaCard/Satis.aspx";
                }
                else if (pageOrderType == "BORCDEKONT")
                {
                    _url = "/FaturaCard/Satis.aspx";
                }
                else if (pageOrderType == "ALIMIADE")
                {
                    _url = "/FaturaCard/Satinalmaiade.aspx";
                }
                else if (pageOrderType == "ALIMDEKONT")
                {
                    _url = "/FaturaCard/Satinalma.aspx";
                }
                else if (pageOrderType == "TAYFA")
                {
                    _url = "/TAYFA/TayfaCard.aspx";
                }
                else if (pageOrderType == "EMPLOOYE")
                {
                    _url = "/Emplooye/EmplooyeCard.aspx";
                }
                else if (pageOrderType == "BOOKING")
                {
                    _url = "/Booking/BookingCard.aspx";
                }
                else if (pageOrderType == "JOBS")
                {
                    _url = "projeolustur.aspx?MusteriNo=C000012&";
                }
                else if (pageOrderType == "MUSTERI")
                {
                    _url = "CRM/musteriCard.aspx?PageName=OnePageList50&";
                }
                else if (pageOrderType == "TEDARIKCI")
                {
                    _url = "CRM/tedarikciCard.aspx?PageName=OnePageList50&";
                }
                else if (pageOrderType == "FIRMA")
                {
                    _url = "CRM/FirmaCard.aspx?PageName=OnePageList50&";
                }
                return _url;
            }
        }
        public string postURLParameter
        {
            get
            {
                string _url = "";
                if (pageOrderType == "QUOTE")
                {
                    _url = "MQID";
                }
                else if (pageOrderType == "PREBOOKING")
                {
                    _url = "MPID";
                }
                else if (pageOrderType == "TAYFA")
                {
                    _url = "TayfaNo";
                }
                else if (pageOrderType == "EMPLOOYE")
                {
                    _url = "Id";
                }
                else if (pageOrderType == "BOOKING")
                {
                    _url = "BookingNo";
                }
                else if (pageOrderType == "JOBS")
                {
                    _url = "ProjeNo";
                }
                else if (pageOrderType == "MUSTERI")
                {
                    _url = "CCUID";
                }
                else if (pageOrderType == "TEDARIKCI")
                {
                    _url = "CCUID";
                }
                else if (pageOrderType == "FIRMA")
                {
                    _url = "CCUID";
                }
                else
                {
                    _url = "invoiceNo";
                }
                return _url;
            }
        }
        public string invoiceType
        {
            get
            {
                string _invoiceType = "";
                if (OBJ1_fieldId == "J50010" | OBJ1_fieldId == "J50020" | OBJ1_fieldId == "J50025")
                {
                    _invoiceType = "SATINALMA";
                }
                else if (OBJ1_fieldId == "J50030" | OBJ1_fieldId == "J50040" | OBJ1_fieldId == "J50045")
                {
                    _invoiceType = "SATIS_IADE";
                }
                else if (OBJ1_fieldId == "J50050" | OBJ1_fieldId == "J50060" | OBJ1_fieldId == "J50065")
                {
                    _invoiceType = "SATIS";
                }
                else if (OBJ1_fieldId == "J50070" | OBJ1_fieldId == "J50080" | OBJ1_fieldId == "J50085")
                {
                    _invoiceType = "ALIMIADE";
                }
                else if (OBJ1_fieldId == "J50110" | OBJ1_fieldId == "J50120" | OBJ1_fieldId == "J50125")
                {
                    _invoiceType = "BORCDEKONT";
                }
                else if (OBJ1_fieldId == "J50130" | OBJ1_fieldId == "J50140" | OBJ1_fieldId == "J50145")
                {
                    _invoiceType = "ALIMDEKONT";
                }

                return _invoiceType;
            }
        }
        private string eFatura_DahiliType
        {
            get
            {
                string _invoiceType = "";
                if (OBJ1_fieldId == "J50150" | OBJ1_fieldId == "J50160" | OBJ1_fieldId == "J50170")
                {
                    _invoiceType = "DAHILIDEKONT";
                }
                else if (OBJ1_fieldId == "J50210" | OBJ1_fieldId == "J50220" | OBJ1_fieldId == "J50230")
                {
                    _invoiceType = "EFGELEN";
                }
                else if (OBJ1_fieldId == "J50310" | OBJ1_fieldId == "J50320" | OBJ1_fieldId == "J50330")
                {
                    _invoiceType = "EFGIDEN";
                }


                return _invoiceType;
            }
        }
        public string invoiceStatu
        {
            get
            {
                string _fatStatu = "";
                switch (OBJ1_fieldId)
                {
                    case "J50010":
                        _fatStatu = "ALIMISLEMDE";
                        break;
                    case "J50020":
                        _fatStatu = "ALIMTAMAMLANDI";
                        break;
                    case "J50030":
                        _fatStatu = "IADEISLEMDE";
                        break;
                    case "J50040":
                        _fatStatu = "IADETAMAMLANDI";
                        break;
                    case "J50050":
                        _fatStatu = "SATISISLEMDE";
                        break;
                    case "J50060":
                        _fatStatu = "SATISTAMAMLANDI";
                        break;
                    case "J50070":
                        _fatStatu = "ALIMIADEISLEMDE";
                        break;
                    case "J50080":
                        _fatStatu = "ALIMIADETAMAMLANDI";
                        break;

                    case "J50110":
                        _fatStatu = "DEKONTISLEMDE";
                        break;
                    case "J50120":
                        _fatStatu = "DEKONTTAMAMLANDI";
                        break;
                    case "J50130":
                        _fatStatu = "ALIMDEKONTISLEMDE";
                        break;
                    case "J50140":
                        _fatStatu = "ALIMDEKONTTAMAMLANDI";
                        break;

                }
                return _fatStatu;
            }
        }
        private string DynamicPageLink
        {
            get
            {
                return f.tekbirsonucdonder("select [DX Path]+[DX Path2] from [0C_50032_01_WEB PAGE CAPTIONS] where COMPANY='" + _obj.Comp + "'" +
                    " and PageID='MasterPage' and FieldID='" + Request.QueryString["P"].ToString() + "'");
            }
        }
        private void QueryStringParameterDesing()
        {
            if (Request.QueryString["P"] != null)
            {
                string _pagelink = DynamicPageLink;

                string[] objs = _pagelink.Split('&');

                int i = 1;
                foreach (string str in objs)
                {
                    #region dynamic page objs
                    if (i == 1)
                    {
                        OBJ1_pageId = str.Split(';')[0];
                        OBJ1_fieldId = str.Split(';')[1];
                    }
                    else if (i == 2)
                    {
                        OBJ2_pageId = str.Split(';')[0];
                        OBJ2_fieldId = str.Split(';')[1];
                    }
                    else if (i == 3)
                    {
                        OBJ3_pageId = str.Split(';')[0];
                        OBJ3_fieldId = str.Split(';')[1];
                    }
                    else if (i == 4)
                    {
                        OBJ4_pageId = str.Split(';')[0];
                        OBJ4_fieldId = str.Split(';')[1];
                    }
                    else if (i == 5)
                    {
                        OBJ5_pageId = str.Split(';')[0];
                        OBJ5_fieldId = str.Split(';')[1];
                    }
                    else if (i == 6)
                    {
                        OBJ6_pageId = str.Split(';')[0];
                        OBJ6_fieldId = str.Split(';')[1];
                    }
                    else if (i == 7)
                    {
                        OBJ7_pageId = str.Split(';')[0];
                        OBJ7_fieldId = str.Split(';')[1];
                    }
                    else if (i == 8)
                    {
                        OBJ8_pageId = str.Split(';')[0];
                        OBJ8_fieldId = str.Split(';')[1];
                    }
                    else if (i == 9)
                    {
                        OBJ9_pageId = str.Split(';')[0];
                        OBJ9_fieldId = str.Split(';')[1];
                    }
                    else if (i == 10)
                    {
                        OBJ10_pageId = str.Split(';')[0];
                        OBJ10_fieldId = str.Split(';')[1];
                    }
                    else if (i == 11)
                    {
                        OBJ11_pageId = str.Split(';')[0];
                        OBJ11_fieldId = str.Split(';')[1];
                    }
                    else if (i == 12)
                    {
                        OBJ12_pageId = str.Split(';')[0];
                        OBJ12_fieldId = str.Split(';')[1];
                    }
                    else if (i == 13)
                    {
                        OBJ13_pageId = str.Split(';')[0];
                        OBJ13_fieldId = str.Split(';')[1];
                    }
                    else if (i == 14)
                    {
                        OBJ14_pageId = str.Split(';')[0];
                        OBJ14_fieldId = str.Split(';')[1];
                    }
                    else if (i == 15)
                    {
                        OBJ15_pageId = str.Split(';')[0];
                        OBJ15_fieldId = str.Split(';')[1];
                    }
                    #endregion
                    i++;
                }

                if (!string.IsNullOrEmpty(Request.QueryString["P1"]))
                    currentP1 = Request.QueryString["P1"].ToString();
                else
                    currentP1 = string.Empty;

                //P2 P3 P4 eklendi

                if (!string.IsNullOrEmpty(Request.QueryString["P2"]))
                    currentP2 = Request.QueryString["P2"].ToString();
                else
                    currentP2 = string.Empty;

                if (!string.IsNullOrEmpty(Request.QueryString["P3"]))
                    currentP3 = Request.QueryString["P3"].ToString();
                else
                    currentP3 = string.Empty;

                if (!string.IsNullOrEmpty(Request.QueryString["P4"]))
                    currentP4 = Request.QueryString["P4"].ToString();
                else
                    currentP4 = string.Empty;
            }
            else
            {

                string[] obj1, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10, obj11, obj12, obj13, obj14, obj15 = { };
                string _val = string.Empty;
                obj1 = Request.QueryString["OJ1"].ToString().Split(';');
                try { obj2 = Request.QueryString["OJ2"].ToString().Split(';'); } catch { _val = Request.QueryString["OJ1"].ToString().Split(';')[0] + ";OJ9999"; obj2 = _val.Split(';'); }
                try { obj3 = Request.QueryString["OJ3"].ToString().Split(';'); } catch { _val = Request.QueryString["OJ1"].ToString().Split(';')[0] + ";OJ9999"; obj3 = _val.Split(';'); }
                try { obj4 = Request.QueryString["OJ4"].ToString().Split(';'); } catch { _val = Request.QueryString["OJ1"].ToString().Split(';')[0] + ";OJ9999"; obj4 = _val.Split(';'); }
                try { obj5 = Request.QueryString["OJ5"].ToString().Split(';'); } catch { _val = Request.QueryString["OJ1"].ToString().Split(';')[0] + ";OJ9999"; obj5 = _val.Split(';'); }
                try { obj6 = Request.QueryString["OJ6"].ToString().Split(';'); } catch { _val = Request.QueryString["OJ1"].ToString().Split(';')[0] + ";OJ9999"; obj6 = _val.Split(';'); }
                try { obj7 = Request.QueryString["OJ7"].ToString().Split(';'); } catch { _val = Request.QueryString["OJ1"].ToString().Split(';')[0] + ";OJ9999"; obj7 = _val.Split(';'); }
                try { obj8 = Request.QueryString["OJ8"].ToString().Split(';'); } catch { _val = Request.QueryString["OJ1"].ToString().Split(';')[0] + ";OJ9999"; obj8 = _val.Split(';'); }
                try { obj9 = Request.QueryString["OJ9"].ToString().Split(';'); } catch { _val = Request.QueryString["OJ1"].ToString().Split(';')[0] + ";OJ9999"; obj9 = _val.Split(';'); }
                try { obj10 = Request.QueryString["OJ10"].ToString().Split(';'); } catch { _val = Request.QueryString["OJ1"].ToString().Split(';')[0] + ";OJ9999"; obj10 = _val.Split(';'); }
                try { obj11 = Request.QueryString["OJ11"].ToString().Split(';'); } catch { _val = Request.QueryString["OJ1"].ToString().Split(';')[0] + ";OJ9999"; obj11 = _val.Split(';'); }
                try { obj12 = Request.QueryString["OJ12"].ToString().Split(';'); } catch { _val = Request.QueryString["OJ1"].ToString().Split(';')[0] + ";OJ9999"; obj12 = _val.Split(';'); }
                try { obj13 = Request.QueryString["OJ13"].ToString().Split(';'); } catch { _val = Request.QueryString["OJ1"].ToString().Split(';')[0] + ";OJ9999"; obj13 = _val.Split(';'); }
                try { obj14 = Request.QueryString["OJ14"].ToString().Split(';'); } catch { _val = Request.QueryString["OJ1"].ToString().Split(';')[0] + ";OJ9999"; obj14 = _val.Split(';'); }
                try { obj15 = Request.QueryString["OJ15"].ToString().Split(';'); } catch { _val = Request.QueryString["OJ1"].ToString().Split(';')[0] + ";OJ9999"; obj15 = _val.Split(';'); }



                if (!string.IsNullOrEmpty(Request.QueryString["P1"]))
                    currentP1 = Request.QueryString["P1"].ToString();
                else
                    currentP1 = string.Empty;

                //P2 P3 P4 eklendi

                if (!string.IsNullOrEmpty(Request.QueryString["P2"]))
                    currentP2 = Request.QueryString["P2"].ToString();
                else
                    currentP2 = string.Empty;

                if (!string.IsNullOrEmpty(Request.QueryString["P3"]))
                    currentP3 = Request.QueryString["P3"].ToString();
                else
                    currentP3 = string.Empty;

                if (!string.IsNullOrEmpty(Request.QueryString["P4"]))
                    currentP4 = Request.QueryString["P4"].ToString();
                else
                    currentP4 = string.Empty;

                try
                {
                    OBJ1_pageId = obj1[0].ToString();
                    OBJ1_fieldId = obj1[1].ToString();

                    OBJ2_pageId = obj2[0].ToString();
                    OBJ2_fieldId = obj2[1].ToString();

                    OBJ3_pageId = obj3[0].ToString();
                    OBJ3_fieldId = obj3[1].ToString();

                    OBJ4_pageId = obj4[0].ToString();
                    OBJ4_fieldId = obj4[1].ToString();

                    OBJ5_pageId = obj5[0].ToString();
                    OBJ5_fieldId = obj5[1].ToString();

                    OBJ6_pageId = obj6[0].ToString();
                    OBJ6_fieldId = obj6[1].ToString();

                    OBJ7_pageId = obj7[0].ToString();
                    OBJ7_fieldId = obj7[1].ToString();

                    OBJ8_pageId = obj8[0].ToString();
                    OBJ8_fieldId = obj8[1].ToString();

                    OBJ9_pageId = obj9[0].ToString();
                    OBJ9_fieldId = obj9[1].ToString();

                    OBJ10_pageId = obj10[0].ToString();
                    OBJ10_fieldId = obj10[1].ToString();

                    OBJ11_pageId = obj11[0].ToString();
                    OBJ11_fieldId = obj11[1].ToString();

                    OBJ12_pageId = obj12[0].ToString();
                    OBJ12_fieldId = obj12[1].ToString();

                    OBJ13_pageId = obj13[0].ToString();
                    OBJ13_fieldId = obj13[1].ToString();

                    OBJ14_pageId = obj14[0].ToString();
                    OBJ14_fieldId = obj14[1].ToString();

                    OBJ15_pageId = obj15[0].ToString();
                    OBJ15_fieldId = obj15[1].ToString();
                }
                catch
                {
                    OBJ1_pageId = string.Empty;
                    OBJ1_fieldId = string.Empty;
                    OBJ2_pageId = string.Empty;
                    OBJ2_fieldId = string.Empty;
                    OBJ3_pageId = string.Empty;
                    OBJ3_fieldId = string.Empty;
                    OBJ4_pageId = string.Empty;
                    OBJ4_fieldId = string.Empty;
                    OBJ5_pageId = string.Empty;
                    OBJ5_fieldId = string.Empty;
                    OBJ6_pageId = string.Empty;
                    OBJ6_fieldId = string.Empty;

                    OBJ7_pageId = string.Empty;
                    OBJ7_fieldId = string.Empty;

                    OBJ8_pageId = string.Empty;
                    OBJ8_fieldId = string.Empty;

                    OBJ9_pageId = string.Empty;
                    OBJ9_fieldId = string.Empty;

                    OBJ10_pageId = string.Empty;
                    OBJ10_fieldId = string.Empty;

                    OBJ11_pageId = string.Empty;
                    OBJ11_fieldId = string.Empty;

                    OBJ12_pageId = string.Empty;
                    OBJ12_fieldId = string.Empty;

                    OBJ13_pageId = string.Empty;
                    OBJ13_fieldId = string.Empty;

                    OBJ14_pageId = string.Empty;
                    OBJ14_fieldId = string.Empty;

                    OBJ15_pageId = string.Empty;
                    OBJ15_fieldId = string.Empty;
                }
            }

            grid1.JSProperties["cpPageLinked"] = false;
            grid2.JSProperties["cpPageLinked"] = false;
            grid3.JSProperties["cpPageLinked"] = false;
            grid4.JSProperties["cpPageLinked"] = false;
            grid5.JSProperties["cpPageLinked"] = false;
            grid6.JSProperties["cpPageLinked"] = false;
            grid7.JSProperties["cpPageLinked"] = false;
            grid8.JSProperties["cpPageLinked"] = false;
            grid9.JSProperties["cpPageLinked"] = false;
            grid10.JSProperties["cpPageLinked"] = false;
            grid11.JSProperties["cpPageLinked"] = false;
            grid12.JSProperties["cpPageLinked"] = false;
            grid13.JSProperties["cpPageLinked"] = false;
            grid14.JSProperties["cpPageLinked"] = false;
            grid15.JSProperties["cpPageLinked"] = false;

            grid1.JSProperties["cpIsUpdated"] = false;
            grid2.JSProperties["cpIsUpdated"] = false;
            grid3.JSProperties["cpIsUpdated"] = false;
            grid4.JSProperties["cpIsUpdated"] = false;
            grid5.JSProperties["cpIsUpdated"] = false;
            grid6.JSProperties["cpIsUpdated"] = false;
            grid7.JSProperties["cpIsUpdated"] = false;
            grid8.JSProperties["cpIsUpdated"] = false;
            grid9.JSProperties["cpIsUpdated"] = false;
            grid10.JSProperties["cpIsUpdated"] = false;
            grid11.JSProperties["cpIsUpdated"] = false;
            grid12.JSProperties["cpIsUpdated"] = false;
            grid13.JSProperties["cpIsUpdated"] = false;
            grid14.JSProperties["cpIsUpdated"] = false;
            grid15.JSProperties["cpIsUpdated"] = false;

            grid1.JSProperties["cpIsLinked"] = false;
            grid2.JSProperties["cpIsLinked"] = false;
            grid3.JSProperties["cpIsLinked"] = false;
            grid4.JSProperties["cpIsLinked"] = false;
            grid5.JSProperties["cpIsLinked"] = false;
            grid6.JSProperties["cpIsLinked"] = false;
            grid7.JSProperties["cpIsLinked"] = false;
            grid8.JSProperties["cpIsLinked"] = false;
            grid9.JSProperties["cpIsLinked"] = false;
            grid10.JSProperties["cpIsLinked"] = false;
            grid11.JSProperties["cpIsLinked"] = false;
            grid12.JSProperties["cpIsLinked"] = false;
            grid13.JSProperties["cpIsLinked"] = false;
            grid14.JSProperties["cpIsLinked"] = false;
            grid15.JSProperties["cpIsLinked"] = false;

            grid1.JSProperties["cpOpType"] = string.Empty;
            grid2.JSProperties["cpOpType"] = string.Empty;
            grid3.JSProperties["cpOpType"] = string.Empty;
            grid4.JSProperties["cpOpType"] = string.Empty;
            grid5.JSProperties["cpOpType"] = string.Empty;
            grid6.JSProperties["cpOpType"] = string.Empty;
            grid7.JSProperties["cpOpType"] = string.Empty;
            grid8.JSProperties["cpOpType"] = string.Empty;
            grid9.JSProperties["cpOpType"] = string.Empty;
            grid10.JSProperties["cpOpType"] = string.Empty;
            grid11.JSProperties["cpOpType"] = string.Empty;
            grid12.JSProperties["cpOpType"] = string.Empty;
            grid13.JSProperties["cpOpType"] = string.Empty;
            grid14.JSProperties["cpOpType"] = string.Empty;
            grid15.JSProperties["cpOpType"] = string.Empty;



            grid1.JSProperties["cpQval"] = string.Empty;
            grid2.JSProperties["cpQval"] = string.Empty;
            grid3.JSProperties["cpQval"] = string.Empty;
            grid4.JSProperties["cpQval"] = string.Empty;
            grid5.JSProperties["cpQval"] = string.Empty;
            grid6.JSProperties["cpQval"] = string.Empty;
            grid7.JSProperties["cpQval"] = string.Empty;
            grid8.JSProperties["cpQval"] = string.Empty;
            grid9.JSProperties["cpQval"] = string.Empty;
            grid10.JSProperties["cpQval"] = string.Empty;
            grid11.JSProperties["cpQval"] = string.Empty;
            grid12.JSProperties["cpQval"] = string.Empty;
            grid13.JSProperties["cpQval"] = string.Empty;
            grid14.JSProperties["cpQval"] = string.Empty;
            grid15.JSProperties["cpQval"] = string.Empty;

            grid1.JSProperties["cpMsg"] = string.Empty;
            grid2.JSProperties["cpMsg"] = string.Empty;
            grid3.JSProperties["cpMsg"] = string.Empty;
            grid4.JSProperties["cpMsg"] = string.Empty;
            grid5.JSProperties["cpMsg"] = string.Empty;
            grid6.JSProperties["cpMsg"] = string.Empty;
            grid7.JSProperties["cpMsg"] = string.Empty;
            grid8.JSProperties["cpMsg"] = string.Empty;
            grid9.JSProperties["cpMsg"] = string.Empty;
            grid10.JSProperties["cpMsg"] = string.Empty;
            grid11.JSProperties["cpMsg"] = string.Empty;
            grid12.JSProperties["cpMsg"] = string.Empty;
            grid13.JSProperties["cpMsg"] = string.Empty;
            grid14.JSProperties["cpMsg"] = string.Empty;
            grid15.JSProperties["cpMsg"] = string.Empty;

            grid1.JSProperties["cpIslemId"] = string.Empty;
            grid2.JSProperties["cpIslemId"] = string.Empty;
            grid3.JSProperties["cpIslemId"] = string.Empty;
            grid4.JSProperties["cpIslemId"] = string.Empty;
            grid5.JSProperties["cpIslemId"] = string.Empty;
            grid6.JSProperties["cpIslemId"] = string.Empty;
            grid7.JSProperties["cpIslemId"] = string.Empty;
            grid8.JSProperties["cpIslemId"] = string.Empty;
            grid9.JSProperties["cpIslemId"] = string.Empty;
            grid10.JSProperties["cpIslemId"] = string.Empty;
            grid11.JSProperties["cpIslemId"] = string.Empty;
            grid12.JSProperties["cpIslemId"] = string.Empty;
            grid13.JSProperties["cpIslemId"] = string.Empty;
            grid14.JSProperties["cpIslemId"] = string.Empty;
            grid15.JSProperties["cpIslemId"] = string.Empty;


            grid1.JSProperties["cpLink"] = string.Empty;
            grid2.JSProperties["cpLink"] = string.Empty;
            grid3.JSProperties["cpLink"] = string.Empty;
            grid4.JSProperties["cpLink"] = string.Empty;
            grid5.JSProperties["cpLink"] = string.Empty;
            grid6.JSProperties["cpLink"] = string.Empty;
            grid7.JSProperties["cpLink"] = string.Empty;
            grid8.JSProperties["cpLink"] = string.Empty;
            grid9.JSProperties["cpLink"] = string.Empty;
            grid10.JSProperties["cpLink"] = string.Empty;
            grid11.JSProperties["cpLink"] = string.Empty;
            grid12.JSProperties["cpLink"] = string.Empty;
            grid13.JSProperties["cpLink"] = string.Empty;
            grid14.JSProperties["cpLink"] = string.Empty;
            grid15.JSProperties["cpLink"] = string.Empty;

            grid1.JSProperties["cpQueryStrings"] = string.Empty;
            grid2.JSProperties["cpQueryStrings"] = string.Empty;
            grid3.JSProperties["cpQueryStrings"] = string.Empty;
            grid4.JSProperties["cpQueryStrings"] = string.Empty;
            grid5.JSProperties["cpQueryStrings"] = string.Empty;
            grid6.JSProperties["cpQueryStrings"] = string.Empty;
            grid7.JSProperties["cpQueryStrings"] = string.Empty;
            grid8.JSProperties["cpQueryStrings"] = string.Empty;
            grid9.JSProperties["cpQueryStrings"] = string.Empty;
            grid10.JSProperties["cpQueryStrings"] = string.Empty;
            grid11.JSProperties["cpQueryStrings"] = string.Empty;
            grid12.JSProperties["cpQueryStrings"] = string.Empty;
            grid13.JSProperties["cpQueryStrings"] = string.Empty;
            grid14.JSProperties["cpQueryStrings"] = string.Empty;
            grid15.JSProperties["cpQueryStrings"] = string.Empty;

            grid1.JSProperties["cpDevexPopup"] = false;
            grid2.JSProperties["cpDevexPopup"] = false;
            grid3.JSProperties["cpDevexPopup"] = false;
            grid4.JSProperties["cpDevexPopup"] = false;
            grid5.JSProperties["cpDevexPopup"] = false;
            grid6.JSProperties["cpDevexPopup"] = false;
            grid7.JSProperties["cpDevexPopup"] = false;
            grid8.JSProperties["cpDevexPopup"] = false;
            grid9.JSProperties["cpDevexPopup"] = false;
            grid10.JSProperties["cpDevexPopup"] = false;
            grid11.JSProperties["cpDevexPopup"] = false;
            grid12.JSProperties["cpDevexPopup"] = false;
            grid13.JSProperties["cpDevexPopup"] = false;
            grid14.JSProperties["cpDevexPopup"] = false;
            grid15.JSProperties["cpDevexPopup"] = false;

            grid1.JSProperties["cpSource"] = string.Empty;
            grid2.JSProperties["cpSource"] = string.Empty;
            grid3.JSProperties["cpSource"] = string.Empty;
            grid4.JSProperties["cpSource"] = string.Empty;
            grid5.JSProperties["cpSource"] = string.Empty;
            grid6.JSProperties["cpSource"] = string.Empty;
            grid7.JSProperties["cpSource"] = string.Empty;
            grid8.JSProperties["cpSource"] = string.Empty;
            grid9.JSProperties["cpSource"] = string.Empty;
            grid10.JSProperties["cpSource"] = string.Empty;
            grid11.JSProperties["cpSource"] = string.Empty;
            grid12.JSProperties["cpSource"] = string.Empty;
            grid13.JSProperties["cpSource"] = string.Empty;
            grid14.JSProperties["cpSource"] = string.Empty;
            grid15.JSProperties["cpSource"] = string.Empty;


            grid1.JSProperties["cpProcType"] = string.Empty;
            grid2.JSProperties["cpProcType"] = string.Empty;
            grid3.JSProperties["cpProcType"] = string.Empty;
            grid4.JSProperties["cpProcType"] = string.Empty;
            grid5.JSProperties["cpProcType"] = string.Empty;
            grid6.JSProperties["cpProcType"] = string.Empty;
            grid7.JSProperties["cpProcType"] = string.Empty;
            grid8.JSProperties["cpProcType"] = string.Empty;
            grid9.JSProperties["cpProcType"] = string.Empty;
            grid10.JSProperties["cpProcType"] = string.Empty;
            grid11.JSProperties["cpProcType"] = string.Empty;
            grid12.JSProperties["cpProcType"] = string.Empty;
            grid13.JSProperties["cpProcType"] = string.Empty;
            grid14.JSProperties["cpProcType"] = string.Empty;
            grid15.JSProperties["cpProcType"] = string.Empty;

            CallbackPanelGeneral.JSProperties["cpProcType"] = string.Empty;
            CallbackPanelGeneral.JSProperties["cpGId"] = string.Empty;


        }
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");

            SetSessionValues();
            PageInit();

        }
        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }
        void PageInit()
        {
            QueryStringParameterDesing();


            if (OBJ1_pageId.Length > 0 & OBJ1_fieldId.Length > 0)
            {
                string OIds = OBJ1_pageId, FIds = OBJ1_fieldId;
                List<object> GrdObjs = new List<object>();
                GrdObjs.Add(grid1);

                bool _IsNewTab = DynamicUtilsV1.IsOpenNewTab(OBJ1_pageId, OBJ1_fieldId, _obj.Comp);
                bool _IsWithoutLeftSide = false;
                if (_IsNewTab & Request.QueryString["ONT"] != null)
                    _IsWithoutLeftSide = true;

                if (!_IsNewTab) _IsWithoutLeftSide = true;

                if (OBJ2_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                {
                    GrdObjs.Add(grid2);
                    OIds += "," + OBJ2_pageId;
                    FIds += "," + OBJ2_fieldId;
                }
                else
                    grid2.Visible = false;
                if (OBJ3_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                {
                    GrdObjs.Add(grid3);
                    OIds += "," + OBJ3_pageId;
                    FIds += "," + OBJ3_fieldId;
                }
                else
                    grid3.Visible = false;
                if (OBJ4_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                {
                    GrdObjs.Add(grid4);
                    OIds += "," + OBJ4_pageId;
                    FIds += "," + OBJ4_fieldId;
                }
                else
                    grid4.Visible = false;
                if (OBJ5_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                {
                    GrdObjs.Add(grid5);
                    OIds += "," + OBJ5_pageId;
                    FIds += "," + OBJ5_fieldId;
                }
                else
                    grid5.Visible = false;
                if (OBJ6_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                {
                    GrdObjs.Add(grid6);
                    OIds += "," + OBJ6_pageId;
                    FIds += "," + OBJ6_fieldId;
                }
                else
                    grid6.Visible = false;
                if (OBJ7_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                {
                    GrdObjs.Add(grid7);
                    OIds += "," + OBJ7_pageId;
                    FIds += "," + OBJ7_fieldId;
                }
                else
                    grid7.Visible = false;
                if (OBJ8_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                {
                    GrdObjs.Add(grid8);
                    OIds += "," + OBJ8_pageId;
                    FIds += "," + OBJ8_fieldId;
                }
                else
                    grid8.Visible = false;
                if (OBJ9_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                {
                    GrdObjs.Add(grid9);
                    OIds += "," + OBJ9_pageId;
                    FIds += "," + OBJ9_fieldId;
                }
                else
                    grid9.Visible = false;
                if (OBJ10_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                {
                    GrdObjs.Add(grid10);
                    OIds += "," + OBJ10_pageId;
                    FIds += "," + OBJ10_fieldId;
                }
                else
                    grid10.Visible = false;
                if (OBJ11_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                {
                    GrdObjs.Add(grid11);
                    OIds += "," + OBJ11_pageId;
                    FIds += "," + OBJ11_fieldId;
                }
                else
                    grid11.Visible = false;
                if (OBJ12_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                {
                    GrdObjs.Add(grid12);
                    OIds += "," + OBJ12_pageId;
                    FIds += "," + OBJ12_fieldId;
                }
                else
                    grid12.Visible = false;
                if (OBJ13_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                {
                    GrdObjs.Add(grid13);
                    OIds += "," + OBJ13_pageId;
                    FIds += "," + OBJ13_fieldId;
                }
                else
                    grid13.Visible = false;
                if (OBJ14_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                {
                    GrdObjs.Add(grid14);
                    OIds += "," + OBJ14_pageId;
                    FIds += "," + OBJ14_fieldId;
                }
                else
                    grid14.Visible = false;
                if (OBJ15_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                {
                    GrdObjs.Add(grid15);
                    OIds += "," + OBJ15_pageId;
                    FIds += "," + OBJ15_fieldId;
                }
                else
                    grid15.Visible = false;


                DynamicUtilsV1.FillDynamicColumns(GrdObjs, OIds, FIds, HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString()
                , HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), _obj.UserName, _obj.Comp, _obj.SelectedLanguage, _obj.GuidKey);


            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            QueryStringParameterDesing();
            if (IsCallback | IsPostBack) return;
            DilGetir();
            GetParameters();
            CollapseDisplay();
            FillDynamicObjects();
            RadioListOrder();
            if (Request.Url.ToString().Contains("P="))
            {
                txtDragParams.Text = _obj.UserName + ";" + _obj.Comp + ";" + Request.QueryString["P"].ToString() + "_" + f.tekbirsonucdonder("select [DX Obje ID]+'_'+[FIELD] from [0_000WEBPAGECAPTIONS] where COMPANY='" + _obj.Comp + "'" +
                 " and [PAGEID]='" + Request.QueryString["P"].ToString() + "' and [TYPE2]='DRAGUPLOAD'");
            }
            else
            {
                txtDragParams.Text = _obj.UserName + ";" + _obj.Comp + ";" + Request.QueryString["OJ1"].ToString().Split(';')[0] + "_" + f.tekbirsonucdonder("select [DX Obje ID]+'_'+[FIELD] from [0_000WEBPAGECAPTIONS] where COMPANY='" + _obj.Comp + "'" +
                " and [PAGEID]='" + Request.QueryString["OJ1"].ToString().Split(';')[0] + "' and [TYPE2]='DRAGUPLOAD'");
            }
            PreparePage();

        }
        private void RadioListNewOrder()
        {
            if (OBJ1_pageId.Length > 0 & OBJ1_fieldId.Length > 0)
            {
                DynamicUtilsV1.preparePageRadioListNew(rblDynamicList, OBJ1_pageId, _obj.UserName, _obj.Comp, _obj.SelectedLanguage);
                if (postURL.Contains("?"))
                {
                    HiddenpageType.Value = postURL + "GId=" + Request.QueryString["GId"] + "&" + "FaturaStatu=" + pageFaturaStatu + "&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
                }
                else
                {
                    HiddenpageType.Value = postURL + "?GId=" + Request.QueryString["GId"] + "&" + "FaturaStatu=" + pageFaturaStatu + "&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
                }

            }
        }
        private void RadioListOrder()
        {
            if (OBJ1_pageId.Length > 0 & OBJ1_fieldId.Length > 0)
            {
                DynamicUtilsV1.preparePageRadioList(rblDynamicList, OBJ1_pageId, OBJ1_fieldId, _obj.UserName, _obj.Comp, _obj.SelectedLanguage);
                if (postURL.Contains("?"))
                {
                    HiddenpageType.Value = postURL + "GId=" + Request.QueryString["GId"] + "&" + "FaturaStatu=" + pageFaturaStatu + "&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
                }
                else
                {
                    HiddenpageType.Value = postURL + "?GId=" + Request.QueryString["GId"] + "&" + "FaturaStatu=" + pageFaturaStatu + "&" + "InvoiceType=" + invoiceType + "&" + postURLParameter + "=";
                }

            }
        }
        private void PreparePage()
        {
            lblGId.Text = Request.QueryString["GId"];
            //if (Request.QueryString["P1"] != null)
            //{
            //    HiddenP1.Value = Request.QueryString["P1"].ToString();
            //    //this.iframeView.Src = postURL + "?" + postURLParameter + "=" + HiddenP1.Value;


            //    HiddenP1.Value = Request.QueryString["P1"].ToString();
            //    if (Request.QueryString["P2"] != null)
            //        HiddenP2.Value = Request.QueryString["P2"].ToString();
            //    string link = HiddenpageType.Value;
            //    if (link.Contains("XXXXX"))
            //    {
            //        link = link.Replace("XXXXX", HiddenP1.Value);
            //        link = link.Replace("YYYYY", HiddenP2.Value);
            //        link = link.Replace("undefined", "TOTAL");
            //    }
            //    else
            //        link += HiddenP1.Value;

            //    this.iframeView.Attributes.Add("src", link);


            //}





            if (OBJ1_fieldId == "J50010" | OBJ1_fieldId == "J50030")
            {

                string _usrs = "TM,RM,DSM,ATA";
                string[] _users = _usrs.Split(',');

                if (!_users.Any(x => x == _obj.UserName))
                {
                    foreach (DevExpress.Web.MenuItem item in menu1.Items)
                    {
                        if (item.Name.Contains("btnSelectedH0005"))
                            item.Visible = false;
                    }
                }

            }
            if (OBJ1_fieldId == "OJ3110")
            {
                foreach (DevExpress.Web.MenuItem item in menu1.Items)
                {
                    if (item.Name.Contains("btnSelectedH0010"))
                        item.Visible = false;
                }
            }


            if (!string.IsNullOrEmpty(currentP1))
            {





                grid1.FilterExpression = HiddendirectParam.Value.Split(';')[0] + "='" + currentP1 + "'";
                grid1.DataSourceID = "dtgrid1";
                grid1.DataBind();


            }

            if (!string.IsNullOrEmpty(currentP2))
            {



                grid1.FilterExpression = HiddendirectParam.Value.Split(';')[1] + "='" + currentP2 + "'";
                grid1.DataSourceID = "dtgrid1";
                grid1.DataBind();
            }

            if (!string.IsNullOrEmpty(currentP3))
            {



                grid1.FilterExpression = HiddendirectParam.Value.Split(';')[2] + "='" + currentP3 + "'";
                grid1.DataSourceID = "dtgrid1";
                grid1.DataBind();
            }

            if (!string.IsNullOrEmpty(currentP4))
            {



                grid1.FilterExpression = HiddendirectParam.Value.Split(';')[3] + "='" + currentP4 + "'";
                grid1.DataSourceID = "dtgrid1";
                grid1.DataBind();
            }


            Session["InsertComboSelectedValue"] = string.Empty;

            if (OBJ1_pageId != "P161120")
            {
                btnPopup.Visible = false;
            }
        }
        void GetParameters()
        {
            Session["ClonedIdsAndValues"] = string.Empty;

            txtOBJP1.Text = OBJ1_pageId;
            txtOBJP2.Text = OBJ2_pageId;
            txtOBJP3.Text = OBJ3_pageId;
            txtOBJP4.Text = OBJ4_pageId;
            txtOBJP5.Text = OBJ5_pageId;
            txtOBJP6.Text = OBJ6_pageId;
            txtOBJP7.Text = OBJ7_pageId;
            txtOBJP8.Text = OBJ8_pageId;
            txtOBJP9.Text = OBJ9_pageId;
            txtOBJP10.Text = OBJ10_pageId;
            txtOBJP11.Text = OBJ11_pageId;
            txtOBJP12.Text = OBJ12_pageId;
            txtOBJP13.Text = OBJ13_pageId;
            txtOBJP14.Text = OBJ14_pageId;
            txtOBJP15.Text = OBJ15_pageId;

            txtOBJF1.Text = OBJ1_fieldId;
            txtOBJF2.Text = OBJ2_fieldId;
            txtOBJF3.Text = OBJ3_fieldId;
            txtOBJF4.Text = OBJ4_fieldId;
            txtOBJF5.Text = OBJ5_fieldId;
            txtOBJF6.Text = OBJ6_fieldId;
            txtOBJF7.Text = OBJ7_fieldId;
            txtOBJF8.Text = OBJ8_fieldId;
            txtOBJF9.Text = OBJ9_fieldId;
            txtOBJF10.Text = OBJ10_fieldId;
            txtOBJF11.Text = OBJ11_fieldId;
            txtOBJF12.Text = OBJ12_fieldId;
            txtOBJF13.Text = OBJ13_fieldId;
            txtOBJF14.Text = OBJ14_fieldId;
            txtOBJF15.Text = OBJ15_fieldId;


            ViewState["OBJ2_Field"] = OBJ2_fieldId;
            ViewState["OBJF1"] = OBJ1_fieldId;
            ViewState["OBJF2"] = OBJ2_fieldId;
            ViewState["OBJF3"] = OBJ3_fieldId;
            ViewState["OBJF4"] = OBJ4_fieldId;
            ViewState["OBJF5"] = OBJ5_fieldId;
            ViewState["OBJF6"] = OBJ6_fieldId;
            ViewState["OBJF7"] = OBJ7_fieldId;
            ViewState["OBJF8"] = OBJ8_fieldId;
            ViewState["OBJF9"] = OBJ9_fieldId;
            ViewState["OBJF10"] = OBJ10_fieldId;
            ViewState["OBJF11"] = OBJ11_fieldId;
            ViewState["OBJF12"] = OBJ12_fieldId;
            ViewState["OBJF13"] = OBJ13_fieldId;
            ViewState["OBJF14"] = OBJ14_fieldId;
            ViewState["OBJF15"] = OBJ15_fieldId;

            ViewState["OBJP1"] = OBJ1_pageId;
            ViewState["OBJP2"] = OBJ2_pageId;
            ViewState["OBJP3"] = OBJ3_pageId;
            ViewState["OBJP4"] = OBJ4_pageId;
            ViewState["OBJP5"] = OBJ5_pageId;
            ViewState["OBJP6"] = OBJ6_pageId;
            ViewState["OBJP7"] = OBJ7_pageId;
            ViewState["OBJP8"] = OBJ8_pageId;
            ViewState["OBJP9"] = OBJ9_pageId;
            ViewState["OBJP10"] = OBJ10_pageId;
            ViewState["OBJP11"] = OBJ11_pageId;
            ViewState["OBJP12"] = OBJ12_pageId;
            ViewState["OBJP13"] = OBJ13_pageId;
            ViewState["OBJP14"] = OBJ14_pageId;
            ViewState["OBJP15"] = OBJ15_pageId;


        }
        private void CollapseDisplay()
        {
            if (OBJ1_fieldId == "JGMAP" || OBJ2_fieldId == "JGMAP" || OBJ3_fieldId == "JGMAP" || OBJ4_fieldId == "JGMAP" || OBJ5_fieldId == "JGMAP" || OBJ6_fieldId == "JGMAP" || OBJ7_fieldId == "JGMAP" || OBJ8_fieldId == "JGMAP" || OBJ9_fieldId == "JGMAP" || OBJ10_fieldId == "JGMAP" || OBJ11_fieldId == "JGMAP" || OBJ12_fieldId == "JGMAP" || OBJ13_fieldId == "JGMAP" || OBJ14_fieldId == "JGMAP" || OBJ15_fieldId == "JGMAP")
            {
                rp1.Visible = false;
                rp2.Visible = false;
                rp3.Visible = false;
                rp4.Visible = false;
                rp5.Visible = false;
                rp6.Visible = false;
                rp7.Visible = false;
                rp8.Visible = false;
                rp9.Visible = false;
                rp10.Visible = false;
                rp11.Visible = false;
                rp12.Visible = false;
                rp13.Visible = false;
                rp14.Visible = false;
                rp15.Visible = false;
                ASPxSplitter1.Visible = false;
                divGoogleMaps.Visible = true;
            }
            else
            {
                if (OBJ2_fieldId == "OJ9999")
                    rp2.Visible = false;
                if (OBJ3_fieldId == "OJ9999")
                    rp3.Visible = false;
                if (OBJ4_fieldId == "OJ9999")
                    rp4.Visible = false;
                if (OBJ5_fieldId == "OJ9999")
                    rp5.Visible = false;
                if (OBJ6_fieldId == "OJ9999")
                    rp6.Visible = false;
                if (OBJ7_fieldId == "OJ9999")
                    rp7.Visible = false;
                if (OBJ8_fieldId == "OJ9999")
                    rp8.Visible = false;
                if (OBJ9_fieldId == "OJ9999")
                    rp9.Visible = false;
                if (OBJ10_fieldId == "OJ9999")
                    rp10.Visible = false;
                if (OBJ11_fieldId == "OJ9999")
                    rp11.Visible = false;
                if (OBJ12_fieldId == "OJ9999")
                    rp12.Visible = false;
                if (OBJ13_fieldId == "OJ9999")
                    rp13.Visible = false;
                if (OBJ14_fieldId == "OJ9999")
                    rp14.Visible = false;
                if (OBJ15_fieldId == "OJ9999")
                    rp15.Visible = false;
                if (OBJ1_pageId == "P41020" | OBJ1_pageId == "P41010")
                {
                    rp1.Collapsed = true;
                    rp1.Enabled = false;
                }

                if (OBJ1_pageId == "P155100")
                {
                    rp1.Height = Unit.Percentage(67);
                }
            }
        }
        private string GetSourceWebpages()
        {
            string sourceandparams = "";
            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT [SOURCE VIEW],[LINK FIELD] FROM  [dbo].[0C_50057_00_WEBPAGES] AS t1 where t1.[COMPANY]='" + _obj.Comp + "' AND t1.[Web Page ID]='" + OBJ1_pageId + "' and t1.[Web Object ID]='" + OBJ1_fieldId + "'";

                    using (SqlDataAdapter adptr = new SqlDataAdapter(cmd))
                    {
                        using (DataSet ds = new DataSet())
                        {
                            adptr.Fill(ds);

                            DataRow[] WebPagesLine = ds.Tables[0].Select();

                            foreach (DataRow row in WebPagesLine)
                            {
                                sourceandparams = row["SOURCE VIEW"].ToString() + "/" + row["LINK FIELD"];
                            }
                        }
                    }
                }
            }
            return sourceandparams;
        }
        private void FillDynamicObjects()
        {
            if (OBJ1_fieldId == "JGMAP")
            {
                ltrlGoogleMapsJavasScript.Text = "<script type=\"text/javascript\" src=\"GoogleMaps/speed_test.js\"></script><script type=\"text/javascript\" src=\"GoogleMaps/markerclusterer.js\"></script><script>google.maps.event.addDomListener(window, 'load', speedTest.init);</script>";
                string sourceandparams = GetSourceWebpages();
                string sourceSp = sourceandparams.Split('/')[0];
                string sourcePrm = sourceandparams.Split('/')[1];
                txtMapsJSONData.Text = "var data = {\"count\": 400,\"photos\": [";
                using (SqlConnection conn = new SqlConnection(strConnString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {

                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = sourceSp;
                        string[] sourcePrms = sourcePrm.Split(';');
                        cmd.Parameters.AddWithValue("@COMP", _obj.Comp);
                        cmd.Parameters.AddWithValue("@USER", _obj.Comp);
                        cmd.Parameters.AddWithValue("@YTKG", "");
                        cmd.Parameters.AddWithValue("@YTK1", "");
                        cmd.Parameters.AddWithValue("@YTK2", "");
                        cmd.Parameters.AddWithValue("@YTK3", "");
                        cmd.Parameters.AddWithValue("@YTK4", "");
                        cmd.Parameters.AddWithValue("@YTK5", "");
                        cmd.Parameters.AddWithValue("@PRM1", "");
                        cmd.Parameters.AddWithValue("@PRM2", "");
                        cmd.Parameters.AddWithValue("@PRM3", "");


                        using (SqlDataAdapter adptr = new SqlDataAdapter(cmd))
                        {
                            using (DataSet ds = new DataSet())
                            {
                                adptr.Fill(ds);

                                DataRow[] ContainerPorts = ds.Tables[0].Select("R0012='DEPOT'"); 
                                int counter = 0;
                                foreach (DataRow row in ContainerPorts)
                                {
                                    txtMapsJSONData.Text += "{\"photo_id\": \""+row["R0011"]+"\",\"photo_title\": \"" + row["R0011"].ToString() + "\",\"photo_url\": \"#\",\"photo_file_url\": \"#\",\"longitude\": " + row["R0015"].ToString() + ",\"latitude\": " + row["R0014"].ToString() + ",\"width\": 500,\"height\": 375,\"upload_date\": \"\",\"owner_id\":\""+row["R0011"].ToString()+"\" ,\"owner_name\": \"\",\"owner_url\": \"\",\"Count\": \""+row["R0013"].ToString()+"\"},";
                                    counter++;
                                }
                            }
                        }
                    }
                }
                txtMapsJSONData.Text = txtMapsJSONData.Text.Substring(0, txtMapsJSONData.Text.Length - 1);
                txtMapsJSONData.Text += "]}";
            }
            else
            {
                if (OBJ1_pageId.Length > 0 & OBJ1_fieldId.Length > 0)
                {
                    bool _IsNewTab = DynamicUtilsV1.IsOpenNewTab(OBJ1_pageId, OBJ1_fieldId, _obj.Comp);
                    if (_IsNewTab) txtOpenNewWindow.Text = "1";
                    bool _IsWithoutLeftSide = false;
                    if (_IsNewTab & Request.QueryString["ONT"] != null)
                        _IsWithoutLeftSide = true;

                    if (!_IsNewTab) _IsWithoutLeftSide = true;


                    string OIds = OBJ1_pageId, FIds = OBJ1_fieldId;
                    List<object> GrdObjs = new List<object>();
                    List<object> MenuObjs = new List<object>();
                    List<object> InsPObjs = new List<object>();
                    List<object> CloneObjs = new List<object>();
                    List<object> EdtPObjs = new List<object>();
                    List<object> DelPObjs = new List<object>();
                    List<object> PageParObjs = new List<object>();
                    GrdObjs.Add(grid1);
                    MenuObjs.Add(menu1);
                    InsPObjs.Add(txtinsertparams1);
                    CloneObjs.Add(txtCloneParameters1);
                    EdtPObjs.Add(txteditparams1);
                    DelPObjs.Add(txtdeleteparams1);
                    PageParObjs.Add(txtlink1);
                    PageParObjs.Add(txtlinkField1);
                    PageParObjs.Add(txtCatchParameters1);
                    PageParObjs.Add(txtCollapseHeader1);
                    PageParObjs.Add(txtCollapseStatu1);
                    PageParObjs.Add(HiddendirectParam);
                    PageParObjs.Add(txtBos);
                    PageParObjs.Add(txtRef1);

                    if (OBJ2_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                    {

                        GrdObjs.Add(grid2);
                        MenuObjs.Add(menu2);
                        InsPObjs.Add(txtinsertparams2);
                        CloneObjs.Add(txtCloneParameters2);
                        EdtPObjs.Add(txteditparams2);
                        DelPObjs.Add(txtdeleteparams2);
                        OIds += "," + OBJ2_pageId;
                        FIds += "," + OBJ2_fieldId;

                        PageParObjs.Add(txtlink2);
                        PageParObjs.Add(txtlinkField2);
                        PageParObjs.Add(txtCatchParameters2);
                        PageParObjs.Add(txtCollapseHeader2);
                        PageParObjs.Add(txtCollapseStatu2);
                        PageParObjs.Add(hndBos);
                        PageParObjs.Add(txtMasterObject2);
                        PageParObjs.Add(txtRef2);

                    }
                    else
                        grid2.Visible = false;
                    if (OBJ3_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                    {
                        GrdObjs.Add(grid3);
                        MenuObjs.Add(menu3);
                        InsPObjs.Add(txtinsertparams3);
                        CloneObjs.Add(txtCloneParameters3);
                        EdtPObjs.Add(txteditparams3);
                        DelPObjs.Add(txtdeleteparams3);
                        OIds += "," + OBJ3_pageId;
                        FIds += "," + OBJ3_fieldId;

                        PageParObjs.Add(txtlink3);
                        PageParObjs.Add(txtlinkField3);
                        PageParObjs.Add(txtCatchParameters3);
                        PageParObjs.Add(txtCollapseHeader3);
                        PageParObjs.Add(txtCollapseStatu3);
                        PageParObjs.Add(hndBos);
                        PageParObjs.Add(txtMasterObject3);
                        PageParObjs.Add(txtRef3);
                    }
                    else
                        grid3.Visible = false;
                    if (OBJ4_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                    {
                        GrdObjs.Add(grid4);
                        MenuObjs.Add(menu4);
                        InsPObjs.Add(txtinsertparams4);
                        CloneObjs.Add(txtCloneParameters4);
                        EdtPObjs.Add(txteditparams4);
                        DelPObjs.Add(txtdeleteparams4);
                        OIds += "," + OBJ4_pageId;
                        FIds += "," + OBJ4_fieldId;

                        PageParObjs.Add(txtlink4);
                        PageParObjs.Add(txtlinkField4);
                        PageParObjs.Add(txtCatchParameters4);
                        PageParObjs.Add(txtCollapseHeader4);
                        PageParObjs.Add(txtCollapseStatu4);
                        PageParObjs.Add(hndBos);
                        PageParObjs.Add(txtMasterObject4);
                        PageParObjs.Add(txtRef4);
                    }
                    else
                        grid4.Visible = false;
                    if (OBJ5_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                    {
                        GrdObjs.Add(grid5);
                        MenuObjs.Add(menu5);
                        InsPObjs.Add(txtinsertparams5);
                        CloneObjs.Add(txtCloneParameters5);
                        EdtPObjs.Add(txteditparams5);
                        DelPObjs.Add(txtdeleteparams5);
                        OIds += "," + OBJ5_pageId;
                        FIds += "," + OBJ5_fieldId;

                        PageParObjs.Add(txtlink5);
                        PageParObjs.Add(txtlinkField5);
                        PageParObjs.Add(txtCatchParameters5);
                        PageParObjs.Add(txtCollapseHeader5);
                        PageParObjs.Add(txtCollapseStatu5);
                        PageParObjs.Add(hndBos);
                        PageParObjs.Add(txtMasterObject5);
                        PageParObjs.Add(txtRef5);
                    }
                    else
                        grid5.Visible = false;
                    if (OBJ6_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                    {
                        GrdObjs.Add(grid6);
                        MenuObjs.Add(menu6);
                        InsPObjs.Add(txtinsertparams6);
                        CloneObjs.Add(txtCloneParameters6);
                        EdtPObjs.Add(txteditparams6);
                        DelPObjs.Add(txtdeleteparams6);
                        OIds += "," + OBJ6_pageId;
                        FIds += "," + OBJ6_fieldId;

                        PageParObjs.Add(txtlink6);
                        PageParObjs.Add(txtlinkField6);
                        PageParObjs.Add(txtCatchParameters6);
                        PageParObjs.Add(txtCollapseHeader6);
                        PageParObjs.Add(txtCollapseStatu6);
                        PageParObjs.Add(hndBos);
                        PageParObjs.Add(txtMasterObject6);
                        PageParObjs.Add(txtRef6);
                    }
                    else
                        grid6.Visible = false;
                    if (OBJ7_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                    {
                        GrdObjs.Add(grid7);
                        MenuObjs.Add(menu7);
                        InsPObjs.Add(txtinsertparams7);
                        CloneObjs.Add(txtCloneParameters7);
                        EdtPObjs.Add(txteditparams7);
                        DelPObjs.Add(txtdeleteparams7);
                        OIds += "," + OBJ7_pageId;
                        FIds += "," + OBJ7_fieldId;

                        PageParObjs.Add(txtlink7);
                        PageParObjs.Add(txtlinkField7);
                        PageParObjs.Add(txtCatchParameters7);
                        PageParObjs.Add(txtCollapseHeader7);
                        PageParObjs.Add(txtCollapseStatu7);
                        PageParObjs.Add(hndBos);
                        PageParObjs.Add(txtMasterObject7);
                        PageParObjs.Add(txtRef7);
                    }
                    else
                        grid7.Visible = false;
                    if (OBJ8_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                    {
                        GrdObjs.Add(grid8);
                        MenuObjs.Add(menu8);
                        InsPObjs.Add(txtinsertparams8);
                        CloneObjs.Add(txtCloneParameters8);
                        EdtPObjs.Add(txteditparams8);
                        DelPObjs.Add(txtdeleteparams8);
                        OIds += "," + OBJ8_pageId;
                        FIds += "," + OBJ8_fieldId;

                        PageParObjs.Add(txtlink8);
                        PageParObjs.Add(txtlinkField8);
                        PageParObjs.Add(txtCatchParameters8);
                        PageParObjs.Add(txtCollapseHeader8);
                        PageParObjs.Add(txtCollapseStatu8);
                        PageParObjs.Add(hndBos);
                        PageParObjs.Add(txtMasterObject8);
                        PageParObjs.Add(txtRef8);
                    }
                    else
                        grid8.Visible = false;
                    if (OBJ9_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                    {
                        GrdObjs.Add(grid9);
                        MenuObjs.Add(menu9);
                        InsPObjs.Add(txtinsertparams9);
                        CloneObjs.Add(txtCloneParameters9);
                        EdtPObjs.Add(txteditparams9);
                        DelPObjs.Add(txtdeleteparams9);
                        OIds += "," + OBJ9_pageId;
                        FIds += "," + OBJ9_fieldId;

                        PageParObjs.Add(txtlink9);
                        PageParObjs.Add(txtlinkField9);
                        PageParObjs.Add(txtCatchParameters9);
                        PageParObjs.Add(txtCollapseHeader9);
                        PageParObjs.Add(txtCollapseStatu9);
                        PageParObjs.Add(hndBos);
                        PageParObjs.Add(txtMasterObject9);
                        PageParObjs.Add(txtRef9);
                    }
                    else
                        grid9.Visible = false;
                    if (OBJ10_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                    {
                        GrdObjs.Add(grid10);
                        MenuObjs.Add(menu10);
                        InsPObjs.Add(txtinsertparams10);
                        CloneObjs.Add(txtCloneParameters10);
                        EdtPObjs.Add(txteditparams10);
                        DelPObjs.Add(txtdeleteparams10);
                        OIds += "," + OBJ10_pageId;
                        FIds += "," + OBJ10_fieldId;

                        PageParObjs.Add(txtlink10);
                        PageParObjs.Add(txtlinkField10);
                        PageParObjs.Add(txtCatchParameters10);
                        PageParObjs.Add(txtCollapseHeader10);
                        PageParObjs.Add(txtCollapseStatu10);
                        PageParObjs.Add(hndBos);
                        PageParObjs.Add(txtMasterObject10);
                        PageParObjs.Add(txtRef10);
                    }
                    else
                        grid10.Visible = false;
                    if (OBJ11_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                    {
                        GrdObjs.Add(grid11);
                        MenuObjs.Add(menu11);
                        InsPObjs.Add(txtinsertparams11);
                        CloneObjs.Add(txtCloneParameters11);
                        EdtPObjs.Add(txteditparams11);
                        DelPObjs.Add(txtdeleteparams11);
                        OIds += "," + OBJ11_pageId;
                        FIds += "," + OBJ11_fieldId;

                        PageParObjs.Add(txtlink11);
                        PageParObjs.Add(txtlinkField11);
                        PageParObjs.Add(txtCatchParameters11);
                        PageParObjs.Add(txtCollapseHeader11);
                        PageParObjs.Add(txtCollapseStatu11);
                        PageParObjs.Add(hndBos);
                        PageParObjs.Add(txtMasterObject11);
                        PageParObjs.Add(txtRef11);
                    }
                    else
                        grid11.Visible = false;
                    if (OBJ12_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                    {
                        GrdObjs.Add(grid12);
                        MenuObjs.Add(menu12);
                        InsPObjs.Add(txtinsertparams12);
                        CloneObjs.Add(txtCloneParameters12);
                        EdtPObjs.Add(txteditparams12);
                        DelPObjs.Add(txtdeleteparams12);
                        OIds += "," + OBJ12_pageId;
                        FIds += "," + OBJ12_fieldId;

                        PageParObjs.Add(txtlink12);
                        PageParObjs.Add(txtlinkField12);
                        PageParObjs.Add(txtCatchParameters12);
                        PageParObjs.Add(txtCollapseHeader12);
                        PageParObjs.Add(txtCollapseStatu12);
                        PageParObjs.Add(hndBos);
                        PageParObjs.Add(txtMasterObject12);
                        PageParObjs.Add(txtRef12);
                    }
                    else
                        grid12.Visible = false;
                    if (OBJ13_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                    {
                        GrdObjs.Add(grid13);
                        MenuObjs.Add(menu13);
                        InsPObjs.Add(txtinsertparams13);
                        CloneObjs.Add(txtCloneParameters13);
                        EdtPObjs.Add(txteditparams13);
                        DelPObjs.Add(txtdeleteparams13);
                        OIds += "," + OBJ13_pageId;
                        FIds += "," + OBJ13_fieldId;

                        PageParObjs.Add(txtlink13);
                        PageParObjs.Add(txtlinkField13);
                        PageParObjs.Add(txtCatchParameters13);
                        PageParObjs.Add(txtCollapseHeader13);
                        PageParObjs.Add(txtCollapseStatu13);
                        PageParObjs.Add(hndBos);
                        PageParObjs.Add(txtMasterObject13);
                        PageParObjs.Add(txtRef13);
                    }
                    else
                        grid13.Visible = false;
                    if (OBJ14_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                    {
                        GrdObjs.Add(grid14);
                        MenuObjs.Add(menu14);
                        InsPObjs.Add(txtinsertparams14);
                        CloneObjs.Add(txtCloneParameters14);
                        EdtPObjs.Add(txteditparams14);
                        DelPObjs.Add(txtdeleteparams14);
                        OIds += "," + OBJ14_pageId;
                        FIds += "," + OBJ14_fieldId;

                        PageParObjs.Add(txtlink14);
                        PageParObjs.Add(txtlinkField14);
                        PageParObjs.Add(txtCatchParameters14);
                        PageParObjs.Add(txtCollapseHeader14);
                        PageParObjs.Add(txtCollapseStatu14);
                        PageParObjs.Add(hndBos);
                        PageParObjs.Add(txtMasterObject14);
                        PageParObjs.Add(txtRef14);
                    }
                    else
                        grid14.Visible = false;
                    if (OBJ15_fieldId != "OJ9999" & _IsWithoutLeftSide == true)
                    {
                        GrdObjs.Add(grid15);
                        MenuObjs.Add(menu15);
                        InsPObjs.Add(txtinsertparams15);
                        CloneObjs.Add(txtCloneParameters15);
                        EdtPObjs.Add(txteditparams15);
                        DelPObjs.Add(txtdeleteparams15);
                        OIds += "," + OBJ15_pageId;
                        FIds += "," + OBJ15_fieldId;

                        PageParObjs.Add(txtlink15);
                        PageParObjs.Add(txtlinkField15);
                        PageParObjs.Add(txtCatchParameters15);
                        PageParObjs.Add(txtCollapseHeader15);
                        PageParObjs.Add(txtCollapseStatu15);
                        PageParObjs.Add(hndBos);
                        PageParObjs.Add(txtMasterObject15);
                        PageParObjs.Add(txtRef15);
                    }
                    else
                        grid15.Visible = false;

                    GenerateRnd();
                    DataColumn[] pricols = new DataColumn[1];
                    //dtgrid1.Table = DynamicUtilsV1.FillDynamicObjects(GrdObjs, MenuObjs, OIds, FIds, HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString()
                    //    , HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), InsPObjs, EdtPObjs, DelPObjs, PageParObjs, "OPL"+ViewState["RndVal"].ToString()+"50Grid");
                    DynamicUtilsV1.FillDynamicObjects(GrdObjs, MenuObjs, OIds, FIds, HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString()
                        , HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), InsPObjs, CloneObjs, EdtPObjs, DelPObjs, PageParObjs
                        , "OPL" + ViewState["RndVal"].ToString() + "50Grid", _obj.UserName, _obj.Comp, _obj.SelectedLanguage);
                    pricols[0] = dtgrid1.Table.Columns["ID"];
                    dtgrid1.Table.PrimaryKey = pricols;
                    grid1.DataSourceID = "dtgrid1";
                    grid1.DataBind();

                    if (!_IsNewTab)
                    {
                        int _getSplitterWidth = Convert.ToInt32(DynamicUtilsV1.getSplitterWidth(OBJ1_pageId, OBJ1_fieldId, _obj.Comp));
                        if (_getSplitterWidth != -1 && _getSplitterWidth != 100)
                        {
                            ASPxSplitter1.GetPaneByName("LeftContainer").Size = Unit.Percentage(_getSplitterWidth);
                        }
                        else
                        {
                            if (_getSplitterWidth == -1)
                            {
                                ASPxSplitter1.GetPaneByName("LeftContainer").Collapsed = true;
                                ASPxSplitter1.GetPaneByName("MiddleContainer").Collapsed = false;
                            }
                            else
                            {
                                ASPxSplitter1.GetPaneByName("LeftContainer").Collapsed = false;
                                ASPxSplitter1.GetPaneByName("MiddleContainer").Collapsed = true;
                            }
                        }
                    }

                    if (_IsNewTab & _IsWithoutLeftSide == true)
                    {
                        ASPxSplitter1.GetPaneByName("LeftContainer").Collapsed = true;
                        ASPxSplitter1.GetPaneByName("MiddleContainer").Collapsed = false;


                        if (!ClientScript.IsStartupScriptRegistered("LoadMainDataP1"))
                        {
                            ClientScript.RegisterStartupScript(GetType(), "LoadMainDataP1", @"grid1.PerformCallback('LoadMainDataP1');", true);
                        }


                    }
                    else if (_IsNewTab & _IsWithoutLeftSide == false)
                    {

                        ASPxSplitter1.GetPaneByName("LeftContainer").Collapsed = false;
                        ASPxSplitter1.GetPaneByName("MiddleContainer").Collapsed = true;
                        ASPxSplitter1.GetPaneByName("MiddleContainer").Visible = false;
                        if (!ClientScript.IsStartupScriptRegistered("JSLoadMainData"))
                        {
                            ClientScript.RegisterStartupScript(GetType(), "JSLoadMainData", @"grid1.PerformCallback('LoadMainData');", true);
                        }
                    }


                    if (!_IsNewTab)
                    {
                        if (Request.QueryString["P1"] != null)
                        {
                            if (!ClientScript.IsStartupScriptRegistered("LoadMainDataP1"))
                            {
                                ClientScript.RegisterStartupScript(GetType(), "LoadMainDataP1", @"grid1.PerformCallback('LoadMainDataP1');", true);
                            }
                        }
                        else
                        {
                            if (!ClientScript.IsStartupScriptRegistered("JSLoadMainData"))
                            {
                                ClientScript.RegisterStartupScript(GetType(), "JSLoadMainData", @"grid1.PerformCallback('LoadMainData');", true);
                            }

                        }

                        DataRow[] InsCmbRows = ((DataTable)Session["OPL" + ViewState["RndVal"].ToString() + "50Grid1Rows"]).Select("[DX Type2]='INSERTCOMBO'");
                        if (InsCmbRows.Count() > 0)
                        {
                            tdmainmenuleft.Attributes.Add("style", "display:normal");
                            var connstr = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                            var sds = new SqlDataSource();
                            string dxfilter = InsCmbRows[0]["DX Filter"].ToString();
                            string addparams = string.Empty, _pi = string.Empty;
                            bool ilk = true;
                            foreach (string str in dxfilter.Split(','))
                            {
                                if (str == "COMP")
                                    _pi = _obj.Comp;
                                else if (str == "USER")
                                    _pi = _obj.UserName;
                                else
                                    _pi = str.Replace("$", string.Empty);
                                if (ilk)
                                {
                                    addparams = " '" + _pi + "'";
                                    ilk = false;
                                }
                                else
                                    addparams += ",'" + _pi + "'";
                            }
                            sds.ConnectionString = connstr;
                            sds.SelectCommand = InsCmbRows[0]["DX Source"].ToString() + addparams;
                            sds.SelectParameters.Clear();
                            cmbInsertCombo.ValueField = InsCmbRows[0]["DX Combo Value"].ToString();
                            cmbInsertCombo.TextField = InsCmbRows[0]["DX Combo Text"].ToString();
                            cmbInsertCombo.DataSource = sds;
                            cmbInsertCombo.DataBind();

                            cmbInsertCombo.ClientSideEvents.SelectedIndexChanged = "function(s,e){grid2.PerformCallback('InsertCombo|'+'" + InsCmbRows[0]["DX Combo Order"].ToString() + "|'+s.GetValue())}";

                            txtInsertComboSource.Text = InsCmbRows[0]["DX Source"].ToString();
                            txtInsertComboParams.Text = addparams;
                            //TODO: Using kullanılacak.
                        }

                        grid2_CustomCallback(grid2, new ASPxGridViewCustomCallbackEventArgs("LoadEmpty"));
                    }


                    if (((DataTable)Session["OPL" + ViewState["RndVal"].ToString() + "50Grid1Rows"]).Select("[iframe Link]<>''").Count() == 0)
                    {
                        rp1.Visible = false;
                        txtIsCard.Text = "0";
                    }

                    DataRow[] RowHeaderPane = ((DataTable)Session["OPL" + ViewState["RndVal"].ToString() + "50Grid1Rows"]).Select("[DX Type1]='PANEL'");
                    if (RowHeaderPane.Count() > 0)
                    {
                        txtHeaderSource.Text = "";
                        txtHeaderParams.Text = "";
                        for (int i = 0; i < RowHeaderPane.Count(); i++)
                        {
                            ASPxSplitter1.Panes["MiddleContainer"].Panes["MiddleHeader"].Visible = true;
                            txtHeaderSource.Text += RowHeaderPane[i]["DX Source"].ToString() + "§";
                            txtHeaderParams.Text += RowHeaderPane[i]["DX Filter"].ToString() + "§";
                            int _paneheight = Convert.ToInt32(RowHeaderPane[0]["DX Width"]);
                            ASPxSplitter1.Panes["MiddleContainer"].Panes["MiddleHeader"].Size = Unit.Percentage(_paneheight);
                        }
                    }
                    else
                        ASPxSplitter1.Panes["MiddleContainer"].Panes["MiddleHeader"].Visible = false;

                    DataRow[] RowHeaderPopup = ((DataTable)Session["OPL" + ViewState["RndVal"].ToString() + "50Grid1Rows"]).Select("[DX Type1]='POPUP'");
                    if (RowHeaderPopup.Count() > 0)
                    {
                        txtPopupSource.Text = RowHeaderPopup[0]["DX Field ID"].ToString();
                    }

                    DataRow[] IsMainData = ((DataTable)Session["OPL" + ViewState["RndVal"].ToString() + "50Grid1Rows"]).Select("[LINK]<>'FILTER'");
                    if (IsMainData.Length > 0)
                    {
                        if (OBJ2_fieldId != "OJ9999")
                            rp2.Collapsed = false;
                    }


                }
                else
                {
                    grid1.Visible = false;
                }
            }


        }
        void GenerateRnd()
        {
            Random rnd = new Random();
            ViewState["RndVal"] = rnd.Next(10000, 100000).ToString();
        }
        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        public static Guid GenerateGuid()
        {
            Guid _guid = Guid.NewGuid();
            return _guid;
        }
        protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            Session["InsertedVal"] = string.Empty;
            Session["WebMessage"] = string.Empty;
            string insertparams = string.Empty;
            string[] _temp = { }, _array = { };
            if (!string.IsNullOrEmpty(Session["ClonedIdsAndValues"].ToString()))
            {
                switch (gridI.ID)
                {
                    case "grid1":
                        insertparams = txtCloneParameters1.Text;
                        _temp = txtCloneParameters1.Text.Split(',');
                        _array = new string[txtCloneParameters1.Text.Split(',').Length - 1];
                        break;
                    case "grid2":
                        insertparams = txtCloneParameters2.Text;
                        _temp = txtCloneParameters2.Text.Split(',');
                        _array = new string[txtCloneParameters2.Text.Split(',').Length - 1];
                        break;
                    case "grid3":
                        insertparams = txtCloneParameters3.Text;
                        _temp = txtCloneParameters3.Text.Split(',');
                        _array = new string[txtCloneParameters3.Text.Split(',').Length - 1];
                        break;
                    case "grid4":
                        insertparams = txtCloneParameters4.Text;
                        _temp = txtCloneParameters4.Text.Split(',');
                        _array = new string[txtCloneParameters4.Text.Split(',').Length - 1];
                        break;
                    case "grid5":
                        insertparams = txtCloneParameters5.Text;
                        _temp = txtCloneParameters5.Text.Split(',');
                        _array = new string[txtCloneParameters5.Text.Split(',').Length - 1];
                        break;
                    case "grid6":
                        insertparams = txtCloneParameters6.Text;
                        _temp = txtCloneParameters6.Text.Split(',');
                        _array = new string[txtCloneParameters6.Text.Split(',').Length - 1];
                        break;
                    case "grid7":
                        insertparams = txtCloneParameters7.Text;
                        _temp = txtCloneParameters7.Text.Split(',');
                        _array = new string[txtCloneParameters7.Text.Split(',').Length - 1];
                        break;
                    case "grid8":
                        insertparams = txtCloneParameters8.Text;
                        _temp = txtCloneParameters8.Text.Split(',');
                        _array = new string[txtCloneParameters8.Text.Split(',').Length - 1];
                        break;
                    case "grid9":
                        insertparams = txtCloneParameters9.Text;
                        _temp = txtCloneParameters9.Text.Split(',');
                        _array = new string[txtCloneParameters9.Text.Split(',').Length - 1];
                        break;
                    case "grid10":
                        insertparams = txtCloneParameters10.Text;
                        _temp = txtCloneParameters10.Text.Split(',');
                        _array = new string[txtCloneParameters10.Text.Split(',').Length - 1];
                        break;
                    case "grid11":
                        insertparams = txtCloneParameters11.Text;
                        _temp = txtCloneParameters11.Text.Split(',');
                        _array = new string[txtCloneParameters11.Text.Split(',').Length - 1];
                        break;
                    case "grid12":
                        insertparams = txtCloneParameters12.Text;
                        _temp = txtCloneParameters12.Text.Split(',');
                        _array = new string[txtCloneParameters12.Text.Split(',').Length - 1];
                        break;
                    case "grid13":
                        insertparams = txtCloneParameters13.Text;
                        _temp = txtCloneParameters13.Text.Split(',');
                        _array = new string[txtCloneParameters13.Text.Split(',').Length - 1];
                        break;
                    case "grid14":
                        insertparams = txtCloneParameters14.Text;
                        _temp = txtCloneParameters14.Text.Split(',');
                        _array = new string[txtCloneParameters14.Text.Split(',').Length - 1];
                        break;
                    case "grid15":
                        insertparams = txtCloneParameters15.Text;
                        _temp = txtCloneParameters15.Text.Split(',');
                        _array = new string[txtCloneParameters15.Text.Split(',').Length - 1];
                        break;
                }
            }
            else
            {
                switch (gridI.ID)
                {
                    case "grid1":
                        insertparams = txtinsertparams1.Text;
                        _temp = txtinsertparams1.Text.Split(',');
                        _array = new string[txtinsertparams1.Text.Split(',').Length - 1];
                        break;
                    case "grid2":
                        insertparams = txtinsertparams2.Text;
                        _temp = txtinsertparams2.Text.Split(',');
                        _array = new string[txtinsertparams2.Text.Split(',').Length - 1];
                        break;
                    case "grid3":
                        insertparams = txtinsertparams3.Text;
                        _temp = txtinsertparams3.Text.Split(',');
                        _array = new string[txtinsertparams3.Text.Split(',').Length - 1];
                        break;
                    case "grid4":
                        insertparams = txtinsertparams4.Text;
                        _temp = txtinsertparams4.Text.Split(',');
                        _array = new string[txtinsertparams4.Text.Split(',').Length - 1];
                        break;
                    case "grid5":
                        insertparams = txtinsertparams5.Text;
                        _temp = txtinsertparams5.Text.Split(',');
                        _array = new string[txtinsertparams5.Text.Split(',').Length - 1];
                        break;
                    case "grid6":
                        insertparams = txtinsertparams6.Text;
                        _temp = txtinsertparams6.Text.Split(',');
                        _array = new string[txtinsertparams6.Text.Split(',').Length - 1];
                        break;
                    case "grid7":
                        insertparams = txtinsertparams7.Text;
                        _temp = txtinsertparams7.Text.Split(',');
                        _array = new string[txtinsertparams7.Text.Split(',').Length - 1];
                        break;
                    case "grid8":
                        insertparams = txtinsertparams8.Text;
                        _temp = txtinsertparams8.Text.Split(',');
                        _array = new string[txtinsertparams8.Text.Split(',').Length - 1];
                        break;
                    case "grid9":
                        insertparams = txtinsertparams9.Text;
                        _temp = txtinsertparams9.Text.Split(',');
                        _array = new string[txtinsertparams9.Text.Split(',').Length - 1];
                        break;
                    case "grid10":
                        insertparams = txtinsertparams10.Text;
                        _temp = txtinsertparams10.Text.Split(',');
                        _array = new string[txtinsertparams10.Text.Split(',').Length - 1];
                        break;
                    case "grid11":
                        insertparams = txtinsertparams11.Text;
                        _temp = txtinsertparams11.Text.Split(',');
                        _array = new string[txtinsertparams11.Text.Split(',').Length - 1];
                        break;
                    case "grid12":
                        insertparams = txtinsertparams12.Text;
                        _temp = txtinsertparams12.Text.Split(',');
                        _array = new string[txtinsertparams12.Text.Split(',').Length - 1];
                        break;
                    case "grid13":
                        insertparams = txtinsertparams13.Text;
                        _temp = txtinsertparams13.Text.Split(',');
                        _array = new string[txtinsertparams13.Text.Split(',').Length - 1];
                        break;
                    case "grid14":
                        insertparams = txtinsertparams14.Text;
                        _temp = txtinsertparams14.Text.Split(',');
                        _array = new string[txtinsertparams14.Text.Split(',').Length - 1];
                        break;
                    case "grid15":
                        insertparams = txtinsertparams15.Text;
                        _temp = txtinsertparams15.Text.Split(',');
                        _array = new string[txtinsertparams15.Text.Split(',').Length - 1];
                        break;
                }
            }

            if (!string.IsNullOrEmpty(insertparams))
            {
                bool _dvar = false;
                for (int i = 1; i < _temp.Length; i++)
                {
                    if (_temp[i].Contains("_d") | _temp[i].Contains("_t"))
                        _dvar = true;
                    else
                        _dvar = false;
                    string _tempi = _temp[i].Replace("_d", string.Empty).Replace("_t", string.Empty);
                    if (_tempi == "COMP")
                    {
                        _array[i - 1] = _obj.Comp;
                    }
                    else if (_tempi == "USER")
                    {
                        _array[i - 1] = _obj.UserName;
                    }
                    else if (_tempi == "P1")
                    {
                        _array[i - 1] = this.HiddenP1.Value;
                    }
                    else if (_tempi == "P2")
                    {
                        _array[i - 1] = this.HiddenP2.Value;
                    }
                    else if (_tempi == "P3")
                    {
                        _array[i - 1] = this.HiddenP3.Value;
                    }
                    else if (_tempi == "P4")
                    {
                        _array[i - 1] = this.HiddenP4.Value;
                    }
                    else if (_tempi == "P5")
                    {
                        _array[i - 1] = this.HiddenP5.Value;
                    }
                    else if (string.IsNullOrEmpty(_tempi))
                    {
                        _array[i - 1] = "";
                    }
                    else if (_tempi.Substring(0, 1) == "$")
                    {
                        try { _array[i - 1] = _tempi.Substring(1, _tempi.Length - 1); } catch { _array[i - 1] = ""; }
                    }
                    else if (_tempi.Substring(0, 1) == "#")
                    {
                        try
                        {
                            string[] transparams = txtCatchParamsAndValues.Text.Split('|');
                            string _val1 = Array.Find(transparams, element => element.StartsWith(_tempi.Replace("#", string.Empty), StringComparison.Ordinal));

                            _array[i - 1] = _val1.Split(';')[1];

                        }
                        catch { _array[i - 1] = ""; }
                    }
                    else
                    {
                        if (e.NewValues[_tempi] != null)
                        {
                            if (e.NewValues[_tempi].ToString().Contains('|'))
                                _array[i - 1] = e.NewValues[_tempi].ToString().Split('|')[e.NewValues[_tempi].ToString().Split('|').Length - 1];
                            else
                            {
                                _array[i - 1] = e.NewValues[_tempi].ToString();
                            }
                        }
                        else
                            _array[i - 1] = "";

                    }

                    if (_dvar & !string.IsNullOrEmpty(_array[i - 1])) _array[i - 1] = _temp[i].Substring(0, 2) + _array[i - 1];
                }


                services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                if (!string.IsNullOrEmpty(Session["InsertedVal"].ToString()))
                    gridI.JSProperties["cpQval"] = Session["InsertedVal"].ToString();

            }

            copiedValues = null;

            gridI.JSProperties["cpIsUpdated"] = true;
            gridI.JSProperties["cpMsg"] = Session["WebMessage"].ToString();
            Session["ClonedIdsAndValues"] = string.Empty;




        }
        protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            Session["InsertedVal"] = string.Empty;
            Session["WebMessage"] = string.Empty;
            ASPxGridView gridI = sender as ASPxGridView;
            string editparams = string.Empty;
            string[] _temp = { }, _array = { };
            switch (gridI.ID)
            {
                case "grid1":
                    editparams = txteditparams1.Text;
                    _temp = txteditparams1.Text.Split(',');
                    _array = new string[txteditparams1.Text.Split(',').Length - 1];
                    break;
                case "grid2":
                    editparams = txteditparams2.Text;
                    _temp = txteditparams2.Text.Split(',');
                    _array = new string[txteditparams2.Text.Split(',').Length - 1];
                    break;
                case "grid3":
                    editparams = txteditparams3.Text;
                    _temp = txteditparams3.Text.Split(',');
                    _array = new string[txteditparams3.Text.Split(',').Length - 1];
                    break;
                case "grid4":
                    editparams = txteditparams4.Text;
                    _temp = txteditparams4.Text.Split(',');
                    _array = new string[txteditparams4.Text.Split(',').Length - 1];
                    break;
                case "grid5":
                    editparams = txteditparams5.Text;
                    _temp = txteditparams5.Text.Split(',');
                    _array = new string[txteditparams5.Text.Split(',').Length - 1];
                    break;
                case "grid6":
                    editparams = txteditparams6.Text;
                    _temp = txteditparams6.Text.Split(',');
                    _array = new string[txteditparams6.Text.Split(',').Length - 1];
                    break;
                case "grid7":
                    editparams = txteditparams7.Text;
                    _temp = txteditparams7.Text.Split(',');
                    _array = new string[txteditparams7.Text.Split(',').Length - 1];
                    break;
                case "grid8":
                    editparams = txteditparams8.Text;
                    _temp = txteditparams8.Text.Split(',');
                    _array = new string[txteditparams8.Text.Split(',').Length - 1];
                    break;
                case "grid9":
                    editparams = txteditparams9.Text;
                    _temp = txteditparams9.Text.Split(',');
                    _array = new string[txteditparams9.Text.Split(',').Length - 1];
                    break;
                case "grid10":
                    editparams = txteditparams10.Text;
                    _temp = txteditparams10.Text.Split(',');
                    _array = new string[txteditparams10.Text.Split(',').Length - 1];
                    break;
                case "grid11":
                    editparams = txteditparams11.Text;
                    _temp = txteditparams11.Text.Split(',');
                    _array = new string[txteditparams11.Text.Split(',').Length - 1];
                    break;
                case "grid12":
                    editparams = txteditparams12.Text;
                    _temp = txteditparams12.Text.Split(',');
                    _array = new string[txteditparams12.Text.Split(',').Length - 1];
                    break;
                case "grid13":
                    editparams = txteditparams13.Text;
                    _temp = txteditparams13.Text.Split(',');
                    _array = new string[txteditparams13.Text.Split(',').Length - 1];
                    break;
                case "grid14":
                    editparams = txteditparams14.Text;
                    _temp = txteditparams14.Text.Split(',');
                    _array = new string[txteditparams14.Text.Split(',').Length - 1];
                    break;
                case "grid15":
                    editparams = txteditparams15.Text;
                    _temp = txteditparams15.Text.Split(',');
                    _array = new string[txteditparams15.Text.Split(',').Length - 1];
                    break;
            }

            if (!string.IsNullOrEmpty(editparams))
            {
                bool _dvar = false;
                for (int i = 1; i < _temp.Length; i++)
                {
                    if (_temp[i].Contains("_d") | _temp[i].Contains("_t"))
                        _dvar = true;
                    else
                        _dvar = false;
                    string _tempi = _temp[i].Replace("_d", string.Empty).Replace("_t", string.Empty);
                    if (_tempi == "COMP")
                    {
                        _array[i - 1] = _obj.Comp;
                    }
                    else if (_tempi == "USER")
                    {
                        _array[i - 1] = _obj.UserName;
                    }
                    else if (_tempi == "P1")
                    {
                        _array[i - 1] = this.HiddenP1.Value;
                    }
                    else if (_tempi == "P2")
                    {
                        _array[i - 1] = this.HiddenP2.Value;
                    }
                    else if (_tempi == "P3")
                    {
                        _array[i - 1] = this.HiddenP3.Value;
                    }
                    else if (_tempi == "P4")
                    {
                        _array[i - 1] = this.HiddenP4.Value;
                    }
                    else if (_tempi == "P5")
                    {
                        _array[i - 1] = this.HiddenP5.Value;
                    }

                    else if (string.IsNullOrEmpty(_tempi))
                    {
                        _array[i - 1] = "";
                    }
                    else if (_tempi.Substring(0, 1) == "$")
                    {
                        try { _array[i - 1] = _tempi.Substring(1, _tempi.Length - 1); } catch { }
                    }
                    else if (_tempi.Substring(0, 1) == "#")
                    {
                        try
                        {
                            string[] transparams = txtCatchParamsAndValues.Text.Split('|');
                            string _val1 = Array.Find(transparams, element => element.StartsWith(_tempi.Replace("#", string.Empty), StringComparison.Ordinal));

                            _array[i - 1] = _val1.Split(';')[1];

                        }
                        catch { }
                    }
                    else
                    {
                        if (e.NewValues[_tempi] != null)
                        {
                            if (e.NewValues[_tempi].ToString().Contains('|'))
                                _array[i - 1] = e.NewValues[_tempi].ToString().Split('|')[e.NewValues[_tempi].ToString().Split('|').Length - 1];
                            else
                            {
                                _array[i - 1] = e.NewValues[_tempi].ToString();
                            }
                        }
                        else
                        {
                            _array[i - 1] = "";
                        }

                    }

                    if (_dvar & !string.IsNullOrEmpty(_array[i - 1])) _array[i - 1] = _temp[i].Substring(0, 2) + _array[i - 1];
                }
                services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                if (!string.IsNullOrEmpty(Session["InsertedVal"].ToString()))
                    gridI.JSProperties["cpQval"] = Session["InsertedVal"].ToString();
            }

            gridI.JSProperties["cpIsUpdated"] = true;
            gridI.JSProperties["cpMsg"] = Session["WebMessage"].ToString();
        }
        protected void grid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            Session["InsertedVal"] = string.Empty;
            Session["WebMessage"] = string.Empty;
            ASPxGridView gridI = sender as ASPxGridView;
            string deleteparams = string.Empty;
            string[] _temp = { }, _array = { };
            switch (gridI.ID)
            {
                case "grid1":
                    deleteparams = txtdeleteparams1.Text;
                    _temp = txtdeleteparams1.Text.Split(',');
                    _array = new string[txtdeleteparams1.Text.Split(',').Length - 1];
                    break;
                case "grid2":
                    deleteparams = txtdeleteparams2.Text;
                    _temp = txtdeleteparams2.Text.Split(',');
                    _array = new string[txtdeleteparams2.Text.Split(',').Length - 1];
                    break;
                case "grid3":
                    deleteparams = txtdeleteparams3.Text;
                    _temp = txtdeleteparams3.Text.Split(',');
                    _array = new string[txtdeleteparams3.Text.Split(',').Length - 1];
                    break;
                case "grid4":
                    deleteparams = txtdeleteparams4.Text;
                    _temp = txtdeleteparams4.Text.Split(',');
                    _array = new string[txtdeleteparams4.Text.Split(',').Length - 1];
                    break;
                case "grid5":
                    deleteparams = txtdeleteparams5.Text;
                    _temp = txtdeleteparams5.Text.Split(',');
                    _array = new string[txtdeleteparams5.Text.Split(',').Length - 1];
                    break;
                case "grid6":
                    deleteparams = txtdeleteparams6.Text;
                    _temp = txtdeleteparams6.Text.Split(',');
                    _array = new string[txtdeleteparams6.Text.Split(',').Length - 1];
                    break;
                case "grid7":
                    deleteparams = txtdeleteparams7.Text;
                    _temp = txtdeleteparams7.Text.Split(',');
                    _array = new string[txtdeleteparams7.Text.Split(',').Length - 1];
                    break;
                case "grid8":
                    deleteparams = txtdeleteparams8.Text;
                    _temp = txtdeleteparams8.Text.Split(',');
                    _array = new string[txtdeleteparams8.Text.Split(',').Length - 1];
                    break;
                case "grid9":
                    deleteparams = txtdeleteparams9.Text;
                    _temp = txtdeleteparams9.Text.Split(',');
                    _array = new string[txtdeleteparams9.Text.Split(',').Length - 1];
                    break;
                case "grid10":
                    deleteparams = txtdeleteparams10.Text;
                    _temp = txtdeleteparams10.Text.Split(',');
                    _array = new string[txtdeleteparams10.Text.Split(',').Length - 1];
                    break;
                case "grid11":
                    deleteparams = txtdeleteparams11.Text;
                    _temp = txtdeleteparams11.Text.Split(',');
                    _array = new string[txtdeleteparams11.Text.Split(',').Length - 1];
                    break;
                case "grid12":
                    deleteparams = txtdeleteparams12.Text;
                    _temp = txtdeleteparams12.Text.Split(',');
                    _array = new string[txtdeleteparams12.Text.Split(',').Length - 1];
                    break;
                case "grid13":
                    deleteparams = txtdeleteparams13.Text;
                    _temp = txtdeleteparams13.Text.Split(',');
                    _array = new string[txtdeleteparams13.Text.Split(',').Length - 1];
                    break;
                case "grid14":
                    deleteparams = txtdeleteparams14.Text;
                    _temp = txtdeleteparams14.Text.Split(',');
                    _array = new string[txtdeleteparams14.Text.Split(',').Length - 1];
                    break;
                case "grid15":
                    deleteparams = txtdeleteparams15.Text;
                    _temp = txtdeleteparams15.Text.Split(',');
                    _array = new string[txtdeleteparams15.Text.Split(',').Length - 1];
                    break;
            }

            if (!string.IsNullOrEmpty(deleteparams))
            {
                bool _dvar = false;
                for (int i = 1; i < _temp.Length; i++)
                {
                    if (_temp[i].Contains("_d") | _temp[i].Contains("_t"))
                        _dvar = true;
                    else
                        _dvar = false;
                    string _tempi = _temp[i].Replace("_d", string.Empty).Replace("_t", string.Empty);
                    if (_tempi == "COMP")
                    {
                        _array[i - 1] = _obj.Comp;
                    }
                    else if (_tempi == "USER")
                    {
                        _array[i - 1] = _obj.UserName;
                    }
                    else if (_tempi == "P1")
                    {
                        _array[i - 1] = this.HiddenP1.Value;
                    }
                    else if (_tempi == "P2")
                    {
                        _array[i - 1] = this.HiddenP2.Value;
                    }
                    else if (_tempi == "P3")
                    {
                        _array[i - 1] = this.HiddenP3.Value;
                    }
                    else if (_tempi == "P4")
                    {
                        _array[i - 1] = this.HiddenP4.Value;
                    }
                    else if (_tempi == "P5")
                    {
                        _array[i - 1] = this.HiddenP5.Value;
                    }
                    else if (string.IsNullOrEmpty(_tempi))
                    {
                        _array[i - 1] = "";
                    }
                    else if (_tempi.Substring(0, 1) == "$")
                    {
                        try { _array[i - 1] = _tempi.Substring(1, _tempi.Length - 1); } catch { }
                    }
                    else if (_tempi.Substring(0, 1) == "#")
                    {
                        try
                        {
                            string[] transparams = txtCatchParamsAndValues.Text.Split('|');
                            string _val1 = Array.Find(transparams, element => element.StartsWith(_tempi.Replace("#", string.Empty), StringComparison.Ordinal));

                            _array[i - 1] = _val1.Split(';')[1];

                        }
                        catch { }
                    }
                    else
                    {
                        if (e.Values[_tempi] != null)
                        {
                            if (e.Values[_tempi].ToString().Contains('|'))
                                _array[i - 1] = e.Values[_tempi].ToString().Split('|')[e.Values[_tempi].ToString().Split('|').Length - 1];
                            else
                            {
                                if (_tempi == "R0037")
                                {
                                    if (e.Values[_tempi].ToString() == "COST")
                                        _array[i - 1] = "1";
                                    else if (e.Values[_tempi].ToString() == "SALES")
                                        _array[i - 1] = "2";
                                    else if (e.Values[_tempi].ToString() == "")
                                        _array[i - 1] = "0";
                                    else
                                        _array[i - 1] = e.Values[_tempi].ToString();

                                }
                                else
                                    _array[i - 1] = e.Values[_tempi].ToString();
                            }
                        }
                        else
                            _array[i - 1] = "";

                    }

                    if (_dvar & !string.IsNullOrEmpty(_array[i - 1])) _array[i - 1] = _temp[i].Substring(0, 2) + _array[i - 1];
                }
                services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                if (!string.IsNullOrEmpty(Session["InsertedVal"].ToString()))
                    gridI.JSProperties["cpQval"] = Session["InsertedVal"].ToString();
            }

            gridI.JSProperties["cpIsUpdated"] = true;
            gridI.JSProperties["cpMsg"] = Session["WebMessage"].ToString();
        }
        protected void Grid_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;


            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + grid.ID.Substring(grid.ID.Length == 5 ? grid.ID.Length - 1 : grid.ID.Length - 2) + "Rows";
            switch (e.ButtonType)
            {
                case ColumnCommandButtonType.Edit:
                    if (grid.SettingsEditing.Mode == GridViewEditingMode.Batch) e.Visible = false;
                    DataRow[] EditRows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='EDIT' and [DX Display Order]>0");
                    if (EditRows.Length > 0)
                    {
                        string _col = EditRows[0]["DX Permision Field"].ToString();
                        if (grid.GetRowValues(e.VisibleIndex, _col) != null)
                        {
                            if (grid.GetRowValues(e.VisibleIndex, _col).ToString() == "0")
                            {
                                e.Visible = false;
                            }
                        }
                    }
                    else
                        e.Visible = false;

                    break;
                case ColumnCommandButtonType.Delete:
                    DataRow[] DeleteRows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='DELETE' and [DX Display Order]>0");
                    if (DeleteRows.Length > 0)
                    {
                        string _col = DeleteRows[0]["DX Permision Field"].ToString();
                        if (grid.GetRowValues(e.VisibleIndex, _col) != null)
                        {
                            if (grid.GetRowValues(e.VisibleIndex, _col).ToString() == "0")
                            {
                                e.Visible = false;
                            }
                        }
                    }
                    else
                        e.Visible = false;
                    break;
                case ColumnCommandButtonType.SelectCheckbox:
                case ColumnCommandButtonType.Select:
                    DataRow[] selectrows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='SELECT' and [DX Display Order]>0");
                    if (selectrows.Length > 0)
                    {
                        string _selcol = selectrows[0]["DX Permision Field"].ToString();
                        if (grid.GetRowValues(e.VisibleIndex, _selcol) != null)
                        {
                            if (grid.GetRowValues(e.VisibleIndex, _selcol).ToString() == "0")
                                e.Enabled = false;
                        }

                    }
                    else
                        e.Visible = false;
                    break;
                case ColumnCommandButtonType.Update:
                    if (grid.JSProperties["cpOpType"].ToString() == "VIEW")
                        e.Visible = false;
                    break;
                case ColumnCommandButtonType.New:
                    if (grid.JSProperties["cpOpType"].ToString() == "INSERT")
                        e.Visible = false;
                    break;
            }


        }
        protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;

            Session["ClonedIdsAndValues"] = string.Empty;

            string Sessionn = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + gridI.ID.Substring(gridI.ID.Length == 5 ? gridI.ID.Length - 1 : gridI.ID.Length - 2) + "Rows";
            string sessionval = string.Empty;
            bool ilk = true;

            if (gridI.JSProperties["cpOpType"].ToString() == "VIEW")
            {
                DataRow[] rowsviewhd = ((DataTable)Session[Sessionn]).Select("[DX Type1]='DATA' and [DX VwPI]=0 and  [DX Display Order]>0");
                if (rowsviewhd.Length > 0)
                {
                    foreach (DataRow row in rowsviewhd)
                    {
                        gridI.DataColumns[row["DX Field ID"].ToString()].EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

                    }
                }

                DataRow[] rowsviewvs = ((DataTable)Session[Sessionn]).Select("[DX Type1]='DATA' and [DX VwPI]>0 and  [DX Display Order]>0");
                if (rowsviewvs.Length > 0)
                {
                    foreach (DataRow row in rowsviewvs)
                    {

                        gridI.DataColumns[row["DX Field ID"].ToString()].EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.True;
                        gridI.DataColumns[row["DX Field ID"].ToString()].EditFormSettings.VisibleIndex = Convert.ToInt32(row["DX VwPI"].ToString());
                    }
                }

                if (viewedValues != null)
                {
                    DataRow[] rows = ((DataTable)Session[Sessionn]).Select("[DX VwPI]>0");
                    foreach (DataRow row in rows)
                        e.NewValues[row["DX Field ID"].ToString()] = viewedValues[row["DX Field ID"].ToString()];
                }


            }
            else
            {

                if (gridI.JSProperties["cpOpType"].ToString() == "CLONE")
                {
                    DataRow[] rowsclonehd = ((DataTable)Session[Sessionn]).Select("[DX Type1]='DATA' and [DX Clone Hidden]>0 and  [DX Display Order]>0 and [DX Edit Popup Index]<>-2");

                    if (rowsclonehd.Length > 0)
                    {
                        foreach (DataRow row in rowsclonehd)
                        {
                            gridI.DataColumns[row["DX Field ID"].ToString()].EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;
                        }
                    }

                    DataRow[] rowsclonenhd = ((DataTable)Session[Sessionn]).Select("[DX Type1]='DATA' and [DX Clone Hidden]=0  and [DX Edit Popup Index]>-1 and [DX Display Order]>0");
                    if (rowsclonenhd.Length > 0)
                    {
                        foreach (DataRow row in rowsclonenhd)
                        {
                            gridI.DataColumns[row["DX Field ID"].ToString()].EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.True;

                        }
                    }


                    if (copiedValues != null)
                    {
                        DataRow[] rows = ((DataTable)Session[Sessionn]).Select("[DX Clone] >0");
                        foreach (DataRow row in rows)
                        {
                            e.NewValues[row["DX Field ID"].ToString()] = copiedValues[row["DX Field ID"].ToString()];

                            if (ilk)
                            {
                                sessionval = row["DX Field ID"].ToString() + ":" + copiedValues[row["DX Field ID"].ToString()];
                                ilk = false;
                            }
                            else
                                sessionval += "#" + row["DX Field ID"].ToString() + ":" + copiedValues[row["DX Field ID"].ToString()];
                        }

                        if (!string.IsNullOrEmpty(sessionval)) Session["ClonedIdsAndValues"] = sessionval;
                    }
                }
                else if (gridI.JSProperties["cpOpType"].ToString() == "INSERT" | string.IsNullOrEmpty(gridI.JSProperties["cpOpType"].ToString()))
                {

                    DataRow[] rowsInsertHidden = ((DataTable)Session[Sessionn]).Select("[DX Type1]='DATA' and [DX Insert Hidden]=1 and  [DX Display Order]>0 and [DX Edit Popup Index]<>-2");

                    if (rowsInsertHidden.Length > 0)
                    {
                        foreach (DataRow row in rowsInsertHidden)
                        {
                            try { gridI.DataColumns[row["DX Field ID"].ToString()].EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False; } catch { }

                        }
                    }


                }

                DataRow[] rows3 = ((DataTable)Session[Sessionn]).Select("[DX Type1]='DATA' and [DX TrFlVw]<>'' and [DX Display Order]>0");
                if (rows3.Length > 0)
                {
                    foreach (DataRow row in rows3)
                    {
                        string defVal = row["DX TrFlVw"].ToString(), _result = string.Empty;

                        if (defVal.Substring(0, 1) == "#")
                        {

                            string[] _arr = txtCatchParamsAndValues.Text.Split('|');

                            var q1 = from p in _arr where p.Contains(defVal.Replace("#", string.Empty) + ";") select p;

                            if (q1.Count() > 0)
                            {
                                if (!string.IsNullOrEmpty(q1.FirstOrDefault().ToString().Split(';')[1]))
                                {
                                    _result = q1.FirstOrDefault().ToString().Split(';')[1];
                                }
                            }

                        }

                        if (_result == "0")
                        {
                            gridI.DataColumns[row["DX Field ID"].ToString()].EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

                        }
                        else if (_result == "1")
                        {
                            gridI.DataColumns[row["DX Field ID"].ToString()].EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.True;

                        }

                    }
                }


            }






        }
        protected void grid_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;


            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + gridI.ID.Substring(gridI.ID.Length == 5 ? gridI.ID.Length - 1 : gridI.ID.Length - 2) + "Rows";
            if (e.ButtonID == "Clone")
            {
                string Sessionn = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + gridI.ID.Substring(gridI.ID.Length == 5 ? gridI.ID.Length - 1 : gridI.ID.Length - 2) + "Rows";
                DataRow[] rows = ((DataTable)Session[Sessionn]).Select("[DX Clone]>0");

                if (rows.Length > 0)
                {
                    copiedValues = new Hashtable();

                    foreach (DataRow row in rows)
                    {
                        copiedValues[row["DX Field ID"].ToString()] = gridI.GetRowValues(e.VisibleIndex, row["DX Field ID"].ToString());
                    }
                }
                gridI.JSProperties["cpOpType"] = "CLONE";
                gridI.AddNewRow();
            }
            else if (e.ButtonID == "View")
            {
                string Sessionn = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + gridI.ID.Substring(gridI.ID.Length == 5 ? gridI.ID.Length - 1 : gridI.ID.Length - 2) + "Rows";
                DataRow[] rows = ((DataTable)Session[Sessionn]).Select("[DX VwPI]>0");
                if (rows.Length > 0)
                {
                    viewedValues = new Hashtable();
                    foreach (DataRow row in rows)
                    {
                        viewedValues[row["DX Field ID"].ToString()] = gridI.GetRowValues(e.VisibleIndex, row["DX Field ID"].ToString());
                    }
                }
                gridI.JSProperties["cpOpType"] = "VIEW";
                gridI.AddNewRow();
            }
            else if (e.ButtonID == "SelectAll")
            {
                gridI.JSProperties["cpOpType"] = "SELECT";
                DataRow[] selectrows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='SELECT' and [DX Display Order]>0");
                string _selcol = selectrows[0]["DX Permision Field"].ToString();
                if (!string.IsNullOrEmpty(_selcol))
                {
                    for (int counter = 0; counter < gridI.VisibleRowCount; counter++)
                    {
                        if (Convert.ToInt32(gridI.GetRowValues(counter, _selcol)) == 1)
                            gridI.Selection.SelectRow(counter);
                    }
                }
                else
                    gridI.Selection.SelectAll();
            }
            else if (e.ButtonID == "UnSelectAll")
            {
                gridI.JSProperties["cpOpType"] = "UNSELECT";
                gridI.Selection.UnselectAll();
            }
            else
            {
                gridI.JSProperties["cpOpType"] = "NEW";
            }



        }
        protected void grid_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView xgrid = (ASPxGridView)sender;
            string _Session = GetSessionName(xgrid.ID);

            if (xgrid.JSProperties["cpOpType"].ToString() == "VIEW")
            {
                e.Editor.ClientEnabled = false;
                return;

            }

            if (xgrid.IsNewRowEditing)
            {
                if (e.Editor.GetType() == typeof(GridViewDataComboBoxColumn)
                    | e.Editor.GetType() == typeof(ASPxComboBox))
                {
                    ASPxComboBox cmb = e.Editor as ASPxComboBox;
                    if (cmb.NullText.Contains("CascColumn1"))
                    {
                        cmb.CallbackPageSize = 5000;
                        try
                        {
                            string[] _array = cmb.NullText.Split('#');
                            string _source = _array[1],
                                _filter = _array[2],
                                _value = _array[3],
                                _text = _array[4],
                                _anafieldId = _array[5],
                                _anasorgu = _array[6],
                                _cascfilter = _array[7];
                            object _val = null;
                            string _val2 = string.Empty;
                            if (Session["ClonedIdsAndValues"] != null)
                            {
                                if (!string.IsNullOrEmpty(Session["ClonedIdsAndValues"].ToString()))
                                {
                                    string[] _arrclonedvals = Session["ClonedIdsAndValues"].ToString().Split('#');

                                    var q1 = from p in _arrclonedvals where p.Contains(_anafieldId + ":") select p;

                                    if (q1.Count() > 0)
                                        _val2 = q1.FirstOrDefault().ToString().Split(':')[1];
                                }
                                else
                                    _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, _anafieldId);
                            }
                            else
                                _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, _anafieldId);
                            if (!string.IsNullOrEmpty(_val2))
                                _val = _val2;

                            if (_val != null)
                            {
                                if (!string.IsNullOrEmpty(_val.ToString()))
                                {
                                    string sorgu = "select " + _value + ", " + _text + "  from [" + _source + "] where 1=1 ";

                                    if (!string.IsNullOrEmpty(_filter))
                                        sorgu += " and " + _filter + "='" + _obj.Comp + "'";
                                    sorgu += " and " + _cascfilter + "='" + _val.ToString() + "'";

                                    f.FillAspxCombobox(cmb, sorgu, _text, _value);


                                    if (!string.IsNullOrEmpty(e.Editor.Value.ToString()))
                                    {
                                        try
                                        {
                                            if (cmb.Items.Count == 1)
                                                cmb.SelectedIndex = 0;
                                            else
                                                cmb.SelectedIndex = cmb.Items.IndexOfValue(e.Editor.Value.ToString());

                                        }
                                        catch { }
                                    }
                                }
                            }
                        }
                        catch { }
                        cmb.Callback += cmbDynamicCascade;
                    }
                    else if (cmb.NullText.Contains("CascColumn2"))
                    {
                        cmb.CallbackPageSize = 5000;
                        try
                        {
                            string[] _array = cmb.NullText.Split('#');
                            string _source = _array[1],
                                _filter = _array[2],
                                _value = _array[3],
                                _text = _array[4],
                                _anafieldId = _array[5],
                                _anasorgu = _array[6],
                                _cascfilter = _array[7],
                                _mainval = _array[8],
                                _mainfilter = _array[9];

                            string _val2par = string.Empty;
                            object _val = null;
                            string _val2 = string.Empty;
                            if (Session["ClonedIdsAndValues"] != null)
                            {
                                if (!string.IsNullOrEmpty(Session["ClonedIdsAndValues"].ToString()))
                                {
                                    string[] _arrclonedvals = Session["ClonedIdsAndValues"].ToString().Split('#');

                                    var q1 = from p in _arrclonedvals where p.Contains(_anafieldId + ":") select p;

                                    if (q1.Count() > 0)
                                        _val2 = q1.FirstOrDefault().ToString().Split(':')[1];
                                }
                                else
                                    _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, _anafieldId);
                            }
                            else
                                _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, _anafieldId);
                            if (!string.IsNullOrEmpty(_val2))
                                _val = _val2;
                            if (_val != null)
                            {
                                if (!string.IsNullOrEmpty(_val.ToString()))
                                {

                                    string sorgu = "select " + _cascfilter + " from [" + _anasorgu + "] where 1=1 ";
                                    if (!string.IsNullOrEmpty(_mainfilter))
                                        sorgu += " and " + _mainfilter + "='" + _obj.Comp + "'";
                                    sorgu += " and " + _mainval + "='" + _val.ToString() + "'";

                                    _val2par = f.tekbirsonucdonder(sorgu);

                                    sorgu = "select " + _value + ", " + _text + "  from [" + _source + "] where 1=1 ";

                                    if (!string.IsNullOrEmpty(_filter))
                                        sorgu += " and " + _filter + "='" + _obj.Comp + "'";
                                    sorgu += " and " + _cascfilter + "='" + _val2par + "'";

                                    f.FillAspxCombobox(cmb, sorgu, _text, _value);

                                    if (!string.IsNullOrEmpty(e.Editor.Value.ToString()))
                                    {

                                        try
                                        {
                                            if (cmb.Items.Count == 1)
                                                cmb.SelectedIndex = 0;
                                            else
                                                cmb.SelectedIndex = cmb.Items.IndexOfValue(e.Editor.Value.ToString());

                                        }
                                        catch { }
                                    }
                                }
                            }
                        }
                        catch { }
                        cmb.Callback += cmbDynamicCascade2;
                    }
                    else if (cmb.NullText.Contains("CascColumn3"))
                    {
                        cmb.CallbackPageSize = 5000;
                        try
                        {
                            string[] _array = cmb.NullText.Split('#');
                            string _source = _array[1],
                                _filter = _array[2],
                                _value = _array[3],
                                _text = _array[4],
                                _anafieldId = _array[5],
                                _anasorgu = _array[6],
                                _cascfilter = _array[7],
                                _mainval = _array[8],
                                _mainfilter = _array[9];

                            string _val2par = string.Empty;
                            object _val = null;
                            string _val2 = string.Empty;
                            if (Session["ClonedIdsAndValues"] != null)
                            {
                                if (!string.IsNullOrEmpty(Session["ClonedIdsAndValues"].ToString()))
                                {
                                    string[] _arrclonedvals = Session["ClonedIdsAndValues"].ToString().Split('#');

                                    var q1 = from p in _arrclonedvals where p.Contains(_anafieldId + ":") select p;

                                    if (q1.Count() > 0)
                                        _val2 = q1.FirstOrDefault().ToString().Split(':')[1];
                                }
                                else
                                    _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, _anafieldId);
                            }
                            else
                                _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, _anafieldId);
                            if (!string.IsNullOrEmpty(_val2))
                                _val = _val2;

                            string sorgu = "select " + _value + ", " + _text + "  from [" + _source + "] where 1=1 ";

                            if (!string.IsNullOrEmpty(_filter))
                                sorgu += " and " + _filter + "='" + _obj.Comp + "'";

                            f.FillAspxCombobox(cmb, sorgu, _text, _value);

                            if (!string.IsNullOrEmpty(e.Editor.Value.ToString()))
                            {
                                try { cmb.SelectedIndex = cmb.Items.IndexOfValue(e.Editor.Value); } catch { }
                            }

                        }
                        catch { }
                        cmb.Callback += cmbDynamicCascade3;
                    }
                    else if (cmb.NullText.Contains("CascColumn4"))
                    {
                        cmb.CallbackPageSize = 5000;
                        string _val2par = string.Empty;
                        object _val = null;
                        string _val2 = string.Empty, _val3 = string.Empty;
                        try
                        {

                            string[] _array = cmb.NullText.Split('@');
                            string _anafieldId = _array[1],
                                    _source = _array[2],
                                    _filter = _array[3],
                                    _value = _array[4],
                                    _text = _array[5];


                            if (Session["ClonedIdsAndValues"] != null)
                            {
                                if (!string.IsNullOrEmpty(Session["ClonedIdsAndValues"].ToString()))
                                {
                                    string[] _arrclonedvals = Session["ClonedIdsAndValues"].ToString().Split('#');

                                    var q1 = from p in _arrclonedvals where p.Contains(_anafieldId + ":") select p;

                                    if (q1.Count() > 0)
                                        _val2 = q1.FirstOrDefault().ToString().Split(':')[1];

                                    var q2 = from p in _arrclonedvals where p.Contains(e.Column.FieldName + ":") select p;

                                    if (q2.Count() > 0)
                                        _val3 = q2.FirstOrDefault().ToString().Split(':')[1];

                                }
                                else
                                    _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, _anafieldId);
                            }
                            else
                                _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, _anafieldId);
                            if (!string.IsNullOrEmpty(_val2))
                                _val = _val2;


                            if (e.Editor.Value != null)
                            {
                                if (!string.IsNullOrEmpty(e.Editor.Value.ToString()))
                                {
                                    try { cmb.SelectedIndex = cmb.Items.IndexOfValue(e.Editor.Value); } catch { }
                                }
                            }

                            if (_val != null)
                            {
                                if (!string.IsNullOrEmpty(_val.ToString()))
                                {

                                    using (SqlConnection conn = new SqlConnection(strConnString))
                                    {
                                        using (SqlCommand cmd = new SqlCommand())
                                        {
                                            cmd.CommandText = _source;
                                            cmd.CommandType = CommandType.StoredProcedure;
                                            cmd.Connection = conn;
                                            _filter = _filter.Substring(0, _filter.Length);
                                            string[] spParams = _filter.Split(',');

                                            cmd.Parameters.AddWithValue("PRM1", _val);

                                            for (int i = 1; i < spParams.Length; i++)
                                            {
                                                string _result = string.Empty;
                                                if (spParams[i] == "USER")
                                                {
                                                    _result = _obj.UserName;
                                                }
                                                else if (spParams[i] == "COMP")
                                                {
                                                    _result = _obj.Comp;
                                                }

                                                else if (spParams[i].Contains("#"))
                                                {
                                                    string[] transParam = Session["CpVals"].ToString().Split('|');
                                                }
                                                else if (spParams[i].ToString() == "P1")
                                                    _result = HiddenP1.Value;
                                                else if (spParams[i].ToString() == "P2")
                                                    _result = HiddenP2.Value;
                                                else
                                                {
                                                    object _res = xgrid.GetRowValuesByKeyValue(e.KeyValue, spParams[i]);
                                                    _result = _res.ToString();
                                                }


                                                cmd.Parameters.AddWithValue("@PRM" + (i + 1), _result);
                                            }



                                            DataTable dt = new DataTable();
                                            SqlDataAdapter adp = new SqlDataAdapter(cmd);
                                            adp.Fill(dt);


                                            cmb.Items.Clear();
                                            cmb.DataSourceID = null;
                                            cmb.DataSource = dt;
                                            cmb.TextField = _text;
                                            cmb.ValueField = _value;
                                            cmb.DataBind();

                                            if (!string.IsNullOrEmpty(e.Editor.Value.ToString()))
                                            {
                                                try { cmb.SelectedIndex = cmb.Items.IndexOfValue(e.Editor.Value); } catch { }
                                            }
                                        }
                                    }

                                }
                            }






                        }
                        catch (Exception ex)
                        {
                        }
                        cmb.Callback += cmbDynamicCascade4;
                        if (!string.IsNullOrEmpty(_val3))
                            try { cmb.SelectedIndex = cmb.Items.IndexOfValue(_val3); } catch { }

                    }
                    else if (cmb.NullText.Contains("CascColumn6"))
                    {
                        cmb.CallbackPageSize = 5000;
                        cmbDynamicCascade6(cmb, xgrid, e);
                    }
                    else if (cmb.NullText.Contains("CascColumn8"))
                    {
                        cmb.CallbackPageSize = 5000;
                        cmb.Callback += cmbDynamicCascade8;
                    }

                    try
                    {
                        if (!string.IsNullOrEmpty(Session["InsertComboSelectedValue"].ToString()))
                        {
                            if (e.Column.FieldName == Session["InsertComboSelectedValue"].ToString().Split('#')[0])
                            {
                                cmb.DataSource = f.datatablegonder(txtInsertComboSource.Text + txtInsertComboParams.Text.Replace("X", Session["InsertComboSelectedValue"].ToString().Split('#')[1]));
                                cmb.DataBind();
                            }
                        }
                    }
                    catch
                    {


                    }

                }
            }
            else
            {
                if (e.Editor.GetType() == typeof(GridViewDataComboBoxColumn)
                | e.Editor.GetType() == typeof(ASPxComboBox))
                {
                    ASPxComboBox cmb = e.Editor as ASPxComboBox;

                    if (cmb.NullText.Contains("CascColumn1"))
                    {
                        cmb.CallbackPageSize = 5000;
                        try
                        {
                            string[] _array = cmb.NullText.Split('#');
                            string _source = _array[1],
                                _filter = _array[2],
                                _value = _array[3],
                                _text = _array[4],
                                _anafieldId = _array[5],
                                _anasorgu = _array[6],
                                _cascfilter = _array[7];
                            object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, _anafieldId);
                            if (_val != null)
                            {
                                if (!string.IsNullOrEmpty(_val.ToString()))
                                {
                                    string sorgu = "select " + _value + ", " + _text + "  from [" + _source + "] where 1=1 ";

                                    if (!string.IsNullOrEmpty(_filter))
                                        sorgu += " and " + _filter + "='" + _obj.Comp + "'";
                                    sorgu += " and " + _cascfilter + "='" + _val.ToString() + "'";
                                    f.FillAspxCombobox(cmb, sorgu, _text, _value);

                                    if (!string.IsNullOrEmpty(e.Editor.Value.ToString()))
                                    {
                                        try
                                        {
                                            if (cmb.Items.Count == 1)
                                                cmb.SelectedIndex = 0;
                                            else
                                                cmb.SelectedIndex = cmb.Items.IndexOfValue(e.Editor.Value.ToString());

                                        }
                                        catch { }
                                    }


                                }
                            }
                        }
                        catch { }
                        cmb.Callback += cmbDynamicCascade;
                    }
                    else if (cmb.NullText.Contains("CascColumn2"))
                    {
                        cmb.CallbackPageSize = 5000;
                        try
                        {
                            string[] _array = cmb.NullText.Split('#');
                            string _source = _array[1],
                                _filter = _array[2],
                                _value = _array[3],
                                _text = _array[4],
                                _anafieldId = _array[5],
                                _anasorgu = _array[6],
                                _cascfilter = _array[7],
                                _mainval = _array[8],
                                _mainfilter = _array[9];

                            string _val2par = string.Empty;
                            object _val = _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, _anafieldId);
                            if (_val != null)
                            {
                                if (!string.IsNullOrEmpty(_val.ToString()))
                                {

                                    string sorgu = "select " + _cascfilter + " from [" + _anasorgu + "] where 1=1 ";
                                    if (!string.IsNullOrEmpty(_mainfilter))
                                        sorgu += " and " + _mainfilter + "='" + _obj.Comp + "'";
                                    sorgu += " and " + _mainval + "='" + _val.ToString() + "'";


                                    _val2par = f.tekbirsonucdonder(sorgu);

                                    sorgu = "select " + _value + ", " + _text + "  from [" + _source + "] where 1=1 ";

                                    if (!string.IsNullOrEmpty(_filter))
                                        sorgu += " and " + _filter + "='" + _obj.Comp + "'";
                                    sorgu += " and " + _cascfilter + "='" + _val2par + "'";

                                    f.FillAspxCombobox(cmb, sorgu, _text, _value);

                                    if (!string.IsNullOrEmpty(e.Editor.Value.ToString()))
                                    {
                                        try
                                        {
                                            if (cmb.Items.Count == 1)
                                                cmb.SelectedIndex = 0;
                                            else
                                                cmb.SelectedIndex = cmb.Items.IndexOfValue(e.Editor.Value.ToString());

                                        }
                                        catch { }
                                    }
                                }
                            }
                        }
                        catch { }
                        cmb.Callback += cmbDynamicCascade2;
                    }
                    else if (cmb.NullText.Contains("CascColumn3"))
                    {
                        cmb.CallbackPageSize = 5000;
                        try
                        {
                            string[] _array = cmb.NullText.Split('#');
                            string _source = _array[1],
                                _filter = _array[2],
                                _value = _array[3],
                                _text = _array[4],
                                _anafieldId = _array[5],
                                _anasorgu = _array[6],
                                _cascfilter = _array[7],
                                _mainval = _array[8],
                                _mainfilter = _array[9];

                            string _val2par = string.Empty;
                            object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, _anafieldId);
                            if (_val != null)
                            {
                                if (!string.IsNullOrEmpty(_val.ToString()))
                                {

                                    string sorgu = "select " + _value + ", " + _text + "  from [" + _source + "] where 1=1 ";

                                    if (!string.IsNullOrEmpty(_filter))
                                        sorgu += " and " + _filter + "='" + _obj.Comp + "'";

                                    f.FillAspxCombobox(cmb, sorgu, _text, _value);

                                    if (!string.IsNullOrEmpty(e.Editor.Value.ToString()))
                                    {
                                        try { cmb.SelectedIndex = cmb.Items.IndexOfValue(e.Editor.Value); } catch { }
                                    }
                                }
                            }
                        }
                        catch { }
                        cmb.Callback += cmbDynamicCascade3;
                    }
                    else if (cmb.NullText.Contains("CascColumn4"))
                    {
                        cmb.CallbackPageSize = 5000;
                        try
                        {
                            string[] _array = cmb.NullText.Split('@');
                            string _anafieldId = _array[1],
                                    _source = _array[2],
                                    _filter = _array[3],
                                    _value = _array[4],
                                    _text = _array[5];

                            string _val2par = string.Empty;
                            object _val = null;
                            _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, _anafieldId);

                            if (_val != null)
                            {
                                if (!string.IsNullOrEmpty(_val.ToString()))
                                {

                                    using (SqlConnection conn = new SqlConnection(strConnString))
                                    {
                                        using (SqlCommand cmd = new SqlCommand())
                                        {
                                            cmd.CommandText = _source;
                                            cmd.CommandType = CommandType.StoredProcedure;
                                            cmd.Connection = conn;
                                            _filter = _filter.Substring(0, _filter.Length);
                                            string[] spParams = _filter.Split(',');

                                            cmd.Parameters.AddWithValue("PRM1", _val);

                                            for (int i = 1; i < spParams.Length; i++)
                                            {
                                                string _result = string.Empty;
                                                if (spParams[i] == "USER")
                                                {
                                                    _result = _obj.UserName;
                                                }
                                                else if (spParams[i] == "COMP")
                                                {
                                                    _result = _obj.Comp;
                                                }
                                                else if (spParams[i].Contains("#"))
                                                {
                                                    string[] transParam = Session["CpVals"].ToString().Split('|');
                                                }
                                                else if (spParams[i].ToString() == "P1")
                                                    _result = HiddenP1.Value;
                                                else if (spParams[i].ToString() == "P2")
                                                    _result = HiddenP2.Value;
                                                else
                                                {
                                                    object _res = xgrid.GetRowValuesByKeyValue(e.KeyValue, spParams[i]);
                                                    _result = _res.ToString();
                                                }


                                                cmd.Parameters.AddWithValue("@PRM" + (i + 1), _result);
                                            }



                                            DataTable dt = new DataTable();
                                            SqlDataAdapter adp = new SqlDataAdapter(cmd);
                                            adp.Fill(dt);


                                            cmb.Items.Clear();
                                            cmb.DataSourceID = null;
                                            cmb.DataSource = dt;
                                            cmb.TextField = _text;
                                            cmb.ValueField = _value;
                                            cmb.DataBind();



                                        }
                                    }

                                }

                            }

                            cmb.Callback += cmbDynamicCascade4;


                            if (e.Editor.Value != null)
                            {

                                if (!string.IsNullOrEmpty(e.Editor.Value.ToString()))
                                {
                                    try { cmb.SelectedIndex = cmb.Items.IndexOfValue(e.Editor.Value); } catch { }
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                        }


                    }
                    else if (cmb.NullText.Contains("CascColumn6"))
                    {
                        cmb.CallbackPageSize = 5000;
                        cmbDynamicCascade6(cmb, xgrid, e);
                    }
                    else if (cmb.NullText.Contains("CascColumn8"))
                    {
                        cmb.CallbackPageSize = 5000;
                        cmb.Callback += cmbDynamicCascade8;
                    }


                }
            }

            if (xgrid.IsNewRowEditing)
            {
                DataRow[] RowDefaultVal = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX DfVl]<>''");
                if (RowDefaultVal.Length > 0)
                {
                    string defVal = RowDefaultVal[0]["DX DfVl"].ToString();
                    if (e.Editor.GetType() == typeof(ASPxComboBox))
                    {

                        ASPxComboBox cmb = e.Editor as ASPxComboBox;
                        if (defVal.Substring(0, 1) == "#")
                        {

                            string[] _arr = txtCatchParamsAndValues.Text.Split('|');

                            var q1 = from p in _arr where p.Contains(defVal.Replace("#", string.Empty) + ";") select p;

                            if (q1.Count() > 0)
                            {
                                if (!string.IsNullOrEmpty(q1.FirstOrDefault().ToString().Split(';')[1]))
                                {
                                    try { cmb.SelectedIndex = cmb.Items.IndexOfValue(q1.FirstOrDefault().ToString().Split(';')[1]); } catch { }
                                }
                            }

                        }
                        else
                        {
                            try
                            {
                                int number;
                                bool success = Int32.TryParse(defVal.Trim(), out number);
                                if (success)
                                    cmb.SelectedIndex = cmb.Items.IndexOfValue(number);
                                else
                                    cmb.SelectedIndex = cmb.Items.IndexOfValue(defVal.Trim());
                            }
                            catch { }
                        }
                    }
                    else if (e.Editor.GetType() == typeof(ASPxTextBox))
                    {
                        ASPxTextBox cmb = e.Editor as ASPxTextBox;
                        if (defVal.Substring(0, 1) == "#")
                        {

                            string[] _arr = txtCatchParamsAndValues.Text.Split('|');

                            var q1 = from p in _arr where p.Contains(defVal.Replace("#", string.Empty) + ";") select p;

                            if (q1.Count() > 0)
                            {
                                if (!string.IsNullOrEmpty(q1.FirstOrDefault().ToString().Split(';')[1]))
                                {
                                    cmb.Text = q1.FirstOrDefault().ToString().Split(';')[1];
                                }
                            }

                        }
                        else
                        {
                            cmb.Text = defVal;
                        }
                    }
                }

                if (xgrid.JSProperties["cpOpType"].ToString() == "CLONE")
                {

                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Clone  Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;

                }
                else
                {
                    DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Readonly]=1");
                    if (RowsReadOnly.Length > 0)
                        e.Editor.ClientEnabled = false;
                    else
                        e.Editor.ClientEnabled = true;
                }

                if (!string.IsNullOrEmpty(Session["InsertComboSelectedValue"].ToString()))
                {
                    DataRow[] RowInsertColumnVisibilties = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX MnCmPrFl]<>''");
                    if (RowInsertColumnVisibilties.Length > 0)
                    {
                        string _xcolumn = RowInsertColumnVisibilties[0]["DX MnCmPrFl"].ToString();
                        try
                        {
                            DataTable dt = f.datatablegonder(txtInsertComboSource.Text + txtInsertComboParams.Text.Replace("X", Session["InsertComboSelectedValue"].ToString().Split('#')[1]));
                            if (dt.Rows.Count > 0)
                            {
                                switch (dt.Rows[0][_xcolumn].ToString())
                                {
                                    case "0":
                                        e.Editor.ClientVisible = false;
                                        break;
                                    case "1":
                                        e.Editor.ClientEnabled = false;
                                        break;
                                }
                            }
                        }
                        catch
                        {


                        }

                    }

                }

            }
            else
            {
                //e.Column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.True;
                DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Edit Readonly]=1");
                if (RowsReadOnly.Length > 0)
                    e.Editor.ClientEnabled = false;
                else
                    e.Editor.ClientEnabled = true;

            }


            int tabindex = 0;

            try
            {
                DataRow[] RowsField = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (RowsField.Length > 0)
                {
                    if (xgrid.JSProperties["cpOpType"].ToString() == "CLONE")
                        tabindex = Convert.ToInt32(RowsField[0]["DX InTI"]);
                    else if (xgrid.JSProperties["cpOpType"].ToString() == "EDIT")
                        tabindex = Convert.ToInt32(RowsField[0]["DX EdTI"]);
                    else
                        tabindex = Convert.ToInt32(RowsField[0]["DX InTI"]);
                }
            }
            catch { }


            e.Editor.TabIndex = Convert.ToInt16(tabindex);





        }
        protected void grid_StartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {

            ASPxGridView gridI = sender as ASPxGridView;
            gridI.JSProperties["cpOpType"] = "EDIT";
            string Sessionn = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + gridI.ID.Substring(gridI.ID.Length == 5 ? gridI.ID.Length - 1 : gridI.ID.Length - 2) + "Rows";

            DataRow[] rows = ((DataTable)Session[Sessionn]).Select("[DX Type1]='DATA' and [DX Edit Hidden]=1 and [DX Display Order]>0 and [DX Edit Popup Index]<>-2");
            if (rows.Length > 0)
            {
                foreach (DataRow row in rows)
                {
                    gridI.DataColumns[row["DX Field ID"].ToString()].EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

                }
            }


            DataRow[] rows2 = ((DataTable)Session[Sessionn]).Select("[DX Type1]='DATA' and [DX Edit Hidden]=0 and [DX Edit Popup Index]>-1 and [DX Display Order]>0");
            if (rows2.Length > 0)
            {
                foreach (DataRow row in rows2)
                {
                    gridI.DataColumns[row["DX Field ID"].ToString()].EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.True;

                }
            }

            DataRow[] rows3 = ((DataTable)Session[Sessionn]).Select("[DX Type1]='DATA' and [DX TrFlVw]<>'' and [DX Display Order]>0");
            if (rows3.Length > 0)
            {
                foreach (DataRow row in rows3)
                {
                    string defVal = row["DX TrFlVw"].ToString(), _result = string.Empty;

                    if (defVal.Substring(0, 1) == "#")
                    {

                        string[] _arr = txtCatchParamsAndValues.Text.Split('|');

                        var q1 = from p in _arr where p.Contains(defVal.Replace("#", string.Empty) + ";") select p;

                        if (q1.Count() > 0)
                        {
                            if (!string.IsNullOrEmpty(q1.FirstOrDefault().ToString().Split(';')[1]))
                            {
                                _result = q1.FirstOrDefault().ToString().Split(';')[1];
                            }
                        }

                    }
                    if (_result == "0")
                    {
                        gridI.DataColumns[row["DX Field ID"].ToString()].EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

                    }
                    else if (_result == "1")
                    {
                        gridI.DataColumns[row["DX Field ID"].ToString()].EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.True;

                    }

                }
            }


        }

        #region Grid1
        protected void grid1_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF1.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters == "LoadMainDataP1")
                grid.JSProperties["cpPageLinked"] = true;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP1.Text, txtOBJF1.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();
            }
            else if (e.Parameters.Contains("DevexPopup|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP1.Text, txtOBJF1.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpDevexPopup"] = true;
                grid.JSProperties["cpSource"] = e.Parameters.Split('|')[2];
            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP1.Text, txtOBJF1.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("ReportButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP1.Text, txtOBJF1.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsWithNavReturnIslemId(grid, rows, _obj.UserName, _obj.Comp, "50000.OnBeforeWebReportSELECTED");
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string Result11 = Session["WebMessage"].ToString()
                       , Result12 = Session["InsertedVal"].ToString(), Result21 = string.Empty, Result22 = string.Empty;
                if (Result11 != "OK")
                {
                    throw new Exception(Result11);
                }
                if (!string.IsNullOrEmpty(e.Parameters.Split('|')[4]))
                {
                    string _PdfResult = ReportUtilities.CreateReport(_obj.UserName, _obj.Comp, e.Parameters.Split('|')[2].Split(',')[0], true
                        , Server.MapPath(CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4]), Result12
                         , txtCatchParamsAndValues.Text, HiddenP1.Value, HiddenP2.Value, IslemId, e.Parameters.Split('|')[3]);



                    if (!string.IsNullOrEmpty(_PdfResult))
                    {
                        throw new Exception(_PdfResult);
                    }

                    string[] arr = new string[] { IslemId, _obj.UserName };
                    services.NavFunctionsStatic(_obj.UserName, _obj.Comp, "50000.OnAfterWebReportSELECTED", arr);

                    Result21 = Session["WebMessage"].ToString();
                    Result22 = Session["InsertedVal"].ToString();

                    grid.JSProperties["cpProcType"] = "POSTMODE|" + Result11 + "|" + Result12 + "|" + Result21 + "|" + Result22;
                    grid.JSProperties["cpLink"] = CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4] + "/" + Result12 + ".PDF";
                }
                else
                {
                    grid.JSProperties["cpProcType"] = "OPENMODE";
                    grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];

                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("PdfMerge|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP1.Text, txtOBJF1.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string _res = DynamicUtilsV1.DynamicPagePdfMerge(_obj.Comp, _obj.UserName, e.Parameters.Split('|')[2], IslemId);
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = _res;
                grid.JSProperties["cpQueryStrings"] = "PDFMERGE";
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters == "LoadEmpty")
            {

                HiddenP1.Value = "XXXXXX";
                HiddenP2.Value = "YYYYYY";

            }


            if (Request.Url.ToString().Contains("PRM1"))
            {
                string values = string.Empty;
                if (Request.Url.ToString().Contains("PRM1="))
                {
                    values = Request.QueryString["PRM1"].ToString();
                }
                if (Request.Url.ToString().Contains("PRM2="))
                {
                    values += ";" + Request.QueryString["PRM2"].ToString();
                }

                this.dtgrid1.Table = DynamicUtilsV1.fillMainGridWithParams(txtOBJP1.Text, txtOBJF1.Text, _obj.Comp, _obj.UserName, values);
            }
            else
            {
                this.dtgrid1.Table = DynamicUtilsV1.fillMainGrid(txtOBJP1.Text, txtOBJF1.Text, _obj.Comp, _obj.UserName);
            }
            string _filterexpression = grid.FilterExpression;
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid1.Table.Columns["ID"];
            this.dtgrid1.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid1";
            grid.KeyFieldName = "ID";


            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + grid.ID.Substring(grid.ID.Length == 5 ? grid.ID.Length - 1 : grid.ID.Length - 2) + "Rows";
            grid.FilterExpression = DynamicUtilsV1.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));



            grid.DataBind();

            grid.FilterExpression = _filterexpression;


        }
        protected void menu1_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Contains("btnExcel"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid1.VisibleRowCount == 0)
                    {
                        gridExport1.WriteXlsToResponse();
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid1.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        gridExport1.WriteXlsToResponse();
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    gridExport1.WriteXlsToResponse();

            }
            else if (e.Item.Name.Contains("btnPdf"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid1.VisibleRowCount == 0)
                    {
                        DynamicUtilsV1.ExportPdf(gridExport1);
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid1.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        DynamicUtilsV1.ExportPdf(gridExport1);
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    DynamicUtilsV1.ExportPdf(gridExport1);

            }
            else if (e.Item.Name.Equals("btninsert"))
            {
                this.HiddenP1.Value = "AXYZT";
                this.HiddenP2.Value = "";

                if (postURL.Contains("?"))
                {
                    iframeView.Attributes.Add("src", postURL + "GId=" + Request.QueryString["GId"] + "&FaturaStatu =" + invoiceStatu + "&InvoiceType=" + invoiceType);
                }
                else
                {
                    iframeView.Attributes.Add("src", postURL + "?GId=" + Request.QueryString["GId"] + "&FaturaStatu =" + invoiceStatu + "&InvoiceType=" + invoiceType);
                }

                HiddenMainStatu.Value = "99";
                try
                {
                    this.dtgrid2.Table.Rows.Clear();
                    grid2.DataBind();
                }
                catch { }
                try
                {
                    this.dtgrid3.Table.Rows.Clear();
                    grid3.DataBind();
                }
                catch { }
                try
                {
                    this.dtgrid4.Table.Rows.Clear();
                    grid4.DataBind();
                }
                catch { }
                try
                {
                    this.dtgrid5.Table.Rows.Clear();
                    grid5.DataBind();
                }
                catch { }
                try
                {
                    this.dtgrid6.Table.Rows.Clear();
                    grid6.DataBind();
                }
                catch { }
            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(OBJ1_pageId, OBJ1_fieldId, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid1, rows, _obj.UserName, _obj.Comp);

                try { grid1_CustomCallback(grid1, new ASPxGridViewCustomCallbackEventArgs("")); } catch { }
                try { grid2_CustomCallback(grid2, new ASPxGridViewCustomCallbackEventArgs("")); } catch { }
                try { grid3_CustomCallback(grid3, new ASPxGridViewCustomCallbackEventArgs("")); } catch { }
                try { grid4_CustomCallback(grid4, new ASPxGridViewCustomCallbackEventArgs("")); } catch { }
                try { grid5_CustomCallback(grid5, new ASPxGridViewCustomCallbackEventArgs("")); } catch { }
                try { grid6_CustomCallback(grid6, new ASPxGridViewCustomCallbackEventArgs("")); } catch { }

                iframeView.Attributes.Add("Src", HiddenpageType.Value + HiddenP1.Value);


            }
            else if (e.Item.Name.Contains("btnWebMethod"))
            {
                MergePDF();
            }
        }

        private void MergePDF()
        {
            List<object> ArrayValues = grid1.GetSelectedFieldValues("R0011");
            string[] _selectedRows = new string[ArrayValues.Count];

            DataTable dt = new DataTable();
            foreach (var item in ArrayValues.ToList())
            {
                string query = "select * from [0C_00167_80_JOB_DOCUMENTS] where [Job no] ='" + item.ToString() + "'";
                dt = f.datatablegonder(query);


                if (dt.Rows.Count > 0)
                {
                    string[] pdfs = new string[dt.Rows.Count];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        pdfs[i] = Server.MapPath(CurrentFolderName(_obj.Comp) + "/" + dt.Rows[i]["DocPATH2"].ToString());
                    }


                    using (PdfDocument targetDoc = new PdfDocument())
                    {
                        foreach (string pdf in pdfs)
                        {
                            try
                            {
                                using (PdfDocument pdfDoc = PdfReader.Open(pdf, PdfDocumentOpenMode.Import))
                                {

                                    for (int i = 0; i < pdfDoc.PageCount; i++)
                                    {
                                        targetDoc.AddPage(pdfDoc.Pages[i]);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {

                            }

                        }

                        string tName = @"/Dosya/Job Pdfs/" + _obj.UserName + "_" + DateTime.Now.ToFileTime().ToString() + "_pdffile.pdf";

                        string targetPath = Server.MapPath(CurrentFolderName(_obj.Comp) + tName);


                        targetDoc.Save(targetPath);

                        Response.Write("<script>");
                        Response.Write("window.open('" + CurrentFolderName(_obj.Comp) + tName + "','_blank', ' fullscreen=yes')");
                        Response.Write("</script>");
                    }

                    break;
                }
            }





        }
        #endregion

        #region Grid2
        protected void grid2_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {

            if (txtOBJF2.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP2.Text, txtOBJF2.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();

                if (rows[0]["DX Caption Filter"].ToString() == "1")
                {
                    grid.JSProperties["cpQueryStrings"] = "PAGEREFRESH";
                    grid.JSProperties["cpIsLinked"] = true;
                }

            }
            else if (e.Parameters.Contains("DevexPopup|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP2.Text, txtOBJF2.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpDevexPopup"] = true;
                grid.JSProperties["cpSource"] = e.Parameters.Split('|')[2];
            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP2.Text, txtOBJF2.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("ReportButton|"))
            {

                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP2.Text, txtOBJF2.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsWithNavReturnIslemId(grid, rows, _obj.UserName, _obj.Comp, "50000.OnBeforeWebReportSELECTED");

                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string Result11 = Session["WebMessage"].ToString()
                       , Result12 = Session["InsertedVal"].ToString(), Result21 = string.Empty, Result22 = string.Empty;
                if (Result11 != "OK")
                {

                    throw new Exception(Result11);
                }

                if (!string.IsNullOrEmpty(e.Parameters.Split('|')[4]))
                {

                    string _PdfResult = ReportUtilities.CreateReport(_obj.UserName, _obj.Comp, e.Parameters.Split('|')[2].Split(',')[0], true
                        , Server.MapPath(CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4]), Result12
                         , txtCatchParamsAndValues.Text, HiddenP1.Value, HiddenP2.Value, IslemId, e.Parameters.Split('|')[3]);

                    if (!string.IsNullOrEmpty(_PdfResult))
                    {
                        throw new Exception(_PdfResult);
                    }

                    string[] arr = new string[] { IslemId, _obj.UserName };
                    services.NavFunctionsStatic(_obj.UserName, _obj.Comp, "50000.OnAfterWebReportSELECTED", arr);

                    Result21 = Session["WebMessage"].ToString();
                    Result22 = Session["InsertedVal"].ToString();

                    grid.JSProperties["cpProcType"] = "POSTMODE|" + Result11 + "|" + Result12 + "|" + Result21 + "|" + Result22;
                    grid.JSProperties["cpLink"] = CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4] + "/" + Result12 + ".PDF";
                }
                else
                {
                    grid.JSProperties["cpProcType"] = "OPENMODE";
                    grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];

                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("PdfMerge|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP2.Text, txtOBJF2.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string _res = DynamicUtilsV1.DynamicPagePdfMerge(_obj.Comp, _obj.UserName, e.Parameters.Split('|')[2], IslemId);
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = _res;
                grid.JSProperties["cpQueryStrings"] = "PDFMERGE";
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("InsertCombo|"))
            {
                Session["InsertComboSelectedValue"] = e.Parameters.Split('|')[1] + "#" + e.Parameters.Split('|')[2];

                grid.AddNewRow();

                return;
            }
            else if (e.Parameters == "LoadEmpty")
            {

                HiddenP1.Value = "XXXXXX";
                HiddenP2.Value = "YYYYYY";

            }

            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP3.Value) & !string.IsNullOrEmpty(txtMasterObject2.Text))
            {

                if (!string.IsNullOrEmpty(HiddenP3.Value)) values = HiddenP3.Value;
                if (!string.IsNullOrEmpty(HiddenP4.Value)) values += ";" + HiddenP4.Value;
                if (!string.IsNullOrEmpty(HiddenP5.Value)) values += ";" + HiddenP5.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
                if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            }
            if (string.IsNullOrEmpty(values)) return;

            if (e.Parameters == "LoadEmpty")
            {

                HiddenP1.Value = "";
                HiddenP2.Value = "";

            }


            this.dtgrid2.Table = DynamicUtilsV1.fillSubGrid(txtOBJP2.Text, txtOBJF2.Text, _obj.Comp, _obj.UserName, txtOBJP1.Text, txtOBJF1.Text, values);

            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid2.Table.Columns["ID"];
            this.dtgrid2.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid2";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + grid.ID.Substring(grid.ID.Length == 5 ? grid.ID.Length - 1 : grid.ID.Length - 2) + "Rows";


            grid.FilterExpression = DynamicUtilsV1.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));
            string _filterexpression = grid.FilterExpression;

            grid.DataBind();
            HiddenP3.Value = "";
            HiddenP4.Value = "";
            grid.FilterExpression = _filterexpression;


        }
        protected void menu2_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Contains("btnExcel"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid2.VisibleRowCount == 0)
                    {
                        gridExport2.WriteXlsToResponse();
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid2.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        gridExport2.WriteXlsToResponse();
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    gridExport2.WriteXlsToResponse();

            }
            else if (e.Item.Name.Contains("btnPdf"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid2.VisibleRowCount == 0)
                    {
                        DynamicUtilsV1.ExportPdf(gridExport2);
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid2.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        DynamicUtilsV1.ExportPdf(gridExport2);
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    DynamicUtilsV1.ExportPdf(gridExport2);

            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(OBJ2_pageId, OBJ2_fieldId, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid2, rows, _obj.UserName, _obj.Comp);

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }
        #endregion

        #region Grid3
        protected void grid3_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF3.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP3.Text, txtOBJF3.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();
            }
            else if (e.Parameters.Contains("DevexPopup|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP3.Text, txtOBJF3.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpDevexPopup"] = true;
                grid.JSProperties["cpSource"] = e.Parameters.Split('|')[2];
            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP3.Text, txtOBJF3.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("ReportButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];

                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP3.Text, txtOBJF3.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsWithNavReturnIslemId(grid, rows, _obj.UserName, _obj.Comp, "50000.OnBeforeWebReportSELECTED");
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string Result11 = Session["WebMessage"].ToString()
                       , Result12 = Session["InsertedVal"].ToString(), Result21 = string.Empty, Result22 = string.Empty;
                if (Result11 != "OK")
                {
                    throw new Exception(Result11);
                }
                if (!string.IsNullOrEmpty(e.Parameters.Split('|')[4]))
                {

                    string _PdfResult = ReportUtilities.CreateReport(_obj.UserName, _obj.Comp, e.Parameters.Split('|')[2].Split(',')[0], true
                        , Server.MapPath(CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4]), Result12
                         , txtCatchParamsAndValues.Text, HiddenP1.Value, HiddenP2.Value, IslemId, e.Parameters.Split('|')[3]);

                    if (!string.IsNullOrEmpty(_PdfResult))
                    {
                        throw new Exception(_PdfResult);
                    }

                    string[] arr = new string[] { IslemId, _obj.UserName };
                    services.NavFunctionsStatic(_obj.UserName, _obj.Comp, "50000.OnAfterWebReportSELECTED", arr);

                    Result21 = Session["WebMessage"].ToString();
                    Result22 = Session["InsertedVal"].ToString();

                    grid.JSProperties["cpProcType"] = "POSTMODE|" + Result11 + "|" + Result12 + "|" + Result21 + "|" + Result22;
                    grid.JSProperties["cpLink"] = CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4] + "/" + Result12 + ".PDF";
                }
                else
                {
                    grid.JSProperties["cpProcType"] = "OPENMODE|";
                    if (e.Parameters.Split('|')[2].Contains(".xlsx") || e.Parameters.Split('|')[2].Contains(".pdf"))
                    {
                        grid.JSProperties["cpLink"] = CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[2];
                    }
                    else
                    {
                        grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                    }
                }



                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("PdfMerge|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP3.Text, txtOBJF3.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string _res = DynamicUtilsV1.DynamicPagePdfMerge(_obj.Comp, _obj.UserName, e.Parameters.Split('|')[2], IslemId);
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = _res;
                grid.JSProperties["cpQueryStrings"] = "PDFMERGE";
                grid.JSProperties["cpIsLinked"] = true;

            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP3.Value) & !string.IsNullOrEmpty(txtMasterObject3.Text))
            {

                if (!string.IsNullOrEmpty(HiddenP3.Value)) values = HiddenP3.Value;
                if (!string.IsNullOrEmpty(HiddenP4.Value)) values += ";" + HiddenP4.Value;
                if (!string.IsNullOrEmpty(HiddenP5.Value)) values += ";" + HiddenP5.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
                if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            }
            if (string.IsNullOrEmpty(values)) return;


            this.dtgrid3.Table = DynamicUtilsV1.fillSubGrid(txtOBJP3.Text, txtOBJF3.Text, _obj.Comp, _obj.UserName, txtOBJP1.Text, txtOBJF1.Text, values);

            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid3.Table.Columns["ID"];
            this.dtgrid3.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid3";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + grid.ID.Substring(grid.ID.Length == 5 ? grid.ID.Length - 1 : grid.ID.Length - 2) + "Rows";


            grid.FilterExpression = DynamicUtilsV1.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;
            grid.DataBind();
            HiddenP3.Value = "";
            HiddenP4.Value = "";
            grid.FilterExpression = _filterexpression;

        }
        protected void menu3_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Contains("btnExcel"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid3.VisibleRowCount == 0)
                    {
                        gridExport3.WriteXlsToResponse();
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid3.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        gridExport3.WriteXlsToResponse();
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    gridExport3.WriteXlsToResponse();

            }
            else if (e.Item.Name.Contains("btnPdf"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid3.VisibleRowCount == 0)
                    {
                        DynamicUtilsV1.ExportPdf(gridExport3);
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid3.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        DynamicUtilsV1.ExportPdf(gridExport3);
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    DynamicUtilsV1.ExportPdf(gridExport3);

            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(OBJ3_pageId, OBJ3_fieldId, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid3, rows, _obj.UserName, _obj.Comp);

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);
            }
        }
        #endregion

        #region Grid4

        protected void grid4_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF4.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP4.Text, txtOBJF4.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();
            }
            else if (e.Parameters.Contains("DevexPopup|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP4.Text, txtOBJF4.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpDevexPopup"] = true;
                grid.JSProperties["cpSource"] = e.Parameters.Split('|')[2];
            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP4.Text, txtOBJF4.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("ReportButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP4.Text, txtOBJF4.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsWithNavReturnIslemId(grid, rows, _obj.UserName, _obj.Comp, "50000.OnBeforeWebReportSELECTED");
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string Result11 = Session["WebMessage"].ToString()
                       , Result12 = Session["InsertedVal"].ToString(), Result21 = string.Empty, Result22 = string.Empty;
                if (Result11 != "OK")
                {
                    throw new Exception(Result11);
                }
                if (!string.IsNullOrEmpty(e.Parameters.Split('|')[4]))
                {
                    string _PdfResult = ReportUtilities.CreateReport(_obj.UserName, _obj.Comp, e.Parameters.Split('|')[2].Split(',')[0], true
                                            , Server.MapPath(CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4]), Result12
                                             , txtCatchParamsAndValues.Text, HiddenP1.Value, HiddenP2.Value, IslemId, e.Parameters.Split('|')[3]);

                    if (!string.IsNullOrEmpty(_PdfResult))
                    {
                        throw new Exception(_PdfResult);
                    }

                    string[] arr = new string[] { IslemId, _obj.UserName };
                    services.NavFunctionsStatic(_obj.UserName, _obj.Comp, "50000.OnAfterWebReportSELECTED", arr);

                    Result21 = Session["WebMessage"].ToString();
                    Result22 = Session["InsertedVal"].ToString();

                    grid.JSProperties["cpProcType"] = "POSTMODE|" + Result11 + "|" + Result12 + "|" + Result21 + "|" + Result22;
                    grid.JSProperties["cpLink"] = CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4] + "/" + Result12 + ".PDF";
                }
                else
                {
                    grid.JSProperties["cpProcType"] = "OPENMODE";
                    grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];

                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("PdfMerge|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP4.Text, txtOBJF4.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string _res = DynamicUtilsV1.DynamicPagePdfMerge(_obj.Comp, _obj.UserName, e.Parameters.Split('|')[2], IslemId);
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = _res;
                grid.JSProperties["cpQueryStrings"] = "PDFMERGE";
                grid.JSProperties["cpIsLinked"] = true;

            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP3.Value) & !string.IsNullOrEmpty(txtMasterObject4.Text))
            {

                if (!string.IsNullOrEmpty(HiddenP3.Value)) values = HiddenP3.Value;
                if (!string.IsNullOrEmpty(HiddenP4.Value)) values += ";" + HiddenP4.Value;
                if (!string.IsNullOrEmpty(HiddenP5.Value)) values += ";" + HiddenP5.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
                if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            }
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid4.Table = DynamicUtilsV1.fillSubGrid(txtOBJP4.Text, txtOBJF4.Text, _obj.Comp, _obj.UserName, txtOBJP1.Text, txtOBJF1.Text, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid4.Table.Columns["ID"];
            this.dtgrid4.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid4";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + grid.ID.Substring(grid.ID.Length == 5 ? grid.ID.Length - 1 : grid.ID.Length - 2) + "Rows";


            grid.FilterExpression = DynamicUtilsV1.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;

            grid.DataBind();
            HiddenP3.Value = "";
            HiddenP4.Value = "";
            grid.FilterExpression = _filterexpression;

        }
        protected void menu4_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Contains("btnExcel"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid4.VisibleRowCount == 0)
                    {
                        gridExport4.WriteXlsToResponse();
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid4.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        gridExport4.WriteXlsToResponse();
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    gridExport4.WriteXlsToResponse();

            }
            else if (e.Item.Name.Contains("btnPdf"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid4.VisibleRowCount == 0)
                    {
                        DynamicUtilsV1.ExportPdf(gridExport4);
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid4.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        DynamicUtilsV1.ExportPdf(gridExport4);
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    DynamicUtilsV1.ExportPdf(gridExport4);

            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(OBJ4_pageId, OBJ4_fieldId, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid4, rows, _obj.UserName, _obj.Comp);

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }
        #endregion

        #region Grid5

        protected void grid5_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF5.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP5.Text, txtOBJF5.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();
            }
            else if (e.Parameters.Contains("DevexPopup|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP5.Text, txtOBJF5.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpDevexPopup"] = true;
                grid.JSProperties["cpSource"] = e.Parameters.Split('|')[2];
            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP5.Text, txtOBJF5.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("ReportButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP5.Text, txtOBJF5.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsWithNavReturnIslemId(grid, rows, _obj.UserName, _obj.Comp, "50000.OnBeforeWebReportSELECTED");
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string Result11 = Session["WebMessage"].ToString()
                       , Result12 = Session["InsertedVal"].ToString(), Result21 = string.Empty, Result22 = string.Empty;
                if (Result11 != "OK")
                {
                    throw new Exception(Result11);
                }
                if (!string.IsNullOrEmpty(e.Parameters.Split('|')[4]))
                {
                    string _PdfResult = ReportUtilities.CreateReport(_obj.UserName, _obj.Comp, e.Parameters.Split('|')[2].Split(',')[0], true
                                            , Server.MapPath(CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4]), Result12
                                             , txtCatchParamsAndValues.Text, HiddenP1.Value, HiddenP2.Value, IslemId, e.Parameters.Split('|')[3]);

                    if (!string.IsNullOrEmpty(_PdfResult))
                    {
                        throw new Exception(_PdfResult);
                    }

                    string[] arr = new string[] { IslemId, _obj.UserName };
                    services.NavFunctionsStatic(_obj.UserName, _obj.Comp, "50000.OnAfterWebReportSELECTED", arr);

                    Result21 = Session["WebMessage"].ToString();
                    Result22 = Session["InsertedVal"].ToString();

                    grid.JSProperties["cpProcType"] = "POSTMODE|" + Result11 + "|" + Result12 + "|" + Result21 + "|" + Result22;
                    grid.JSProperties["cpLink"] = CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4] + "/" + Result12 + ".PDF";
                }
                else
                {
                    grid.JSProperties["cpProcType"] = "OPENMODE";
                    grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];

                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("PdfMerge|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP5.Text, txtOBJF5.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string _res = DynamicUtilsV1.DynamicPagePdfMerge(_obj.Comp, _obj.UserName, e.Parameters.Split('|')[2], IslemId);
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = _res;
                grid.JSProperties["cpQueryStrings"] = "PDFMERGE";
                grid.JSProperties["cpIsLinked"] = true;

            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP3.Value) & !string.IsNullOrEmpty(txtMasterObject5.Text))
            {

                if (!string.IsNullOrEmpty(HiddenP3.Value)) values = HiddenP3.Value;
                if (!string.IsNullOrEmpty(HiddenP4.Value)) values += ";" + HiddenP4.Value;
                if (!string.IsNullOrEmpty(HiddenP5.Value)) values += ";" + HiddenP5.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
                if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            }
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid5.Table = DynamicUtilsV1.fillSubGrid(txtOBJP5.Text, txtOBJF5.Text, _obj.Comp, _obj.UserName, txtOBJP1.Text, txtOBJF1.Text, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid5.Table.Columns["ID"];
            this.dtgrid5.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid5";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + grid.ID.Substring(grid.ID.Length == 5 ? grid.ID.Length - 1 : grid.ID.Length - 2) + "Rows";

            grid.FilterExpression = DynamicUtilsV1.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;

            grid.DataBind();
            HiddenP3.Value = "";
            HiddenP4.Value = "";
            grid.FilterExpression = _filterexpression;

        }
        protected void menu5_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Contains("btnExcel"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid5.VisibleRowCount == 0)
                    {
                        gridExport5.WriteXlsToResponse();
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid5.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        gridExport5.WriteXlsToResponse();
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    gridExport5.WriteXlsToResponse();

            }
            else if (e.Item.Name.Contains("btnPdf"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid5.VisibleRowCount == 0)
                    {
                        DynamicUtilsV1.ExportPdf(gridExport5);
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid5.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        DynamicUtilsV1.ExportPdf(gridExport5);
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    DynamicUtilsV1.ExportPdf(gridExport5);

            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(OBJ5_pageId, OBJ5_fieldId, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid5, rows, _obj.UserName, _obj.Comp);

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }
        #endregion

        #region Grid6

        protected void grid6_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF6.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP6.Text, txtOBJF6.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();
            }
            else if (e.Parameters.Contains("DevexPopup|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP6.Text, txtOBJF6.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpDevexPopup"] = true;
                grid.JSProperties["cpSource"] = e.Parameters.Split('|')[2];
            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP6.Text, txtOBJF6.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("ReportButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP6.Text, txtOBJF6.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsWithNavReturnIslemId(grid, rows, _obj.UserName, _obj.Comp, "50000.OnBeforeWebReportSELECTED");
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string Result11 = Session["WebMessage"].ToString()
                       , Result12 = Session["InsertedVal"].ToString(), Result21 = string.Empty, Result22 = string.Empty;
                if (Result11 != "OK")
                {
                    throw new Exception(Result11);
                }
                if (!string.IsNullOrEmpty(e.Parameters.Split('|')[4]))
                {
                    string _PdfResult = ReportUtilities.CreateReport(_obj.UserName, _obj.Comp, e.Parameters.Split('|')[2].Split(',')[0], true
                                            , Server.MapPath(CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4]), Result12
                                             , txtCatchParamsAndValues.Text, HiddenP1.Value, HiddenP2.Value, IslemId, e.Parameters.Split('|')[3]);

                    if (!string.IsNullOrEmpty(_PdfResult))
                    {
                        throw new Exception(_PdfResult);
                    }

                    string[] arr = new string[] { IslemId, _obj.UserName };
                    services.NavFunctionsStatic(_obj.UserName, _obj.Comp, "50000.OnAfterWebReportSELECTED", arr);

                    Result21 = Session["WebMessage"].ToString();
                    Result22 = Session["InsertedVal"].ToString();

                    grid.JSProperties["cpProcType"] = "POSTMODE|" + Result11 + "|" + Result12 + "|" + Result21 + "|" + Result22;
                    grid.JSProperties["cpLink"] = CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4] + "/" + Result12 + ".PDF";
                }
                else
                {
                    grid.JSProperties["cpProcType"] = "OPENMODE";
                    grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];

                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("PdfMerge|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP6.Text, txtOBJF6.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string _res = DynamicUtilsV1.DynamicPagePdfMerge(_obj.Comp, _obj.UserName, e.Parameters.Split('|')[2], IslemId);
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = _res;
                grid.JSProperties["cpQueryStrings"] = "PDFMERGE";
                grid.JSProperties["cpIsLinked"] = true;

            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP3.Value) & !string.IsNullOrEmpty(txtMasterObject6.Text))
            {

                if (!string.IsNullOrEmpty(HiddenP3.Value)) values = HiddenP3.Value;
                if (!string.IsNullOrEmpty(HiddenP4.Value)) values += ";" + HiddenP4.Value;
                if (!string.IsNullOrEmpty(HiddenP5.Value)) values += ";" + HiddenP5.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
                if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            }
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid6.Table = DynamicUtilsV1.fillSubGrid(txtOBJP6.Text, txtOBJF6.Text, _obj.Comp, _obj.UserName, txtOBJP1.Text, txtOBJF1.Text, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid6.Table.Columns["ID"];
            this.dtgrid6.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid6";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + grid.ID.Substring(grid.ID.Length == 5 ? grid.ID.Length - 1 : grid.ID.Length - 2) + "Rows";


            grid.FilterExpression = DynamicUtilsV1.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;

            grid.DataBind();
            HiddenP3.Value = "";
            HiddenP4.Value = "";
            grid.FilterExpression = _filterexpression;

        }
        protected void menu6_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Contains("btnExcel"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid6.VisibleRowCount == 0)
                    {
                        gridExport6.WriteXlsToResponse();
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid6.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        gridExport6.WriteXlsToResponse();
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    gridExport6.WriteXlsToResponse();

            }
            else if (e.Item.Name.Contains("btnPdf"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid6.VisibleRowCount == 0)
                    {
                        DynamicUtilsV1.ExportPdf(gridExport6);
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid6.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        DynamicUtilsV1.ExportPdf(gridExport6);
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    DynamicUtilsV1.ExportPdf(gridExport6);

            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(OBJ6_pageId, OBJ6_fieldId, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid6, rows, _obj.UserName, _obj.Comp);

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }
        #endregion

        #region Grid7
        protected void menu7_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Contains("btnExcel"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid7.VisibleRowCount == 0)
                    {
                        gridExport7.WriteXlsToResponse();
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid7.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        gridExport7.WriteXlsToResponse();
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    gridExport7.WriteXlsToResponse();

            }
            else if (e.Item.Name.Contains("btnPdf"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid7.VisibleRowCount == 0)
                    {
                        DynamicUtilsV1.ExportPdf(gridExport7);
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid7.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        DynamicUtilsV1.ExportPdf(gridExport7);
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    DynamicUtilsV1.ExportPdf(gridExport7);

            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(OBJ7_pageId, OBJ7_fieldId, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid7, rows, _obj.UserName, _obj.Comp);

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }
        protected void grid7_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF7.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP7.Text, txtOBJF7.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();
            }
            else if (e.Parameters.Contains("DevexPopup|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP7.Text, txtOBJF7.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpDevexPopup"] = true;
                grid.JSProperties["cpSource"] = e.Parameters.Split('|')[2];
            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP7.Text, txtOBJF7.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("ReportButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP7.Text, txtOBJF7.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsWithNavReturnIslemId(grid, rows, _obj.UserName, _obj.Comp, "50000.OnBeforeWebReportSELECTED");
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string Result11 = Session["WebMessage"].ToString()
                       , Result12 = Session["InsertedVal"].ToString(), Result21 = string.Empty, Result22 = string.Empty;
                if (Result11 != "OK")
                {
                    throw new Exception(Result11);
                }
                if (!string.IsNullOrEmpty(e.Parameters.Split('|')[4]))
                {
                    string _PdfResult = ReportUtilities.CreateReport(_obj.UserName, _obj.Comp, e.Parameters.Split('|')[2].Split(',')[0], true
                                            , Server.MapPath(CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4]), Result12
                                             , txtCatchParamsAndValues.Text, HiddenP1.Value, HiddenP2.Value, IslemId, e.Parameters.Split('|')[3]);

                    if (!string.IsNullOrEmpty(_PdfResult))
                    {
                        throw new Exception(_PdfResult);
                    }

                    string[] arr = new string[] { IslemId, _obj.UserName };
                    services.NavFunctionsStatic(_obj.UserName, _obj.Comp, "50000.OnAfterWebReportSELECTED", arr);

                    Result21 = Session["WebMessage"].ToString();
                    Result22 = Session["InsertedVal"].ToString();

                    grid.JSProperties["cpProcType"] = "POSTMODE|" + Result11 + "|" + Result12 + "|" + Result21 + "|" + Result22;
                    grid.JSProperties["cpLink"] = CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4] + "/" + Result12 + ".PDF";
                }
                else
                {
                    grid.JSProperties["cpProcType"] = "OPENMODE";
                    grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];

                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("PdfMerge|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP7.Text, txtOBJF7.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string _res = DynamicUtilsV1.DynamicPagePdfMerge(_obj.Comp, _obj.UserName, e.Parameters.Split('|')[2], IslemId);
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = _res;
                grid.JSProperties["cpQueryStrings"] = "PDFMERGE";
                grid.JSProperties["cpIsLinked"] = true;

            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP3.Value) & !string.IsNullOrEmpty(txtMasterObject7.Text))
            {

                if (!string.IsNullOrEmpty(HiddenP3.Value)) values = HiddenP3.Value;
                if (!string.IsNullOrEmpty(HiddenP4.Value)) values += ";" + HiddenP4.Value;
                if (!string.IsNullOrEmpty(HiddenP5.Value)) values += ";" + HiddenP5.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
                if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            }
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid7.Table = DynamicUtilsV1.fillSubGrid(txtOBJP7.Text, txtOBJF7.Text, _obj.Comp, _obj.UserName, txtOBJP1.Text, txtOBJF1.Text, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid7.Table.Columns["ID"];
            this.dtgrid7.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid7";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + grid.ID.Substring(grid.ID.Length == 5 ? grid.ID.Length - 1 : grid.ID.Length - 2) + "Rows";


            grid.FilterExpression = DynamicUtilsV1.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;

            grid.DataBind();
            HiddenP3.Value = "";
            HiddenP4.Value = "";
            grid.FilterExpression = _filterexpression;

        }

        #endregion

        #region Grid8

        protected void menu8_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Contains("btnExcel"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid8.VisibleRowCount == 0)
                    {
                        gridExport8.WriteXlsToResponse();
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid8.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        gridExport8.WriteXlsToResponse();
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    gridExport8.WriteXlsToResponse();

            }
            else if (e.Item.Name.Contains("btnPdf"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid8.VisibleRowCount == 0)
                    {
                        DynamicUtilsV1.ExportPdf(gridExport8);
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid8.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        DynamicUtilsV1.ExportPdf(gridExport8);
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    DynamicUtilsV1.ExportPdf(gridExport8);

            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(OBJ8_pageId, OBJ8_fieldId, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid8, rows, _obj.UserName, _obj.Comp);

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }

        protected void grid8_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF8.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP8.Text, txtOBJF8.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();
            }
            else if (e.Parameters.Contains("DevexPopup|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP8.Text, txtOBJF8.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpDevexPopup"] = true;
                grid.JSProperties["cpSource"] = e.Parameters.Split('|')[2];
            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP8.Text, txtOBJF8.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("ReportButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP8.Text, txtOBJF8.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsWithNavReturnIslemId(grid, rows, _obj.UserName, _obj.Comp, "50000.OnBeforeWebReportSELECTED");
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string Result11 = Session["WebMessage"].ToString()
                       , Result12 = Session["InsertedVal"].ToString(), Result21 = string.Empty, Result22 = string.Empty;
                if (Result11 != "OK")
                {
                    throw new Exception(Result11);
                }
                if (!string.IsNullOrEmpty(e.Parameters.Split('|')[4]))
                {
                    string _PdfResult = ReportUtilities.CreateReport(_obj.UserName, _obj.Comp, e.Parameters.Split('|')[2].Split(',')[0], true
                                            , Server.MapPath(CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4]), Result12
                                             , txtCatchParamsAndValues.Text, HiddenP1.Value, HiddenP2.Value, IslemId, e.Parameters.Split('|')[3]);

                    if (!string.IsNullOrEmpty(_PdfResult))
                    {
                        throw new Exception(_PdfResult);
                    }

                    string[] arr = new string[] { IslemId, _obj.UserName };
                    services.NavFunctionsStatic(_obj.UserName, _obj.Comp, "50000.OnAfterWebReportSELECTED", arr);

                    Result21 = Session["WebMessage"].ToString();
                    Result22 = Session["InsertedVal"].ToString();

                    grid.JSProperties["cpProcType"] = "POSTMODE|" + Result11 + "|" + Result12 + "|" + Result21 + "|" + Result22;
                    grid.JSProperties["cpLink"] = CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4] + "/" + Result12 + ".PDF";
                }
                else
                {
                    grid.JSProperties["cpProcType"] = "OPENMODE";
                    grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];

                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("PdfMerge|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP8.Text, txtOBJF8.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string _res = DynamicUtilsV1.DynamicPagePdfMerge(_obj.Comp, _obj.UserName, e.Parameters.Split('|')[2], IslemId);
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = _res;
                grid.JSProperties["cpQueryStrings"] = "PDFMERGE";
                grid.JSProperties["cpIsLinked"] = true;

            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP3.Value) & !string.IsNullOrEmpty(txtMasterObject8.Text))
            {

                if (!string.IsNullOrEmpty(HiddenP3.Value)) values = HiddenP3.Value;
                if (!string.IsNullOrEmpty(HiddenP4.Value)) values += ";" + HiddenP4.Value;
                if (!string.IsNullOrEmpty(HiddenP5.Value)) values += ";" + HiddenP5.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
                if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            }
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid8.Table = DynamicUtilsV1.fillSubGrid(txtOBJP8.Text, txtOBJF8.Text, _obj.Comp, _obj.UserName, txtOBJP1.Text, txtOBJF1.Text, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid8.Table.Columns["ID"];
            this.dtgrid8.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid8";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + grid.ID.Substring(grid.ID.Length == 5 ? grid.ID.Length - 1 : grid.ID.Length - 2) + "Rows";


            grid.FilterExpression = DynamicUtilsV1.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;

            grid.DataBind();
            HiddenP3.Value = "";
            HiddenP4.Value = "";
            grid.FilterExpression = _filterexpression;

        }
        #endregion

        #region Grid9

        protected void menu9_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Contains("btnExcel"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid9.VisibleRowCount == 0)
                    {
                        gridExport9.WriteXlsToResponse();
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid9.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        gridExport9.WriteXlsToResponse();
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    gridExport9.WriteXlsToResponse();

            }
            else if (e.Item.Name.Contains("btnPdf"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid9.VisibleRowCount == 0)
                    {
                        DynamicUtilsV1.ExportPdf(gridExport9);
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid9.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        DynamicUtilsV1.ExportPdf(gridExport9);
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    DynamicUtilsV1.ExportPdf(gridExport9);

            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(OBJ9_pageId, OBJ9_fieldId, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid9, rows, _obj.UserName, _obj.Comp);

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }

        protected void grid9_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF9.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP9.Text, txtOBJF9.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();
            }
            else if (e.Parameters.Contains("DevexPopup|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP9.Text, txtOBJF9.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpDevexPopup"] = true;
                grid.JSProperties["cpSource"] = e.Parameters.Split('|')[2];
            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP9.Text, txtOBJF9.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("ReportButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP9.Text, txtOBJF9.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsWithNavReturnIslemId(grid, rows, _obj.UserName, _obj.Comp, "50000.OnBeforeWebReportSELECTED");
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string Result11 = Session["WebMessage"].ToString()
                       , Result12 = Session["InsertedVal"].ToString(), Result21 = string.Empty, Result22 = string.Empty;
                if (Result11 != "OK")
                {
                    throw new Exception(Result11);
                }
                if (!string.IsNullOrEmpty(e.Parameters.Split('|')[4]))
                {
                    string _PdfResult = ReportUtilities.CreateReport(_obj.UserName, _obj.Comp, e.Parameters.Split('|')[2].Split(',')[0], true
                                            , Server.MapPath(CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4]), Result12
                                             , txtCatchParamsAndValues.Text, HiddenP1.Value, HiddenP2.Value, IslemId, e.Parameters.Split('|')[3]);

                    if (!string.IsNullOrEmpty(_PdfResult))
                    {
                        throw new Exception(_PdfResult);
                    }

                    string[] arr = new string[] { IslemId, _obj.UserName };
                    services.NavFunctionsStatic(_obj.UserName, _obj.Comp, "50000.OnAfterWebReportSELECTED", arr);

                    Result21 = Session["WebMessage"].ToString();
                    Result22 = Session["InsertedVal"].ToString();

                    grid.JSProperties["cpProcType"] = "POSTMODE|" + Result11 + "|" + Result12 + "|" + Result21 + "|" + Result22;
                    grid.JSProperties["cpLink"] = CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4] + "/" + Result12 + ".PDF";
                }
                else
                {
                    grid.JSProperties["cpProcType"] = "OPENMODE";
                    grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];

                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("PdfMerge|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP9.Text, txtOBJF9.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string _res = DynamicUtilsV1.DynamicPagePdfMerge(_obj.Comp, _obj.UserName, e.Parameters.Split('|')[2], IslemId);
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = _res;
                grid.JSProperties["cpQueryStrings"] = "PDFMERGE";
                grid.JSProperties["cpIsLinked"] = true;

            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP3.Value) & !string.IsNullOrEmpty(txtMasterObject9.Text))
            {

                if (!string.IsNullOrEmpty(HiddenP3.Value)) values = HiddenP3.Value;
                if (!string.IsNullOrEmpty(HiddenP4.Value)) values += ";" + HiddenP4.Value;
                if (!string.IsNullOrEmpty(HiddenP5.Value)) values += ";" + HiddenP5.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
                if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            }
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid9.Table = DynamicUtilsV1.fillSubGrid(txtOBJP9.Text, txtOBJF9.Text, _obj.Comp, _obj.UserName, txtOBJP1.Text, txtOBJF1.Text, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid9.Table.Columns["ID"];
            this.dtgrid9.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid9";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + grid.ID.Substring(grid.ID.Length == 5 ? grid.ID.Length - 1 : grid.ID.Length - 2) + "Rows";


            grid.FilterExpression = DynamicUtilsV1.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;

            grid.DataBind();
            HiddenP3.Value = "";
            HiddenP4.Value = "";
            grid.FilterExpression = _filterexpression;

        }

        #endregion

        #region Grid10

        protected void menu10_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Contains("btnExcel"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid10.VisibleRowCount == 0)
                    {
                        gridExport10.WriteXlsToResponse();
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid10.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        gridExport10.WriteXlsToResponse();
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    gridExport10.WriteXlsToResponse();

            }
            else if (e.Item.Name.Contains("btnPdf"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid10.VisibleRowCount == 0)
                    {
                        DynamicUtilsV1.ExportPdf(gridExport10);
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid10.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        DynamicUtilsV1.ExportPdf(gridExport10);
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    DynamicUtilsV1.ExportPdf(gridExport10);

            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(OBJ10_pageId, OBJ10_fieldId, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid10, rows, _obj.UserName, _obj.Comp);

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }

        protected void grid10_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF10.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP10.Text, txtOBJF10.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();
            }
            else if (e.Parameters.Contains("DevexPopup|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP10.Text, txtOBJF10.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpDevexPopup"] = true;
                grid.JSProperties["cpSource"] = e.Parameters.Split('|')[2];
            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP10.Text, txtOBJF10.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("ReportButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP10.Text, txtOBJF10.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsWithNavReturnIslemId(grid, rows, _obj.UserName, _obj.Comp, "50000.OnBeforeWebReportSELECTED");
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string Result11 = Session["WebMessage"].ToString()
                       , Result12 = Session["InsertedVal"].ToString(), Result21 = string.Empty, Result22 = string.Empty;
                if (Result11 != "OK")
                {
                    throw new Exception(Result11);
                }
                if (!string.IsNullOrEmpty(e.Parameters.Split('|')[4]))
                {
                    string _PdfResult = ReportUtilities.CreateReport(_obj.UserName, _obj.Comp, e.Parameters.Split('|')[2].Split(',')[0], true
                                            , Server.MapPath(CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4]), Result12
                                             , txtCatchParamsAndValues.Text, HiddenP1.Value, HiddenP2.Value, IslemId, e.Parameters.Split('|')[3]);

                    if (!string.IsNullOrEmpty(_PdfResult))
                    {
                        throw new Exception(_PdfResult);
                    }

                    string[] arr = new string[] { IslemId, _obj.UserName };
                    services.NavFunctionsStatic(_obj.UserName, _obj.Comp, "50000.OnAfterWebReportSELECTED", arr);

                    Result21 = Session["WebMessage"].ToString();
                    Result22 = Session["InsertedVal"].ToString();

                    grid.JSProperties["cpProcType"] = "POSTMODE|" + Result11 + "|" + Result12 + "|" + Result21 + "|" + Result22;
                    grid.JSProperties["cpLink"] = CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4] + "/" + Result12 + ".PDF";
                }
                else
                {
                    grid.JSProperties["cpProcType"] = "OPENMODE";
                    grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];

                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("PdfMerge|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP11.Text, txtOBJF11.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string _res = DynamicUtilsV1.DynamicPagePdfMerge(_obj.Comp, _obj.UserName, e.Parameters.Split('|')[2], IslemId);
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = _res;
                grid.JSProperties["cpQueryStrings"] = "PDFMERGE";
                grid.JSProperties["cpIsLinked"] = true;

            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP3.Value) & !string.IsNullOrEmpty(txtMasterObject10.Text))
            {

                if (!string.IsNullOrEmpty(HiddenP3.Value)) values = HiddenP3.Value;
                if (!string.IsNullOrEmpty(HiddenP4.Value)) values += ";" + HiddenP4.Value;
                if (!string.IsNullOrEmpty(HiddenP5.Value)) values += ";" + HiddenP5.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
                if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            }
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid10.Table = DynamicUtilsV1.fillSubGrid(txtOBJP10.Text, txtOBJF10.Text, _obj.Comp, _obj.UserName, txtOBJP1.Text, txtOBJF1.Text, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid10.Table.Columns["ID"];
            this.dtgrid10.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid10";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + grid.ID.Substring(grid.ID.Length == 5 ? grid.ID.Length - 1 : grid.ID.Length - 2) + "Rows";


            grid.FilterExpression = DynamicUtilsV1.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;

            grid.DataBind();
            HiddenP3.Value = "";
            HiddenP4.Value = "";
            grid.FilterExpression = _filterexpression;

        }
        #endregion

        #region Grid11

        protected void menu11_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Contains("btnExcel"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid11.VisibleRowCount == 0)
                    {
                        gridExport11.WriteXlsToResponse();
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid11.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        gridExport11.WriteXlsToResponse();
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    gridExport11.WriteXlsToResponse();

            }
            else if (e.Item.Name.Contains("btnPdf"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid11.VisibleRowCount == 0)
                    {
                        DynamicUtilsV1.ExportPdf(gridExport11);
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid11.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        DynamicUtilsV1.ExportPdf(gridExport11);
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    DynamicUtilsV1.ExportPdf(gridExport11);

            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(OBJ11_pageId, OBJ11_fieldId, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid11, rows, _obj.UserName, _obj.Comp);

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }

        protected void grid11_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF11.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP11.Text, txtOBJF11.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();
            }
            else if (e.Parameters.Contains("DevexPopup|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP11.Text, txtOBJF11.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpDevexPopup"] = true;
                grid.JSProperties["cpSource"] = e.Parameters.Split('|')[2];
            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP11.Text, txtOBJF11.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("ReportButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP11.Text, txtOBJF11.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsWithNavReturnIslemId(grid, rows, _obj.UserName, _obj.Comp, "50000.OnBeforeWebReportSELECTED");
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string Result11 = Session["WebMessage"].ToString()
                       , Result12 = Session["InsertedVal"].ToString(), Result21 = string.Empty, Result22 = string.Empty;
                if (Result11 != "OK")
                {
                    throw new Exception(Result11);
                }
                if (!string.IsNullOrEmpty(e.Parameters.Split('|')[4]))
                {
                    string _PdfResult = ReportUtilities.CreateReport(_obj.UserName, _obj.Comp, e.Parameters.Split('|')[2].Split(',')[0], true
                                            , Server.MapPath(CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4]), Result12
                                             , txtCatchParamsAndValues.Text, HiddenP1.Value, HiddenP2.Value, IslemId, e.Parameters.Split('|')[3]);

                    if (!string.IsNullOrEmpty(_PdfResult))
                    {
                        throw new Exception(_PdfResult);
                    }

                    string[] arr = new string[] { IslemId, _obj.UserName };
                    services.NavFunctionsStatic(_obj.UserName, _obj.Comp, "50000.OnAfterWebReportSELECTED", arr);

                    Result21 = Session["WebMessage"].ToString();
                    Result22 = Session["InsertedVal"].ToString();

                    grid.JSProperties["cpProcType"] = "POSTMODE|" + Result11 + "|" + Result12 + "|" + Result21 + "|" + Result22;
                    grid.JSProperties["cpLink"] = CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4] + "/" + Result12 + ".PDF";
                }
                else
                {
                    grid.JSProperties["cpProcType"] = "OPENMODE";
                    grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];

                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("PdfMerge|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP11.Text, txtOBJF11.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string _res = DynamicUtilsV1.DynamicPagePdfMerge(_obj.Comp, _obj.UserName, e.Parameters.Split('|')[2], IslemId);
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = _res;
                grid.JSProperties["cpQueryStrings"] = "PDFMERGE";
                grid.JSProperties["cpIsLinked"] = true;

            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP3.Value) & !string.IsNullOrEmpty(txtMasterObject11.Text))
            {

                if (!string.IsNullOrEmpty(HiddenP3.Value)) values = HiddenP3.Value;
                if (!string.IsNullOrEmpty(HiddenP4.Value)) values += ";" + HiddenP4.Value;
                if (!string.IsNullOrEmpty(HiddenP5.Value)) values += ";" + HiddenP5.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
                if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            }
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid11.Table = DynamicUtilsV1.fillSubGrid(txtOBJP11.Text, txtOBJF11.Text, _obj.Comp, _obj.UserName, txtOBJP1.Text, txtOBJF1.Text, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid11.Table.Columns["ID"];
            this.dtgrid11.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid11";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + grid.ID.Substring(grid.ID.Length == 5 ? grid.ID.Length - 1 : grid.ID.Length - 2) + "Rows";

            grid.FilterExpression = DynamicUtilsV1.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;

            grid.DataBind();
            HiddenP3.Value = "";
            HiddenP4.Value = "";
            grid.FilterExpression = _filterexpression;
        }
        #endregion

        #region Grid12

        protected void menu12_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Contains("btnExcel"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid12.VisibleRowCount == 0)
                    {
                        gridExport12.WriteXlsToResponse();
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid12.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        gridExport12.WriteXlsToResponse();
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    gridExport12.WriteXlsToResponse();

            }
            else if (e.Item.Name.Contains("btnPdf"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid12.VisibleRowCount == 0)
                    {
                        DynamicUtilsV1.ExportPdf(gridExport12);
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid12.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        DynamicUtilsV1.ExportPdf(gridExport12);
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    DynamicUtilsV1.ExportPdf(gridExport12);

            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(OBJ12_pageId, OBJ12_fieldId, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid12, rows, _obj.UserName, _obj.Comp);

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }

        protected void grid12_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF12.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP12.Text, txtOBJF12.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();
            }
            else if (e.Parameters.Contains("DevexPopup|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP12.Text, txtOBJF12.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpDevexPopup"] = true;
                grid.JSProperties["cpSource"] = e.Parameters.Split('|')[2];
            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP12.Text, txtOBJF12.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("ReportButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP12.Text, txtOBJF12.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsWithNavReturnIslemId(grid, rows, _obj.UserName, _obj.Comp, "50000.OnBeforeWebReportSELECTED");
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string Result11 = Session["WebMessage"].ToString()
                       , Result12 = Session["InsertedVal"].ToString(), Result21 = string.Empty, Result22 = string.Empty;
                if (Result11 != "OK")
                {
                    throw new Exception(Result11);
                }
                if (!string.IsNullOrEmpty(e.Parameters.Split('|')[4]))
                {
                    string _PdfResult = ReportUtilities.CreateReport(_obj.UserName, _obj.Comp, e.Parameters.Split('|')[2].Split(',')[0], true
                                            , Server.MapPath(CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4]), Result12
                                             , txtCatchParamsAndValues.Text, HiddenP1.Value, HiddenP2.Value, IslemId, e.Parameters.Split('|')[3]);

                    if (!string.IsNullOrEmpty(_PdfResult))
                    {
                        throw new Exception(_PdfResult);
                    }

                    string[] arr = new string[] { IslemId, _obj.UserName };
                    services.NavFunctionsStatic(_obj.UserName, _obj.Comp, "50000.OnAfterWebReportSELECTED", arr);

                    Result21 = Session["WebMessage"].ToString();
                    Result22 = Session["InsertedVal"].ToString();

                    grid.JSProperties["cpProcType"] = "POSTMODE|" + Result11 + "|" + Result12 + "|" + Result21 + "|" + Result22;
                    grid.JSProperties["cpLink"] = CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4] + "/" + Result12 + ".PDF";
                }
                else
                {
                    grid.JSProperties["cpProcType"] = "OPENMODE";
                    grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];

                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("PdfMerge|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP12.Text, txtOBJF12.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string _res = DynamicUtilsV1.DynamicPagePdfMerge(_obj.Comp, _obj.UserName, e.Parameters.Split('|')[2], IslemId);
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = _res;
                grid.JSProperties["cpQueryStrings"] = "PDFMERGE";
                grid.JSProperties["cpIsLinked"] = true;

            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP3.Value) & !string.IsNullOrEmpty(txtMasterObject12.Text))
            {

                if (!string.IsNullOrEmpty(HiddenP3.Value)) values = HiddenP3.Value;
                if (!string.IsNullOrEmpty(HiddenP4.Value)) values += ";" + HiddenP4.Value;
                if (!string.IsNullOrEmpty(HiddenP5.Value)) values += ";" + HiddenP5.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
                if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            }
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid12.Table = DynamicUtilsV1.fillSubGrid(txtOBJP12.Text, txtOBJF12.Text, _obj.Comp, _obj.UserName, txtOBJP1.Text, txtOBJF1.Text, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid12.Table.Columns["ID"];
            this.dtgrid12.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid12";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + grid.ID.Substring(grid.ID.Length == 5 ? grid.ID.Length - 1 : grid.ID.Length - 2) + "Rows";


            grid.FilterExpression = DynamicUtilsV1.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;

            grid.DataBind();
            HiddenP3.Value = "";
            HiddenP4.Value = "";
            grid.FilterExpression = _filterexpression;
        }

        #endregion

        #region Grid13

        protected void menu13_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Contains("btnExcel"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid13.VisibleRowCount == 0)
                    {
                        gridExport13.WriteXlsToResponse();
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid13.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        gridExport13.WriteXlsToResponse();
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    gridExport13.WriteXlsToResponse();

            }
            else if (e.Item.Name.Contains("btnPdf"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid13.VisibleRowCount == 0)
                    {
                        DynamicUtilsV1.ExportPdf(gridExport13);
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid13.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        DynamicUtilsV1.ExportPdf(gridExport13);
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    DynamicUtilsV1.ExportPdf(gridExport13);

            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(OBJ13_pageId, OBJ13_fieldId, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid13, rows, _obj.UserName, _obj.Comp);

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }

        protected void grid13_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF13.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP13.Text, txtOBJF13.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();
            }
            else if (e.Parameters.Contains("DevexPopup|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP13.Text, txtOBJF13.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpDevexPopup"] = true;
                grid.JSProperties["cpSource"] = e.Parameters.Split('|')[2];
            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP13.Text, txtOBJF13.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("ReportButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP13.Text, txtOBJF13.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsWithNavReturnIslemId(grid, rows, _obj.UserName, _obj.Comp, "50000.OnBeforeWebReportSELECTED");
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string Result11 = Session["WebMessage"].ToString()
                       , Result12 = Session["InsertedVal"].ToString(), Result21 = string.Empty, Result22 = string.Empty;
                if (Result11 != "OK")
                {
                    throw new Exception(Result11);
                }
                if (!string.IsNullOrEmpty(e.Parameters.Split('|')[4]))
                {
                    string _PdfResult = ReportUtilities.CreateReport(_obj.UserName, _obj.Comp, e.Parameters.Split('|')[2].Split(',')[0], true
                                            , Server.MapPath(CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4]), Result12
                                             , txtCatchParamsAndValues.Text, HiddenP1.Value, HiddenP2.Value, IslemId, e.Parameters.Split('|')[3]);

                    if (!string.IsNullOrEmpty(_PdfResult))
                    {
                        throw new Exception(_PdfResult);
                    }

                    string[] arr = new string[] { IslemId, _obj.UserName };
                    services.NavFunctionsStatic(_obj.UserName, _obj.Comp, "50000.OnAfterWebReportSELECTED", arr);

                    Result21 = Session["WebMessage"].ToString();
                    Result22 = Session["InsertedVal"].ToString();

                    grid.JSProperties["cpProcType"] = "POSTMODE|" + Result11 + "|" + Result12 + "|" + Result21 + "|" + Result22;
                    grid.JSProperties["cpLink"] = CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4] + "/" + Result12 + ".PDF";
                }
                else
                {
                    grid.JSProperties["cpProcType"] = "OPENMODE";
                    grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];

                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("PdfMerge|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP13.Text, txtOBJF13.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string _res = DynamicUtilsV1.DynamicPagePdfMerge(_obj.Comp, _obj.UserName, e.Parameters.Split('|')[2], IslemId);
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = _res;
                grid.JSProperties["cpQueryStrings"] = "PDFMERGE";
                grid.JSProperties["cpIsLinked"] = true;

            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP3.Value) & !string.IsNullOrEmpty(txtMasterObject13.Text))
            {

                if (!string.IsNullOrEmpty(HiddenP3.Value)) values = HiddenP3.Value;
                if (!string.IsNullOrEmpty(HiddenP4.Value)) values += ";" + HiddenP4.Value;
                if (!string.IsNullOrEmpty(HiddenP5.Value)) values += ";" + HiddenP5.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
                if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            }
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid13.Table = DynamicUtilsV1.fillSubGrid(txtOBJP13.Text, txtOBJF13.Text, _obj.Comp, _obj.UserName, txtOBJP1.Text, txtOBJF1.Text, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid13.Table.Columns["ID"];
            this.dtgrid13.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid13";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + grid.ID.Substring(grid.ID.Length == 5 ? grid.ID.Length - 1 : grid.ID.Length - 2) + "Rows";

            grid.FilterExpression = DynamicUtilsV1.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;

            grid.DataBind();
            HiddenP3.Value = "";
            HiddenP4.Value = "";
            grid.FilterExpression = _filterexpression;

        }

        #endregion

        #region Grid14

        protected void menu14_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Contains("btnExcel"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid14.VisibleRowCount == 0)
                    {
                        gridExport14.WriteXlsToResponse();
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid14.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        gridExport14.WriteXlsToResponse();
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    gridExport14.WriteXlsToResponse();

            }
            else if (e.Item.Name.Contains("btnPdf"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid14.VisibleRowCount == 0)
                    {
                        DynamicUtilsV1.ExportPdf(gridExport14);
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid14.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        DynamicUtilsV1.ExportPdf(gridExport14);
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    DynamicUtilsV1.ExportPdf(gridExport14);

            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(OBJ14_pageId, OBJ14_fieldId, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid14, rows, _obj.UserName, _obj.Comp);

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }

        protected void grid14_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF14.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP14.Text, txtOBJF14.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();
            }
            else if (e.Parameters.Contains("DevexPopup|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP14.Text, txtOBJF14.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpDevexPopup"] = true;
                grid.JSProperties["cpSource"] = e.Parameters.Split('|')[2];
            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP14.Text, txtOBJF14.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("ReportButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP14.Text, txtOBJF14.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsWithNavReturnIslemId(grid, rows, _obj.UserName, _obj.Comp, "50000.OnBeforeWebReportSELECTED");
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string Result11 = Session["WebMessage"].ToString()
                       , Result12 = Session["InsertedVal"].ToString(), Result21 = string.Empty, Result22 = string.Empty;
                if (Result11 != "OK")
                {
                    throw new Exception(Result11);
                }
                if (!string.IsNullOrEmpty(e.Parameters.Split('|')[4]))
                {
                    string _PdfResult = ReportUtilities.CreateReport(_obj.UserName, _obj.Comp, e.Parameters.Split('|')[2].Split(',')[0], true
                                            , Server.MapPath(CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4]), Result12
                                             , txtCatchParamsAndValues.Text, HiddenP1.Value, HiddenP2.Value, IslemId, e.Parameters.Split('|')[3]);

                    if (!string.IsNullOrEmpty(_PdfResult))
                    {
                        throw new Exception(_PdfResult);
                    }

                    string[] arr = new string[] { IslemId, _obj.UserName };
                    services.NavFunctionsStatic(_obj.UserName, _obj.Comp, "50000.OnAfterWebReportSELECTED", arr);

                    Result21 = Session["WebMessage"].ToString();
                    Result22 = Session["InsertedVal"].ToString();

                    grid.JSProperties["cpProcType"] = "POSTMODE|" + Result11 + "|" + Result12 + "|" + Result21 + "|" + Result22;
                    grid.JSProperties["cpLink"] = CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4] + "/" + Result12 + ".PDF";
                }
                else
                {
                    grid.JSProperties["cpProcType"] = "OPENMODE";
                    grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];

                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("PdfMerge|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP14.Text, txtOBJF14.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string _res = DynamicUtilsV1.DynamicPagePdfMerge(_obj.Comp, _obj.UserName, e.Parameters.Split('|')[2], IslemId);
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = _res;
                grid.JSProperties["cpQueryStrings"] = "PDFMERGE";
                grid.JSProperties["cpIsLinked"] = true;

            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP3.Value) & !string.IsNullOrEmpty(txtMasterObject14.Text))
            {

                if (!string.IsNullOrEmpty(HiddenP3.Value)) values = HiddenP3.Value;
                if (!string.IsNullOrEmpty(HiddenP4.Value)) values += ";" + HiddenP4.Value;
                if (!string.IsNullOrEmpty(HiddenP5.Value)) values += ";" + HiddenP5.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
                if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            }
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid14.Table = DynamicUtilsV1.fillSubGrid(txtOBJP14.Text, txtOBJF14.Text, _obj.Comp, _obj.UserName, txtOBJP1.Text, txtOBJF1.Text, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid14.Table.Columns["ID"];
            this.dtgrid14.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid14";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + grid.ID.Substring(grid.ID.Length == 5 ? grid.ID.Length - 1 : grid.ID.Length - 2) + "Rows";

            grid.FilterExpression = DynamicUtilsV1.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;

            grid.DataBind();
            HiddenP3.Value = "";
            HiddenP4.Value = "";
            grid.FilterExpression = _filterexpression;

        }

        #endregion

        #region Grid15

        protected void menu15_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Contains("btnExcel"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid15.VisibleRowCount == 0)
                    {
                        gridExport15.WriteXlsToResponse();
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid15.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        gridExport15.WriteXlsToResponse();
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    gridExport15.WriteXlsToResponse();

            }
            else if (e.Item.Name.Contains("btnPdf"))
            {
                if (e.Item.Name.Contains("|"))
                {
                    if (grid15.VisibleRowCount == 0)
                    {
                        DynamicUtilsV1.ExportPdf(gridExport15);
                        return;
                    }
                    string[] _fnames = { e.Item.Name.Split('|')[1] };
                    object _ytk = grid15.GetRowValues(0, _fnames);
                    if (_ytk.ToString() == "1")
                        DynamicUtilsV1.ExportPdf(gridExport15);
                    else
                    {
                        MessageBox("You dont have permissions export to list");
                        return;
                    }
                }
                else
                    DynamicUtilsV1.ExportPdf(gridExport15);

            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(OBJ15_pageId, OBJ15_fieldId, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid15, rows, _obj.UserName, _obj.Comp);

                if (!Request.RawUrl.Contains("P1="))
                    Response.Redirect(Request.RawUrl + "&P1=" + HiddenP1.Value);
                else
                    Response.Redirect(Request.RawUrl);

            }
        }

        protected void grid15_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF14.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP15.Text, txtOBJF15.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                DynamicUtilsV1.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
                grid.JSProperties["cpIsUpdated"] = true;
                grid.JSProperties["cpMsg"] = Session["WebMessage"].ToString();
            }
            else if (e.Parameters.Contains("DevexPopup|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP15.Text, txtOBJF15.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpDevexPopup"] = true;
                grid.JSProperties["cpSource"] = e.Parameters.Split('|')[2];
            }
            else if (e.Parameters.Contains("SelectDirectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP15.Text, txtOBJF15.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                Session["IslemId"] = IslemId;
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("ReportButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP15.Text, txtOBJF15.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsWithNavReturnIslemId(grid, rows, _obj.UserName, _obj.Comp, "50000.OnBeforeWebReportSELECTED");
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string Result11 = Session["WebMessage"].ToString()
                       , Result12 = Session["InsertedVal"].ToString(), Result21 = string.Empty, Result22 = string.Empty;
                if (Result11 != "OK")
                {
                    throw new Exception(Result11);
                }
                if (!string.IsNullOrEmpty(e.Parameters.Split('|')[4]))
                {
                    string _PdfResult = ReportUtilities.CreateReport(_obj.UserName, _obj.Comp, e.Parameters.Split('|')[2].Split(',')[0], true
                                            , Server.MapPath(CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4]), Result12
                                             , txtCatchParamsAndValues.Text, HiddenP1.Value, HiddenP2.Value, IslemId, e.Parameters.Split('|')[3]);

                    if (!string.IsNullOrEmpty(_PdfResult))
                    {
                        throw new Exception(_PdfResult);
                    }

                    string[] arr = new string[] { IslemId, _obj.UserName };
                    services.NavFunctionsStatic(_obj.UserName, _obj.Comp, "50000.OnAfterWebReportSELECTED", arr);

                    Result21 = Session["WebMessage"].ToString();
                    Result22 = Session["InsertedVal"].ToString();

                    grid.JSProperties["cpProcType"] = "POSTMODE|" + Result11 + "|" + Result12 + "|" + Result21 + "|" + Result22;
                    grid.JSProperties["cpLink"] = CurrentFolderName(_obj.Comp) + "/" + e.Parameters.Split('|')[4] + "/" + Result12 + ".PDF";
                }
                else
                {
                    grid.JSProperties["cpProcType"] = "OPENMODE";
                    grid.JSProperties["cpLink"] = e.Parameters.Split('|')[2];
                }
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpQueryStrings"] = e.Parameters.Split('|')[3];
                grid.JSProperties["cpIsLinked"] = true;

            }
            else if (e.Parameters.Contains("PdfMerge|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = DynamicUtilsV1.GetDynamicWebPageObjects(txtOBJP15.Text, txtOBJF15.Text, _obj.UserName, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string IslemId = DynamicUtilsV1.ProcSelectedRowsReturnIslemId(grid, rows, _obj.UserName, _obj.Comp);
                if (string.IsNullOrEmpty(IslemId))
                {
                    throw new Exception("transaction failed");
                }
                string _res = DynamicUtilsV1.DynamicPagePdfMerge(_obj.Comp, _obj.UserName, e.Parameters.Split('|')[2], IslemId);
                grid.JSProperties["cpIslemId"] = IslemId;
                grid.JSProperties["cpLink"] = _res;
                grid.JSProperties["cpQueryStrings"] = "PDFMERGE";
                grid.JSProperties["cpIsLinked"] = true;

            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP3.Value) & !string.IsNullOrEmpty(txtMasterObject15.Text))
            {

                if (!string.IsNullOrEmpty(HiddenP3.Value)) values = HiddenP3.Value;
                if (!string.IsNullOrEmpty(HiddenP4.Value)) values += ";" + HiddenP4.Value;
                if (!string.IsNullOrEmpty(HiddenP5.Value)) values += ";" + HiddenP5.Value;
            }
            else
            {
                if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
                if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            }
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid15.Table = DynamicUtilsV1.fillSubGrid(txtOBJP15.Text, txtOBJF15.Text, _obj.Comp, _obj.UserName, txtOBJP1.Text, txtOBJF1.Text, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid15.Table.Columns["ID"];
            this.dtgrid15.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid15";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + grid.ID.Substring(grid.ID.Length == 5 ? grid.ID.Length - 1 : grid.ID.Length - 2) + "Rows";


            grid.FilterExpression = DynamicUtilsV1.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;

            grid.DataBind();
            HiddenP3.Value = "";
            HiddenP4.Value = "";
            grid.FilterExpression = _filterexpression;


        }
        #endregion

        protected void iframecallbackPanel_Callback(object sender, CallbackEventArgsBase e)
        {
            if (postURL.Contains("?"))
            {
                iframeView.Attributes.Add("Src", postURL + "GId=" + Request.QueryString["GId"] + "&" + postURLParameter + " =" + e.Parameter);
            }
            else
            {
                iframeView.Attributes.Add("Src", postURL + "?GId=" + Request.QueryString["GId"] + "&" + postURLParameter + " =" + e.Parameter);
            }


        }
        protected void grid_RowValidating(object sender, ASPxDataValidationEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            string sessionname = string.Empty;
            switch (gridI.ID)
            {
                case "grid1":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid1Rows";
                    break;
                case "grid2":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid2Rows";
                    break;
                case "grid3":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid3Rows";
                    break;
                case "grid4":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid4Rows";
                    break;
                case "grid5":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid5Rows";
                    break;
                case "grid6":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid6Rows";
                    break;
                case "grid7":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid7Rows";
                    break;
                case "grid8":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid8Rows";
                    break;
                case "grid9":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid9Rows";
                    break;
                case "grid10":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid10Rows";
                    break;
                case "grid11":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid11Rows";
                    break;
                case "grid12":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid12Rows";
                    break;
                case "grid13":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid13Rows";
                    break;
                case "grid14":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid14Rows";
                    break;
                case "grid15":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid15Rows";
                    break;
                default:
                    break;
            }

            if (e.NewValues["REDIT"] == null)
                e.NewValues["REDIT"] = 0;

            //DataRow[] rows2 = ((DataTable)Session[sessionname]).Select("[DX Type1]='DATA'");
            //foreach (DataRow row in rows2)
            //{
            //    if (e.NewValues[row["DX Field ID"].ToString()] == null)

            //        e.NewValues[row["DX Field ID"].ToString()] = "";
            //}

            DataRow[] rows1 = ((DataTable)Session[sessionname]).Select("[DX Type1]='DECIMAL' OR [DX Type2]='INTEGER'");
            foreach (DataRow row in rows1)
            {
                if (e.NewValues[row["DX Field ID"].ToString()] == null)
                    e.NewValues[row["DX Field ID"].ToString()] = 0;
            }


            if (gridI.ID == "grid2")
            {
                if (ViewState["OBJ2_Field"].ToString() == "J50011" | ViewState["OBJ2_Field"].ToString() == "J50031")
                {
                    if (e.NewValues["R0048"] != null)
                    {
                        string sorgu = "select [Personel Kodu] PERS,[Departman Kodu] DEPT,[Bolge Kodu]  BLG from [0C_50029_00_HAT_ARAC_KAYIT] where COMPANY='" + _obj.Comp + "' and [Arac Boyutu]='" + e.NewValues["R0048"].ToString() + "'";
                        DataTable dt = f.datatablegonder(sorgu);
                        e.NewValues["R0040"] = dt.Rows[0]["DEPT"].ToString();
                        e.NewValues["R0042"] = dt.Rows[0]["BLG"].ToString();
                        e.NewValues["R0046"] = dt.Rows[0]["PERS"].ToString();

                    }
                }
                else
                {

                    // MLH cost tarif girişini bozdu VAT % ile çakıştı
                    //if (e.NewValues["R0040"] != null)
                    //{
                    //    string sorgu = "select [Personel Kodu] PERS,[Departman Kodu] DEPT,[Bolge Kodu]  BLG from [0C_50029_00_HAT_ARAC_KAYIT] where COMPANY='" + _obj.Comp + "' and [Arac Boyutu]='" + e.NewValues["R0040"].ToString() + "'";
                    //    DataTable dt = f.datatablegonder(sorgu);
                    //    e.NewValues["R0043"] = dt.Rows[0]["DEPT"].ToString();
                    //    e.NewValues["R0042"] = dt.Rows[0]["BLG"].ToString();
                    //    e.NewValues["R0038"] = dt.Rows[0]["PERS"].ToString();

                    //}
                }


            }



        }
        protected void grid_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
        {

            ASPxGridView grid = (ASPxGridView)sender;
            GridViewCommandColumn col = grid.Columns["cmnd"] as GridViewCommandColumn;
            string perm = "1";

            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + grid.ID.Substring(grid.ID.Length == 5 ? grid.ID.Length - 1 : grid.ID.Length - 2) + "Rows";

            int Redt = Convert.ToInt32(((DataTable)Session[_sessionname]).Rows[0]["REDIT"].ToString());

            if (Redt == 1)
            {
                if (txtCatchParamsAndValues.Text.Contains("REDIT"))
                {
                    string[] fields = txtCatchParamsAndValues.Text.Split('|');

                    if (fields.Contains("REDIT;0")) perm = "0";

                }

            }
            else if (Redt > 1)
            {
                if (txtCatchParamsAndValues.Text.Contains("REDT" + Redt.ToString()))
                {
                    string[] fields = txtCatchParamsAndValues.Text.Split('|');

                    if (fields.Contains("REDT" + Redt.ToString() + ";0")) perm = "0";

                }
            }

            if (perm == "0")
            {
                try
                {
                    if (col.ShowNewButtonInHeader)
                        col.ShowNewButtonInHeader = false;
                }
                catch { }
                try
                {
                    foreach (GridViewCommandColumnCustomButton item in col.CustomButtons)
                    {
                        if (item.ID == "Clone") col.CustomButtons.Remove(item);

                    }


                }
                catch { }
            }


        }

        void WriteLog(string ex, int messageType)
        {
            string path = "";
            if (messageType == 1) //ERROR
            {
                path = HttpContext.Current.Server.MapPath("~/App_Data/ErrorLog.txt");
            }
            else if (messageType == 2) //INFO
            {
                path = HttpContext.Current.Server.MapPath("~/App_Data/Log.txt");
            }

            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += "Comp: " + _obj.Comp + " | User:" + _obj.UserName;
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += Request.RawUrl.ToString();
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex);
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;

            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(path, true))
            {
                writer.WriteLine(message);
                writer.Close();
            }
        }
        string AddValues(string _val1, string _val2)
        {
            if (string.IsNullOrEmpty(_val1))
                _val1 = _val2;
            else
                _val1 += "," + _val2;
            return _val1;
        }
        protected void Grid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            ASPxGridView Igrid = sender as ASPxGridView;
            // 20191206 eğitim için kommentlendi
            //if(e.Cell.GetType() == typeof(GridViewDataHyperLinkColumn))
            //{
            //    e.Cell.Text = e.CellValue.ToString().Replace(@"\", "/");
            //}
            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + Igrid.ID.Substring(Igrid.ID.Length == 5 ? Igrid.ID.Length - 1 : Igrid.ID.Length - 2) + "Rows";

            DataRow[] rows = ((DataTable)Session[_sessionname]).Select("[DX MouseOver Text]<>'' and [DX Field ID]='" + e.DataColumn.FieldName + "'");
            if (rows.Length > 0)
            {
                try
                {
                    string _val = Igrid.GetRowValues(e.VisibleIndex, rows[0]["DX MouseOver Text"].ToString()).ToString();
                    e.Cell.ToolTip = _val;
                }
                catch { e.Cell.ToolTip = string.Format("{0}", e.CellValue); }
            }
            else
                e.Cell.ToolTip = string.Format("{0}", e.CellValue);

            string _cssclass = string.Empty;
            object rback = null;

            if (e.Cell.CssClass.Contains("$"))
            {
                _cssclass = e.Cell.CssClass;
                _cssclass = _cssclass.TrimStart();
                _cssclass = _cssclass.TrimEnd();
                _cssclass = _cssclass.Replace("dx-nowrap", string.Empty).Replace("dxgv", string.Empty);
                _cssclass = _cssclass.TrimEnd();
                _cssclass = _cssclass.Replace("$", string.Empty);
                try { rback = e.GetValue(_cssclass); e.Cell.CssClass = "dx-nowrap dxgv"; } catch { }
                try { if (!string.IsNullOrEmpty(rback.ToString())) e.Cell.Style.Add(HtmlTextWriterStyle.BackgroundColor, rback.ToString() + " !important;"); } catch { }
            }


            try
            {
                if (e.DataColumn is GridViewDataHyperLinkColumn)
                {
                    if (string.IsNullOrEmpty(e.CellValue.ToString()))
                        e.Cell.Controls[0].Visible = false;

                }
            }
            catch
            {

            }





        }
        private void DilGetir()
        {
            txtDHT1.Text = l.dilgetirfordynamically(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, OBJ1_pageId + "_" + OBJ1_fieldId);
            rp1.HeaderText = txtDHT1.Text;

            txtDHT2.Text = l.dilgetirfordynamically(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, OBJ2_pageId + "_" + OBJ2_fieldId);
            rp2.HeaderText = txtDHT2.Text;

            txtDHT3.Text = l.dilgetirfordynamically(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, OBJ3_pageId + "_" + OBJ3_fieldId);
            rp3.HeaderText = txtDHT3.Text;

            txtDHT4.Text = l.dilgetirfordynamically(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, OBJ4_pageId + "_" + OBJ4_fieldId);
            rp4.HeaderText = txtDHT4.Text;

            txtDHT5.Text = l.dilgetirfordynamically(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, OBJ5_pageId + "_" + OBJ5_fieldId);
            rp5.HeaderText = txtDHT5.Text;

            txtDHT6.Text = l.dilgetirfordynamically(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, OBJ6_pageId + "_" + OBJ6_fieldId);
            rp6.HeaderText = txtDHT6.Text;

            txtDHT7.Text = l.dilgetirfordynamically(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, OBJ7_pageId + "_" + OBJ7_fieldId);
            rp7.HeaderText = txtDHT7.Text;

            txtDHT8.Text = l.dilgetirfordynamically(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, OBJ8_pageId + "_" + OBJ8_fieldId);
            rp8.HeaderText = txtDHT8.Text;

            txtDHT9.Text = l.dilgetirfordynamically(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, OBJ9_pageId + "_" + OBJ9_fieldId);
            rp9.HeaderText = txtDHT9.Text;

            txtDHT10.Text = l.dilgetirfordynamically(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, OBJ10_pageId + "_" + OBJ10_fieldId);
            rp10.HeaderText = txtDHT10.Text;

            txtDHT11.Text = l.dilgetirfordynamically(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, OBJ11_pageId + "_" + OBJ11_fieldId);
            rp11.HeaderText = txtDHT11.Text;

            txtDHT12.Text = l.dilgetirfordynamically(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, OBJ12_pageId + "_" + OBJ12_fieldId);
            rp12.HeaderText = txtDHT12.Text;

            txtDHT13.Text = l.dilgetirfordynamically(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, OBJ13_pageId + "_" + OBJ13_fieldId);
            rp13.HeaderText = txtDHT13.Text;

            txtDHT14.Text = l.dilgetirfordynamically(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, OBJ14_pageId + "_" + OBJ14_fieldId);
            rp14.HeaderText = txtDHT14.Text;

            txtDHT15.Text = l.dilgetirfordynamically(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, OBJ15_pageId + "_" + OBJ15_fieldId);
            rp15.HeaderText = txtDHT15.Text;
        }
        protected void grid_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {

            try
            {

                if (e.RowType == GridViewRowType.Data)
                {

                    ASPxGridView gridI = sender as ASPxGridView;
                    object rfont = null, rback = null
                                , rftsz = null
                                , rftwt = null
                                , rftst = null;



                    try { rfont = e.GetValue("RFONT"); } catch { }
                    try { rback = e.GetValue("RBACK"); } catch { }
                    try { rftsz = e.GetValue("RFTSZ"); } catch { }
                    try { rftwt = e.GetValue("RFTWT"); } catch { }
                    try { rftst = e.GetValue("RFTST"); } catch { }



                    try { if (!string.IsNullOrEmpty(rfont.ToString())) e.Row.Style.Add(HtmlTextWriterStyle.Color, rfont.ToString() + " !important;"); } catch { }
                    try { if (!string.IsNullOrEmpty(rback.ToString())) e.Row.Style.Add(HtmlTextWriterStyle.BackgroundColor, rback.ToString()); } catch { }
                    try { if (!string.IsNullOrEmpty(rftsz.ToString())) e.Row.Style.Add(HtmlTextWriterStyle.FontSize, rftsz.ToString() + "px" + " !important;"); } catch { }
                    try { if (!string.IsNullOrEmpty(rftwt.ToString())) e.Row.Style.Add(HtmlTextWriterStyle.FontWeight, Convert.ToInt32(rftwt.ToString()) == 0 ? "normal" : "bold"); } catch { }
                    try { if (!string.IsNullOrEmpty(rftst.ToString())) e.Row.Style.Add(HtmlTextWriterStyle.FontStyle, Convert.ToInt32(rftst.ToString()) == 0 ? "normal" : "italic"); } catch { }






                }
            }
            catch { }
        }

        string GetSessionName(string gridId)
        {
            string sessionname = string.Empty;
            switch (gridId)
            {
                case "grid1":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid1Rows";
                    break;
                case "grid2":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid2Rows";
                    break;
                case "grid3":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid3Rows";
                    break;
                case "grid4":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid4Rows";
                    break;
                case "grid5":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid5Rows";
                    break;
                case "grid6":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid6Rows";
                    break;
                case "grid7":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid7Rows";
                    break;
                case "grid8":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid8Rows";
                    break;
                case "grid9":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid9Rows";
                    break;
                case "grid10":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid10Rows";
                    break;
                case "grid11":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid11Rows";
                    break;
                case "grid12":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid12Rows";
                    break;
                case "grid13":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid13Rows";
                    break;
                case "grid14":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid14Rows";
                    break;
                case "grid15":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid15Rows";
                    break;
                case "grid25":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid25Rows";
                    break;
                default:
                    break;
            }

            return sessionname;
        }

        protected void rblDynamicList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string _url = "";
            _url = rblDynamicList.SelectedItem.Value.ToString();
            if (_url.Contains("?"))
                Response.Redirect(_url + "&GId=" + lblGId.Text);
            else
                Response.Redirect(_url + "?GId=" + lblGId.Text);
        }

        private string currentInvoiceNo = string.Empty;

        void PopTemizle()
        {
            currentInvoiceNo = string.Empty;
            txtSeriNo.Text = "";
            txtDovizKuru.Text = "";
            txtCurrency.Text = "";
            txtHariciBelgeNo.Text = "";
            txtFaturaEkMetin.Text = "";
            txtDate.Text = "";
            Pop.HeaderText = "";
            txtPassportNo.Text = "";
            txtIdentityNo.Text = "";
            txtCurrentInvoiceNo.Text = string.Empty;

        }
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            if (!KurKontrol())
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "onepagelist50", "msg001"));
                return;
            }

            Methods method = new Methods();
            List<object> ArrayValues = new List<object>();
            ArrayValues = grid1.GetSelectedFieldValues("R0011");
            string _param1 = string.Empty;
            foreach (var item in ArrayValues)
            {
                _param1 += item.ToString() + ",";
            }
            _param1 = _param1.Substring(0, _param1.Length - 1);

            CrystalReportOpen();

            string Drm = GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SeaLineInvoicePost§" + txtCurrentInvoiceNo.Text, _obj.Comp);
            if (Drm == "True")
            {

                if (!ClientScript.IsStartupScriptRegistered("JSSave"))
                {
                    ClientScript.RegisterStartupScript(GetType(), "JSSave", @"<script type=""text/javascript""> "
                    + "var popup = window.open('" + CurrentFolderName(_obj.Comp) + "/PDF/" + txtCurrentInvoiceNo.Text + ".pdf'); popup.focus(); " +
                    "</script>");
                }

            }

            string _filterexpression = grid1.FilterExpression;
            this.dtgrid1.Table = webObjectHandler.fillMainGrid(OBJ1_pageId, OBJ1_fieldId, _obj.Comp, _obj.UserName);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid1.Table.Columns["ID"];
            this.dtgrid1.Table.PrimaryKey = pricols;
            grid1.DataSourceID = "dtgrid1";
            grid1.KeyFieldName = "ID";


            //string _sessionname = "OPL10Grid" + grid1.ID.Substring(grid1.ID.Length - 1) + "Rows";
            //grid1.FilterExpression = DynamicUtilsV1.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));



            grid1.DataBind();

            grid1.FilterExpression = _filterexpression;
        }

        private void CrystalReportOpen()
        {
            ReportDocument objrptdoc = new ReportDocument();
            objrptdoc.Load(f.GetReportPath("INVOICE STENA.rpt", _obj.Comp));
            objrptdoc.ReportOptions.EnableSaveDataWithReport = false;
            objrptdoc.SetParameterValue("@DOCNO", txtCurrentInvoiceNo.Text);
            objrptdoc.SetParameterValue("@COMP", _obj.Comp);
            objrptdoc.SetParameterValue("DETAYTOPLU", 1);
            objrptdoc.SetParameterValue("TOPLUFATURAMETNI", "");
            objrptdoc.SetParameterValue("NORMALPROFORMA", 1);
            objrptdoc.SetParameterValue("EKMETIN", txtFaturaEkMetin.Text);
            objrptdoc.SetParameterValue("ONIZLEME", 1);
            objrptdoc.ExportToDisk(ExportFormatType.PortableDocFormat, Server.MapPath(CurrentFolderName(_obj.Comp) + "/PDF/" + txtCurrentInvoiceNo.Text + ".pdf"));


            if (objrptdoc != null)
            {
                objrptdoc.Close();
                objrptdoc.Dispose();
                objrptdoc = null;
            }
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            if (!KurKontrol())
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "onepagelist30", "msg002"));
                return;
            }

            Methods method = new Methods();
            List<object> ArrayValues = new List<object>();
            ArrayValues = grid1.GetSelectedFieldValues("R0011");
            string _param1 = string.Empty;
            bool ilk = true;
            foreach (var item in ArrayValues)
            {
                if (ilk)
                {
                    _param1 = item.ToString();
                    ilk = false;
                }
                else
                    _param1 += "," + item.ToString();

            }
            currentInvoiceNo = GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SealineBookingCreateDueInvoice§" +
                _param1 + "§" + txtSeriNo.Text + "§" + txtHariciBelgeNo.Text + "§" +
                txtFaturaEkMetin.Text + "§" + txtCurrency.Text + "§" + method.StringToDecimal(txtDovizKuru.Text).ToString() + "§" +
                method.DateToStringNAVType(txtDate.Text).ToString() + "§" + "1" + "§" + _obj.UserName + "§" + txtPassportNo.Text + "§" + txtIdentityNo.Text,
                _obj.Comp);

            Pop.HeaderText = currentInvoiceNo;

            txtCurrentInvoiceNo.Text = currentInvoiceNo;
            ClientScript.RegisterStartupScript(GetType(), "JSMusteriSave", @"<script type=""text/javascript""> "
                  + "window.open('../Raporlar/DynamicReport.aspx?ReportCode=RP055&DocNo=" + currentInvoiceNo + "&DETAYTOPLU=1&TOPLUFATURAMETNI=&EKMETIN=" + txtFaturaEkMetin.Text + "&NORMALPROFORMA=1&ONIZLEME=2&GId=" + Request.QueryString["GId"] + "');</script>");

        }

        bool KurKontrol()
        {
            bool _result = true;
            List<object> ArrayValues = new List<object>();
            ArrayValues = grid1.GetSelectedFieldValues("R0016").ToList();


            foreach (var mainItem in ArrayValues)
            {

                foreach (var subItem in ArrayValues)
                {

                    if (mainItem.ToString() != subItem.ToString())
                    {
                        _result = false;
                        return _result;
                    }
                }
            }

            return _result;
        }

        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            Web_GenericFunction_PortClient webFonksiyonlari;
            BasicHttpBinding _binding;
            ws_baglantı ws = new ws_baglantı();
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            EndpointAddress _endpoint = new EndpointAddress(new Uri(ws.sirketal(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }

        protected void btnTemizle_Click(object sender, EventArgs e)
        {
            PopTemizle();
        }

        protected void pnlDovizCinsi_Callback(object sender, CallbackEventArgsBase e)
        {

            txtCurrency.Text = txtR0016s.Text;
        }

        protected void pnlDovizKuru_Callback(object sender, CallbackEventArgsBase e)
        {
            string sorgu = "select convert(decimal(10,4)," + txtR0016s.Text + ") from [00_EXCHANGE RATES_01] where COMPANY='" + _obj.Comp + "' and datediff(day,[Starting Date],'" + txtDate.Date.Year + "-" + txtDate.Date.Month + "-" + txtDate.Date.Day + "')=0";
            string rate = f.tekbirsonucdonder(sorgu);

            txtDovizKuru.Text = rate;

        }

        protected void Pop_WindowCallback(object source, PopupWindowCallbackArgs e)
        {

            PopTemizle();
            Box2.FindItemByFieldName("serino").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0014");
            Box2.FindItemByFieldName("haricibelgeno").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0015");
            Box2.FindItemByFieldName("Tarih").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0016");
            Box2.FindItemByFieldName("CurrencyType").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0013");
            Box2.FindItemByFieldName("DovizKuru").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0020");
            Box2.FindItemByFieldName("Faturaekmetin").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0019");
            Box2.FindItemByFieldName("identityNo").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0021");
            Box2.FindItemByFieldName("PassportNo").Caption = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "R0022");

            btnCreate.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "F1001");
            btnPrint.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "P161110_O161150", "F1002");

            string _curr = string.Empty, _vals = string.Empty;
            string[] _arr = e.Parameter.Split('|');
            _curr = _arr[1];
            _vals = _arr[0];
            txtDate.Date = DateTime.Now.Date;
            txtCurrency.Text = _curr;
            string sorgu = "select convert(decimal(10,4)," + _curr + ") from [00_EXCHANGE RATES_01] where COMPANY='" + _obj.Comp + "' and datediff(day,[Starting Date],'" + txtDate.Date.Year + "-" + txtDate.Date.Month + "-" + txtDate.Date.Day + "')=0";
            string rate = f.tekbirsonucdonder(sorgu);

            txtDovizKuru.Text = rate;



            string result = string.Empty;

            result = GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName, "50000", "SealineBookingDueInvoicePreCheck§" + _vals, _obj.Comp);

            if (result == "10")
            {
                txtIdentityNo.Enabled = true;
                txtPassportNo.Enabled = false;
            }
            else if (result == "20")
            {
                txtIdentityNo.Enabled = false;
                txtPassportNo.Enabled = true;
            }
            else if (result == "99")
            {
                txtIdentityNo.Enabled = false;
                txtPassportNo.Enabled = false;
            }
            else
            {
                lblresult.Text = result;
                return;
            }

        }

        private void cmbDynamicCascade(object sender, CallbackEventArgsBase e)
        {
            if (string.IsNullOrEmpty(e.Parameter)) return;
            if (!e.Parameter.Contains("#")) return;
            string[] PMarray = e.Parameter.Split('#');
            string[] PSArray = PMarray[1].Split(';');
            string _valpar = PMarray[0];
            string DxSource = PSArray[0], ComboValue = PSArray[1], ComboText = PSArray[2]
                , ComboFilter = PSArray[3], ComboOrder = PSArray[4], DxCascFilter = PSArray[5];
            ASPxComboBox cmb = (ASPxComboBox)sender;

            string sorgu = "select " + ComboValue + ", " + ComboText + "  from [" + DxSource + "] where 1=1 ";

            if (!string.IsNullOrEmpty(ComboFilter))
                sorgu += " and " + ComboFilter + "='" + _obj.Comp + "'";
            sorgu += " and " + DxCascFilter + "='" + _valpar + "'";

            if (!string.IsNullOrEmpty(ComboOrder))
                sorgu += " order by " + ComboOrder;

            //Session["AddValPar1"] = " and " + DxCascFilter + "='" + _valpar + "'";


            f.FillAspxCombobox(cmb, sorgu, ComboText, ComboValue);


        }

        private void cmbDynamicCascade2(object sender, CallbackEventArgsBase e)
        {
            if (string.IsNullOrEmpty(e.Parameter)) return;
            if (!e.Parameter.Contains("#")) return;
            string[] PMarray = e.Parameter.Split('#');
            string[] PSArray = PMarray[1].Split(';');
            string _valpar = PMarray[0], _val2par = string.Empty;
            string DxSource = PSArray[0], ComboValue = PSArray[1], ComboText = PSArray[2]
                , ComboFilter = PSArray[3], ComboOrder = PSArray[4], DxCascFilter = PSArray[5]
                , MainSource = PSArray[6], MainValue = PSArray[7], MainFilter = PSArray[8];
            ASPxComboBox cmb = (ASPxComboBox)sender;
            string sorgu = string.Empty;

            sorgu = "select " + DxCascFilter + " from [" + MainSource + "] where 1=1 ";
            if (!string.IsNullOrEmpty(MainFilter))
                sorgu += " and " + MainFilter + "='" + _obj.Comp + "'";
            sorgu += " and " + MainValue + "='" + _valpar + "'";


            _val2par = f.tekbirsonucdonder(sorgu);

            sorgu = "select " + ComboValue + ", " + ComboText + "  from [" + DxSource + "] where 1=1 ";

            if (!string.IsNullOrEmpty(ComboFilter))
                sorgu += " and " + ComboFilter + "='" + _obj.Comp + "'";
            sorgu += " and " + DxCascFilter + "='" + _val2par + "'";

            if (!string.IsNullOrEmpty(ComboOrder))
                sorgu += " order by " + ComboOrder;


            f.FillAspxCombobox(cmb, sorgu, ComboText, ComboValue);

            //Session["AddValPar2"] = " and " + DxCascFilter + "='" + _val2par + "'";


        }

        private void cmbDynamicCascade3(object sender, CallbackEventArgsBase e)
        {
            if (string.IsNullOrEmpty(e.Parameter)) return;
            if (!e.Parameter.Contains("#")) return;

            string[] PMarray = e.Parameter.Split('#');
            string[] PSArray = PMarray[1].Split(';');
            string _valpar = PMarray[0], _val2par = string.Empty;
            string DxSource = PSArray[0], ComboValue = PSArray[1], ComboText = PSArray[2]
                , ComboFilter = PSArray[3], ComboOrder = PSArray[4], DxCascFilter = PSArray[5]
                , MainSource = PSArray[6], MainValue = PSArray[7], MainFilter = PSArray[8];
            ASPxComboBox cmb = (ASPxComboBox)sender;
            string sorgu = string.Empty;


            sorgu = "select " + DxCascFilter + " from [" + MainSource + "] where 1=1 ";
            if (!string.IsNullOrEmpty(MainFilter))
                sorgu += " and " + MainFilter + "='" + _obj.Comp + "'";
            sorgu += " and " + MainValue + "='" + _valpar + "'";

            _val2par = f.tekbirsonucdonder(sorgu);

            sorgu = "select " + ComboValue + ", " + ComboText + "  from [" + DxSource + "] where 1=1 ";

            if (!string.IsNullOrEmpty(ComboFilter))
                sorgu += " and " + ComboFilter + "='" + _obj.Comp + "'";


            if (!string.IsNullOrEmpty(ComboOrder))
                sorgu += " order by " + ComboOrder;

            f.FillAspxCombobox(cmb, sorgu, ComboText, ComboValue);

            if (_val2par != "" & _val2par != "NULL")
            {
                cmb.SelectedItem = cmb.Items.FindByValue(_val2par);

            }



        }

        private void cmbDynamicCascade4(object sender, CallbackEventArgsBase e)
        {


            if (string.IsNullOrEmpty(e.Parameter)) return;
            if (!e.Parameter.Contains("#")) return;

            string[] PMarray = e.Parameter.Split('#');
            string[] PSArray = PMarray[1].Split(';');

            string[] Pfields = PSArray[1].Split('|'); //


            string _valpar = PMarray[0];

            string DxSource = PSArray[0],
                    ComboValue = PSArray[1],
                    ComboText = PSArray[2],
                    ComboFilter = PSArray[3],
                    ComboOrder = PSArray[4],
                    DxCascFilter = PSArray[5],
                    MainSource = PSArray[6],
                    MainValue = PSArray[7],
                    MainFilter = PSArray[8];



            ASPxComboBox cmb = (ASPxComboBox)sender;
            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = DxSource;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = conn;
                    string[] spParams = ComboFilter.Split(',');

                    cmd.Parameters.AddWithValue("PRM1", PMarray[0].ToString());
                    for (int i = 1; i < spParams.Length; i++)
                    {
                        //if (spParams[i].ToString() == "s.GetValue()")
                        //    cmd.Parameters.AddWithValue("PRM" + (i + 1), PMarray[0].ToString());
                        //else
                        //    cmd.Parameters.AddWithValue("PRM" + (i + 1), spParams[i].ToString());


                        if (spParams[i].ToString() == "P1")
                            cmd.Parameters.AddWithValue("PRM" + (i + 1), HiddenP1.Value);
                        else if (spParams[i].ToString() == "P2")
                            cmd.Parameters.AddWithValue("PRM" + (i + 1), HiddenP2.Value);
                        else
                        {
                            try
                            {
                                string[] transparams = txtCatchParamsAndValues.Text.Split('|');
                                string _xary = Array.Find(transparams, element => element.StartsWith(spParams[i].ToString(), StringComparison.Ordinal));

                                if (_xary.Length > 0)
                                    cmd.Parameters.AddWithValue("PRM" + (i + 1), _xary.Split(';')[1]);
                                else
                                    cmd.Parameters.AddWithValue("PRM" + (i + 1), spParams[i].ToString());
                            }
                            catch
                            {

                                cmd.Parameters.AddWithValue("PRM" + (i + 1), spParams[i].ToString());
                            }

                        }

                    }

                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);


                    cmb.Items.Clear();
                    cmb.DataSourceID = null;
                    cmb.DataSource = dt;
                    cmb.TextField = ComboText;
                    cmb.ValueField = ComboValue;
                    cmb.DataBind();


                }
            }



        }

        private void cmbDynamicCascade6(ASPxComboBox cmb, ASPxGridView xgrid, ASPxGridViewEditorEventArgs e)
        {
            string _params = cmb.NullText;
            if (string.IsNullOrEmpty(_params)) return;
            if (!_params.Contains("#")) return;

            string[] PMarray = _params.Split('#');

            string DxSource = PMarray[1],
                    ProcParams = PMarray[2],
                    ComboValue = PMarray[3],
                    ComboText = PMarray[4];


            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = conn;
                    string[] spParams = ProcParams.Split(',');

                    string _sqlquery = DxSource;
                    bool ilk = true;
                    for (int i = 0; i < spParams.Length; i++)
                    {
                        if (ilk)
                        {
                            if (spParams[i].ToString() == "P1")
                                _sqlquery += " '" + HiddenP1.Value + "'";
                            else if (spParams[i].ToString() == "P2")
                                _sqlquery += " '" + HiddenP2.Value + "'";
                            else if (spParams[i].ToString() == "P3")
                                _sqlquery += " '" + HiddenP3.Value + "'";
                            else if (spParams[i].ToString() == "P4")
                                _sqlquery += " '" + HiddenP4.Value + "'";
                            else if (spParams[i].ToString() == "USER")
                                _sqlquery += " '" + _obj.UserName + "'";
                            else if (spParams[i].ToString() == "COMP")
                                _sqlquery += " '" + _obj.Comp + "'";
                            else if (spParams[i].Substring(0, 1) == "$")
                                _sqlquery += " '" + spParams[i].Replace("$", "") + "'";
                            else
                            {
                                try
                                {
                                    string[] transparams = txtCatchParamsAndValues.Text.Split('|');
                                    string _xary = Array.Find(transparams, element => element.StartsWith(spParams[i].ToString(), StringComparison.Ordinal));
                                    if (_xary.Length > 0)
                                        _sqlquery += " '" + _xary.Split(';')[1] + "'";
                                    else
                                    {
                                        object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, spParams[i].ToString());
                                        if (_val == null) _val = string.Empty;
                                        _sqlquery += " '" + _val.ToString() + "'";
                                    }
                                }
                                catch
                                {

                                    object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, spParams[i].ToString());
                                    if (_val == null) _val = string.Empty;
                                    _sqlquery += " '" + _val.ToString() + "'";
                                }


                            }
                            ilk = false;
                        }
                        else
                        {
                            if (spParams[i].ToString() == "P1")
                                _sqlquery += ", '" + HiddenP1.Value + "'";
                            else if (spParams[i].ToString() == "P2")
                                _sqlquery += ", '" + HiddenP2.Value + "'";
                            else if (spParams[i].ToString() == "P3")
                                _sqlquery += ", '" + HiddenP3.Value + "'";
                            else if (spParams[i].ToString() == "P4")
                                _sqlquery += ", '" + HiddenP4.Value + "'";
                            else if (spParams[i].ToString() == "USER")
                                _sqlquery += ", '" + _obj.UserName + "'";
                            else if (spParams[i].ToString() == "COMP")
                                _sqlquery += ", '" + _obj.Comp + "'";
                            else if (spParams[i].Substring(0, 1) == "$")
                                _sqlquery += ", '" + spParams[i].Replace("$", "") + "'";
                            else
                            {
                                try
                                {
                                    string[] transparams = txtCatchParamsAndValues.Text.Split('|');
                                    string _xary = Array.Find(transparams, element => element.StartsWith(spParams[i].ToString(), StringComparison.Ordinal));
                                    if (_xary.Length > 0)
                                        _sqlquery += ", '" + _xary.Split(';')[1] + "'";
                                    else
                                    {
                                        object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, spParams[i].ToString());
                                        if (_val == null) _val = string.Empty;
                                        _sqlquery += ", '" + _val.ToString() + "'";
                                    }
                                }
                                catch
                                {

                                    object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, spParams[i].ToString());
                                    if (_val == null) _val = string.Empty;
                                    _sqlquery += ", '" + _val.ToString() + "'";
                                }


                            }



                        }



                    }
                    cmd.CommandText = _sqlquery;




                    using (DataTable dt = new DataTable())
                    {
                        using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                        {
                            adp.Fill(dt);

                            cmb.Items.Clear();
                            cmb.DataSourceID = null;
                            cmb.DataSource = dt;
                            cmb.TextField = ComboText;
                            cmb.ValueField = ComboValue;
                            cmb.DataBind();
                        }
                    }

                }
            }

        }


        private void cmbDynamicCascade8(object sender, CallbackEventArgsBase e)
        {
            if (string.IsNullOrEmpty(e.Parameter)) return;
            if (!e.Parameter.Contains("|")) return;
            string[] PMarray = e.Parameter.Split('|');

            string DxSource = PMarray[0], ComboValue = PMarray[1], ComboText = PMarray[2];
            ASPxComboBox cmb = (ASPxComboBox)sender;


            string sorgu = "select " + ComboValue + ", " + ComboText + "  from [" + DxSource + "] where 1=1";

            sorgu += " and RCOMP='" + _obj.Comp + "'";
            sorgu += " order by " + ComboText;


            f.FillAspxCombobox(cmb, sorgu, ComboText, ComboValue);





        }
        protected void PnlHeader_Callback(object sender, CallbackEventArgsBase e)
        {

            string[] _panelSources = txtHeaderSource.Text.Split('§');
            string[] _panelSourceParams = txtHeaderParams.Text.Split('§');
            int lenght = _panelSources.Length - 1;
            for (int i = 0; i < lenght; i++)
            {
                if (i == 0)
                {
                    ltrHeader.Text += "<div class=\"item active\">";
                    ltrlPanelLinkler.Text += "<li data-target=\"#myCarousel\" data-slide-to=\"" + i.ToString() + "\" class=\"active\"></li>";
                }
                else
                {
                    pnlArrow.Visible = true;
                    ltrlPanelLinkler.Visible = true;
                    ltrHeader.Text += "<div class=\"item\">";
                    ltrlPanelLinkler.Text += "<li data-target=\"#myCarousel\" data-slide-to=\"" + i.ToString() + "\"></li>";
                }

                using (DataTable dt = new DataTable())
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {

                            cmd.Connection = conn;
                            cmd.CommandTimeout = 500;
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.Clear();

                            string[] _prms = _panelSourceParams[i].Split(';');
                            string prms = string.Empty;
                            bool ilk = true;
                            foreach (string str in _prms)
                            {
                                if (str == "PVAL" | str == "SLANG") continue;
                                string[] transparams = txtCatchParamsAndValues.Text.Split('|');
                                string _val1 = Array.Find(transparams, element => element.StartsWith(str, StringComparison.Ordinal));
                                if (ilk)
                                {
                                    prms = " " + _val1.Split(';')[1];
                                    ilk = false;
                                }
                                else
                                    prms += "," + _val1.Split(';')[1];
                            }

                            if (txtHeaderParams.Text.Contains("PVAL") & txtHeaderParams.Text.Contains("SLANG"))
                            {
                                prms += "," + txtOBJP1.Text;
                                prms += "," + Session["SelectedLanguage"];
                            }

                            cmd.CommandText = "EXEC " + _panelSources[i] + prms;

                            using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                            {
                                adp.Fill(dt);
                                if (dt.Rows.Count > 0)
                                    ltrHeader.Text += dt.Rows[0][0].ToString();
                            }
                        }
                    }

                }

                ltrHeader.Text += "</div>";
            }


        }

        protected void grid_HtmlEditFormCreated(object sender, ASPxGridViewEditFormEventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;
            if (txtCatchParamsAndValues.Text.Contains("RPBG"))
            {
                string[] fields = txtCatchParamsAndValues.Text.Split('|');
                var results = Array.FindAll(fields, s => s.Contains("RPBG"));
                grid.StylesPopup.EditForm.Header.BackColor = System.Drawing.Color.FromName(results[0].ToString().Split(';')[1]);
            }

        }

        protected void DevexPopup_WindowCallback(object source, PopupWindowCallbackArgs e)
        {
            if (e.Parameter.ToString().Contains("|"))
            {
                string _source = e.Parameter.ToString().Split('|')[0]
                    , _IslemId = e.Parameter.ToString().Split('|')[1];

                ASPxPopupControl _pop = (ASPxPopupControl)source;

                _pop.ContentUrl = "~/OnePageList51.aspx?GId=" + lblGId.Text + "&OJ1=" + _source.Replace("_", ";") + "&IslemId=" + _IslemId;


            }
        }

        protected void CallbackPanelGeneral_Callback(object source, CallbackEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Parameter)) return;
            ASPxCallback _clb = source as ASPxCallback;
            string[] array1 = e.Parameter.Split('@');
            string[] arrayParams = array1[3].Split('#');

            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                string outputSP = string.Empty;
                try
                {

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = array1[2];
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = conn;
                        conn.Open();
                        string[] spParams = array1[3].Split('#');

                        for (int i = 0; i < spParams.Length; i++)
                        {

                            if (spParams[i].ToString().Contains("&"))
                            {
                                string[] typeControl = spParams[i].Split('&');

                                if (typeControl[1] == "DATE")
                                {
                                    string[] parseDate = typeControl[0].Split('/');

                                    cmd.Parameters.AddWithValue("@PRM" + (i + 1), new DateTime(Convert.ToInt32(parseDate[2]), Convert.ToInt32(parseDate[0]), Convert.ToInt32(parseDate[1])));


                                }
                                else if (typeControl[1] == "DECIMAL")
                                {
                                    cmd.Parameters.AddWithValue("@PRM" + (i + 1), Convert.ToDecimal(typeControl[0]));

                                }
                                else if (typeControl[1] == "INTEGER")
                                {
                                    cmd.Parameters.AddWithValue("@PRM" + (i + 1), Convert.ToInt32(typeControl[0]));

                                }
                                else
                                {
                                    cmd.Parameters.AddWithValue("@PRM" + (i + 1), typeControl[0]);
                                }

                            }
                            else
                            {

                                if (spParams[i] == "P1")
                                    cmd.Parameters.AddWithValue("@PRM" + (i + 1), HiddenP1.Value);
                                else if (spParams[i] == "P2")
                                    cmd.Parameters.AddWithValue("@PRM" + (i + 1), HiddenP2.Value);
                                else if (spParams[i] == "SLANG")
                                    cmd.Parameters.AddWithValue("@PRM" + (i + 1), Session["SelectedLanguage"].ToString());
                                else if (spParams[i] == "COMP")
                                {
                                    cmd.Parameters.AddWithValue("@PRM" + (i + 1), _obj.Comp);
                                }
                                else
                                    cmd.Parameters.AddWithValue("@PRM" + (i + 1), spParams[i]);
                            }


                        }


                        DataTable dtTargetField = new DataTable();
                        using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                        {
                            adp.Fill(dtTargetField);
                        }
                        string _result = string.Empty;
                        if (dtTargetField.Rows.Count < 1)
                        {
                            _result = "NO DATA";
                        }
                        else
                        {
                            var _columns = dtTargetField.Columns.Cast<DataColumn>();
                            bool ilk = true;
                            foreach (var item in _columns)
                            {
                                if (ilk)
                                {
                                    _result = dtTargetField.Rows[0][item].ToString() + "," + item.ColumnName;
                                    ilk = false;
                                }
                                else
                                {
                                    _result += "|" + dtTargetField.Rows[0][item].ToString() + "," + item.ColumnName;
                                }
                            }
                        }
                        _clb.JSProperties["cpGId"] = array1[0];
                        txtCasc8Values.Text = _result;
                        e.Result = _result;

                    }
                }
                catch (Exception ex)
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    var line = frame.GetFileLineNumber();
                    WriteLog(ex.Message, 1);
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        protected void callbackPanel1_Callback(object source, CallbackEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Parameter)) return;
            string[] array1 = e.Parameter.Split('@');
            string[] arrayParams = array1[3].Split('#');

            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                string outputSP = string.Empty;
                try
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = array1[2];
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = conn;
                        conn.Open();
                        string[] spParams = array1[3].Split('#');
                        for (int i = 0; i < spParams.Length; i++)
                        {
                            if (spParams[i].ToString().Contains("&"))
                            {
                                string[] typeControl = spParams[i].Split('&');
                                if (typeControl[1] == "DATE")
                                {
                                    string[] parseDate = typeControl[0].Split('/');

                                    cmd.Parameters.AddWithValue("@PRM" + (i + 1), new DateTime(Convert.ToInt32(parseDate[2]), Convert.ToInt32(parseDate[0]), Convert.ToInt32(parseDate[1])));

                                }
                                else if (typeControl[1] == "DECIMAL")
                                    cmd.Parameters.AddWithValue("@PRM" + (i + 1), Convert.ToDecimal(typeControl[0]));
                                else if (typeControl[1] == "INTEGER")
                                    cmd.Parameters.AddWithValue("@PRM" + (i + 1), Convert.ToInt32(typeControl[0]));
                                else
                                    cmd.Parameters.AddWithValue("@PRM" + (i + 1), typeControl[0]);


                            }
                            else
                            {
                                if (spParams[i] == "P1")
                                    cmd.Parameters.AddWithValue("@PRM" + (i + 1), HiddenP1.Value);
                                else if (spParams[i] == "P2")
                                    cmd.Parameters.AddWithValue("@PRM" + (i + 1), HiddenP2.Value);
                                else if (spParams[i] == "SLANG")
                                    cmd.Parameters.AddWithValue("@PRM" + (i + 1), Session["SelectedLanguage"].ToString());
                                else
                                    cmd.Parameters.AddWithValue("@PRM" + (i + 1), spParams[i]);
                            }


                        }


                        DataTable dtTargetField = new DataTable();
                        using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                        {
                            adp.Fill(dtTargetField);
                        }



                        string[] targetfocusSplitter = array1[1].Split('&');
                        string[] _targetFields = targetfocusSplitter[0].Split(',');
                        string[] _focusFields = targetfocusSplitter[1].Split(',');



                        for (int k = 0; k < _targetFields.Length; k++)
                        {
                            string value = dtTargetField.Rows[0][_focusFields[k].ToString()].ToString();
                            outputSP += value.Replace(",", "") + ",";
                        }
                        outputSP = outputSP.Substring(0, outputSP.Length - 1);

                        e.Result = array1[0] + "#" + targetfocusSplitter[0] + "#" + outputSP;

                    }
                }
                catch (Exception ex)
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    var line = frame.GetFileLineNumber();
                }
                finally
                {
                    conn.Close();
                }
            }
        }
    }
}