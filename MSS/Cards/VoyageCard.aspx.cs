﻿using MSS1.Codes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.WSGeneric;
using System.ServiceModel;

namespace MSS1.Cards
{
    public partial class VoyageCard : Bases
    {
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        Methods method = new Methods();
        public ws_baglantı ws = new ws_baglantı();
        public string jobNo = string.Empty, jobTaskNo = string.Empty;
        SessionBase _obj;
        private string currentNo
        {
            get
            {
                string _curr = string.Empty;
                if (Request.QueryString["currentNO"] != null)
                    _curr = Request.QueryString["currentNO"].ToString();

                return _curr;
            }
        }
        private string currentTaskNo
        {
            get
            {
                string _curr = string.Empty;
                if (Request.QueryString["currentTaskNo"] != null)
                    _curr = Request.QueryString["currentTaskNo"].ToString();

                return _curr;
            }
        }


        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack || IsCallback)
            {
                return;
            }
            FillForm();
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }

        private void FillForm()
        {
            string query = string.Empty;
            query = "select * from [0_W140010]  where RCOMP='" + _obj.Comp + "' and R0011='" + currentNo + "'";
            if (currentTaskNo != "TOTAL")
            {
                query += " and R0013='" + currentTaskNo + "'";
            }
            DataTable dt = f.datatablegonder(query);
            if (dt.Rows.Count > 0)
            {
                txtVoyageNo.Text = dt.Rows[0]["R0012"].ToString();
                if (currentTaskNo != "TOTAL")
                {
                    txtPortArea.Text = dt.Rows[0]["R0022"].ToString() + "-" + dt.Rows[0]["R0023"].ToString();
                    txtPort.Text = dt.Rows[0]["R0020"].ToString() + "-" + dt.Rows[0]["R0021"].ToString();
                }


                if (Convert.ToDateTime(dt.Rows[0]["R0024"]).Year > 2000)
                    txtArrDate.Date = Convert.ToDateTime(dt.Rows[0]["R0024"]);

                if (Convert.ToDateTime(dt.Rows[0]["R0025"]).Year > 2000)
                    txtDepDate.Date = Convert.ToDateTime(dt.Rows[0]["R0025"]);

                if (Convert.ToDateTime(dt.Rows[0]["R0026"]).Year > 2000)
                    txtCallMonth.Date = Convert.ToDateTime(dt.Rows[0]["R0026"]);


                if (currentTaskNo == "TOTAL")
                    btnEdit.Visible = false;
                else
                    btnEdit.Visible = true;
            }
            else
            {

                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "voyagecard", "msg001"));
            }


        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            string d1 = string.Empty, d2 = string.Empty, d3 = string.Empty;
            if (!string.IsNullOrEmpty(txtArrDate.Text))
                d1 = string.Format("{0:dd.MM.yyyy}", txtArrDate.Date);
            if (!string.IsNullOrEmpty(txtDepDate.Text))
                d2 = string.Format("{0:dd.MM.yyyy}", txtDepDate.Date);
            if (!string.IsNullOrEmpty(txtCallMonth.Text))
                d3 = string.Format("{0:dd.MM.yyyy}", txtCallMonth.Date);

            GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "JobTaskKTFDateUpdate§" + currentNo + "§" + currentTaskNo + "§" + method.DateToStringNAVType(d1) + "§" + method.DateToStringNAVType(d2) + "§" + _obj.UserName + "§" + method.DateToStringNAVType(d3), _obj.Comp);

            FillForm();

            if (!ClientScript.IsStartupScriptRegistered("JSSave"))
            {
                ClientScript.RegisterStartupScript(GetType(), "JSSave", @"<script type=""text/javascript""> "
                + "window.parent.location='./../OnePageList.aspx?OJ1=P140010;O140002&OJ2=P140010;O140005&OJ3=P140010;O140006&OJ4=P140010;OJ9999&OJ5=P140010;OJ9999&OJ6=P140010;O140004&P1=" + currentNo + "&P2=" + currentTaskNo + "&GId=" + Request.QueryString["GId"] + "';</script>");
            }

        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }


    }
}