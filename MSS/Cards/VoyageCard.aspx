﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VoyageCard.aspx.cs" Inherits="MSS1.Cards.VoyageCard" %>


<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="text-align: left">
            <dx:ASPxFormLayout ID="Box1" runat="server" Width="100%">
                <Items>
                    <dx:LayoutGroup ShowCaption="False" ColCount="3">
                        <Items>
                            <dx:LayoutItem FieldName="Voyage No"  >
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxTextBox ID="txtVoyageNo" runat="server" Width="100%" Enabled="false"
                                            CssClass="carditemStyle">
                                        </dx:ASPxTextBox>
                                    </dx:LayoutItemNestedControlContainer> 
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                             <dx:LayoutItem FieldName="Port Area"  >
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxTextBox ID="txtPortArea" runat="server" Width="100%" Enabled="false"
                                            CssClass="carditemStyle">
                                        </dx:ASPxTextBox>
                                    </dx:LayoutItemNestedControlContainer> 
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                             <dx:LayoutItem FieldName="Arr. Date"  >
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                         <dx:ASPxDateEdit ID="txtArrDate"  Width="100%" runat="server" EditFormatString="dd.MM.yyyy" DisplayFormatString="dd.MM.yyyy"></dx:ASPxDateEdit>
                                    </dx:LayoutItemNestedControlContainer> 
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                             <dx:LayoutItem   FieldName="Call Month"  >
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>  
                                        <dx:ASPxDateEdit ID="txtCallMonth" Width="100%" runat="server"  EditFormatString="dd.MM.yyyy" DisplayFormatString="dd.MM.yyyy"> 
                                            </dx:ASPxDateEdit>
                                    </dx:LayoutItemNestedControlContainer> 
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                             <dx:LayoutItem FieldName="Port"  >
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                         <dx:ASPxTextBox ID="txtPort" runat="server" Width="100%" Enabled="false"
                                            CssClass="carditemStyle">
                                        </dx:ASPxTextBox>
                                    </dx:LayoutItemNestedControlContainer> 
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                            <dx:LayoutItem FieldName="Dep. Date"   >
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <dx:ASPxDateEdit ID="txtDepDate"  Width="100%" runat="server" EditFormatString="dd.MM.yyyy" DisplayFormatString="dd.MM.yyyy"> 
                                        </dx:ASPxDateEdit>
                                    </dx:LayoutItemNestedControlContainer> 
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                            <dx:LayoutItem  ShowCaption="False"   >
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer> 
                                    </dx:LayoutItemNestedControlContainer> 
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem> 
                             <dx:LayoutItem  ShowCaption="False"   >
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer> 
                                    </dx:LayoutItemNestedControlContainer> 
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                              <dx:LayoutItem  ShowCaption="False"   >
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer> 
                                    </dx:LayoutItemNestedControlContainer> 
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                             <dx:LayoutItem  ShowCaption="False"   >
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer> 
                                    </dx:LayoutItemNestedControlContainer> 
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                             <dx:LayoutItem  ShowCaption="False"   >
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer> 
                                    </dx:LayoutItemNestedControlContainer> 
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                              <dx:LayoutItem FieldName="btn"   HorizontalAlign="Right" ShowCaption="False"  >
                                <LayoutItemNestedControlCollection>
                                    <dx:LayoutItemNestedControlContainer>
                                        <asp:Button  id="btnEdit" runat="server" Text="SAVE" OnClick="btnEdit_Click"/>
                                    </dx:LayoutItemNestedControlContainer> 
                                </LayoutItemNestedControlCollection>
                            </dx:LayoutItem>
                        </Items>
                    </dx:LayoutGroup>
                </Items>
                
            </dx:ASPxFormLayout>
            
        </div>
    </form>
</body>
</html>
