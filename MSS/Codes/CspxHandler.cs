﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace CspxHandler
{
    public class CspxHandler : IHttpHandler,IReadOnlySessionState
    {
        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        
        {
            context.Response.ContentType = "text/plain";
            if (context.Request.RawUrl.Contains("?P=XST"))
            {
                context.Response.Redirect(context.Request.RawUrl.Replace("?P=XST", string.Empty));
                return;
            }

            if (context.Request.RawUrl.ToUpper().Contains(".PDF"))
            {
                if (context.Session["Sirket"] == null)
                { //context.Response.ContentType = "text/plain";
                  //context.Response.Write("Access Denied-Giriş izin verilmedi. lütfen giriş yapın.");
                  //  context.Response.Redirect("/Default.aspx");

                }
                else
                {
                    //string newUrl = context.Request.RawUrl.Replace(".txt", ".csk");
                    //context.Server.Transfer(context.Request.RawUrl);
                    context.Response.ContentType = "application/pdf";
                    context.Response.WriteFile(context.Request.PhysicalPath);
                }
            }
        }
    }
}