﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MSS1.Codes
{
    public class ReportUtilities
    {
        public static string CreateReport(string _user, string _comp, string _reportcode, bool IsSavePdf, string PdfPath, string filename
            ,string _transferfields, string P1, string P2, string IslemId, string _params)
        {
            string _result = string.Empty;
            try
            {
                using (ReportDocument crystalReport = new ReportDocument())
                {
                    fonksiyonlar f = new fonksiyonlar();
                    string _aspx = string.Empty, _crystal = string.Empty, _SessionVal = string.Empty;
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
                    {

                        using (DataTable dt = new DataTable())
                        {
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.CommandText = "Select * from [0C_50409_01_REPORT_DIRECTORY] where COMPANY=@Company and  ReportCode=@ReportCode";
                                cmd.CommandType = CommandType.Text;
                                cmd.CommandTimeout = 1000;
                                cmd.Connection = conn;
                                cmd.Parameters.Clear();
                                cmd.Parameters.AddWithValue("@Company", _comp);
                                cmd.Parameters.AddWithValue("@ReportCode", _reportcode);
                                using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                                {
                                    adp.Fill(dt);

                                    if (dt.Rows.Count > 0)
                                    {
                                        _crystal = dt.Rows[0]["CRYSTAL1"].ToString() + dt.Rows[0]["CRYSTAL2"].ToString();
                                        string[] _arr1 = _params.Split(',');
                                        string[] _arr2 = _crystal.Split(',');
                                        int _sayac = 0;
                                        crystalReport.Load(dt.Rows[0]["Report Path"].ToString());
                                        crystalReport.ReportOptions.EnableSaveDataWithReport = false;
                                        foreach (string item in _arr1)
                                        {
                                            if (string.IsNullOrEmpty(item)) continue;
                                            if (item == "USER")
                                                crystalReport.SetParameterValue(_arr2[_sayac], _user);
                                            else if (item == "COMP")
                                                crystalReport.SetParameterValue(_arr2[_sayac], _comp);
                                            else if (item == "P1")
                                                crystalReport.SetParameterValue(_arr2[_sayac], P1);
                                            else if (item == "P2")
                                                crystalReport.SetParameterValue(_arr2[_sayac], P2);
                                            else if (item == "ISLEMID")
                                                crystalReport.SetParameterValue(_arr2[_sayac], IslemId);
                                            else if (item.Substring(0, 1) == "#")
                                            {
                                                string[] transparams = _transferfields.Split('|');
                                                string _xary = Array.Find(transparams, element => element.StartsWith(item.Replace("#", string.Empty), StringComparison.Ordinal));
                                                if (_xary.Length > 0)
                                                    crystalReport.SetParameterValue(_arr2[_sayac], _xary.Split(';')[1]);
                                                else
                                                    crystalReport.SetParameterValue(_arr2[_sayac], "");
                                            }
                                            else
                                                crystalReport.SetParameterValue(_arr2[_sayac], item);
                                            _sayac++;
                                        }

                                    }

                                }
                            }
                        }

                    }
                    if (IsSavePdf)
                        crystalReport.ExportToDisk(ExportFormatType.PortableDocFormat, PdfPath + "/" + filename + ".PDF");

                    _result = string.Empty;

                }
            }
            catch (Exception ex)
            {
                _result = ex.Message;
            }

            return _result;
            
        }
    }
}