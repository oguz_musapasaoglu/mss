﻿using DevExpress.Data;
using DevExpress.Web;
using DevExpress.Web.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Collections;
using System.Text;
using DevExpress.Utils;
using MSS1.Codes;
using DevExpress.Pdf;
using MSS1.WSGeneric;
using System.ServiceModel;

public class DynamicUtilsV1
{
    Methods m = new Methods();
    ServiceUtilities services = new ServiceUtilities();
    static string pageId, objectId;
    public static int screenWidth, screenHeight;
    static string[] copiedFields = new string[150];
    static int copiedSayac = 0;
    static int columnindex = 1;
    string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
    fonksiyonlar f = new fonksiyonlar();

    public static string dbName(string _comp)
    {
        string _res = string.Empty;

        switch (_comp)
        {
            case "LAMKARA":
                _res = "LAMKARA.dbo.";
                break;
            case "MOBILIS":
                _res = "LAMKARA.dbo.";
                break;
            case "STENA":
                _res = "LAMKARA.dbo.";
                break;
            case "LUCENT":
                _res = "LAMKARA.dbo.";
                break;
            case "LIMANSAHA":
                _res = "LAMKARA.dbo.";
                break;
            case "MLH":
            case "MLHEG":
            case "MLHFR":
            case "MLHGE":
            case "MLHUA":
                _res = "MLH.dbo.";
                break;
            case "GAC":
                _res = "";
                break;
            case "LAM":
                _res = "";
                break;
            case "SINOTRANS":
                _res = "SINOTRANS.dbo.";
                break;
            case "LAMLOJ":
            case "LAMLOJEG":
            case "LAMLOJFR":
            case "LAMLOJGE":
            case "LAMLOJUA":
            case "LAMLOJUS":
                _res = "LAMLOJ.dbo.";
                break;
        }
        return _res;

    }

    public static string CurrentFolderName(string _comp)
    {
        try
        {
            if (_comp == "GAC" | _comp == "LAM")
                return "GACLAM";
            else
                return _comp;

        }
        catch (Exception)
        {
            return "";

        }

    }
    public DynamicUtilsV1()
    {

    }


    public static void FillDynamicColumns(List<object> GrdObjs, string PIds, string FIds, string _screenWidth,
    string _screenHeight, string _user, string _comp, string _selectedLang, string _objKey)
    {
        int i = 0;
        Methods m = new Methods();
        screenWidth = m.StringToInt(_screenWidth);
        screenHeight = m.StringToInt(_screenHeight);
        string _sessionname = string.Empty;
        string _cachekey = "sp_0C_50057_00_WEBPAGES_14" + "_" + PIds + "_" + FIds + "_" + _comp;
        List<CacheObject> _obj = CacheManager.Get(_cachekey, DateTime.Now);
        if (_obj.Count == 0 | CacheManager.CacheActive == 0)
        {
            using (DataSet ds = GetObjectSource(PIds, FIds, _user, _comp))
            {

                CacheManager.SetDynamic(ds, _cachekey, 24 * 60, 1);
                _obj = CacheManager.Get(_cachekey, DateTime.Now);

            }
        }


        foreach (ASPxGridView grd in GrdObjs)
        {

            copiedSayac = 0;
            columnindex = 1;
            pageId = PIds.Split(',')[i];
            objectId = FIds.Split(',')[i];
            prepareGridColumns(grd, _obj[0].objds.Tables[(i * 6) + 4], _obj[0].objds.Tables[(i * 6) + 2], int.Parse(grd.ID.Substring(grd.ID.Length == 5 ? grd.ID.Length - 1 : grd.ID.Length - 2)), _user, _comp, _selectedLang, _objKey);
            i++;
        }
    }


    public static void FillDynamicObjects(List<object> GrdObjs, List<object> MenuObjs, string PIds, string FIds, string _screenWidth,
        string _screenHeight, List<object> InsPObjs, List<object> CloneObjs, List<object> EdtPObjs
        , List<object> DelPObjs, List<object> PageParObjs, string PageDesc, string _user, string _comp, string _selectedLang)
    {
        int i = 0;
        Methods m = new Methods();
        screenWidth = m.StringToInt(_screenWidth);
        screenHeight = m.StringToInt(_screenHeight);
        string _sessionname = string.Empty, _HeaderSessionName = string.Empty;
        string _cachekey = "sp_0C_50057_00_WEBPAGES_15" + "_" + PIds + "_" + FIds + "_" + _comp;
        List<CacheObject> _obj = CacheManager.Get(_cachekey, DateTime.Now);
        if (_obj.Count == 0 | CacheManager.CacheActive == 0)
        {
            using (DataSet ds = GetObjectSourceV1(PIds, FIds, _user, _comp))
            {

                CacheManager.SetDynamic(ds, _cachekey, 24 * 60, 1);
                _obj = CacheManager.Get(_cachekey, DateTime.Now);

            }
        }

        foreach (ASPxGridView grd in GrdObjs)
        {
            #region page parameters link linkfield vs...
            ((TextBox)PageParObjs[(i * 8) + 0]).Text = _obj[0].objds.Tables[(i * 6) + 0].Rows[0]["LINK"].ToString();
            ((TextBox)PageParObjs[(i * 8) + 1]).Text = _obj[0].objds.Tables[(i * 6) + 0].Rows[0]["LINK FIELD"].ToString();
            ((TextBox)PageParObjs[(i * 8) + 2]).Text = _obj[0].objds.Tables[(i * 6) + 0].Rows[0]["Transferred Fields"].ToString();
            ((TextBox)PageParObjs[(i * 8) + 3]).Text = _obj[0].objds.Tables[(i * 6) + 0].Rows[0]["Collapse Dinamik Header"].ToString();
            ((TextBox)PageParObjs[(i * 8) + 4]).Text = _obj[0].objds.Tables[(i * 6) + 0].Rows[0]["Default Collapse State"].ToString();
            ((HiddenField)PageParObjs[(i * 8) + 5]).Value = _obj[0].objds.Tables[(i * 6) + 0].Rows[0]["PrimaryField"].ToString();
            ((TextBox)PageParObjs[(i * 8) + 6]).Text = _obj[0].objds.Tables[(i * 6) + 0].Rows[0]["MasterObject"].ToString();
            ((TextBox)PageParObjs[(i * 8) + 7]).Text = _obj[0].objds.Tables[(i * 6) + 0].Rows[0]["Objects To Be Refreshed"].ToString();
            _sessionname = PageDesc + grd.ID.Substring(grd.ID.Length == 5 ? grd.ID.Length - 1 : grd.ID.Length - 2) + "Rows";
            HttpContext.Current.Session[_sessionname] = _obj[0].objds.Tables[(i * 6) + 4];
            #endregion
            copiedSayac = 0;
            columnindex = 1;
            pageId = PIds.Split(',')[i];
            objectId = FIds.Split(',')[i];
            preparegridHeader(grd, (ASPxMenu)MenuObjs[i], grd.ClientInstanceName, _obj[0].objds.Tables[(i * 6) + 0], _selectedLang, _comp, _user);
            preparegridGroup(grd, _obj[0].objds.Tables[(i * 6) + 1]);
            preparegridCaption(grd, _obj[0].objds.Tables[(i * 6) + 2]);
            preparegridSearch(grd, _obj[0].objds.Tables[(i * 6) + 3]);
            preparegridFooter(grd, _obj[0].objds.Tables[(i * 6) + 5]);
            preparePageParams(grd, _obj[0].objds.Tables[(i * 6) + 4], _obj[0].objds.Tables[(i * 6) + 2], int.Parse(grd.ID.Substring(grd.ID.Length == 5 ? grd.ID.Length - 1 : grd.ID.Length - 2)), (TextBox)InsPObjs[i], (TextBox)CloneObjs[i], (TextBox)EdtPObjs[i], (TextBox)DelPObjs[i]);
            PrepareMainGridOptions(grd, _obj[0].objds.Tables[(i * 6) + 0], _obj[0].objds.Tables[(i * 6) + 4]);
            i++;
        }
    }

    private static DataSet GetObjectSource(string PIds, string FIds, string _user, string _comp)
    {
        using (DataSet ds = new DataSet())
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "[sp_0C_50057_00_WEBPAGES_14]";
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@PageIds", PIds);
                        cmd.Parameters.AddWithValue("@ObjectIds", FIds);
                        cmd.Parameters.AddWithValue("@Sirket", _comp);
                        cmd.Parameters.AddWithValue("@UserName", _user);
                        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                        {
                            adapter.Fill(ds);
                        }

                    }
                }
            }
            catch
            {

            }
            return ds;
        }
    }

    private static DataSet GetObjectSourceV1(string PIds, string FIds, string _user, string _comp)
    {
        using (DataSet ds = new DataSet())
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "[sp_0C_50057_00_WEBPAGES_15]";
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@PageIds", PIds);
                        cmd.Parameters.AddWithValue("@ObjectIds", FIds);
                        cmd.Parameters.AddWithValue("@Sirket", _comp);
                        cmd.Parameters.AddWithValue("@UserName", _user);
                        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                        {
                            adapter.Fill(ds);
                        }

                    }
                }
            }
            catch
            {

            }
            return ds;
        }
    }

    private static void preparegridHeader(ASPxGridView grd, ASPxMenu menu, string gridId, DataTable dth, string _selectedLang, string _comp, string _user)
    {
        menu.Items.Clear();
        var query = from p in dth.AsEnumerable()
                    where p.Field<string>("DX Field ID") == "H0000" & p.Field<int>("DX Display Order") > 0
                    select new
                    {
                        FieldId = p.Field<string>("DX Field ID"),
                        DxdisplayOrder = p.Field<int>("DX Display Order"),
                        dxWidth = p.Field<int>("DX Width")
                    };
        var query2 = from p in dth.AsEnumerable()
                     where p.Field<string>("DX Field ID") != "H0000" & p.Field<int>("DX Display Order") > 0
                     orderby p.Field<int>("DX Display Order")
                     select new
                     {
                         ObjeID = p.Field<string>("DX Obje ID"),
                         FieldId = p.Field<string>("DX Field ID"),
                         DxdisplayOrder = p.Field<int>("DX Display Order"),
                         Type1 = p.Field<string>("DX Type1"),
                         Type2 = p.Field<string>("DX Type2"),
                         DxSource = p.Field<string>("DX Source"),
                         DxFilter = p.Field<string>("DX Filter"),
                         Sl = p.Field<string>(_selectedLang),
                         dxWidth = p.Field<int>("DX Width"),
                         dxTrFlVw = p.Field<string>("DX TrFlVw"),
                         dxPath = p.Field<string>("DX Path"),
                         dxpfield = p.Field<string>("DX Permision Field"),
                         dxppfr = p.Field<byte>("DX Caption Filter"),
                         dxPath2 = p.Field<string>("DX Path2"),
                         dxGroupParent = p.Field<string>("DX Casc Target")
                     };
        if (query.Count() == 0) return;

        if (query.FirstOrDefault().DxdisplayOrder > 0)
        {
            query2.ToList().ForEach(m =>
            {
                menu.Items.Add(createMenuItem(m.Type1, m.Type2, m.Sl, m.FieldId, m.DxSource, gridId, m.DxFilter, m.dxTrFlVw, m.dxPath, m.dxpfield, _comp, _user, m.dxWidth, m.dxPath2, m.dxGroupParent, m.ObjeID));
            });
        }



        int headerWidth = query.FirstOrDefault().dxWidth;
        grd.Width = Unit.Percentage(headerWidth);

        //DataTable _dtTemp = dth.Copy();
        //if (_dtTemp.Select("[DX Field ID]<>'H0000' and [DX Display Order]>0").Count() > 0)
        //{
        //    if (dth.Select("[DX Field ID]='H0000'")[0]["DX Display Order"].ToString() != "0")
        //    {
        //        _dtTemp = _dtTemp.Select("[DX Field ID]<>'H0000' and [DX Display Order]>0").CopyToDataTable();
        //        _dtTemp.DefaultView.Sort = "[DX Display Order]";
        //        _dtTemp = _dtTemp.DefaultView.ToTable();
        //        foreach (DataRow row in _dtTemp.Rows)
        //        {
        //            menu.Items.Add(createMenuItem(row["DX Type1"].ToString(), row["DX Type2"].ToString(),
        //                row[_selectedLang].ToString(),
        //                row["DX Field ID"].ToString(), row["DX Source"].ToString(), gridId, row["DX Filter"].ToString()));
        //        }
        //    }
        //}


        //int headerWidth = Convert.ToInt32(dth.Select("[DX Field ID]='H0000'")[0]["DX Width"].ToString());

        //grd.Width = Unit.Percentage(headerWidth);


    }

    public static void preparePageRadioList(ASPxRadioButtonList rbl, string _pageId, string _fieldId, string _user, string _comp, string _selectedLang)
    {
        using (DataTable dt = GetRadioListObjectSource(_pageId, _fieldId, _user, _comp))
        {

            if (dt.Rows.Count > 0)
            {
                rbl.Visible = true;
                foreach (DataRow item in dt.Rows)
                {
                    ListEditItem li = new ListEditItem(item[_selectedLang].ToString(), item["DX Path"].ToString());

                    rbl.Items.Add(li);
                    if (item["DX Source"].ToString() != "")
                    {

                        rbl.Items[rbl.Items.Count - 1].ImageUrl = "images/" + item["DX Source"].ToString();
                        rbl.Items[rbl.Items.Count - 1].Text = "";
                    }


                    if (item["DX Type2"].ToString() == "SELECTED")
                    {
                        rbl.SelectedIndex = rbl.Items.Count - 1;
                    }
                }

            }
            else
            {
                rbl.Visible = false;
            }
        }

    }

    /* UĞURCAN SELMAN BEYİN İSTEĞİYLE L3'DEN ALAN YENİ RADIO BUTTON GELİŞTİRMESİ YAPILDI */
    public static void preparePageRadioListNew(ASPxRadioButtonList rbl, string _pageId, string _user, string _comp, string _selectedLang)
    {
        using (DataTable dt = GetRadioListObjectSourceNew(_pageId, _comp))
        {

            if (dt.Rows.Count > 0)
            {
                rbl.Visible = true;
                foreach (DataRow item in dt.Rows)
                {
                    ListEditItem li = new ListEditItem(item[_selectedLang].ToString(), item["DX Path"].ToString());

                    rbl.Items.Add(li);
                    if (item["DX Source"].ToString() != "")
                    {

                        rbl.Items[rbl.Items.Count - 1].ImageUrl = "images/" + item["DX Source"].ToString();
                        rbl.Items[rbl.Items.Count - 1].Text = "";
                    }


                    if (item["DX Path"].ToString().Contains(_pageId))
                    {
                        rbl.SelectedIndex = rbl.Items.Count - 1;
                    }
                }

            }
            else
            {
                rbl.Visible = false;
            }
        }

    }


    private static void PrepareMainGridOptions(ASPxGridView grid, DataTable dtHeader, DataTable dtRows)
    {

        DataRow[] rowsh = dtHeader.Select("[DX Field ID]='H0000'");
        int scroltype = Convert.ToInt32(rowsh[0]["SCROLL TYPE"].ToString());
        int rowcount = Convert.ToInt32(rowsh[0]["ROW COUNT"].ToString());
        int height = Convert.ToInt32(rowsh[0]["Height"].ToString());
        string sumfield = rowsh[0]["SUM FIELD"].ToString();
        int IsMenu = Convert.ToInt32(rowsh[0]["Is Menu"].ToString());
        int IsFroozen = Convert.ToInt32(rowsh[0]["Froze Column Order"].ToString());
        int PopupColumnCSize = Convert.ToInt32(rowsh[0]["Edit Popup Column Count"].ToString());
        int EditPopupWidth = Convert.ToInt32(rowsh[0]["Edit Popup Width"]);
        grid.SettingsDataSecurity.AllowReadUnlistedFieldsFromClientApi = DefaultBoolean.True;

        if (scroltype == 0)
        {

            grid.Settings.VerticalScrollBarMode = ScrollBarMode.Visible;
            grid.Settings.VerticalScrollableHeight = height == 0 ? 350 : height;
            grid.Settings.VerticalScrollableHeight = Convert.ToInt32((double)screenHeight * ((double)height / 100));
            grid.SettingsPager.Mode = GridViewPagerMode.EndlessPaging;
            grid.SettingsPager.PageSize = rowcount == 0 ? 20 : rowcount;
            grid.SettingsPager.ShowSeparators = false;

        }
        else if (scroltype == 2)
        {
            grid.SettingsPager.Mode = GridViewPagerMode.ShowPager;
            grid.SettingsPager.PageSize = rowcount == 0 ? 20 : rowcount;
        }
        if (IsMenu > 0)
        {
            //grid.EnableTheming = false;
            grid.Styles.Cell.Font.Bold = true;
            grid.Styles.Cell.Font.Size = FontUnit.Point(10);

        }

        grid.KeyFieldName = "ID";
        grid.Styles.Header.Wrap = DevExpress.Utils.DefaultBoolean.True;
        grid.Settings.HorizontalScrollBarMode = ScrollBarMode.Auto;
        grid.Width = Unit.Percentage(100);
        grid.SettingsText.PopupEditFormCaption = " ";



        string _filter = DefaultSearchGrid(dtRows.Select("[DX Default Search]<>'' AND [DX Display Order]>0"));
        if (!string.IsNullOrEmpty(_filter)) grid.FilterExpression = _filter;

        try
        {
            var queryr1 = from p in dtRows.AsEnumerable()
                          where string.IsNullOrEmpty(p.Field<string>("DX Default OrderBy Type")) == false & p.Field<int>("DX Default OrderBy Sequency") > 0
                          orderby p.Field<int>("DX Default OrderBy Sequency")
                          select new
                          {
                              DxdisplayOrder = p.Field<int>("DX Display Order"),
                              fieldId = p.Field<string>("DX Field ID"),
                              defaultorderbytype = p.Field<string>("DX Default OrderBy Type")
                          };

            queryr1.ToList().ForEach(m =>
                        {
                            grid.SortBy(grid.Columns[m.fieldId], m.defaultorderbytype == "ASC" ? DevExpress.Data.ColumnSortOrder.Ascending : DevExpress.Data.ColumnSortOrder.Descending);

                        });


        }
        catch
        {


        }

        if (PopupColumnCSize > 0)
        {
            grid.SettingsEditing.Mode = GridViewEditingMode.PopupEditForm;
            grid.SettingsEditing.EditFormColumnCount = PopupColumnCSize;
            grid.SettingsEditing.UseFormLayout = true;
            grid.SettingsEditing.NewItemRowPosition = GridViewNewItemRowPosition.Top;
            grid.SettingsPopup.EditForm.Modal = true;
            grid.SettingsPopup.CustomizationWindow.CloseOnEscape = AutoBoolean.True;
            grid.SettingsPopup.EditForm.ShowFooter = false;
            if (EditPopupWidth > 0)
                grid.SettingsPopup.EditForm.Width = Unit.Pixel(EditPopupWidth);
            else
                grid.SettingsPopup.EditForm.Width = Unit.Pixel(800);
            grid.SettingsPopup.EditForm.AllowResize = true;
            grid.SettingsPopup.EditForm.HorizontalAlign = PopupHorizontalAlign.WindowCenter;
            grid.SettingsPopup.EditForm.VerticalAlign = PopupVerticalAlign.WindowCenter;
        }
        else if (PopupColumnCSize < 0)
        {
            grid.SettingsEditing.Mode = GridViewEditingMode.Batch;
            grid.SettingsEditing.BatchEditSettings.EditMode = GridViewBatchEditMode.Row;
            grid.SettingsEditing.BatchEditSettings.StartEditAction = GridViewBatchStartEditAction.FocusedCellClick;
            grid.SettingsEditing.BatchEditSettings.HighlightDeletedRows = true;
            grid.SettingsEditing.BatchEditSettings.KeepChangesOnCallbacks = DefaultBoolean.True;
            grid.Settings.ShowStatusBar = GridViewStatusBarMode.Hidden;
        }


        // grid.HtmlCommandCellPrepared += Grid_HtmlCommandCellPrepared;
        grid.SettingsCookies.Enabled = true;
        grid.SettingsCookies.Version = "V20180626";
        grid.SettingsCookies.CookiesID = pageId + "_" + objectId;
        grid.SettingsCookies.StoreSearchPanelFiltering = false;
        grid.SettingsCookies.StoreColumnsHierarchy = false;
        grid.SettingsCookies.StoreControlWidth = false;
        grid.SettingsCookies.StorePaging = false;
        grid.SettingsCookies.StoreFiltering = false;
        grid.SettingsCookies.StoreGroupingAndSorting = true;
        grid.SettingsCookies.StoreColumnsVisiblePosition = false;
        grid.SettingsCookies.StoreColumnsWidth = false;


        if (IsFroozen > 0)
        {
            grid.SettingsBehavior.AllowDragDrop = false;

        }


        //grup summary ekliyor
        try
        {

            var queryr2 = from p in dtRows.AsEnumerable()
                          where string.IsNullOrEmpty(p.Field<string>("DX Grouping Desc Type")) == false & p.Field<int>("DX Display Order") > 0
                          orderby p.Field<int>("DX Default OrderBy Sequency")
                          select new
                          {
                              fieldId = p.Field<string>("DX Field ID"),
                              desctext = p.Field<string>("DX Grouping Desc Text"),
                              descnumberformat = p.Field<string>("DX Grouping Desc Number Format"),
                              desctype = p.Field<string>("DX Grouping Desc Type"),
                              descstart = p.Field<decimal>("DX Grouping Desc Start"),
                              descend = p.Field<decimal>("DX Grouping Desc Finish"),
                              descplace = p.Field<int>("DX Grouping Desc Place")
                          };
            grid.TotalSummary.Clear();
            grid.GroupSummary.Clear();
            ASPxSummaryItem item = new ASPxSummaryItem();
            ASPxSummaryItem Titem = new ASPxSummaryItem();
            queryr2.ToList().ForEach(m =>
            {
                switch (m.descplace)
                {
                    case 0:
                        item = new ASPxSummaryItem();
                        item.FieldName = m.fieldId;
                        item.DisplayFormat = m.desctext + m.descnumberformat;

                        switch (m.desctype)
                        {
                            case "Avarage":
                                item.SummaryType = SummaryItemType.Average;
                                break;
                            case "Count":
                                item.SummaryType = SummaryItemType.Count;
                                break;
                            case "Max":
                                item.SummaryType = SummaryItemType.Max;
                                break;
                            case "Min":
                                item.SummaryType = SummaryItemType.Min;
                                break;
                            case "Sum":
                                item.SummaryType = SummaryItemType.Sum;
                                break;
                        }
                        grid.GroupSummary.Add(item);
                        break;
                    case 1:
                        item = new ASPxSummaryItem();
                        item.FieldName = m.fieldId;
                        item.DisplayFormat = m.descnumberformat;

                        switch (m.desctype)
                        {
                            case "Avarage":
                                item.SummaryType = SummaryItemType.Average;
                                break;
                            case "Count":
                                item.SummaryType = SummaryItemType.Count;
                                break;
                            case "Max":
                                item.SummaryType = SummaryItemType.Max;
                                break;
                            case "Min":
                                item.SummaryType = SummaryItemType.Min;
                                break;
                            case "Sum":
                                item.SummaryType = SummaryItemType.Sum;
                                break;
                        }
                        grid.TotalSummary.Add(item);
                        break;
                    case 2:
                        item = new ASPxSummaryItem();
                        Titem = new ASPxSummaryItem();
                        item.FieldName = m.fieldId;
                        Titem.FieldName = m.fieldId;
                        item.DisplayFormat = m.desctext + m.descnumberformat;
                        Titem.DisplayFormat = m.descnumberformat;

                        switch (m.desctype)
                        {
                            case "Avarage":
                                item.SummaryType = SummaryItemType.Average;
                                Titem.SummaryType = SummaryItemType.Average;
                                break;
                            case "Count":
                                item.SummaryType = SummaryItemType.Count;
                                Titem.SummaryType = SummaryItemType.Count;
                                break;
                            case "Max":
                                item.SummaryType = SummaryItemType.Max;
                                Titem.SummaryType = SummaryItemType.Max;
                                break;
                            case "Min":
                                item.SummaryType = SummaryItemType.Min;
                                Titem.SummaryType = SummaryItemType.Min;
                                break;
                            case "Sum":
                                item.SummaryType = SummaryItemType.Sum;
                                Titem.SummaryType = SummaryItemType.Sum;
                                break;
                        }
                        grid.GroupSummary.Add(item);
                        grid.TotalSummary.Add(Titem);
                        break;
                }
                grid.SummaryText = "";
            });

        }
        catch { }


    }


    private static DataTable GetRadioListObjectSource(string pageId, string fieldId, string _user, string _comp)
    {
        string _cachekey = "sp_0C_50032_00_WEB PAGE CAPTIONS_RADIO LINKS" + "_" + pageId + "_" + fieldId + "_" + _comp;
        List<CacheObject> _obj = CacheManager.Get(_cachekey, DateTime.Now);
        if (_obj.Count == 0 | CacheManager.CacheActive == 0)
        {
            using (DataTable dt = new DataTable())
            {
                try
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = conn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "[sp_0C_50032_00_WEB PAGE CAPTIONS_RADIO LINKS]";
                            cmd.Parameters.Clear();
                            cmd.Parameters.AddWithValue("@PageId", pageId);
                            cmd.Parameters.AddWithValue("@ObjectId", fieldId);
                            cmd.Parameters.AddWithValue("@Sirket", _comp);
                            cmd.Parameters.AddWithValue("@USER", _user);
                            using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                            {
                                adapter.Fill(dt);
                                CacheManager.SetDynamic(dt, _cachekey, 24 * 60, 1);
                                _obj = CacheManager.Get(_cachekey, DateTime.Now);
                            }

                        }
                    }
                }
                catch
                {

                }

            }
        }

        return _obj[0].obj;

    }
    private static DataTable GetRadioListObjectSourceNew(string pageId, string _comp)
    {
        string _cachekey = "sp_0C_50032_00_WEB PAGE CAPTIONS_RADIO LINKS|01" + "_" + pageId + "_" + _comp;
        List<CacheObject> _obj = CacheManager.Get(_cachekey, DateTime.Now);
        if (_obj.Count == 0 | CacheManager.CacheActive == 0)
        {
            using (DataTable dt = new DataTable())
            {
                try
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = conn;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "[sp_0C_50032_00_WEB PAGE CAPTIONS_RADIO LINKS|01]";
                            cmd.Parameters.Clear();
                            cmd.Parameters.AddWithValue("@PageId", pageId);
                            cmd.Parameters.AddWithValue("@Sirket", _comp);
                            using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                            {
                                adapter.Fill(dt);
                                CacheManager.SetDynamic(dt, _cachekey, 24 * 60, 1);
                                _obj = CacheManager.Get(_cachekey, DateTime.Now);
                            }

                        }
                    }
                }
                catch
                {

                }

            }
        }

        return _obj[0].obj;

    }
    private static void preparegridGroup(ASPxGridView grd, DataTable dtg)
    {
        var query = from p in dtg.AsEnumerable()
                    where p.Field<string>("DX Field ID") == "G0000" & p.Field<int>("DX Display Order") == 0
                    select new
                    {
                        DxdisplayOrder = p.Field<int>("DX Display Order")
                    };
        if (query.Count() > 0)
            grd.Settings.ShowGroupPanel = false;
        else
            grd.Settings.ShowGroupPanel = true;
    }
    private static void preparegridCaption(ASPxGridView grd, DataTable dtg)
    {
        var query = from p in dtg.AsEnumerable()
                    where p.Field<string>("DX Field ID") == "C0000" & p.Field<int>("DX Display Order") == 0
                    select new
                    {
                        DxdisplayOrder = p.Field<int>("DX Display Order")
                    };
        if (query.Count() > 0)
            grd.Settings.ShowColumnHeaders = false;
        else
            grd.Settings.ShowColumnHeaders = true;
    }
    private static void preparegridSearch(ASPxGridView grd, DataTable dts)
    {
        var query = from p in dts.AsEnumerable()
                    where p.Field<string>("DX Field ID") == "S0000" & p.Field<int>("DX Display Order") == 0
                    select new
                    {
                        DxdisplayOrder = p.Field<int>("DX Display Order")
                    };
        if (query.Count() > 0)
        {
            grd.Settings.ShowFilterRow = false;
        }
        else
        {
            grd.Settings.ShowFilterRow = true;
            grd.SettingsBehavior.FilterRowMode = GridViewFilterRowMode.OnClick;
        }
    }

    private static void prepareGridColumns(ASPxGridView grid, DataTable dtRows, DataTable dtCaption, int ObjectNumberPerPage, string _user, string _comp, string _selectedLang, string _objKey)
    {

        bool _select = false, _edit = false, _delete = false, _deleteHeader = false, _insert = false, _clone = false, _clear = false, _insertHeader = false, _view = false;
        int sayac = 1;
        for (int i = 0; i < dtRows.Rows.Count; i++)
        {
            switch (dtRows.Rows[i]["DX Type1"].ToString())
            {
                case "DATA":
                    if (dtRows.Rows[i]["DX Type2"].ToString() == "INTEGER" || dtRows.Rows[i]["DX Type2"].ToString() == "DECIMAL")
                    {
                        grid.Columns.Add(creategridviewTextColumn(grid, dtRows, dtRows.Rows[i], dtRows.Rows[i]["DX Field ID"].ToString(),
                                        dtRows.Rows[i][_selectedLang].ToString(), Convert.ToInt32(dtRows.Rows[i]["DX Width"].ToString()),
                                        Convert.ToInt32(dtRows.Rows[i]["Aligned"].ToString()), dtRows.Rows[i]["DX Background Color"].ToString(),
                                        dtRows.Rows[i]["DX Equation"].ToString(), dtRows.Rows[i]["DX Equation Result"].ToString(), dtRows.Rows[i]["DX Display Format"].ToString()
                                        ));
                        columnindex++;


                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "OPTION")
                    {
                        grid.Columns.Add(creategridviewOptionColumn(grid, dtRows, dtRows.Rows[i], dtRows.Rows[i]["DX Field ID"].ToString(), dtRows.Rows[i][_selectedLang].ToString(), Convert.ToInt32(dtRows.Rows[i]["DX Width"].ToString()), Convert.ToInt32(dtRows.Rows[i]["Aligned"].ToString()), dtRows.Rows[i]["DX Background Color"].ToString(),
                                       dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(), dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), sayac));
                        columnindex++;
                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "DATE")
                    {
                        grid.Columns.Add(creategridviewDateColumn(grid, dtRows, dtRows.Rows[i], dtRows.Rows[i]["DX Field ID"].ToString(),
                                      dtRows.Rows[i][_selectedLang].ToString(), Convert.ToInt32(dtRows.Rows[i]["DX Width"].ToString()),
                                      Convert.ToInt32(dtRows.Rows[i]["Aligned"].ToString()), dtRows.Rows[i]["DX Background Color"].ToString(),
                                      dtRows.Rows[i]["DX Display Format"].ToString()
                                      , dtRows.Rows[i]["DX Casc Target"].ToString(), dtRows.Rows[i]["DX Casc Filter"].ToString(), Convert.ToInt32(dtRows.Rows[i]["DX Casc Type"].ToString()),
                                Convert.ToInt32(dtRows.Rows[i]["DX CsMsTy"].ToString())
                                , _user, _comp));
                        columnindex++;
                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "TIME")
                    {
                        grid.Columns.Add(creategridviewTimeColumn(dtRows, dtRows.Rows[i], dtRows.Rows[i]["DX Field ID"].ToString(),
                                      dtRows.Rows[i][_selectedLang].ToString(), Convert.ToInt32(dtRows.Rows[i]["DX Width"].ToString()),
                                      Convert.ToInt32(dtRows.Rows[i]["Aligned"].ToString()), dtRows.Rows[i]["DX Background Color"].ToString(),
                                      dtRows.Rows[i]["DX Display Format"].ToString()));
                        columnindex++;
                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "BOOLEAN")
                    {
                        grid.Columns.Add(creategridviewCheckColumn(dtRows, dtRows.Rows[i], dtRows.Rows[i]["DX Field ID"].ToString(),
                                      dtRows.Rows[i][_selectedLang].ToString(), Convert.ToInt32(dtRows.Rows[i]["DX Width"].ToString()),
                                      Convert.ToInt32(dtRows.Rows[i]["Aligned"].ToString()), dtRows.Rows[i]["DX Background Color"].ToString()));
                        columnindex++;
                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "PDF")
                    {
                        grid.Columns.Add(creategridviewDataHyperLink(dtRows.Rows[i], dtRows.Rows[i]["DX Field ID"].ToString(),
                                      dtRows.Rows[i][_selectedLang].ToString(), Convert.ToInt32(dtRows.Rows[i]["DX Width"].ToString()), _comp
                            , dtRows.Rows[i]["DX Filter"].ToString(), dtRows.Rows[i]["DX Source"].ToString()));
                        columnindex++;
                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "LINK")
                    {
                        grid.Columns.Add(createHyperLinkColumn(dtRows, dtRows.Rows[i], dtRows.Rows[i]["DX Field ID"].ToString(),
                            dtRows.Rows[i][_selectedLang].ToString(), Convert.ToInt32(dtRows.Rows[i]["DX Width"].ToString()),
                            Convert.ToInt32(dtRows.Rows[i]["Aligned"].ToString()), dtRows.Rows[i]["DX Background Color"].ToString(), dtRows.Rows[i]["DX FlLnk"].ToString(), dtRows.Rows[i]["DX Source"].ToString(), _objKey));
                        columnindex++;
                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "COMBO")
                    {
                        grid.Columns.Add(creategridviewComboboboxColumn(grid, dtRows, dtRows.Rows[i], dtRows.Rows[i]["DX Field ID"].ToString(), dtRows.Rows[i][_selectedLang].ToString(), Convert.ToInt32(dtRows.Rows[i]["DX Width"].ToString()), Convert.ToInt32(dtRows.Rows[i]["Aligned"].ToString()), dtRows.Rows[i]["DX Background Color"].ToString(),
                                dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(), dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(),
                                sayac, ObjectNumberPerPage, dtRows.Rows[i]["DX Equation"].ToString(), dtRows.Rows[i]["DX Equation Result"].ToString()
                                , dtRows.Rows[i]["DX Casc Target"].ToString(), dtRows.Rows[i]["DX Casc Filter"].ToString(), Convert.ToInt32(dtRows.Rows[i]["DX Casc Type"].ToString()),
                                Convert.ToInt32(dtRows.Rows[i]["DX CsMsTy"].ToString())
                                , _user, _comp));
                        columnindex++;
                        sayac++;

                    }

                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "MEMO")
                    {
                        grid.Columns.Add(creategridviewDataMemoColumn(dtRows, dtRows.Rows[i], dtRows.Rows[i]["DX Field ID"].ToString(),
                           dtRows.Rows[i][_selectedLang].ToString(), Convert.ToInt32(dtRows.Rows[i]["DX Width"].ToString()),
                           Convert.ToInt32(dtRows.Rows[i]["Aligned"].ToString()), dtRows.Rows[i]["DX Background Color"].ToString()));
                        columnindex++;
                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "TEXT"
                        | dtRows.Rows[i]["DX Type2"].ToString() == "CODE"
                        | dtRows.Rows[i]["DX Type2"].ToString() == "DIRECT")
                    {
                        grid.Columns.Add(creategridviewColumn(dtRows, dtRows.Rows[i], dtRows.Rows[i]["DX Field ID"].ToString(),
                            dtRows.Rows[i][_selectedLang].ToString(), Convert.ToInt32(dtRows.Rows[i]["DX Width"].ToString()),
                            Convert.ToInt32(dtRows.Rows[i]["Aligned"].ToString()), dtRows.Rows[i]["DX Background Color"].ToString()));
                        columnindex++;
                    }
                    break;
                case "BUTTON":
                    if (dtRows.Rows[i]["DX Type2"].ToString() == "SELECT")
                    {
                        _select = true;


                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "EDIT")
                    {
                        if (dtRows.Rows[i]["DX Permision Field"].ToString() != "")
                        {
                            _edit = true;// f.DynamicPageYetkiKontrol(sirket, user, dtRows.Rows[i]["DX Permision Field"].ToString());
                        }
                        else
                        {
                            _edit = true;
                        }
                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "DELETE")
                    {
                        if (dtRows.Rows[i]["DX Permision Field"].ToString() != "")
                        {
                            _delete = true;// f.DynamicPageYetkiKontrol(sirket, user, dtRows.Rows[i]["DX Permision Field"].ToString());
                        }
                        else
                        {
                            _delete = true;
                        }
                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "INSERT")
                    {
                        if (dtRows.Rows[i]["DX Permision Field"].ToString() != "")
                        {
                            _insert = true;//f.DynamicPageYetkiKontrol(sirket, user, dtRows.Rows[i]["DX Permision Field"].ToString());
                        }
                        else
                        {
                            _insert = true;
                        }
                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "CLONE")
                    {
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Permision Field"].ToString()))
                        {
                            _clone = true;//f.DynamicPageYetkiKontrol(sirket, user, dtRows.Rows[i]["DX Permision Field"].ToString());
                        }
                        else
                        {
                            _clone = true;
                        }

                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "VIEW")
                    {
                        _view = true;

                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "CLEAR")
                    {
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Permision Field"].ToString()))
                        {
                            _clear = true;//f.DynamicPageYetkiKontrol(sirket, user, dtRows.Rows[i]["DX Permision Field"].ToString());
                        }
                        else
                        {
                            _clear = true;
                        }

                    }
                    break;
            }

        }

        var queryC = from p in dtCaption.AsEnumerable()
                     where p.Field<string>("DX Type1") == "BUTTON" & p.Field<string>("DX Type2") == "INSERT" & p.Field<int>("DX Display Order") > 0
                     select new
                     {
                         DxdisplayOrder = p.Field<int>("DX Display Order")
                     };
        if (queryC.Count() > 0)
            _insertHeader = true;
        else
            _insertHeader = false;

        var queryCd = from p in dtCaption.AsEnumerable()
                      where p.Field<string>("DX Type1") == "BUTTON" & p.Field<string>("DX Type2") == "DELETE" & p.Field<int>("DX Display Order") > 0
                      select new
                      {
                          DxdisplayOrder = p.Field<int>("DX Display Order")
                      };
        if (queryCd.Count() > 0)
            _deleteHeader = true;
        else
            _deleteHeader = false;


        bool _isfiltericon = false;

        try
        {
            var queryr = from p in dtRows.AsEnumerable()
                         where p.Field<string>("DX Field ID") == "R0000" & p.Field<int>("DX Display Order") > 0
                         select new
                         {
                             DxdisplayOrder = p.Field<int>("DX Display Order")
                         };
            if (queryr.Count() > 0)
                _isfiltericon = true;
        }
        catch { }


        if (_select | _edit | _delete | _insert | _clone | _clear | _isfiltericon | _view)
        {
            leftmenuSettings(grid, dtRows, _select, _edit, _delete, _insert, _insertHeader, _deleteHeader, _clone, _clear, _view, _selectedLang);
        }
        columnindex++;
        grid.Columns.Add(creategridviewBlankColumn());

    }
    private static void preparePageParams(ASPxGridView grid, DataTable dtRows, DataTable dtCaption, int ObjectNumberPerPage, TextBox InsertParams, TextBox CloneParams, TextBox EditParams, TextBox DeleteParams)
    {


        var queryEdit = from p in dtRows.AsEnumerable()
                        where p.Field<string>("DX Type1") == "BUTTON" & p.Field<string>("DX Type2") == "EDIT" & p.Field<int>("DX Display Order") > 0
                        select new
                        {
                            DxdisplayOrder = p.Field<int>("DX Display Order"),
                            DxSource = p.Field<string>("DX Source"),
                            DxFilter = p.Field<string>("DX Filter")
                        };
        if (queryEdit.Count() > 0)
            EditParams.Text = queryEdit.FirstOrDefault().DxSource + "," + queryEdit.FirstOrDefault().DxFilter;

        var queryRowsDelete = from p in dtRows.AsEnumerable()
                              where p.Field<string>("DX Type1") == "BUTTON" & p.Field<string>("DX Obje Zone") == "5ROWS" & p.Field<string>("DX Type2") == "DELETE" & p.Field<int>("DX Display Order") > 0
                              select new
                              {
                                  DxdisplayOrder = p.Field<int>("DX Display Order"),
                                  DxSource = p.Field<string>("DX Source"),
                                  DxFilter = p.Field<string>("DX Filter")
                              };
        if (queryRowsDelete.Count() > 0)
            DeleteParams.Text = queryRowsDelete.FirstOrDefault().DxSource + "," + queryRowsDelete.FirstOrDefault().DxFilter;

        var queryInsert = from p in dtCaption.AsEnumerable()
                          where p.Field<string>("DX Type1") == "BUTTON" & p.Field<string>("DX Type2") == "INSERT"
                          select new
                          {
                              DxdisplayOrder = p.Field<int>("DX Display Order"),
                              DxSource = p.Field<string>("DX Source"),
                              DxFilter = p.Field<string>("DX Filter")
                          };
        if (queryInsert.Count() > 0)
            InsertParams.Text = queryInsert.FirstOrDefault().DxSource + "," + queryInsert.FirstOrDefault().DxFilter;

        var queryClone = from p in dtRows.AsEnumerable()
                         where p.Field<string>("DX Type1") == "BUTTON" & p.Field<string>("DX Type2") == "CLONE" & p.Field<int>("DX Display Order") > 0
                         select new
                         {
                             DxdisplayOrder = p.Field<int>("DX Display Order"),
                             DxSource = p.Field<string>("DX Source"),
                             DxFilter = p.Field<string>("DX Filter")
                         };
        if (queryClone.Count() > 0)
            CloneParams.Text = queryClone.FirstOrDefault().DxSource + "," + queryClone.FirstOrDefault().DxFilter;


        //var queryDelete = from p in dtCaption.AsEnumerable()
        //                  where p.Field<string>("DX Type1") == "BUTTON"  & p.Field<string>("DX Type2") == "DELETE" & p.Field<int>("DX Display Order") > 0
        //                  select new
        //                  {
        //                      DxdisplayOrder = p.Field<int>("DX Display Order"),
        //                      DxSource = p.Field<string>("DX Source"),
        //                      DxFilter = p.Field<string>("DX Filter")
        //                  };
        //if (queryDelete.Count() > 0)
        //    DeleteParams.Text = queryDelete.FirstOrDefault().DxSource + "," + queryDelete.FirstOrDefault().DxFilter;

    }

    private static void preparegridFooter(ASPxGridView grd, DataTable dtf)
    {
        var query = from p in dtf.AsEnumerable()
                    where p.Field<string>("DX Field ID") == "F0000" & p.Field<int>("DX Display Order") > 0
                    select new
                    {
                        DxdisplayOrder = p.Field<int>("DX Display Order")
                    };
        if (query.Count() > 0)
            grd.Settings.ShowFooter = true;
        else
            grd.Settings.ShowFooter = false;

    }

    private static GridViewDataTextColumn creategridviewTextColumn(ASPxGridView grid, DataTable dtRows, DataRow row, string fieldID, string caption, int width, int halignment, string bgcolor, string Equation, string equationResult, string displayFormat)
    {

        GridViewDataTextColumn _column = new GridViewDataTextColumn();
        _column.FieldName = fieldID;
        _column.Name = fieldID;
        _column.Caption = caption;
        _column.VisibleIndex = columnindex;
        double resize = (double)screenWidth * ((double)width / 100);
        _column.Width = Unit.Pixel(Convert.ToInt32(resize));

        if (Convert.ToInt32(row["DX DfGrI"]) > 0) _column.GroupIndex = Convert.ToInt32(row["DX DfGrI"]) - 1;

        if (!string.IsNullOrEmpty(bgcolor))
        {
            if (bgcolor.Substring(0, 1) == "#")
                _column.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);
            else
                _column.CellStyle.CssClass = bgcolor;
        }
        else
        {
            bgcolor = "#FFFFFF";
            _column.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);
        }
        _column.CellStyle.Wrap = DevExpress.Utils.DefaultBoolean.False;
        HorizontalAlign _haling = halignment == 0 ? HorizontalAlign.Left :
                                  halignment == 1 ? HorizontalAlign.Center :
                                  halignment == 2 ? HorizontalAlign.Right : HorizontalAlign.NotSet;
        _column.CellStyle.HorizontalAlign = _haling;
        _column.HeaderStyle.HorizontalAlign = _haling;

        if (!string.IsNullOrEmpty(displayFormat))
        {
            _column.PropertiesTextEdit.DisplayFormatString = displayFormat;
            _column.PropertiesTextEdit.DisplayFormatInEditMode = true;
        }

        _column.Settings.AutoFilterCondition = AutoFilterCondition.Contains;

        if (Equation != "")
        {
            string[] eqArr = Equation.Split('|');
            string[] eqResultArr = equationResult.Split('|');
            string valueChange = "function(s,e) { ";
            for (int i = 0; i < eqArr.Length; i++)
            {
                valueChange += grid.ClientInstanceName + ".SetEditValue('" + eqResultArr[i] + "',parseFloat(" + eqArr[i].Replace("grid", grid.ClientInstanceName) + ").toFixed(2)); ";
            }
            valueChange += "}";
            _column.PropertiesTextEdit.ClientSideEvents.ValueChanged = valueChange;
        }

        //if (Equation != "")
        //{
        //    string valueChange = "function(s,e) { " + grid.ClientInstanceName + ".SetEditValue('" + equationResult + "'," + Equation.Replace("grid", grid.ClientInstanceName) + "); }";
        //    _column.PropertiesTextEdit.ClientSideEvents.ValueChanged = valueChange;
        //}

        if (fieldID != "REDIT11")
        {
            copiedFields[copiedSayac] = fieldID;
            copiedSayac++;
        }




        try
        {
            DataRow[] rows = dtRows.Select("[DX Caption Filter]>0 AND [DX Field ID]='" + fieldID + "'");
            if (rows.Length > 0)
                _column.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;

            DataRow[] rowsg = dtRows.Select("[DX Field ID]='" + fieldID + "'");
            //if (rowsg[0]["DX Insert Hidden"].ToString() == "1")
            //    _column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

            int dxpopupindex = Convert.ToInt32(rowsg[0]["DX Edit Popup Index"].ToString());

            if (dxpopupindex > -1)
            {
                _column.EditFormSettings.VisibleIndex = dxpopupindex;
            }
            else if (dxpopupindex == -2)
            {
                _column.EditFormSettings.VisibleIndex = 99999;
                _column.PropertiesTextEdit.ClientSideEvents.Init = "function(s,e){PopupDisplayNone(s, e);}";
                _column.EditFormCaptionStyle.CssClass = "dis-none";
            }



        }
        catch
        {


        }
        return _column;
    }

    private static GridViewDataColumn creategridviewColumn(DataTable dtRows, DataRow row, string fieldID, string caption, int width, int halignment, string bgcolor)
    {
        GridViewDataTextColumn _column = new GridViewDataTextColumn();
        _column.FieldName = fieldID;
        _column.Name = fieldID;
        _column.Caption = caption;
        _column.VisibleIndex = columnindex;
        double resize = (double)screenWidth * ((double)width / 100);
        _column.Width = Unit.Pixel(Convert.ToInt32(resize));
        if (!string.IsNullOrEmpty(bgcolor))
        {
            if (bgcolor.Substring(0, 1) == "#")
                _column.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);
            else
                _column.CellStyle.CssClass = bgcolor;
        }
        else
        {
            bgcolor = "#FFFFFF";
            _column.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);
        }

        if (Convert.ToInt32(row["DX DfGrI"]) > 0) _column.GroupIndex = Convert.ToInt32(row["DX DfGrI"]) - 1;

        HorizontalAlign _haling = halignment == 0 ? HorizontalAlign.Left :
                                  halignment == 1 ? HorizontalAlign.Center :
                                  halignment == 2 ? HorizontalAlign.Right : HorizontalAlign.NotSet;

        _column.CellStyle.HorizontalAlign = _haling;
        _column.HeaderStyle.HorizontalAlign = _haling;
        _column.CellStyle.Wrap = DevExpress.Utils.DefaultBoolean.False;
        _column.Settings.AutoFilterCondition = AutoFilterCondition.Contains;

        if (fieldID != "REDIT11")
        {
            copiedFields[copiedSayac] = fieldID;
            copiedSayac++;
        }


        try
        {
            DataRow[] rows = dtRows.Select("[DX Caption Filter]>0 AND [DX Field ID]='" + fieldID + "'");
            if (rows.Length > 0)
                _column.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;

            DataRow[] rowsg = dtRows.Select("[DX Field ID]='" + fieldID + "'");
            //if (rowsg[0]["DX Insert Hidden"].ToString() == "1")
            //    _column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

            int dxpopupindex = Convert.ToInt32(rowsg[0]["DX Edit Popup Index"].ToString());

            if (dxpopupindex > -1)
            {
                _column.EditFormSettings.VisibleIndex = dxpopupindex;

            }

            else if (dxpopupindex == -2)
            {
                _column.EditFormSettings.VisibleIndex = 99999;
                _column.PropertiesTextEdit.ClientSideEvents.Init = "function(s,e){PopupDisplayNone(s, e);}";
                _column.EditFormCaptionStyle.CssClass = "dis-none";
            }
            if (caption.Contains("*"))
            {
                _column.PropertiesTextEdit.ClientSideEvents.Init = "function(s,e){requiredControl(s,e)}";
                _column.PropertiesEdit.Style.CssClass += " requiredCheck";
            }

        }
        catch
        {


        }

        return _column;
    }

    private static GridViewDataMemoColumn creategridviewDataMemoColumn(DataTable dtRows, DataRow row, string fieldID, string caption, int width, int halignment, string bgcolor)
    {
        GridViewDataMemoColumn _column = new GridViewDataMemoColumn();
        _column.FieldName = fieldID;
        _column.Name = fieldID;
        _column.Caption = caption;
        _column.Settings.AllowCellMerge = DevExpress.Utils.DefaultBoolean.True;
        _column.Settings.AllowEllipsisInText = DevExpress.Utils.DefaultBoolean.True;
        _column.EditFormSettings.ColumnSpan = 3;
        _column.PropertiesMemoEdit.Height = Unit.Pixel(50);
        _column.PropertiesMemoEdit.ClientSideEvents.KeyDown = "function(s,e){OnMemoKeyDown(s, e);}";
        _column.VisibleIndex = columnindex;
        double resize = (double)screenWidth * ((double)width / 100);
        _column.Width = Unit.Pixel(Convert.ToInt32(resize));

        if (!string.IsNullOrEmpty(bgcolor))
        {
            if (bgcolor.Substring(0, 1) == "#")
                _column.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);
            else
                _column.CellStyle.CssClass = bgcolor;
        }
        else
        {
            bgcolor = "#FFFFFF";
            _column.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);
        }

        if (Convert.ToInt32(row["DX DfGrI"]) > 0) _column.GroupIndex = Convert.ToInt32(row["DX DfGrI"]) - 1;

        HorizontalAlign _haling = halignment == 0 ? HorizontalAlign.Left :
                                  halignment == 1 ? HorizontalAlign.Center :
                                  halignment == 2 ? HorizontalAlign.Right : HorizontalAlign.NotSet;

        _column.CellStyle.HorizontalAlign = _haling;
        _column.HeaderStyle.HorizontalAlign = _haling;
        _column.Settings.AutoFilterCondition = AutoFilterCondition.Contains;

        if (fieldID != "REDIT11")
        {
            copiedFields[copiedSayac] = fieldID;
            copiedSayac++;
        }


        try
        {
            DataRow[] rows = dtRows.Select("[DX Caption Filter]>0 AND [DX Field ID]='" + fieldID + "'");
            if (rows.Length > 0)
                _column.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;

            DataRow[] rowsg = dtRows.Select("[DX Field ID]='" + fieldID + "'");
            //if (rowsg[0]["DX Insert Hidden"].ToString() == "1")
            //    _column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

            int dxpopupindex = Convert.ToInt32(rowsg[0]["DX Edit Popup Index"].ToString());

            if (dxpopupindex > -1)
            {
                _column.EditFormSettings.VisibleIndex = dxpopupindex;

            }
            else if (dxpopupindex == -2)
            {
                _column.EditFormSettings.VisibleIndex = 99999;
                _column.PropertiesMemoEdit.ClientSideEvents.Init = "function(s,e){PopupDisplayNone(s, e);}";
                _column.EditFormCaptionStyle.CssClass = "dis-none";
            }
            if (caption.Contains("*"))
            {
                _column.PropertiesMemoEdit.ClientSideEvents.Init = "function(s,e){requiredControl(s,e)}";
                _column.PropertiesEdit.Style.CssClass += " requiredCheck";
            }
        }
        catch
        {


        }

        return _column;
    }

    private static GridViewDataColumn creategridviewBlankColumn()
    {
        GridViewDataColumn _column = new GridViewDataColumn();
        _column.FieldName = "REDIT";
        _column.Name = "REDIT11";
        _column.Caption = "";
        _column.Width = Unit.Pixel(0);
        _column.VisibleIndex = columnindex;
        _column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;
        return _column;
    }

    private static GridViewDataComboBoxColumn creategridviewComboboboxColumn(ASPxGridView grid, DataTable dtRows, DataRow row, string fieldID, string caption, int width, int halignment, string bgcolor, string Source
        , string Filter, string Value, string Text, int sayac, int ObjectNumberPerPage, string Equation, string equationResult
        , string DxCascTarget, string DxCascFilter, int CascType, int CsMsTy, string _user, string _comp)
    {

        GridViewDataComboBoxColumn _comboboxColumn = new GridViewDataComboBoxColumn();
        _comboboxColumn.FieldName = fieldID;
        _comboboxColumn.Name = fieldID;
        _comboboxColumn.PropertiesComboBox.ClientInstanceName = fieldID;
        _comboboxColumn.Caption = caption;
        _comboboxColumn.VisibleIndex = columnindex;

        double resize = (double)screenWidth * ((double)width / 100);
        _comboboxColumn.Width = Unit.Pixel(Convert.ToInt32(resize));


        if (Convert.ToInt32(row["DX DfGrI"]) > 0) _comboboxColumn.GroupIndex = Convert.ToInt32(row["DX DfGrI"]) - 1;


        if (!string.IsNullOrEmpty(bgcolor))
        {
            if (bgcolor.Substring(0, 1) == "#")
                _comboboxColumn.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);
            else
                _comboboxColumn.CellStyle.CssClass = bgcolor;
        }
        else
        {
            bgcolor = "#FFFFFF";
            _comboboxColumn.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);
        }

        HorizontalAlign _haling = halignment == 0 ? HorizontalAlign.Left :
                                  halignment == 1 ? HorizontalAlign.Center :
                                  halignment == 2 ? HorizontalAlign.Right : HorizontalAlign.NotSet;

        _comboboxColumn.CellStyle.HorizontalAlign = _haling;
        _comboboxColumn.HeaderStyle.HorizontalAlign = _haling;


        if (Equation != "")
        {
            string[] eqArr = Equation.Split('|');
            string[] eqResultArr = equationResult.Split('|');
            string valueChange = "function(s,e) { ";
            for (int i = 0; i < eqArr.Length; i++)
            {
                valueChange += grid.ClientInstanceName + ".SetEditValue('" + eqResultArr[i] + "',parseFloat(" + eqArr[i].Replace("grid", grid.ClientInstanceName) + ").toFixed(2)); ";
            }
            valueChange += "}";
            _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = valueChange;
        }

        #region  cascading dinamik yapı devam ediyor
        if (!string.IsNullOrEmpty(DxCascTarget))
        {
            string[] cascarr = DxCascTarget.Split(',');
            string selectindexjs = "function(s, e) {", casfilters = string.Empty;
            int filterindex = 0;
            foreach (string filval in cascarr)
            {
                var casc5Control = from p in dtRows.AsEnumerable()
                                   where p.Field<string>("DX Field ID") == filval & p.Field<int>("DX Display Order") > 0
                                   select new
                                   {
                                       Cont = p.Field<int>("DX Casc Type")
                                   };

                int _tempcascType = 0;
                if (casc5Control.Count() > 0)
                    _tempcascType = casc5Control.FirstOrDefault().Cont;


                if (_tempcascType == 5)
                {
                    continue;
                }

                var queryr = from p in dtRows.AsEnumerable()
                             where p.Field<string>("DX Field ID") == filval & p.Field<int>("DX Display Order") > 0
                             select new
                             {
                                 DxSource = p.Field<string>("DX Source"),
                                 ComboValue = p.Field<string>("DX Combo Value"),
                                 ComboText = p.Field<string>("DX Combo Text"),
                                 ComboFilter = p.Field<string>("DX Filter"),
                                 ComboOrder = p.Field<string>("DX Combo Order"),
                             };



                if (queryr.Count() > 0)
                {
                    if (_tempcascType == 4)
                    {

                        string _paramValues = string.Empty, _dxFieldParams = string.Empty;
                        string[] _dxparams = queryr.FirstOrDefault().ComboFilter.Split(',');
                        for (int i = 0; i < _dxparams.Length; i++)
                        {
                            if (_dxparams[i].ToString() == "USER")
                                _paramValues += _user + ",";
                            else if (_dxparams[i].ToString() == "COMP")
                                _paramValues += _comp + ",";
                            else if (_dxparams[i].ToString().Contains("#"))
                                _paramValues += _dxparams[i].ToString() + ",";
                            else
                                _dxFieldParams += _dxparams[i].ToString() + ",";
                            //_dxFieldParams += grid.ClientInstanceName + ".GetEditValue('" + _dxparams[i].ToString() + "');";
                        }

                        string output = _dxFieldParams + _paramValues;
                        output = output.Substring(0, output.Length - 1);
                        casfilters = queryr.FirstOrDefault().DxSource + ";" +
                               queryr.FirstOrDefault().ComboValue + ";" +
                               queryr.FirstOrDefault().ComboText + ";" +
                               output + ";" +
                               queryr.FirstOrDefault().ComboOrder + ";" +
                               DxCascFilter.Split(',')[filterindex] + ";" +
                               Source + ";" +
                               Value + ";" +
                               Filter;

                    }
                    else
                    {
                        casfilters = queryr.FirstOrDefault().DxSource + ";" +
                          queryr.FirstOrDefault().ComboValue + ";" +
                          queryr.FirstOrDefault().ComboText + ";" +
                          queryr.FirstOrDefault().ComboFilter + ";" +
                          queryr.FirstOrDefault().ComboOrder + ";" +
                          DxCascFilter.Split(',')[filterindex] + ";" +
                          Source + ";" +
                          Value + ";" +
                          Filter;
                    }

                    selectindexjs += filval + ".PerformCallback(s.GetValue()+'#'+'" + casfilters + "');";
                }

                filterindex++;
            }

            selectindexjs += "}";

            if (cascarr.Length > 0)
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = selectindexjs;
        }
        #endregion       


        string _ppage = string.Empty;
        if (ObjectNumberPerPage > 10 & sayac < 10)
            _ppage = ObjectNumberPerPage.ToString() + "0";
        else
            _ppage = ObjectNumberPerPage.ToString();


        // _comboboxColumn.PropertiesComboBox.DataSourceID = "DS" + _ppage + sayac.ToString();
        _comboboxColumn.PropertiesComboBox.ValueField = Value;
        _comboboxColumn.PropertiesComboBox.TextField = "customText";
        _comboboxColumn.PropertiesComboBox.ValueType = typeof(String);
        _comboboxColumn.PropertiesComboBox.EnableCallbackMode = true;
        _comboboxColumn.PropertiesComboBox.CallbackPageSize = 25;
        _comboboxColumn.PropertiesComboBox.IncrementalFilteringMode = IncrementalFilteringMode.Contains;
        _comboboxColumn.PropertiesComboBox.DropDownStyle = DropDownStyle.DropDown;

        _comboboxColumn.CellStyle.Wrap = DevExpress.Utils.DefaultBoolean.False;
        //_comboboxColumn.PropertiesComboBox.ConvertEmptyStringToNull = false;

        if (CascType == 0)
        {
            _comboboxColumn.PropertiesComboBox.NullText = Source + "#" + Filter + "#" + Value + "#" + Text + "#" + _user + "#" + _comp;
            _comboboxColumn.PropertiesComboBox.NullDisplayText = string.Empty;
            _comboboxColumn.PropertiesComboBox.NullTextStyle.ForeColor = System.Drawing.Color.White;
            _comboboxColumn.PropertiesComboBox.ItemsRequestedByFilterCondition += PropertiesComboBox_ItemsRequestedByFilterCondition;
            _comboboxColumn.PropertiesComboBox.ItemRequestedByValue += PropertiesComboBox_ItemRequestedByValue;
        }
        if (CascType == 6)
        {
            _comboboxColumn.PropertiesComboBox.NullText = "CascColumn6" + "#" + Source + "#" + Filter + "#" + Value + "#" + Text;
            _comboboxColumn.PropertiesComboBox.NullDisplayText = string.Empty;
            _comboboxColumn.PropertiesComboBox.NullTextStyle.ForeColor = System.Drawing.Color.White;
        }
        else if (CascType == 1)
        {
            var queryr = from p in dtRows.AsEnumerable()
                         where p.Field<string>("DX Casc Target").Contains(fieldID)
                         select new
                         {
                             DxSource = p.Field<string>("DX Source"),
                             AnaFieldId = p.Field<string>("DX Field ID"),
                             DxCascTarget = p.Field<string>("DX Casc Target"),
                             dxCascFilter = p.Field<string>("DX Casc Filter"),
                             MainValue = p.Field<string>("DX Combo Value"),
                             MainFilter = p.Field<string>("DX Filter")
                         };

            string anafieldId = string.Empty, anasource = string.Empty, casctarget = string.Empty
                , cascfilter = string.Empty, MainVal = string.Empty, MainFilter = string.Empty, _cascfilter = string.Empty;
            int _count = 0;


            if (queryr.Count() > 0)
            {
                anafieldId = queryr.FirstOrDefault().AnaFieldId;
                anasource = queryr.FirstOrDefault().DxSource;
                casctarget = queryr.FirstOrDefault().DxCascTarget;
                cascfilter = queryr.FirstOrDefault().dxCascFilter;
                MainVal = queryr.FirstOrDefault().MainValue;
                MainFilter = queryr.FirstOrDefault().MainFilter;
                foreach (string fid in casctarget.Split(','))
                {
                    if (fid == fieldID)
                    {
                        try
                        {
                            _cascfilter = cascfilter.Split(',')[_count];

                        }
                        catch { }

                        break;
                    }
                    _count++;
                }


            }

            _comboboxColumn.PropertiesComboBox.NullText = "CascColumn1" + "#" + Source + "#" + Filter + "#" + Value + "#"
                + Text + "#" + anafieldId + "#" + anasource + "#" + _cascfilter + "#" + MainVal + "#" + MainFilter + "#" + _user + "#" + _comp;
            _comboboxColumn.PropertiesComboBox.NullDisplayText = string.Empty;
            _comboboxColumn.PropertiesComboBox.NullTextStyle.ForeColor = System.Drawing.Color.White;
            //_comboboxColumn.PropertiesComboBox.ItemsRequestedByFilterCondition += PropertiesComboBox_ItemsRequestedByFilterCondition1;
            //_comboboxColumn.PropertiesComboBox.ItemRequestedByValue += PropertiesComboBox_ItemRequestedByValue1;
        }
        else if (CascType == 2)
        {
            var queryr = from p in dtRows.AsEnumerable()
                         where p.Field<string>("DX Casc Target").Contains(fieldID)
                         select new
                         {
                             DxSource = p.Field<string>("DX Source"),
                             AnaFieldId = p.Field<string>("DX Field ID"),
                             DxCascTarget = p.Field<string>("DX Casc Target"),
                             dxCascFilter = p.Field<string>("DX Casc Filter"),
                             MainValue = p.Field<string>("DX Combo Value"),
                             MainFilter = p.Field<string>("DX Filter")
                         };

            string anafieldId = string.Empty, anasource = string.Empty, casctarget = string.Empty
                , cascfilter = string.Empty, MainVal = string.Empty, MainFilter = string.Empty, _cascfilter = string.Empty;
            int _count = 0;



            if (queryr.Count() > 0)
            {
                anafieldId = queryr.FirstOrDefault().AnaFieldId;
                anasource = queryr.FirstOrDefault().DxSource;
                casctarget = queryr.FirstOrDefault().DxCascTarget;
                cascfilter = queryr.FirstOrDefault().dxCascFilter;
                MainVal = queryr.FirstOrDefault().MainValue;
                MainFilter = queryr.FirstOrDefault().MainFilter;
                foreach (string fid in casctarget.Split(','))
                {
                    if (fid == fieldID)
                    {
                        try
                        {
                            _cascfilter = cascfilter.Split(',')[_count];

                        }
                        catch { }

                        break;
                    }
                    _count++;
                }


            }



            _comboboxColumn.PropertiesComboBox.NullText = "CascColumn2" + "#" + Source + "#" + Filter + "#" + Value + "#"
                + Text + "#" + anafieldId + "#" + anasource + "#" + _cascfilter + "#" + MainVal + "#" + MainFilter + "#" + _user + "#" + _comp;
            _comboboxColumn.PropertiesComboBox.NullDisplayText = string.Empty;
            _comboboxColumn.PropertiesComboBox.NullTextStyle.ForeColor = System.Drawing.Color.White;
            // _comboboxColumn.PropertiesComboBox.ItemsRequestedByFilterCondition += PropertiesComboBox_ItemsRequestedByFilterCondition1;
            // _comboboxColumn.PropertiesComboBox.ItemRequestedByValue += PropertiesComboBox_ItemRequestedByValue1;
        }
        else if (CascType == 3)
        {
            var queryr = from p in dtRows.AsEnumerable()
                         where p.Field<string>("DX Casc Target").Contains(fieldID) // targeta aynı field ID birden fazla yazılmamalı 
                         select new
                         {
                             DxSource = p.Field<string>("DX Source"),
                             AnaFieldId = p.Field<string>("DX Field ID"),
                             DxCascTarget = p.Field<string>("DX Casc Target"),
                             dxCascFilter = p.Field<string>("DX Casc Filter"),
                             MainValue = p.Field<string>("DX Combo Value"),
                             MainFilter = p.Field<string>("DX Filter")
                         };

            string anafieldId = string.Empty, anasource = string.Empty, casctarget = string.Empty
                , cascfilter = string.Empty, MainVal = string.Empty, MainFilter = string.Empty, _cascfilter = string.Empty;
            int _count = 0;



            if (queryr.Count() > 0)
            {
                anafieldId = queryr.FirstOrDefault().AnaFieldId;
                anasource = queryr.FirstOrDefault().DxSource;
                casctarget = queryr.FirstOrDefault().DxCascTarget;
                cascfilter = queryr.FirstOrDefault().dxCascFilter;
                MainVal = queryr.FirstOrDefault().MainValue;
                MainFilter = queryr.FirstOrDefault().MainFilter;
                foreach (string fid in casctarget.Split(','))
                {
                    if (fid == fieldID)
                    {
                        try
                        {
                            _cascfilter = cascfilter.Split(',')[_count];

                        }
                        catch { }

                        break;
                    }
                    _count++;
                }


            }



            _comboboxColumn.PropertiesComboBox.NullText = "CascColumn3" + "#" + Source + "#" + Filter + "#" + Value + "#"
                + Text + "#" + anafieldId + "#" + anasource + "#" + _cascfilter + "#" + MainVal + "#" + MainFilter;
            _comboboxColumn.PropertiesComboBox.NullDisplayText = string.Empty;
            _comboboxColumn.PropertiesComboBox.NullTextStyle.ForeColor = System.Drawing.Color.White;
        }
        else if (CascType == 4)
        {
            var queryr = from p in dtRows.AsEnumerable()
                         where p.Field<string>("DX Casc Target").Contains(fieldID) // targeta aynı field ID birden fazla yazılmamalı 
                         select new
                         {
                             dxTarget = p.Field<string>("DX Field ID")
                         };

            string targetField = string.Empty;

            if (queryr.Count() > 0)
            {
                targetField = queryr.FirstOrDefault().dxTarget;
            }

            var queryTarget = from p in dtRows.AsEnumerable()
                              where p.Field<string>("DX Field ID").Contains(fieldID)
                              select new
                              {
                                  dxSource = p.Field<string>("DX Source"),
                                  dxFilter = p.Field<string>("DX Filter"),
                                  dxText = p.Field<string>("DX Combo Text"),
                                  dxValue = p.Field<string>("DX Combo Value")
                              };

            string targetSource = string.Empty, targetFilter = string.Empty, comboValue = string.Empty, comboText = string.Empty;
            if (queryTarget.Count() > 0)
            {
                targetSource = queryTarget.FirstOrDefault().dxSource;
                targetFilter = queryTarget.FirstOrDefault().dxFilter;
                comboValue = queryTarget.FirstOrDefault().dxValue;
                comboText = queryTarget.FirstOrDefault().dxText;
            }

            string _dxtarget = targetField;
            string _dxsource = targetSource;
            string _dxValue = comboValue;
            string _dxText = comboText;
            string[] _dxparams = targetFilter.Split(',');


            string scrCasc = _dxtarget + "@" + _dxsource + "@" + targetFilter + "@" + _dxValue + "@" + _dxText;
            //string scrFunc = "function(s,e) { " + targetField + ".PerformCallback('+" + scrCasc + "+') } ";

            _comboboxColumn.PropertiesComboBox.NullText = "CascColumn4" + "@" + scrCasc;
            _comboboxColumn.PropertiesComboBox.NullDisplayText = string.Empty;
            _comboboxColumn.PropertiesComboBox.NullTextStyle.ForeColor = System.Drawing.Color.White;
        }
        else if (CascType == 8)
        {
            _comboboxColumn.PropertiesComboBox.NullText = "CascColumn8";
            _comboboxColumn.PropertiesComboBox.NullDisplayText = string.Empty;
            _comboboxColumn.PropertiesComboBox.NullTextStyle.ForeColor = System.Drawing.Color.White;

        }


        if (CsMsTy == 5)
        {
            var queryr = from p in dtRows.AsEnumerable()
                         where p.Field<string>("DX Field ID").Contains(fieldID) // targeta aynı field ID birden fazla yazılmamalı 
                         select new
                         {
                             dxTarget = p.Field<string>("DX Casc Target")
                         };

            string targetField = string.Empty;

            if (queryr.Count() > 0)
            {
                targetField = queryr.FirstOrDefault().dxTarget;
            }

            var queryTarget = from p in dtRows.AsEnumerable()
                              where p.Field<string>("DX Field ID").Contains(targetField)
                              select new
                              {
                                  dxSource = p.Field<string>("DX Source"),
                                  dxFilter = p.Field<string>("DX Filter")
                              };

            string targetSource = string.Empty, targetFilter = string.Empty;
            if (queryTarget.Count() > 0)
            {
                targetSource = queryTarget.FirstOrDefault().dxSource;
                targetFilter = queryTarget.FirstOrDefault().dxFilter;
            }

            string _dxtarget = targetField;
            string _dxsource = targetSource;
            string[] _dxparams = targetFilter.Split(',');

            string _paramValues = string.Empty, _dxFieldParams = string.Empty, _dxparameters = string.Empty;
            for (int i = 0; i < _dxparams.Length; i++)
            {
                if (_dxparams[i].ToString() == "USER")
                    _paramValues += "'" + _user + "'+'#'+";
                else if (_dxparams[i].ToString() == "COMP")
                    _paramValues += "'" + _comp + "'+'#'+";
                else if (_dxparams[i].ToString().Contains("#"))
                    _paramValues += _dxparams[i].ToString() + "+'#'+";
                else
                {
                    var queryType = from p in dtRows.AsEnumerable()
                                    where p.Field<string>("DX Field ID").Contains(_dxparams[i])
                                    select new
                                    {
                                        dxType = p.Field<string>("DX Type2")
                                    };

                    string dxType = string.Empty;

                    if (queryType.Count() > 0)
                    {
                        dxType = queryType.FirstOrDefault().dxType;
                    }

                    if (dxType == "DATE")
                        _dxFieldParams += grid.ClientInstanceName + ".GetEditValue('" + _dxparams[i].ToString() + "').toLocaleDateString('EN-us')+'&" + dxType + "#'+";
                    else
                        _dxFieldParams += grid.ClientInstanceName + ".GetEditValue('" + _dxparams[i].ToString() + "')+'&" + dxType + "#'+";
                }
            }
            _paramValues = _paramValues.Substring(0, _paramValues.Length - 5);
            string scrCasc = "function(s,e){  callbackPanel1.PerformCallback('" + grid.ClientInstanceName.ToString() + "@" + _dxtarget + "@" + _dxsource + "@'+" + _dxFieldParams + _paramValues + "); }";
            _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = scrCasc;

        }
        else if (CsMsTy == 7)
        {
            var queryr = from p in dtRows.AsEnumerable()
                         where p.Field<string>("DX Field ID").Contains(fieldID) // targeta aynı field ID birden fazla yazılmamalı 
                         select new
                         {
                             dxTarget = p.Field<string>("DX Casc Target"),
                             dxCascFilter = p.Field<string>("DX Casc Filter"),
                             dxCascSource = p.Field<string>("Dx Casc Source SP"),
                             dxCascSPParam = p.Field<string>("DX CsSPPr")
                         };




            string _dxtarget = queryr.FirstOrDefault().dxTarget;
            string _dxsource = queryr.FirstOrDefault().dxCascSource;
            string _dxFilter = queryr.FirstOrDefault().dxCascFilter;
            string[] _dxparams = queryr.FirstOrDefault().dxCascSPParam.Split(',');

            string _paramValues = string.Empty, _dxFieldParams = string.Empty, _dxparameters = string.Empty;
            for (int i = 0; i < _dxparams.Length; i++)
            {
                if (_dxparams[i].ToString() == "USER")
                    _paramValues += "'" + _user + "'+'#'+";
                else if (_dxparams[i].ToString() == "COMP")
                    _paramValues += "'" + _comp + "'+'#'+";
                else if (_dxparams[i].ToString() == "P1")
                    _paramValues += "'P1'+'#'+";
                else if (_dxparams[i].ToString() == "P2")
                    _paramValues += "'P2'+'#'+";
                else if (_dxparams[i].ToString() == "SLANG")
                    _paramValues += "'SLANG'+'#'+";
                else if (_dxparams[i].ToString().Contains("#"))
                    _paramValues += _dxparams[i].ToString() + "+'#'+";
                else if (_dxparams[i].ToString().Contains("$"))
                {
                    _paramValues += _dxparams[i].ToString().Replace("$", "") + "+'#'+";
                }
                else
                {
                    var queryType = from p in dtRows.AsEnumerable()
                                    where p.Field<string>("DX Field ID").Contains(_dxparams[i])
                                    select new
                                    {
                                        dxType = p.Field<string>("DX Type2")
                                    };

                    string dxType = string.Empty;

                    if (queryType.Count() > 0)
                    {
                        dxType = queryType.FirstOrDefault().dxType;
                    }

                    if (dxType == "DATE")
                        _dxFieldParams += grid.ClientInstanceName + ".GetEditValue('" + _dxparams[i].ToString() + "').toLocaleDateString('EN-us')+'&" + dxType + "#'+";
                    else
                        _dxFieldParams += grid.ClientInstanceName + ".GetEditValue('" + _dxparams[i].ToString() + "')+'&" + dxType + "#'+";


                }
            }
            string scrCasc = string.Empty;
            if (_paramValues.Length > 0)
            {
                _paramValues = _paramValues.Substring(0, _paramValues.Length - 5);
            }
            else
            {
                if (_dxFieldParams.Length > 0)
                {
                    _dxFieldParams = _dxFieldParams.Substring(0, _dxFieldParams.Length - 3);
                    _dxFieldParams += "'";
                }

            }
            scrCasc = "function(s,e){  callbackPanel1.PerformCallback('" + grid.ClientInstanceName.ToString() + "@" + _dxtarget + "&" + _dxFilter + "@" + _dxsource + "@'+" + _dxFieldParams + _paramValues + "); }";
            // scrCasc = "function(s,e){alert(\"" + grid.ClientInstanceName.ToString() + "@" + _dxtarget + "&" + _dxFilter + "@" + _dxsource + "@'+" + _dxFieldParams + _paramValues + "\");}";
            _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = scrCasc;

        }
        else if (CsMsTy == 8)
        {
            var queryr = from p in dtRows.AsEnumerable()
                         where p.Field<string>("DX Field ID").Contains(fieldID) // targeta aynı field ID birden fazla yazılmamalı 
                         select new
                         {
                             dxTarget = p.Field<string>("DX Casc Target"),
                             dxCascFilter = p.Field<string>("DX Casc Filter"),
                             dxCascSource = p.Field<string>("Dx Casc Source SP"),
                             dxCascSPParam = p.Field<string>("DX CsSPPr")
                         };




            string _dxtarget = queryr.FirstOrDefault().dxTarget;
            string _dxsource = queryr.FirstOrDefault().dxCascSource;
            string _dxFilter = queryr.FirstOrDefault().dxCascFilter;
            string[] _dxparams = queryr.FirstOrDefault().dxCascSPParam.Split(',');

            string _paramValues = string.Empty, _dxFieldParams = string.Empty, _dxparameters = string.Empty;
            for (int i = 0; i < _dxparams.Length; i++)
            {
                if (_dxparams[i].ToString() == "USER")
                    _paramValues += "'" + _user + "'+'#'+";
                else if (_dxparams[i].ToString() == "COMP")
                    _paramValues += "'" + _comp + "'+'#'+";
                else if (_dxparams[i].ToString() == "P1")
                    _paramValues += "'P1'+'#'+";
                else if (_dxparams[i].ToString() == "P2")
                    _paramValues += "'P2'+'#'+";
                else if (_dxparams[i].ToString() == "SLANG")
                    _paramValues += "'SLANG'+'#'+";
                else if (_dxparams[i].ToString().Contains("#"))
                    _paramValues += _dxparams[i].ToString() + "+'#'+";
                else if (_dxparams[i].ToString().Contains("$"))
                {
                    _paramValues += _dxparams[i].ToString().Replace("$", "") + "+'#'+";
                }
                else
                {
                    var queryType = from p in dtRows.AsEnumerable()
                                    where p.Field<string>("DX Field ID").Contains(_dxparams[i])
                                    select new
                                    {
                                        dxType = p.Field<string>("DX Type2")
                                    };

                    string dxType = string.Empty;

                    if (queryType.Count() > 0)
                    {
                        dxType = queryType.FirstOrDefault().dxType;
                    }

                    if (dxType == "DATE")
                        _dxFieldParams += grid.ClientInstanceName + ".GetEditValue('" + _dxparams[i].ToString() + "').toLocaleDateString('EN-us')+'&" + dxType + "#'+";
                    else
                        _dxFieldParams += grid.ClientInstanceName + ".GetEditValue('" + _dxparams[i].ToString() + "')+'&" + dxType + "#'+";


                }
            }
            string scrCasc = string.Empty;
            if (_paramValues.Length > 0)
            {
                _paramValues = _paramValues.Substring(0, _paramValues.Length - 5);
            }
            else
            {
                if (_dxFieldParams.Length > 0)
                {
                    _dxFieldParams = _dxFieldParams.Substring(0, _dxFieldParams.Length - 3);
                    _dxFieldParams += "'";
                }

            }
            scrCasc = "function(s,e){  CallbackPanelGeneral.PerformCallback('" + grid.ClientInstanceName.ToString() + "@" + _dxtarget + "&" + _dxFilter + "@" + _dxsource + "@'+" + _dxFieldParams + _paramValues + "); }";
            // scrCasc = "function(s,e){alert(\""+ grid.ClientInstanceName.ToString() + "@" + _dxtarget + "&" + _dxFilter + "@" + _dxsource + "@'+" + _dxFieldParams + _paramValues+"\");}";
            _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = scrCasc;

        }
        try
        {
            DataRow[] rows = dtRows.Select("[DX Caption Filter]>0 AND [DX Field ID]='" + fieldID + "'");
            if (rows.Length > 0)
                _comboboxColumn.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;

            DataRow[] rowsg = dtRows.Select("[DX Field ID]='" + fieldID + "'");
            //if (rowsg[0]["DX Insert Hidden"].ToString() == "1")
            //    _comboboxColumn.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

            int dxpopupindex = Convert.ToInt32(rowsg[0]["DX Edit Popup Index"].ToString());

            if (dxpopupindex > -1)
            {
                _comboboxColumn.EditFormSettings.VisibleIndex = dxpopupindex;

            }
            else if (dxpopupindex == -2)
            {
                _comboboxColumn.EditFormSettings.VisibleIndex = 99999;
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.Init = "function(s,e){PopupDisplayNone(s, e);}";
                _comboboxColumn.EditFormCaptionStyle.CssClass = "dis-none";
            }

        }
        catch
        {


        };
        _comboboxColumn.PropertiesEdit.Style.CssClass += " toUpperCase";
        _comboboxColumn.PropertiesEdit.EnableClientSideAPI = true;
        _comboboxColumn.PropertiesComboBox.ClientSideEvents.KeyUp = "function(s, e){toUpperCombo(s,e)}";
        if (caption.Contains("*"))
        {
            _comboboxColumn.PropertiesComboBox.ClientSideEvents.Init = "function(s,e){requiredControl(s,e)}";
            _comboboxColumn.PropertiesEdit.Style.CssClass += " requiredCheck";
        }

        return _comboboxColumn;
    }


    static void PropertiesComboBox_ItemRequestedByValue(object source, ListEditItemRequestedByValueEventArgs e)
    {
        try
        {
            if (e.Value == null) return;
            if (string.IsNullOrEmpty(e.Value.ToString())) return;
            ASPxComboBox cmb = (ASPxComboBox)source;
            cmb.CallbackPageSize = 15;
            cmb.Items.Clear();
            string[] _params = null;
            if (cmb.NullText.Contains("#"))
            {
                _params = cmb.NullText.Split('#');
            }
            else return;

            var connstr = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
            var sds = new SqlDataSource();
            sds.ConnectionString = connstr;
            sds.SelectCommand = FillComboboxV2(_params[0], _params[1], _params[2], _params[3], _params[5], _params[4]);
            sds.SelectParameters.Clear();
            sds.SelectParameters.Add("ID", e.Value.ToString());
            cmb.ValueType = typeof(System.String);
            cmb.DataSource = sds;
            cmb.DataBind();

        }
        catch
        {


        }

    }

    static void PropertiesComboBox_ItemsRequestedByFilterCondition(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox cmb = (ASPxComboBox)source;
        cmb.CallbackPageSize = 15;


        string[] _params = null;
        if (cmb.NullText.Contains("#"))
        {
            _params = cmb.NullText.Split('#');
        }
        else return;

        //try
        //{
        //    var connstr = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        //    var sds = new SqlDataSource();
        //    sds.ConnectionString = connstr;
        //    cmb.Items.Clear();
        //    sds.SelectCommand = FillComboboxV1(_params[0], _params[1], _params[2], _params[3], _params[5], _params[4]);


        //    sds.SelectParameters.Clear();
        //    sds.SelectParameters.Add("filter", TypeCode.String, string.Format("%{0}%", e.Filter));
        //    sds.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        //    sds.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());
        //    cmb.ValueType = typeof(System.String);
        //    cmb.DataSource = sds;
        //    cmb.DataBind();


        //}
        //catch
        //{

        //}

        #region cache çalışması sql den daha yavaş çalıştı iptal edildi 20190503 Hakan Doğan
        string _cachekey = _params[0];
        List<CacheObject> _obj = CacheManager.Get(_cachekey, DateTime.Now);
        if (_obj.Count == 0 | CacheManager.CacheActive == 0)
        {
            try
            {
                var connstr = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                var sds = new SqlDataSource();
                sds.ConnectionString = connstr;
                cmb.Items.Clear();
                sds.SelectCommand = FillComboboxV1(_params[0], _params[1], _params[2], _params[3], _params[5], _params[4]);


                sds.SelectParameters.Clear();
                sds.SelectParameters.Add("filter", TypeCode.String, string.Format("%{0}%", e.Filter));
                sds.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
                sds.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());
                cmb.ValueType = typeof(System.String);
                cmb.DataSource = sds;
                cmb.DataBind();


            }
            catch
            {

            }
        }
        else
        {
            try
            {
                string query = "1=1";
                string[] _array = _params[3].Split(',');
                string customText = string.Empty;
                string SearchText = string.Empty;
                if (_array.Length > 1)
                {
                    customText = _array[0].ToString() + "+' - '+" + _array[1].ToString();
                    SearchText = _array[0].ToString();
                }
                else
                {
                    customText = _params[3];
                    SearchText = _params[3];
                }

                string[] filters = _params[1].Split(',');
                for (int i = 0; i < filters.Length; i++)
                {
                    switch (filters[i].ToString())
                    {
                        case "RCOMP":
                            query += " and RCOMP='" + _params[5] + "'";
                            break;
                        case "RUSER":
                            query += " and RUSER='" + _params[4] + "'";
                            break;
                    }
                }

                //query += " and " + SearchText + " like '%" + e.Filter + "%'";

                DataRow[] drs = _obj[0].obj.Select(query);
                if (_array.Length > 1)
                {
                    var _Source = (from p in drs.AsEnumerable()
                                   select new
                                   {

                                       Code = p.Field<string>(_params[2]),
                                       customText = p.Field<string>(_array[0].ToString()) + " - " + p.Field<string>(_array[1].ToString())
                                   });


                    if (!string.IsNullOrEmpty(e.Filter))
                        _Source = _Source.Where(x => x.customText.Contains(e.Filter));

                    if (string.IsNullOrEmpty(e.Filter))
                        _Source = _Source.Skip(e.BeginIndex + 1).Take(e.EndIndex + 1);

                    cmb.ValueField = "Code";
                    cmb.TextField = "customText";
                    cmb.ValueType = typeof(System.String);
                    cmb.DataSource = _Source.ToArray();
                    cmb.DataBind();

                }
                else
                {
                    var _Source = (from p in drs.AsEnumerable()
                                   select new
                                   {
                                       Code = p.Field<string>(_params[2]),
                                       customText = p.Field<string>(_params[3])
                                   });
                    if (!string.IsNullOrEmpty(e.Filter))
                        _Source = _Source.Where(x => x.customText.Contains(e.Filter));

                    if (string.IsNullOrEmpty(e.Filter))
                        _Source = _Source.Skip(e.BeginIndex + 1).Take(e.EndIndex + 1);

                    cmb.ValueField = "Code";
                    cmb.TextField = "customText";
                    cmb.ValueType = typeof(System.String);
                    cmb.DataSource = _Source.ToArray();
                    cmb.DataBind();

                }
            }
            catch
            {


            }


        }
        #endregion

    }


    static void PropertiesComboBox_ItemRequestedByValue1(object source, ListEditItemRequestedByValueEventArgs e)
    {
        try
        {
            if (e.Value == null) return;
            if (string.IsNullOrEmpty(e.Value.ToString())) return;
            ASPxComboBox cmb = (ASPxComboBox)source;
            cmb.CallbackPageSize = 15;
            cmb.Items.Clear();
            string[] _params = null;
            if (cmb.NullText.Contains("#"))
            {
                _params = cmb.NullText.Split('#');
            }
            else return;

            var connstr = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
            var sds = new SqlDataSource();
            sds.ConnectionString = connstr;
            sds.SelectCommand = FillComboboxV2(_params[1], _params[2], _params[3], _params[4], _params[11], _params[10]);
            sds.SelectParameters.Clear();
            sds.SelectParameters.Add("ID", e.Value.ToString());
            cmb.ValueType = typeof(System.String);
            cmb.DataSource = sds;
            cmb.DataBind();

        }
        catch
        {


        }

    }

    static void PropertiesComboBox_ItemsRequestedByFilterCondition1(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox cmb = (ASPxComboBox)source;
        cmb.CallbackPageSize = 15;
        string[] _params = null;
        if (cmb.NullText.Contains("#"))
        {
            _params = cmb.NullText.Split('#');
        }
        else return;

        string valpar = string.Empty;

        try
        {
            if (_params[0] == "CascColumn2")
                valpar = HttpContext.Current.Session["AddValPar2"].ToString();
            if (_params[0] == "CascColumn1")
                valpar = HttpContext.Current.Session["AddValPar1"].ToString();
        }
        catch { }

        // string sss = string.Empty;
        //throw new Exception(FillComboboxV1(_params[0], _params[1], _params[2], _params[3], _comp, _user));
        try
        {
            var connstr = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
            var sds = new SqlDataSource();
            sds.ConnectionString = connstr;
            cmb.Items.Clear();
            sds.SelectCommand = FillComboboxV3(_params[1], _params[2], _params[3], _params[4], _params[11], _params[10], valpar);
            //sss = sds.SelectCommand;


            sds.SelectParameters.Clear();
            sds.SelectParameters.Add("filter", TypeCode.String, string.Format("%{0}%", e.Filter));
            sds.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
            sds.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());
            cmb.ValueType = typeof(System.String);
            cmb.DataSource = sds;
            cmb.DataBind();

        }
        catch (Exception ex)
        {

        }

    }

    private static GridViewDataComboBoxColumn creategridviewOptionColumn(ASPxGridView grid, DataTable dtRows, DataRow row, string fieldID, string caption, int width, int halignment, string bgcolor, string Source,
        string Filter, string Value, string Text, int sayac)
    {
        GridViewDataComboBoxColumn _comboboxColumn = new GridViewDataComboBoxColumn();
        _comboboxColumn.Name = fieldID;
        _comboboxColumn.FieldName = fieldID;
        _comboboxColumn.Caption = caption;
        _comboboxColumn.VisibleIndex = columnindex;
        _comboboxColumn.PropertiesComboBox.ValueType = typeof(int);
        _comboboxColumn.PropertiesComboBox.IncrementalFilteringMode = IncrementalFilteringMode.Contains;

        if (Convert.ToInt32(row["DX DfGrI"]) > 0) _comboboxColumn.GroupIndex = Convert.ToInt32(row["DX DfGrI"]) - 1;

        double resize = (double)screenWidth * ((double)width / 100);
        _comboboxColumn.Width = Unit.Pixel(Convert.ToInt32(resize));


        if (!string.IsNullOrEmpty(bgcolor))
        {
            if (bgcolor.Substring(0, 1) == "#")
                _comboboxColumn.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);
            else
                _comboboxColumn.CellStyle.CssClass = bgcolor;
        }
        else
        {
            bgcolor = "#FFFFFF";
            _comboboxColumn.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);
        }

        HorizontalAlign _haling = halignment == 0 ? HorizontalAlign.Left :
                                  halignment == 1 ? HorizontalAlign.Center :
                                  halignment == 2 ? HorizontalAlign.Right : HorizontalAlign.NotSet;

        _comboboxColumn.CellStyle.HorizontalAlign = _haling;
        _comboboxColumn.HeaderStyle.HorizontalAlign = _haling;

        string[] valueSplitter = Value.Split(',');
        string[] textSplitter = Text.Split(',');


        for (int i = 0; i < textSplitter.Length; i++)
        {

            if (Value.Length > 0)
            {
                _comboboxColumn.PropertiesComboBox.Items.Add(textSplitter[i].ToString(), valueSplitter[i].ToString() == "" ? i : Convert.ToInt32(valueSplitter[i].ToString()));
            }
            else
            {
                _comboboxColumn.PropertiesComboBox.Items.Add(textSplitter[i].ToString(), i);
            }

        }
        copiedFields[copiedSayac] = fieldID;
        copiedSayac++;

        try
        {
            DataRow[] rows = dtRows.Select("[DX Caption Filter]>0 AND [DX Field ID]='" + fieldID + "'");
            if (rows.Length > 0)
                _comboboxColumn.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;

            DataRow[] rowsg = dtRows.Select("[DX Field ID]='" + fieldID + "'");
            //if (rowsg[0]["DX Insert Hidden"].ToString() == "1")
            //    _comboboxColumn.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

            int dxpopupindex = Convert.ToInt32(rowsg[0]["DX Edit Popup Index"].ToString());

            if (dxpopupindex > -1)
            {
                _comboboxColumn.EditFormSettings.VisibleIndex = dxpopupindex;

            }
            else if (dxpopupindex == -2)
            {
                _comboboxColumn.EditFormSettings.VisibleIndex = 99999;
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.Init = "function(s,e){PopupDisplayNone(s, e);}";
                _comboboxColumn.EditFormCaptionStyle.CssClass = "dis-none";
            }
            if (caption.Contains("*"))
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.Init = "function(s,e){requiredControl(s,e)}";
                _comboboxColumn.PropertiesEdit.Style.CssClass += " requiredCheck";
            }


        }
        catch
        {


        }

        return _comboboxColumn;
    }

    private static GridViewDataDateColumn creategridviewDateColumn(ASPxGridView grid, DataTable dtRows, DataRow row, string fieldID, string caption, int width, int halignment
        , string bgcolor, string displayFormat, string DxCascTarget, string DxCascFilter, int CascType, int CsMsTy, string _user, string _comp)
    {
        GridViewDataDateColumn _column = new GridViewDataDateColumn();
        _column.FieldName = fieldID;
        _column.Caption = caption;
        _column.VisibleIndex = columnindex;
        double resize = (double)screenWidth * ((double)width / 100);
        _column.Width = Unit.Pixel(Convert.ToInt32(resize));


        if (Convert.ToInt32(row["DX DfGrI"]) > 0) _column.GroupIndex = Convert.ToInt32(row["DX DfGrI"]) - 1;

        if (!string.IsNullOrEmpty(bgcolor))
        {
            if (bgcolor.Substring(0, 1) == "#")
                _column.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);
            else
                _column.CellStyle.CssClass = bgcolor;
        }
        else
        {
            bgcolor = "#FFFFFF";
            _column.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);
        }

        if (!string.IsNullOrEmpty(displayFormat))
        {
            _column.PropertiesEdit.DisplayFormatString = displayFormat;
            _column.PropertiesDateEdit.DisplayFormatString = displayFormat;
            _column.PropertiesDateEdit.DisplayFormatInEditMode = true;
        }

        HorizontalAlign _haling = halignment == 0 ? HorizontalAlign.Left :
                                  halignment == 1 ? HorizontalAlign.Center :
                                  halignment == 2 ? HorizontalAlign.Right : HorizontalAlign.NotSet;

        _column.CellStyle.HorizontalAlign = _haling;
        _column.HeaderStyle.HorizontalAlign = _haling;
        //_column.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;
        copiedFields[copiedSayac] = fieldID;
        copiedSayac++;

        if (CsMsTy == 7)
        {
            var queryr = from p in dtRows.AsEnumerable()
                         where p.Field<string>("DX Field ID").Contains(fieldID) // targeta aynı field ID birden fazla yazılmamalı 
                         select new
                         {
                             dxTarget = p.Field<string>("DX Casc Target"),
                             dxCascFilter = p.Field<string>("DX Casc Filter"),
                             dxCascSource = p.Field<string>("Dx Casc Source SP"),
                             dxCascSPParam = p.Field<string>("DX CsSPPr")
                         };




            string _dxtarget = queryr.FirstOrDefault().dxTarget;
            string _dxsource = queryr.FirstOrDefault().dxCascSource;
            string _dxFilter = queryr.FirstOrDefault().dxCascFilter;
            string[] _dxparams = queryr.FirstOrDefault().dxCascSPParam.Split(',');

            string _paramValues = string.Empty, _dxFieldParams = string.Empty, _dxparameters = string.Empty;
            for (int i = 0; i < _dxparams.Length; i++)
            {
                if (_dxparams[i].ToString() == "USER")
                    _paramValues += "'" + _user + "'+'#'+";
                else if (_dxparams[i].ToString() == "COMP")
                    _paramValues += "'" + _comp + "'+'#'+";
                else if (_dxparams[i].ToString() == "P1")
                    _paramValues += "'P1'+'#'+";
                else if (_dxparams[i].ToString() == "P2")
                    _paramValues += "'P2'+'#'+";
                else if (_dxparams[i].ToString().Contains("#"))
                    _paramValues += _dxparams[i].ToString() + "+'#'+";
                else if (_dxparams[i].ToString().Contains("$"))
                {
                    _paramValues += _dxparams[i].ToString().Replace("$", "") + "+'#'+";
                }
                else
                {
                    var queryType = from p in dtRows.AsEnumerable()
                                    where p.Field<string>("DX Field ID").Contains(_dxparams[i])
                                    select new
                                    {
                                        dxType = p.Field<string>("DX Type2")
                                    };

                    string dxType = string.Empty;

                    if (queryType.Count() > 0)
                    {
                        dxType = queryType.FirstOrDefault().dxType;
                    }

                    if (dxType == "DATE")
                        _dxFieldParams += grid.ClientInstanceName + ".GetEditValue('" + _dxparams[i].ToString() + "').toLocaleDateString('EN-us')+'&" + dxType + "#'+";
                    else
                        _dxFieldParams += grid.ClientInstanceName + ".GetEditValue('" + _dxparams[i].ToString() + "')+'&" + dxType + "#'+";


                }
            }
            string scrCasc = string.Empty;
            if (_paramValues.Length > 0)
            {
                _paramValues = _paramValues.Substring(0, _paramValues.Length - 5);
            }
            else
            {
                if (_dxFieldParams.Length > 0)
                {
                    _dxFieldParams = _dxFieldParams.Substring(0, _dxFieldParams.Length - 3);
                    _dxFieldParams += "'";
                }

            }
            scrCasc = "function(s,e){  callbackPanel1.PerformCallback('" + grid.ClientInstanceName.ToString() + "@" + _dxtarget + "&" + _dxFilter + "@" + _dxsource + "@'+" + _dxFieldParams + _paramValues + "); }";

            _column.PropertiesDateEdit.ClientSideEvents.DateChanged = scrCasc;

        }

        try
        {
            DataRow[] rows = dtRows.Select("[DX Caption Filter]>0 AND [DX Field ID]='" + fieldID + "'");
            if (rows.Length > 0)
                _column.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;

            DataRow[] rowsg = dtRows.Select("[DX Field ID]='" + fieldID + "'");
            //if (rowsg[0]["DX Insert Hidden"].ToString() == "1")
            //    _column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

            int dxpopupindex = Convert.ToInt32(rowsg[0]["DX Edit Popup Index"].ToString());

            if (dxpopupindex > -1)
            {
                _column.EditFormSettings.VisibleIndex = dxpopupindex;

            }
            else if (dxpopupindex == -2)
            {
                _column.EditFormSettings.VisibleIndex = 99999;
                _column.PropertiesDateEdit.ClientSideEvents.Init = "function(s,e){PopupDisplayNone(s, e);}";
                _column.EditFormCaptionStyle.CssClass = "dis-none";
            }
            if (caption.Contains("*"))
            {
                _column.PropertiesDateEdit.ClientSideEvents.Init = "function(s,e){requiredControl(s,e)}";
                _column.PropertiesEdit.Style.CssClass += " requiredCheck";
            }
        }
        catch
        {


        }
        return _column;
    }

    private static GridViewDataTimeEditColumn creategridviewTimeColumn(DataTable dtRows, DataRow row, string fieldID, string caption, int width, int halignment, string bgcolor, string displayFormat)
    {
        GridViewDataTimeEditColumn _column = new GridViewDataTimeEditColumn();
        _column.FieldName = fieldID;
        _column.Caption = caption;
        _column.VisibleIndex = columnindex;
        double resize = (double)screenWidth * ((double)width / 100);
        _column.Width = Unit.Pixel(Convert.ToInt32(resize));

        if (Convert.ToInt32(row["DX DfGrI"]) > 0) _column.GroupIndex = Convert.ToInt32(row["DX DfGrI"]) - 1;

        if (!string.IsNullOrEmpty(bgcolor))
        {
            if (bgcolor.Substring(0, 1) == "#")
                _column.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);
            else
                _column.CellStyle.CssClass = bgcolor;
        }
        else
        {
            bgcolor = "#FFFFFF";
            _column.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);
        }

        if (!string.IsNullOrEmpty(displayFormat))
        {
            _column.PropertiesEdit.DisplayFormatString = displayFormat;
            _column.PropertiesTimeEdit.DisplayFormatString = displayFormat;
            _column.PropertiesTimeEdit.DisplayFormatInEditMode = true;
        }

        HorizontalAlign _haling = halignment == 0 ? HorizontalAlign.Left :
                                  halignment == 1 ? HorizontalAlign.Center :
                                  halignment == 2 ? HorizontalAlign.Right : HorizontalAlign.NotSet;

        _column.CellStyle.HorizontalAlign = _haling;
        _column.HeaderStyle.HorizontalAlign = _haling;
        _column.PropertiesTimeEdit.DisplayFormatString = "HH:mm";
        _column.PropertiesTimeEdit.EditFormat = EditFormat.Custom;
        copiedFields[copiedSayac] = fieldID;
        copiedSayac++;

        try
        {
            DataRow[] rows = dtRows.Select("[DX Caption Filter]>0 AND [DX Field ID]='" + fieldID + "'");
            if (rows.Length > 0)
                _column.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;

            DataRow[] rowsg = dtRows.Select("[DX Field ID]='" + fieldID + "'");
            //if (rowsg[0]["DX Insert Hidden"].ToString() == "1")
            //    _column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

            int dxpopupindex = Convert.ToInt32(rowsg[0]["DX Edit Popup Index"].ToString());

            if (dxpopupindex > -1)
            {
                _column.EditFormSettings.VisibleIndex = dxpopupindex;

            }
            else if (dxpopupindex == -2)
            {
                _column.EditFormSettings.VisibleIndex = 99999;
                _column.PropertiesTimeEdit.ClientSideEvents.Init = "function(s,e){PopupDisplayNone(s, e);}";
                _column.EditFormCaptionStyle.CssClass = "dis-none";
            }
            if (caption.Contains("*"))
            {
                _column.PropertiesTimeEdit.ClientSideEvents.Init = "function(s,e){requiredControl(s,e)}";
                _column.PropertiesEdit.Style.CssClass += " requiredCheck";
            }
        }
        catch
        {


        }
        return _column;
    }

    private static GridViewDataCheckColumn creategridviewCheckColumn(DataTable dtRows, DataRow row, string fieldID, string caption, int width, int halignment, string bgcolor)
    {
        GridViewDataCheckColumn _column = new GridViewDataCheckColumn();
        _column.FieldName = fieldID;
        _column.Caption = caption;
        _column.VisibleIndex = columnindex;
        double resize = (double)screenWidth * ((double)width / 100);
        _column.Width = Unit.Pixel(Convert.ToInt32(resize));

        if (Convert.ToInt32(row["DX DfGrI"]) > 0) _column.GroupIndex = Convert.ToInt32(row["DX DfGrI"]) - 1;

        if (!string.IsNullOrEmpty(bgcolor))
        {
            if (bgcolor.Substring(0, 1) == "#")
                _column.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);
            else
                _column.CellStyle.CssClass = bgcolor;
        }
        else
        {
            bgcolor = "#FFFFFF";
            _column.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);
        }

        HorizontalAlign _haling = halignment == 0 ? HorizontalAlign.Left :
                                  halignment == 1 ? HorizontalAlign.Center :
                                  halignment == 2 ? HorizontalAlign.Right : HorizontalAlign.NotSet;

        _column.CellStyle.HorizontalAlign = _haling;
        _column.HeaderStyle.HorizontalAlign = _haling;
        copiedFields[copiedSayac] = fieldID;
        copiedSayac++;

        try
        {
            DataRow[] rows = dtRows.Select("[DX Caption Filter]>0 AND [DX Field ID]='" + fieldID + "'");
            if (rows.Length > 0)
                _column.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;

            DataRow[] rowsg = dtRows.Select("[DX Field ID]='" + fieldID + "'");
            //if (rowsg[0]["DX Insert Hidden"].ToString() == "1")
            //    _column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

            int dxpopupindex = Convert.ToInt32(rowsg[0]["DX Edit Popup Index"].ToString());

            if (dxpopupindex > -1)
            {
                _column.EditFormSettings.VisibleIndex = dxpopupindex;


            }
            else if (dxpopupindex == -2)
            {
                _column.EditFormSettings.VisibleIndex = 99999;
                _column.PropertiesCheckEdit.ClientSideEvents.Init = "function(s,e){PopupDisplayNone(s, e);}";
                _column.EditFormCaptionStyle.CssClass = "dis-none";
            }
            if (caption.Contains("*"))
            {
                _column.PropertiesCheckEdit.ClientSideEvents.Init = "function(s,e){requiredControl(s,e)}";
                _column.PropertiesEdit.Style.CssClass += " requiredCheck";
            }
        }
        catch
        {

        }

        return _column;
    }

    private static GridViewDataHyperLinkColumn creategridviewDataHyperLink(DataRow row, string fieldID, string caption, int width, string _comp, string dxfilter, string dxsource)
    {
        GridViewDataHyperLinkColumn _column = new GridViewDataHyperLinkColumn();
        _column.FieldName = fieldID;
        _column.Caption = caption;
        _column.VisibleIndex = columnindex;
        if (string.IsNullOrEmpty(dxsource))
            _column.PropertiesHyperLinkEdit.ImageUrl = "~/images/pdfico.png";
        else
            _column.PropertiesHyperLinkEdit.ImageUrl = "~/images/" + dxsource;
        _column.PropertiesHyperLinkEdit.Target = "_blank";


        switch (dxfilter)
        {
            case "NOCOMP":
                _column.PropertiesHyperLinkEdit.NavigateUrlFormatString = "{0}";
                break;
            case "NOCOMPPOPUP":
                _column.PropertiesHyperLinkEdit.Target = string.Empty;
                _column.PropertiesHyperLinkEdit.NavigateUrlFormatString = "javascript:popupWindow('{0}','PDF',window,900,650);";
                break;
            case "POPUP":
                _column.PropertiesHyperLinkEdit.Target = string.Empty;
                _column.PropertiesHyperLinkEdit.NavigateUrlFormatString = "javascript:popupWindow('" + CurrentFolderName(_comp) + "/{0}','PDF',window,900,650);";
                break;
            default:
                _column.PropertiesHyperLinkEdit.NavigateUrlFormatString = CurrentFolderName(_comp) + "/" + "{0}";
                break;
        }




        double resize = (double)screenWidth * ((double)width / 100);
        _column.Width = Unit.Pixel(Convert.ToInt32(resize));
        if (Convert.ToInt32(row["DX DfGrI"]) > 0) _column.GroupIndex = Convert.ToInt32(row["DX DfGrI"]) - 1;
        return _column;
    }

    private static GridViewDataHyperLinkColumn createHyperLinkColumn(DataTable dtRows, DataRow row, string fieldID, string caption, int width
        , int halignment, string bgcolor, string FieldLink, string _source, string _objKey)
    {
        GridViewDataHyperLinkColumn _column = new GridViewDataHyperLinkColumn();
        _column.FieldName = FieldLink;
        _column.Name = fieldID;
        _column.Caption = caption;
        _column.VisibleIndex = columnindex;
        _column.PropertiesHyperLinkEdit.TextField = fieldID;
        _column.PropertiesHyperLinkEdit.NavigateUrlFormatString = "{0}" + "&GId=" + _objKey;
        _column.PropertiesHyperLinkEdit.Target = "_blank";
        if (!string.IsNullOrEmpty(_source))
            _column.PropertiesHyperLinkEdit.ImageUrl = "~/images/" + _source;
        //_column.PropertiesHyperLinkEdit.TextFormatString = "{0}";
        if (Convert.ToInt32(row["DX DfGrI"]) > 0) _column.GroupIndex = Convert.ToInt32(row["DX DfGrI"]) - 1;


        double resize = (double)screenWidth * ((double)width / 100);
        _column.Width = Unit.Pixel(Convert.ToInt32(resize));
        if (!string.IsNullOrEmpty(bgcolor))
        {
            if (bgcolor.Substring(0, 1) == "#")
                _column.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);
            else
                _column.CellStyle.CssClass = bgcolor;
        }
        else
        {
            bgcolor = "#FFFFFF";
            _column.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);
        }

        HorizontalAlign _haling = halignment == 0 ? HorizontalAlign.Left :
                                  halignment == 1 ? HorizontalAlign.Center :
                                  halignment == 2 ? HorizontalAlign.Right : HorizontalAlign.NotSet;

        _column.CellStyle.HorizontalAlign = _haling;
        _column.HeaderStyle.HorizontalAlign = _haling;
        _column.CellStyle.Wrap = DevExpress.Utils.DefaultBoolean.False;
        _column.Settings.AutoFilterCondition = AutoFilterCondition.Contains;


        try
        {
            DataRow[] rows = dtRows.Select("[DX Caption Filter]>0 AND [DX Field ID]='" + fieldID + "'");
            if (rows.Length > 0)
                _column.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;

            DataRow[] rowsg = dtRows.Select("[DX Field ID]='" + fieldID + "'");
            //if (rowsg[0]["DX Insert Hidden"].ToString() == "1")
            //    _column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

            int dxpopupindex = Convert.ToInt32(rowsg[0]["DX Edit Popup Index"].ToString());

            if (dxpopupindex > -1)
            {
                _column.EditFormSettings.VisibleIndex = dxpopupindex;

            }
            else if (dxpopupindex == -2)
            {
                _column.EditFormSettings.VisibleIndex = 99999;
                _column.PropertiesHyperLinkEdit.ClientSideEvents.Init = "function(s,e){PopupDisplayNone(s, e);}";
                _column.EditFormCaptionStyle.CssClass = "dis-none";
            }
            if (caption.Contains("*"))
            {
                _column.PropertiesHyperLinkEdit.ClientSideEvents.Init = "function(s,e){requiredControl(s,e)}";
                _column.PropertiesEdit.Style.CssClass += " requiredCheck";
            }


        }
        catch
        {


        }

        return _column;
    }

    public static string FillCombobox(string Source, string Filter, string value, string text, string sirket, string user)
    {
        string[] _array = text.Split(',');
        string customText = string.Empty;
        if (_array.Length > 1)
        {
            customText = _array[0].ToString() + "+' - '+" + _array[1].ToString();
        }
        else
        {
            customText = text;
        }
        string query = "select *," + customText + " as customText from [" + Source + "] where 1=1";
        string[] filters = Filter.Split(',');

        for (int i = 0; i < filters.Length; i++)
        {
            switch (filters[i].ToString())
            {
                case "RCOMP":
                    query = query + " and RCOMP='" + sirket + "' ";
                    break;
                case "RUSER":
                    query = query + " and RUSER='" + user + "'";
                    break;
            }
        }

        return query;
    }

    public static string FillComboboxV1(string Source, string Filter, string value, string text, string sirket, string user)
    {
        string[] _array = text.Split(',');
        string customText = string.Empty;
        string SearchText = string.Empty;
        if (_array.Length > 1)
        {
            customText = _array[0].ToString() + "+' - '+" + _array[1].ToString();
            SearchText = _array[0].ToString();
        }
        else
        {
            customText = text;
            SearchText = text;
        }
        string query = "select * from (select " + value + "," + customText + " as customText, row_number() over(order by " + value + ") as [rn] from [" + Source + "]  where 1=1";
        string[] filters = Filter.Split(',');

        for (int i = 0; i < filters.Length; i++)
        {
            switch (filters[i].ToString())
            {
                case "RCOMP":
                    query = query + " and RCOMP='" + sirket + "' ";
                    break;
                case "COMPANY":
                    query = query + " and COMPANY='" + sirket + "' ";
                    break;
                case "RUSER":
                    query = query + " and RUSER='" + user + "'";
                    break;
            }
        }

        query += " and " + SearchText + " like @filter ) as st where st.[rn] between @startIndex and @endIndex ";

        return query;
    }

    public static string FillComboboxV2(string Source, string Filter, string value, string text, string sirket, string user)
    {
        string[] _array = text.Split(',');
        string customText = string.Empty;
        string SearchText = string.Empty;
        if (_array.Length > 1)
        {
            customText = _array[0].ToString() + "+' - '+" + _array[1].ToString();
            SearchText = _array[0].ToString();
        }
        else
        {
            customText = text;
            SearchText = text;
        }
        string query = "select " + value + "," + customText + " as customText from [" + Source + "]  where 1=1";
        string[] filters = Filter.Split(',');

        for (int i = 0; i < filters.Length; i++)
        {
            switch (filters[i].ToString())
            {
                case "RCOMP":
                    query = query + " and RCOMP='" + sirket + "' ";
                    break;
                case "COMPANY":
                    query = query + " and COMPANY='" + sirket + "' ";
                    break;
                case "RUSER":
                    query = query + " and RUSER='" + user + "'";
                    break;
            }
        }

        query += " and " + value + "=@ID ";

        return query;
    }

    public static string FillComboboxV3(string Source, string Filter, string value, string text, string sirket, string user, string valpar)
    {
        string[] _array = text.Split(',');
        string customText = string.Empty;
        string SearchText = string.Empty;
        if (_array.Length > 1)
        {
            customText = _array[0].ToString() + "+' - '+" + _array[1].ToString();
            SearchText = _array[0].ToString();
        }
        else
        {
            customText = text;
            SearchText = text;
        }
        string query = "select * from (select " + value + "," + customText + " as customText, " +
            "row_number() over(order by " + value + ") as [rn] from [" + Source + "]  where 1=1";
        string[] filters = Filter.Split(',');

        for (int i = 0; i < filters.Length; i++)
        {
            switch (filters[i].ToString())
            {
                case "RCOMP":
                    query = query + " and RCOMP='" + sirket + "' ";
                    break;
                case "RUSER":
                    query = query + " and RUSER='" + user + "'";
                    break;
            }
        }

        if (!string.IsNullOrEmpty(valpar)) query += valpar;

        query += " and " + SearchText + " like @filter ) as st where st.[rn] between @startIndex and @endIndex ";

        return query;
    }

    private static DevExpress.Web.MenuItem createMenuItem(string type, string Type2, string text
        , string fieldID, string source, string gridId, string dxfilter, string dxTrFlVw, string dxpath, string _dxpf, string _comp, string _user, int _dxWidth, string _dxPath2, string _dxGroupParent, string _ObjeID)
    {
        DevExpress.Web.MenuItem _menu = new DevExpress.Web.MenuItem();
        switch (type)
        {
            case "LABEL":
                _menu.Text = text;
                _menu.Enabled = false;
                break;
            case "BUTTON":
                if (Type2 == "PDF")
                {

                    _menu.Text = "<i class=\"far fa-file-pdf\"></i>";
                    _menu.ItemStyle.CssClass += " convertIcon";
                    _menu.ToolTip = "PDF";
                    if (!string.IsNullOrEmpty(_dxpf))
                        _menu.Name = "btnPdf|" + _dxpf;
                    else
                        _menu.Name = "btnPdf";
                    if (!string.IsNullOrEmpty(_dxpf))
                        _menu.Visible = DynamicPageYetkiKontrol(_comp, _user, _dxpf);
                }
                else if (Type2 == "EXCEL")
                {
                    _menu.Text = "<i class=\"far fa-file-excel\"></i>";
                    _menu.ItemStyle.CssClass += " convertIcon";
                    _menu.ToolTip = "EXCEL";
                    if (!string.IsNullOrEmpty(_dxpf))
                        _menu.Name = "btnExcel|" + _dxpf;
                    else
                        _menu.Name = "btnExcel";
                    if (!string.IsNullOrEmpty(_dxpf))
                        _menu.Visible = DynamicPageYetkiKontrol(_comp, _user, _dxpf);
                }
                else if (Type2 == "INSERT")
                {
                    _menu.Image.Url = "~/images/add_16.png";
                    _menu.Name = "btninsert";
                    _menu.Text = "";
                    if (!string.IsNullOrEmpty(_dxpf))
                        _menu.Visible = DynamicPageYetkiKontrol(_comp, _user, _dxpf);
                }
                else if (Type2 == "INSERTPOPUP")
                {
                    _menu.Image.Url = "~/images/add_16.png";
                    _menu.NavigateUrl = "javascript:OpenFirstCollapse();";
                    _menu.Name = fieldID;
                    _menu.Text = "";
                    if (!string.IsNullOrEmpty(_dxpf))
                        _menu.Visible = DynamicPageYetkiKontrol(_comp, _user, _dxpf);
                }
                else if (Type2 == "DIRECT")
                {
                    if (dxfilter.Contains("#"))
                        _menu.NavigateUrl = "javascript:OpenDirectLinkWithTransferParams('" + source + "','" + dxfilter.Replace("#", string.Empty) + "','" + gridId.Substring(gridId.Length == 5 ? gridId.Length - 1 : gridId.Length - 2) + "')";
                    else
                        _menu.NavigateUrl = "javascript:OpenDirectLink('" + source + "')";
                    if (dxTrFlVw.Contains("#"))
                        _menu.Name = "btnSelected" + fieldID + "|" + dxTrFlVw.Replace("#", string.Empty);
                    else
                        _menu.Name = "btnSelected" + fieldID;

                    string uzunadi = "";
                    string kisaadi = "";
                    if (text.Contains("|"))
                    {
                        uzunadi = text.Split('|')[0];
                        kisaadi = text.Split('|')[1];
                        _menu.Text = kisaadi;
                        _menu.ToolTip = uzunadi;
                    }
                    else
                    {
                        _menu.Text = text;
                        _menu.ToolTip = text;
                    }

                    if (_dxWidth > 0)
                    {
                        _menu.ItemStyle.Width = _dxWidth;
                        _menu.ItemStyle.CssClass += " ozel";
                    }
                    if (!string.IsNullOrEmpty(_dxPath2))
                    {
                        _menu.Text = "<i class=\"" + _dxPath2 + "\"></i>";
                        _menu.ItemStyle.CssClass += " convertIcon";
                    }

                    if (!string.IsNullOrEmpty(_dxpf))
                        _menu.Visible = DynamicPageYetkiKontrol(_comp, _user, _dxpf);

                    if (!string.IsNullOrEmpty(_dxGroupParent))
                    {
                        _menu.ItemStyle.CssClass += " SG_" + gridId + "_" + _dxGroupParent;
                    }
                }
                else if (Type2 == "INSERTFILE")
                {
                    HttpContext.Current.Session["Upd" + fieldID + "FilePath"] = dxpath;
                    _menu.NavigateUrl = "javascript:OpenFileUpload('" + fieldID + "','" + gridId.Substring(gridId.Length == 5 ? gridId.Length - 1 : gridId.Length - 2) + "');";
                    // Butona icon getirme - dxpath değişecek
                    string uzunadi = "";
                    string kisaadi = "";
                    if (text.Contains("|"))
                    {
                        uzunadi = text.Split('|')[0];
                        kisaadi = text.Split('|')[1];
                        _menu.Text = kisaadi;
                        _menu.ToolTip = uzunadi;
                    }
                    else
                    {
                        _menu.Text = text;
                        _menu.ToolTip = text;
                    }

                    if (_dxWidth > 0)
                    {
                        _menu.ItemStyle.Width = _dxWidth;
                        _menu.ItemStyle.CssClass += "ozel";
                    }
                    if (!string.IsNullOrEmpty(_dxPath2))
                    {
                        _menu.Text = "<i class=\"" + _dxPath2 + "\"></i>";
                        _menu.ItemStyle.CssClass += " convertIcon";
                    }
                    if (!string.IsNullOrEmpty(_dxGroupParent))
                    {
                        _menu.ItemStyle.CssClass += " SG_" + gridId + "_" + _dxGroupParent;
                    }
                }
                else if (Type2 == "REFRESH")
                {

                    _menu.NavigateUrl = "javascript:" + gridId + ".PerformCallback('" + fieldID + "')";
                    _menu.Name = "btnSelected" + fieldID;
                    _menu.Text = text;
                    _menu.Image.Url = "~/images/refresh.png";
                    if (_dxWidth > 0)
                    {
                        _menu.ItemStyle.Width = _dxWidth;
                        _menu.ItemStyle.CssClass += " ozel";
                    }

                }
                else if (Type2 == "SELECT")
                {
                    _menu.NavigateUrl = "javascript:" + gridId + ".PerformCallback('SelectButton|" + fieldID + "')";
                    if (dxTrFlVw.Contains("#"))
                        _menu.Name = "btnSelected" + fieldID + "|" + dxTrFlVw.Replace("#", string.Empty);
                    else
                        _menu.Name = "btnSelected" + fieldID;

                    string uzunadi = "";
                    string kisaadi = "";
                    if (text.Contains("|"))
                    {
                        uzunadi = text.Split('|')[0];
                        kisaadi = text.Split('|')[1];
                        _menu.Text = kisaadi;
                        _menu.ToolTip = uzunadi;
                    }
                    else
                    {
                        _menu.Text = text;
                        _menu.ToolTip = text;
                    }

                    if (_dxWidth > 0)
                    {
                        _menu.ItemStyle.Width = _dxWidth;
                        _menu.ItemStyle.CssClass += " ozel";
                    }
                    if (!string.IsNullOrEmpty(_dxPath2))
                    {
                        _menu.Text = "<i class=\"" + _dxPath2 + "\"></i>";
                        _menu.ItemStyle.CssClass += " convertIcon";
                    }
                    if (!string.IsNullOrEmpty(_dxGroupParent))
                    {
                        _menu.ItemStyle.CssClass += " SG_" + gridId + "_" + _dxGroupParent;
                    }
                }
                else if (Type2 == "SELECTDIRECT")
                {
                    _menu.NavigateUrl = "javascript:" + gridId + ".PerformCallback('SelectDirectButton|" + fieldID + "|" + source + "|" + dxfilter.Split('|')[0].Replace("#", string.Empty) + "');";
                    _menu.Name = "btnSelected" + fieldID;

                    string uzunadi = "";
                    string kisaadi = "";
                    if (text.Contains("|"))
                    {
                        uzunadi = text.Split('|')[0];
                        kisaadi = text.Split('|')[1];
                        _menu.Text = kisaadi;
                        _menu.ToolTip = uzunadi;
                    }
                    else
                    {
                        _menu.Text = text;
                        _menu.ToolTip = text;
                    }

                    if (_dxWidth > 0)
                    {
                        _menu.ItemStyle.Width = _dxWidth;
                        _menu.ItemStyle.CssClass += " ozel";
                    }
                    if (!string.IsNullOrEmpty(_dxPath2))
                    {
                        _menu.Text = "<i class=\"" + _dxPath2 + "\"></i>";
                        _menu.ItemStyle.CssClass += " convertIcon";
                    }
                    if (!string.IsNullOrEmpty(_dxGroupParent))
                    {
                        _menu.ItemStyle.CssClass += " SG_" + gridId + "_" + _dxGroupParent;
                    }
                }
                else if (Type2 == "REPORT")
                {
                    _menu.NavigateUrl = "javascript:" + gridId + ".PerformCallback('ReportButton|" + fieldID + "|" + source + "|" + dxfilter.Split('|')[0] + "|" + dxpath + "');";
                    _menu.Name = "btnSelected" + fieldID;
                    string uzunadi = "";
                    string kisaadi = "";
                    if (text.Contains("|"))
                    {
                        uzunadi = text.Split('|')[0];
                        kisaadi = text.Split('|')[1];
                        _menu.Text = kisaadi;
                        _menu.ToolTip = uzunadi;
                    }
                    else
                    {
                        _menu.Text = text;
                        _menu.ToolTip = text;
                    }

                    if (_dxWidth > 0)
                    {
                        _menu.ItemStyle.Width = _dxWidth;
                        _menu.ItemStyle.CssClass += " ozel";
                    }
                    if (!string.IsNullOrEmpty(_dxPath2))
                    {
                        _menu.Text = "<i class=\"" + _dxPath2 + "\"></i>";
                        _menu.ItemStyle.CssClass += " convertIcon";
                    }
                    if (!string.IsNullOrEmpty(_dxGroupParent))
                    {
                        _menu.ItemStyle.CssClass += " SG_" + gridId + "_" + _dxGroupParent;
                    }
                }
                else if (Type2 == "DEVEXPOPUP")
                {
                    _menu.NavigateUrl = "javascript:" + gridId + ".PerformCallback('DevexPopup|" + fieldID + "|" + source + "')";
                    _menu.Name = "btnDevexPopup" + fieldID;
                    string uzunadi = "";
                    string kisaadi = "";
                    if (text.Contains("|"))
                    {
                        uzunadi = text.Split('|')[0];
                        kisaadi = text.Split('|')[1];
                        _menu.Text = kisaadi;
                        _menu.ToolTip = uzunadi;
                    }
                    else
                    {
                        _menu.Text = text;
                        _menu.ToolTip = text;
                    }

                    if (_dxWidth > 0)
                    {
                        _menu.ItemStyle.Width = _dxWidth;
                        _menu.ItemStyle.CssClass += " ozel";
                    }
                    if (!string.IsNullOrEmpty(_dxPath2))
                    {
                        _menu.Text = "<i class=\"" + _dxPath2 + "\"></i>";
                        _menu.ItemStyle.CssClass += " convertIcon";
                    }
                    if (!string.IsNullOrEmpty(_dxGroupParent))
                    {
                        _menu.ItemStyle.CssClass += " SG_" + gridId + "_" + _dxGroupParent;
                    }
                }
                else if (Type2 == "PDFMERGE")
                {
                    _menu.NavigateUrl = "javascript:" + gridId + ".PerformCallback('PdfMerge|" + fieldID + "|" + source + "')";
                    _menu.Name = "btnPdfMerge" + fieldID;
                    string uzunadi = "";
                    string kisaadi = "";
                    if (text.Contains("|"))
                    {
                        uzunadi = text.Split('|')[0];
                        kisaadi = text.Split('|')[1];
                        _menu.Text = kisaadi;
                        _menu.ToolTip = uzunadi;
                    }
                    else
                    {
                        _menu.Text = text;
                        _menu.ToolTip = text;
                    }

                    if (_dxWidth > 0)
                    {
                        _menu.ItemStyle.Width = _dxWidth;
                        _menu.ItemStyle.CssClass += " ozel";
                    }
                    if (!string.IsNullOrEmpty(_dxPath2))
                    {
                        _menu.Text = "<i class=\"" + _dxPath2 + "\"></i>";
                        _menu.ItemStyle.CssClass += " convertIcon";
                    }
                    if (!string.IsNullOrEmpty(_dxGroupParent))
                    {
                        _menu.ItemStyle.CssClass += " SG_" + gridId + "_" + _dxGroupParent;
                    }
                }
                else if (Type2 == "WEBMETHOD")
                {
                    _menu.Name = "btnWebMethod" + fieldID;
                    string uzunadi = "";
                    string kisaadi = "";
                    if (text.Contains("|"))
                    {
                        uzunadi = text.Split('|')[0];
                        kisaadi = text.Split('|')[1];
                        _menu.Text = kisaadi;
                        _menu.ToolTip = uzunadi;
                    }
                    else
                    {
                        _menu.Text = text;
                        _menu.ToolTip = text;
                    }

                    if (_dxWidth > 0)
                    {
                        _menu.ItemStyle.Width = _dxWidth;
                        _menu.ItemStyle.CssClass += " ozel";
                    }
                    if (!string.IsNullOrEmpty(_dxPath2))
                    {
                        _menu.Text = "<i class=\"" + _dxPath2 + "\"></i>";
                        _menu.ItemStyle.CssClass += " convertIcon";
                    }
                    if (!string.IsNullOrEmpty(_dxGroupParent))
                    {
                        _menu.ItemStyle.CssClass += " SG_" + gridId + "_" + _dxGroupParent;
                    }
                }
                else if (Type2 == "BATCHSAVE")
                {
                    _menu.Image.Url = "~/images/update.gif";
                    _menu.NavigateUrl = "javascript:" + gridId + ".UpdateEdit();";
                    _menu.Name = "SaveChange" + fieldID;
                    _menu.Text = text;
                    if (_dxpf.Contains("#"))
                    {
                        _menu.NavigateUrl = "javascript:BatchEditControl('" + gridId + "','" + _dxpf.Replace("#", string.Empty) + "');";
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(_dxpf))
                            _menu.Visible = DynamicPageYetkiKontrol(_comp, _user, _dxpf);
                    }

                }
                else if (Type2 == "BATCHCANCEL")
                {
                    _menu.Image.Url = "~/images/cancelnew.png";
                    _menu.NavigateUrl = "javascript:" + gridId + ".CancelEdit();";
                    _menu.Name = "CancelChange" + fieldID;
                    _menu.Text = text;
                    if (_dxpf.Contains("#"))
                    {
                        _menu.NavigateUrl = "javascript:BatchEditControl('" + gridId + "','" + _dxpf.Replace("#", string.Empty) + "');";
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(_dxpf))
                            _menu.Visible = DynamicPageYetkiKontrol(_comp, _user, _dxpf);
                    }
                }
                else if (Type2 == "GROUP")
                {
                    string uzunadi = "";
                    string kisaadi = "";
                    if (text.Contains("|"))
                    {
                        uzunadi = text.Split('|')[0];
                        kisaadi = text.Split('|')[1];
                        _menu.Text = kisaadi;
                        _menu.ToolTip = uzunadi;
                    }
                    else
                    {
                        _menu.Text = text;
                        _menu.ToolTip = text;
                    }

                    if (_dxWidth > 0)
                    {
                        _menu.ItemStyle.Width = _dxWidth;
                        _menu.ItemStyle.CssClass += " ozel";
                    }
                    if (!string.IsNullOrEmpty(_dxPath2))
                    {
                        _menu.Text = "<i class=\"" + _dxPath2 + "\"></i>";
                        _menu.ItemStyle.CssClass += " convertIcon";
                    }
                    _menu.ItemStyle.CssClass += " MG_" + gridId + "_" + fieldID + "_MG groupMainButton convertIcon";
                    _menu.Text += "<i class=\"fa fa-angle-double-down\" style=\"margin-left:5px !important;\"></i>";
                    _menu.Enabled = false;
                }
                else if (Type2 == "DRAGUPLOAD")
                {
                    _menu.ItemStyle.CssClass = "flpUpload Params_" + _ObjeID + "_" + fieldID;
                    _menu.Enabled = false;
                }
                break;
        }
        return _menu;
    }

    private static void leftmenuSettings(ASPxGridView grid, DataTable dtRows, bool select, bool edit, bool delete, bool insert
        , bool insertHeader, bool deleteHeader, bool clone, bool clear, bool _view, string _selectedLang)
    {
        GridViewCommandColumn _column = new GridViewCommandColumn();

        _column.Name = "cmnd";
        _column.ShowClearFilterButton = true;
        _column.ShowSelectCheckbox = select;
        _column.ShowSelectButton = select;
        _column.ShowDeleteButton = delete;
        _column.ShowEditButton = edit;
        _column.ShowNewButton = insert;


        _column.ShowNewButtonInHeader = insertHeader;
        if (clone) _column.CustomButtons.Add(commandcolumncustomButton());
        if (_view) _column.CustomButtons.Add(commandcolumnviewButton());

        if (select)
        {
            _column.CustomButtons.Add(CreateCommandCustomButton("SelectAll", "Select All", true, "~/images/plus.png"));
            _column.CustomButtons.Add(CreateCommandCustomButton("UnSelectAll", "Remove Selection", true, "~/images/minus.png"));
            _column.Width = Unit.Pixel(85);
        }

        _column.ShowCancelButton = true;
        _column.ShowUpdateButton = true;
        _column.ButtonRenderMode = GridCommandButtonRenderMode.Image;
        _column.VisibleIndex = 0;
        if (!select)
            _column.Width = Unit.Pixel(Convert.ToInt32(dtRows.Select("[DX Field ID]='R0000'")[0]["DX Width"]));

        grid.Columns.Add(_column);

        grid.SettingsCommandButton.DeleteButton.Image.Url = "/images/trash.png";
        grid.SettingsCommandButton.EditButton.Image.Url = "/images/edit.png";
        grid.SettingsCommandButton.ClearFilterButton.Image.Url = "/images/clear_filter.png";
        grid.SettingsCommandButton.NewButton.Image.Url = "/images/add_16.png";
        grid.SettingsCommandButton.ClearFilterButton.Image.Url = "/images/clear_filter.png";
        if (dtRows.Select("[DX Type2]='DELETE'").Count() > 0)
        {
            grid.SettingsText.ConfirmDelete = dtRows.Select("[DX Type2]='DELETE'")[0][_selectedLang].ToString();
        }


    }

    private static GridViewCommandColumnCustomButton commandcolumncustomButton()
    {
        GridViewCommandColumnCustomButton _cmdColumn = new GridViewCommandColumnCustomButton();
        _cmdColumn.ID = "Clone";
        _cmdColumn.Image.Url = "~/images/clone.png";
        _cmdColumn.Text = "Clone";
        //_cmdColumn.Visibility = GridViewCustomButtonVisibility.FilterRow;

        return _cmdColumn;
    }

    private static GridViewCommandColumnCustomButton commandcolumnviewButton()
    {
        GridViewCommandColumnCustomButton _cmdColumn = new GridViewCommandColumnCustomButton();
        _cmdColumn.ID = "View";
        _cmdColumn.Image.Url = "~/images/View.png";
        _cmdColumn.Text = "View";
        //_cmdColumn.Visibility = GridViewCustomButtonVisibility.FilterRow;

        return _cmdColumn;
    }

    private static GridViewCommandColumnCustomButton CreateCommandCustomButton(string Id, string ColumnText, bool IsFilterRow, string iconpath)
    {
        GridViewCommandColumnCustomButton _cmdColumn = new GridViewCommandColumnCustomButton();
        _cmdColumn.ID = Id;
        _cmdColumn.Image.Url = iconpath;
        _cmdColumn.Text = ColumnText;
        if (IsFilterRow) _cmdColumn.Visibility = GridViewCustomButtonVisibility.FilterRow;
        return _cmdColumn;
    }

    private GridViewCommandColumnCustomButton CreateCommandNewColumn()
    {
        GridViewCommandColumnCustomButton _cmdColumn = new GridViewCommandColumnCustomButton();
        _cmdColumn.ID = "AddNewRow";
        _cmdColumn.Image.Url = "~/images/add_16.png";
        _cmdColumn.Text = "AddNewRow";
        _cmdColumn.Visibility = GridViewCustomButtonVisibility.FilterRow;


        return _cmdColumn;
    }

    public static int GetPageGroupedColumn(string PageId, string ObjectId, string Sirket, string UserName)
    {
        int _result = 0;
        try
        {
            using (DataTable dt = new DataTable())
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "[sp_0C_50057_00_WEBPAGES_04]";
                        cmd.Connection = conn;
                        cmd.CommandTimeout = 1000;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@PageId", PageId);
                        cmd.Parameters.AddWithValue("@ObjectId", ObjectId);
                        cmd.Parameters.AddWithValue("@Sirket", Sirket);
                        cmd.Parameters.AddWithValue("@UserName", UserName);

                        using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                        {
                            adp.Fill(dt);
                        }

                    }
                }

                _result = Convert.ToInt32(Convert.ToDouble(dt.Rows[0]["Total"].ToString().Replace(".", ",")));
            }
        }
        catch
        {
            _result = 0;
        }
        return _result;
    }

    public static bool IsOpenNewTab(string _PageId, string _ObjectId, string _Sirket)
    {
        bool _result = false;
        try
        {
            fonksiyonlar fs = new fonksiyonlar();
            int _count = Convert.ToInt32(fs.tekbirsonucdonder("select count(*) from [0C_50057_00_WEBPAGES] where COMPANY='" + _Sirket + "' and " +
                            "[Web Page ID]='" + _PageId + "' and [Web Object ID]='" + _ObjectId + "' and [Open New Tab]>0"));

            if (_count > 0) _result = true;
        }
        catch
        {
            _result = false;
        }
        return _result;
    }

    public static int getSplitterWidth(string _PageId, string _ObjectId, string _Sirket)
    {
        int _result = 27;
        try
        {
            fonksiyonlar fs = new fonksiyonlar();
            decimal _count = 0;
            _count = Convert.ToDecimal(fs.tekbirsonucdonder("select [Split Width] from [0C_50057_00_WEBPAGES] where COMPANY='" + _Sirket + "' and " +
                         "[Web Page ID]='" + _PageId + "' and [Web Object ID]='" + _ObjectId + "' and [Open New Tab]=0"));
            _result = Convert.ToInt32(_count);
            if (_result == 0)
            {
                _result = 27;
            }
        }
        catch
        {
            _result = 27;
        }
        return _result;
    }
    public static Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
    {
        Web_GenericFunction_PortClient webFonksiyonlari;
        BasicHttpBinding _binding;
        ws_baglantı ws = new ws_baglantı();
        _binding = new BasicHttpBinding();
        _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
        _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
        EndpointAddress _endpoint = new EndpointAddress(new Uri(ws.sirketal(sirket)));
        webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
        webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
        webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
        return webFonksiyonlari;
    }

    public static void InsertWebTopluIslem(Guid IslemId, int PSize, List<object> ArrayValues, bool HasYtk
        , List<string> ServisObjs, string FieldId, string FunctionName, string _user, string _comp)
    {
        int newPsize = PSize;
        int LastIndex = 1;
        int ObjType = 0;
        #region decide obj type
        try
        {
            foreach (object[] item in ArrayValues)
            {
                break;
            }
            ObjType = 1;
        }
        catch
        {
            try
            {
                foreach (string[] item in ArrayValues)
                {
                    break;
                }
                ObjType = 2;
            }
            catch
            {

                ObjType = 3;
            }


        }
        #endregion
        string _params = "";
        switch (ObjType)
        {
            case 1:
                foreach (object[] item in ArrayValues)
                {
                    newPsize = PSize;
                    if (HasYtk)
                    {
                        if (Convert.ToInt32(item[newPsize - 1].ToString()) == 0) continue;
                        newPsize = newPsize - 1;
                    }
                    _params = IslemId.ToString() + "§" + _comp + "§" + _user + "§" + FunctionName;

                    for (int i = 1; i <= newPsize; i++)
                    {
                        _params += "§" + item[i - 1].ToString();
                        LastIndex = i;
                    }
                    LastIndex++;
                    foreach (string str in ServisObjs)
                    {
                        if (str == "IslemID" | str == "USER") continue;
                        if (str == "DOSYAYOLU")
                            _params += "§" + HttpContext.Current.Session["Upd" + FieldId + "FilePath2"].ToString();

                        else if (str == "DOSYAADI")
                            _params += "§" + HttpContext.Current.Session["Upd" + FieldId + "FileName"].ToString();

                        else
                            _params += "§" + str;

                        LastIndex++;
                    }
                    int finish = 8 - (_params.Split('§').Length - 1);
                    for (int i = 1; i <= finish; i++)
                    {
                        _params += "§";
                    }
                    GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_user, "50007", "InsertWebTopluIslem§" + _params, _comp);
                }
                break;
            case 2:
                foreach (string[] item in ArrayValues)
                {
                    newPsize = PSize;
                    if (HasYtk)
                    {
                        if (Convert.ToInt32(item[newPsize - 1].ToString()) == 0) continue;
                        newPsize = newPsize - 1;
                    }

                    _params = IslemId.ToString() + "§" + _comp + "§" + _user + "§" + FunctionName;

                    for (int i = 1; i <= newPsize; i++)
                    {
                        _params += "§" + item[i - 1].ToString();

                        LastIndex = i;
                    }
                    LastIndex++;
                    foreach (string str in ServisObjs)
                    {
                        if (str == "IslemID" | str == "USER") continue;
                        if (str == "DOSYAYOLU")
                            _params += "§" + HttpContext.Current.Session["Upd" + FieldId + "FilePath2"].ToString();

                        else if (str == "DOSYAADI")
                            _params += "§" + HttpContext.Current.Session["Upd" + FieldId + "FileName"].ToString();

                        else
                            _params += "§" + str;
                        LastIndex++;
                    }
                    int finish = 8 - (_params.Split('§').Length - 1);
                    for (int i = 1; i <= finish; i++)
                    {
                        _params += "§";
                    }
                    GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_user, "50007", "InsertWebTopluIslem§" + _params, _comp);

                }
                break;
            case 3:
                foreach (object item in ArrayValues)
                {
                    _params = IslemId.ToString() + "§" + _comp + "§" + _user + "§" + FunctionName + "§" + item.ToString();

                    LastIndex = 2;
                    foreach (string str in ServisObjs)
                    {
                        if (str == "IslemID" | str == "USER") continue;
                        if (str == "DOSYAYOLU")
                            _params += "§" + HttpContext.Current.Session["Upd" + FieldId + "FilePath2"].ToString();

                        else if (str == "DOSYAADI")
                            _params += "§" + HttpContext.Current.Session["Upd" + FieldId + "FileName"].ToString();

                        else
                            _params += "§" + str;

                        LastIndex++;
                    }
                    int finish = 8 - (_params.Split('§').Length - 1);
                    for (int i = 1; i <= finish; i++)
                    {
                        _params += "§";
                    }
                    GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_user, "50007", "InsertWebTopluIslem§" + _params, _comp);
                }
                break;
        }

    }

    public static DataSet GetDynamicWebPageObjects(string PageId, string ObjectId, string _user, string _comp)
    {

        string _cachekey = "sp_0C_50032_00_WEB PAGE CAPTIONS" + "_" + PageId + "_" + ObjectId + "_" + _comp;
        List<CacheObject> _obj = CacheManager.Get(_cachekey, DateTime.Now);
        if (_obj.Count == 0 | CacheManager.CacheActive == 0)
        {

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
            {

                using (DataSet ds = new DataSet())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "[sp_0C_50032_00_WEB PAGE CAPTIONS]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 1000;
                        cmd.Connection = conn;
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@Sirket", _comp);
                        cmd.Parameters.AddWithValue("@PageId", PageId);
                        cmd.Parameters.AddWithValue("@ObjectId", ObjectId);
                        using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                        {
                            adp.Fill(ds);

                            CacheManager.SetDynamic(ds, _cachekey, 24 * 60, 1);
                            _obj = CacheManager.Get(_cachekey, DateTime.Now);
                        }
                    }
                }

            }
        }

        return _obj[0].objds;
    }
    public static DataTable fillSubGrid(string pageId, string objectId, string sirket, string user, string MainPageId, string MainObjectId, string MainParams)
    {

        using (DataTable dt = new DataTable())
        {

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "[sp_0C_50057_00_WEBPAGES_03]";
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@MainPageId", pageId);
                    cmd.Parameters.AddWithValue("@MainObjectId", MainObjectId);
                    cmd.Parameters.AddWithValue("@MainParams", MainParams);
                    cmd.Parameters.AddWithValue("@PageId", pageId);
                    cmd.Parameters.AddWithValue("@ObjectId", objectId);
                    cmd.Parameters.AddWithValue("@Sirket", sirket);
                    cmd.Parameters.AddWithValue("@UserName", user);

                    using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                    {
                        adp.Fill(dt);
                    }
                }
            }

            return dt;
        }
    }

    public static DataTable fillMainGrid(string pageId, string objectId, string sirket, string user)
    {

        using (DataTable dt = new DataTable())
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "[sp_0C_50057_00_WEBPAGES]";
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@PageId", pageId);
                    cmd.Parameters.AddWithValue("@ObjectId", objectId);
                    cmd.Parameters.AddWithValue("@Sirket", sirket);
                    cmd.Parameters.AddWithValue("@UserName", user);

                    using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                    {
                        adp.Fill(dt);
                    }
                }
            }
            return dt;
        }
    }

    public static DataTable fillMainGridWithParams(string pageId, string objectId, string sirket, string user, string values)
    {

        using (DataTable dt = new DataTable())
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "[sp_0C_50057_00_WEBPAGES_05]";
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@PageId", pageId);
                    cmd.Parameters.AddWithValue("@ObjectId", objectId);
                    cmd.Parameters.AddWithValue("@Sirket", sirket);
                    cmd.Parameters.AddWithValue("@UserName", user);
                    cmd.Parameters.AddWithValue("@MainParams", values);

                    using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                    {
                        adp.Fill(dt);
                    }
                }
            }
            return dt;
        }
    }
    public static DataTable fillEmptyMainGrid(string pageId, string objectId, string sirket, string user)
    {

        using (DataTable dt = new DataTable())
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "[sp_0C_50057_00_WEBPAGES_01]";
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@PageId", pageId);
                    cmd.Parameters.AddWithValue("@ObjectId", objectId);
                    cmd.Parameters.AddWithValue("@Sirket", sirket);
                    cmd.Parameters.AddWithValue("@UserName", user);

                    using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                    {
                        adp.Fill(dt);
                    }
                }
            }
            return dt;
        }
    }
    public static DataTable GetSubGridInsertPermission(string pageId, string objectId, string sirket, string user, string MainPageId, string MainObjectId, string MainParams)
    {
        using (DataTable dt = new DataTable())
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "[sp_0C_50057_00_WEBPAGES_02]";
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 1000;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@MainPageId", pageId);
                    cmd.Parameters.AddWithValue("@MainObjectId", MainObjectId);
                    cmd.Parameters.AddWithValue("@MainParams", MainParams);
                    cmd.Parameters.AddWithValue("@PageId", pageId);
                    cmd.Parameters.AddWithValue("@ObjectId", objectId);
                    cmd.Parameters.AddWithValue("@Sirket", sirket);
                    cmd.Parameters.AddWithValue("@UserName", user);

                    using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                    {
                        adp.Fill(dt);
                    }
                }
            }
            return dt;
        }
    }

    public static void ExportPdf(ASPxGridViewExporter exporter)
    {
        //Convert.ToInt32(exporter.GridView.Width.Value)
        exporter.Landscape = true;
        exporter.PaperKind = System.Drawing.Printing.PaperKind.DSheet;
        exporter.MaxColumnWidth = 150;
        exporter.LeftMargin = 2;
        exporter.TopMargin = 2;
        exporter.GridView.Columns[0].Visible = false;
        exporter.GridView.Columns["REDIT11"].Visible = false;
        exporter.WritePdfToResponse();
    }

    public static string DefaultSearchGrid(DataRow[] rows)
    {
        string filterexpression = string.Empty;
        try
        {
            //DataRow[] rows = dt.Select("[DX Default Search]<>'' AND [DX Display Order]>0");
            bool ilk = true;
            foreach (DataRow row in rows)
            {
                if (ilk)
                {
                    filterexpression = "[" + row["DX Field ID"].ToString() + "]  like '%" + row["DX Default Search"].ToString() + "%'";
                    ilk = false;
                }
                else
                {
                    filterexpression += " AND [" + row["DX Field ID"].ToString() + "] like '%" + row["DX Default Search"].ToString() + "%'";
                }
            }
        }
        catch
        {


        }

        return filterexpression;
    }

    public static void ProcSelectedRows(ASPxGridView grid, DataRow[] rows, string kulad, string Sirket)
    {
        HttpContext.Current.Session["WebMessage"] = string.Empty;
        if (rows.Length > 0)
        {
            string filter = rows[0]["DX Filter"].ToString();
            string fieldId = rows[0]["DX Field ID"].ToString();
            string websource = rows[0]["DX Source"].ToString();
            string yetkistr = rows[0]["DX Permision Field"].ToString();
            bool HasYtk = false;
            List<object> ArrayValues = new List<object>();
            List<string> ArrayObjs = new List<string>();
            List<string> ServisObjs = new List<string>();
            int PSize = 0;

            if (!string.IsNullOrEmpty(filter) && !string.IsNullOrEmpty(websource))
            {
                Guid IslemId = Guid.NewGuid();
                ArrayObjs.Clear();
                ServisObjs.Clear();
                foreach (var item in filter.Split(','))
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        if (item.Substring(0, 1) == "$")
                            ArrayObjs.Add(item.Replace("$", string.Empty));
                        else
                            ServisObjs.Add(item);
                    }
                }
                if (!string.IsNullOrEmpty(yetkistr))
                {
                    if (yetkistr.Substring(0, 2) != "AK")
                    {
                        ArrayObjs.Add(yetkistr);
                        HasYtk = true;
                    }
                }



                PSize = ArrayObjs.Count;
                ArrayValues.Clear();
                if (grid.VisibleRowCount == 1) grid.Selection.SelectAll();
                ArrayValues = grid.GetSelectedFieldValues(ArrayObjs.ToArray());
                InsertWebTopluIslem(IslemId, PSize, ArrayValues, HasYtk, ServisObjs, fieldId, websource, kulad, Sirket);
                string[] arr = new string[] { IslemId.ToString(), kulad };
                ServiceUtilities services = new ServiceUtilities();
                services.NavFunctionsStatic(kulad, Sirket, websource, arr);


            }
        }
    }

    public static string ProcSelectedRowsReturnIslemId(ASPxGridView grid, DataRow[] rows, string kulad, string Sirket)
    {
        string _result = string.Empty;
        try
        {
            if (rows.Length > 0)
            {
                string filter = rows[0]["DX Filter"].ToString().Split('|')[1];
                string fieldId = rows[0]["DX Field ID"].ToString();
                string websource = rows[0]["DX Source"].ToString();
                string yetkistr = rows[0]["DX Permision Field"].ToString();
                bool HasYtk = false;
                List<object> ArrayValues = new List<object>();
                List<string> ArrayObjs = new List<string>();
                List<string> ServisObjs = new List<string>();
                int PSize = 0;

                if (!string.IsNullOrEmpty(filter) && !string.IsNullOrEmpty(websource))
                {
                    ArrayObjs.Clear();
                    ServisObjs.Clear();
                    foreach (var item in filter.Split(','))
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            if (item.Substring(0, 1) == "$")
                                ArrayObjs.Add(item.Replace("$", string.Empty));
                            else
                                ServisObjs.Add(item);
                        }
                    }
                    if (!string.IsNullOrEmpty(yetkistr))
                    {
                        if (yetkistr.Substring(0, 2) != "AK")
                        {
                            ArrayObjs.Add(yetkistr);
                            HasYtk = true;
                        }
                    }



                    PSize = ArrayObjs.Count;
                    ArrayValues.Clear();
                    if (grid.VisibleRowCount == 1) grid.Selection.SelectAll();
                    ArrayValues = grid.GetSelectedFieldValues(ArrayObjs.ToArray());
                    Guid IslemId = Guid.NewGuid();
                    InsertWebTopluIslem(IslemId, PSize, ArrayValues, HasYtk, ServisObjs, fieldId, websource, kulad, Sirket);
                    _result = IslemId.ToString();


                }
            }
            else
                _result = "BOŞ";
        }
        catch
        {
            _result = "HATA";
        }
        return _result;

    }

    public static string ProcSelectedRowsWithNavReturnIslemId(ASPxGridView grid, DataRow[] rows, string kulad, string Sirket, string _funcname)
    {
        string _result = string.Empty;
        try
        {
            if (rows.Length > 0)
            {
                string filter = rows[0]["DX Filter"].ToString().Split('|')[1];
                string fieldId = rows[0]["DX Field ID"].ToString();
                string websource = rows[0]["DX Source"].ToString();
                string yetkistr = rows[0]["DX Permision Field"].ToString();
                bool HasYtk = false;
                List<object> ArrayValues = new List<object>();
                List<string> ArrayObjs = new List<string>();
                List<string> ServisObjs = new List<string>();
                int PSize = 0;

                if (!string.IsNullOrEmpty(filter) && !string.IsNullOrEmpty(websource))
                {

                    ArrayObjs.Clear();
                    ServisObjs.Clear();
                    foreach (var item in filter.Split(','))
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            if (item.Substring(0, 1) == "$")
                                ArrayObjs.Add(item.Replace("$", string.Empty));
                            else
                                ServisObjs.Add(item);
                        }
                    }
                    if (!string.IsNullOrEmpty(yetkistr))
                    {
                        if (yetkistr.Substring(0, 2) != "AK")
                        {
                            ArrayObjs.Add(yetkistr);
                            HasYtk = true;
                        }
                    }



                    PSize = ArrayObjs.Count;
                    ArrayValues.Clear();
                    if (grid.VisibleRowCount == 1) grid.Selection.SelectAll();
                    ArrayValues = grid.GetSelectedFieldValues(ArrayObjs.ToArray());
                    Guid IslemId = Guid.NewGuid();
                    InsertWebTopluIslem(IslemId, PSize, ArrayValues, HasYtk, ServisObjs, fieldId, websource, kulad, Sirket);
                    string[] arr = new string[] { IslemId.ToString(), kulad };
                    ServiceUtilities services = new ServiceUtilities();
                    services.NavFunctionsStatic(kulad, Sirket, _funcname, arr);
                    _result = IslemId.ToString();


                }
            }
            else
                _result = string.Empty;
        }
        catch
        {
            _result = string.Empty;
        }

        return _result;
    }

    public static bool DynamicPageYetkiKontrol(string sirket, string kad, string Kontrolismi)
    {
        if (Kontrolismi.Length > 1)
        {
            if (Kontrolismi.ToUpper().Substring(0, 2) != "AK") return true;
        }
        bool sonuc = false;
        using (DataTable dt = new DataTable())
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SP_0WEB_YETKI_KONTROL_0_1";
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@COMP", sirket);
                    cmd.Parameters.AddWithValue("@USER", kad);
                    cmd.Parameters.AddWithValue("@ALAN", Kontrolismi);

                    using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                    {
                        adp.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            if (dt.Rows[0][0].ToString() == "1")
                                sonuc = true;

                        }
                        else
                            sonuc = false;
                    }
                }
            }
            return sonuc;
        }
    }

    public static string DynamicPagePdfMerge(string sirket, string kad, string procedurename, string IslemId)
    {
        string sonuc = string.Empty;
        sonuc = "NOROW|Pdf not found.";
        using (DataTable dt = new DataTable())
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = procedurename;
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@COMP", sirket);
                    cmd.Parameters.AddWithValue("@USER", kad);
                    cmd.Parameters.AddWithValue("@ISLEMID", IslemId);

                    using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                    {
                        adp.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            bool ilk = true;
                            PdfDocumentProcessor pdfDocumentProcessor = new PdfDocumentProcessor();
                            foreach (DataRow row in dt.Rows)
                            {
                                if (ilk)
                                {
                                    pdfDocumentProcessor.LoadDocument(HttpContext.Current.Server.MapPath("~/") + row["INPUT"].ToString());
                                    ilk = false;
                                }
                                else
                                    pdfDocumentProcessor.AppendDocument(HttpContext.Current.Server.MapPath("~/") + row["INPUT"].ToString());
                            }

                            try
                            {
                                if (System.IO.File.Exists(HttpContext.Current.Server.MapPath("~/") + dt.Rows[0]["OUTPUT"].ToString()))
                                    System.IO.File.Delete(HttpContext.Current.Server.MapPath("~/") + dt.Rows[0]["OUTPUT"].ToString());
                            }
                            catch { }


                            pdfDocumentProcessor.SaveDocument(HttpContext.Current.Server.MapPath("~/") + dt.Rows[0]["OUTPUT"].ToString());

                            sonuc = "OK|" + dt.Rows[0]["OUTPUT"].ToString();
                        }
                        else
                            sonuc = "NOROW|Pdf not found.";
                    }
                }
            }
            return sonuc;
        }
    }


    static void WriteLog(string ex)
    {
        string message = string.Format("Time: {0}", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
        message += Environment.NewLine;
        message += "-----------------------------------------------------------";
        message += Environment.NewLine;
        message += string.Format("Message: {0}", ex);
        message += Environment.NewLine;
        message += "-----------------------------------------------------------";
        message += Environment.NewLine;
        string path = HttpContext.Current.Server.MapPath("~/App_Data/Log.txt");
        using (System.IO.StreamWriter writer = new System.IO.StreamWriter(path, true))
        {
            writer.WriteLine(message);
            writer.Close();
        }
    }






}


