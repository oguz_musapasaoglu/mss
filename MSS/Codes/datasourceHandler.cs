﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class datasourceHandler
{
    string ConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
    fonksiyonlar f = new fonksiyonlar();
    public datasourceHandler()
    { }

    public DataTable InitComboDataTable(string objeType, string Comp)
    {
        using (SqlConnection conn = new SqlConnection(ConnString))
        {
            string sorgu = "";
            switch (objeType)
            {
                case "SEHIR":
                    sorgu = "Select NEWID() ID, Code,Code+' - '+City as Text from [GAC_NAV2]. [dbo].[0C_00225_01_POST CODE]  " +
                        "where COMPANY = '" + Comp + "' AND Type=0  order by Sıralama asc";
                    break;
                case "MUSTERIGRUBU":
                    sorgu = "Select NEWID() ID, Code, Description Text from  [GAC_NAV2].[dbo].[0C_05105_00_CUSTOMER TEMPLATE] " +
                        "where COMPANY = '" + Comp + "' AND Tur=0 and Stena = 0";
                    break;
                case "TEDARIKCIGRUBU":
                    sorgu = "Select NEWID() ID, Code, Description Text from [GAC_NAV2].[dbo].[0C_05105_00_CUSTOMER TEMPLATE] " +
                        "where COMPANY = '" + Comp + "' AND Tur=1 and Stena = 0";
                    break;
                case "ODEMEVADESI":
                    sorgu = "Select NEWID() ID, Code, Description Text from [GAC_NAV2].[dbo].[0C_00003_00_PAYMENT_TERMS] " +
                        "where COMPANY = '" + Comp + "' order by [Gun Sayisi] asc";
                    break;
                case "MUSTERITEMSILCISI":
                    sorgu = "Select NEWID() ID, Code,Code +' - ' + Name as Text from [GAC_NAV2].[dbo].[0C_00013_00_SALESPERSON]  " +
                        "where COMPANY = '" + Comp + "' AND [İsten Ayrildi]=0";
                    break;
                case "NETWORKSORUMLUSU":
                    sorgu = "Select  NEWID() ID, Code,Code +' - ' + Name as Text from [GAC_NAV2].[dbo].[0C_00013_00_SALESPERSON] " +
                        " where COMPANY = '" + Comp + "' AND Network=1";
                    break;
                case "VD":
                    sorgu = "Select NEWID() ID,Code, Description Text  from [GAC_NAV2].[dbo].[0C_00318_00_TAX AREA]" +
                        " where COMPANY = '" + Comp + "' order by Sıralama asc";
                    break;
                case "SEKTOR":
                    sorgu = "Select NEWID() ID,Name Code, Name Text from [GAC_NAV2].[dbo].[0C_50022_05_ILETISIM_TURLERI_SECTOR]" +
                        " where COMPANY = '" + Comp + "' AND Type= '4' order by Name asc";
                    break;
                case "ULKE":
                    sorgu = "select  NEWID() ID,Kod Code, Aciklama Text  from [GAC_NAV2].[dbo].[0C_90773_01_FAMILY_COUNTRIES]" +
                        " where COMPANY = '" + Comp + "' AND Tür=0";
                    break;
                case "plofRec":
                    sorgu = "select  NEWID() ID,R0011 Code, R0012 Text from [GAC_NAV2].[dbo].[0_W10042]  " +
                        "where COMPANY = '" + Comp + "'";
                    break;
                case "plofDel":
                    sorgu = "select  NEWID() ID,R0011 Code, R0012 Text from [GAC_NAV2].[dbo].[0_W10042] " +
                        "where COMPANY = '" + Comp + "'";
                    break;
                case "POL":
                    sorgu = "select  NEWID() ID,R0011 Code, R0012 Text from [GAC_NAV2].[dbo].[0_W10030] " +
                        "where COMPANY = '" + Comp + "'";
                    break;
                case "POD":
                    sorgu = "select  NEWID() ID,R0011 Code, R0012 Text from [GAC_NAV2].[dbo].[0_W10030] " +
                        "where COMPANY = '" + Comp + "'";
                    break;
                case "CARRIER":
                    sorgu = "select  NEWID() ID,R0012 Code, R0014 Text from [GAC_NAV2].[dbo].[0_W10081] " +
                        "where COMPANY = '" + Comp + "'";
                    break;
                case "EMPLOYEE":
                    sorgu = "select  NEWID() ID,No_ Code, [Tam Ad] Text from [GAC_NAV2].[dbo].[0C_05200_00_EMPLOYEE_2] " +
                        " where COMPANY = '" + Comp + "' " +
                        "and [Mutabakat Sorumlusu]=1 and Status=0  Order By [Tam Ad]";
                    break;
                case "TALEPEDEN":
                    sorgu = "select  NEWID() ID,No_ Code, [Tam Ad] Text from [GAC_NAV2].[dbo].[0C_05200_00_EMPLOYEE] " +
                        " where COMPANY = '" + Comp + "' Order By [Tam Ad]";
                    break;
                case "LUCENTSTATUS":
                    sorgu = "select  NEWID() ID,R0011 Code, R0012 Text from [0_W153220] " +
                        " where RCOMP = '" + Comp + "' Order By R0012";
                    break;
                case "EPSRANK":
                    sorgu = "select  NEWID() ID,R0011 Code, R0012 Text from [0_W153090] " +
                        " where RCOMP = '" + Comp + "' Order By R0012";
                    break;
                case "LUCENTRANK":
                    sorgu = "select  NEWID() ID,R0011 Code, R0012 Text from [0_W153090] " +
                        " where RCOMP = '" + Comp + "' Order By R0012";
                    break;
                case "COC":
                    sorgu = "select  NEWID() ID,R0011 Code, R0012 Text from [0_W153090] " +
                        " where RCOMP = '" + Comp + "' Order By R0012";
                    break;
                case "LUCENTNAT":
                    sorgu = "select  NEWID() ID,R0011 Code, R0012 Text from [0_W153270] " +
                        " where RCOMP = '" + Comp + "' Order By R0012";
                    break;

            }
            return f.datatablegonder(sorgu);

        }
    }

    public DataTable InitComboDataTableV1(string objeType, string Sirket)
    {
        using (SqlConnection conn = new SqlConnection(ConnString))
        {
            string sorgu = "";
            switch (objeType)
            {
                case "SEHIR":
                    sorgu = "Select NEWID() ID, Code,Code+' - '+City as Text from [GAC_NAV2]. [dbo].[0C_00225_01_POST CODE]  " +
                        "where COMPANY = '" + Sirket + "' AND Type=0  order by Sıralama asc";
                    break;
                case "MUSTERIGRUBU":
                    sorgu = "Select NEWID() ID, Code, Description Text from  [GAC_NAV2].[dbo].[0C_05105_00_CUSTOMER TEMPLATE] " +
                        "where COMPANY = '" + Sirket + "' AND Tur=0 and Stena = 0";
                    break;
                case "TEDARIKCIGRUBU":
                    sorgu = "Select NEWID() ID, Code, Description Text from [GAC_NAV2].[dbo].[0C_05105_00_CUSTOMER TEMPLATE] " +
                        "where COMPANY = '" + Sirket + "' AND Tur=1 and Stena = 0";
                    break;
                case "ODEMEVADESI":
                    sorgu = "Select NEWID() ID, Code, Description Text from [GAC_NAV2].[dbo].[0C_00003_00_PAYMENT_TERMS] " +
                        "where COMPANY = '" + Sirket + "' order by [Gun Sayisi] asc";
                    break;
                case "MUSTERITEMSILCISI":
                    sorgu = "Select NEWID() ID, Code,Code +' - ' + Name as Text from [GAC_NAV2].[dbo].[0C_00013_00_SALESPERSON]  " +
                        "where COMPANY = '" + Sirket + "' AND [İsten Ayrildi]=0";
                    break;
                case "NETWORKSORUMLUSU":
                    sorgu = "Select  NEWID() ID, Code,Code +' - ' + Name as Text from [GAC_NAV2].[dbo].[0C_00013_00_SALESPERSON] " +
                        " where COMPANY = '" + Sirket + "' AND Network=1";
                    break;
                case "VD":
                    sorgu = "Select NEWID() ID,Code, Description Text  from [GAC_NAV2].[dbo].[0C_00318_00_TAX AREA]" +
                        " where COMPANY = '" + Sirket + "' order by Sıralama asc";
                    break;
                case "SEKTOR":
                    sorgu = "Select NEWID() ID,Name Code, Name Text from [GAC_NAV2].[dbo].[0C_50022_05_ILETISIM_TURLERI_SECTOR]" +
                        " where COMPANY = '" + Sirket + "' AND Type= '4' order by Name asc";
                    break;
                case "ULKE":
                    sorgu = "select  NEWID() ID,Kod Code, Aciklama Text  from [GAC_NAV2].[dbo].[0C_90773_01_FAMILY_COUNTRIES]" +
                        " where COMPANY = '" + Sirket + "' AND Tür=0";
                    break;
                case "plofRec":
                    sorgu = "select  NEWID() ID,R0011 Code, R0012 Text from [GAC_NAV2].[dbo].[0_W10042]  " +
                        "where COMPANY = '" + Sirket + "'";
                    break;
                case "plofDel":
                    sorgu = "select  NEWID() ID,R0011 Code, R0012 Text from [GAC_NAV2].[dbo].[0_W10042] " +
                        "where COMPANY = '" + Sirket + "'";
                    break;
                case "POL":
                    sorgu = "select  NEWID() ID,R0011 Code, R0012 Text from [GAC_NAV2].[dbo].[0_W10030] " +
                        "where COMPANY = '" + Sirket + "'";
                    break;
                case "POD":
                    sorgu = "select  NEWID() ID,R0011 Code, R0012 Text from [GAC_NAV2].[dbo].[0_W10030] " +
                        "where COMPANY = '" + Sirket + "'";
                    break;
                case "CARRIER":
                    sorgu = "select  NEWID() ID,R0012 Code, R0014 Text from [GAC_NAV2].[dbo].[0_W10081]" +
                        " where COMPANY = '" + Sirket + "'";
                    break;
                case "EMPLOYEE":
                    sorgu = "select  NEWID() ID,No_ Code, [Tam Ad] Text from [GAC_NAV2].[dbo].[0C_05200_00_EMPLOYEE] " +
                        " where COMPANY = '" + Sirket + "'  Order By [Tam Ad]";
                    break;                
                case "TALEPEDEN":
                    sorgu = "select  NEWID() ID,R0011 Code, R0012 Text from [GAC_NAV2].[dbo].[0_W10220] where RCOMP='" + Sirket + "' Order By R0012";
                    break;
                case "ILGILIDEPT":
                    sorgu = "select  NEWID() ID,R0011 Code, R0012 Text from [GAC_NAV2].[dbo].[0_W10460] where RCOMP='" + Sirket + "' Order By R0012";
                    break;
                case "TEDARIKCITIPI":
                    sorgu = "select  NEWID() ID,[LineNo] Code, Adi Text from [GAC_NAV2].[dbo].[0C_50055_54_LOOKUPS_TEDARIKCI_TURU] where COMPANY='" + Sirket + "' ";
                    break;
                    


            }
            return f.datatablegonder(sorgu);

        }
    }

    public DataTable CompanyFromDatabase()
    {
        using (SqlConnection conn = new SqlConnection(ConnString))
        {
            string sorgu  = "select * from [0C_70055_01_CODE_PARAMETERS]";
                    
            return f.datatablegonder(sorgu);

        }
    }
}