﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;

namespace MSS1.Codes
{

    public class CacheObject
    {
        public string Key { get; set; }
        public int Duration { get; set; }
        public int Refresh { get; set; }
        public DateTime RefreshDate { get; set; }
        public DataTable obj { get; set; }
        public DataSet objds { get; set; }
        public string ValueType { get; set; }
    }

    public class ReportObject
    {
        public string Key { get; set; }
        public ReportDocument Rpt { get; set; }
    }

    public static class ReportManager
    {
        public static List<ReportObject> _ReportCacheObjects = new List<ReportObject>();

        public static List<ReportObject> Get(string key)
        {
            var _obj = _ReportCacheObjects.Where(d => d.Key == key).ToList();
            return _obj;
        }
    }

    public static class CacheManager
    {
        public static int CacheActive
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["CacheActive"].ToString());
            }
        }

        public static bool _isCacheDuring { get; set; }

        public static List<CacheObject> _cacheObjects = new List<CacheObject>();
        public static List<CacheObject> Get(string key)
        {
            var _obj = _cacheObjects.Where(d => d.Key == key).ToList();
            return _obj;
        }

        public static List<CacheObject> Get(string key, DateTime _date)
        {
            var _obj = _cacheObjects.Where(d => d.Key == key & (d.RefreshDate > _date | d.Refresh == 0)).ToList();
            return _obj;
        }

        public static void Set(DataTable dt, string key, int _duration, int _refresh)
        {
            List<CacheObject> _obj = _cacheObjects.Where(d => d.Key == key).ToList();

            if (_obj.Count == 0)
            {
                CacheObject _cObject = new CacheObject();

                _cObject.Key = key;
                _cObject.obj = dt;
                _cObject.RefreshDate = DateTime.Now.AddMinutes(_duration);
                _cObject.Duration = _duration;
                _cObject.Refresh = _refresh;
                _cacheObjects.Add(_cObject);
            }

        }

        public static void Set(DataSet ds, string key, int _duration, int _refresh)
        {
            List<CacheObject> _obj = _cacheObjects.Where(d => d.Key == key).ToList();

            if (_obj.Count == 0)
            {
                CacheObject _cObject = new CacheObject();

                _cObject.Key = key;
                _cObject.objds = ds;
                _cObject.RefreshDate = DateTime.Now.AddMinutes(_duration);
                _cObject.Duration = _duration;
                _cObject.Refresh = _refresh;
                _cacheObjects.Add(_cObject);
            }

        }

        public static List<CacheObject> GetDynamicCachesAt(string _comp, string _pageId)
        {
            var _obj = _cacheObjects.Where(d => d.Key.Contains("_" + _pageId) && d.Key.Contains("_" + _comp)).ToList();
            return _obj;
        }

        public static void SetDynamic(DataTable dt, string key, int _duration, int _refresh)
        {
            List<CacheObject> _obj = _cacheObjects.Where(d => d.Key == key).ToList();

            if (_obj.Count == 0)
            {
                CacheObject _cObject = new CacheObject();

                _cObject.Key = key;
                _cObject.obj = dt;
                _cObject.RefreshDate = DateTime.Now.AddMinutes(_duration);
                _cObject.Duration = _duration;
                _cObject.Refresh = _refresh;
                _cacheObjects.Add(_cObject);
            }
            else
            {
                _cacheObjects.Remove(_obj[0]);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                CacheObject _cObject = new CacheObject();

                _cObject.Key = key;
                _cObject.obj = dt;
                _cObject.RefreshDate = DateTime.Now.AddMinutes(_duration);
                _cObject.Duration = _duration;
                _cObject.Refresh = _refresh;
                _cacheObjects.Add(_cObject);
            }

        }

        public static void SetDynamic(DataSet ds, string key, int _duration, int _refresh)
        {
            List<CacheObject> _obj = _cacheObjects.Where(d => d.Key == key).ToList();

            if (_obj.Count == 0)
            {
                CacheObject _cObject = new CacheObject();

                _cObject.Key = key;
                _cObject.objds = ds;
                _cObject.RefreshDate = DateTime.Now.AddMinutes(_duration);
                _cObject.Duration = _duration;
                _cObject.Refresh = _refresh;
                _cacheObjects.Add(_cObject);
            }
            else
            {
                _cacheObjects.Remove(_obj[0]);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                CacheObject _cObject = new CacheObject();
                _cObject.Key = key;
                _cObject.objds = ds;
                _cObject.RefreshDate = DateTime.Now.AddMinutes(_duration);
                _cObject.Duration = _duration;
                _cObject.Refresh = _refresh;
                _cacheObjects.Add(_cObject);
            }
        }

        public static void StartCache()
        {
            Thread thcache = new Thread(() => FillCacheObjects());
            thcache.Start();
        }

        public static void FillCacheObjects()
        {
            _isCacheDuring = true;
            try
            {

                using (DataTable dt = new DataTable())
                {
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = conn;
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandText = "SELECT * FROM [GAC_NAV2].[dbo].[0C_70057_00_CACHE_OBJECTS] WHERE ACTIVE=1";
                            cmd.Parameters.Clear();

                            using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                            {
                                adapter.Fill(dt);
                            }
                        }

                        foreach (DataRow row in dt.Rows)
                        {
                            try
                            {
                                using (DataTable _objectdt = new DataTable())
                                {
                                    using (SqlCommand cmd = new SqlCommand())
                                    {
                                        cmd.Connection = conn;
                                        cmd.CommandType = CommandType.Text;
                                        cmd.CommandText = "SELECT * FROM [GAC_NAV2].[dbo].[" + row["OBJECT NAME"].ToString() + "]";
                                        cmd.CommandTimeout = 5000;
                                        cmd.Parameters.Clear();

                                        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                                        {


                                            List<CacheObject> _obj = Get(row["OBJECT NAME"].ToString());
                                            if (_obj.Count == 0)
                                            {
                                                adapter.Fill(_objectdt);
                                                Set(_objectdt, row["OBJECT NAME"].ToString(), Convert.ToInt32(row["DURATION"]), Convert.ToInt32(row["REFRESH"]));
                                            }
                                            else
                                            {
                                                CacheObject _cObject = _obj[0];

                                                if (_cObject.Refresh == 1 & _cObject.RefreshDate < DateTime.Now)
                                                {
                                                    {
                                                        adapter.Fill(_objectdt);
                                                        _cObject.obj = _objectdt;
                                                        _cacheObjects.Where(d => d.Key == row["OBJECT NAME"].ToString()).ToList()[0].obj = _objectdt;

                                                    }

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {

                            }

                        }


                    }
                }
            }
            catch (Exception ex1)
            {

            }
            finally
            {
                _isCacheDuring = false;
            }


        }

        public static void WriteLog(string ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex);
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            string path = HttpContext.Current.Server.MapPath("~/App_Data/Log.txt");
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(path, true))
            {
                writer.WriteLine(message);
                writer.Close();
            }
        }
    }
}