﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace MSS1.Codes
{
    public class Bases : Page
    {

        public string CurrentUserSirketAdi
        {
            get
            {
                try
                {
                    return Session["Sirket"].ToString();
                }
                catch (Exception)
                {
                    return "-1";

                }

            }
        }

        public string CurrentFolderName(string _comp)
        {


            try
            {
                if (_comp == "GAC" | _comp == "LAM")
                    return "GACLAM";
                else
                    return _comp;

            }
            catch (Exception)
            {
                return "";

            }


        }

        public string CustomUserSirketAdi
        {
            get
            {
                return "LAMLOJ";
            }
        }
    }
}