﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using MSS1.Codes;

/// <summary>
/// Summary description for Language
/// </summary>
public class Language
{
    string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
    fonksiyonlar f = new fonksiyonlar();
    string _objName = "0C_50032_01_WEB PAGE CAPTIONS";
    public Language()
    {

    }

    public string dilgetir(string selectedLanguage, string sirket, string kull, string page_id, string field_id)
    {
        _objName = "0C_50032_01_WEB PAGE CAPTIONS";
        List<CacheObject> _obj = CacheManager.Get(_objName);
        if (_obj.Count == 0)
        {
            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                string sorgu = "SELECT TR,EN,FR,UA,RU,GE,EG FROM [0C_50032_01_WEB PAGE CAPTIONS] WHERE COMPANY= '" + sirket + "' and [PageID]+[FieldID]='" + page_id + field_id + "'";
                conn.Open();


                using (SqlDataAdapter adp = new SqlDataAdapter(sorgu, conn))
                {
                    DataTable dt = new DataTable();
                    adp.Fill(dt);

                    try
                    {
                        return dt.Rows[0][selectedLanguage].ToString();
                        
                    }
                    catch
                    {
                        return page_id + "," + field_id;
                    }
                }
            }
        }
        else
        {
            DataRow[] _langRow = _obj[0].obj.Select("COMPANY='" + sirket + "' and [PageID]='" + page_id + "' and [FieldID]='" + field_id + "'");

            try
            {
               
                return _langRow[0][selectedLanguage].ToString();
            }
            catch
            {
                return page_id + "," + field_id;
            }
        }

    }

    public string dilgetirfordynamically(string selectedLanguage, string Comp, string kull, string page_id)
    {
        _objName = "0C_50032_01_WEB PAGE CAPTIONS";
        List<CacheObject> _obj = CacheManager.Get(_objName);
        if (_obj.Count == 0)
        {
            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                string sorgu = "SELECT TR,EN,FR,UA,RU,GE,EG FROM  [0C_50032_01_WEB PAGE CAPTIONS]  WHERE COMPANY= '" + Comp + "' and FieldID='H0001' and  [PageID]='" + page_id + "'";
                conn.Open();
                using (SqlDataAdapter adp = new SqlDataAdapter(sorgu, conn))
                {
                    using (DataTable dt = new DataTable())
                    {
                        adp.Fill(dt);

                        try
                        {                            
                            return dt.Rows[0][selectedLanguage].ToString();                            
                        }
                        catch
                        {
                            return "";
                        }

                    }
                }
            }
        }
        else
        {
            DataRow[] _langRow = _obj[0].obj.Select("COMPANY='" + Comp + "' and FieldID='H0001' and  [PageID]='" + page_id + "'");

            try
            {
                return _langRow[0][selectedLanguage].ToString();
            }
            catch
            {
                return "";
            }
        }




    }
}