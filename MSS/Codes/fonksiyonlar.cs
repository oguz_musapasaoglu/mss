﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Web;
using CrystalDecisions.Shared;
using DevExpress.Web;

public class fonksiyonlar
{
    #region GlobalDeğişkenler
    public string projeolusturturu = string.Empty;
    public Int32 seciliunit = 0;
    public static int page = 1;
    public static string sirket = string.Empty;

    public static bool Home = true;
    public static string kullaniciadi = "";
    public static string girisyapilansirket = "";
    public static string isgacno = string.Empty, isgorevadi = string.Empty, projeNo = string.Empty, projeGorev = string.Empty;


    #endregion

    public static bool evrakkayitaltlogo = true;
    public static string[] lojaylikRaporParameters = new string[26];
    public static string[] lojanalizRaporParameters = new string[26];
    public static string[] genelGiderAylikRaporParameters = new string[22];
    public static string password = "Password1";
    SqlConnection baglanti;
    string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
    ws_baglantı ws = new ws_baglantı();
    Methods method = new Methods();


    public fonksiyonlar()
    {

    }

    #region Musa Yeni Fonksiyonlar

     

    public DataTable datatablegonder(string sorgu)
    {
        baglanti = new SqlConnection(strConnString);
        DataTable dt = new DataTable();
        try
        {
            baglanti.Open();
            SqlCommand cmd = new SqlCommand(sorgu, baglanti);
            cmd.CommandTimeout = 180;
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dt);
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }

        return dt;

    }
       
    public void FillAspxCombobox(ASPxComboBox cmb, string sorgu, string Text, string Value)
    {
        cmb.Items.Clear();
        cmb.DataSourceID = null;
        cmb.DataSource = datatablegonder(sorgu);
        cmb.TextField = Text;
        cmb.ValueField = Value;
        cmb.DataBind();
    }

    public void DropDownDoldurIlkSatirEkle(string sorgu, DropDownList ddl, bool ilksatirekle, string ilksatirVal)
    {
        baglanti = new SqlConnection(strConnString);
        try
        {
            baglanti.Open();
            SqlCommand cmd = new SqlCommand(sorgu, baglanti);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adp.Fill(dt);

            ddl.DataSource = dt;

            ddl.DataTextField = "name";
            ddl.DataValueField = "kod";
            ddl.DataBind();
            if (ilksatirekle)
                ddl.Items.Insert(0, ilksatirVal);
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }


    }
     
    public string tekbirsonucdonder(string sorgu)
    {
        baglanti = new SqlConnection(strConnString);
        try
        {
            baglanti.Open();
            SqlCommand cmd = new SqlCommand(sorgu, baglanti);
            return cmd.ExecuteScalar().ToString();
        }
        catch (Exception)
        {
            return "NULL";
        }
        finally
        {
            baglanti.Close();
        }

    }

    public DataSet GetDs(string pageId, string ObjectId, string Comp)
    {

        using (DataSet ds = new DataSet())
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(strConnString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "[sp_0C_50057_00_WEBPAGES_12]";
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@Sirket", Comp);
                        cmd.Parameters.AddWithValue("@PageIds", pageId);
                        cmd.Parameters.AddWithValue("@ObjectIds", ObjectId);
                        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                        {
                            adapter.Fill(ds);
                        }

                    }
                }
            }
            catch
            {

            }
            return ds;
        }
    }

     

    public void updateyap(string sorgu)
    {
        using (baglanti = new SqlConnection(strConnString))
        {
            try
            {
                baglanti.Open();
                SqlCommand cmd = new SqlCommand(sorgu, baglanti);
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                baglanti.Close();
            }
        }

    }
    #endregion

    #region FONKSİYONLAR
        
    public void FillDevExCombobox(ASPxComboBox cmb, string sorgu, string TextField, string ValueField)
    {
        using (baglanti = new SqlConnection(strConnString))
        {
            try
            {
                cmb.Items.Clear();
                ListEditItem lv = new ListEditItem();
                lv.Selected = true;
                lv.Text = "";
                lv.Value = "";
                cmb.Items.Add(lv);
                using (SqlDataAdapter ad = new SqlDataAdapter(sorgu, baglanti))
                {
                    using (DataTable dt = new DataTable())
                    {
                        ad.Fill(dt);
                        foreach (DataRow row in dt.Rows)
                        {
                            lv = new ListEditItem();
                            lv.Text = row[TextField].ToString();
                            lv.Value = row[ValueField].ToString();
                            cmb.Items.Add(lv);
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                baglanti.Close();
            }
        }
    }
   
    public bool YetkiKontrol(string sirket, string kad, string Kontrolismi)
    {
        bool sonuc = false;
        using (SqlConnection conSQL = new SqlConnection(strConnString))
        {
            using (SqlCommand com3 = new SqlCommand("SP_0WEB_SAYFA_YETKI", conSQL))
            {
                com3.CommandType = CommandType.StoredProcedure;
                try
                {
                    SqlParameter Kontrol = new SqlParameter("@WEBADI", SqlDbType.VarChar, 50);
                    Kontrol.Value = Kontrolismi;
                    com3.Parameters.Add(Kontrol);

                    SqlParameter p_sirket = new SqlParameter("@COMP", SqlDbType.VarChar);
                    p_sirket.Value = sirket;
                    com3.Parameters.Add(p_sirket);

                    SqlParameter p_kad = new SqlParameter("@USER", SqlDbType.VarChar);
                    p_kad.Value = kad;
                    com3.Parameters.Add(p_kad);

                }
                catch
                {
                    return sonuc;
                }
                try
                {
                    conSQL.Open();
                    using (SqlDataAdapter adp = new SqlDataAdapter(com3))
                    {
                        DataTable dt = new DataTable();
                        adp.Fill(dt);
                        sonuc = dt.Rows[0]["YETKI"].ToString() == "1" ? true : false;
                        conSQL.Close();
                    }
                }
                catch
                {
                    return sonuc;
                }
                finally
                {
                    conSQL.Close();
                }
                return sonuc;
            }
        }
    }

    

    public string MusteriyecevirKontrol(string sorgu)
    {
        baglanti = new SqlConnection(strConnString);
        string sonuc = string.Empty;
        try
        {
            SqlCommand query = new SqlCommand(sorgu, baglanti);
            baglanti.Open();
            sonuc = query.ExecuteScalar().ToString();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }


        return sonuc;
    }

   
    public string GetContactMusteriNo(string text, string Comp)
    {


        string _return = string.Empty;
        using (SqlConnection baglanti = new SqlConnection(strConnString))
        {
            try
            {
                baglanti.Open();
                using (SqlCommand sorgu = new SqlCommand("select [Müşteri No] Kod from [0C_05050_00_CONTACT] " +
                    "  where COMPANY='" +  Comp + "' AND No_='" + text + "'", baglanti))
                {

                    _return = sorgu.ExecuteScalar().ToString();

                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                baglanti.Close();
            }
        }

        return _return;


    }
    public string GetKontakSirketNo(string text, string Comp)
    {

        using (baglanti = new SqlConnection(strConnString))
        {
            baglanti.Open();
            using (SqlCommand sorgu = new SqlCommand("Select [Company No_] Kod from [0C_05050_00_CONTACT] where " +
                 "COMPANY='" + Comp + "' AND  No_='" + text + "'", baglanti))
            {
                SqlDataReader sqr;
                sqr = sorgu.ExecuteReader();
                if (sqr.Read())
                {
                    return text = sqr["Kod"].ToString();
                }
                else
                {
                    return text = "";
                }

                sqr.Close();
            }
        }

    }
    public string GetMusteriContactNo(string text, string Comp)
    {
        string _return = string.Empty;
        using (SqlConnection baglanti = new SqlConnection(strConnString))
        {
            try
            {
                baglanti.Open();
                using (SqlCommand sorgu = new SqlCommand("select No_ Kod from [0C_05050_00_CONTACT]  " +
                    " where COMPANY='" + Comp + "' AND [Müşteri No]='" + text + "'", baglanti))
                {

                    _return = sorgu.ExecuteScalar().ToString();

                }
            }
            catch (Exception)
            {

            }
            finally
            {
                baglanti.Close();
            }
        }

        return _return;



    }

    public bool GetMusteriOnOnayKontrol(string Code, string Comp)
    {
        SqlConnection baglanti = new SqlConnection(strConnString);
        bool Kontrol = false;
        try
        {
            baglanti.Open();
            string sorgu = "SELECT [Ön Onay] FROM [GAC_NAV2].[dbo].[0C_00018_00_CUSTOMER]  WHERE " +
         "COMPANY = '" + Comp + "' AND [No_] = '" + Code + "'";
            SqlCommand query = new SqlCommand(sorgu, baglanti);
            Kontrol = Convert.ToBoolean(Convert.ToInt32(query.ExecuteScalar()));
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }
        return Kontrol;



    }

    public bool GetMusteriBlockedKontrol(string Code, string Comp)
    {

        SqlConnection baglanti = new SqlConnection(strConnString);
        bool Kontrol = false;
        try
        {
            baglanti.Open();
            string sorgu = "SELECT [Blocked] FROM [GAC_NAV2].[dbo].[0C_00018_00_CUSTOMER]  WHERE " +
                  "COMPANY = '" + Comp + "' AND [No_] = '" + Code + "' AND [Ön Onay]=1";
            SqlCommand query = new SqlCommand(sorgu, baglanti);
            Kontrol = Convert.ToBoolean(Convert.ToInt32(query.ExecuteScalar()));
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }
        return Kontrol;




    }

    public bool GetTedarikciOnOnayKontrol(string Code, string Comp)
    {
        SqlConnection baglanti = new SqlConnection(strConnString);
        bool Kontrol = false;
        try
        {
            baglanti.Open();
            string sorgu = "select [Ön Onay] from [GAC_NAV2].[dbo].[0C_00023_00_VENDOR]  " +
            "where COMPANY = '" +   Comp + "' AND No_='" + Code + "'";
            SqlCommand query = new SqlCommand(sorgu, baglanti);
            Kontrol = Convert.ToBoolean(Convert.ToInt32(query.ExecuteScalar()));
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }
        return Kontrol;



    }

    public bool GetTedarikciBlockedKontrol(string Code, string Comp)
    {

        SqlConnection baglanti = new SqlConnection(strConnString);
        bool Kontrol = false;
        try
        {
            baglanti.Open();
            string sorgu = "select Blocked from [GAC_NAV2].[dbo].[0C_00023_00_VENDOR] " +
            "where COMPANY = '" + Comp + "' AND No_='" + Code + "' and [Ön Onay]=1";
            SqlCommand query = new SqlCommand(sorgu, baglanti);
            Kontrol = Convert.ToBoolean(Convert.ToInt32(query.ExecuteScalar()));

        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }
        return Kontrol;


    }
 

    public void SatısTemsilcisiDoldur(DropDownList ddl, string Comp)
    {
        baglanti = new SqlConnection(strConnString);
        try
        {
            SqlDataAdapter ad = new SqlDataAdapter("Select Code,Code +' - ' + Name as name from [0C_00013_00_SALESPERSON] where COMPANY='" + Comp + "' ", baglanti);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            ddl.DataSource = ds;
            ddl.DataTextField = "name";
            ddl.DataValueField = "Code";
            ddl.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }
    }



    public void SirketKisiDoldur(string sirketno, DropDownList ddl, string Comp)
    {
        baglanti = new SqlConnection(strConnString);
        try
        {
            SqlDataAdapter ad = new SqlDataAdapter("Select No_, Name from [0C_05050_01_CONTACT]  where COMPANY='" + Comp + "' and [Company No_]= '" + sirketno + "' and Type = 1", baglanti);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            ddl.DataSource = ds;
            ddl.DataTextField = "Name";
            ddl.DataValueField = "No_";
            ddl.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }

    }

    public void SirketKisiveTemsilciDoldur(string sirketno, DropDownList ddl, string Comp)
    {
        baglanti = new SqlConnection(strConnString);
        try
        {
            SqlCommand cmd = new SqlCommand("sp_0WEB_KATILIMCI_DOLDUR", baglanti);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@COMP", Comp);
            cmd.Parameters.AddWithValue("@sirketno", sirketno);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            ddl.Items.Clear();
            ddl.DataSource = ds;
            ddl.DataTextField = "Name";
            ddl.DataValueField = "Code";
            ddl.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }

    }

    public void iletisimContactTemsilciDoldur(string sirketno, DropDownList ddl, string Comp)
    {
        baglanti = new SqlConnection(strConnString);
        try
        {
            baglanti.Open();
            SqlCommand sorgu = new SqlCommand("Select [Salesperson Code] as temsilcikodu from [0C_05050_01_CONTACT] where  COMPANY='" + Comp + "' and [No_]= '" + sirketno + "' and Type = 0", baglanti);
            SqlDataReader sqr;
            sqr = sorgu.ExecuteReader();
            if (sqr.Read())
            {
                if (sqr["temsilcikodu"].ToString() != "")
                {
                    ddl.SelectedValue = sqr["temsilcikodu"].ToString();
                }

            }
            else
            {
                ddl.SelectedValue = "";
            }

            sqr.Close();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }


    }

    

    #region Mudül 1 Revize

    public void iletisimturudoldur(int tur, DropDownList ddl, string Comp)
    {
        baglanti = new SqlConnection(strConnString);
        try
        {
            string sorgu = string.Empty;
            switch (tur)
            {
                case 0:
                    sorgu = "Select Name from [0C_50022_01_ILETISIM_TURLERI_TYPE] " +
                        "where COMPANY='" + Comp + "' AND Type= '" + tur + "' order by Name asc";
                    break;
                case 1:
                    sorgu = "Select Name from [0C_50022_02_ILETISIM_TURLERI_REASON] " +
                        "where COMPANY='" + Comp + "'  order by Name asc";
                    break;
                case 2:
                    sorgu = "Select Name from [0C_50022_03_ILETISIM_TURLERI_RESULT] " +
                        "where COMPANY='" + Comp + "'  order by Name asc";
                    break;
            }
            SqlDataAdapter ad = new SqlDataAdapter(sorgu, baglanti);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            ddl.DataSource = ds;
            ddl.DataTextField = "Name";
            ddl.DataValueField = "Name";
            ddl.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }

    }

    #endregion

    #region Modül 2 Revize

    public string returnKur(string tarih, string dovizcinsi, string Comp)
    {

        baglanti = new SqlConnection(strConnString);
        try
        {
            string kur = string.Empty;
            string tarihcevir = "";
            if (tarih.Length == 10)
            {
                tarihcevir = tarih;
                string[] tcevir = tarihcevir.Split('.');
                baglanti.Open();
                if (dovizcinsi == string.Empty)
                {
                    dovizcinsi = "TL";
                }
                if (dovizcinsi == "TL") return "1.00";
                SqlCommand sorgu = new SqlCommand("Select CONVERT(decimal(10,4),[Relational Exch_ Rate Amount]) as kur from [00_EXCHANGE RATES]" +
                    " where COMPANY='" + Comp + "' AND [Starting Date]='" + tcevir[1] + "." + tcevir[0] + "." + tcevir[2] + "' and [Currency Code]='" + dovizcinsi + "'", baglanti);
                SqlDataReader sqr;
                sqr = sorgu.ExecuteReader();
                if (sqr.Read())
                {
                    kur = sqr["kur"].ToString();
                }
                else
                {
                    kur = "0.00";
                }

                sqr.Close();
            }
            else
            {
                kur = "0.00";
            }
            return kur;
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }



    }

    public string GetReportPath(string reportName, string Comp)
    {
        string reportPath;

        baglanti = new SqlConnection(strConnString);
        try
        {
            baglanti.Open();
            SqlCommand sorgu = new SqlCommand("Select TOP 1 [Report Path] from [0C_50409_00_REPORT_DIRECTORY] where COMPANY='" + Comp + "' AND [Report Name]='" + reportName + "'", baglanti);
            try
            {
                reportPath = sorgu.ExecuteScalar().ToString();
            }
            catch (Exception)
            {
                reportPath = "Rapor Yolu Okunamadı.";
                throw new Exception(reportPath);
            }

        }

        catch (Exception ex)
        {

            throw ex;
        }
        finally
        {
            baglanti.Close();
        }

        return reportPath;


    }
   
    #endregion
     

    #region Muhasebe & Fatura Revize 1 Sonradan Taşındı 
    public int KKEGbul(string sirket, string kaynakno)
    {
        SqlConnection baglanti = new SqlConnection(strConnString);
        try
        {
            baglanti.Open();
            SqlCommand sorgu = new SqlCommand("select KKEG as kkeg from [0C_00156_00_RESOURCE] where COMPANY='" + sirket + "' and No_='" + kaynakno + "'", baglanti);
            SqlDataReader sqr;
            sqr = sorgu.ExecuteReader();
            if (sqr.Read())
            {
                return Convert.ToInt32(sqr["kkeg"]);
            }
            else
            {
                return 0;
            }

            sqr.Close();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }

    }

    public bool vendororcustomercontrol(string company, int type, string kod)
    {

        SqlConnection baglanti = new SqlConnection(strConnString);
        try
        {
            baglanti.Open();
            SqlCommand sorgu = null;
            if (type == 0)
            {
                sorgu = new SqlCommand("Select [Own Customer] as kod from [0C_00079_00_COMP_INFO] where COMPANY='" + company + "' ", baglanti);
            }
            else if (type == 1)
            {
                sorgu = new SqlCommand("Select [Own Vendor] as kod from [0C_00079_00_COMP_INFO] where COMPANY='" + company + "' ", baglanti);
            }

            SqlDataReader sqr;
            sqr = sorgu.ExecuteReader();
            if (sqr.Read())
            {
                if (sqr["kod"].ToString() == kod)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }

            sqr.Close();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }


    }
    
    public void bankalarigetir2(string sirket, string kadi, DropDownList ddl)
    {
        SqlConnection baglanti = new SqlConnection(strConnString);
        try
        {
            SqlDataAdapter ad = new SqlDataAdapter("Select [Hesap No] as hesapno,[Banka Hesap İsmi] as hesapismi from [0C_50014_00_BANKA_KASA_YETKI] where COMPANY='" + sirket + "' and [Banka No]='' and [Kullanıcı Adı] ='" + kadi + "' and Personel =1", baglanti);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            ddl.Items.Insert(0, "");
            ddl.DataSource = ds;
            ddl.DataTextField = "hesapismi";
            ddl.DataValueField = "hesapno";
            ddl.DataBind();

        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }

    }
    

    public void KDVUrunNakilGrubuDoldur(string sirket, DropDownList ddl, int tur)
    {
        SqlConnection baglanti = new SqlConnection(strConnString);
        try
        {
            baglanti.Open();
            SqlDataAdapter ad = new SqlDataAdapter("Select Code,Description from [0C_00324_00_VAT_PRODUCT_POSTING_GROUPS] where COMPANY='" + sirket + "' and [Iade]=" + tur.ToString() + " and [Web Gosterme] = 0", baglanti);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            ddl.DataSource = ds;
            ddl.DataTextField = "Code";
            ddl.DataValueField = "Code";
            ddl.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }


    }
    public void alacakhesabıdoldur(string sirket, DropDownList ddl, int per, string kadi)
    {
        SqlConnection baglanti = new SqlConnection(strConnString);
        try
        {
            baglanti.Open();
            SqlDataAdapter ad;
            if (per == 1)
            {
                ad = new SqlDataAdapter("Select No_,Name as Name from [0C_00023_00_VENDOR] where COMPANY='" + sirket + "' AND [Vendor Posting Group] LIKE '195%'", baglanti);
            }
            else if (per == 2)
            {
                ad = new SqlDataAdapter("Select No_,Name as Name from [0C_00270_00_BANK ACCOUNTS] where COMPANY='" + sirket + "' AND  [Web Kasa]=1", baglanti);
            }
            else if (per == 3)
            {
                ad = new SqlDataAdapter("Select No_,Name as Name from [0C_00270_00_BANK ACCOUNTS] where  COMPANY='" + sirket + "' AND [Bank Acc_ Posting Group] LIKE '195%'", baglanti);
            }
            else
            {
                ad = new SqlDataAdapter("Select [Hesap No] as No_ ,[Banka Hesap İsmi] as Name from [0C_50014_00_BANKA_KASA_YETKI] where COMPANY='" + sirket + "' AND  [Banka No]='' and [Kullanıcı Adı] ='" + kadi + "' and Kasa =1", baglanti);
            }
            DataTable ds = new DataTable();
            ad.Fill(ds);
            ddl.DataSource = ds;
            ddl.DataTextField = "Name";
            ddl.DataValueField = "No_";
            ddl.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }


    }
    
    public void BolgeDoldur(string sirket, DropDownList ddl)
    {
        SqlConnection baglanti = new SqlConnection(strConnString);
        try
        {
            baglanti.Open();
            SqlDataAdapter ad = new SqlDataAdapter("Select * from [0C_00349_00_DIMENSION VALUES] where COMPANY='" + sirket + "' and [Dimension Code]= 'BÖLGE' and Blocked=0 and Totaling='' ", baglanti);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            ddl.DataSource = ds;
            ddl.DataTextField = "Name";
            ddl.DataValueField = "Code";
            ddl.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }

    }
    public void PersonelDoldur(string sirket, DropDownList ddl)
    {
        SqlConnection baglanti = new SqlConnection(strConnString);
        try
        {
            baglanti.Open();
            SqlDataAdapter ad = new SqlDataAdapter("Select * from [0C_00349_00_DIMENSION VALUES] where COMPANY='" + sirket + "' and [Dimension Code]= 'PERSONEL' and Totaling='' and Blocked = 0 order by Name asc ", baglanti);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            ddl.DataSource = ds;
            ddl.DataTextField = "Name";
            ddl.DataValueField = "Code";
            ddl.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }

    }
    public void AracPlakaDoldur(string sirket, DropDownList ddl)
    {
        SqlConnection baglanti = new SqlConnection(strConnString);
        try
        {
            baglanti.Open();
            SqlDataAdapter ad = new SqlDataAdapter("Select * from [0C_00349_00_DIMENSION VALUES] where COMPANY='" + sirket + "' and [Dimension Code]= 'ARAÇ_PLAKA' and Totaling='' ", baglanti);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            ddl.DataSource = ds;
            ddl.DataTextField = "Name";
            ddl.DataValueField = "Code";
            ddl.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }

    }
    public void HizmetDoldur(string sirket, DropDownList ddl)
    {
        SqlConnection baglanti = new SqlConnection(strConnString);
        try
        {
            baglanti.Open();
            SqlDataAdapter ad = new SqlDataAdapter("Select * from [0C_00349_00_DIMENSION VALUES] where COMPANY='" + sirket + "' and [Dimension Code]= 'FAALİYET' and Totaling='' and Blocked = 0", baglanti);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            ddl.DataSource = ds;
            ddl.DataTextField = "Name";
            ddl.DataValueField = "Code";
            ddl.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }

    }
     
    public void tur1doldur(string sirket, DropDownList ddl)
    {

        SqlConnection baglanti = new SqlConnection(strConnString);
        try
        {
            baglanti.Open();
            ddl.Items.Clear();
            ListItem lv = new ListItem();
            lv.Selected = true;
            lv.Text = "";
            lv.Value = "";
            ddl.Items.Add(lv);
            SqlDataAdapter ad = new SqlDataAdapter("Select Kod,Kod+' - '+Aciklama as name from [0C_50003_00_KAYNAK_GRUPLARI] where COMPANY='" + sirket + "' and Tur = 0", baglanti);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            ddl.DataSource = ds;
            ddl.DataTextField = "name";
            ddl.DataValueField = "Kod";
            ddl.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }



    }
    public void tur2doldur(string sirket, string tur1, DropDownList ddl)
    {

        SqlConnection baglanti = new SqlConnection(strConnString);
        try
        {
            baglanti.Open();
            ddl.Items.Clear();
            ListItem lv = new ListItem();
            lv.Selected = true;
            lv.Text = "";
            lv.Value = "";
            ddl.Items.Add(lv);
            SqlDataAdapter ad = new SqlDataAdapter("Select Kod,Kod+' - '+Aciklama as name from [0C_50003_00_KAYNAK_GRUPLARI] where COMPANY='" + sirket + "' and Tur = 1 and [Tur 1 Code]='" + tur1 + "'", baglanti);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            ddl.DataSource = ds;
            ddl.DataTextField = "name";
            ddl.DataValueField = "Kod";
            ddl.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }


    }
    public void tur3doldur(string sirket, string tur1, string tur2, DropDownList ddl)
    {

        SqlConnection baglanti = new SqlConnection(strConnString);
        try
        {
            baglanti.Open();
            ddl.Items.Clear();
            ListItem lv = new ListItem();
            lv.Selected = true;
            lv.Text = "";
            lv.Value = "";
            ddl.Items.Add(lv);
            SqlDataAdapter ad = new SqlDataAdapter("Select Kod,Kod+' - '+Aciklama as name from [0C_50003_00_KAYNAK_GRUPLARI] where COMPANY='" + sirket + "' and Tur = 2 and [Tur 1 Code] ='" + tur1 + "' and [Tur 2 Code]='" + tur2 + "' and [Kullanım Dışı] =0", baglanti);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            ddl.DataSource = ds;
            ddl.DataTextField = "name";
            ddl.DataValueField = "Kod";
            ddl.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }
    }
    public void projeboyutdoldur(string sirket, string projeno, string projegorevno, DropDownList ddlbolge, DropDownList ddldepartman, DropDownList ddlfaaliyet, DropDownList ddlpersonel, DropDownList ddlarac)
    {
        SqlConnection baglanti = new SqlConnection(strConnString);
        try
        {
            baglanti.Open();
            string query = "select * from [0C_01002_04_JOB TASK DIMENSION JOBS] where COMPANY='" + sirket + "' and [Job No_]='" + projeno + "' and [Job Task No_]='" + projegorevno + "'";
            SqlCommand sorgu = new SqlCommand(query, baglanti);

            SqlDataReader sqr = sorgu.ExecuteReader();
            while (sqr.Read())
            {
                if (sqr["BLG"].ToString() != "")
                    ddlbolge.SelectedValue = sqr["BLG"].ToString();
                if (sqr["DEPT"].ToString() != "")
                    ddldepartman.SelectedValue = sqr["DEPT"].ToString();
                if (sqr["FAAL"].ToString() != "")
                    ddlfaaliyet.SelectedValue = sqr["FAAL"].ToString();
                if (sqr["PERS"].ToString() != "")
                    ddlpersonel.SelectedValue = sqr["PERS"].ToString();
                if (sqr["ARACPLAKA"].ToString() != "")
                    ddlarac.SelectedValue = sqr["ARACPLAKA"].ToString();
                

            }


            sqr.Close();
            sqr = null;
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }
    }

    public void satisfaturamusteribilgilerisatissip1(string sirket, string projeno, string mustno, TextBox adres, ASPxTextBox vergidaire, ASPxTextBox vergino, ASPxTextBox satist, ASPxComboBox ddlodemekodu, CheckBox cb, Label kdvnakilgrubu)
    {
        using (SqlConnection baglanti = new SqlConnection(strConnString))
        {
            try
            {
                baglanti.Open();
                using (SqlCommand sorgu = new SqlCommand("Select c.Address+c.[Address 2] as adres,c.[Tax Area Code] as vergidaire,c.[Tax Registration No_] as vergino,MTADI as satist,c.[Payment Terms Code] as odemekodu,Avukatlik,c.[Customer Posting Group] as musterinakilgrubu From [0C_00018_02_CUSTOMER] c where  COMPANY='" + sirket + "' and c.[Salesperson Code] != 'PASİF' AND c.No_='" + mustno + "'", baglanti))
                {
                    SqlDataReader sqr;
                    sqr = sorgu.ExecuteReader();
                    if (sqr.Read())
                    {
                        adres.Text = sqr["adres"].ToString();
                        vergidaire.Text = sqr["vergidaire"].ToString();
                        vergino.Text = sqr["vergino"].ToString();
                        satist.Text = sqr["satist"].ToString();
                        ddlodemekodu.Value = sqr["odemekodu"].ToString();
                        cb.Checked = Convert.ToBoolean(sqr["Avukatlik"]);
                        kdvnakilgrubu.Text = sqr["musterinakilgrubu"].ToString();
                    }
                    else
                    {
                        adres.Text = string.Empty;
                        vergidaire.Text = string.Empty;
                        vergino.Text = string.Empty;
                        satist.Text = string.Empty;
                        ddlodemekodu.SelectedIndex = -1;
                        kdvnakilgrubu.Text = string.Empty;
                    }

                    sqr.Close();
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                baglanti.Close();
            }
        }


    }
    
    public bool KaynakKodKontrol(string sirket, string tur1, string tur2, string tur3)
    {

        using (baglanti = new SqlConnection(strConnString))
        {
            bool sonuc = false;
            baglanti.Open();
            string query = "select No_ AS kod,Name as ad from [0C_00156_00_RESOURCE] where COMPANY='" + sirket + "' and [Tur Kodu]='" + tur1 + tur2 + tur3 + "' and Bordro=0";

            SqlCommand sorgu = new SqlCommand(query, baglanti);

            SqlDataAdapter sorguDA = new SqlDataAdapter(sorgu);
            DataTable sorgugridDT = new DataTable();
            sorguDA.Fill(sorgugridDT);
            SqlDataReader sqr;
            sqr = sorgu.ExecuteReader();
            if (sqr.Read())
            {
                sonuc = true;
            }
            return sonuc;
            sqr.Close();
            baglanti.Close();
        }
    }

    public void KaynakKoduDoldur(string sirket, string tur1, string tur2, string tur3, Label lblkod, TextBox txt)
    {
        lblkod.Text = string.Empty;
        SqlConnection baglanti = new SqlConnection(strConnString);
        try
        {
            baglanti.Open();
            string query = "select No_ AS kod,Name as ad from [0C_00156_00_RESOURCE] where COMPANY='" + sirket + "' and [Tur Kodu]='" + tur1 + tur2 + tur3 + "' and Bordro=0";
            SqlCommand sorgu = new SqlCommand(query, baglanti);
            SqlDataAdapter sorguDA = new SqlDataAdapter(sorgu);
            DataTable sorgugridDT = new DataTable();
            sorguDA.Fill(sorgugridDT);
            SqlDataReader sqr;
            sqr = sorgu.ExecuteReader();
            if (sqr.Read())
            {
                lblkod.Text = sqr["kod"].ToString();
                txt.Text = sqr["ad"].ToString();
            }

            sqr.Close();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }

    }
 
    public void projeKoduDoldur(string sirket, string ad, Label lblkod)
    {
        lblkod.Text = string.Empty;
        SqlConnection baglanti = new SqlConnection(strConnString);
        try
        {
            baglanti.Open();
            string query = "select No_ AS kod from [0C_00167_00_JOB] where COMPANY='" + sirket + "' and [GAC Status] = 1 and [GAC Proje No]='" + ad + "'";
            SqlCommand sorgu = new SqlCommand(query, baglanti);
            SqlDataAdapter sorguDA = new SqlDataAdapter(sorgu);
            DataTable sorgugridDT = new DataTable();
            sorguDA.Fill(sorgugridDT);
            SqlDataReader sqr;
            sqr = sorgu.ExecuteReader();
            if (sqr.Read())
            {
                lblkod.Text = sqr["kod"].ToString();
            }

            sqr.Close();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }

    }
    public void projegorevKoduDoldur(string sirket, string pkod, string pgad, int tur, Label lblkod)
    {
        lblkod.Text = string.Empty;
        SqlConnection baglanti = new SqlConnection(strConnString);

        try
        {
            baglanti.Open();
            string query = "select top 1 [Job Task No_] AS kod from [0C_01001_00_JOB_TASK] where COMPANY='" + sirket + "' and [Job No_]='" + pkod + "'";
            if (!string.IsNullOrEmpty(pgad))
            {
                query += " and  (UPPER(Description)+' | '+UPPER([Liman Adı]) +' | '+ UPPER([Job Task No_]) =UPPER('" + pgad + "') or UPPER(Description)=UPPER('" + pgad + "'))";
            }
            SqlCommand sorgu = new SqlCommand(query, baglanti);
            SqlDataReader read = sorgu.ExecuteReader();
            while (read.Read())
            {
                lblkod.Text = read["kod"].ToString();
            }
            read.Close();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }

    }
    public void projeadidoldur(string sirket, string no, TextBox txt)
    {
        SqlConnection baglanti = new SqlConnection(strConnString);
        try
        {
            baglanti.Open();
            SqlCommand sorgu = new SqlCommand("Select [GAC Proje No] as name from [0C_00167_00_JOB] where COMPANY='" + sirket + "' and No_='" + no + "'", baglanti);
            SqlDataReader sqr;
            sqr = sorgu.ExecuteReader();
            if (sqr.Read())
            {
                txt.Text = sqr["name"].ToString();
            }
            else
            {
                txt.Text = "";
            }

            sqr.Close();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }
    }
    public void projegorevadidoldur(string sirket, string projeno, string no, TextBox txt)
    {
        SqlConnection baglanti = new SqlConnection(strConnString);
        try
        {
            baglanti.Open();
            SqlCommand sorgu = new SqlCommand("Select Description as name from [0C_01001_00_JOB_TASK] where COMPANY='" + sirket + "' and [Job No_]='" + projeno + "' and [Job Task No_]='" + no + "'", baglanti);
            SqlDataReader sqr;
            sqr = sorgu.ExecuteReader();
            if (sqr.Read())
            {
                txt.Text = sqr["name"].ToString();
            }
            else
            {
                txt.Text = "";
            }

            sqr.Close();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }




    }
 

    public void kurgetir(string sirket, TextBox kur, string tarih, string dovizcinsi)
    {
        SqlConnection baglanti = new SqlConnection(strConnString);
        try
        {
            string tarihcevir = "";
            if (tarih.Length == 10)
            {
                tarihcevir = tarih;
                string[] tcevir = tarihcevir.Split('.');
                baglanti.Open();
                SqlCommand sorgu = new SqlCommand("Select CONVERT(decimal(10,4),[Relational Exch_ Rate Amount]) as kur from [00_EXCHANGE RATES] where COMPANY='" + sirket + "' and [Starting Date]='" + tcevir[1] + "." + tcevir[0] + "." + tcevir[2] + "' and [Currency Code]='" + dovizcinsi + "'", baglanti);
                SqlDataReader sqr;
                sqr = sorgu.ExecuteReader();
                if (sqr.Read())
                {
                    kur.Text = sqr["kur"].ToString();
                }
                else
                {
                    kur.Text = "1.00";
                }

                sqr.Close();
            }
            else
            {
                kur.Text = "1.00";
            }
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }
    }
     
    public void DepartmanDoldur(string sirket, DropDownList ddl)
    {
        SqlConnection baglanti = new SqlConnection(strConnString);
        try
        {
            baglanti.Open();
            SqlDataAdapter ad = new SqlDataAdapter("Select * from [0C_00349_00_DIMENSION VALUES] where COMPANY='" + sirket + "' and [Dimension Code]= 'DEPARTMAN' and Blocked = 0 and Totaling=''", baglanti);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            ddl.Items.Clear();
            ddl.Items.Insert(0, "");
            ddl.DataSource = ds;
            ddl.DataTextField = "Name";
            ddl.DataValueField = "Code";
            ddl.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }

    }
     

    public bool WebYetkiKontrol(string departman, string bolge, string field, string createby, string dpts, string blgs, string mt, string Comp, string userName)
    {
       
        baglanti = new SqlConnection(strConnString);
        baglanti.Open();
        SqlCommand cmd = new SqlCommand("SP_0WEB_YETKI_KONTROL_BUTTON", baglanti);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@COMP", Comp);
        cmd.Parameters.AddWithValue("@USER", userName);
        cmd.Parameters.AddWithValue("@ALAN", field);
        cmd.Parameters.AddWithValue("@CREATEBY", createby);
        cmd.Parameters.AddWithValue("@DEPT", departman);
        cmd.Parameters.AddWithValue("@DPTS", dpts);
        cmd.Parameters.AddWithValue("@BLG", bolge);
        cmd.Parameters.AddWithValue("@BLGS", blgs);
        cmd.Parameters.AddWithValue("@MT", mt);
        string _result = cmd.ExecuteScalar().ToString();
        if (_result == "0")
            return false;
        else
            return true;
    }
  

    public bool MaksDovizTutariKontrol(string dovizCinsi, string tutar, string selectedDate, string Comp)
    {
        bool result = false;
        SqlConnection baglanti = new SqlConnection(strConnString);
        string sonuc = "";
        try
        {
            baglanti.Open();
            string queryText = "select count(*) from [0C_00004_00_CURRENCY] where COMPANY ='" + Comp + "' and Code='" + dovizCinsi + "' and " + tutar + "< MAXDEGER ";
            SqlCommand query = new SqlCommand(queryText, baglanti);
            sonuc = query.ExecuteScalar().ToString();
        }
        catch (Exception)
        {
        }
        finally
        {
            baglanti.Close();
        }
        if (sonuc == "0")
            result = false;
        else
            result = true;

        return result;
    }


	
    public bool GETDepoMusteriKaydi(string Code, string Comp)
    {

        SqlConnection baglanti = new SqlConnection(strConnString);
        bool Kontrol = false;
        try
        {
            baglanti.Open();
            string sorgu = "select case when LEFT([Depo Cari Kod],2) ='CD'  THEN 1  ELSE 0 END from [0C_00018_00_CUSTOMER_DEPO] WHERE " +
                  "COMPANY = '" + Comp + "' AND [No_] = '" + Code + "' ";
            SqlCommand query = new SqlCommand(sorgu, baglanti);
            Kontrol = Convert.ToBoolean(Convert.ToInt32(query.ExecuteScalar()));
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }
        return Kontrol;




    }
    public bool GETDepoTedarikciKaydi(string Code, string Comp)
    {

        SqlConnection baglanti = new SqlConnection(strConnString);
        bool Kontrol = false;
        try
        {
            baglanti.Open();
            string sorgu = "select case when LEFT([Depo Cari Kod],2) ='VD'  THEN 1  ELSE 0 END from [0C_00023_00_VENDOR_DEPO] WHERE " +
                  "COMPANY = '" + Comp + "' AND [No_] = '" + Code + "' ";
            SqlCommand query = new SqlCommand(sorgu, baglanti);
            Kontrol = Convert.ToBoolean(Convert.ToInt32(query.ExecuteScalar()));
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            baglanti.Close();
        }
        return Kontrol;




    }
    #endregion
}
#endregion