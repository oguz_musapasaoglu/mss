﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace MSS1.Codes
{
    public class NavServicesContents
    {
        Methods method = new Methods();

        static void WriteLog(string ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex);
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            string path = HttpContext.Current.Server.MapPath("~/App_Data/Log.txt");
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(path, true))
            {
                writer.WriteLine(message);
                writer.Close();
            }
        }
        public string[] NavServiceParams(string functionName, string[] _params)
        {
        
            DataTable dt = new fonksiyonlar().datatablegonder("select * from [0C_70054_00_WEB_FUNCTIONS] where FunctionName='" + functionName + "'");
         
            string codeUnit = dt.Rows[0][0].ToString();
       
            string _paramTypes = dt.Rows[0][2].ToString();
           
            string _par = string.Empty;
            
            string[] _parType = _paramTypes.Split(',');

            if (_params.Length != 0)
            {

            
                if (_parType.Length != _params.Length)
                {
                    throw new Exception("Tür tablosunda " + _parType.Length.ToString() + " değer var. WPC'de " + _params.Length.ToString() + " değer var. Bilgiler Tutarsız.");
                } 
                if (_parType.Length > 0)
                {
                    for (int i = 0; i < _params.Length; i++)
                    {
                        string _values = string.Empty;
                        switch (_parType[i])
                        {
                            case "Tx":
                                _values = _params[i];
                                break;
                            case "Dt":
                                _values = method.DateToStringNAVType(_params[i]);
                                break;
                            case "In":
                                _values = method.StringToInt(_params[i]).ToString();
                                break;
                            case "Dc":
                                _values = method.StringToDecimal(_params[i]).ToString();
                                break;
                            case "Bl":
                                _values = method.StringToInt(_params[i]).ToString();
                                break;
                            case "Tm":
                                _values = method.TimeText(_params[i]).ToString();
                                break;
                        }

                        _par += _values + "§";
                    }
           
                    _par = _par.Remove(_par.Length - 1, 1);
           
                }
                else
                {
                    throw new Exception("Parametre Tipleri Hatalı.");
                }
            }

            string[] result = new string[3];
            result[0] = codeUnit;
            result[1] = functionName;
            result[2] = _par; 
            return result;
        }
    }
}