﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.Web;
using DevExpress.Web;
using MSS1.WSGeneric;
using MSS1.Codes;

public class ServiceUtilities
{

    public Methods method = new Methods();
    public ws_baglantı ws = new ws_baglantı();
    Methods m = new Methods();
    public Web_GenericFunction_PortClient webFonksiyonlari;
    //public Web_Operasyon_Shipping_PortClient webShipping;
    public BasicHttpBinding _binding;
    public EndpointAddress _endpoint;
    public string customSirket = "LAMLOJ";

    private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
    {

        _binding = new BasicHttpBinding();
        _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
        _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
        EndpointAddress _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
        webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
        webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
        webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
        return webFonksiyonlari;
    }

    public ServiceUtilities()
    {

    }

    public static string paramresult(string[] _array)
    {
        string result = string.Empty;
        foreach (string item in _array)
        {
            result = result + "*" + item;
        }
        return result;
    }

    public static void ProcSelectedRows(ASPxGridView grid, DataRow[] rows, string kulad, string Sirket)
    {
        HttpContext.Current.Session["WebMessage"] = string.Empty;
        if (rows.Length > 0)
        {
            string filter = rows[0]["DX Filter"].ToString();
            string fieldId = rows[0]["DX Field ID"].ToString();
            string websource = rows[0]["DX Source"].ToString();
            string yetkistr = rows[0]["DX Permision Field"].ToString();
            bool HasYtk = false;
            List<object> ArrayValues = new List<object>();
            List<string> ArrayObjs = new List<string>();
            List<string> ServisObjs = new List<string>();
            int PSize = 0;

            if (!string.IsNullOrEmpty(filter) && !string.IsNullOrEmpty(websource))
            {
                Guid IslemId = Guid.NewGuid();
                ArrayObjs.Clear();
                ServisObjs.Clear();
                foreach (var item in filter.Split(','))
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        if (item.Substring(0, 1) == "$")
                            ArrayObjs.Add(item.Replace("$", string.Empty));
                        else
                            ServisObjs.Add(item);
                    }
                }
                if (!string.IsNullOrEmpty(yetkistr))
                {
                    if (yetkistr.Substring(0, 2) != "AK")
                    {
                        ArrayObjs.Add(yetkistr);
                        HasYtk = true;
                    }
                }



                PSize = ArrayObjs.Count;
                ArrayValues.Clear();
                if (grid.VisibleRowCount == 1) grid.Selection.SelectAll();
                ArrayValues = grid.GetSelectedFieldValues(ArrayObjs.ToArray());
                DynamicUtilsV1.InsertWebTopluIslem(IslemId, PSize, ArrayValues, HasYtk, ServisObjs, fieldId, websource, kulad, Sirket);
                string[] arr = new string[] { IslemId.ToString(), kulad };
                ServiceUtilities services = new ServiceUtilities();
                services.NavFunctionsStatic(kulad, Sirket, websource, arr);


            }
        }
    }

    public static void ProcSelectedRows(ASPxGridView grid, DataRow[] rows, string kulad, string Sirket, Guid IslemId)
    {
        HttpContext.Current.Session["WebMessage"] = string.Empty;
        if (rows.Length > 0)
        {
            string filter = rows[0]["DX Filter"].ToString();
            string fieldId = rows[0]["DX Field ID"].ToString();
            string websource = rows[0]["DX Source"].ToString();
            string yetkistr = rows[0]["DX Permision Field"].ToString();
            bool HasYtk = false;
            List<object> ArrayValues = new List<object>();
            List<string> ArrayObjs = new List<string>();
            List<string> ServisObjs = new List<string>();
            int PSize = 0;

            if (!string.IsNullOrEmpty(filter) && !string.IsNullOrEmpty(websource))
            {

                ArrayObjs.Clear();
                ServisObjs.Clear();
                foreach (var item in filter.Split(','))
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        if (item.Substring(0, 1) == "$")
                            ArrayObjs.Add(item.Replace("$", string.Empty));
                        else
                            ServisObjs.Add(item);
                    }
                }
                if (!string.IsNullOrEmpty(yetkistr))
                {
                    if (yetkistr.Substring(0, 2) != "AK")
                    {
                        ArrayObjs.Add(yetkistr);
                        HasYtk = true;
                    }
                }



                PSize = ArrayObjs.Count;
                ArrayValues.Clear();
                if (grid.VisibleRowCount == 1) grid.Selection.SelectAll();
                ArrayValues = grid.GetSelectedFieldValues(ArrayObjs.ToArray());
                DynamicUtilsV1.InsertWebTopluIslem(IslemId, PSize, ArrayValues, HasYtk, ServisObjs, fieldId, websource, kulad, Sirket);
                string[] arr = new string[] { IslemId.ToString(), kulad };
                ServiceUtilities services = new ServiceUtilities();
                services.NavFunctionsStatic(kulad, Sirket, websource, arr);


            }
        }
    }

    static void WriteLog(string ex)
    {
        string message = string.Format("Time: {0}", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
        message += Environment.NewLine;
        message += "-----------------------------------------------------------";
        message += Environment.NewLine;
        message += string.Format("Message: {0}", ex);
        message += Environment.NewLine;
        message += "-----------------------------------------------------------";
        message += Environment.NewLine;
        string path = HttpContext.Current.Server.MapPath("~/App_Data/Log.txt");
        using (System.IO.StreamWriter writer = new System.IO.StreamWriter(path, true))
        {
            writer.WriteLine(message);
            writer.Close();
        }
    }
    public void NavFunctionsStatic(string _user, string sirket, string functionName, string[] _params)
    {

        if (functionName.Contains("."))
        {
            string[] _funcar = functionName.Split('.');
            string _paramsAct = string.Empty;
            int _ind = 0;
            _paramsAct = _funcar[1];
            foreach (var str in _params)
            {

                if (str.Contains("_d"))
                    _paramsAct += "§" + method.DateToStringNAVType(str.Replace("_d", string.Empty));
                else if (str.Contains("_t"))
                    _paramsAct += "§" + method.TimeText(str.Replace("_t", string.Empty));
                else
                    _paramsAct += "§" + str;
                _ind++;

            }
            string _Returnval = GetWebFonksiyonlari(customSirket).ExecGenericWebFunctionAllComp(_user, _funcar[0], _paramsAct, sirket);
            if (_Returnval.Contains("|"))
            {
                string[] splitedstr = _Returnval.Split('|');

                if (!string.IsNullOrEmpty(splitedstr[1]))
                    HttpContext.Current.Session["InsertedVal"] = splitedstr[1];
                if (!string.IsNullOrEmpty(splitedstr[0]))
                    HttpContext.Current.Session["WebMessage"] = splitedstr[0].Replace("A call to System.Reflection.RuntimeMethodInfo.Invoke failed with this message:", string.Empty);
            }
            else
                HttpContext.Current.Session["WebMessage"] = _Returnval.Replace("A call to System.Reflection.RuntimeMethodInfo.Invoke failed with this message:", string.Empty);


        }
        else
        {
            NavServicesContents serviceContent = new NavServicesContents();
            string[] _pars = serviceContent.NavServiceParams(functionName, _params);
            HttpContext.Current.Session["WebMessage"] = GetWebFonksiyonlari(customSirket).ExecCustomWebFunctionAllComp(_user, sirket, _pars[0], _pars[1], _pars[2], "").Replace("A call to System.Reflection.RuntimeMethodInfo.Invoke failed with this message:", string.Empty);
        }
    }



}