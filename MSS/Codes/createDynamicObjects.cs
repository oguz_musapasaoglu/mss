﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using System.Web.UI;

public class createDynamicObjects
    {
        
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        fonksiyonlar f = new fonksiyonlar();
        public createDynamicObjects()
        { }
        public   GridViewDataComboBoxColumn creategridviewComboboboxColumn(string fieldID, string caption, int screenWidth, int width, int halignment, string bgcolor, string Source, string Filter, string Value, string Text)
        {
            GridViewDataComboBoxColumn _comboboxColumn = new GridViewDataComboBoxColumn();
            _comboboxColumn.FieldName = fieldID;
            _comboboxColumn.PropertiesComboBox.ClientInstanceName = fieldID;
            _comboboxColumn.Caption = caption;


            double resize = (double)screenWidth * ((double)width / 100);
            _comboboxColumn.Width = Unit.Pixel(Convert.ToInt32(resize));

            
            if (string.IsNullOrEmpty(bgcolor)) bgcolor = "#FFFFFF";
            _comboboxColumn.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);

            HorizontalAlign _haling = halignment == 0 ? HorizontalAlign.Left :
                                      halignment == 1 ? HorizontalAlign.Center :
                                      halignment == 2 ? HorizontalAlign.Right : HorizontalAlign.NotSet;

            _comboboxColumn.CellStyle.HorizontalAlign = _haling;
            _comboboxColumn.HeaderStyle.HorizontalAlign = _haling;
            _comboboxColumn.PropertiesComboBox.EnableCallbackMode = true;
            _comboboxColumn.PropertiesComboBox.CallbackPageSize = 10;
            _comboboxColumn.PropertiesComboBox.EnableCallbackMode = true;
         
            _comboboxColumn.PropertiesComboBox.ItemsRequestedByFilterCondition += comboMethods.PropertiesComboBox_ItemsRequestedByFilterCondition;
            _comboboxColumn.PropertiesComboBox.ItemRequestedByValue +=  comboMethods.PropertiesComboBox_ItemRequestedByValue;
            _comboboxColumn.PropertiesComboBox.ValueField = Value;
            _comboboxColumn.PropertiesComboBox.TextField = "customText";
            return _comboboxColumn;
        }


        public GridViewDataColumn creategridviewComboboboxColumnDeneme()
        {
            GridViewDataColumn dc = new GridViewDataColumn();
            dc.Caption = "deneme";
            dc.FieldName = "deneme";
            dc.DataItemTemplate = new ComboTemplate();
             return dc;

        }
    public GridViewDataComboBoxColumn creategridviewComboboboxColumntest(string fieldID, string caption, int screenWidth, int width, int halignment, string bgcolor, string Source, string Filter, string Value, string Text)
    {
        GridViewDataComboBoxColumn _comboboxColumn = new GridViewDataComboBoxColumn();
        _comboboxColumn.FieldName = fieldID;
        _comboboxColumn.PropertiesComboBox.ClientInstanceName = fieldID;
        _comboboxColumn.Caption = caption; 
        _comboboxColumn.PropertiesComboBox.EnableCallbackMode = true;
        _comboboxColumn.PropertiesComboBox.CallbackPageSize = 10; 

        _comboboxColumn.PropertiesComboBox.ItemsRequestedByFilterCondition += comboMethods.PropertiesComboBox_ItemsRequestedByFilterCondition;
        _comboboxColumn.PropertiesComboBox.ItemRequestedByValue += comboMethods.PropertiesComboBox_ItemRequestedByValue;
        _comboboxColumn.PropertiesComboBox.ValueField = Value;
        _comboboxColumn.PropertiesComboBox.TextField = "customText";
        return _comboboxColumn;
    }


    //public void PropertiesComboBox_ItemRequestedByValue(object source, ListEditItemRequestedByValueEventArgs e)
    //{
    //    long value = 0;
    //    if (e.Value == null || !Int64.TryParse(e.Value.ToString(), out value))
    //        return;

    //    SqlDataSource sqlds = new SqlDataSource();
    //    sqlds.ConnectionString = strConnString;
    //    ASPxComboBox comboBox = (ASPxComboBox)source;
    //    sqlds.SelectCommand = @"SELECT NEWID() PK, *,R0012 as customText FROM VW0063   ORDER BY R0012";
    //    sqlds.SelectParameters.Clear();
    //    sqlds.SelectParameters.Add("ID", TypeCode.Int64, e.Value.ToString());
    //    comboBox.DataSource = sqlds;
    //    comboBox.DataBind();
    //}

    //public void PropertiesComboBox_ItemsRequestedByFilterCondition(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    //{
    //    SqlDataSource sqlds = new SqlDataSource();
    //    sqlds.ConnectionString = strConnString;
    //    ASPxComboBox comboBox = (ASPxComboBox)source;
    //    sqlds.SelectCommand = "select * from (select NEWID() PK, *,R0012 as customText,R0012 as R0013,R0012 as R0014,R0012 as R0015,R0012 as R0016,R0012 as R0017, " +
    //          "row_number()over(order by wh.R0012) as customrec from" +
    //          " VW0063 wh where (R0012 like @filter+'%') ) st where st.customrec between @startIndex and @endIndex";
    //    sqlds.SelectParameters.Clear();
    //    sqlds.SelectParameters.Add("filter", TypeCode.String, string.Format("%{0}%", e.Filter));
    //    sqlds.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
    //    sqlds.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());
    //    comboBox.DataSource = sqlds;
    //    comboBox.DataBind();
    //}



    public static class comboMethods
        {

            public static string strConnStringStatic = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
            public static void PropertiesComboBox_ItemsRequestedByFilterCondition(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
            {
                SqlDataSource sqlds = new SqlDataSource();
                sqlds.ConnectionString = strConnStringStatic;
                ASPxComboBox comboBox = (ASPxComboBox)source;
                sqlds.SelectCommand = "select * from (select NEWID() PK, *,R0012 as customText,R0012 as R0013,R0012 as R0014,R0012 as R0015,R0012 as R0016,R0012 as R0017, " +
                      "row_number()over(order by wh.R0012) as customrec from" +
                      " VW0063 wh where (R0012 like @filter+'%') ) st where st.customrec between @startIndex and @endIndex";
                sqlds.SelectParameters.Clear();
                sqlds.SelectParameters.Add("filter", TypeCode.String, string.Format("%{0}%", e.Filter));
                sqlds.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
                sqlds.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());
                comboBox.DataSource = sqlds;
                comboBox.DataBind();
            }

            public static void PropertiesComboBox_ItemRequestedByValue(object source, ListEditItemRequestedByValueEventArgs e)
            {
                long value = 0;
                if (e.Value == null || !Int64.TryParse(e.Value.ToString(), out value))
                    return;

                SqlDataSource sqlds = new SqlDataSource();
                sqlds.ConnectionString = strConnStringStatic;
                ASPxComboBox comboBox = (ASPxComboBox)source;
                sqlds.SelectCommand = @"SELECT NEWID() PK, *,R0012 as customText FROM VW0063   ORDER BY R0012";
                sqlds.SelectParameters.Clear();
                sqlds.SelectParameters.Add("ID", TypeCode.Int64, e.Value.ToString());
                comboBox.DataSource = sqlds;
                comboBox.DataBind();
            }

        }

    class ComboTemplate : ITemplate
    {
        public void InstantiateIn(Control container)
        {
            TextBox txt = new TextBox();
            GridViewDataItemTemplateContainer gridContainer = (GridViewDataItemTemplateContainer)container;
            txt.Text = "aaa";
            container.Controls.Add(txt);
        }
    }

}
