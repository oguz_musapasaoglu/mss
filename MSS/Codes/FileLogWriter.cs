﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MSS1.Codes
{
    public class FileLogWriter : ILogWriter
    {
        public void Error(string message)
        {
            throw new NotImplementedException();
        }

        public void Info(string message)
        {
            //TODO: Dosya Adı olarak Web Config'e taşınacak !
            string path = HttpContext.Current.Server.MapPath("~/App_Data/" + DateTime.Now.ToString("ddMMyyy") + "_Info.txt");
            if (string.IsNullOrEmpty(path))
            {
                return;
            }
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(path, true))
            {
                message = string.Format("{0}", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss:ffff"))+"_"+message;
                writer.WriteLine(message);
                writer.Close();
            }

        }
    }
}