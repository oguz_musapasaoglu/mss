﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace MSS1.Codes
{
    public class TabSessions
    {
        public static List<SessionBase> _SessionBase = new List<SessionBase>();

        public static SessionBase Get(string key)
        {
            return _SessionBase.Where(d => d.GuidKey == key).ToArray()[0];

        }

        public static void Set(string key, string Comp, string UserName, string NameSurName, string SelectedLanguage, string SelectedLangVal)
        {
            SessionBase _base = new SessionBase();
            _base.GuidKey = key;
            _base.UserName = UserName;
            _base.Comp = Comp;
            _base.NameSurName = NameSurName;
            _base.SelectedLanguage = SelectedLanguage;
            _base.SelectedLangVal = SelectedLangVal;
            _base.DynMenu = null;
            _base.NewPage = 1;
            if (_SessionBase.Contains(_base))
                _SessionBase.Remove(_base);
            _SessionBase.Add(_base);

        }

        public static SessionBase ControlState(Page _page, string key)
        {

            if (HttpContext.Current.Session["uyedurum"] == null)
                _page.Response.Redirect("~/Default.aspx");

            SessionBase _obj = _SessionBase.Where(d => d.GuidKey == key).ToArray()[0];

            if (_obj == null)
                _page.Response.Redirect("~/Default.aspx");
            else
            {

                _obj = TabSessions.Get(key);


            }

            return _obj;


        }

        public static SessionBase ControlState(string key)
        {

            SessionBase _obj = _SessionBase.Where(d => d.GuidKey == key).ToArray()[0];

            return _obj;


        }



    }

    public class SessionBase
    {
        public string Comp { get; set; }

        public string UserName { get; set; }

        public string GuidKey { get; set; }

        public string NameSurName { get; set; }

        public string SelectedLanguage { get; set; }

        public string SelectedLangVal { get; set; }

        public int NewPage { get; set; }

        public DataTable DynMenu { get; set; }


    }
}