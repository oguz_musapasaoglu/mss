﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for Methods
/// </summary>
public class Methods
{
    public Methods()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string BinligeCevir(string sayi)
    {
        if (sayi != "")
            return Convert.ToDecimal(sayi).ToString("N").Replace(".", ";").Replace(",", ".").Replace(";", ",");
        else
            return "";

    }

    public string NormaleCevir(string sayi)
    {
        return sayi.Replace(".", "").Replace(",", ".");
    }



    public bool SessionControl(SessionParameter session)
    {
        if (session == null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public string ConverttoDate(string date)
    {
        string editingDate;
        if (date != string.Empty)
        {
            string[] tarihDizi = date.Replace("/", ".").Replace(" ", ".").Split('.');
            editingDate = tarihDizi[1] + "." + tarihDizi[0] + "." + tarihDizi[2];
        }
        else
        {
            editingDate = "";
        }
        return editingDate;
    }

    public string StringToDate(string date)
    {
        string editingDate;
        if (date != string.Empty)
        {
            editingDate = date;
        }
        else
        {
            editingDate = "";
        }
        return editingDate;
    }

    public string isnullorEmpty(string text)
    {
        string _result = string.Empty;
        try
        {
            if (string.IsNullOrEmpty(text))
                _result = "";
            else
                _result = text;

        }
        catch (Exception)
        {
            _result = "";
        }
        return _result;
    }
    public string DateToStringNAVType(string date)
    {
        try
        {
            string[] editdate;
            string _date = string.Empty;
            if (!string.IsNullOrEmpty(date))
            {
                editdate = string.Format("{0:dd.MM.yyyy}", date.Replace("/", ".").Replace("-", ".")).Split('.');
                _date = editdate[1] + "." + editdate[0] + "." + editdate[2].Replace(" 00:00:00", "");
            }
            else
                _date = "01.01.1753";
            return _date;
        }
        catch (Exception)
        {
            return "";
        }

    }


    public string DateToStringGlobalType(string date)
    {
        try
        {
            string[] editdate;
            if (date != string.Empty)
            {
                editdate = string.Format("{0:dd.MM.yyyy}", date).Split('.');
                return editdate[0] + "." + editdate[1] + "." + editdate[2].Replace(" 00:00:00", "");
            }
            else
            {
                return date;
            }
        }
        catch (Exception)
        {
            return "";
        }

    }
    public int StringToInt(string value)
    {
        try
        {
            if (value.Contains("."))
                return Convert.ToInt32(value.Substring(0, value.LastIndexOf('.')));
            else
                return Convert.ToInt32(value == "" ? 0 : Convert.ToInt32(value));
        }
        catch (Exception)
        {
            return 0;
        }

    }
    public decimal StringToDecimal(string value)
    {
        decimal sayi = value == "" ? 0 : Convert.ToDecimal(value);
        return sayi;
    }

    public decimal DoubleToInt(double value)
    {
        int sayi = value == 0 ? 0 : Convert.ToInt32(value);
        return sayi;
    }

    public string TimeText(string _val)
    {
        string res = string.Empty;
        res = _val.Replace("01.01.0100 ", "").Replace("01.01.1753 ", "").Replace("01.01.1754 ", "");
        return res;
    }

    public void WriteLog(string ex)
    {
        string message = string.Format("Time: {0}", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
        message += Environment.NewLine;
        message += "-----------------------------------------------------------";
        message += Environment.NewLine;
        message += string.Format("Message: {0}", ex);
        message += Environment.NewLine;
        message += "-----------------------------------------------------------";
        message += Environment.NewLine;
        string path = HttpContext.Current.Server.MapPath("~/App_Data/Log.txt");
        using (System.IO.StreamWriter writer = new System.IO.StreamWriter(path, true))
        {
            writer.WriteLine(message);
            writer.Close();
        }
    }
    public SqlConnection returnConnection()
    {
        SqlConnection conn;
        string ConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        conn = new SqlConnection(ConnString);
        return conn;
    }
}