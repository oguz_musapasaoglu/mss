﻿using DevExpress.Data;
using DevExpress.Web;
using DevExpress.Web.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Collections;
using System.Text;
using DevExpress.Utils;
using MSS1.Codes;

public class webObjectHandler
{
    Methods m = new Methods();
    ServiceUtilities services = new ServiceUtilities();
    ASPxGridView grid;
    string user, sirket, pageId, objectId, insertParams, editParams, deleteParams;
    public int screenWidth, screenHeight;
    Hashtable copiedValues = null;
    string[] copiedFields = new string[150];
    int copiedSayac = 0;
    int columnindex = 1;

    public static string CurrentFolderName(string _comp)
    {
        try
        {
            if (_comp == "GAC" | _comp == "LAM")
                return "GACLAM";
            else
                return _comp;

        }
        catch (Exception)
        {
            return "";

        }

    }

    public webObjectHandler()
    {
    }

    public DataTable webObjectHandlerExtracted(ASPxGridView gridview, ASPxMenu _menu, TextBox _insertParams, TextBox _editParams
        , TextBox _deleteParams, string username, string sirketadi, string _pageId, string _objectId, string _screenWidth,
        string _screenHeight, int ObjectNumberPerPage)
    {
        columnindex = 1;
        gridview.Columns.Clear();
        _menu.Items.Clear();
        grid = gridview;
        user = username;
        sirket = sirketadi;
        pageId = _pageId;
        objectId = _objectId;


        screenWidth = m.StringToInt(_screenWidth);
        screenHeight = m.StringToInt(_screenHeight);
        fillDatatable();
        preparegridHeader(_menu, gridview.ClientInstanceName);
        preparegridGroup();
        preparegridCaption();
        preparegridSearch();
        prepareGridBody(ObjectNumberPerPage);
        preparegridFooter();
        PrepareMainGridOptions(grid);
        _insertParams.Text = insertParams;
        _editParams.Text = editParams;
        _deleteParams.Text = deleteParams;
        gridview = grid;

        //if (dtHeader.Rows[0]["SOURCE VIEW"].ToString().Substring(0,2).ToUpper() == "SP")
        //    newData = fillGridonSP();
        //else 
        return fillGrid();

    }



    public DataTable webObjectHandlerExtractedSub(ASPxGridView gridview, ASPxMenu _menu, TextBox _insertParams, TextBox _editParams
    , TextBox _deleteParams, string username, string sirketadi, string _pageId, string _objectId, string _screenWidth,
    string _screenHeight, int ObjectNumberPerPage)
    {
        columnindex = 1;
        gridview.Columns.Clear();
        _menu.Items.Clear();
        grid = gridview;
        user = username;
        sirket = sirketadi;
        pageId = _pageId;
        objectId = _objectId;


        screenWidth = m.StringToInt(_screenWidth);
        screenHeight = m.StringToInt(_screenHeight);
        fillDatatable();
        preparegridHeader(_menu, gridview.ClientInstanceName);
        preparegridGroup();
        preparegridCaption();
        preparegridSearch();
        prepareGridBody(ObjectNumberPerPage);
        preparegridFooter();
        PrepareMainGridOptions(grid);
        _insertParams.Text = insertParams;
        _editParams.Text = editParams;
        _deleteParams.Text = deleteParams;

        gridview = grid;
        //if (dtHeader.Rows[0]["SOURCE VIEW"].ToString().Substring(0,2).ToUpper() == "SP")
        //    newData = fillGridonSP();
        //else 
        return new DataTable();// fillEmptySubGrid();

    }

    string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
    fonksiyonlar f = new fonksiyonlar();
    DataTable dtHeader = new DataTable();
    DataTable dtGroup = new DataTable();
    DataTable dtCaption = new DataTable();
    DataTable dtSearch = new DataTable();
    DataTable dtRows = new DataTable();
    DataTable dtFooter = new DataTable();

    private bool gridHeaderDisplay
    {
        get
        {
            bool _result;
            if (dtHeader.Select("[DX Field ID]='H0000'")[0]["DX Display Order"].ToString() == "0")
            {
                _result = false;
            }
            else
            {
                _result = true;
            }

            return _result;
        }
    }
    private bool gridGroupingDisplay
    {
        get
        {
            bool _result;
            if (dtGroup.Select("[DX Field ID]='G0000'")[0]["DX Display Order"].ToString() == "0")
            {
                _result = false;
            }
            else
            {
                _result = true;
            }

            return _result;
        }
    }
    private bool gridCaptionDisplay
    {
        get
        {
            bool _result;
            if (dtCaption.Select("[DX Field ID]='C0000'")[0]["DX Display Order"].ToString() == "0")
            {
                _result = false;
            }
            else
            {
                _result = true;
            }

            return _result;
        }
    }
    private bool gridSearchDisplay
    {
        get
        {
            bool _result;
            if (dtSearch.Select("[DX Field ID]='S0000'")[0]["DX Display Order"].ToString() == "0")
            {
                _result = false;
            }
            else
            {
                _result = true;
            }

            return _result;
        }
    }
    private bool gridFooterDisplay
    {
        get
        {
            bool _result;
            if (dtFooter.Select("[DX Field ID]='F0000'")[0]["DX Display Order"].ToString() == "0")
            {
                _result = false;
            }
            else
            {
                _result = true;
            }

            return _result;
        }
    }

    private DataTable fillGrid()
    {


        using (DataTable dt = new DataTable())
        {
            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "[sp_0C_50057_00_WEBPAGES]";
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 1000;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@PageId", pageId);
                    cmd.Parameters.AddWithValue("@ObjectId", objectId);
                    cmd.Parameters.AddWithValue("@Sirket", sirket);
                    cmd.Parameters.AddWithValue("@UserName", user);

                    using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                    {
                        adp.Fill(dt);
                    }
                }
            }
            return dt;
        }
    }

    private DataTable fillEmptySubGrid()
    {
        using (DataTable dt = new DataTable())
        {
            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "[sp_0C_50057_00_WEBPAGES_01]";
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 1000;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@PageId", pageId);
                    cmd.Parameters.AddWithValue("@ObjectId", objectId);
                    cmd.Parameters.AddWithValue("@Sirket", sirket);
                    cmd.Parameters.AddWithValue("@UserName", user);

                    using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                    {
                        adp.Fill(dt);
                    }
                }
            }
            return dt;
        }
    }


    private DataTable fillGridonSP()
    {
        string[] splitsource = dtHeader.Rows[0]["SOURCE VIEW"].ToString().Split(';');

        using (DataTable dt = new DataTable())
        {
            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = splitsource[0].ToString(); ;
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 1000;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    switch (splitsource.Length)
                    {
                        case 2:
                            //    cmd.Parameters.AddWithValue("@PRMT1", spFilter);
                            break;
                        case 3:
                            cmd.Parameters.AddWithValue("@PRMT1", splitsource[1].ToString());
                            cmd.Parameters.AddWithValue("@PRMT2", splitsource[2].ToString());
                            break;
                        case 4:
                            cmd.Parameters.AddWithValue("@PRMT1", splitsource[1].ToString());
                            cmd.Parameters.AddWithValue("@PRMT2", splitsource[2].ToString());
                            cmd.Parameters.AddWithValue("@PRMT3", splitsource[3].ToString());
                            break;
                    }


                    using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                    {
                        adp.Fill(dt);
                    }
                }
            }

            return dt;
        }
    }

    private static DataSet GetObjectSource(string pageId, string objectId, string sirket)
    {
        using (DataSet ds = new DataSet())
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "[sp_0C_50032_00_WEB PAGE CAPTIONS]";
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@Sirket", sirket);
                        cmd.Parameters.AddWithValue("@PageId", pageId);
                        cmd.Parameters.AddWithValue("@ObjectId", objectId);
                        using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                        {
                            adapter.Fill(ds);
                        }

                    }
                }
            }
            catch
            {

            }
            return ds;
        }
    }



    private void fillDatatable()
    {

        string _cachekey = "sp_0C_50032_00_WEB PAGE CAPTIONS" + "_" + pageId + "_" + objectId + "_" + sirket;
        List<CacheObject> _obj = CacheManager.Get(_cachekey, DateTime.Now);
        if (_obj.Count == 0 | CacheManager.CacheActive == 0)
        {
            using (DataSet ds = GetObjectSource(pageId, objectId, sirket))
            {

                CacheManager.SetDynamic(ds, _cachekey, 24, 1);
                _obj = CacheManager.Get(_cachekey, DateTime.Now);
            }
        }

        dtHeader = new DataTable();
        dtHeader = _obj[0].objds.Tables[0].Copy();

        //GROUP
        dtGroup = new DataTable();
        dtGroup = _obj[0].objds.Tables[1].Copy();

        //CAPTION
        dtCaption = new DataTable();
        dtCaption = _obj[0].objds.Tables[2].Copy();

        //SEARCH
        dtSearch = new DataTable();
        dtSearch = _obj[0].objds.Tables[3].Copy();

        //ROWS
        dtRows = new DataTable();
        dtRows = _obj[0].objds.Tables[4].Copy();

        //FOOTER
        dtFooter = new DataTable();
        dtFooter = _obj[0].objds.Tables[5].Copy();
    }

    private void preparegridHeader(ASPxMenu menu, string gridId)
    {
        DataTable _dtTemp = dtHeader.Copy();
        if (_dtTemp.Select("[DX Field ID]<>'H0000' and [DX Display Order]>0").Count() > 0)
        {
            if (gridHeaderDisplay)
            {
                _dtTemp = _dtTemp.Select("[DX Field ID]<>'H0000' and [DX Display Order]>0").CopyToDataTable();
                _dtTemp.DefaultView.Sort = "[DX Display Order]";
                _dtTemp = _dtTemp.DefaultView.ToTable();
                foreach (DataRow row in _dtTemp.Rows)
                {
                    menu.Items.Add(createMenuItem(row["DX Type1"].ToString(), row["DX Type2"].ToString(), row["TR"].ToString(), row["DX Field ID"].ToString(), row["DX Source"].ToString(), gridId));
                }
            }
        }


        int headerWidth = Convert.ToInt32(dtHeader.Select("[DX Field ID]='H0000'")[0]["DX Width"].ToString());



        // grid.Width = Unit.Percentage(headerWidth >100 ? 100 : headerWidth);
        grid.Width = Unit.Percentage(headerWidth);


    }

    private void PrepareMainGridOptions(ASPxGridView grid)
    {
        int scroltype = Convert.ToInt32(dtHeader.Select("[DX Field ID]='H0000'")[0]["SCROLL TYPE"].ToString());
        int rowcount = Convert.ToInt32(dtHeader.Select("[DX Field ID]='H0000'")[0]["ROW COUNT"].ToString());
        int height = Convert.ToInt32(dtHeader.Select("[DX Field ID]='H0000'")[0]["Height"].ToString());
        string sumfield = dtHeader.Select("[DX Field ID]='H0000'")[0]["SUM FIELD"].ToString();
        int IsMenu = Convert.ToInt32(dtHeader.Select("[DX Field ID]='H0000'")[0]["Is Menu"].ToString());
        int IsFroozen = Convert.ToInt32(dtHeader.Select("[DX Field ID]='H0000'")[0]["Froze Column Order"].ToString());
        grid.SettingsDataSecurity.AllowReadUnlistedFieldsFromClientApi = DefaultBoolean.True;

        if (scroltype == 0)
        {

            grid.Settings.VerticalScrollBarMode = ScrollBarMode.Visible;
            grid.Settings.VerticalScrollableHeight = height == 0 ? 350 : height;
            grid.Settings.VerticalScrollableHeight = Convert.ToInt32((double)screenHeight * ((double)height / 100));
            grid.SettingsPager.Mode = GridViewPagerMode.EndlessPaging;
            grid.SettingsPager.PageSize = rowcount == 0 ? 20 : rowcount;
            grid.SettingsPager.ShowSeparators = false;

        }
        else if (scroltype == 2)
        {
            grid.SettingsPager.Mode = GridViewPagerMode.ShowPager;
            grid.SettingsPager.PageSize = rowcount == 0 ? 20 : rowcount;
        }
        if (IsMenu > 0)
        {
            //grid.EnableTheming = false;
            grid.Styles.Cell.Font.Bold = true;
            grid.Styles.Cell.Font.Size = FontUnit.Point(10);

        }


        grid.Styles.Header.Wrap = DevExpress.Utils.DefaultBoolean.True;
        grid.Settings.HorizontalScrollBarMode = ScrollBarMode.Auto;
        grid.Width = Unit.Percentage(100);

        string _filter = DefaultSearchGrid(dtRows.Select("[DX Default Search]<>'' AND [DX Display Order]>0"));
        if (!string.IsNullOrEmpty(_filter)) grid.FilterExpression = _filter;

        try
        {
            DataRow[] rows = dtRows.Select("[DX Default OrderBy Type]<>'' AND [DX Default OrderBy Sequency]>0", "[DX Default OrderBy Sequency] ASC");
            foreach (DataRow row in rows)
            {

                grid.SortBy(grid.Columns[row["DX Field ID"].ToString()],
                    row["DX Default OrderBy Type"].ToString() == "ASC" ? DevExpress.Data.ColumnSortOrder.Ascending : DevExpress.Data.ColumnSortOrder.Descending);
            }
        }
        catch
        {


        }

        // grid.HtmlCommandCellPrepared += Grid_HtmlCommandCellPrepared;
        grid.SettingsCookies.Enabled = true;
        grid.SettingsCookies.Version = "V20171021";
        grid.SettingsCookies.CookiesID = pageId + "_" + objectId;
        grid.SettingsCookies.StoreSearchPanelFiltering = false;
        grid.SettingsCookies.StoreColumnsHierarchy = false;
        grid.SettingsCookies.StoreControlWidth = false;
        grid.SettingsCookies.StorePaging = false;
        grid.SettingsCookies.StoreFiltering = false;
        grid.SettingsCookies.StoreGroupingAndSorting = true;
        grid.SettingsCookies.StoreColumnsVisiblePosition = false;
        grid.SettingsCookies.StoreColumnsWidth = false;

        if (IsFroozen > 0)
        {
            grid.SettingsBehavior.AllowDragDrop = false;

        }


        // grid.HtmlDataCellPrepared += Grid_HtmlDataCellPrepared;
        //grid.CommandButtonInitialize += Grid_CommandButtonInitialize;
        //if (grid.Settings.ShowGroupPanel)
        //{
        //    if (!string.IsNullOrEmpty(sumfield))
        //    {

        //        ASPxSummaryItem totalSummary = new ASPxSummaryItem();
        //        totalSummary.FieldName = sumfield;
        //        totalSummary.ShowInColumn = sumfield;
        //        totalSummary.DisplayFormat = "N0";
        //        totalSummary.SummaryType = SummaryItemType.Sum;
        //        grid.TotalSummary.Add(totalSummary);

        //    }
        //}
    }



    private void preparegridGroup()
    {
        grid.Settings.ShowGroupPanel = gridGroupingDisplay;
    }

    private void preparegridCaption()
    {
        grid.Settings.ShowColumnHeaders = gridCaptionDisplay;
    }
    private void preparegridSearch()
    {
        grid.Settings.ShowFilterRow = gridSearchDisplay;
    }

    private void prepareGridBody(int ObjectNumberPerPage)
    {
        bool _select = false, _edit = false, _delete = false, _deleteHeader = false, _insert = false, _clone = false, _clear = false, _insertHeader = false;
        int sayac = 1;

        for (int i = 0; i < dtRows.Rows.Count; i++)
        {
            switch (dtRows.Rows[i]["DX Type1"].ToString())
            {
                case "DATA":
                    if (dtRows.Rows[i]["DX Type2"].ToString() == "INTEGER" || dtRows.Rows[i]["DX Type2"].ToString() == "DECIMAL")
                    {
                        grid.Columns.Add(creategridviewTextColumn(dtRows.Rows[i]["DX Field ID"].ToString(),
                                        dtRows.Rows[i]["TR"].ToString(), Convert.ToInt32(dtRows.Rows[i]["DX Width"].ToString()),
                                        Convert.ToInt32(dtRows.Rows[i]["Aligned"].ToString()), dtRows.Rows[i]["DX Background Color"].ToString(),
                                        dtRows.Rows[i]["DX Equation"].ToString(), dtRows.Rows[i]["DX Equation Result"].ToString(), dtRows.Rows[i]["DX Display Format"].ToString()
                                        ));
                        columnindex++;
                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "OPTION")
                    {
                        grid.Columns.Add(creategridviewOptionColumn(dtRows.Rows[i]["DX Field ID"].ToString(), dtRows.Rows[i]["TR"].ToString(), Convert.ToInt32(dtRows.Rows[i]["DX Width"].ToString()), Convert.ToInt32(dtRows.Rows[i]["Aligned"].ToString()), dtRows.Rows[i]["DX Background Color"].ToString(),
                                       dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(), dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), sayac));
                        columnindex++;
                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "DATE")
                    {
                        grid.Columns.Add(creategridviewDateColumn(dtRows.Rows[i]["DX Field ID"].ToString(),
                                      dtRows.Rows[i]["TR"].ToString(), Convert.ToInt32(dtRows.Rows[i]["DX Width"].ToString()),
                                      Convert.ToInt32(dtRows.Rows[i]["Aligned"].ToString()), dtRows.Rows[i]["DX Background Color"].ToString(),
                                      dtRows.Rows[i]["DX Display Format"].ToString()
                                      ));
                        columnindex++;
                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "TIME")
                    {
                        grid.Columns.Add(creategridviewTimeColumn(dtRows.Rows[i]["DX Field ID"].ToString(),
                                      dtRows.Rows[i]["TR"].ToString(), Convert.ToInt32(dtRows.Rows[i]["DX Width"].ToString()),
                                      Convert.ToInt32(dtRows.Rows[i]["Aligned"].ToString()), dtRows.Rows[i]["DX Background Color"].ToString(),
                                      dtRows.Rows[i]["DX Display Format"].ToString()));
                        columnindex++;
                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "BOOLEAN")
                    {
                        grid.Columns.Add(creategridviewCheckColumn(dtRows.Rows[i]["DX Field ID"].ToString(),
                                      dtRows.Rows[i]["TR"].ToString(), Convert.ToInt32(dtRows.Rows[i]["DX Width"].ToString()),
                                      Convert.ToInt32(dtRows.Rows[i]["Aligned"].ToString()), dtRows.Rows[i]["DX Background Color"].ToString()));
                        columnindex++;
                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "PDF")
                    {
                        grid.Columns.Add(creategridviewDataHyperLink(dtRows.Rows[i]["Dx Field ID"].ToString(), dtRows.Rows[i]["TR"].ToString(), dtRows.Rows[i]["DX Filter"].ToString()));
                        columnindex++;
                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "COMBO")
                    {
                        grid.Columns.Add(creategridviewComboboboxColumn(dtRows.Rows[i]["DX Field ID"].ToString(), dtRows.Rows[i]["TR"].ToString(), Convert.ToInt32(dtRows.Rows[i]["DX Width"].ToString()), Convert.ToInt32(dtRows.Rows[i]["Aligned"].ToString()), dtRows.Rows[i]["DX Background Color"].ToString(),
                             dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(), dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(),
                             sayac, ObjectNumberPerPage, dtRows.Rows[i]["DX Equation"].ToString(), dtRows.Rows[i]["DX Equation Result"].ToString()
                             ));
                        columnindex++;
                        sayac++;
                    }
                    else
                    {
                        grid.Columns.Add(creategridviewColumn(dtRows.Rows[i]["DX Field ID"].ToString(),
                            dtRows.Rows[i]["TR"].ToString(), Convert.ToInt32(dtRows.Rows[i]["DX Width"].ToString()),
                            Convert.ToInt32(dtRows.Rows[i]["Aligned"].ToString()), dtRows.Rows[i]["DX Background Color"].ToString()));
                        columnindex++;
                    }

                    if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Grouping Desc Type"].ToString()))
                    {
                        ASPxSummaryItem item = new ASPxSummaryItem();
                        item.FieldName = dtRows.Rows[i]["DX Field ID"].ToString();
                        //int start = Convert.ToInt32(dtRows.Rows[i]["DX Grouping Desc Start"])
                        //    , end = Convert.ToInt32(dtRows.Rows[i]["DX Grouping Desc Finish"]),
                        //    total = end - start;

                        //total = Math.Abs(total);
                        item.DisplayFormat = dtRows.Rows[i]["DX Grouping Desc Text"].ToString() + dtRows.Rows[i]["DX Grouping Desc Number Format"].ToString();
                        //item.DisplayFormat = "<div style=\"position:relative;right:" + start.ToString() + "px;float:right;width:" + total.ToString() + "px;white-space: nowrap;display: inline-block;\">"
                        //     + "<table style=\"width: 100%;table-layout: fixed;\"><tr><td style=\"text-align:left;width:60%table-layout: fixed;\">"
                        //     + dtRows.Rows[i]["DX Grouping Desc Text"].ToString() + "</td><td style=\"text-align:right;width:40%;font-weight: bold;table-layout: fixed;\">"
                        //     + dtRows.Rows[i]["DX Grouping Desc Number Format"].ToString() + "</td></tr></table></div>";
                        switch (dtRows.Rows[i]["DX Grouping Desc Type"].ToString())
                        {
                            case "Avarage":
                                item.SummaryType = SummaryItemType.Average;
                                break;
                            case "Count":
                                item.SummaryType = SummaryItemType.Count;
                                break;
                            case "Max":
                                item.SummaryType = SummaryItemType.Max;
                                break;
                            case "Min":
                                item.SummaryType = SummaryItemType.Min;
                                break;
                            case "Sum":
                                item.SummaryType = SummaryItemType.Sum;
                                break;
                        }

                        grid.GroupSummary.Add(item);
                    }
                    break;
                case "BUTTON":
                    if (dtRows.Rows[i]["DX Type2"].ToString() == "SELECT")
                    {
                        _select = true;


                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "EDIT")
                    {
                        if (dtRows.Rows[i]["DX Permision Field"].ToString() != "")
                        {
                            _edit = true;// f.DynamicPageYetkiKontrol(sirket, user, dtRows.Rows[i]["DX Permision Field"].ToString());
                        }
                        else
                        {
                            _edit = true;
                        }
                        editParams = dtRows.Rows[i]["DX Source"].ToString() + "," + dtRows.Rows[i]["DX Filter"].ToString();
                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "DELETE")
                    {
                        if (dtRows.Rows[i]["DX Permision Field"].ToString() != "")
                        {
                            _delete = true;// f.DynamicPageYetkiKontrol(sirket, user, dtRows.Rows[i]["DX Permision Field"].ToString());
                        }
                        else
                        {
                            _delete = true;
                        }
                        deleteParams = dtRows.Rows[i]["DX Source"].ToString() + "," + dtRows.Rows[i]["DX Filter"].ToString();
                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "INSERT")
                    {
                        if (dtRows.Rows[i]["DX Permision Field"].ToString() != "")
                        {
                            _insert = true;//f.DynamicPageYetkiKontrol(sirket, user, dtRows.Rows[i]["DX Permision Field"].ToString());
                        }
                        else
                        {
                            _insert = true;
                        }
                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "CLONE")
                    {
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Permision Field"].ToString()))
                        {
                            _clone = true;//f.DynamicPageYetkiKontrol(sirket, user, dtRows.Rows[i]["DX Permision Field"].ToString());
                        }
                        else
                        {
                            _clone = true;
                        }

                    }
                    else if (dtRows.Rows[i]["DX Type2"].ToString() == "CLEAR")
                    {
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Permision Field"].ToString()))
                        {
                            _clear = true;//f.DynamicPageYetkiKontrol(sirket, user, dtRows.Rows[i]["DX Permision Field"].ToString());
                        }
                        else
                        {
                            _clear = true;
                        }

                    }
                    break;
            }

        }

        if (dtCaption.Select("[DX Type1] = 'BUTTON' and [DX Type2] = 'INSERT'  and [DX Display Order]>0").Length > 0)
        {
            if (dtCaption.Select("[DX Type1] = 'BUTTON' and [DX Type2] = 'INSERT'  and [DX Display Order]>0")[0]["DX Permision Field"].ToString() != "")
            {
                _insertHeader = true;//f.DynamicPageYetkiKontrol(sirket, user, dtCaption.Select("[DX Type1] = 'BUTTON' and [DX Type2] = 'INSERT'  and [DX Display Order]>0")[0]["DX Permision Field"].ToString());
            }
            else
            {
                _insertHeader = true;
            }
            insertParams = dtCaption.Select("[DX Type1] = 'BUTTON' and [DX Type2] = 'INSERT' and [DX Display Order]>0")[0]["DX Source"].ToString() + "," + dtCaption.Select("[DX Type1] = 'BUTTON' and [DX Type2] = 'INSERT'")[0]["DX Filter"].ToString();
        }
        else
        {
            _insertHeader = false;

        }

        if (dtCaption.Select("[DX Type1] = 'BUTTON' and [DX Type2] = 'DELETE'  and [DX Display Order]>0").Length > 0)
        {
            if (dtCaption.Select("[DX Type1] = 'BUTTON' and [DX Type2] = 'DELETE'  and [DX Display Order]>0")[0]["DX Permision Field"].ToString() != "")
            {
                _deleteHeader = true;//f.DynamicPageYetkiKontrol(sirket, user, dtCaption.Select("[DX Type1] = 'BUTTON' and [DX Type2] = 'DELETE'  and [DX Display Order]>0")[0]["DX Permision Field"].ToString());
            }
            else
            {
                _deleteHeader = true;
            }
            deleteParams = dtCaption.Select("[DX Type1] = 'BUTTON' and [DX Type2] = 'DELETE'  and [DX Display Order]>0")[0]["DX Source"].ToString() + "," + dtCaption.Select("[DX Type1] = 'BUTTON' and [DX Type2] = 'DELETE'")[0]["DX Filter"].ToString();
        }
        else
        {
            _deleteHeader = false;

        }

        bool _isfiltericon = false;

        try { _isfiltericon = Convert.ToInt32(dtRows.Select("[DX Field ID]='R0000'")[0]["DX Display Order"]) == 1 ? true : false; } catch { }


        if (_select | _edit | _delete | _insert | _clone | _clear | _isfiltericon)
        {
            leftmenuSettings(grid, _select, _edit, _delete, _insert, _insertHeader, _deleteHeader, _clone, _clear);
        }
        columnindex++;
        grid.Columns.Add(creategridviewBlankColumn());

    }

    private void preparegridFooter()
    {
        grid.Settings.ShowFooter = gridFooterDisplay;
        for (int i = 0; i < dtFooter.Rows.Count; i++)
        {
            switch (dtFooter.Rows[i]["DX Type1"].ToString())
            {
                case "BUTTON":

                    break;
            }
        }
    }

    private GridViewDataTextColumn creategridviewTextColumn(string fieldID, string caption, int width, int halignment, string bgcolor, string Equation, string equationResult, string displayFormat)
    {
        GridViewDataTextColumn _column = new GridViewDataTextColumn();
        _column.FieldName = fieldID;
        _column.Name = fieldID;
        _column.Caption = caption;
        _column.VisibleIndex = columnindex;
        double resize = (double)screenWidth * ((double)width / 100);
        _column.Width = Unit.Pixel(Convert.ToInt32(resize));


        if (string.IsNullOrEmpty(bgcolor)) bgcolor = "#FFFFFF";
        _column.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);
        _column.CellStyle.Wrap = DevExpress.Utils.DefaultBoolean.False;
        HorizontalAlign _haling = halignment == 0 ? HorizontalAlign.Left :
                                  halignment == 1 ? HorizontalAlign.Center :
                                  halignment == 2 ? HorizontalAlign.Right : HorizontalAlign.NotSet;
        _column.CellStyle.HorizontalAlign = _haling;
        _column.HeaderStyle.HorizontalAlign = _haling;

        if (!string.IsNullOrEmpty(displayFormat))
            _column.PropertiesTextEdit.DisplayFormatString = displayFormat;

        _column.Settings.AutoFilterCondition = AutoFilterCondition.Contains;

        if (Equation != "")
        {
            string[] eqArr = Equation.Split('|');
            string[] eqResultArr = equationResult.Split('|');
            string valueChange = "function(s,e) { ";
            for (int i = 0; i < eqArr.Length; i++)
            {
                valueChange += grid.ClientInstanceName + ".SetEditValue('" + eqResultArr[i] + "',parseFloat(" + eqArr[i].Replace("grid", grid.ClientInstanceName) + ").toFixed(2)); ";
            }
            valueChange += "}";
            _column.PropertiesTextEdit.ClientSideEvents.ValueChanged = valueChange;
        }

        //if (Equation != "")
        //{
        //    string valueChange = "function(s,e) { " + grid.ClientInstanceName + ".SetEditValue('" + equationResult + "'," + Equation.Replace("grid", grid.ClientInstanceName) + "); }";
        //    _column.PropertiesTextEdit.ClientSideEvents.ValueChanged = valueChange;
        //}

        if (fieldID != "REDIT11")
        {
            copiedFields[copiedSayac] = fieldID;
            copiedSayac++;
        }


        try
        {
            DataRow[] rows = dtRows.Select("[DX Caption Filter]>0 AND [DX Field ID]='" + fieldID + "'");
            if (rows.Length > 0)
                _column.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;

            DataRow[] rowsg = dtRows.Select("[DX Field ID]='" + fieldID + "'");
            if (rowsg[0]["DX Insert Hidden"].ToString() == "1")
                _column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

            int dxpopupindex = Convert.ToInt32(rowsg[0]["DX Edit Popup Index"].ToString());

            if (dxpopupindex > -1)
            {
                _column.EditFormSettings.VisibleIndex = dxpopupindex;

            }
        }
        catch
        {


        }
        return _column;
    }

    private GridViewDataColumn creategridviewColumn(string fieldID, string caption, int width, int halignment, string bgcolor)
    {
        GridViewDataColumn _column = new GridViewDataColumn();
        _column.FieldName = fieldID;
        _column.Name = fieldID;
        _column.Caption = caption;
        _column.VisibleIndex = columnindex;
        double resize = (double)screenWidth * ((double)width / 100);
        _column.Width = Unit.Pixel(Convert.ToInt32(resize));
        if (string.IsNullOrEmpty(bgcolor)) bgcolor = "#FFFFFF";
        _column.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);

        HorizontalAlign _haling = halignment == 0 ? HorizontalAlign.Left :
                                  halignment == 1 ? HorizontalAlign.Center :
                                  halignment == 2 ? HorizontalAlign.Right : HorizontalAlign.NotSet;

        _column.CellStyle.HorizontalAlign = _haling;
        _column.HeaderStyle.HorizontalAlign = _haling;
        _column.CellStyle.Wrap = DevExpress.Utils.DefaultBoolean.False;
        _column.Settings.AutoFilterCondition = AutoFilterCondition.Contains;

        if (fieldID != "REDIT11")
        {
            copiedFields[copiedSayac] = fieldID;
            copiedSayac++;
        }


        try
        {
            DataRow[] rows = dtRows.Select("[DX Caption Filter]>0 AND [DX Field ID]='" + fieldID + "'");
            if (rows.Length > 0)
                _column.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;

            DataRow[] rowsg = dtRows.Select("[DX Field ID]='" + fieldID + "'");
            if (rowsg[0]["DX Insert Hidden"].ToString() == "1")
                _column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

            int dxpopupindex = Convert.ToInt32(rowsg[0]["DX Edit Popup Index"].ToString());

            if (dxpopupindex > -1)
            {
                _column.EditFormSettings.VisibleIndex = dxpopupindex;

            }
        }
        catch
        {


        }

        return _column;
    }

    private GridViewDataColumn creategridviewBlankColumn()
    {
        GridViewDataColumn _column = new GridViewDataColumn();
        _column.FieldName = "REDIT";
        _column.Name = "REDIT11";
        _column.Caption = "";
        _column.Width = Unit.Pixel(0);
        _column.VisibleIndex = columnindex;
        _column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;
        return _column;
    }

    private GridViewDataComboBoxColumn creategridviewComboboboxColumn(string fieldID, string caption, int width, int halignment, string bgcolor, string Source
        , string Filter, string Value, string Text, int sayac, int ObjectNumberPerPage, string Equation, string equationResult)
    {
        GridViewDataComboBoxColumn _comboboxColumn = new GridViewDataComboBoxColumn();
        _comboboxColumn.FieldName = fieldID;
        _comboboxColumn.Name = fieldID;
        _comboboxColumn.PropertiesComboBox.ClientInstanceName = fieldID;
        _comboboxColumn.Caption = caption;
        _comboboxColumn.VisibleIndex = columnindex;
        double resize = (double)screenWidth * ((double)width / 100);
        _comboboxColumn.Width = Unit.Pixel(Convert.ToInt32(resize));


        if (string.IsNullOrEmpty(bgcolor)) bgcolor = "#FFFFFF";
        _comboboxColumn.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);

        HorizontalAlign _haling = halignment == 0 ? HorizontalAlign.Left :
                                  halignment == 1 ? HorizontalAlign.Center :
                                  halignment == 2 ? HorizontalAlign.Right : HorizontalAlign.NotSet;

        _comboboxColumn.CellStyle.HorizontalAlign = _haling;
        _comboboxColumn.HeaderStyle.HorizontalAlign = _haling;
        _comboboxColumn.PropertiesComboBox.EnableCallbackMode = true;
        _comboboxColumn.PropertiesComboBox.CallbackPageSize = 10;

        if (Equation != "")
        {
            string[] eqArr = Equation.Split('|');
            string[] eqResultArr = equationResult.Split('|');
            string valueChange = "function(s,e) { ";
            for (int i = 0; i < eqArr.Length; i++)
            {
                valueChange += grid.ClientInstanceName + ".SetEditValue('" + eqResultArr[i] + "',parseFloat(" + eqArr[i].Replace("grid", grid.ClientInstanceName) + ").toFixed(2)); ";
            }
            valueChange += "}";
            _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = valueChange;
        }

        if (objectId == "J50011" | objectId == "J50031")
        {
            if (fieldID == "RTYP1")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {RTYP2.PerformCallback(s.GetValue()); }";
            }
            else if (fieldID == "RTYP2")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {RTYP3.PerformCallback(s.GetValue()); }";
            }
            else if (fieldID == "R0022")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {R0024.PerformCallback(s.GetValue()); }";
            }
            else if (fieldID == "RTYP3")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {RTEMP.PerformCallback(s.GetValue()); }";
            }
            else if (fieldID == "RTEMP")
            {
                string func = "function(s,e){ " + grid.ClientInstanceName + ".SetEditValue('R0015', s.GetText() );}";
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = func;
            }
            else if (fieldID == "R0014")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {R0016.PerformCallback(s.GetValue()); }";
            }
            else if (fieldID == "R0024" | fieldID == "R0048")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) { var param =  new Array('" + fieldID + "' ,s.GetValue().toString());R0046.PerformCallback(param); R0040.PerformCallback(param); R0042.PerformCallback(param); R0044.PerformCallback(param);  }";
            }
            else if (fieldID == "R0046")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) { var param =  new Array('" + fieldID + "' ,s.GetValue().toString()); R0040.PerformCallback(param); R0042.PerformCallback(param); R0044.PerformCallback(param) }";

            }
        }
        else if (objectId == "J50051" | objectId == "J50071" | objectId == "J50111" | objectId == "J50131")
        {
            if (fieldID == "RTYP1")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {RTYP2.PerformCallback(s.GetValue()); }";
            }
            else if (fieldID == "RTYP2")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {RTYP3.PerformCallback(s.GetValue()); }";
            }
            else if (fieldID == "R0031")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {R0044.PerformCallback(s.GetValue()); }";
            }
            else if (fieldID == "RTYP3")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {RTEMP.PerformCallback(s.GetValue()); }";
            }
            else if (fieldID == "RTEMP")
            {
                string func = "function(s,e){ " + grid.ClientInstanceName + ".SetEditValue('R0016', s.GetText() );}";
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = func;
            }
            else if (fieldID == "R0014")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {R0017.PerformCallback(s.GetValue()); }";
            }
            else if (fieldID == "R0044" | fieldID == "R0040")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) { var param =  new Array('" + fieldID + "' ,s.GetValue().toString());R0038.PerformCallback(param); R0043.PerformCallback(param); R0042.PerformCallback(param); R0036.PerformCallback(param);   }";
            }
            else if (fieldID == "R0038")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) { var param =  new Array('" + fieldID + "' ,s.GetValue().toString()); R0043.PerformCallback(param); R0042.PerformCallback(param); R0036.PerformCallback(param);   }";
            }
        }
        else if (objectId == "J50150" | objectId == "J50160" | objectId == "J50170")
        {
            if (fieldID == "RTYP1")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {RTYP2.PerformCallback(s.GetValue()); }";
            }
            else if (fieldID == "RTYP2")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {RTYP3.PerformCallback(s.GetValue()); }";
            }
            else if (fieldID == "R0022")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {R0024.PerformCallback(s.GetValue()); }";
            }
            else if (fieldID == "RTYP3")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {RTEMP.PerformCallback(s.GetValue()); }";
            }
            else if (fieldID == "RTEMP")
            {
                string func = "function(s,e){ " + grid.ClientInstanceName + ".SetEditValue('R0022', s.GetText() );}";
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = func;
            }
            else if (fieldID == "R0014")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {R0016.PerformCallback(s.GetValue()); }";
            }
        }
        else if (objectId == "OJ3056")
        {
            if (fieldID == "RTYP1")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {RTYP2.PerformCallback(s.GetValue()); }";
            }
            else if (fieldID == "RTYP2")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {RTYP3.PerformCallback(s.GetValue()); }";
            }
            else if (fieldID == "R0037")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {R0042.PerformCallback(s.GetValue()); }";
            }
        }
        else if (objectId == "OJ0031")
        {
            if (fieldID == "RTYP1")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {RTYP2.PerformCallback(s.GetValue());}";
            }
            else if (fieldID == "RTYP2")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {RTYP3.PerformCallback(s.GetValue()); }";
            }
        }
        else if (objectId == "OJ5012" | objectId == "OJ5018")
        {
            if (fieldID == "RTYP1")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {RTYP2.PerformCallback(s.GetValue());" +
                    "R0018.PerformCallback(s.GetValue());R0023.PerformCallback(s.GetValue());}";
            }
            else if (fieldID == "RTYP2")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {RTYP3.PerformCallback(s.GetValue()); }";
            }


        }
        else if (objectId == "OJ3009")
        {
            if (fieldID == "R0015")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {R0027.PerformCallback(s.GetValue());" +
                    "R0031.PerformCallback(s.GetValue());R0033.PerformCallback(s.GetValue()); }";
            }
        }
        else if (objectId == "O171300")
        {
            if (fieldID == "R0013")
            {
                _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {R0014.PerformCallback(s.GetValue());}";
            }
        }


        string _ppage = string.Empty;
        if (ObjectNumberPerPage > 10 & sayac < 10)
            _ppage = ObjectNumberPerPage.ToString() + "0";
        else
            _ppage = ObjectNumberPerPage.ToString();

        _comboboxColumn.PropertiesComboBox.DataSourceID = "DS" + _ppage + sayac.ToString();
        _comboboxColumn.PropertiesComboBox.ValueField = Value;
        _comboboxColumn.PropertiesComboBox.TextField = "customText";
        _comboboxColumn.CellStyle.Wrap = DevExpress.Utils.DefaultBoolean.False;

        try
        {
            DataRow[] rows = dtRows.Select("[DX Caption Filter]>0 AND [DX Field ID]='" + fieldID + "'");
            if (rows.Length > 0)
                _comboboxColumn.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;

            DataRow[] rowsg = dtRows.Select("[DX Field ID]='" + fieldID + "'");
            if (rowsg[0]["DX Insert Hidden"].ToString() == "1")
                _comboboxColumn.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

            int dxpopupindex = Convert.ToInt32(rowsg[0]["DX Edit Popup Index"].ToString());

            if (dxpopupindex > -1)
            {
                _comboboxColumn.EditFormSettings.VisibleIndex = dxpopupindex;

            }

        }
        catch
        {


        }

        return _comboboxColumn;
    }

    private GridViewDataComboBoxColumn creategridviewOptionColumn(string fieldID, string caption, int width, int halignment, string bgcolor, string Source,
        string Filter, string Value, string Text, int sayac)
    {
        GridViewDataComboBoxColumn _comboboxColumn = new GridViewDataComboBoxColumn();
        _comboboxColumn.Name = fieldID;
        _comboboxColumn.FieldName = fieldID;
        _comboboxColumn.Caption = caption;
        _comboboxColumn.VisibleIndex = columnindex;
        _comboboxColumn.PropertiesComboBox.ValueType = typeof(int);
        _comboboxColumn.PropertiesComboBox.IncrementalFilteringMode = IncrementalFilteringMode.Contains;

        double resize = (double)screenWidth * ((double)width / 100);
        _comboboxColumn.Width = Unit.Pixel(Convert.ToInt32(resize));


        if (string.IsNullOrEmpty(bgcolor)) bgcolor = "#FFFFFF";
        _comboboxColumn.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);

        HorizontalAlign _haling = halignment == 0 ? HorizontalAlign.Left :
                                  halignment == 1 ? HorizontalAlign.Center :
                                  halignment == 2 ? HorizontalAlign.Right : HorizontalAlign.NotSet;

        _comboboxColumn.CellStyle.HorizontalAlign = _haling;
        _comboboxColumn.HeaderStyle.HorizontalAlign = _haling;

        string[] valueSplitter = Value.Split(',');
        string[] textSplitter = Text.Split(',');


        if (fieldID == "R0037")
        {
            _comboboxColumn.PropertiesComboBox.ClientSideEvents.SelectedIndexChanged = "function(s, e) {R0042.PerformCallback(s.GetValue()); }";
        }

        for (int i = 0; i < textSplitter.Length; i++)
        {

            if (Value.Length > 0)
            {
                _comboboxColumn.PropertiesComboBox.Items.Add(textSplitter[i].ToString(), valueSplitter[i].ToString() == "" ? i : Convert.ToInt32(valueSplitter[i].ToString()));
            }
            else
            {
                _comboboxColumn.PropertiesComboBox.Items.Add(textSplitter[i].ToString(), i);
            }

        }
        copiedFields[copiedSayac] = fieldID;
        copiedSayac++;

        try
        {
            DataRow[] rows = dtRows.Select("[DX Caption Filter]>0 AND [DX Field ID]='" + fieldID + "'");
            if (rows.Length > 0)
                _comboboxColumn.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;

            DataRow[] rowsg = dtRows.Select("[DX Field ID]='" + fieldID + "'");
            if (rowsg[0]["DX Insert Hidden"].ToString() == "1")
                _comboboxColumn.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

            int dxpopupindex = Convert.ToInt32(rowsg[0]["DX Edit Popup Index"].ToString());

            if (dxpopupindex > -1)
            {
                _comboboxColumn.EditFormSettings.VisibleIndex = dxpopupindex;

            }


        }
        catch
        {


        }

        return _comboboxColumn;
    }

    private GridViewDataDateColumn creategridviewDateColumn(string fieldID, string caption, int width, int halignment, string bgcolor, string displayFormat)
    {
        GridViewDataDateColumn _column = new GridViewDataDateColumn();
        _column.FieldName = fieldID;
        _column.Caption = caption;
        _column.VisibleIndex = columnindex;
        double resize = (double)screenWidth * ((double)width / 100);
        _column.Width = Unit.Pixel(Convert.ToInt32(resize));

        if (string.IsNullOrEmpty(bgcolor)) bgcolor = "#FFFFFF";
        _column.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);

        if (!string.IsNullOrEmpty(displayFormat))
        {
            _column.PropertiesEdit.DisplayFormatString = displayFormat;
            _column.PropertiesDateEdit.DisplayFormatString = displayFormat;
        }

        HorizontalAlign _haling = halignment == 0 ? HorizontalAlign.Left :
                                  halignment == 1 ? HorizontalAlign.Center :
                                  halignment == 2 ? HorizontalAlign.Right : HorizontalAlign.NotSet;

        _column.CellStyle.HorizontalAlign = _haling;
        _column.HeaderStyle.HorizontalAlign = _haling;
        //_column.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;
        copiedFields[copiedSayac] = fieldID;
        copiedSayac++;
        try
        {
            DataRow[] rows = dtRows.Select("[DX Caption Filter]>0 AND [DX Field ID]='" + fieldID + "'");
            if (rows.Length > 0)
                _column.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;

            DataRow[] rowsg = dtRows.Select("[DX Field ID]='" + fieldID + "'");
            if (rowsg[0]["DX Insert Hidden"].ToString() == "1")
                _column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

            int dxpopupindex = Convert.ToInt32(rowsg[0]["DX Edit Popup Index"].ToString());

            if (dxpopupindex > -1)
            {
                _column.EditFormSettings.VisibleIndex = dxpopupindex;

            }
        }
        catch
        {


        }
        return _column;
    }

    private GridViewDataTimeEditColumn creategridviewTimeColumn(string fieldID, string caption, int width, int halignment, string bgcolor, string displayFormat)
    {
        GridViewDataTimeEditColumn _column = new GridViewDataTimeEditColumn();
        _column.FieldName = fieldID;
        _column.Caption = caption;
        _column.VisibleIndex = columnindex;
        double resize = (double)screenWidth * ((double)width / 100);
        _column.Width = Unit.Pixel(Convert.ToInt32(resize));

        if (string.IsNullOrEmpty(bgcolor)) bgcolor = "#FFFFFF";
        _column.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);

        if (!string.IsNullOrEmpty(displayFormat))
        {
            _column.PropertiesEdit.DisplayFormatString = displayFormat;
            _column.PropertiesTimeEdit.DisplayFormatString = displayFormat;
        }

        HorizontalAlign _haling = halignment == 0 ? HorizontalAlign.Left :
                                  halignment == 1 ? HorizontalAlign.Center :
                                  halignment == 2 ? HorizontalAlign.Right : HorizontalAlign.NotSet;

        _column.CellStyle.HorizontalAlign = _haling;
        _column.HeaderStyle.HorizontalAlign = _haling;
        _column.PropertiesTimeEdit.DisplayFormatString = "HH:mm";
        _column.PropertiesTimeEdit.EditFormat = EditFormat.Custom;
        copiedFields[copiedSayac] = fieldID;
        copiedSayac++;

        try
        {
            DataRow[] rows = dtRows.Select("[DX Caption Filter]>0 AND [DX Field ID]='" + fieldID + "'");
            if (rows.Length > 0)
                _column.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;

            DataRow[] rowsg = dtRows.Select("[DX Field ID]='" + fieldID + "'");
            if (rowsg[0]["DX Insert Hidden"].ToString() == "1")
                _column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

            int dxpopupindex = Convert.ToInt32(rowsg[0]["DX Edit Popup Index"].ToString());

            if (dxpopupindex > -1)
            {
                _column.EditFormSettings.VisibleIndex = dxpopupindex;

            }

        }
        catch
        {


        }
        return _column;
    }

    private GridViewDataCheckColumn creategridviewCheckColumn(string fieldID, string caption, int width, int halignment, string bgcolor)
    {
        GridViewDataCheckColumn _column = new GridViewDataCheckColumn();
        _column.FieldName = fieldID;
        _column.Caption = caption;
        _column.VisibleIndex = columnindex;
        double resize = (double)screenWidth * ((double)width / 100);
        _column.Width = Unit.Pixel(Convert.ToInt32(resize));

        if (string.IsNullOrEmpty(bgcolor)) bgcolor = "#FFFFFF";
        _column.CellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(bgcolor);

        HorizontalAlign _haling = halignment == 0 ? HorizontalAlign.Left :
                                  halignment == 1 ? HorizontalAlign.Center :
                                  halignment == 2 ? HorizontalAlign.Right : HorizontalAlign.NotSet;

        _column.CellStyle.HorizontalAlign = _haling;
        _column.HeaderStyle.HorizontalAlign = _haling;
        copiedFields[copiedSayac] = fieldID;
        copiedSayac++;

        try
        {
            DataRow[] rows = dtRows.Select("[DX Caption Filter]>0 AND [DX Field ID]='" + fieldID + "'");
            if (rows.Length > 0)
                _column.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.True;

            DataRow[] rowsg = dtRows.Select("[DX Field ID]='" + fieldID + "'");
            if (rowsg[0]["DX Insert Hidden"].ToString() == "1")
                _column.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

            int dxpopupindex = Convert.ToInt32(rowsg[0]["DX Edit Popup Index"].ToString());

            if (dxpopupindex > -1)
            {
                _column.EditFormSettings.VisibleIndex = dxpopupindex;

            }
        }
        catch
        {

        }

        return _column;
    }

    private GridViewDataHyperLinkColumn creategridviewDataHyperLink(string fieldID, string caption, string dxFilter)
    {
        GridViewDataHyperLinkColumn _column = new GridViewDataHyperLinkColumn();
        _column.FieldName = fieldID;
        _column.Caption = caption;
        _column.VisibleIndex = columnindex;
        _column.PropertiesHyperLinkEdit.NavigateUrlFormatString = CurrentFolderName(sirket) + "/" + "{0}";
        _column.PropertiesHyperLinkEdit.ImageUrl = "~/images/pdfico.png";
        _column.PropertiesHyperLinkEdit.Target = "_blank";

        switch (dxFilter)
        {
            case "NOCOMP":
                _column.PropertiesHyperLinkEdit.NavigateUrlFormatString = "{0}";
                break;
            case "NOCOMPPOPUP":
                _column.PropertiesHyperLinkEdit.Target = string.Empty;
                _column.PropertiesHyperLinkEdit.NavigateUrlFormatString = "javascript:popupWindow('{0}','PDF',window,900,650);";
                break;
            case "POPUP":
                _column.PropertiesHyperLinkEdit.Target = string.Empty;
                _column.PropertiesHyperLinkEdit.NavigateUrlFormatString = "javascript:popupWindow('" + CurrentFolderName(sirket) + "/{0}','PDF',window,900,650);";
                break;
            default:
                _column.PropertiesHyperLinkEdit.NavigateUrlFormatString = CurrentFolderName(sirket) + "/" + "{0}";
                break;
        }

        return _column;
    }
    

    public static string FillCombobox(string Source, string Filter, string value, string text, string sirket, string user)
    {
        string[] _array = text.Split(',');
        string customText = string.Empty;
        if (_array.Length > 1)
        {
            customText = _array[0].ToString() + "+' - '+" + _array[1].ToString();
        }
        else
        {
            customText = text;
        }
        string query = "select *," + customText + " as customText from [" + Source + "] where 1=1";
        string[] filters = Filter.Split(',');

        for (int i = 0; i < filters.Length; i++)
        {
            switch (filters[i].ToString())
            {
                case "RCOMP":
                    query = query + " and RCOMP='" + sirket + "' ";
                    break;
                case "RUSER":
                    query = query + " and RUSER='" + user + "'";
                    break;
            }
        }

        return query;
    }
    private string FillCombobox(string Source, string Filter, string value, string text)
    {
        string[] _array = text.Split(',');
        string customText = string.Empty;
        if (_array.Length > 1)
        {
            customText = _array[0].ToString() + "+' - '+" + _array[1].ToString();
        }
        else
        {
            customText = text;
        }
        string query = "select *," + customText + " as customText from [" + Source + "] where 1=1";
        string[] filters = Filter.Split(',');

        for (int i = 0; i < filters.Length; i++)
        {
            switch (filters[i].ToString())
            {
                case "RCOMP":
                    query = query + " and RCOMP='" + sirket + "' ";
                    break;
                case "RUSER":
                    query = query + " and RUSER='" + user + "'";
                    break;
            }
        }

        //query = query.Substring(0, query.Length - 3);
        return query;
    }

    private DevExpress.Web.MenuItem createMenuItem(string type, string Type2, string text, string fieldID, string source, string gridId)
    {
        DevExpress.Web.MenuItem _menu = new DevExpress.Web.MenuItem();
        switch (type)
        {
            case "LABEL":
                _menu.Text = text;
                _menu.Enabled = false;
                break;
            case "BUTTON":
                if (Type2 == "PDF")
                {
                    _menu.Image.Url = "~/images/pdfico.png";
                    _menu.Name = "btnPdf";
                    _menu.Text = "";
                }
                else if (Type2 == "EXCEL")
                {
                    _menu.Image.Url = "~/images/excelico.png";
                    _menu.Name = "btnExcel";
                    _menu.Text = "";
                }
                else if (Type2 == "INSERT")
                {
                    _menu.Image.Url = "~/images/add_16.png";
                    _menu.Name = "btninsert";
                    _menu.Text = "";
                }
                else if (Type2 == "DIRECT")
                {
                    _menu.NavigateUrl = "javascript:OpenDirectLink('" + source + "')";
                    _menu.Name = fieldID;
                    _menu.Text = text;
                }
                else if (Type2 == "REFRESH")
                {
                    _menu.Image.Url = "~/images/refresh.png";
                    _menu.NavigateUrl = "javascript:" + gridId + ".PerformCallback('" + fieldID + "')";
                    _menu.Name = "btnSelected" + fieldID;
                    _menu.Text = text;

                }
                else if (Type2 == "SELECT")
                {
                    _menu.NavigateUrl = "javascript:" + gridId + ".PerformCallback('SelectButton|" + fieldID + "')";
                    _menu.Name = "btnSelected" + fieldID;
                    _menu.Text = text;
                }
                break;
        }

        return _menu;
    }

    private void leftmenuSettings(ASPxGridView grid, bool select, bool edit, bool delete, bool insert, bool insertHeader, bool deleteHeader, bool clone, bool clear)
    {
        GridViewCommandColumn _column = new GridViewCommandColumn();

        _column.Name = "cmnd";
        _column.ShowClearFilterButton = true;
        _column.ShowSelectCheckbox = select;
        _column.ShowSelectButton = select;
        _column.ShowDeleteButton = delete;
        _column.ShowEditButton = edit;
        _column.ShowNewButton = insert;

        _column.ShowNewButtonInHeader = insertHeader;
        if (clone && insertHeader) _column.CustomButtons.Add(commandcolumncustomButton());

        if (select)
        {
            _column.CustomButtons.Add(CreateCommandCustomButton("SelectAll", "Select All", true, "~/images/plus.png"));
            _column.CustomButtons.Add(CreateCommandCustomButton("UnSelectAll", "Remove Selection", true, "~/images/minus.png"));
            _column.Width = Unit.Pixel(70);
        }


        _column.ShowCancelButton = true;
        _column.ShowUpdateButton = true;
        _column.ButtonType = GridCommandButtonRenderMode.Image;
        _column.VisibleIndex = 0;
        if (!select)
            _column.Width = Unit.Pixel(Convert.ToInt32(dtRows.Select("[DX Field ID]='R0000'")[0]["DX Width"]));

        grid.Columns.Add(_column);

        grid.SettingsCommandButton.DeleteButton.Image.Url = "/images/trash.png";
        grid.SettingsCommandButton.EditButton.Image.Url = "/images/edit.png";
        grid.SettingsCommandButton.ClearFilterButton.Image.Url = "/images/clear_filter.png";
        grid.SettingsCommandButton.NewButton.Image.Url = "/images/add_16.png";
        grid.SettingsCommandButton.ClearFilterButton.Image.Url = "/images/clear_filter.png";
        if (dtRows.Select("[DX Type2]='DELETE'").Count() > 0)
        {
            grid.SettingsText.ConfirmDelete = dtRows.Select("[DX Type2]='DELETE'")[0]["TR"].ToString();
        }


    }

    private GridViewCommandColumnCustomButton commandcolumncustomButton()
    {
        GridViewCommandColumnCustomButton _cmdColumn = new GridViewCommandColumnCustomButton();
        _cmdColumn.ID = "Clone";
        _cmdColumn.Image.Url = "~/images/clone.png";
        _cmdColumn.Text = "Clone";
        //_cmdColumn.Visibility = GridViewCustomButtonVisibility.FilterRow;

        return _cmdColumn;
    }

    private GridViewCommandColumnCustomButton CreateCommandCustomButton(string Id, string ColumnText, bool IsFilterRow, string iconpath)
    {
        GridViewCommandColumnCustomButton _cmdColumn = new GridViewCommandColumnCustomButton();
        _cmdColumn.ID = Id;
        _cmdColumn.Image.Url = iconpath;
        _cmdColumn.Text = ColumnText;
        if (IsFilterRow) _cmdColumn.Visibility = GridViewCustomButtonVisibility.FilterRow;
        return _cmdColumn;
    }

    public static DataSet GetDynamicWebPageObjects(string PageId, string ObjectId, string _comp)
    {

        string _cachekey = "sp_0C_50032_00_WEB PAGE CAPTIONS" + "_" + PageId + "_" + ObjectId + "_" + _comp;
        List<CacheObject> _obj = CacheManager.Get(_cachekey, DateTime.Now);
        if (_obj.Count == 0 | CacheManager.CacheActive == 0)
        {

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
            {

                using (DataSet ds = new DataSet())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "[sp_0C_50032_00_WEB PAGE CAPTIONS]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 1000;
                        cmd.Connection = conn;
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@Sirket", _comp);
                        cmd.Parameters.AddWithValue("@PageId", PageId);
                        cmd.Parameters.AddWithValue("@ObjectId", ObjectId);
                        using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                        {
                            adp.Fill(ds);

                            CacheManager.SetDynamic(ds, _cachekey, 24, 1);
                            _obj = CacheManager.Get(_cachekey, DateTime.Now);
                        }
                    }
                }

            }
        }

        return _obj[0].objds;

        
    }
    public static DataTable fillSubGrid(string pageId, string objectId, string sirket, string user, string MainPageId, string MainObjectId, string MainParams)
    {

        using (DataTable dt = new DataTable())
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "[sp_0C_50057_00_WEBPAGES_03]";
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 1000;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@MainPageId", pageId);
                    cmd.Parameters.AddWithValue("@MainObjectId", MainObjectId);
                    cmd.Parameters.AddWithValue("@MainParams", MainParams);
                    cmd.Parameters.AddWithValue("@PageId", pageId);
                    cmd.Parameters.AddWithValue("@ObjectId", objectId);
                    cmd.Parameters.AddWithValue("@Sirket", sirket);
                    cmd.Parameters.AddWithValue("@UserName", user);

                    using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                    {
                        adp.Fill(dt);
                    }
                }
            }
            return dt;
        }
    }

    public static DataTable fillMainGrid(string pageId, string objectId, string sirket, string user)
    {

        using (DataTable dt = new DataTable())
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "[sp_0C_50057_00_WEBPAGES]";
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 1000;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@PageId", pageId);
                    cmd.Parameters.AddWithValue("@ObjectId", objectId);
                    cmd.Parameters.AddWithValue("@Sirket", sirket);
                    cmd.Parameters.AddWithValue("@UserName", user);

                    using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                    {
                        adp.Fill(dt);
                    }
                }
            }
            return dt;
        }
    }
    public static DataTable GetSubGridInsertPermission(string pageId, string objectId, string sirket, string user, string MainPageId, string MainObjectId, string MainParams)
    {
        using (DataTable dt = new DataTable())
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "[sp_0C_50057_00_WEBPAGES_02]";
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 1000;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@MainPageId", pageId);
                    cmd.Parameters.AddWithValue("@MainObjectId", MainObjectId);
                    cmd.Parameters.AddWithValue("@MainParams", MainParams);
                    cmd.Parameters.AddWithValue("@PageId", pageId);
                    cmd.Parameters.AddWithValue("@ObjectId", objectId);
                    cmd.Parameters.AddWithValue("@Sirket", sirket);
                    cmd.Parameters.AddWithValue("@UserName", user);

                    using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                    {
                        adp.Fill(dt);
                    }
                }
            }
            return dt;
        }
    }

    public static void ExportPdf(ASPxGridViewExporter exporter)
    {
        //Convert.ToInt32(exporter.GridView.Width.Value)
        exporter.Landscape = true;
        exporter.PaperKind = System.Drawing.Printing.PaperKind.DSheet;
        exporter.MaxColumnWidth = 150;
        exporter.LeftMargin = 2;
        exporter.TopMargin = 2;
        exporter.GridView.Columns[0].Visible = false;
        exporter.GridView.Columns["REDIT11"].Visible = false;
        exporter.WritePdfToResponse();
    }

    public static string DefaultSearchGrid(DataRow[] rows)
    {
        string filterexpression = string.Empty;
        try
        {
            //DataRow[] rows = dt.Select("[DX Default Search]<>'' AND [DX Display Order]>0");
            bool ilk = true;
            foreach (DataRow row in rows)
            {
                if (ilk)
                {
                    filterexpression = "[" + row["DX Field ID"].ToString() + "]  like '%" + row["DX Default Search"].ToString() + "%'";
                    ilk = false;
                }
                else
                {
                    filterexpression += " AND [" + row["DX Field ID"].ToString() + "] like '%" + row["DX Default Search"].ToString() + "%'";
                }
            }
        }
        catch
        {


        }

        return filterexpression;
    }



}


