﻿using System.Collections.Generic;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;

public class PdfMerge
{
    private BaseFont baseFont;
    private bool enablePagination = false;
    private readonly List<PdfReader> documents;
    private int totalPages;

    public BaseFont BaseFont
    {
        get { return baseFont; }
        set { baseFont = value; }
    }

    public bool EnablePagination
    {
        get { return enablePagination; }
        set
        {
            enablePagination = value;
            if (value && baseFont == null)
                baseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        }
    }

    public List<PdfReader> Documents
    {
        get { return documents; }
    }

    public void AddDocument(string filename)
    {
        documents.Add(new PdfReader(filename));
    }
    public void AddDocument(Stream pdfStream)
    {
        documents.Add(new PdfReader(pdfStream));
    }
    public void AddDocument(byte[] pdfContents)
    {
        documents.Add(new PdfReader(pdfContents));
    }
    public void AddDocument(PdfReader pdfDocument)
    {
        documents.Add(pdfDocument);
    }

    public void Merge(string outputFilename)
    {
        Merge(new FileStream(outputFilename, FileMode.Create));
    }
    public void Merge(Stream outputStream)
    {
        if (outputStream == null || !outputStream.CanWrite)
            throw new Exception("OutputStream es nulo o no se puede escribir en éste.");

        Document newDocument = null;
        try
        {
            newDocument = new Document();
            PdfWriter pdfWriter = PdfWriter.GetInstance(newDocument, outputStream);

            newDocument.Open();
            PdfContentByte pdfContentByte = pdfWriter.DirectContent;

            if (EnablePagination)
                documents.ForEach(delegate(PdfReader doc)
                {
                    totalPages += doc.NumberOfPages;
                });

            int currentPage = 1;
            float pageWidth=0, pageHeight=0;
            int rotation = 0, pdfcount = 1;
            foreach (PdfReader pdfReader in documents)
            {

                for (int page = 1; page <= pdfReader.NumberOfPages; page++)
                {
                    //newDocument.NewPage();
                    pageWidth = 0;
                    pageHeight = 0;
                    rotation = 0;
                    PdfImportedPage importedPage = pdfWriter.GetImportedPage(pdfReader, page);
                    pageWidth = importedPage.Width;
                    pageHeight = importedPage.Height;
                    rotation = importedPage.Rotation;

                    if (rotation == 0)
                    {
                        if (pdfcount == 1)
                            pdfContentByte.AddTemplate(importedPage, 1f, 0, 0, pageHeight, 0, 0);
                        else if (pdfcount == 2)
                            pdfContentByte.AddTemplate(importedPage, 1f, 0, 0, pageHeight - 200, 0, 0);
                        //rotationEvent.setRotation(PdfPage.PORTRAIT);
                    }
                    else if (rotation == 90)
                    {
                        pdfContentByte.AddTemplate(importedPage, 0, -1f, 1f, 0, 0, pageHeight);
                        //rotationEvent.setRotation(PdfPage.LANDSCAPE);
                    }
                    else if (rotation == 180)
                    {
                        pdfContentByte.AddTemplate(importedPage, 1f, 0, 0, -1f, pageWidth, pageHeight);
                        //rotationEvent.setRotation(PdfPage.INVERTEDPORTRAIT);
                    }
                    else if (rotation == 270)
                    {
                        pdfContentByte.AddTemplate(importedPage, 0, -1f, 1f, 0, 0, pageHeight);
                        //cb.addTemplate(page, 0, 1f, -1f, 0, pageWidth, 0);
                        //rotationEvent.setRotation(PdfPage.SEASCAPE);
                    }

                   // pdfContentByte.AddTemplate(importedPage, 0, 0);

                    if (EnablePagination)
                    {
                        pdfContentByte.BeginText();
                        pdfContentByte.SetFontAndSize(baseFont, 9);
                        pdfContentByte.ShowTextAligned(PdfContentByte.ALIGN_CENTER,
                            string.Format("{0} de {1}", currentPage++, totalPages), 520, 5, 0);
                        pdfContentByte.EndText();
                    }
                }
                pdfcount++;
            }
        }
        finally
        {
            outputStream.Flush();
            if (newDocument != null)
                newDocument.Close();
            outputStream.Close();
        }
    }

    public PdfMerge()
    {
        documents = new List<PdfReader>();
    }
}