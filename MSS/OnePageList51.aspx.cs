﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.Data;
using System.Collections;
using System.IO;
using System.Threading;
using MSS1.WSGeneric;
using System.ServiceModel;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Diagnostics;
using MSS1.Codes;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;

namespace MSS1
{
    public partial class OnePageList51 : Bases
    {
        Language l = new Language();
        Hashtable copiedValues = null, viewedValues = null;
        ServiceUtilities services = new ServiceUtilities();
        fonksiyonlar f = new fonksiyonlar();
        static string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        SessionBase _obj;

        public string OBJ1_pageId { get; set; }
        public string OBJ1_fieldId { get; set; }

        private void QueryStringParameterDesing()
        {
            if (Request.QueryString["P"] != null)
            {
                string _pagelink = DynamicPageLink;

                string[] objs = _pagelink.Split('&');

                int i = 1;
                foreach (string str in objs)
                {
                    #region dynamic page objs
                    if (i == 1)
                    {
                        OBJ1_pageId = str.Split(';')[0];
                        OBJ1_fieldId = str.Split(';')[1];
                    }

                    #endregion
                    i++;
                }

            }
            else
            {

                string[] obj1, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10, obj11, obj12, obj13, obj14, obj15 = { };
                string _val = string.Empty;
                obj1 = Request.QueryString["OJ1"].ToString().Split(';');


                try
                {
                    OBJ1_pageId = obj1[0].ToString();
                    OBJ1_fieldId = obj1[1].ToString();

                }
                catch
                {
                    OBJ1_pageId = string.Empty;
                    OBJ1_fieldId = string.Empty;
                }
            }

            grid1.JSProperties["cpPageLinked"] = false;
            grid1.JSProperties["cpIsUpdated"] = false;
            grid1.JSProperties["cpIsLinked"] = false;
            grid1.JSProperties["cpOpType"] = string.Empty;
            grid1.JSProperties["cpQval"] = string.Empty;
            grid1.JSProperties["cpMsg"] = string.Empty;
            grid1.JSProperties["cpIslemId"] = string.Empty;
            grid1.JSProperties["cpLink"] = string.Empty;
            grid1.JSProperties["cpQueryStrings"] = string.Empty;
            grid1.JSProperties["cpSource"] = string.Empty;
            grid1.JSProperties["cpProcType"] = string.Empty;

        }

        private string DynamicPageLink
        {
            get
            {
                return f.tekbirsonucdonder("select [DX Path]+[DX Path2] from [0C_50032_01_WEB PAGE CAPTIONS] where COMPANY='" + _obj.Comp + "'" +
                    " and PageID='MasterPage' and FieldID='" + Request.QueryString["P"].ToString() + "'");
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();
            PageInit();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }
        void PageInit()
        {
            QueryStringParameterDesing();


            if (OBJ1_pageId.Length > 0 & OBJ1_fieldId.Length > 0)
            {
                string OIds = OBJ1_pageId, FIds = OBJ1_fieldId;
                List<object> GrdObjs = new List<object>();
                GrdObjs.Add(grid1);


                DynamicUtilsV1.FillDynamicColumns(GrdObjs, OIds, FIds, HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString()
                , HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), _obj.UserName, _obj.Comp, _obj.SelectedLanguage, _obj.GuidKey);


            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            QueryStringParameterDesing();
            if (IsCallback | IsPostBack) return;
            DilGetir();
            GetParameters();
            PreparePage();
            FillDynamicObjects();

        }

        #region pageload methods
        private void DilGetir()
        {
            txtDHT1.Text = l.dilgetirfordynamically(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, OBJ1_pageId + "_" + OBJ1_fieldId);

        }
        void GetParameters()
        {
            Session["ClonedIdsAndValues"] = string.Empty;

            txtOBJP1.Text = OBJ1_pageId;
            txtOBJF1.Text = OBJ1_fieldId;
            ViewState["OBJF1"] = OBJ1_fieldId;
            ViewState["OBJP1"] = OBJ1_pageId;
        }
        private void FillDynamicObjects()
        {
            if (OBJ1_pageId.Length > 0 & OBJ1_fieldId.Length > 0)
            {


                string OIds = OBJ1_pageId, FIds = OBJ1_fieldId;
                List<object> GrdObjs = new List<object>();
                List<object> MenuObjs = new List<object>();
                List<object> InsPObjs = new List<object>();
                List<object> CloneObjs = new List<object>();
                List<object> EdtPObjs = new List<object>();
                List<object> DelPObjs = new List<object>();
                List<object> PageParObjs = new List<object>();
                GrdObjs.Add(grid1);
                MenuObjs.Add(new ASPxMenu());
                InsPObjs.Add(txtinsertparams1);
                CloneObjs.Add(txtCloneParameters1);
                EdtPObjs.Add(txteditparams1);
                DelPObjs.Add(txtdeleteparams1);
                PageParObjs.Add(txtlink1);
                PageParObjs.Add(txtlinkField1);
                PageParObjs.Add(txtCatchParameters1);
                PageParObjs.Add(txtCollapseHeader1);
                PageParObjs.Add(txtCollapseStatu1);
                PageParObjs.Add(HiddendirectParam);
                PageParObjs.Add(txtBos);
                PageParObjs.Add(txtRef1);

                GenerateRnd();
                DataColumn[] pricols = new DataColumn[1];
                DynamicUtilsV1.FillDynamicObjects(GrdObjs, MenuObjs, OIds, FIds, HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString()
                    , HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), InsPObjs, CloneObjs, EdtPObjs, DelPObjs, PageParObjs
                    , "OPL" + ViewState["RndVal"].ToString() + "50Grid", _obj.UserName, _obj.Comp, _obj.SelectedLanguage);
                pricols[0] = dtgrid1.Table.Columns["ID"];
                dtgrid1.Table.PrimaryKey = pricols;
                grid1.DataSourceID = "dtgrid1";
                grid1.DataBind();

                grid1.Width = Unit.Pixel(0);

                if (!ClientScript.IsStartupScriptRegistered("JSLoadMainData"))
                {
                    ClientScript.RegisterStartupScript(GetType(), "JSLoadMainData", @"grid1.PerformCallback('LoadMainData');", true);
                }


            }
            else
            {
                grid1.Visible = false;
            }
        }
        void GenerateRnd()
        {
            Random rnd = new Random();
            ViewState["RndVal"] = rnd.Next(10000, 100000).ToString();
        }

        private void PreparePage()
        {
            lblGId.Text = Request.QueryString["GId"];
            if (Request.QueryString["H1"] != null) this.HiddenP1.Value = Request.QueryString["H1"];
            if (Request.QueryString["H2"] != null) this.HiddenP2.Value = Request.QueryString["H2"];
            if (Request.QueryString["H3"] != null) this.HiddenP3.Value = Request.QueryString["H3"];
            if (Request.QueryString["H4"] != null) this.HiddenP4.Value = Request.QueryString["H4"];
            if (Request.QueryString["H5"] != null) this.HiddenP5.Value = Request.QueryString["H5"];
            if (Request.QueryString["cpVals"] != null) this.txtCatchParamsAndValues.Text = Request.QueryString["cpVals"];

        }
        #endregion

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }
        protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {

            Session["InsertedVal"] = string.Empty;
            Session["WebMessage"] = string.Empty;
            ASPxGridView gridI = sender as ASPxGridView;
            string insertparams = string.Empty, _IslemId = string.Empty;
            if (Request.QueryString["IslemId"] != null)
                _IslemId = Request.QueryString["IslemId"].ToString();
            string[] _temp = { }, _array = { };
            switch (gridI.ID)
            {
                case "grid1":
                    insertparams = txtinsertparams1.Text;
                    _temp = txtinsertparams1.Text.Split(',');
                    _array = new string[txtinsertparams1.Text.Split(',').Length - 1];
                    break;
            }

            if (!string.IsNullOrEmpty(insertparams))
            {
                bool _dvar = false;
                for (int i = 1; i < _temp.Length; i++)
                {
                    if (_temp[i].Contains("_d") | _temp[i].Contains("_t"))
                        _dvar = true;
                    else
                        _dvar = false;
                    string _tempi = _temp[i].Replace("_d", string.Empty).Replace("_t", string.Empty);
                    if (_tempi == "COMP")
                    {
                        _array[i - 1] = _obj.Comp;
                    }
                    else if (_tempi == "USER")
                    {
                        _array[i - 1] = _obj.UserName;
                    }
                    else if (_tempi == "ISLEMID")
                    {
                        if (Request.QueryString["IslemId"] != null)
                            _array[i - 1] = Request.QueryString["IslemId"].ToString();
                        else
                            _array[i - 1] = "";
                    }
                    else if (_tempi == "P1")
                    {
                        _array[i - 1] = this.HiddenP1.Value;
                    }
                    else if (_tempi == "P2")
                    {
                        _array[i - 1] = this.HiddenP2.Value;
                    }
                    else if (_tempi == "P3")
                    {
                        _array[i - 1] = this.HiddenP3.Value;
                    }
                    else if (_tempi == "P4")
                    {
                        _array[i - 1] = this.HiddenP4.Value;
                    }
                    else if (_tempi == "P5")
                    {
                        _array[i - 1] = this.HiddenP5.Value;
                    }
                    else if (string.IsNullOrEmpty(_tempi))
                    {
                        _array[i - 1] = "";
                    }
                    else if (_tempi.Substring(0, 1) == "$")
                    {
                        try { _array[i - 1] = _tempi.Substring(1, _tempi.Length - 1); } catch { _array[i - 1] = ""; }
                    }
                    else if (_tempi.Substring(0, 1) == "#")
                    {
                        try
                        {
                            string[] transparams = txtCatchParamsAndValues.Text.Split('|');
                            string _val1 = Array.Find(transparams, element => element.StartsWith(_tempi.Replace("#", string.Empty), StringComparison.Ordinal));

                            _array[i - 1] = _val1.Split(';')[1];

                        }
                        catch { _array[i - 1] = ""; }
                    }
                    else
                    {
                        if (e.NewValues[_tempi] != null)
                        {
                            if (e.NewValues[_tempi].ToString().Contains('|'))
                                _array[i - 1] = e.NewValues[_tempi].ToString().Split('|')[e.NewValues[_tempi].ToString().Split('|').Length - 1];
                            else
                            {
                                _array[i - 1] = e.NewValues[_tempi].ToString();
                            }
                        }
                        else
                            _array[i - 1] = "";

                    }

                    if (_dvar & !string.IsNullOrEmpty(_array[i - 1])) _array[i - 1] = _temp[i].Substring(0, 2) + _array[i - 1];
                }


                services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                if (!string.IsNullOrEmpty(Session["InsertedVal"].ToString()))
                    gridI.JSProperties["cpQval"] = Session["InsertedVal"].ToString();

            }

            string _cachekey = "sp_0C_50057_00_WEBPAGES_15" + "_" + OBJ1_pageId + "_" + OBJ1_fieldId + "_" + _obj.Comp;

            CacheObject _obj1 = CacheManager.Get(_cachekey, DateTime.Now)[0];


            var Q1 = from p in _obj1.objds.Tables[0].AsEnumerable()
                     where p.Field<string>("DX Type2") == "REPORT" & p.Field<int>("DX Display Order") > 0
                     select new
                     {
                         FieldId = p.Field<string>("DX Field ID"),
                         source = p.Field<string>("DX Source"),
                         filter = p.Field<string>("DX Filter"),
                         dxpath = p.Field<string>("DX Path")
                     };

            if (Q1.Count() > 0)
            {
                string Result11 = Session["WebMessage"].ToString()
                , Result12 = Session["InsertedVal"].ToString(), Result21 = string.Empty, Result22 = string.Empty;

                if (!string.IsNullOrEmpty(Q1.FirstOrDefault().dxpath))
                {
                    string _PdfResult = ReportUtilities.CreateReport(_obj.UserName, _obj.Comp, Q1.FirstOrDefault().source.Split(',')[0], true
                    , Server.MapPath(CurrentFolderName(_obj.Comp) + "/" + Q1.FirstOrDefault().dxpath), Result12
                     , txtCatchParamsAndValues.Text, HiddenP1.Value, HiddenP2.Value, _IslemId, Q1.FirstOrDefault().filter.Split('|')[0]);

                    if (!string.IsNullOrEmpty(_PdfResult))
                    {
                        throw new Exception(_PdfResult);
                    }

                    string[] arr = new string[] { _IslemId, _obj.UserName };
                    services.NavFunctionsStatic(_obj.UserName, _obj.Comp, "50000.OnAfterWebReportSELECTED", arr);


                    Result21 = Session["WebMessage"].ToString();
                    Result22 = Session["InsertedVal"].ToString();

                    gridI.JSProperties["cpProcType"] = "POSTMODE|" + Result11 + "|" + Result12 + "|" + Result21 + "|" + Result22;
                    gridI.JSProperties["cpLink"] = CurrentFolderName(_obj.Comp) + "/" + Q1.FirstOrDefault().dxpath + "/" + Result12 + ".PDF";
                    gridI.JSProperties["cpIslemId"] = _IslemId;
                    gridI.JSProperties["cpQueryStrings"] = Q1.FirstOrDefault().filter.Split('|')[0];
                    gridI.JSProperties["cpIsLinked"] = true;
                }
                else
                {
                    gridI.JSProperties["cpProcType"] = "OPENMODE";
                    gridI.JSProperties["cpLink"] = Q1.FirstOrDefault().source;
                    gridI.JSProperties["cpIslemId"] = _IslemId;
                    gridI.JSProperties["cpQueryStrings"] = Q1.FirstOrDefault().filter.Split('|')[0];
                    gridI.JSProperties["cpIsLinked"] = true;
                }
            }

            copiedValues = null;

            gridI.JSProperties["cpIsUpdated"] = true;
            gridI.JSProperties["cpMsg"] = Session["WebMessage"].ToString();
            Session["ClonedIdsAndValues"] = string.Empty;

        }
        protected void Grid_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;
            string _sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + grid.ID.Substring(grid.ID.Length == 5 ? grid.ID.Length - 1 : grid.ID.Length - 2) + "Rows";
            switch (e.ButtonType)
            {

                case ColumnCommandButtonType.New:
                    if (grid.JSProperties["cpOpType"].ToString() == "INSERT")
                        e.Visible = false;
                    break;
            }



        }
        protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
        {

            Session["ClonedIdsAndValues"] = string.Empty;
            ASPxGridView gridI = sender as ASPxGridView;
            string Sessionn = "OPL" + ViewState["RndVal"].ToString() + "50Grid" + gridI.ID.Substring(gridI.ID.Length == 5 ? gridI.ID.Length - 1 : gridI.ID.Length - 2) + "Rows";
            string sessionval = string.Empty;
            bool ilk = true;

            if (gridI.JSProperties["cpOpType"].ToString() == "VIEW")
            {
                DataRow[] rowsviewhd = ((DataTable)Session[Sessionn]).Select("[DX Type1]='DATA' and [DX VwPI]=0 and  [DX Display Order]>0");
                if (rowsviewhd.Length > 0)
                {
                    foreach (DataRow row in rowsviewhd)
                    {
                        gridI.DataColumns[row["DX Field ID"].ToString()].EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;

                    }
                }

                DataRow[] rowsviewvs = ((DataTable)Session[Sessionn]).Select("[DX Type1]='DATA' and [DX VwPI]>0 and  [DX Display Order]>0");
                if (rowsviewvs.Length > 0)
                {
                    foreach (DataRow row in rowsviewvs)
                    {

                        gridI.DataColumns[row["DX Field ID"].ToString()].EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.True;
                        gridI.DataColumns[row["DX Field ID"].ToString()].EditFormSettings.VisibleIndex = Convert.ToInt32(row["DX VwPI"].ToString());
                    }
                }

                if (viewedValues != null)
                {
                    DataRow[] rows = ((DataTable)Session[Sessionn]).Select("[DX VwPI]>0");
                    foreach (DataRow row in rows)
                        e.NewValues[row["DX Field ID"].ToString()] = viewedValues[row["DX Field ID"].ToString()];
                }


            }
            else
            {

                if (gridI.JSProperties["cpOpType"].ToString() == "CLONE")
                {
                    DataRow[] rowsclonehd = ((DataTable)Session[Sessionn]).Select("[DX Type1]='DATA' and [DX Clone Hidden]>0 and  [DX Display Order]>0");

                    if (rowsclonehd.Length > 0)
                    {
                        foreach (DataRow row in rowsclonehd)
                        {
                            gridI.DataColumns[row["DX Field ID"].ToString()].EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;
                        }
                    }

                    DataRow[] rowsclonenhd = ((DataTable)Session[Sessionn]).Select("[DX Type1]='DATA' and [DX Clone Hidden]=0  and [DX Edit Popup Index]>-1 and [DX Display Order]>0");
                    if (rowsclonenhd.Length > 0)
                    {
                        foreach (DataRow row in rowsclonenhd)
                        {
                            gridI.DataColumns[row["DX Field ID"].ToString()].EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.True;

                        }
                    }


                    if (copiedValues != null)
                    {
                        DataRow[] rows = ((DataTable)Session[Sessionn]).Select("[DX Clone] >0");
                        foreach (DataRow row in rows)
                        {
                            e.NewValues[row["DX Field ID"].ToString()] = copiedValues[row["DX Field ID"].ToString()];

                            if (ilk)
                            {
                                sessionval = row["DX Field ID"].ToString() + ":" + copiedValues[row["DX Field ID"].ToString()];
                                ilk = false;
                            }
                            else
                                sessionval += "#" + row["DX Field ID"].ToString() + ":" + copiedValues[row["DX Field ID"].ToString()];
                        }

                        if (!string.IsNullOrEmpty(sessionval)) Session["ClonedIdsAndValues"] = sessionval;
                    }
                }
                else if (gridI.JSProperties["cpOpType"].ToString() == "INSERT" | string.IsNullOrEmpty(gridI.JSProperties["cpOpType"].ToString()))
                {
                    DataRow[] rowsInsertHidden = ((DataTable)Session[Sessionn]).Select("[DX Type1]='DATA' and [DX Insert Hidden]=1 and  [DX Display Order]>0");

                    if (rowsInsertHidden.Length > 0)
                    {
                        foreach (DataRow row in rowsInsertHidden)
                        {
                            try { gridI.DataColumns[row["DX Field ID"].ToString()].EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False; } catch { }

                        }
                    }


                }




            }


        }
        protected void grid_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;

            gridI.JSProperties["cpOpType"] = "NEW";


        }
        protected void grid_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            ASPxGridView xgrid = (ASPxGridView)sender;
            string _Session = GetSessionName(xgrid.ID);

            if (xgrid.JSProperties["cpOpType"].ToString() == "VIEW")
            {
                e.Editor.ClientEnabled = false;
                return;

            }



            if (xgrid.IsNewRowEditing)
            {
                if (e.Editor.GetType() == typeof(GridViewDataComboBoxColumn)
                    | e.Editor.GetType() == typeof(ASPxComboBox))
                {
                    ASPxComboBox cmb = e.Editor as ASPxComboBox;
                    if (cmb.NullText.Contains("CascColumn1"))
                    {
                        cmb.CallbackPageSize = 5000;
                        try
                        {
                            string[] _array = cmb.NullText.Split('#');
                            string _source = _array[1],
                                _filter = _array[2],
                                _value = _array[3],
                                _text = _array[4],
                                _anafieldId = _array[5],
                                _anasorgu = _array[6],
                                _cascfilter = _array[7];
                            object _val = null;
                            string _val2 = string.Empty;
                            if (Session["ClonedIdsAndValues"] != null)
                            {
                                if (!string.IsNullOrEmpty(Session["ClonedIdsAndValues"].ToString()))
                                {
                                    string[] _arrclonedvals = Session["ClonedIdsAndValues"].ToString().Split('#');

                                    var q1 = from p in _arrclonedvals where p.Contains(_anafieldId + ":") select p;

                                    if (q1.Count() > 0)
                                        _val2 = q1.FirstOrDefault().ToString().Split(':')[1];
                                }
                                else
                                    _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, _anafieldId);
                            }
                            else
                                _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, _anafieldId);
                            if (!string.IsNullOrEmpty(_val2))
                                _val = _val2;

                            if (_val != null)
                            {
                                if (!string.IsNullOrEmpty(_val.ToString()))
                                {
                                    string sorgu = "select " + _value + ", " + _text + "  from [" + _source + "] where 1=1 ";

                                    if (!string.IsNullOrEmpty(_filter))
                                        sorgu += " and " + _filter + "='" + _obj.Comp + "'";
                                    sorgu += " and " + _cascfilter + "='" + _val.ToString() + "'";

                                    f.FillAspxCombobox(cmb, sorgu, _text, _value);

                                    //Session["AddValPar1"] = " and " + _cascfilter + "='" + _val.ToString() + "'";

                                    if (!string.IsNullOrEmpty(e.Editor.Value.ToString()))
                                    {
                                        try
                                        {
                                            if (cmb.Items.Count == 1)
                                                cmb.SelectedIndex = 0;
                                            else
                                                cmb.SelectedIndex = cmb.Items.IndexOfValue(e.Editor.Value.ToString());

                                        }
                                        catch { }
                                    }
                                }
                            }
                        }
                        catch { }
                        cmb.Callback += cmbDynamicCascade;
                    }
                    else if (cmb.NullText.Contains("CascColumn2"))
                    {
                        cmb.CallbackPageSize = 5000;
                        try
                        {
                            string[] _array = cmb.NullText.Split('#');
                            string _source = _array[1],
                                _filter = _array[2],
                                _value = _array[3],
                                _text = _array[4],
                                _anafieldId = _array[5],
                                _anasorgu = _array[6],
                                _cascfilter = _array[7],
                                _mainval = _array[8],
                                _mainfilter = _array[9];

                            string _val2par = string.Empty;
                            object _val = null;
                            string _val2 = string.Empty;
                            if (Session["ClonedIdsAndValues"] != null)
                            {
                                if (!string.IsNullOrEmpty(Session["ClonedIdsAndValues"].ToString()))
                                {
                                    string[] _arrclonedvals = Session["ClonedIdsAndValues"].ToString().Split('#');

                                    var q1 = from p in _arrclonedvals where p.Contains(_anafieldId + ":") select p;

                                    if (q1.Count() > 0)
                                        _val2 = q1.FirstOrDefault().ToString().Split(':')[1];
                                }
                                else
                                    _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, _anafieldId);
                            }
                            else
                                _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, _anafieldId);
                            if (!string.IsNullOrEmpty(_val2))
                                _val = _val2;
                            if (_val != null)
                            {
                                if (!string.IsNullOrEmpty(_val.ToString()))
                                {

                                    string sorgu = "select " + _cascfilter + " from [" + _anasorgu + "] where 1=1 ";
                                    if (!string.IsNullOrEmpty(_mainfilter))
                                        sorgu += " and " + _mainfilter + "='" + _obj.Comp + "'";
                                    sorgu += " and " + _mainval + "='" + _val.ToString() + "'";

                                    _val2par = f.tekbirsonucdonder(sorgu);

                                    sorgu = "select " + _value + ", " + _text + "  from [" + _source + "] where 1=1 ";

                                    if (!string.IsNullOrEmpty(_filter))
                                        sorgu += " and " + _filter + "='" + _obj.Comp + "'";
                                    sorgu += " and " + _cascfilter + "='" + _val2par + "'";

                                    f.FillAspxCombobox(cmb, sorgu, _text, _value);

                                    //Session["AddValPar2"] = " and " + _cascfilter + "='" + _val2par + "'";

                                    if (!string.IsNullOrEmpty(e.Editor.Value.ToString()))
                                    {
                                        try
                                        {
                                            if (cmb.Items.Count == 1)
                                                cmb.SelectedIndex = 0;
                                            else
                                                cmb.SelectedIndex = cmb.Items.IndexOfValue(e.Editor.Value.ToString());

                                        }
                                        catch { }
                                    }
                                }
                            }
                        }
                        catch { }
                        cmb.Callback += cmbDynamicCascade2;
                    }
                    else if (cmb.NullText.Contains("CascColumn3"))
                    {
                        cmb.CallbackPageSize = 5000;
                        try
                        {
                            string[] _array = cmb.NullText.Split('#');
                            string _source = _array[1],
                                _filter = _array[2],
                                _value = _array[3],
                                _text = _array[4],
                                _anafieldId = _array[5],
                                _anasorgu = _array[6],
                                _cascfilter = _array[7],
                                _mainval = _array[8],
                                _mainfilter = _array[9];

                            string _val2par = string.Empty;
                            object _val = null;
                            string _val2 = string.Empty;
                            if (Session["ClonedIdsAndValues"] != null)
                            {
                                if (!string.IsNullOrEmpty(Session["ClonedIdsAndValues"].ToString()))
                                {
                                    string[] _arrclonedvals = Session["ClonedIdsAndValues"].ToString().Split('#');

                                    var q1 = from p in _arrclonedvals where p.Contains(_anafieldId + ":") select p;

                                    if (q1.Count() > 0)
                                        _val2 = q1.FirstOrDefault().ToString().Split(':')[1];
                                }
                                else
                                    _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, _anafieldId);
                            }
                            else
                                _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, _anafieldId);
                            if (!string.IsNullOrEmpty(_val2))
                                _val = _val2;
                            //if (_val != null)
                            //{
                            //    if (!string.IsNullOrEmpty(_val.ToString()))
                            //    {

                            //string sorgu = "select " + _cascfilter + " from [" + _anasorgu + "] where 1=1 ";
                            //if (!string.IsNullOrEmpty(_mainfilter))
                            //    sorgu += " and " + _mainfilter + "='" + _obj.Comp + "'";
                            //sorgu += " and " + _mainval + "='" + _val.ToString() + "'";


                            //_val2par = f.tekbirsonucdonder(sorgu);

                            string sorgu = "select " + _value + ", " + _text + "  from [" + _source + "] where 1=1 ";

                            if (!string.IsNullOrEmpty(_filter))
                                sorgu += " and " + _filter + "='" + _obj.Comp + "'";
                            //sorgu += " and " + _value + "='" + _val2par + "'";

                            f.FillAspxCombobox(cmb, sorgu, _text, _value);

                            if (!string.IsNullOrEmpty(e.Editor.Value.ToString()))
                            {
                                try { cmb.SelectedIndex = cmb.Items.IndexOfValue(e.Editor.Value); } catch { }
                            }
                            //    }
                            //}
                        }
                        catch { }
                        cmb.Callback += cmbDynamicCascade3;
                    }
                    else if (cmb.NullText.Contains("CascColumn4"))
                    {
                        cmb.CallbackPageSize = 5000;
                        string _val2par = string.Empty;
                        object _val = null;
                        string _val2 = string.Empty, _val3 = string.Empty;
                        try
                        {
                            string[] _array = cmb.NullText.Split('@');
                            string _anafieldId = _array[1],
                                    _source = _array[2],
                                    _filter = _array[3],
                                    _value = _array[4],
                                    _text = _array[5];


                            if (Session["ClonedIdsAndValues"] != null)
                            {
                                if (!string.IsNullOrEmpty(Session["ClonedIdsAndValues"].ToString()))
                                {
                                    string[] _arrclonedvals = Session["ClonedIdsAndValues"].ToString().Split('#');

                                    var q1 = from p in _arrclonedvals where p.Contains(_anafieldId + ":") select p;

                                    if (q1.Count() > 0)
                                        _val2 = q1.FirstOrDefault().ToString().Split(':')[1];

                                    var q2 = from p in _arrclonedvals where p.Contains(e.Column.FieldName + ":") select p;

                                    if (q2.Count() > 0)
                                        _val3 = q2.FirstOrDefault().ToString().Split(':')[1];

                                }
                                else
                                    _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, _anafieldId);
                            }
                            else
                                _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, _anafieldId);
                            if (!string.IsNullOrEmpty(_val2))
                                _val = _val2;


                            if (e.Editor.Value != null)
                            {
                                if (!string.IsNullOrEmpty(e.Editor.Value.ToString()))
                                {
                                    try { cmb.SelectedIndex = cmb.Items.IndexOfValue(e.Editor.Value); } catch { }
                                }
                            }

                            if (_val != null)
                            {
                                if (!string.IsNullOrEmpty(_val.ToString()))
                                {

                                    using (SqlConnection conn = new SqlConnection(strConnString))
                                    {
                                        using (SqlCommand cmd = new SqlCommand())
                                        {
                                            cmd.CommandText = _source;
                                            cmd.CommandType = CommandType.StoredProcedure;
                                            cmd.Connection = conn;
                                            _filter = _filter.Substring(0, _filter.Length);
                                            string[] spParams = _filter.Split(',');

                                            cmd.Parameters.AddWithValue("PRM1", _val);

                                            for (int i = 1; i < spParams.Length; i++)
                                            {
                                                string _result = string.Empty;
                                                if (spParams[i] == "USER")
                                                {
                                                    _result = _obj.UserName;
                                                }
                                                else if (spParams[i] == "COMP")
                                                {
                                                    _result = _obj.Comp;
                                                }
                                                else if (spParams[i].Contains("#"))
                                                {
                                                    string[] transParam = txtCatchParamsAndValues.Text.Split('|');
                                                }
                                                else if (spParams[i].ToString() == "P1")
                                                    _result = HiddenP1.Value;
                                                else if (spParams[i].ToString() == "P2")
                                                    _result = HiddenP2.Value;
                                                else
                                                {
                                                    object _res = xgrid.GetRowValuesByKeyValue(e.KeyValue, spParams[i]);
                                                    _result = _res.ToString();
                                                }


                                                cmd.Parameters.AddWithValue("@PRM" + (i + 1), _result);
                                            }



                                            DataTable dt = new DataTable();
                                            SqlDataAdapter adp = new SqlDataAdapter(cmd);
                                            adp.Fill(dt);


                                            cmb.Items.Clear();
                                            cmb.DataSourceID = null;
                                            cmb.DataSource = dt;
                                            cmb.TextField = _text;
                                            cmb.ValueField = _value;
                                            cmb.DataBind();

                                            if (!string.IsNullOrEmpty(e.Editor.Value.ToString()))
                                            {
                                                try { cmb.SelectedIndex = cmb.Items.IndexOfValue(e.Editor.Value); } catch { }
                                            }
                                        }
                                    }

                                }
                            }






                        }
                        catch (Exception ex)
                        {
                        }
                        cmb.Callback += cmbDynamicCascade4;
                        if (!string.IsNullOrEmpty(_val3))
                            try { cmb.SelectedIndex = cmb.Items.IndexOfValue(_val3); } catch { }

                    }
                    else if (cmb.NullText.Contains("CascColumn6"))
                    {
                        cmb.CallbackPageSize = 5000;
                        cmbDynamicCascade6(cmb, xgrid, e);
                    }

                    try
                    {
                        if (!string.IsNullOrEmpty(Session["InsertComboSelectedValue"].ToString()))
                        {
                            if (e.Column.FieldName == Session["InsertComboSelectedValue"].ToString().Split('#')[0])
                            {
                                cmb.DataSource = f.datatablegonder(txtInsertComboSource.Text + txtInsertComboParams.Text.Replace("X", Session["InsertComboSelectedValue"].ToString().Split('#')[1]));
                                cmb.DataBind();
                            }
                        }
                    }
                    catch
                    {


                    }

                }
            }


            if (xgrid.IsNewRowEditing)
            {
                DataRow[] RowDefaultVal = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX DfVl]<>''");
                if (RowDefaultVal.Length > 0)
                {
                    string defVal = RowDefaultVal[0]["DX DfVl"].ToString();
                    if (e.Editor.GetType() == typeof(ASPxComboBox))
                    {
                        ASPxComboBox cmb = e.Editor as ASPxComboBox;
                        if (defVal.Substring(0, 1) == "#")
                        {



                        }
                        else
                        {
                            try
                            {
                                int number;
                                bool success = Int32.TryParse(defVal.Trim(), out number);
                                if (success)
                                    cmb.SelectedIndex = cmb.Items.IndexOfValue(number);
                                else
                                    cmb.SelectedIndex = cmb.Items.IndexOfValue(defVal.Trim());
                            }
                            catch { }
                        }
                    }
                    else if (e.Editor.GetType() == typeof(ASPxTextBox))
                    {
                        ASPxTextBox cmb = e.Editor as ASPxTextBox;
                        if (defVal.Substring(0, 1) == "#")
                        {



                        }
                        else
                        {
                            cmb.Text = defVal;
                        }
                    }
                }


                DataRow[] RowsReadOnly = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "' and [DX Insert Readonly]=1");
                if (RowsReadOnly.Length > 0)
                    e.Editor.ClientEnabled = false;
                else
                    e.Editor.ClientEnabled = true;



            }



            int tabindex = 0;

            try
            {
                DataRow[] RowsField = ((DataTable)Session[_Session]).Select("[DX Type1]='DATA' and [DX Field ID]='" + e.Column.FieldName + "'");
                if (RowsField.Length > 0)
                {
                    tabindex = Convert.ToInt32(RowsField[0]["DX InTI"]);
                }
            }
            catch { }


            e.Editor.TabIndex = Convert.ToInt16(tabindex);





        }
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            Web_GenericFunction_PortClient webFonksiyonlari;
            BasicHttpBinding _binding;
            ws_baglantı ws = new ws_baglantı();
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            EndpointAddress _endpoint = new EndpointAddress(new Uri(ws.sirketal(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }
        string GetSessionName(string gridId)
        {
            string sessionname = string.Empty;
            switch (gridId)
            {
                case "grid1":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid1Rows";
                    break;
                default:
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid1Rows";
                    break;
            }

            return sessionname;
        }

        protected void grid_RowValidating(object sender, ASPxDataValidationEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            string sessionname = string.Empty;
            switch (gridI.ID)
            {
                case "grid1":
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid1Rows";
                    break;
                default:
                    sessionname = "OPL" + ViewState["RndVal"].ToString() + "50Grid1Rows";
                    break;
            }

            if (e.NewValues["REDIT"] == null)
                e.NewValues["REDIT"] = 1;


            DataRow[] rows1 = ((DataTable)Session[sessionname]).Select("[DX Type1]='DECIMAL' OR [DX Type2]='INTEGER'");
            foreach (DataRow row in rows1)
            {
                if (e.NewValues[row["DX Field ID"].ToString()] == null)
                    e.NewValues[row["DX Field ID"].ToString()] = 0;
            }




        }

        protected void grid1_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
        {
            if (txtOBJF1.Text == "OJ9999") return;
            ASPxGridView grid = (ASPxGridView)sender;
            this.dtgrid1.Table = DynamicUtilsV1.fillEmptyMainGrid(txtOBJP1.Text, txtOBJF1.Text, _obj.Comp, _obj.UserName);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid1.Table.Columns["ID"];
            this.dtgrid1.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid1";
            grid.KeyFieldName = "ID";
            grid.DataBind();

            grid.AddNewRow();

        }

        private void cmbDynamicCascade(object sender, CallbackEventArgsBase e)
        {
            if (string.IsNullOrEmpty(e.Parameter)) return;
            if (!e.Parameter.Contains("#")) return;
            string[] PMarray = e.Parameter.Split('#');
            string[] PSArray = PMarray[1].Split(';');
            string _valpar = PMarray[0];
            string DxSource = PSArray[0], ComboValue = PSArray[1], ComboText = PSArray[2]
                , ComboFilter = PSArray[3], ComboOrder = PSArray[4], DxCascFilter = PSArray[5];
            ASPxComboBox cmb = (ASPxComboBox)sender;

            string sorgu = "select " + ComboValue + ", " + ComboText + "  from [" + DxSource + "] where 1=1 ";

            if (!string.IsNullOrEmpty(ComboFilter))
                sorgu += " and " + ComboFilter + "='" + _obj.Comp + "'";
            sorgu += " and " + DxCascFilter + "='" + _valpar + "'";

            if (!string.IsNullOrEmpty(ComboOrder))
                sorgu += " order by " + ComboOrder;

            //Session["AddValPar1"] = " and " + DxCascFilter + "='" + _valpar + "'";


            f.FillAspxCombobox(cmb, sorgu, ComboText, ComboValue);


        }

        private void cmbDynamicCascade2(object sender, CallbackEventArgsBase e)
        {
            if (string.IsNullOrEmpty(e.Parameter)) return;
            if (!e.Parameter.Contains("#")) return;
            string[] PMarray = e.Parameter.Split('#');
            string[] PSArray = PMarray[1].Split(';');
            string _valpar = PMarray[0], _val2par = string.Empty;
            string DxSource = PSArray[0], ComboValue = PSArray[1], ComboText = PSArray[2]
                , ComboFilter = PSArray[3], ComboOrder = PSArray[4], DxCascFilter = PSArray[5]
                , MainSource = PSArray[6], MainValue = PSArray[7], MainFilter = PSArray[8];
            ASPxComboBox cmb = (ASPxComboBox)sender;
            string sorgu = string.Empty;

            sorgu = "select " + DxCascFilter + " from [" + MainSource + "] where 1=1 ";
            if (!string.IsNullOrEmpty(MainFilter))
                sorgu += " and " + MainFilter + "='" + _obj.Comp + "'";
            sorgu += " and " + MainValue + "='" + _valpar + "'";


            _val2par = f.tekbirsonucdonder(sorgu);

            sorgu = "select " + ComboValue + ", " + ComboText + "  from [" + DxSource + "] where 1=1 ";

            if (!string.IsNullOrEmpty(ComboFilter))
                sorgu += " and " + ComboFilter + "='" + _obj.Comp + "'";
            sorgu += " and " + DxCascFilter + "='" + _val2par + "'";

            if (!string.IsNullOrEmpty(ComboOrder))
                sorgu += " order by " + ComboOrder;


            f.FillAspxCombobox(cmb, sorgu, ComboText, ComboValue);

            //Session["AddValPar2"] = " and " + DxCascFilter + "='" + _val2par + "'";


        }

        private void cmbDynamicCascade3(object sender, CallbackEventArgsBase e)
        {
            if (string.IsNullOrEmpty(e.Parameter)) return;
            if (!e.Parameter.Contains("#")) return;

            string[] PMarray = e.Parameter.Split('#');
            string[] PSArray = PMarray[1].Split(';');
            string _valpar = PMarray[0], _val2par = string.Empty;
            string DxSource = PSArray[0], ComboValue = PSArray[1], ComboText = PSArray[2]
                , ComboFilter = PSArray[3], ComboOrder = PSArray[4], DxCascFilter = PSArray[5]
                , MainSource = PSArray[6], MainValue = PSArray[7], MainFilter = PSArray[8];
            ASPxComboBox cmb = (ASPxComboBox)sender;
            string sorgu = string.Empty;


            sorgu = "select " + DxCascFilter + " from [" + MainSource + "] where 1=1 ";
            if (!string.IsNullOrEmpty(MainFilter))
                sorgu += " and " + MainFilter + "='" + _obj.Comp + "'";
            sorgu += " and " + MainValue + "='" + _valpar + "'";

            _val2par = f.tekbirsonucdonder(sorgu);

            sorgu = "select " + ComboValue + ", " + ComboText + "  from [" + DxSource + "] where 1=1 ";

            if (!string.IsNullOrEmpty(ComboFilter))
                sorgu += " and " + ComboFilter + "='" + _obj.Comp + "'";


            if (!string.IsNullOrEmpty(ComboOrder))
                sorgu += " order by " + ComboOrder;

            f.FillAspxCombobox(cmb, sorgu, ComboText, ComboValue);

            if (_val2par != "" & _val2par != "NULL")
            {
                cmb.SelectedItem = cmb.Items.FindByValue(_val2par);

            }



        }

        private void cmbDynamicCascade4(object sender, CallbackEventArgsBase e)
        {


            if (string.IsNullOrEmpty(e.Parameter)) return;
            if (!e.Parameter.Contains("#")) return;

            string[] PMarray = e.Parameter.Split('#');
            string[] PSArray = PMarray[1].Split(';');

            string[] Pfields = PSArray[1].Split('|'); //


            string _valpar = PMarray[0];

            string DxSource = PSArray[0],
                    ComboValue = PSArray[1],
                    ComboText = PSArray[2],
                    ComboFilter = PSArray[3],
                    ComboOrder = PSArray[4],
                    DxCascFilter = PSArray[5],
                    MainSource = PSArray[6],
                    MainValue = PSArray[7],
                    MainFilter = PSArray[8];



            ASPxComboBox cmb = (ASPxComboBox)sender;
            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = DxSource;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = conn;
                    string[] spParams = ComboFilter.Split(',');

                    cmd.Parameters.AddWithValue("PRM1", PMarray[0].ToString());
                    for (int i = 1; i < spParams.Length; i++)
                    {
                        //if (spParams[i].ToString() == "s.GetValue()")
                        //    cmd.Parameters.AddWithValue("PRM" + (i + 1), PMarray[0].ToString());
                        //else
                        //    cmd.Parameters.AddWithValue("PRM" + (i + 1), spParams[i].ToString());


                        if (spParams[i].ToString() == "P1")
                            cmd.Parameters.AddWithValue("PRM" + (i + 1), HiddenP1.Value);
                        else if (spParams[i].ToString() == "P2")
                            cmd.Parameters.AddWithValue("PRM" + (i + 1), HiddenP2.Value);
                        else
                        {
                            try
                            {
                                string[] transparams = txtCatchParamsAndValues.Text.Split('|');
                                string _xary = Array.Find(transparams, element => element.StartsWith(spParams[i].ToString(), StringComparison.Ordinal));

                                if (_xary.Length > 0)
                                    cmd.Parameters.AddWithValue("PRM" + (i + 1), _xary.Split(';')[1]);
                                else
                                    cmd.Parameters.AddWithValue("PRM" + (i + 1), spParams[i].ToString());
                            }
                            catch
                            {

                                cmd.Parameters.AddWithValue("PRM" + (i + 1), spParams[i].ToString());
                            }

                        }

                    }

                    DataTable dt = new DataTable();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dt);


                    cmb.Items.Clear();
                    cmb.DataSourceID = null;
                    cmb.DataSource = dt;
                    cmb.TextField = ComboText;
                    cmb.ValueField = ComboValue;
                    cmb.DataBind();


                }
            }



        }

        private void cmbDynamicCascade6(ASPxComboBox cmb, ASPxGridView xgrid, ASPxGridViewEditorEventArgs e)
        {
            string _params = cmb.NullText;
            if (string.IsNullOrEmpty(_params)) return;
            if (!_params.Contains("#")) return;

            string[] PMarray = _params.Split('#');

            string DxSource = PMarray[1],
                    ProcParams = PMarray[2],
                    ComboValue = PMarray[3],
                    ComboText = PMarray[4];


            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = conn;
                    string[] spParams = ProcParams.Split(',');

                    string _sqlquery = DxSource;
                    bool ilk = true;
                    for (int i = 0; i < spParams.Length; i++)
                    {
                        if (ilk)
                        {
                            if (spParams[i].ToString() == "P1")
                                _sqlquery += " '" + HiddenP1.Value + "'";
                            else if (spParams[i].ToString() == "P2")
                                _sqlquery += " '" + HiddenP2.Value + "'";
                            else if (spParams[i].ToString() == "P3")
                                _sqlquery += " '" + HiddenP3.Value + "'";
                            else if (spParams[i].ToString() == "P4")
                                _sqlquery += " '" + HiddenP4.Value + "'";
                            else if (spParams[i].ToString() == "USER")
                                _sqlquery += " '" + _obj.UserName + "'";
                            else if (spParams[i].ToString() == "COMP")
                                _sqlquery += " '" + _obj.Comp + "'";
                            else if (spParams[i].Substring(0, 1) == "$")
                                _sqlquery += " '" + spParams[i].Replace("$", "") + "'";
                            else
                            {
                                try
                                {
                                    string[] transparams = txtCatchParamsAndValues.Text.Split('|');
                                    string _xary = Array.Find(transparams, element => element.StartsWith(spParams[i].ToString(), StringComparison.Ordinal));
                                    if (_xary.Length > 0)
                                        _sqlquery += " '" + _xary.Split(';')[1] + "'";
                                    else
                                    {
                                        object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, spParams[i].ToString());
                                        if (_val == null) _val = string.Empty;
                                        _sqlquery += " '" + _val.ToString() + "'";
                                    }
                                }
                                catch
                                {

                                    object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, spParams[i].ToString());
                                    if (_val == null) _val = string.Empty;
                                    _sqlquery += " '" + _val.ToString() + "'";
                                }


                            }
                            ilk = false;
                        }
                        else
                        {
                            if (spParams[i].ToString() == "P1")
                                _sqlquery += ", '" + HiddenP1.Value + "'";
                            else if (spParams[i].ToString() == "P2")
                                _sqlquery += ", '" + HiddenP2.Value + "'";
                            else if (spParams[i].ToString() == "P3")
                                _sqlquery += ", '" + HiddenP3.Value + "'";
                            else if (spParams[i].ToString() == "P4")
                                _sqlquery += ", '" + HiddenP4.Value + "'";
                            else if (spParams[i].ToString() == "USER")
                                _sqlquery += ", '" + _obj.UserName + "'";
                            else if (spParams[i].ToString() == "COMP")
                                _sqlquery += ", '" + _obj.Comp + "'";
                            else if (spParams[i].Substring(0, 1) == "$")
                                _sqlquery += ", '" + spParams[i].Replace("$", "") + "'";
                            else
                            {
                                try
                                {
                                    string[] transparams = txtCatchParamsAndValues.Text.Split('|');
                                    string _xary = Array.Find(transparams, element => element.StartsWith(spParams[i].ToString(), StringComparison.Ordinal));
                                    if (_xary.Length > 0)
                                        _sqlquery += ", '" + _xary.Split(';')[1] + "'";
                                    else
                                    {
                                        object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, spParams[i].ToString());
                                        if (_val == null) _val = string.Empty;
                                        _sqlquery += ", '" + _val.ToString() + "'";
                                    }
                                }
                                catch
                                {

                                    object _val = xgrid.GetRowValuesByKeyValue(e.KeyValue, spParams[i].ToString());
                                    if (_val == null) _val = string.Empty;
                                    _sqlquery += ", '" + _val.ToString() + "'";
                                }


                            }



                        }



                    }
                    cmd.CommandText = _sqlquery;


                    using (DataTable dt = new DataTable())
                    {
                        using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                        {
                            adp.Fill(dt);

                            cmb.Items.Clear();
                            cmb.DataSourceID = null;
                            cmb.DataSource = dt;
                            cmb.TextField = ComboText;
                            cmb.ValueField = ComboValue;
                            cmb.DataBind();
                        }
                    }

                }
            }

        }
    }
}