﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="OnePageList7.aspx.cs" EnableViewState="true" Inherits="MSS1.OnePageList7" %>

<%@ Register Src="~/Controls/DataTableControlGAC.ascx" TagName="StDataTable" TagPrefix="STDT" %>

<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script>
        function OnGridFocusedRowChanged(s, e) {
            var link = $("#" + "<%= txtlink.ClientID %>").val();
            var linkfield = "<%= txtlinkField.ClientID %>";
            if (link == "JOB") {
                s.GetRowValues(grid.GetFocusedRowIndex(), $("#" + linkfield).val(), OnGetRowValuesJob);
            }
            else {
                s.GetRowValues(grid.GetFocusedRowIndex(), $("#" + linkfield).val(), OnGetRowValues);
            }
        }

        function OnGetRowValuesJob(values) {

            var splitter = values.split(';');
            var targetLink = "";
            if (splitter[1] == "1") {
                window.open("/Jobs/mainJobs.aspx?MJID=" + splitter[0], "_Blank");
            }
            else {
                window.open("/projeolustur.aspx?Tur=1&ProjeNo=" + splitter[0], "_Blank");
            }
        }

        function OnGetRowValues(values) {

            var linkFormat = "<%= txtlink.ClientID %>";
            var link = $("#" + linkFormat).val();

            link = link.replace("XXXXX", values[0]);
            link = link.replace("YYYYY", values[1]);

            window.open(AdGIdToLink(link), "_Blank");

        }

        function AdGIdToLink(_val) {
            if (_val.includes("GId=")) return _val;
            var ObjGId = "<%= lblGId.ClientID %>";
            if (_val.includes("?"))
                _val = _val + "&GId=" + $("#" + ObjGId).text();
            else
                _val = _val + "?GId=" + $("#" + ObjGId).text();

            return _val;

        }

        $(document).ready(function () {
            $("#total").val('2');
        });

        window. = function () {
            var _size = window.innerWidth;
            $('#ctl00_ContentPlaceHolder1_txtbrowserWidth').val(_size);
            alert($('#ctl00_ContentPlaceHolder1_txtbrowserWidth').val());
        };


    </script>
    <asp:Label ID="lblden" runat="server"></asp:Label>


    <table style="vertical-align: top">
        <tr>
            <td style="width: 25%">
                <dx:ASPxMenu ID="menu1" runat="server" AutoSeparators="RootOnly"
                    Theme="Glass"
                    ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                    ShowSubMenuShadow="False" OnItemClick="menu1_ItemClick">
                    <SubMenuStyle GutterWidth="0px" />
                    <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                    <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                    </SubMenuItemStyle>
                    <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                </dx:ASPxMenu>
                <dx:ASPxGridView ID="grid1" ClientInstanceName="grid" Caption="" runat="server" AutoGenerateColumns="False" EnableTheming="false"
                    Theme="Glass" DataSourceID="dtgrid1" KeyFieldName="ID" OnCellEditorInitialize="grid1_CellEditorInitialize"
                    OnHtmlRowPrepared="grid_HtmlRowPrepared" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                    OnCustomButtonCallback="grid_CustomButtonCallback" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                    OnRowInserting="grid1_RowInserting" OnRowUpdating="grid1_RowUpdating" OnRowDeleting="grid1_RowDeleting">
                    <Settings />
                    <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="True" />
                    <SettingsEditing Mode="Inline"></SettingsEditing>
                    <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                    <SettingsText EmptyDataRow=" " />
                    <SettingsCommandButton>
                        <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                        </CancelButton>
                        <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                        </UpdateButton>
                    </SettingsCommandButton>
                    <Styles>
                        <Header ForeColor="White"></Header>
                        <HeaderPanel ForeColor="White"></HeaderPanel>
                        <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                    </Styles>

                </dx:ASPxGridView>
                <dx:ASPxGridViewExporter ID="gridExport1" ExportedRowType="All" runat="server" GridViewID="grid"></dx:ASPxGridViewExporter>
            </td>
            <td style="width: 2px"></td>
            <td style="width: 49%; visibility: hidden">
                <dx:ASPxMenu ID="menu2" runat="server" AutoSeparators="RootOnly"
                    Theme="Glass"
                    ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                    ShowSubMenuShadow="False" OnItemClick="menu2_ItemClick">
                    <SubMenuStyle GutterWidth="0px" />
                    <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                    <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                    </SubMenuItemStyle>
                    <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                </dx:ASPxMenu>
                <dx:ASPxGridView ID="grid2" ClientInstanceName="grid" Caption="" runat="server" AutoGenerateColumns="False" Settings-HorizontalScrollBarMode="Auto"
                    Theme="Glass" EnableTheming="false" DataSourceID="dtgrid2" KeyFieldName="ID" OnCellEditorInitialize="grid2_CellEditorInitialize"
                    OnHtmlRowPrepared="grid_HtmlRowPrepared" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                    OnCustomButtonCallback="grid_CustomButtonCallback" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                    OnRowInserting="grid2_RowInserting" OnRowUpdating="grid2_RowUpdating" OnRowDeleting="grid2_RowDeleting">
                    <Settings />
                    <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="True" />
                    <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                    <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>

                    <SettingsText EmptyDataRow=" " />
                    <SettingsCommandButton>
                        <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                        </CancelButton>
                        <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                        </UpdateButton>
                    </SettingsCommandButton>
                    <Styles>
                        <Header ForeColor="White"></Header>
                        <HeaderPanel ForeColor="White"></HeaderPanel>
                        <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                    </Styles>

                </dx:ASPxGridView>
                <dx:ASPxGridViewExporter ID="gridExport2" ExportedRowType="All" runat="server" GridViewID="grid2"></dx:ASPxGridViewExporter>
            </td>
            <td style="width: 2px"></td>
            <td style="width: 25%">
                <dx:ASPxMenu ID="menu3" runat="server" AutoSeparators="RootOnly"
                    Theme="Glass"
                    ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                    ShowSubMenuShadow="False" OnItemClick="menu3_ItemClick">
                    <SubMenuStyle GutterWidth="0px" />
                    <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                    <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                    </SubMenuItemStyle>
                    <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                </dx:ASPxMenu>
                <dx:ASPxGridView ID="grid3" ClientInstanceName="grid" Caption="" runat="server" AutoGenerateColumns="False"
                    Theme="Glass" EnableTheming="false" DataSourceID="dtgrid3" KeyFieldName="ID" Settings-UseFixedTableLayout="true"
                    OnRowInserting="grid3_RowInserting" OnRowUpdating="grid3_RowUpdating" OnRowDeleting="grid3_RowDeleting"
                    OnCustomButtonCallback="grid_CustomButtonCallback" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                    OnHtmlRowPrepared="grid_HtmlRowPrepared" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared" OnCellEditorInitialize="grid3_CellEditorInitialize">
                    <Settings />
                    <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="True" />
                    <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                    <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                    <SettingsCommandButton>
                        <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                        </CancelButton>
                        <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                        </UpdateButton>
                    </SettingsCommandButton>
                    <Styles>
                        <Header ForeColor="White"></Header>
                        <HeaderPanel ForeColor="White"></HeaderPanel>
                        <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                    </Styles>
                    <Columns>
                        <dx:GridViewDataColumn FieldName="ID" Visible="false" ReadOnly="true"></dx:GridViewDataColumn>
                    </Columns>

                </dx:ASPxGridView>
                <dx:ASPxGridViewExporter ID="gridExport3" ExportedRowType="All" runat="server" GridViewID="grid3"></dx:ASPxGridViewExporter>
            </td>
        </tr>

    </table>

    <STDT:StDataTable ID="dtgrid1" runat="server" />
    <STDT:StDataTable ID="dtgrid2" runat="server" />
    <STDT:StDataTable ID="dtgrid3" runat="server" />

    <STDT:StDataTable ID="DS11" runat="server" />
    <STDT:StDataTable ID="DS12" runat="server" />
    <STDT:StDataTable ID="DS13" runat="server" />
    <STDT:StDataTable ID="DS14" runat="server" />
    <STDT:StDataTable ID="DS15" runat="server" />
    <STDT:StDataTable ID="DS16" runat="server" />
    <STDT:StDataTable ID="DS17" runat="server" />
    <STDT:StDataTable ID="DS18" runat="server" />
    <STDT:StDataTable ID="DS19" runat="server" />
    <STDT:StDataTable ID="DS110" runat="server" />
    <STDT:StDataTable ID="DS111" runat="server" />
    <STDT:StDataTable ID="DS112" runat="server" />
    <STDT:StDataTable ID="DS113" runat="server" />
    <STDT:StDataTable ID="DS114" runat="server" />
    <STDT:StDataTable ID="DS115" runat="server" />
    <STDT:StDataTable ID="DS116" runat="server" />
    <STDT:StDataTable ID="DS117" runat="server" />
    <STDT:StDataTable ID="DS118" runat="server" />
    <STDT:StDataTable ID="DS119" runat="server" />
    <STDT:StDataTable ID="DS120" runat="server" />

    <STDT:StDataTable ID="DS21" runat="server" />
    <STDT:StDataTable ID="DS22" runat="server" />
    <STDT:StDataTable ID="DS23" runat="server" />
    <STDT:StDataTable ID="DS24" runat="server" />
    <STDT:StDataTable ID="DS25" runat="server" />
    <STDT:StDataTable ID="DS26" runat="server" />
    <STDT:StDataTable ID="DS27" runat="server" />
    <STDT:StDataTable ID="DS28" runat="server" />
    <STDT:StDataTable ID="DS29" runat="server" />
    <STDT:StDataTable ID="DS210" runat="server" />
    <STDT:StDataTable ID="DS211" runat="server" />
    <STDT:StDataTable ID="DS212" runat="server" />
    <STDT:StDataTable ID="DS213" runat="server" />
    <STDT:StDataTable ID="DS214" runat="server" />
    <STDT:StDataTable ID="DS215" runat="server" />
    <STDT:StDataTable ID="DS216" runat="server" />
    <STDT:StDataTable ID="DS217" runat="server" />
    <STDT:StDataTable ID="DS218" runat="server" />
    <STDT:StDataTable ID="DS219" runat="server" />
    <STDT:StDataTable ID="DS220" runat="server" />

    <div style="display: none">
        <asp:Label ID="lblGId" runat="server"></asp:Label>
        <asp:TextBox ID="txtlink" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams2" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams2" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams2" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams3" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams3" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams3" runat="server"></asp:TextBox>
    </div>
</asp:Content>

