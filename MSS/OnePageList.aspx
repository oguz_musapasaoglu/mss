﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" EnableViewState="true"
    CodeBehind="OnePageList.aspx.cs" Inherits="MSS1.OnePageList" %>

<%@ Register Src="~/controls/DataTableControlGAC.ascx" TagName="StDataTable" TagPrefix="STDT" %>

<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        iframe {
            -moz-transform: scale(0.99, 0.99);
            -webkit-transform: scale(0.99, 0.99);
            -o-transform: scale(0.99, 0.99);
            -ms-transform: scale(0.99, 0.99);
            transform: scale(0.99, 0.99);
            -moz-transform-origin: top center;
            -webkit-transform-origin: top center;
            -o-transform-origin: top center;
            -ms-transform-origin: top center;
            transform-origin: top center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript"> 

        function ShowModalPopup() {
            callbackKTFMessage.PerformCallback();
            KTFCreateInvoicePopup.Show();
        }

        function ShowModalDatePopup() {
            DatePopup.Show();
        }

        function HideModalPopup(s, e) {
            ConfirmMessage(s, e);
            KTFCreateInvoicePopup.Hide();
        }

        function HideModalDatePopup(s, e) {
            DatePopup.Hide();
        }

        function ConfirmMessage(s, e) {
            var ktfMessage = document.getElementById('ctl00_ContentPlaceHolder1_callbackKTFMessage_txtKTFmessage_I').value;
            e.processOnServer = confirm(ktfMessage);
        }

        function OnGridFocusedRowChanged(s, e, linkType) {

            var link = $("#" + "<%= txtlink1.ClientID %>").val();
            var linkfield = "<%= txtlinkField1.ClientID %>";
            switch (linkType) {
                case "1":
                    link = $("#" + "<%= txtlink1.ClientID %>").val();
                    linkfield = "<%= txtlinkField1.ClientID %>";
                    break;
                case "2":
                    link = $("#" + "<%= txtlink2.ClientID %>").val();
                    linkfield = "<%= txtlinkField2.ClientID %>";
                    break;
                case "3":
                    link = $("#" + "<%= txtlink3.ClientID %>").val();
                    linkfield = "<%= txtlinkField3.ClientID %>";
                    break;
                case "4":
                    link = $("#" + "<%= txtlink4.ClientID %>").val();
                    linkfield = "<%= txtlinkField4.ClientID %>";
                    break;
                case "5":
                    link = $("#" + "<%= txtlink5.ClientID %>").val();
                    linkfield = "<%= txtlinkField5.ClientID %>";
                    break;
                case "6":
                    link = $("#" + "<%= txtlink6.ClientID %>").val();
                    linkfield = "<%= txtlinkField6.ClientID %>";
                    break;

            }

            if (link == "JOB") {
                s.GetRowValues(s.GetFocusedRowIndex(), $("#" + linkfield).val(), OnGetRowValuesJob);
            }
            else if (link == "FILTER") {
                s.GetRowValues(s.GetFocusedRowIndex(), $("#" + linkfield).val(), OnGetRowValuesFilter);
<%--                var transferredval = $("#" + "<%= txtCatchParameters1.ClientID %>").val();
                if (transferredval.length > 0) {
                    $("#" + "<%= txtCatchValues1.ClientID %>").val('0');
                    $("#" + "<%= txtCatchParameters1.ClientID %>").val().split(";").forEach(function (item) {
                        if (item == 'REDIT') {
                            s.GetRowValues(s.GetFocusedRowIndex(), item, OnGetRowParameters);
                        }
                    });
                }--%>
            }
            else {
                $("#" + "<%= txtlinkType.ClientID %>").val(linkType);
                s.GetRowValues(s.GetFocusedRowIndex(), $("#" + linkfield).val(), OnGetRowValues);
            }


        }

        function StatuFilter(values) {
            var splitter = values.toString().split(",");
            document.getElementById('ctl00_ContentPlaceHolder1_HiddenMainStatu').value = splitter[0];

        }

        function OnGetRowValues(values) {
            var linkType = $("#" + "<%= txtlinkType.ClientID %>").val();
            var linkFormat = "<%= txtlink1.ClientID %>";

            if (linkType == "1")
                linkFormat = "<%= txtlink1.ClientID %>";
            else if (linkType == "2")
                linkFormat = "<%= txtlink2.ClientID %>";
            else if (linkType == "3")
                linkFormat = "<%= txtlink3.ClientID %>";
            else if (linkType == "4")
                linkFormat = "<%= txtlink4.ClientID %>";
            else if (linkType == "5")
                linkFormat = "<%= txtlink5.ClientID %>";
            else if (linkType == "6")
                linkFormat = "<%= txtlink6.ClientID %>";
            var link = $("#" + linkFormat).val();
            link = link.replace("XXXXX", values);
            if (link != '') {
                window.location.href = AdGIdToLink(link);
                window.focus();
            }

        }

        function AdGIdToLink(_val) {
            if (_val.includes("GId=")) return _val;
            var ObjGId = "<%= lblGId.ClientID %>";
            if (_val.includes("?"))
                _val = _val + "&GId=" + $("#" + ObjGId).text();
            else
                _val = _val + "?GId=" + $("#" + ObjGId).text();

            return _val;

        }
        function OnGetRowValuesPopup(values) {

           <%-- var linkFormat = "<%= txtlink.ClientID %>";
             var link = $("#" + linkFormat).val();
             link = link.replace("XXXXX", values[0]);
             link = link.replace("YYYYY", values[1]);
             window.location.href = link;
             window.focus();--%>
        }

        function OnGetRowValuesFilter(values) {

            var queryText = "";
            var linkFormat = "<%= txtlinkField1.ClientID %>";
            var link = $("#" + linkFormat).val();
            var filterFields = link.split(';');

            if (filterFields.length == 1) {
                queryText = filterFields[0] + " = '" + values + "'";
            }
            else if (filterFields.length == 2) {
                queryText = filterFields[0] + " = '" + values[0] + "' and " + filterFields[1] + " = '" + values[1] + "'";
            }
            else if (filterFields.length == 3) {
                queryText = filterFields[0] + " = '" + values[0] + "' and " + filterFields[1] + " = '" + values[1] + " and " + filterFields[2] + " = '" + values[2] + "'";
            }
            else if (filterFields.length == 4) {
                queryText = filterFields[0] + " = '" + values[0] + "' and " + filterFields[1] + " = '" + values[1] + " and " + filterFields[2] + " = '" + values[2] + "' and " + filterFields[3] + " = '" + values[3] + "'";
            }



            rpCargoDetails.SetCollapsed(true);
            rpCostSales.SetCollapsed(true);
            rpTasks.SetCollapsed(true);
            rpMain.SetCollapsed(false);


            document.getElementById('ctl00_ContentPlaceHolder1_HiddenLoadqueryText').value = queryText;
            var splitter = values.toString().split(",");

            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value = '';
            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP2').value = '';

            if (splitter.length == 1) {
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value = splitter[0];

            }
            else if (splitter.length == 2) {
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value = splitter[0];
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP2').value = splitter[1];
            }

            var _link = document.getElementById('ctl00_ContentPlaceHolder1_HiddenpageType').value.replace("XXXXX", splitter[0]);
            _link = _link.replace("YYYYY", splitter[1]);
            _link = _link.replace("undefined", "TOTAL");
            document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rpMain_iframeView').src = _link;

            var pane = xsplitter.GetPaneByName('MiddleContainer');
            if (pane.IsCollapsed())
                document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_1_S_CB_Img').click();


            var sendValue = "<%= txtValues.ClientID %>";
            $("#" + sendValue).val(values);

        }

        function OnRefreshAllObjects(s, e) {
            if (s.cpIsUpdated) {
                try { grid1.PerformCallback(''); } catch (err) { }
                try { grid2.PerformCallback(''); } catch (err) { }
                try { grid3.PerformCallback(''); } catch (err) { }
                try { grid6.PerformCallback(''); } catch (err) { }
                document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rpMain_iframeView').src = AdGIdToLink(document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rpMain_iframeView').src);
            }
        }



        function OnGetRowValuesJob(values) {
            //alert(values);
            var splitter = values.split(';');
            var targetLink = "";
            if (splitter[1] == "1") {
                window.open("/Jobs/mainJobs.aspx?MJID=" + splitter[0]);
            }
            else {

                window.open("/projeolustur.aspx?Tur=1&ProjeNo=" + splitter[0]);
            }
        }


        var postponedCallbackRequired = false;
        function OnListBoxIndexChanged(values) {
            if (iframecallbackPanel.InCallback())
                postponedCallbackRequired = true;
            else
                iframecallbackPanel.PerformCallback(values);
        }
        function OnEndCallback(values) {
            if (postponedCallbackRequired) {
                iframecallbackPanel.PerformCallback(values);
                postponedCallbackRequired = false;
            }
        }

        function OpenDirectLink(link) {
            var P1 = document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value;
            var P2 = document.getElementById('ctl00_ContentPlaceHolder1_HiddenP2').value;
            link = link.replace("XXXXX", P1);
            link = link.replace("YYYYY", P2);
            var popwin = window.open(link, '_blank');
            popwin.focus();
        }

        function OnEditorValueChanged(s, e) {
            var r23 = grid.GetEditValue("R0023");
            var r28 = grid.GetEditValue("R0028");
            grid.SetEditValue("R0029", grid.GetEditValue("R0023") * grid.GetEditValue("R0028"));
        }

    </script>
    <asp:HiddenField runat="server" ID="HiddenLoadqueryText" Value="AA" />
    <asp:HiddenField runat="server" ID="HiddenP1" Value="" />
    <asp:HiddenField runat="server" ID="HiddenP2" Value="" />
    <asp:HiddenField runat="server" ID="HiddenMainStatu" Value="" />
    <asp:HiddenField runat="server" ID="HiddenpageType" Value="" />
    <asp:Label ID="lblden" runat="server"></asp:Label>
    <dx:ASPxSplitter Theme="iOS" ID="ASPxSplitter1" ClientInstanceName="xsplitter" Height="650px" runat="server" Width="99%">
        <Panes>
            <dx:SplitterPane ShowCollapseBackwardButton="True" ShowSeparatorImage="True" Size="25%" Name="LeftContainer">
                <Panes>
                    <dx:SplitterPane ShowCollapseBackwardButton="True" ShowSeparatorImage="True" Size="80%" Name="Leftinside">
                        <Panes>
                            <dx:SplitterPane Size="90%" ScrollBars="Auto">
                                <ContentCollection>
                                    <dx:SplitterContentControl ID="SPContent" runat="server">
                                        <table>
                                            <tr>
                                                <td style="width: 100%; height: 100%">
                                                    <dx:ASPxRadioButtonList RepeatDirection="Horizontal" Theme="Glass" runat="server" Width="100%" ID="RdList"
                                                        OnSelectedIndexChanged="RdList_SelectedIndexChanged" AutoPostBack="true">
                                                        <Items>
                                                            <dx:ListEditItem Text="VOYAGE" Value="A" />
                                                            <dx:ListEditItem Text="PORT" Value="B" />

                                                        </Items>
                                                    </dx:ASPxRadioButtonList>
                                                </td>
                                            </tr>
                                        </table>
                                        <dx:ASPxMenu ID="menu1" runat="server" AutoSeparators="RootOnly"
                                            Theme="Glass"
                                            ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                            ShowSubMenuShadow="False" OnItemClick="menu1_ItemClick">
                                            <SubMenuStyle GutterWidth="0px" />
                                            <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                            <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                            </SubMenuItemStyle>
                                            <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                        </dx:ASPxMenu>
                                        <dx:ASPxGridView ID="grid1" ClientInstanceName="grid1" Caption="" runat="server" AutoGenerateColumns="false"
                                            Theme="Glass" EnableTheming="false" DataSourceID="dtgrid1" KeyFieldName="ID"
                                            OnCellEditorInitialize="grid1_CellEditorInitialize" EnableRowsCache="false"
                                            Settings-HorizontalScrollBarMode="Auto"
                                            OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnRowDeleting="grid_RowDeleting">

                                            <ClientSideEvents RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e);} " />
                                            <Settings />
                                            <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" AllowFocusedRow="true" />
                                            <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                            <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                                            <SettingsCommandButton>
                                                <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                </CancelButton>
                                                <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                </UpdateButton>
                                            </SettingsCommandButton>
                                            <SettingsText EmptyDataRow=" " />
                                            <Styles>
                                                <Cell Paddings-Padding="1"></Cell>
                                                <Header ForeColor="White"></Header>
                                                <HeaderPanel ForeColor="White"></HeaderPanel>
                                                <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>

                                            </Styles>
                                            <SettingsText EmptyDataRow=" " />
                                        </dx:ASPxGridView>

                                        <dx:ASPxGridViewExporter ID="gridExport1" ExportedRowType="All" runat="server" GridViewID="grid1"></dx:ASPxGridViewExporter>
                                    </dx:SplitterContentControl>
                                </ContentCollection>
                            </dx:SplitterPane>
                        </Panes>
                    </dx:SplitterPane>
                </Panes>
            </dx:SplitterPane>
            <dx:SplitterPane ShowCollapseForwardButton="True" ShowSeparatorImage="True" Size="65%" Name="MiddleContainer" ScrollBars="Auto">
                <Panes>
                    <dx:SplitterPane ShowCollapseBackwardButton="True" ShowSeparatorImage="True" Size="80%" Name="Middleinside">
                        <ContentCollection>
                            <dx:SplitterContentControl>
                                <dx:ASPxRoundPanel ID="rpMain" Theme="Glass" Collapsed="false" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rpMain" HeaderText="VOYAGE"
                                    runat="server" Width="100%" Height="60%">
                                    <PanelCollection>
                                        <dx:PanelContent>
                                            <%--<dx:ASPxCallbackPanel ID="iframecallbackPanel" ClientInstanceName="iframecallbackPanel" Height="100%"
                                                runat="server" OnCallback="iframecallbackPanel_Callback">
                                                <PanelCollection>
                                                    <dx:PanelContent>--%>
                                            <iframe id="iframeView" width="100%" height="100%" runat="server"></iframe>
                                            <%-- </dx:PanelContent>
                                                </PanelCollection>
                                            </dx:ASPxCallbackPanel>--%>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){  
                                            if(rpMain.GetCollapsed()){                                                  
                                                document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rpMain_iframeView').style.display = 'none';
                                            }
                                            else {
                                               document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rpMain_iframeView').style.display = 'block';
                                          }
                                        }" />
                                </dx:ASPxRoundPanel>

                                <dx:ASPxRoundPanel ID="rpTasks" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rpTasks" HeaderText="PORTS" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent SupportsDisabledAttribute="True" runat="server">

                                            <dx:ASPxMenu ID="menu6" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu6_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid6" ClientInstanceName="grid6" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid6_CustomCallback" OnCellEditorInitialize="grid6_CellEditorInitialize" Width="100%"
                                                EnableTheming="false" DataSourceID="dtgrid6" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'6');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false"
                                                    AllowFocusedRow="false" />
                                                <SettingsPager PageSize="7" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>

                                                </SettingsCommandButton>
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <Styles>
                                                    <Cell Paddings-Padding="1"></Cell>

                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                                <Columns>
                                                    <dx:GridViewDataColumn Width="100px" Caption="Temp1R0030" Visible="true" Name="R0030" FieldName="R0030" VisibleIndex="1">
                                                        <EditItemTemplate>
                                                            <dx:ASPxComboBox ID="cmbR0030" Value='<%# Eval("R0030")%>' DropDownStyle="DropDownList"
                                                                Theme="Glass" Width="100%" runat="server" EnableCallbackMode="true" CallbackPageSize="20">
                                                            </dx:ASPxComboBox>
                                                        </EditItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Width="100px" Caption="Temp1R0032" Visible="true" Name="R0032" FieldName="R0032" VisibleIndex="2">
                                                        <EditItemTemplate>
                                                            <dx:ASPxComboBox ID="cmbR0032" Value='<%# Eval("R0032")%>' DropDownStyle="DropDownList"
                                                                Theme="Glass" Width="100%" runat="server" EnableCallbackMode="true" CallbackPageSize="20">
                                                            </dx:ASPxComboBox>
                                                        </EditItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Width="100px" Caption="Temp1R0034" Visible="true" Name="R0034" FieldName="R0034" VisibleIndex="3">
                                                        <EditItemTemplate>
                                                            <dx:ASPxComboBox ID="cmbR0034" Value='<%# Eval("R0034")%>' DropDownStyle="DropDownList"
                                                                Theme="Glass" Width="100%" runat="server" EnableCallbackMode="true" CallbackPageSize="20">
                                                            </dx:ASPxComboBox>
                                                        </EditItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Width="100px" Caption="Temp1R0036" Visible="true" Name="R0036" FieldName="R0036" VisibleIndex="4">
                                                        <EditItemTemplate>
                                                            <dx:ASPxComboBox ID="cmbR0036" Value='<%# Eval("R0036")%>' DropDownStyle="DropDownList"
                                                                Theme="Glass" Width="100%" runat="server" EnableCallbackMode="true" CallbackPageSize="20">
                                                            </dx:ASPxComboBox>
                                                        </EditItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Width="100px" Caption="Temp1R0028" Visible="true" Name="R0028" FieldName="R0028" VisibleIndex="5">
                                                        <EditItemTemplate>
                                                            <dx:ASPxComboBox ID="cmbR0028" Value='<%# Eval("R0028")%>' DropDownStyle="DropDownList"
                                                                Theme="Glass" Width="100%" runat="server" EnableCallbackMode="true" CallbackPageSize="20">
                                                            </dx:ASPxComboBox>
                                                        </EditItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                </Columns>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport6" ExportedRowType="All" runat="server" GridViewID="grid6"></dx:ASPxGridViewExporter>

                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){
                                            txttask.SetText(rpTasks.GetCollapsed());
                                            if(!rpTasks.GetCollapsed()){   
                                                 grid6.PerformCallback(document.getElementById('ctl00_ContentPlaceHolder1_HiddenLoadqueryText').value);
                                            } 
                                        }" />
                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rpCargoDetails" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rpCargoDetails" HeaderText="CONTAINER TRANSACTIONS" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent>

                                            <dx:ASPxButton ID="ktfFatura" runat="server" Text="FATURA OLUŞTUR" AutoPostBack="false">
                                                <ClientSideEvents Click="function(s, e) { ShowModalPopup(); }" />
                                            </dx:ASPxButton>
                                            <dx:ASPxMenu ID="menu2" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu2_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid2" ClientInstanceName="grid2" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid2_CustomCallback" OnCellEditorInitialize="grid2_CellEditorInitialize"
                                                EnableTheming="false" DataSourceID="dtgrid2" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'2');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" />
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport2" ExportedRowType="All" runat="server" GridViewID="grid2"></dx:ASPxGridViewExporter>

                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){ 
                                            HiddenCargoCollepseStatu.SetText(rpCargoDetails.GetCollapsed());
                                            if(!rpCargoDetails.GetCollapsed()){
                                                grid2.PerformCallback(document.getElementById('ctl00_ContentPlaceHolder1_HiddenLoadqueryText').value);
                                            }
                                        }" />
                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rpCostSales" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rpCostSales" HeaderText="INVOICES" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent>


                                            <dx:ASPxMenu ID="menu3" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu3_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid3" ClientInstanceName="grid3" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid3_CustomCallback" OnCellEditorInitialize="grid3_CellEditorInitialize"
                                                EnableTheming="false" DataSourceID="dtgrid3" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'3');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" />
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport3" ExportedRowType="All" runat="server" GridViewID="grid3"></dx:ASPxGridViewExporter>

                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){ 
                                            HiddenCostCollepseStatu.SetText(rpCostSales.GetCollapsed());
                                            if(!rpCostSales.GetCollapsed()){
                                                grid3.PerformCallback(document.getElementById('ctl00_ContentPlaceHolder1_HiddenLoadqueryText').value);
                                            }
                                        }" />

                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rpComments" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rpComments" HeaderText="COMMENTS" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent>

                                            <dx:ASPxMenu ID="menu5" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu5_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid5" ClientInstanceName="grid5" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid5_CustomCallback" OnCellEditorInitialize="grid5_CellEditorInitialize"
                                                EnableTheming="false" DataSourceID="dtgrid5" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'5');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" AllowFocusedRow="false" />
                                                <SettingsPager PageSize="7" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <Styles>


                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport5" ExportedRowType="All" runat="server" GridViewID="grid5"></dx:ASPxGridViewExporter>

                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){ 
                                            HiddenCommentCollepseStatu.SetText(rpComments.GetCollapsed());
                                            if(!rpComments.GetCollapsed()){
                                                grid5.PerformCallback(document.getElementById('ctl00_ContentPlaceHolder1_HiddenLoadqueryText').value);
                                            }
                                        }" />
                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rpDocuments" Theme="Glass" Visible="false" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true" ClientInstanceName="rpDocuments" HeaderText="DOCUMENTS" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent>

                                            <dx:ASPxMenu ID="menu4" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu4_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid4" ClientInstanceName="grid4" Caption="" runat="server" AutoGenerateColumns="False"
                                                EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid4_CustomCallback"
                                                EnableTheming="false" DataSourceID="dtgrid4" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'4');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" AllowFocusedRow="false" />
                                                <SettingsPager PageSize="7" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport4" ExportedRowType="All" runat="server" GridViewID="grid4"></dx:ASPxGridViewExporter>

                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />

                                    <ClientSideEvents CollapsedChanged="function(s,e){ 
                                            HiddenDocumentCollepseStatu.SetText(rpDocuments.GetCollapsed());
                                            if(!rpDocuments.GetCollapsed()){
                                                grid4.PerformCallback(document.getElementById('ctl00_ContentPlaceHolder1_HiddenLoadqueryText').value);
                                            }
                                        }" />
                                </dx:ASPxRoundPanel>
                            </dx:SplitterContentControl>
                        </ContentCollection>
                    </dx:SplitterPane>
                </Panes>
            </dx:SplitterPane>

        </Panes>
    </dx:ASPxSplitter>
    <STDT:StDataTable ID="dtgrid1" runat="server" />
    <STDT:StDataTable ID="dtgrid2" runat="server" />
    <STDT:StDataTable ID="dtgrid3" runat="server" />
    <STDT:StDataTable ID="dtgrid4" runat="server" />
    <STDT:StDataTable ID="dtgrid5" runat="server" />
    <STDT:StDataTable ID="dtgrid6" runat="server" />
    <STDT:StDataTable ID="CmbS1" runat="server" />
    <STDT:StDataTable ID="CmbS2" runat="server" />
    <STDT:StDataTable ID="CmbS3" runat="server" />
    <STDT:StDataTable ID="CmbS4" runat="server" />
    <STDT:StDataTable ID="CmbS5" runat="server" />

    <STDT:StDataTable ID="DS11" runat="server" />
    <STDT:StDataTable ID="DS12" runat="server" />
    <STDT:StDataTable ID="DS13" runat="server" />
    <STDT:StDataTable ID="DS14" runat="server" />
    <STDT:StDataTable ID="DS15" runat="server" />
    <STDT:StDataTable ID="DS16" runat="server" />
    <STDT:StDataTable ID="DS17" runat="server" />
    <STDT:StDataTable ID="DS18" runat="server" />
    <STDT:StDataTable ID="DS19" runat="server" />
    <STDT:StDataTable ID="DS110" runat="server" />
    <STDT:StDataTable ID="DS111" runat="server" />
    <STDT:StDataTable ID="DS112" runat="server" />
    <STDT:StDataTable ID="DS113" runat="server" />
    <STDT:StDataTable ID="DS114" runat="server" />
    <STDT:StDataTable ID="DS115" runat="server" />
    <STDT:StDataTable ID="DS116" runat="server" />
    <STDT:StDataTable ID="DS117" runat="server" />
    <STDT:StDataTable ID="DS118" runat="server" />
    <STDT:StDataTable ID="DS119" runat="server" />
    <STDT:StDataTable ID="DS120" runat="server" />

    <STDT:StDataTable ID="DS21" runat="server" />
    <STDT:StDataTable ID="DS22" runat="server" />
    <STDT:StDataTable ID="DS23" runat="server" />
    <STDT:StDataTable ID="DS24" runat="server" />
    <STDT:StDataTable ID="DS25" runat="server" />
    <STDT:StDataTable ID="DS26" runat="server" />
    <STDT:StDataTable ID="DS27" runat="server" />
    <STDT:StDataTable ID="DS28" runat="server" />
    <STDT:StDataTable ID="DS29" runat="server" />
    <STDT:StDataTable ID="DS210" runat="server" />
    <STDT:StDataTable ID="DS211" runat="server" />
    <STDT:StDataTable ID="DS212" runat="server" />
    <STDT:StDataTable ID="DS213" runat="server" />
    <STDT:StDataTable ID="DS214" runat="server" />
    <STDT:StDataTable ID="DS215" runat="server" />
    <STDT:StDataTable ID="DS216" runat="server" />
    <STDT:StDataTable ID="DS217" runat="server" />
    <STDT:StDataTable ID="DS218" runat="server" />
    <STDT:StDataTable ID="DS219" runat="server" />
    <STDT:StDataTable ID="DS220" runat="server" />


    <STDT:StDataTable ID="DS31" runat="server" />
    <STDT:StDataTable ID="DS32" runat="server" />
    <STDT:StDataTable ID="DS33" runat="server" />
    <STDT:StDataTable ID="DS34" runat="server" />
    <STDT:StDataTable ID="DS35" runat="server" />
    <STDT:StDataTable ID="DS36" runat="server" />
    <STDT:StDataTable ID="DS37" runat="server" />
    <STDT:StDataTable ID="DS38" runat="server" />
    <STDT:StDataTable ID="DS39" runat="server" />
    <STDT:StDataTable ID="DS310" runat="server" />
    <STDT:StDataTable ID="DS311" runat="server" />
    <STDT:StDataTable ID="DS312" runat="server" />
    <STDT:StDataTable ID="DS313" runat="server" />
    <STDT:StDataTable ID="DS314" runat="server" />
    <STDT:StDataTable ID="DS315" runat="server" />
    <STDT:StDataTable ID="DS316" runat="server" />
    <STDT:StDataTable ID="DS317" runat="server" />
    <STDT:StDataTable ID="DS318" runat="server" />
    <STDT:StDataTable ID="DS319" runat="server" />
    <STDT:StDataTable ID="DS320" runat="server" />

    <STDT:StDataTable ID="DS41" runat="server" />
    <STDT:StDataTable ID="DS42" runat="server" />
    <STDT:StDataTable ID="DS43" runat="server" />
    <STDT:StDataTable ID="DS44" runat="server" />
    <STDT:StDataTable ID="DS45" runat="server" />
    <STDT:StDataTable ID="DS46" runat="server" />
    <STDT:StDataTable ID="DS47" runat="server" />
    <STDT:StDataTable ID="DS48" runat="server" />
    <STDT:StDataTable ID="DS49" runat="server" />
    <STDT:StDataTable ID="DS410" runat="server" />
    <STDT:StDataTable ID="DS411" runat="server" />
    <STDT:StDataTable ID="DS412" runat="server" />
    <STDT:StDataTable ID="DS413" runat="server" />
    <STDT:StDataTable ID="DS414" runat="server" />
    <STDT:StDataTable ID="DS415" runat="server" />
    <STDT:StDataTable ID="DS416" runat="server" />
    <STDT:StDataTable ID="DS417" runat="server" />
    <STDT:StDataTable ID="DS418" runat="server" />
    <STDT:StDataTable ID="DS419" runat="server" />
    <STDT:StDataTable ID="DS420" runat="server" />

    <STDT:StDataTable ID="DS51" runat="server" />
    <STDT:StDataTable ID="DS52" runat="server" />
    <STDT:StDataTable ID="DS53" runat="server" />
    <STDT:StDataTable ID="DS54" runat="server" />
    <STDT:StDataTable ID="DS55" runat="server" />
    <STDT:StDataTable ID="DS56" runat="server" />
    <STDT:StDataTable ID="DS57" runat="server" />
    <STDT:StDataTable ID="DS58" runat="server" />
    <STDT:StDataTable ID="DS59" runat="server" />
    <STDT:StDataTable ID="DS510" runat="server" />
    <STDT:StDataTable ID="DS511" runat="server" />
    <STDT:StDataTable ID="DS512" runat="server" />
    <STDT:StDataTable ID="DS513" runat="server" />
    <STDT:StDataTable ID="DS514" runat="server" />
    <STDT:StDataTable ID="DS515" runat="server" />
    <STDT:StDataTable ID="DS516" runat="server" />
    <STDT:StDataTable ID="DS517" runat="server" />
    <STDT:StDataTable ID="DS518" runat="server" />
    <STDT:StDataTable ID="DS519" runat="server" />
    <STDT:StDataTable ID="DS520" runat="server" />

    <STDT:StDataTable ID="DS61" runat="server" />
    <STDT:StDataTable ID="DS62" runat="server" />
    <STDT:StDataTable ID="DS63" runat="server" />
    <STDT:StDataTable ID="DS64" runat="server" />
    <STDT:StDataTable ID="DS65" runat="server" />
    <STDT:StDataTable ID="DS66" runat="server" />
    <STDT:StDataTable ID="DS67" runat="server" />
    <STDT:StDataTable ID="DS68" runat="server" />
    <STDT:StDataTable ID="DS69" runat="server" />
    <STDT:StDataTable ID="DS610" runat="server" />
    <STDT:StDataTable ID="DS611" runat="server" />
    <STDT:StDataTable ID="DS612" runat="server" />
    <STDT:StDataTable ID="DS613" runat="server" />
    <STDT:StDataTable ID="DS614" runat="server" />
    <STDT:StDataTable ID="DS615" runat="server" />
    <STDT:StDataTable ID="DS616" runat="server" />
    <STDT:StDataTable ID="DS617" runat="server" />
    <STDT:StDataTable ID="DS618" runat="server" />
    <STDT:StDataTable ID="DS619" runat="server" />
    <STDT:StDataTable ID="DS620" runat="server" />


    <asp:SqlDataSource ID="dsVW0044" runat="server"
        SelectCommand="select * from VW0044"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>"></asp:SqlDataSource>
    <div style="display: none">
        <asp:HiddenField runat="server" ID="HiddendirectParam" />
        <asp:HiddenField runat="server" ID="HiddenField1" Value="AA" />
        <asp:HiddenField runat="server" ID="HiddenField2" Value="" />
        <asp:HiddenField runat="server" ID="HiddenField3" Value="" />
        <asp:HiddenField runat="server" ID="HiddenField4" Value="" />
        <asp:HiddenField runat="server" ID="HiddenField5" Value="" />

        <dx:ASPxHyperLink runat="server" ID="HiddenTaskCollepseStatu" ClientInstanceName="HiddenTaskCollepseStatu" Text="Test" />
        <dx:ASPxHyperLink runat="server" ID="HiddenCargoCollepseStatu" ClientInstanceName="HiddenCargoCollepseStatu" Text="Test" />
        <dx:ASPxHyperLink runat="server" ID="HiddenCostCollepseStatu" ClientInstanceName="HiddenCostCollepseStatu" Text="Test" />
        <dx:ASPxHyperLink runat="server" ID="HiddenCommentCollepseStatu" ClientInstanceName="HiddenCommentCollepseStatu" Text="Test" />
        <dx:ASPxHyperLink runat="server" ID="HiddenDocumentCollepseStatu" ClientInstanceName="HiddenDocumentCollepseStatu" Text="Test" />
        <dx:ASPxButton ID="btnhat" runat="server" OnClick="btnhat_Click"></dx:ASPxButton>

        <asp:Label ID="lblGId" runat="server"></asp:Label>
        <asp:TextBox ID="txtlink1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink2" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField2" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink3" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField3" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink4" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField4" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink5" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField5" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink6" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField6" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkType" runat="server"></asp:TextBox>

        <asp:TextBox ID="txtFilterText" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams2" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams2" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams2" runat="server"></asp:TextBox>

        <asp:TextBox ID="txtinsertparams3" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams3" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams3" runat="server"></asp:TextBox>

        <asp:TextBox ID="txtinsertparams4" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams4" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams4" runat="server"></asp:TextBox>

        <asp:TextBox ID="txtinsertparams5" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams5" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams5" runat="server"></asp:TextBox>

        <asp:TextBox ID="txtinsertparams6" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams6" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams6" runat="server" BorderStyle="Double"></asp:TextBox>

        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        <asp:TextBox ID="TextBox3" runat="server" Text="First"></asp:TextBox>

        <asp:TextBox ID="txtCatchParameters1" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters2" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters3" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters4" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters5" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters6" runat="server" Text=""></asp:TextBox>

        <asp:TextBox ID="txtCatchValues1" runat="server" Text=""></asp:TextBox>

        <asp:TextBox ID="txtTransferedParameters1" runat="server" Text=""></asp:TextBox>

        <asp:TextBox ID="txtSubInsertPer2" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer3" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer4" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer5" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer6" runat="server" Text=""></asp:TextBox>

        <asp:TextBox ID="txtcurrentId" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtMessage" runat="server"></asp:TextBox>
        <dx:ASPxTextBox ID="txttask" ClientInstanceName="txttask" runat="server"></dx:ASPxTextBox>
        <asp:TextBox ID="txtPostDurum" runat="server" Text="First"></asp:TextBox>
        <asp:TextBox ID="txtValues" runat="server" ForeColor="Yellow"></asp:TextBox>

        <asp:TextBox ID="txtCurGridId" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCurGridObjecId" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCurGridPageId" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCurGridFieldId" runat="server" Text=""></asp:TextBox>

    </div>

    <dx:ASPxPopupControl ID="KTFCreateInvoicePopup" runat="server" CloseAction="CloseButton" CloseOnEscape="true" Modal="true" ClientInstanceName="KTFCreateInvoicePopup"
        HeaderText="KTF Create Invoice Popup" AllowDragging="true" PopupAnimationType="Slide" EnableViewState="false"
        Left="378" Top="156">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <asp:Panel ID="pnlContent" runat="server">
                    <dx:ASPxFormLayout ID="Box1" runat="server" Width="100%">
                        <Items>
                            <dx:LayoutGroup ShowCaption="False" ColCount="3">
                                <Items>
                                    <dx:LayoutItem FieldName="INVOICE DATE">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxDateEdit ID="txtInvDate" runat="server" DisplayFormatString="dd.MM.yyyy"></dx:ASPxDateEdit>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem FieldName="" ShowCaption="False">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxButton ID="btnSave" runat="server" CssClass="formglobalbuttonStyle" OnClick="btnSave_Click" Text="CREATE" ForeColor="White">
                                                    <ClientSideEvents Click="function(s,e) { HideModalPopup(s,e); }" />
                                                </dx:ASPxButton>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                </Items>
                            </dx:LayoutGroup>
                        </Items>
                    </dx:ASPxFormLayout>
                </asp:Panel>

            </dx:PopupControlContentControl>
        </ContentCollection>

    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl ID="DatePopup" runat="server" CloseAction="CloseButton" CloseOnEscape="true" Modal="true" ClientInstanceName="DatePopup"
        HeaderText="Tarih Alanını Doldurunuz" AllowDragging="true" PopupAnimationType="Slide" EnableViewState="false"
        Left="378" Top="156">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <asp:Panel ID="pnlDateContent" runat="server">
                    <dx:ASPxFormLayout ID="Box2" runat="server" Width="100%">
                        <Items>
                            <dx:LayoutGroup ShowCaption="False" ColCount="3">
                                <Items>
                                    <dx:LayoutItem FieldName="TARIH">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxDateEdit ID="txtDate" runat="server" EditFormatString="dd.MM.yyyy" DisplayFormatString="dd.MM.yyyy"></dx:ASPxDateEdit>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem FieldName="" ShowCaption="False">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxButton ID="BtnProc" runat="server" CssClass="formglobalbuttonStyle" OnClick="BtnProc_Click" Text="İŞLEMİ YAP">
                                                    <ClientSideEvents Click="function(s,e) { HideModalDatePopup(s,e); }" />
                                                </dx:ASPxButton>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                </Items>
                            </dx:LayoutGroup>
                        </Items>
                    </dx:ASPxFormLayout>
                </asp:Panel>

            </dx:PopupControlContentControl>
        </ContentCollection>

    </dx:ASPxPopupControl>
    <dx:ASPxCallbackPanel ID="callbackKTFMessage" ClientInstanceName="callbackKTFMessage" runat="server" OnCallback="callbackKTFMessage_Callback">
        <PanelCollection>
            <dx:PanelContent>
                <div style="display: none">
                    <dx:ASPxTextBox ID="txtKTFmessage" runat="server"></dx:ASPxTextBox>
                </div>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>

</asp:Content>


