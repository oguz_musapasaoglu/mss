﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using MSS1.WSGeneric;
using System.ServiceModel;
using System.Data;
using MSS1.Codes;

namespace MSS1
{
    public partial class iadefaturaonayla : Bases 
    {
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        SqlConnection baglanti;
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        SessionBase _obj;
        public ws_baglantı ws = new ws_baglantı();
        Methods method = new Methods();

        #region Dil Getir
        public void dilgetir()
        {
            lblBelgeTarihiB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "iadefaturaonayla", "lblBelgeTarihiB");
            lblDovizKuruB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "iadefaturaonayla", "lblDovizKuruB");
            btnonayla.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "iadefaturaonayla", "btnonayla");
        }
        #endregion

        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        { 
              _binding = new BasicHttpBinding();
             _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
              _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }
        protected void Page_Load(object sender, EventArgs e)
        { 
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }

     
            _obj = TabSessions.ControlState(this.Page, Request.QueryString["GId"].ToString());
            dilgetir();
            if (!IsPostBack)
            {
                tarihkurgetir(_obj.Comp.ToString(), Request.QueryString["BelgeNo"], 0);
            }
        }
        protected void txtbelgetarihi_TextChanged(object sender, EventArgs e)
        {
            f.kurgetir(_obj.Comp.ToString(), txtkur, txtbelgetarihi.Text, Request.QueryString["DCins"]);
        }
        private void tarihkurgetir(string sirket, string sipno, int tur)
        {

            baglanti = new SqlConnection(strConnString);
            baglanti.Open();
            SqlCommand sorgu = new SqlCommand();

            sorgu.Connection = baglanti;
            sorgu.CommandType = CommandType.StoredProcedure;
            sorgu.CommandText = "SP_0WEB_SATINALMA_FATURA_BASLIK_ARA";
            sorgu.Parameters.AddWithValue("@COMP", sirket);
            sorgu.Parameters.AddWithValue("@USER", _obj.UserName.ToString());
            sorgu.Parameters.AddWithValue("@MENU", 2);
            sorgu.Parameters.AddWithValue("@ALTMENU", 0);
            sorgu.Parameters.AddWithValue("@ad", "");
            sorgu.Parameters.AddWithValue("@FatTipi", 9);
            string basTarih = "0001-01-01";
            sorgu.Parameters.AddWithValue("@bastarih", basTarih);
            string bitTarih = DateTime.Now.ToString("yyyy-MM-dd");
            sorgu.Parameters.AddWithValue("@bittarih", bitTarih);
            sorgu.Parameters.AddWithValue("@dnbastarih", basTarih);
            sorgu.Parameters.AddWithValue("@dnbittarih", bitTarih);
            sorgu.Parameters.AddWithValue("@oluskul", "");
            sorgu.Parameters.AddWithValue("@hbno", "");
            sorgu.Parameters.AddWithValue("@isno", "");
            sorgu.Parameters.AddWithValue("@saticituru", "");
            sorgu.Parameters.AddWithValue("@refno", "");
            sorgu.Parameters.AddWithValue("@type", "");
            sorgu.Parameters.AddWithValue("@belgeNo", sipno);

            SqlDataAdapter sorguDA = new SqlDataAdapter(sorgu);
            DataTable sorgugridDT = new DataTable();
            sorguDA.Fill(sorgugridDT);
            SqlDataReader sqr;
            sqr = sorgu.ExecuteReader();
            if (sqr.Read())
            {
                txtbelgetarihi.Text = DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year;
                txtkur.Text = Convert.ToDouble(sqr["kur"]).ToString();
            }

            sqr.Close();

        }

        string tcevirtum;
        protected void btnonayla_Click(object sender, EventArgs e)
        {
            if (txtbelgetarihi.Text != string.Empty)
            {
                 
                string[] tcevir = txtbelgetarihi.Text.Split('.');
                tcevirtum += tcevir[1] + tcevir[0] + tcevir[2];
                if (f.MaksDovizTutariKontrol(Request.QueryString["DCins"].ToString(), txtkur.Text, tcevir[2] + "-" + tcevir[1] + "-" + tcevir[0],_obj.Comp) || Request.QueryString["DCins"].ToString() == "TL")
                {

                    string[] _params = { Request.QueryString["BelgeNo"], tcevirtum, Convert.ToDecimal(txtkur.Text).ToString(), Request.QueryString["DCins"], _obj.NameSurName };
                    GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "SatinFatTarihGuncelle§" + string.Join("§", _params), _obj.Comp);

                    if (!ClientScript.IsStartupScriptRegistered("JSFaturaAcik"))
                    {
                        ClientScript.RegisterStartupScript(GetType(), "JSFaturaAcik", @"window.opener.parent.RefreshAllCollapse();self.close();", true);
                    }
                }
                else
                {
                    MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "iadefaturaonayla", "msg142"));
                }
            }
        }
        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }
    }
}