﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" MasterPageFile="~/MasterPage.Master" Inherits="MSS1.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">


    <style type="text/css">
        html body {
            width: 99%;
            height: 99%;
        }

        .ustTable {
            table-layout: fixed;
        }

        .spacetr {
            height: 2px;
        }

        .middletable {
            border: none;
            border-width: 1px;
            border-color: black;
            table-layout: fixed;
            width: 100%;
            height: 100%;
        }


            .middletable tr {
                min-height: 1%;
                min-width: 1%;
            }

            .middletable td {
                min-height: 1%;
                min-width: 1%;
                border: none;
                border-width: 1px;
                border-color: black;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="specialdiv1" style="display: none">
    </div>
    <table style="width: 100%; height: 100%;" class="ustTable" cellspacing="2">


        <tr>
            <td runat="server" id="tdleft" style="width: 20%; height: 100%; vertical-align: top; display:none">
                <table runat="server" id="tblTodo" border="0" style="width: 100%; height: 100%; table-layout: fixed" cellspacing="2" cellpadding="0">
                    <tr>
                        <td style="width: 100%">
                            <hr style="background-color: #35a1d9; border: 0px solid #000000; height: 1px; margin: 0; width: 100%;">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%">
                            <table style="width: 99%; height: 100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 65%">&nbsp;
                                        <asp:Label runat="server" CssClass="labelstyle" ID="Label1"></asp:Label></td>
                                    <td runat="server" id="TodoRight1" style="width: 35%">&nbsp;
                                        <asp:LinkButton runat="server" CssClass="linklabelstyle" ID="LinkButton1"></asp:LinkButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%;">
                            <table style="width: 99%; height: 100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 65%">&nbsp;
                                        <asp:Label runat="server" CssClass="labelstyle" ID="Label2"></asp:Label></td>
                                    <td runat="server" id="TodoRight2" style="width: 35%">&nbsp;
                                        <asp:LinkButton runat="server" CssClass="linklabelstyle" ID="LinkButton2"></asp:LinkButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%">
                            <table style="width: 99%; height: 100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 65%">&nbsp;
                                        <asp:Label runat="server" CssClass="labelstyle" ID="Label3"></asp:Label></td>
                                    <td runat="server" id="TodoRight3" style="width: 35%">&nbsp;
                                        <asp:LinkButton runat="server" CssClass="linklabelstyle" ID="LinkButton3"></asp:LinkButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%">
                            <table style="width: 99%; height: 100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 65%">&nbsp;
                                        <asp:Label runat="server" CssClass="labelstyle" ID="Label4"></asp:Label></td>
                                    <td runat="server" id="TodoRight4" style="width: 35%">&nbsp;
                                        <asp:LinkButton runat="server" CssClass="linklabelstyle" ID="LinkButton4"></asp:LinkButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%">
                            <table style="width: 99%; height: 100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 65%">&nbsp;
                                        <asp:Label runat="server" CssClass="labelstyle" ID="Label5"></asp:Label></td>
                                    <td runat="server" id="TodoRight5" style="width: 35%">&nbsp;
                                        <asp:LinkButton runat="server" CssClass="linklabelstyle" ID="LinkButton5"></asp:LinkButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%">
                            <table style="width: 99%; height: 100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 65%">&nbsp;
                                        <asp:Label runat="server" CssClass="labelstyle" ID="Label6"></asp:Label></td>
                                    <td runat="server" id="TodoRight6" style="width: 35%">&nbsp;
                                        <asp:LinkButton runat="server" CssClass="linklabelstyle" ID="LinkButton6"></asp:LinkButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%">
                            <table style="width: 99%; height: 100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 65%">&nbsp;
                                        <asp:Label runat="server" CssClass="labelstyle" ID="Label7"></asp:Label></td>
                                    <td runat="server" id="TodoRight7" style="width: 35%">&nbsp;
                                        <asp:LinkButton runat="server" CssClass="linklabelstyle" ID="LinkButton7"></asp:LinkButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%">
                            <table style="width: 99%; height: 100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 65%">&nbsp;
                                        <asp:Label runat="server" CssClass="labelstyle" ID="Label8"></asp:Label></td>
                                    <td runat="server" id="TodoRight8" style="width: 35%">&nbsp;
                                        <asp:LinkButton runat="server" CssClass="linklabelstyle" ID="LinkButton8"></asp:LinkButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%">
                            <table style="width: 99%; height: 100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 65%">&nbsp;
                                        <asp:Label runat="server" CssClass="labelstyle" ID="Label9"></asp:Label></td>
                                    <td runat="server" id="TodoRight9" style="width: 35%">&nbsp;
                                        <asp:LinkButton runat="server" CssClass="linklabelstyle" ID="LinkButton9"></asp:LinkButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%">
                            <table style="width: 99%; height: 100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 65%">&nbsp;
                                        <asp:Label runat="server" CssClass="labelstyle" ID="Label10"></asp:Label></td>
                                    <td runat="server" id="TodoRight10" style="width: 35%">&nbsp;
                                        <asp:LinkButton runat="server" CssClass="linklabelstyle" ID="LinkButton10"></asp:LinkButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%">
                            <table style="width: 99%; height: 100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 65%">&nbsp;
                                        <asp:Label runat="server" CssClass="labelstyle" ID="Label11"></asp:Label></td>
                                    <td runat="server" id="TodoRight11" style="width: 35%">&nbsp;
                                        <asp:LinkButton runat="server" CssClass="linklabelstyle" ID="LinkButton11"></asp:LinkButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%">
                            <table style="width: 99%; height: 100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 65%">&nbsp;
                                        <asp:Label runat="server" CssClass="labelstyle" ID="Label12"></asp:Label></td>
                                    <td runat="server" id="TodoRight12" style="width: 35%">&nbsp;
                                        <asp:LinkButton runat="server" CssClass="linklabelstyle" ID="LinkButton12"></asp:LinkButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%">
                            <table style="width: 99%; height: 100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 65%">&nbsp;
                                        <asp:Label runat="server" CssClass="labelstyle" ID="Label13"></asp:Label></td>
                                    <td runat="server" id="TodoRight13" style="width: 35%">&nbsp;
                                        <asp:LinkButton runat="server" CssClass="linklabelstyle" ID="LinkButton13"></asp:LinkButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%">
                            <table style="width: 99%; height: 100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 65%">&nbsp;
                                        <asp:Label runat="server" CssClass="labelstyle" ID="Label14"></asp:Label></td>
                                    <td runat="server" id="TodoRight14" style="width: 35%">&nbsp;
                                        <asp:LinkButton runat="server" CssClass="linklabelstyle" ID="LinkButton14"></asp:LinkButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%">
                            <table style="width: 99%; height: 100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 65%">&nbsp;
                                        <asp:Label runat="server" CssClass="labelstyle" ID="Label15"></asp:Label></td>
                                    <td runat="server" id="TodoRight15" style="width: 35%">&nbsp;
                                        <asp:LinkButton runat="server" CssClass="linklabelstyle" ID="LinkButton15"></asp:LinkButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%">
                            <table style="width: 99%; height: 100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 65%">&nbsp;
                                        <asp:Label runat="server" CssClass="labelstyle" ID="Label16"></asp:Label></td>
                                    <td runat="server" id="TodoRight16" style="width: 35%">&nbsp;
                                        <asp:LinkButton runat="server" CssClass="linklabelstyle" ID="LinkButton16"></asp:LinkButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%">
                            <table style="width: 99%; height: 100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 65%">&nbsp;
                                        <asp:Label runat="server" CssClass="labelstyle" ID="Label17"></asp:Label></td>
                                    <td runat="server" id="TodoRight17" style="width: 35%">&nbsp;
                                        <asp:LinkButton runat="server" CssClass="linklabelstyle" ID="LinkButton17"></asp:LinkButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%">
                            <table style="width: 99%; height: 100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 65%">&nbsp;
                                        <asp:Label runat="server" CssClass="labelstyle" ID="Label18"></asp:Label></td>
                                    <td runat="server" id="TodoRight18" style="width: 35%">&nbsp;
                                        <asp:LinkButton runat="server" CssClass="linklabelstyle" ID="LinkButton18"></asp:LinkButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%">
                            <table style="width: 99%; height: 100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 65%">&nbsp;
                                        <asp:Label runat="server" CssClass="labelstyle" ID="Label19"></asp:Label></td>
                                    <td runat="server" id="TodoRight19" style="width: 35%">&nbsp;
                                        <asp:LinkButton runat="server" CssClass="linklabelstyle" ID="LinkButton19"></asp:LinkButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%">
                            <table style="width: 99%; height: 100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 65%">&nbsp;
                                        <asp:Label runat="server" CssClass="labelstyle" ID="Label20"></asp:Label></td>
                                    <td runat="server" id="TodoRight20" style="width: 35%">&nbsp;
                                        <asp:LinkButton runat="server" CssClass="linklabelstyle" ID="LinkButton20"></asp:LinkButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            <td runat="server" id="tdright" style="width: 80%; vertical-align: top;">
                <table runat="server" class="middletable" id="tblmain" cellspacing="0" cellpadding="0">
                    <tr>
                        <td></td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
</asp:Content>

