﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LojAylikRaporParameter.aspx.cs" Inherits="MSS1.LojAylikRaporParameter" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .burakalan {
            width: 240px;
        }

        .burakalan2 {
            width: 200px;
        }

        #overlay {
            position: fixed;
            z-index: 3;
            top: 0px;
            left: 0px;
            background-color: #f8f8f8;
            width: 100%;
            height: 100%;
            filter: Alpha(Opacity=90);
            opacity: 0.9;
            -moz-opacity: 0.9;
            visibility: hidden;
        }

        #theprogress {
            background-color: #fff;
            border: 1px solid #ccc;
            padding: 10px;
            width: 300px;
            height: 70px;
            line-height: 30px;
            text-align: center;
            filter: Alpha(Opacity=100);
            opacity: 1;
            -moz-opacity: 1;
        }

        #modalprogress {
            position: absolute;
            top: 40%;
            left: 50%;
            margin: -11px 0 0 -150px;
            color: #990000;
            font-weight: bold;
            font-size: 14px;
        }
    </style>

    <script type="text/javascript" language="javascript">
        function resetPosition(object, args) {

            var tb = object._element; // tb.id is the associated textbox ID in the grid.

            // Get the position with scrolling.
            var tbposition = [tb.offsetLeft + tb.offsetParent.offsetLeft, tb.offsetTop + tb.offsetParent.offsetTop];

            var xposition = tbposition[0] + 10;

            var yposition = tbposition[1] + 335;

            var ex = object._completionListElement;
            if (ex)

                $common.setLocation(ex, new Sys.UI.Point(xposition, yposition));
        } // End resetPosition
        function resetPosition2(object, args) {

            var tb = object._element; // tb.id is the associated textbox ID in the grid.

            // Get the position with scrolling.
            var tbposition = [tb.offsetLeft + tb.offsetParent.offsetLeft, tb.offsetTop + tb.offsetParent.offsetTop];

            var xposition = tbposition[0] + 10;

            var yposition = tbposition[1] + 690;

            var ex = object._completionListElement;
            if (ex)

                $common.setLocation(ex, new Sys.UI.Point(xposition, yposition));
        } // End resetPosition
        function resetPosition3(object, args) {

            var tb = object._element; // tb.id is the associated textbox ID in the grid.

            // Get the position with scrolling.
            var tbposition = [tb.offsetLeft + tb.offsetParent.offsetLeft, tb.offsetTop + tb.offsetParent.offsetTop];

            var xposition = tbposition[0] + 10;

            var yposition = tbposition[1] + 700;

            var ex = object._completionListElement;
            if (ex)

                $common.setLocation(ex, new Sys.UI.Point(xposition, yposition));
        } // End resetPosition
        function resetPositionItem(object, args) {

            var tb = object._element; // tb.id is the associated textbox ID in the grid.

            // Get the position with scrolling.
            var tbposition = [tb.offsetLeft + tb.offsetParent.offsetLeft, tb.offsetTop + tb.offsetParent.offsetTop];

            var xposition = tbposition[0] + 10;

            var yposition = tbposition[1] + 650;

            var ex = object._completionListElement;
            if (ex)

                $common.setLocation(ex, new Sys.UI.Point(xposition, yposition));
        } // End resetPosition
        function resetPositionserino(object, args) {

            var tb = object._element; // tb.id is the associated textbox ID in the grid.

            // Get the position with scrolling.
            var tbposition = [tb.offsetLeft + tb.offsetParent.offsetLeft, tb.offsetTop + tb.offsetParent.offsetTop];

            var xposition = tbposition[0] + 10;

            var yposition = tbposition[1] + 650;

            var ex = object._completionListElement;
            if (ex)

                $common.setLocation(ex, new Sys.UI.Point(xposition, yposition));
        } // End resetPosition
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <br />
            <h2>
                <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptLocalization="true"
                    EnableScriptGlobalization="true" EnablePageMethods="true">
                </asp:ScriptManager>
                <asp:Label ID="lblsayfaadi" runat="server"></asp:Label></h2>
            <table class="fixtable" cellspacing="1">
                <tr>
                    <td nowrap="nowrap" class="burakalan">
                        <asp:Label ID="lblSirket" runat="server"></asp:Label>
                    </td>
                    <td class="burakalan">


                        <asp:DropDownList ID="ddlSirket" runat="server" AppendDataBoundItems="True"
                            CssClass="textbox" Height="22px" Width="180px">
                            <asp:ListItem Value="COMBINED" Text="COMBINED"> </asp:ListItem>
                            <asp:ListItem Value="SEPARATED" Text="SEPARATED"> </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">
                        <asp:CheckBox ID="chOpDept" runat="server" AutoPostBack="True" OnCheckedChanged="chOpDept_CheckedChanged" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtOpDept" runat="server" CssClass="textbox" MaxLength="30" Width="180px" OnTextChanged="txtOpDept_TextChanged" AutoPostBack="True"></asp:TextBox>
                        <cc1:AutoCompleteExtender ServiceMethod="SearchDept" UseContextKey="true" MinimumPrefixLength="1"
                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtOpDept"
                            ID="AutoCompleteExtender2" runat="server" FirstRowSelected="false" CompletionListCssClass="listMain"
                            CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected"
                            OnClientShown="resetPosition">
                        </cc1:AutoCompleteExtender>
                        <asp:Literal ID="ltrOpDept" runat="server" Visible="false"> </asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">
                        <asp:Label ID="lblRaporTuruYeni" runat="server"></asp:Label>
                    </td>
                    <td class="burakalan">
                        <asp:DropDownList ID="ddlRaporTuruYeni" runat="server" AppendDataBoundItems="True"
                            CssClass="textbox" Height="22px" Width="210px">
                            <asp:ListItem Value="COMPANY BASED" Text="COMPANY BASED"> </asp:ListItem>
                            <asp:ListItem Value="OP. DEPT BASED" Text="OP. DEPT BASED"> </asp:ListItem>
                            <asp:ListItem Value="OP. REGION BASED" Text="OP. REGION BASED"> </asp:ListItem>
                            <asp:ListItem Value="SALESPERSON GROUP BASED" Text="SALESPERSON GROUP BASED"> </asp:ListItem>
                            <asp:ListItem Value="SALESPERSON DEPT BASED" Text="SALESPERSON DEPT BASED"> </asp:ListItem>
                            <asp:ListItem Value="SALESPERSON BASED" Text="SALESPERSON BASED"> </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">
                        <asp:CheckBox ID="chOpBolge" runat="server" AutoPostBack="True" OnCheckedChanged="chOpBolge_CheckedChanged" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtOpBolge" runat="server" CssClass="textbox" MaxLength="30" Width="180px" OnTextChanged="txtOpBolge_TextChanged" AutoPostBack="True"></asp:TextBox>
                        <cc1:AutoCompleteExtender ServiceMethod="SearchBolge" UseContextKey="true" MinimumPrefixLength="1"
                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtOpBolge"
                            ID="AutoCompleteExtender1" runat="server" FirstRowSelected="false" CompletionListCssClass="listMain"
                            CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected"
                            OnClientShown="resetPosition">
                        </cc1:AutoCompleteExtender>
                        <asp:Literal ID="ltrOpBolge" runat="server" Visible="False"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">
                        <asp:Label ID="lblYil" runat="server"></asp:Label>
                    </td>
                    <td class="burakalan">
                        <asp:DropDownList ID="ddlYil" runat="server" AppendDataBoundItems="True" AutoPostBack="true"
                            CssClass="textbox" Height="22px" Width="180px" OnSelectedIndexChanged="ddlYil_SelectedIndexChanged">
                            <asp:ListItem Value="2016" Text="2016"> </asp:ListItem>
                            <asp:ListItem Value="2017" Text="2017"> </asp:ListItem>
                            <asp:ListItem Value="2018" Text="2018" Selected="True"> </asp:ListItem>
                            <asp:ListItem Value="2019" Text="2019"> </asp:ListItem>
                            <asp:ListItem Value="2020" Text="2020"> </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">
                        <asp:CheckBox ID="chMtDept" runat="server" AutoPostBack="True" OnCheckedChanged="chMtDept_CheckedChanged" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtMtDept" runat="server" CssClass="textbox" MaxLength="30" Width="180px" OnTextChanged="txtMtDept_TextChanged" AutoPostBack="True"></asp:TextBox>
                        <cc1:AutoCompleteExtender ServiceMethod="SearchDept" UseContextKey="true" MinimumPrefixLength="1"
                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtMtDept"
                            ID="AutoCompleteExtender3" runat="server" FirstRowSelected="false" CompletionListCssClass="listMain"
                            CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected"
                            OnClientShown="resetPosition">
                        </cc1:AutoCompleteExtender>
                        <asp:Literal ID="ltrMtDept" runat="server" Visible="False"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">
                        <asp:Label ID="lblDonem" runat="server"></asp:Label>
                    </td>
                    <td class="burakalan">
                        <asp:DropDownList ID="ddlDonem" runat="server" AppendDataBoundItems="True" Enabled="false" CssClass="textbox" Height="22px" Width="180px">
                            <asp:ListItem Value="YEAR TOTAL" Text="YEAR TOTAL" Selected="True"> </asp:ListItem>
                            <asp:ListItem Value="Q1" Text="QUARTER 1"> </asp:ListItem>
                            <asp:ListItem Value="Q2" Text="QUARTER 2"> </asp:ListItem>
                            <asp:ListItem Value="Q3" Text="QUARTER 3"> </asp:ListItem>
                            <asp:ListItem Value="Q4" Text="QUARTER 4"> </asp:ListItem>

                        </asp:DropDownList>
                    </td>


                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">
                        <asp:CheckBox ID="chMT" runat="server" AutoPostBack="True" OnCheckedChanged="chMT_CheckedChanged" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtMT" runat="server" CssClass="textbox" MaxLength="30" Width="180px" OnTextChanged="txtMT_TextChanged" AutoPostBack="True"></asp:TextBox>
                        <cc1:AutoCompleteExtender ServiceMethod="SearchMT" UseContextKey="true" MinimumPrefixLength="1"
                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtMT"
                            ID="AutoCompleteExtender4" runat="server" FirstRowSelected="false" CompletionListCssClass="listMain"
                            CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected"
                            OnClientShown="resetPosition">
                        </cc1:AutoCompleteExtender>
                        <asp:Literal ID="ltrMT" runat="server" Visible="False"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">
                        <asp:Label ID="lbljobCusttype" runat="server"></asp:Label>
                    </td>
                    <td class="burakalan">
                        <asp:DropDownList ID="ddljobCusttype" runat="server" AppendDataBoundItems="True" CssClass="textbox" Height="22px" Width="180px">
                            <asp:ListItem Value="COMBINED" Text="ALL"> </asp:ListItem>
                            <asp:ListItem Value="DOMESTIC" Text="DOMESTIC"> </asp:ListItem>
                            <asp:ListItem Value="FOREIGN" Text="FOREIGN"> </asp:ListItem>
                            <asp:ListItem Value="TYPE A" Text="TYPE A"> </asp:ListItem>
                            <asp:ListItem Value="TYPE B" Text="TYPE B"> </asp:ListItem>
                            <asp:ListItem Value="TYPE C" Text="TYPE C"> </asp:ListItem>

                        </asp:DropDownList>
                    </td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">
                        <asp:CheckBox ID="chMusteri" runat="server" AutoPostBack="True" OnCheckedChanged="chMusteri_CheckedChanged" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtMusteri" runat="server" CssClass="textbox" MaxLength="30" Width="180px" OnTextChanged="txtMusteri_TextChanged" AutoPostBack="True"></asp:TextBox>
                        <cc1:AutoCompleteExtender ServiceMethod="SearchMusteri" UseContextKey="true" MinimumPrefixLength="1"
                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtMusteri"
                            ID="AutoCompleteExtender5" runat="server" FirstRowSelected="false" CompletionListCssClass="listMain"
                            CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected"
                            OnClientShown="resetPosition">
                        </cc1:AutoCompleteExtender>
                        <asp:Literal ID="ltrMusteri" runat="server" Visible="False"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">
                        <asp:Label ID="lblOpType" runat="server"></asp:Label>
                    </td>
                    <td class="burakalan">
                        <asp:DropDownList ID="ddlOpType" runat="server" AppendDataBoundItems="True"
                            CssClass="textbox" Height="22px" Width="180px">
                            <asp:ListItem Value="ALL" Text="ALL"> </asp:ListItem>
                            <asp:ListItem Value="EXPORT" Text="EXPORT"> </asp:ListItem>
                            <asp:ListItem Value="IMPORT" Text="IMPORT"> </asp:ListItem>
                            <asp:ListItem Value="CROSS" Text="CROSS"> </asp:ListItem>
                            <asp:ListItem Value="INLAND" Text="INLAND"> </asp:ListItem>
                            <asp:ListItem Value="OTHER" Text="OTHER"> </asp:ListItem>
                        </asp:DropDownList>
                    </td>

                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">
                        <asp:CheckBox ID="chCarrier" runat="server" AutoPostBack="True" OnCheckedChanged="chCarrier_CheckedChanged" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtCarrier" runat="server" CssClass="textbox" MaxLength="30" Width="180px" OnTextChanged="txtCarrier_TextChanged" AutoPostBack="True"></asp:TextBox>
                        <cc1:AutoCompleteExtender ServiceMethod="SearchCarrier" UseContextKey="true" MinimumPrefixLength="1"
                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtCarrier"
                            ID="AutoCompleteExtender6" runat="server" FirstRowSelected="false" CompletionListCssClass="listMain"
                            CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected"
                            OnClientShown="resetPosition">
                        </cc1:AutoCompleteExtender>
                        <asp:Literal ID="ltrCarrier" runat="server" Visible="False"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">&nbsp;</td>
                    <td class="burakalan">&nbsp;</td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">
                        <asp:CheckBox ID="lblAgent" runat="server" AutoPostBack="True" OnCheckedChanged="lblAgent_CheckedChanged" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtAgentListe" runat="server" CssClass="textbox" MaxLength="30" Width="180px" ForeColor="#808080"
                            OnTextChanged="txtAgentListe_TextChanged"
                            AutoPostBack="True"></asp:TextBox>


                        <cc1:AutoCompleteExtender ServiceMethod="SearchAgent" UseContextKey="true" MinimumPrefixLength="1"
                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtAgentListe"
                            ID="txtAgentListe_AutoCompleteExtender" runat="server" FirstRowSelected="false" CompletionListCssClass="listMain"
                            CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected"
                            OnClientShown="resetPosition">
                        </cc1:AutoCompleteExtender>



                        <asp:Literal ID="ltrAgentListe" runat="server" Visible="False"></asp:Literal>


                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">
                        <asp:Label ID="lblRaporGrup" runat="server" Visible="false"></asp:Label>
                    </td>
                    <td class="burakalan">
                        <asp:DropDownList ID="ddlRaporGrup" runat="server" AppendDataBoundItems="True" Visible="false"
                            CssClass="textbox" Height="22px" Width="180px">
                            <asp:ListItem Value="01 - FINANCIAL" Text="01 - FINANCIAL"> </asp:ListItem>
                            <asp:ListItem Value="02 - OPERATIONAL" Text="02 - OPERATIONAL"> </asp:ListItem>
                            <asp:ListItem Value="03 - FINANCIAL + OPERATIONAL" Text="03 - FINANCIAL + OPERATIONAL"> </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">
                        <asp:CheckBox ID="chIncome" runat="server" AutoPostBack="True" Checked="true" />
                    </td>
                    <td class="burakalan">
                        <asp:CheckBox ID="chExpense" runat="server" AutoPostBack="True" Checked="true" />
                    </td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">
                        <asp:CheckBox ID="chPolPod" runat="server" AutoPostBack="True" OnCheckedChanged="chPolPod_CheckedChanged" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtTextPol" runat="server" Style="text-align: center" MaxLength="30" Width="180px" BorderStyle="None" Enabled="False" Font-Bold="True">POL</asp:TextBox>
                        <asp:TextBox ID="txtTextPod" runat="server" Style="text-align: center" MaxLength="30" Width="180px" BorderStyle="None" Enabled="False" Font-Bold="True">POD</asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">&nbsp;</td>
                    <td class="burakalan">&nbsp;</td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="lblLiman" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtPOLLimanListe" runat="server" CssClass="textbox" MaxLength="30" Width="180px" ForeColor="#808080" OnTextChanged="txtPOLLimanListe_TextChanged"
                            AutoPostBack="True"></asp:TextBox>
                        <asp:TextBox ID="txtPODLimanListe" runat="server" CssClass="textbox" MaxLength="30" Width="180px" ForeColor="#808080" OnTextChanged="txtPODLimanListe_TextChanged"
                            AutoPostBack="True"></asp:TextBox>


                        <cc1:AutoCompleteExtender ServiceMethod="SearchLiman" UseContextKey="true" MinimumPrefixLength="1"
                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtPOLLimanListe"
                            ID="AutoCompleteExtender11" runat="server" FirstRowSelected="false" CompletionListCssClass="listMain"
                            CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected"
                            OnClientShown="resetPosition">
                        </cc1:AutoCompleteExtender>
                        <asp:Literal ID="ltrPOLLimanListe" runat="server" Visible="False"></asp:Literal>


                        <cc1:AutoCompleteExtender ServiceMethod="SearchLiman" UseContextKey="true" MinimumPrefixLength="1"
                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtPODLimanListe"
                            ID="AutoCompleteExtender12" runat="server" FirstRowSelected="false" CompletionListCssClass="listMain"
                            CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected"
                            OnClientShown="resetPosition">
                        </cc1:AutoCompleteExtender>
                        <asp:Literal ID="ltrPODLimanListe" runat="server" Visible="False"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">
                        <asp:Label ID="lblTop10Grup" runat="server"></asp:Label>
                    </td>
                    <td class="burakalan">
                        <asp:DropDownList ID="ddlTop10Group" runat="server" AppendDataBoundItems="True"
                            CssClass="textbox" Height="22px" Width="180px">
                            <asp:ListItem Value="OP DEPT" Text="OP DEPT"> </asp:ListItem>
                            <asp:ListItem Value="OP REGION" Text="OP REGION"> </asp:ListItem>
                            <asp:ListItem Value="SALESPERSON" Text="SALESPERSON"> </asp:ListItem>
                            <asp:ListItem Value="SALESPERSON GROUP" Text="SALESPERSON GROUP"> </asp:ListItem>
                            <asp:ListItem Value="SALESPERSON DEPT" Text="SALESPERSON DEPT"> </asp:ListItem>
                            <asp:ListItem Value="CUSTOMER TYPE" Text="CUSTOMER TYPE"> </asp:ListItem>
                            <asp:ListItem Value="CUSTOMER" Text="CUSTOMER"> </asp:ListItem>
                            <asp:ListItem Value="CARRIER" Text="CARRIER"> </asp:ListItem>
                            <asp:ListItem Value="AGENT" Text="AGENT"> </asp:ListItem>
                            <asp:ListItem Value="POL" Text="POL"> </asp:ListItem>
                            <asp:ListItem Value="POD" Text="POD"> </asp:ListItem>
                            <asp:ListItem Value="POL COUNTRY" Text="POL COUNTRY"> </asp:ListItem>
                            <asp:ListItem Value="POD COUNTRY" Text="POD COUNTRY"> </asp:ListItem>
                            <asp:ListItem Value="POL REGION" Text="POL REGION"> </asp:ListItem>
                            <asp:ListItem Value="POD REGION" Text="POD REGION"> </asp:ListItem>

                        </asp:DropDownList>
                    </td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="lblUlke" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtPOLUlkeListe" runat="server" CssClass="textbox" MaxLength="30" Width="180px" ForeColor="#808080"
                            OnTextChanged="txtPOLUlkeListe_TextChanged" AutoPostBack="True"></asp:TextBox>
                        <asp:TextBox ID="txtPODUlkeListe" runat="server" CssClass="textbox" MaxLength="30" Width="180px" ForeColor="#808080"
                            OnTextChanged="txtPODUlkeListe_TextChanged" AutoPostBack="True"></asp:TextBox>



                        <cc1:AutoCompleteExtender ServiceMethod="SearchUlke" UseContextKey="true" MinimumPrefixLength="1"
                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtPOLUlkeListe"
                            ID="AutoCompleteExtender7" runat="server" FirstRowSelected="false" CompletionListCssClass="listMain"
                            CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected"
                            OnClientShown="resetPosition">
                        </cc1:AutoCompleteExtender>
                        <asp:Literal ID="ltrPOLUlkeListe" runat="server" Visible="False"></asp:Literal>


                        <cc1:AutoCompleteExtender ServiceMethod="SearchUlke" UseContextKey="true" MinimumPrefixLength="1"
                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtPODUlkeListe"
                            ID="AutoCompleteExtender8" runat="server" FirstRowSelected="false" CompletionListCssClass="listMain"
                            CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected"
                            OnClientShown="resetPosition">
                        </cc1:AutoCompleteExtender>
                        <asp:Literal ID="ltrPODUlkeListe" runat="server" Visible="False"></asp:Literal>

                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">
                        <asp:Label ID="lblTop10Konu" runat="server"></asp:Label></td>
                    <td class="burakalan">
                        <asp:DropDownList ID="ddlTop10Konu" runat="server" AppendDataBoundItems="True"
                            CssClass="textbox" Height="22px" Width="180px">
                            <asp:ListItem Value="PROFIT" Text="PROFIT"> </asp:ListItem>
                            <asp:ListItem Value="INCOME" Text="INCOME"> </asp:ListItem>
                            <asp:ListItem Value="JOB" Text="JOB"> </asp:ListItem>
                            <asp:ListItem Value="TEU" Text="TEU"> </asp:ListItem>
                            <asp:ListItem Value="CW" Text="CW"> </asp:ListItem>
                            <asp:ListItem Value="KG" Text="KG"> </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="lblBolge" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtPOLBolgeListe" runat="server" CssClass="textbox" MaxLength="30" Width="180px" ForeColor="#808080"
                            OnTextChanged="txtPOLBolgeListe_TextChanged" AutoPostBack="True"></asp:TextBox>
                        <asp:TextBox ID="txtPODBolgeListe" runat="server" CssClass="textbox" MaxLength="30" Width="180px" ForeColor="#808080"
                            OnTextChanged="txtPODBolgeListe_TextChanged" AutoPostBack="True"></asp:TextBox>


                        <cc1:AutoCompleteExtender ServiceMethod="SearchBolgePolPod" UseContextKey="true" MinimumPrefixLength="1"
                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtPOLBolgeListe"
                            ID="AutoCompleteExtender9" runat="server" FirstRowSelected="false" CompletionListCssClass="listMain"
                            CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected"
                            OnClientShown="resetPosition">
                        </cc1:AutoCompleteExtender>
                        <asp:Literal ID="ltrPOLBolgeListe" runat="server" Visible="False"></asp:Literal>


                        <cc1:AutoCompleteExtender ServiceMethod="SearchBolgePolPod" UseContextKey="true" MinimumPrefixLength="1"
                            CompletionInterval="100" EnableCaching="false" CompletionSetCount="30" TargetControlID="txtPODBolgeListe"
                            ID="AutoCompleteExtender10" runat="server" FirstRowSelected="false" CompletionListCssClass="listMain"
                            CompletionListItemCssClass="itemmain" CompletionListHighlightedItemCssClass="itemsSelected"
                            OnClientShown="resetPosition">
                        </cc1:AutoCompleteExtender>
                        <asp:Literal ID="ltrPODBolgeListe" runat="server" Visible="False"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">&nbsp;</td>
                    <td class="burakalan">&nbsp;</td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td nowrap="nowrap" class="burakalan">&nbsp;</td>
                    <td class="burakalan">&nbsp;</td>
                    <td width="50px">&nbsp;</td>
                    <td class="burakalan2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="burakalan" nowrap="nowrap">&nbsp;</td>
                    <td class="burakalan"></td>
                    <td width="50px">
                        <div id="overlay">
                            <div id="modalprogress">
                                <div id="theprogress">
                                    <img src="images/ajax-loader.gif" name="imgLoading" id="imgLoading" style="visibility: hidden;">
                                    <br />
                                    <div id="yukleniyor" style="visibility: hidden;">
                                        <asp:Label ID="lblyukleniyor" runat="server" Text="Yükleniyor..."></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="burakalan2">&nbsp;</td>
                    <td>
                        <asp:Button ID="btnRaporOlustur" runat="server" CssClass="myButton" OnClick="btnRaporOlustur_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
