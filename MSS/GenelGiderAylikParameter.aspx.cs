﻿using MSS1.Codes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MSS1
{
    public partial class GenelGiderAylikParameter : System.Web.UI.Page
    {
        Language l = new Language();
        SessionBase _obj;
        static SessionBase _objStatic;
        private void DilGetir()
        {
            lblsayfaadi.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "GenelGiderAylikParameter", "lblsayfaadi");
            lblSirket.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "GenelGiderAylikParameter", "lblSirket");
            lblRaporTuru.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "GenelGiderAylikParameter", "lblRaporTuru");
            lblYil.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "GenelGiderAylikParameter", "lblYil");
            ltrGrup1.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "GenelGiderAylikParameter", "ltrGrup1");
            ltrGrup2.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "GenelGiderAylikParameter", "ltrGrup2");
            ltrGrup3.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "GenelGiderAylikParameter", "ltrGrup3");
            lblAnaDepartman.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "GenelGiderAylikParameter", "lblAnaDepartman");
            lblBolge.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "GenelGiderAylikParameter", "lblBolge");
            lblDepartman.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "GenelGiderAylikParameter", "lblDepartman");
            lblPersonel.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "GenelGiderAylikParameter", "lblPersonel");
            lblArac.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "GenelGiderAylikParameter", "lblArac");
            lblFirma.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "GenelGiderAylikParameter", "lblFirma");
            lblGGGrup.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "GenelGiderAylikParameter", "lblGGGrup");
            lblGG.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "GenelGiderAylikParameter", "lblGG");
            btnRaporOlustur.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "GenelGiderAylikParameter", "btnRaporOlustur");
        }


        #region Services

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchPersonel(string prefixText, int count, string contextKey)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    string query = "Select Name+'|'+Code Show from  [0C_00349_00_DIMENSION VALUES] where COMPANY='" + _objStatic.Comp + "' AND [Dimension Code]= 'PERSONEL' and Blocked = 0 and Totaling=''";

                    if (prefixText != "*")
                    {
                        query += " and UPPER(Name) like '" + prefixText.ToUpper() + "%'";
                    }
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;

                    cmd.Connection = conn;
                    conn.Open();
                    List<string> Cust = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {

                            Cust.Add(sdr["Show"].ToString());
                        }
                    }
                    conn.Close();
                    return Cust;
                }
            }
        }



        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchGGGrup(string prefixText, int count)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    string query = "Select   distinct [Grup Adı] +'|'+ Grup  Show FROM [0_W00020]";

                    if (prefixText != "*")
                    {
                        query += " and UPPER([Grup Adı]) like '" + prefixText.ToUpper() + "%'";
                    }


                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;

                    cmd.Connection = conn;
                    conn.Open();
                    List<string> Cust = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {

                            Cust.Add(sdr["Show"].ToString());
                        }
                    }
                    conn.Close();
                    return Cust;
                }
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchGG(string prefixText, int count, string contextKey)
        {

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    string query = "Select distinct GG Show FROM [vw_JLEGGGRUP] where";

                    if (contextKey != "")
                    {
                        query += " UPPER(GGGRUP) = '" + contextKey.ToUpper() + "'";
                    }


                    if (prefixText != "*")
                    {
                        query += " and UPPER(GG) like '" + prefixText.ToUpper() + "%'";
                    }

                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;

                    cmd.Connection = conn;
                    conn.Open();
                    List<string> Cust = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {

                            Cust.Add(sdr["Show"].ToString());
                        }
                    }
                    conn.Close();
                    return Cust;
                }
            }


        }



        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchArac(string prefixText, int count, string contextKey)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    string query = "Select Name+'|'+Code Show from [0C_00349_00_DIMENSION VALUES] where COMPANY='" + _objStatic.Comp + "' AND [Dimension Code]= 'ARAÇ_PLAKA' and Blocked = 0 and Totaling=''";

                    if (prefixText != "*")
                    {
                        query += " and UPPER(Name) like '" + prefixText.ToUpper() + "%'";
                    }
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;

                    cmd.Connection = conn;
                    conn.Open();
                    List<string> Cust = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {

                            Cust.Add(sdr["Show"].ToString());
                        }
                    }
                    conn.Close();
                    return Cust;
                }
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchFirmalar(string prefixText, int count)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    string query = "select TOP 200 Name + '|' + Code Show from [0T_00169_00_JOB LEGDER_FIRMALAR] WHERE  COMPANY='" + _objStatic.Comp + "'  ";
                    if (prefixText.ToString() != "*")
                    {
                        query += " where UPPER(Name) like '" + prefixText.ToString().ToUpper() + "%'";
                    }
                    List<string> Cust = new List<string>();

                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;

                    cmd.Connection = conn;
                    conn.Open();

                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Cust.Add(sdr["Show"].ToString());
                        }
                    }
                    conn.Close();

                    return Cust;
                }
            }
        }


        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchAnaDept(string prefixText, int count, string contextKey)
        {
            List<string> Cust = new List<string>();
            Cust.Add("1 - GEN. MÜD.");
            Cust.Add("2 - ACENTE");
            Cust.Add("3 - LOJİSTİK");
            Cust.Add("4 - HAT / PROJE");
            return Cust;
        }



        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchDept(string prefixText, int count, string contextKey)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    string query = "Select Name+'|'+Code Show from [dbo].[0C_00349_00_DIMENSION VALUES] where  COMPANY='" + _objStatic.Comp + "' AND [Dimension Code]= 'DEPARTMAN' and Blocked = 0 and Totaling=''";

                    if (prefixText != "*")
                    {
                        query += " and UPPER(Name) like '" + prefixText.ToUpper() + "%'";
                    }
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;

                    cmd.Connection = conn;
                    conn.Open();
                    List<string> Cust = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {

                            Cust.Add(sdr["Show"].ToString());
                        }
                    }
                    conn.Close();
                    return Cust;
                }
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchBolge(string prefixText, int count)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    string query = "Select Name+'|'+Code Show from [0C_00349_00_DIMENSION VALUES] where COMPANY='" + _objStatic.Comp + "' AND [Dimension Code]= 'BÖLGE' and Blocked = 0 and Totaling=''";
                    if (prefixText != "*")
                    {
                        query += " and UPPER(Name) like '" + prefixText.ToUpper() + "%'";
                    }
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;

                    cmd.Connection = conn;
                    conn.Open();
                    List<string> Cust = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {

                            Cust.Add(sdr["Show"].ToString());
                        }
                    }
                    conn.Close();
                    return Cust;
                }
            }
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchMusteri(string prefixText, int count)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    string query = "Select TOP 150 Name+'|'+No_ Show from [0C_00018_00_CUSTOMER] where  COMPANY='" + _objStatic.Comp + "' AND Blocked=0";
                    if (prefixText != "*")
                    {
                        query += " and UPPER(Name) like '" + prefixText.ToUpper() + "%'";
                    }
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;

                    cmd.Connection = conn;
                    conn.Open();
                    List<string> Cust = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {

                            Cust.Add(sdr["Show"].ToString());
                        }
                    }
                    conn.Close();
                    return Cust;
                }
            }
        }


        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchMT(string prefixText, int count)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    string query = "Select Name+'|'+Code Show from [dbo].[0C_00013_00_SALESPERSON] where COMPANY='" + _objStatic.Comp + "'  AND [İsten Ayrildi]=0";
                    if (prefixText != "*")
                    {
                        query += " and UPPER(Name) like '" + prefixText.ToUpper() + "%'";
                    }
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;

                    cmd.Connection = conn;
                    conn.Open();
                    List<string> Cust = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {

                            Cust.Add(sdr["Show"].ToString());
                        }
                    }
                    conn.Close();
                    return Cust;
                }
            }
        }


        #endregion

        #region kodugetir
        protected void txtanaDept_TextChanged(object sender, EventArgs e)
        {
            if (txtanaDept.Text.Replace("*", "") != "")
            {
                string[] arr = txtanaDept.Text.Split('-');
                txtanaDept.Text = arr[1].ToString().Trim();
                ltrAnaDept.Text = arr[0].ToString().Trim();


            }
            else if (txtanaDept.Text == "*")
            {
                txtanaDept.Text = "";
            }
        }

        protected void txtBolge_TextChanged(object sender, EventArgs e)
        {
            if (txtBolge.Text.Replace("*", "") != "")
            {
                string[] arr = txtBolge.Text.Split('|');
                txtBolge.Text = arr[0].ToString();
                ltrBolge.Text = arr[1].ToString();


            }
            else if (txtBolge.Text == "*")
            {
                txtBolge.Text = "";
            }
        }

        protected void txtDept_TextChanged(object sender, EventArgs e)
        {
            if (txtDept.Text.Replace("*", "") != "")
            {
                string[] arr = txtDept.Text.Split('|');
                txtDept.Text = arr[0].ToString();
                ltrDept.Text = arr[1].ToString();
            }
            else if (txtDept.Text == "*")
            {
                txtDept.Text = "";
            }
        }

        protected void txtPersonel_TextChanged(object sender, EventArgs e)
        {
            if (txtPersonel.Text.Replace("*", "") != "")
            {
                string[] arr = txtPersonel.Text.Split('|');
                txtPersonel.Text = arr[0].ToString();
                ltrPersonel.Text = arr[1].ToString();
            }
            else if (txtPersonel.Text == "*")
            {
                txtPersonel.Text = "";
            }
        }

        protected void txtArac_TextChanged(object sender, EventArgs e)
        {
            if (txtArac.Text.Replace("*", "") != "")
            {
                string[] arr = txtArac.Text.Split('|');
                txtArac.Text = arr[0].ToString();
                ltrArac.Text = arr[1].ToString();
            }
            else if (txtArac.Text == "*")
            {
                txtArac.Text = "";
            }
        }

        protected void txtFirma_TextChanged(object sender, EventArgs e)
        {
            if (txtFirma.Text.Replace("*", "") != "")
            {
                string[] arr = txtFirma.Text.Split('|');
                txtFirma.Text = arr[0].ToString();
                ltrFirma.Text = arr[1].ToString();
            }
            else if (txtFirma.Text == "*")
            {
                txtFirma.Text = "";
            }
        }





        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            _obj = TabSessions.ControlState(this.Page, Request.QueryString["GId"].ToString());
            _objStatic = TabSessions.ControlState(this.Page, Request.QueryString["GId"].ToString());
            DilGetir();
            if (!IsPostBack)
            {
                PreparePage();
            }
        }

        void PreparePage()
        {
            string sorgu = "select COMPANY Code, CASE WHEN [COMPANY]  ='STENA' THEN 'SEALINE' WHEN [COMPANY]  ='LAMLOJ' THEN 'LAMGLOBAL'  " +
                "ELSE [COMPANY] END AS Text from [0C_50001_00_KULLANICILAR] where COMPANY='"+ _obj.Comp + "' group by COMPANY";
            ddlSirket.Items.Clear();
            DsComp.SelectCommand = sorgu;
            ddlSirket.DataBind();
            ddlSirket.SelectedIndex = 0;
        }

        protected void btnRaporOlustur_Click(object sender, EventArgs e)
        {

            fonksiyonlar.genelGiderAylikRaporParameters[0] = ddlSirket.SelectedValue.ToString();
            fonksiyonlar.genelGiderAylikRaporParameters[1] = ddlRaporTuru.SelectedValue.ToString();
            fonksiyonlar.genelGiderAylikRaporParameters[2] = ddlGrup1.SelectedValue.ToString();
            fonksiyonlar.genelGiderAylikRaporParameters[3] = ddlGrup2.SelectedValue.ToString();
            fonksiyonlar.genelGiderAylikRaporParameters[4] = ddlGrup3.SelectedValue.ToString();
            fonksiyonlar.genelGiderAylikRaporParameters[5] = ltrAnaDept.Text;
            fonksiyonlar.genelGiderAylikRaporParameters[6] = _obj.Comp.ToString();
            fonksiyonlar.genelGiderAylikRaporParameters[7] = _obj.UserName.ToString();
            fonksiyonlar.genelGiderAylikRaporParameters[8] = ddlYil.SelectedValue.ToString();
            fonksiyonlar.genelGiderAylikRaporParameters[9] = ltrDept.Text;
            fonksiyonlar.genelGiderAylikRaporParameters[10] = ltrBolge.Text;
            fonksiyonlar.genelGiderAylikRaporParameters[11] = ltrPersonel.Text;
            fonksiyonlar.genelGiderAylikRaporParameters[12] = ltrArac.Text;
            fonksiyonlar.genelGiderAylikRaporParameters[13] = ltrFirma.Text;
            fonksiyonlar.genelGiderAylikRaporParameters[14] = ltrGGGrup.Text;
            Response.Redirect("Raporlar/GenelGiderAylikRaporu.aspx?GId=" + Request.QueryString["GId"]);


        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        protected void txtGGGrup_TextChanged(object sender, EventArgs e)
        {
            if (txtGGGrup.Text.Replace("*", "") != "")
            {
                string[] arr = txtGGGrup.Text.Split('|');
                txtGGGrup.Text = arr[0].ToString();
                ltrGGGrup.Text = arr[1].ToString();
            }
            else if (txtGGGrup.Text == "*")
            {
                txtGGGrup.Text = "";
            }

        }
    }
}