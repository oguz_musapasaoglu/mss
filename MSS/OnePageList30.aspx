﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="OnePageList30.aspx.cs" EnableViewState="true" Inherits="MSS1.OnePageList30" %>

<%@ Register Src="~/controls/DataTableControlGAC.ascx" TagName="StDataTable" TagPrefix="STDT" %>

<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        iframe {
            -moz-transform: scale(0.99, 0.99);
            -webkit-transform: scale(0.99, 0.99);
            -o-transform: scale(0.99, 0.99);
            -ms-transform: scale(0.99, 0.99);
            transform: scale(0.99, 0.99);
            -moz-transform-origin: top left;
            -webkit-transform-origin: top left;
            -o-transform-origin: top left;
            -ms-transform-origin: top left;
            transform-origin: top left;
            border: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        var _currentVal;
        var _currLink;
        function OnGridFocusedRowChanged(s, e, linkType) {
            var link = $("#" + "<%= txtlink1.ClientID %>").val();
            var linkfield = "<%= txtlinkField1.ClientID %>";
            var transferf = "<%= txtCatchParameters1.ClientID %>";
            var CatchParamsVals = "<%= txtCatchParamsAndValues.ClientID %>";
            switch (linkType) {
                case "1":
                    link = $("#" + "<%= txtlink1.ClientID %>").val();
                    linkfield = "<%= txtlinkField1.ClientID %>";
                    break;
                case "2":
                    link = $("#" + "<%= txtlink2.ClientID %>").val();
                    linkfield = "<%= txtlinkField2.ClientID %>";
                    break;
                case "3":
                    link = $("#" + "<%= txtlink3.ClientID %>").val();
                    linkfield = "<%= txtlinkField3.ClientID %>";
                    break;
                case "4":
                    link = $("#" + "<%= txtlink4.ClientID %>").val();
                    linkfield = "<%= txtlinkField4.ClientID %>";
                    break;
                case "5":
                    link = $("#" + "<%= txtlink5.ClientID %>").val();
                    linkfield = "<%= txtlinkField5.ClientID %>";
                    break;
                case "6":
                    link = $("#" + "<%= txtlink6.ClientID %>").val();
                    linkfield = "<%= txtlinkField6.ClientID %>";
                    break;
                case "7":
                    link = $("#" + "<%= txtlink7.ClientID %>").val();
                    linkfield = "<%= txtlinkField7.ClientID %>";
                    break;
                case "8":
                    link = $("#" + "<%= txtlink8.ClientID %>").val();
                    linkfield = "<%= txtlinkField8.ClientID %>";
                    break;
                case "9":
                    link = $("#" + "<%= txtlink9.ClientID %>").val();
                    linkfield = "<%= txtlinkField9.ClientID %>";
                    break;
                case "10":
                    link = $("#" + "<%= txtlink10.ClientID %>").val();
                    linkfield = "<%= txtlinkField10.ClientID %>";
                    break;
                case "11":
                    link = $("#" + "<%= txtlink11.ClientID %>").val();
                    linkfield = "<%= txtlinkField11.ClientID %>";
                    break;
                case "12":
                    link = $("#" + "<%= txtlink12.ClientID %>").val();
                    linkfield = "<%= txtlinkField12.ClientID %>";
                    break;
                case "13":
                    link = $("#" + "<%= txtlink13.ClientID %>").val();
                    linkfield = "<%= txtlinkField13.ClientID %>";
                    break;
                case "14":
                    link = $("#" + "<%= txtlink14.ClientID %>").val();
                    linkfield = "<%= txtlinkField14.ClientID %>";
                    break;
                case "15":
                    link = $("#" + "<%= txtlink15.ClientID %>").val();
                    linkfield = "<%= txtlinkField15.ClientID %>";
                    break;

            }


            if (link == "JOB") {
                s.GetRowValues(s.GetFocusedRowIndex(), $("#" + linkfield).val(), OnGetRowValuesJob);
            }
            else if (link == "FILTER2") {
                $("#" + "<%= txtCurLinkFields.ClientID %>").val($("#" + linkfield).val());
                s.GetRowValues(s.GetFocusedRowIndex(), $("#" + linkfield).val(), OnGetRowValuesFilterDetail);

            }
            else if (link == "FILTER") {
                $("#" + CatchParamsVals).val('');
                var tfields = $("#" + linkfield).val();
                if ($("#" + transferf).val() != '') tfields += ';' + $("#" + transferf).val();
                s.GetRowValues(s.GetFocusedRowIndex(), tfields, OnGetRowValuesFilter);


            }
            else {
                $("#" + "<%= txtlinkType.ClientID %>").val(linkType);
                s.GetRowValues(s.GetFocusedRowIndex(), $("#" + linkfield).val(), OnGetRowValues);
            }

        }

        function OpenPdfFilter(values) {
            document.getElementById('ctl00_ContentPlaceHolder1_HiddenRpath').value = values;
            document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_framePDF').src = values;
        }

        function StatuFilter(values) {
            var splitter = values.toString().split(",");
            document.getElementById('ctl00_ContentPlaceHolder1_HiddenMainStatu').value = splitter[0];

        }

        function AdGIdToLink(_val) {
            if (_val.includes("GId=")) return _val;
            var ObjGId = "<%= lblGId.ClientID %>";
            if (_val.includes("?"))
                _val = _val + "&GId=" + $("#" + ObjGId).text();
            else
                _val = _val + "?GId=" + $("#" + ObjGId).text();

            return _val;

        }

        function openpdfpopup() {
            var link = document.getElementById('ctl00_ContentPlaceHolder1_HiddenRpath').value;
            var popwin = window.open(AdGIdToLink(link), '_blank');
            popwin.focus();
        }

        function OnGetRowValues(values) {
            var linkType = $("#" + "<%= txtlinkType.ClientID %>").val();
            var linkFormat = "<%= txtlink1.ClientID %>";

            if (linkType == "1")
                linkFormat = "<%= txtlink1.ClientID %>";
            else if (linkType == "2")
                linkFormat = "<%= txtlink2.ClientID %>";
            else if (linkType == "3")
                linkFormat = "<%= txtlink3.ClientID %>";
            else if (linkType == "4")
                linkFormat = "<%= txtlink4.ClientID %>";
            else if (linkType == "5")
                linkFormat = "<%= txtlink5.ClientID %>";
            else if (linkType == "6")
                linkFormat = "<%= txtlink6.ClientID %>";
            else if (linkType == "7")
                linkFormat = "<%= txtlink7.ClientID %>";
            else if (linkType == "8")
                linkFormat = "<%= txtlink8.ClientID %>";
            else if (linkType == "9")
                linkFormat = "<%= txtlink9.ClientID %>";
            else if (linkType == "10")
                linkFormat = "<%= txtlink10.ClientID %>";
            else if (linkType == "11")
                linkFormat = "<%= txtlink11.ClientID %>";
            else if (linkType == "12")
                linkFormat = "<%= txtlink12.ClientID %>";
            else if (linkType == "13")
                linkFormat = "<%= txtlink13.ClientID %>";
            else if (linkType == "14")
                linkFormat = "<%= txtlink14.ClientID %>";
            else if (linkType == "15")
                linkFormat = "<%= txtlink15.ClientID %>";
            var link = $("#" + linkFormat).val();
            link = link.replace("XXXXX", values);
            if (link != '') {
                window.location.href = AdGIdToLink(link);
                window.focus();
            }

        }

        function OnGetRowValuesPopup(values) {
            alert(values);
           <%-- var linkFormat = "<%= txtlink.ClientID %>";
             var link = $("#" + linkFormat).val();
             link = link.replace("XXXXX", values[0]);
             link = link.replace("YYYYY", values[1]);
             window.location.href = link;
             window.focus();--%>
        }

        function OpenDirectLink(link) {
            var P1 = document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value;
            var P2 = document.getElementById('ctl00_ContentPlaceHolder1_HiddenP2').value;
            link = link.replace("XXXXX", P1);
            link = link.replace("YYYYY", P2);
            var popwin = window.open(AdGIdToLink(link), '_blank');
            popwin.focus();
        }

        var checkval = '';
        var checkRefVal = '';

        function checkParamVal(params) {
            return params.startsWith(checkval);
        }

        function checkRefObjs(params) {
            return params.startsWith(checkRefVal);
        }

        function ReturnSayacVal(sayac) {
            if (sayac == 1)
                return "XXXXX";
            else if (sayac == 2)
                return "YYYYY";
            else if (sayac == 3)
                return "ZZZZZ";
            else if (sayac == 4)
                return "TTTTT";
            else if (sayac == 5)
                return "PPPPP";
        }

        function OpenFirstCollapse() {
            grid2.AddNewRow();

        }

        function SetDynamicHeaders() {
            var CatchParamsVals = "<%= txtCatchParamsAndValues.ClientID %>";
            var cpvals = $("#" + CatchParamsVals).val();
            var cpvalarray = cpvals.split('|');
            var _val = '';
            for (var i = 1; i < 16; i++) {
                var dynmchdr = document.getElementById('ctl00_ContentPlaceHolder1_txtCollapseHeader' + i.toString()).value;
                if (dynmchdr.trim() != '') {
                    checkval = dynmchdr.trim();
                    _val = cpvalarray.find(checkParamVal);
                    if (_val.length > 0) {
                        //alert(_val);
                        if (i == 1)
                            DoHeaders1(_val.split(';')[1])
                        else if (i == 2)
                            DoHeaders2(_val.split(';')[1])
                        else if (i == 3)
                            DoHeaders3(_val.split(';')[1])
                        else if (i == 4)
                            DoHeaders4(_val.split(';')[1])
                        else if (i == 5)
                            DoHeaders5(_val.split(';')[1])
                        else if (i == 6)
                            DoHeaders6(_val.split(';')[1])
                        else if (i == 7)
                            DoHeaders7(_val.split(';')[1])
                        else if (i == 8)
                            DoHeaders8(_val.split(';')[1])
                        else if (i == 9)
                            DoHeaders9(_val.split(';')[1])
                        else if (i == 10)
                            DoHeaders10(_val.split(';')[1])
                        else if (i == 11)
                            DoHeaders11(_val.split(';')[1])
                        else if (i == 12)
                            DoHeaders12(_val.split(';')[1])
                        else if (i == 13)
                            DoHeaders13(_val.split(';')[1])
                        else if (i == 14)
                            DoHeaders14(_val.split(';')[1])
                        else if (i == 15)
                            DoHeaders15(_val.split(';')[1])
                    }
                }
            }
        }

        function SetCurVal(values) {

            var splitter = values.toString().split(",");

            if (splitter.length == 4) {
                _currLink = _currLink.replace('YYYYY', splitter[0]);
                _currLink = _currLink.replace('ZZZZZ', splitter[1]);
                _currLink = _currLink.replace('TTTTT', splitter[2]);
                _currLink = _currLink.replace('PPPPP', splitter[3]);
            }
            else if (splitter.length == 3) {
                _currLink = _currLink.replace('YYYYY', splitter[0]);
                _currLink = _currLink.replace('ZZZZZ', splitter[1]);
                _currLink = _currLink.replace('TTTTT', splitter[2]);
            }
            else if (splitter.length == 2) {
                _currLink = _currLink.replace('YYYYY', splitter[0]);
                _currLink = _currLink.replace('ZZZZZ', splitter[1]);
            }
            else if (splitter.length == 1) {
                _currLink = _currLink.replace('YYYYY', splitter[0]);
            }
            else {
                _currLink = _currLink.replace('YYYYY', values);

            }


            var popwin = window.open(AdGIdToLink(_currLink), '_blank');
            popwin.focus();
        }

        function OpenDirectLinkWithTransferParams(link, filterobjs, objId) {
            var array1 = filterobjs.split(',');
            var sayac = 1;
            var sayval = '';
            var _val = '';
            var P1 = document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value;
            var P2 = document.getElementById('ctl00_ContentPlaceHolder1_HiddenP2').value;
            var CatchParamsVals = "<%= txtCatchParamsAndValues.ClientID %>";
            var cpvals = $("#" + CatchParamsVals).val();
            var cpvalarray = cpvals.split('|');

            array1.forEach(function (element) {
                if (!element.includes('$')) {
                    _val = '';
                    sayval = ReturnSayacVal(sayac);
                    if (element == 'P1')
                        link = link.replace(sayval, P1);
                    else if (element == 'P2')
                        link = link.replace(sayval, P2);
                    else {
                        //alert(element);
                        checkval = element;
                        _val = cpvalarray.find(checkParamVal);
                        //alert(_val);
                        link = link.replace(sayval, _val.split(';')[1]);
                    }

                    sayac = sayac + 1;
                }
            });

            _currLink = link;


            if (filterobjs.includes('$')) {

                var _parsecol;
                for (var i = 1; i < array1.length; i++) {
                    if (i == 1) {
                        _parsecol = array1[i].replace('$', '');
                    }
                    else
                        _parsecol = _parsecol + ';' + array1[i].replace('$', '');
                }

                if (objId == '1')
                    grid1.GetRowValues(0, _parsecol, SetCurVal);
                if (objId == '2')
                    grid2.GetRowValues(0, _parsecol, SetCurVal);
                if (objId == '3')
                    grid3.GetRowValues(0, _parsecol, SetCurVal);
                if (objId == '4')
                    grid4.GetRowValues(0, _parsecol, SetCurVal);
                if (objId == '5')
                    grid5.GetRowValues(0, _parsecol, SetCurVal);
                if (objId == '6')
                    grid6.GetRowValues(0, _parsecol, SetCurVal);
                if (objId == '7')
                    grid7.GetRowValues(0, _parsecol, SetCurVal);
                if (objId == '8')
                    grid8.GetRowValues(0, _parsecol, SetCurVal);
                if (objId == '9')
                    grid9.GetRowValues(0, _parsecol, SetCurVal);
                if (objId == '10')
                    grid10.GetRowValues(0, _parsecol, SetCurVal);
                if (objId == '11')
                    grid11.GetRowValues(0, _parsecol, SetCurVal);
                if (objId == '12')
                    grid12.GetRowValues(0, _parsecol, SetCurVal);
                if (objId == '13')
                    grid13.GetRowValues(0, _parsecol, SetCurVal);
                if (objId == '14')
                    grid14.GetRowValues(0, _parsecol, SetCurVal);
                if (objId == '15')
                    grid15.GetRowValues(0, _parsecol, SetCurVal);

            }
            else {
                var popwin = window.open(AdGIdToLink(link), '_blank');
                popwin.focus();
            }

        }

        function OnGetRowValuesFilterDetail(values) {


            var splitter = values.toString().split(",");

            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP3').value = '';
            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP4').value = '';
            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP5').value = '';

            if (splitter.length == 1) {
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP3').value = splitter[0];

            }
            else if (splitter.length == 2) {
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP3').value = splitter[0];
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP4').value = splitter[1];
            }
            else if (values.length == 3) {
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP3').value = values[0];
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP4').value = values[1];
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP5').value = values[2];
            }



            try {
                if ($("#" + "<%= txtMasterObject2.ClientID %>").val() != '') {
                    if (rp2.GetCollapsed()) {
                        rp2.SetCollapsed(false);
                    } else {
                        grid2.PerformCallback('');
                    }
                }
            } catch (e) { }
            try {
                if ($("#" + "<%= txtMasterObject3.ClientID %>").val() != '') {
                    if (rp3.GetCollapsed()) {
                        rp3.SetCollapsed(false);
                    } else {
                        grid3.PerformCallback('');
                    }
                }
            } catch (e) { }
            try {
                if ($("#" + "<%= txtMasterObject4.ClientID %>").val() != '') {
                    if (rp4.GetCollapsed()) {
                        rp4.SetCollapsed(false);
                    } else {
                        grid4.PerformCallback('');
                    }
                }
            } catch (e) { }
            try {
                if ($("#" + "<%= txtMasterObject5.ClientID %>").val() != '') {
                    if (rp5.GetCollapsed()) {
                        rp5.SetCollapsed(false);
                    } else {
                        grid5.PerformCallback('');
                    }
                }
            } catch (e) { }
            try {
                if ($("#" + "<%= txtMasterObject6.ClientID %>").val() != '') {
                    if (rp6.GetCollapsed()) {
                        rp6.SetCollapsed(false);
                    } else {
                        grid6.PerformCallback('');
                    }
                }
            } catch (e) { }
            try {
                if ($("#" + "<%= txtMasterObject7.ClientID %>").val() != '') {
                    if (rp7.GetCollapsed()) {
                        rp7.SetCollapsed(false);
                    } else {
                        grid7.PerformCallback('');
                    }
                }
            } catch (e) { }
            try {
                if ($("#" + "<%= txtMasterObject8.ClientID %>").val() != '') {
                    if (rp8.GetCollapsed()) {
                        rp8.SetCollapsed(false);
                    } else {
                        grid8.PerformCallback('');
                    }
                }
            } catch (e) { }
            try {
                if ($("#" + "<%= txtMasterObject9.ClientID %>").val() != '') {
                    if (rp9.GetCollapsed()) {
                        rp9.SetCollapsed(false);
                    } else {
                        grid9.PerformCallback('');
                    }
                }
            } catch (e) { }
            try {
                if ($("#" + "<%= txtMasterObject10.ClientID %>").val() != '') {
                    if (rp10.GetCollapsed()) {
                        rp10.SetCollapsed(false);
                    } else {
                        grid10.PerformCallback('');
                    }
                }
            } catch (e) { }
            try {
                if ($("#" + "<%= txtMasterObject11.ClientID %>").val() != '') {
                    if (rp11.GetCollapsed()) {
                        rp11.SetCollapsed(false);
                    } else {
                        grid11.PerformCallback('');
                    }
                }
            } catch (e) { }
            try {
                if ($("#" + "<%= txtMasterObject12.ClientID %>").val() != '') {
                    if (rp12.GetCollapsed()) {
                        rp12.SetCollapsed(false);
                    } else {
                        grid12.PerformCallback('');
                    }
                }
            } catch (e) { }
            try {
                if ($("#" + "<%= txtMasterObject13.ClientID %>").val() != '') {
                    if (rp13.GetCollapsed()) {
                        rp13.SetCollapsed(false);
                    } else {
                        grid13.PerformCallback('');
                    }
                }
            } catch (e) { }
            try {
                if ($("#" + "<%= txtMasterObject14.ClientID %>").val() != '') {
                    if (rp14.GetCollapsed()) {
                        rp14.SetCollapsed(false);
                    } else {
                        grid14.PerformCallback('');
                    }
                }
            } catch (e) { }
            try {
                if ($("#" + "<%= txtMasterObject15.ClientID %>").val() != '') {
                    if (rp15.GetCollapsed()) {
                        rp15.SetCollapsed(false);
                    } else {
                        grid15.PerformCallback('');
                    }
                }
            } catch (e) { }

        }

        function OnGetRowValuesFilter(values) {
            var linkFormat = "<%= txtlinkField1.ClientID %>";
            var link = $("#" + linkFormat).val();
            var CatchParamsVals = "<%= txtCatchParamsAndValues.ClientID %>";
            var transferf = "<%= txtCatchParameters1.ClientID %>";
            var objf1 = "<%= txtOBJF1.ClientID %>";
            var objIsCard = "<%= txtIsCard.ClientID %>";

            var _cs2, _cs3, _cs4, _c5, _cs6, _cs7, _cs8, _cs9, _cs10, _cs11, _cs12, _cs13, _cs14, _cs15;
            if ($("#" + "<%= txtCollapseStatu2.ClientID %>").val().toString() == "1")
                _cs2 = false;
            else
                _cs2 = true;

            if ($("#" + "<%= txtCollapseStatu3.ClientID %>").val().toString() == "1")
                _cs3 = false;
            else
                _cs3 = true;

            if ($("#" + "<%= txtCollapseStatu4.ClientID %>").val().toString() == "1")
                _cs4 = false;
            else
                _cs4 = true;

            if ($("#" + "<%= txtCollapseStatu5.ClientID %>").val().toString() == "1")
                _cs5 = false;
            else
                _cs5 = true;

            if ($("#" + "<%= txtCollapseStatu6.ClientID %>").val().toString() == "1")
                _cs6 = false;
            else
                _cs6 = true;

            if ($("#" + "<%= txtCollapseStatu7.ClientID %>").val().toString() == "1")
                _cs7 = false;
            else
                _cs7 = true;

            if ($("#" + "<%= txtCollapseStatu8.ClientID %>").val().toString() == "1")
                _cs8 = false;
            else
                _cs8 = true;

            if ($("#" + "<%= txtCollapseStatu9.ClientID %>").val().toString() == "1")
                _cs9 = false;
            else
                _cs9 = true;

            if ($("#" + "<%= txtCollapseStatu10.ClientID %>").val().toString() == "1")
                _cs10 = false;
            else
                _cs10 = true;

            if ($("#" + "<%= txtCollapseStatu11.ClientID %>").val().toString() == "1")
                _cs11 = false;
            else
                _cs11 = true;

            if ($("#" + "<%= txtCollapseStatu12.ClientID %>").val().toString() == "1")
                _cs12 = false;
            else
                _cs12 = true;

            if ($("#" + "<%= txtCollapseStatu13.ClientID %>").val().toString() == "1")
                _cs13 = false;
            else
                _cs13 = true;

            if ($("#" + "<%= txtCollapseStatu14.ClientID %>").val().toString() == "1")
                _cs14 = false;
            else
                _cs14 = true;

            if ($("#" + "<%= txtCollapseStatu15.ClientID %>").val().toString() == "1")
                _cs15 = false;
            else
                _cs15 = true;



            var splitter = values.toString().split(",");

            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value = '';
            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP2').value = '';
            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP3').value = '';
            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP4').value = '';
            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP5').value = '';

            var CpVals;
            var linksize = link.split(";");
            var catchfields = $("#" + transferf).val().split(";");
            var ilk = true;
            var cfc = 0;
            for (var i = linksize.length; i < splitter.length; i++) {
                if (ilk) {
                    CpVals = catchfields[cfc] + ';' + splitter[i];
                    ilk = false;
                }
                else
                    CpVals += '|' + catchfields[cfc] + ';' + splitter[i];

                cfc++;
            }

            $("#" + CatchParamsVals).val(CpVals)
            '<%Session["CpVals"] = "' + CpVals + '"; %>';

            if (linksize.length == 1) {
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value = splitter[0];

            }
            else if (linksize.length == 2) {
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value = splitter[0];
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP2').value = splitter[1];
            }
            else if (linksize.length == 3) {
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP3').value = values[0];
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP4').value = values[1];
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP5').value = values[2];
            }

            var _O2 = false, _O3 = false, _O4 = false, _O5 = false, _O6 = false,
                _O7 = false, _O8 = false, _O9 = false, _O10 = false, _O11 = false, _O12 = false
                , _O13 = false, _O14 = false, _O15 = false;


            try { if (!rp2.GetCollapsed()) _O2 = true; rp2.SetCollapsed(true); } catch (e) { }
            try { if (!rp3.GetCollapsed()) _O3 = true; rp3.SetCollapsed(true); } catch (e) { }
            try { if (!rp4.GetCollapsed()) _O4 = true; rp4.SetCollapsed(true); } catch (e) { }
            try { if (!rp5.GetCollapsed()) _O5 = true; rp5.SetCollapsed(true); } catch (e) { }
            try { if (!rp6.GetCollapsed()) _O6 = true; rp6.SetCollapsed(true); } catch (e) { }
            try { if (!rp7.GetCollapsed()) _O7 = true; rp7.SetCollapsed(true); } catch (e) { }
            try { if (!rp8.GetCollapsed()) _O8 = true; rp8.SetCollapsed(true); } catch (e) { }
            try { if (!rp9.GetCollapsed()) _O9 = true; rp9.SetCollapsed(true); } catch (e) { }
            try { if (!rp10.GetCollapsed()) _O10 = true; rp10.SetCollapsed(true); } catch (e) { }
            try { if (!rp11.GetCollapsed()) _O11 = true; rp11.SetCollapsed(true); } catch (e) { }
            try { if (!rp12.GetCollapsed()) _O12 = true; rp12.SetCollapsed(true); } catch (e) { }
            try { if (!rp13.GetCollapsed()) _O13 = true; rp13.SetCollapsed(true); } catch (e) { }
            try { if (!rp14.GetCollapsed()) _O14 = true; rp14.SetCollapsed(true); } catch (e) { }
            try { if (!rp15.GetCollapsed()) _O15 = true; rp15.SetCollapsed(true); } catch (e) { }


            try { rp2.SetCollapsed(_cs2); } catch (e) { }
            try { rp3.SetCollapsed(_cs3); } catch (e) { }
            try { rp4.SetCollapsed(_cs4); } catch (e) { }
            try { rp5.SetCollapsed(_cs5); } catch (e) { }
            try { rp6.SetCollapsed(_cs6); } catch (e) { }
            try { rp7.SetCollapsed(_cs7); } catch (e) { }
            try { rp8.SetCollapsed(_cs8); } catch (e) { }
            try { rp9.SetCollapsed(_cs9); } catch (e) { }
            try { rp10.SetCollapsed(_cs10); } catch (e) { }
            try { rp11.SetCollapsed(_cs11); } catch (e) { }
            try { rp12.SetCollapsed(_cs12); } catch (e) { }
            try { rp13.SetCollapsed(_cs13); } catch (e) { }
            try { rp14.SetCollapsed(_cs14); } catch (e) { }
            try { rp15.SetCollapsed(_cs15); } catch (e) { }


            try { if (_O2 & _cs2) rp2.SetCollapsed(false); } catch (e) { }
            try { if (_O3 & _cs3) rp3.SetCollapsed(false); } catch (e) { }
            try { if (_O4 & _cs4) rp4.SetCollapsed(false); } catch (e) { }
            try { if (_O5 & _cs5) rp5.SetCollapsed(false); } catch (e) { }
            try { if (_O6 & _cs6) rp6.SetCollapsed(false); } catch (e) { }
            try { if (_O7 & _cs7) rp7.SetCollapsed(false); } catch (e) { }
            try { if (_O8 & _cs8) rp8.SetCollapsed(false); } catch (e) { }
            try { if (_O9 & _cs9) rp9.SetCollapsed(false); } catch (e) { }
            try { if (_O10 & _cs10) rp10.SetCollapsed(false); } catch (e) { }
            try { if (_O11 & _cs11) rp11.SetCollapsed(false); } catch (e) { }
            try { if (_O12 & _cs12) rp12.SetCollapsed(false); } catch (e) { }
            try { if (_O13 & _cs13) rp13.SetCollapsed(false); } catch (e) { }
            try { if (_O14 & _cs14) rp14.SetCollapsed(false); } catch (e) { }
            try { if (_O15 & _cs15) rp15.SetCollapsed(false); } catch (e) { }



            try {

                var objf1no = $("#" + objf1).val();
                var objISCardVal = $("#" + objIsCard).val();
                if (objISCardVal == "1") {
                    if (objf1no == "J50010" || objf1no == "J50030") {
                        var link1 = document.getElementById('ctl00_ContentPlaceHolder1_HiddenpageType').value.replace("FaturaOnGiris.aspx", "FaturaCard/Satinalma.aspx");
                        //alert(CpVals);
                        if (CpVals.includes("R0037;1") || CpVals.includes("R0037;4"))
                            document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rp1_iframeView').src = AdGIdToLink(document.getElementById('ctl00_ContentPlaceHolder1_HiddenpageType').value + splitter[0]);
                        else
                            document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rp1_iframeView').src = AdGIdToLink(link1 + splitter[0]);


                    }
                    else
                        document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rp1_iframeView').src = AdGIdToLink(document.getElementById('ctl00_ContentPlaceHolder1_HiddenpageType').value + splitter[0]);
                }
                var pane = xsplitter.GetPaneByName('MiddleContainer');
                if (pane.IsCollapsed())
                    document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_1_S_CB_Img').click();
                //LeftContainer
            } catch (e) {

            }

            SetDynamicHeaders();

        }

        function DoHeaders1(values) {
            try {

                var _HVal = $("#" + "<%= txtDHT1.ClientID %>").val();
                rp1.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }

        function DoHeaders2(values) {
            try {
                var _HVal = $("#" + "<%= txtDHT2.ClientID %>").val();
                rp2.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }

        function DoHeaders3(values) {
            try {
                var _HVal = $("#" + "<%= txtDHT3.ClientID %>").val();
                rp3.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }

        function DoHeaders4(values) {
            try {
                var _HVal = $("#" + "<%= txtDHT4.ClientID %>").val();
                rp4.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }

        function DoHeaders5(values) {
            try {
                var _HVal = $("#" + "<%= txtDHT5.ClientID %>").val();
                rp5.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }

        function DoHeaders6(values) {
            try {
                var _HVal = $("#" + "<%= txtDHT6.ClientID %>").val();
                rp6.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }

        function DoHeaders7(values) {
            try {
                var _HVal = $("#" + "<%= txtDHT7.ClientID %>").val();
                rp7.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }

        function DoHeaders8(values) {
            try {
                var _HVal = $("#" + "<%= txtDHT8.ClientID %>").val();
                rp8.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }

        function DoHeaders9(values) {
            try {
                var _HVal = $("#" + "<%= txtDHT9.ClientID %>").val();
                rp9.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }

        function DoHeaders10(values) {
            try {
                var _HVal = $("#" + "<%= txtDHT10.ClientID %>").val();
                rp10.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }

        function DoHeaders11(values) {
            try {
                var _HVal = $("#" + "<%= txtDHT11.ClientID %>").val();
                rp11.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }

        function DoHeaders12(values) {
            try {
                var _HVal = $("#" + "<%= txtDHT12.ClientID %>").val();
                rp12.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }

        function DoHeaders13(values) {
            try {
                var _HVal = $("#" + "<%= txtDHT13.ClientID %>").val();
                rp13.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }

        function DoHeaders14(values) {
            try {
                var _HVal = $("#" + "<%= txtDHT14.ClientID %>").val();
                rp14.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }

        function DoHeaders15(values) {
            try {
                var _HVal = $("#" + "<%= txtDHT15.ClientID %>").val();
                rp15.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }


        function OnGetRowValuesJob(values) {
            //alert(values);
            var splitter = values.split(';');
            var targetLink = "";
            if (splitter[1] == "1") {
                window.open("/Jobs/mainJobs.aspx?MJID=" + splitter[0]);
            }
            else {

                window.open("/projeolustur.aspx?Tur=1&ProjeNo=" + splitter[0]);
            }
        }


        var postponedCallbackRequired = false;
        function OnListBoxIndexChanged(values) {
            if (iframecallbackPanel.InCallback())
                postponedCallbackRequired = true;
            else
                iframecallbackPanel.PerformCallback(values);
        }
        function OnEndCallback(values) {

            if (postponedCallbackRequired) {
                iframecallbackPanel.PerformCallback(values);
                postponedCallbackRequired = false;
            }
        }

        function OnRefreshAllObjects(s, e, i) {
            var objIsCard = "<%= txtIsCard.ClientID %>";
            var objISCardVal = $("#" + objIsCard).val();
            if (s.cpIsUpdated) {


                try {

                    var _val2 = s.cpQval;
                    if (_val2.length > 0) {
                        var insertarray = _val2.split("|");
                        if (insertarray.length == 1)
                            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value = insertarray[0];
                        else {
                            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value = insertarray[0];
                            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP2').value = insertarray[1];
                        }
                    }

                } catch (e) {

                }

                try {

                    var _val3 = s.cpMsg;
                    if (_val3.length > 0)
                        alert(_val3.replace("'", " "));

                } catch (e) {

                }

                var ferobjs = $("#ctl00_ContentPlaceHolder1_txtRef" + i.toString()).val();

                var cpvalarray = ferobjs.split(';');
                var _val = '';

                for (var j = 0; j < 16; j++) {

                    checkRefVal = j.toString();
                    _val = cpvalarray.find(checkRefObjs);
                    if (_val != null) {
                        if (_val.length > 0) {
                            if (j == 0)
                                try {
                                    if (objISCardVal == "1") document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rp1_iframeView').src = document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rp1_iframeView').src;
                                } catch (err) { }
                            else if (j == 1)
                                try { grid1.PerformCallback(''); } catch (err) { }
                            else if (j == 2)
                                try { grid2.PerformCallback(''); } catch (err) { }
                            else if (j == 3)
                                try { grid3.PerformCallback(''); } catch (err) { }
                            else if (j == 4)
                                try { grid4.PerformCallback(''); } catch (err) { }
                            else if (j == 5)
                                try { grid5.PerformCallback(''); } catch (err) { }
                            else if (j == 6)
                                try { grid6.PerformCallback(''); } catch (err) { }
                            else if (j == 7)
                                try { grid7.PerformCallback(''); } catch (err) { }
                            else if (j == 8)
                                try { grid8.PerformCallback(''); } catch (err) { }
                            else if (j == 9)
                                try { grid9.PerformCallback(''); } catch (err) { }
                            else if (j == 10)
                                try { grid10.PerformCallback(''); } catch (err) { }
                            else if (j == 11)
                                try { grid11.PerformCallback(''); } catch (err) { }
                            else if (j == 12)
                                try { grid12.PerformCallback(''); } catch (err) { }
                            else if (j == 13)
                                try { grid13.PerformCallback(''); } catch (err) { }
                            else if (j == 14)
                                try { grid14.PerformCallback(''); } catch (err) { }
                            else if (j == 15)
                                try { grid15.PerformCallback(''); } catch (err) { }


                        }
                    }


                }

                if (i == 1)
                    try { grid1.PerformCallback(''); } catch (err) { }
                else if (i == 2)
                    try { grid2.PerformCallback(''); } catch (err) { }
                else if (i == 3)
                    try { grid3.PerformCallback(''); } catch (err) { }
                else if (i == 4)
                    try { grid4.PerformCallback(''); } catch (err) { }
                else if (i == 5)
                    try { grid5.PerformCallback(''); } catch (err) { }
                else if (i == 6)
                    try { grid6.PerformCallback(''); } catch (err) { }
                else if (i == 7)
                    try { grid7.PerformCallback(''); } catch (err) { }
                else if (i == 8)
                    try { grid8.PerformCallback(''); } catch (err) { }
                else if (i == 9)
                    try { grid9.PerformCallback(''); } catch (err) { }
                else if (i == 10)
                    try { grid10.PerformCallback(''); } catch (err) { }
                else if (i == 11)
                    try { grid11.PerformCallback(''); } catch (err) { }
                else if (i == 12)
                    try { grid12.PerformCallback(''); } catch (err) { }
                else if (i == 13)
                    try { grid13.PerformCallback(''); } catch (err) { }
                else if (i == 14)
                    try { grid14.PerformCallback(''); } catch (err) { }
                else if (i == 15)
                    try { grid15.PerformCallback(''); } catch (err) { }


            }
            if (s.cpIsLinked) {
                OpenSelectDirect(s.cpLink, s.cpQueryStrings, s.cpIslemId);
            }
        }

        function OpenSelectDirect(link, filterobjs, IslemId) {
            var array1 = filterobjs.split(',');
            var sayac = 1;
            var sayval = '';
            var _val = '';
            var P1 = document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value;
            var P2 = document.getElementById('ctl00_ContentPlaceHolder1_HiddenP2').value;
            var CatchParamsVals = "<%= txtCatchParamsAndValues.ClientID %>";
            var cpvals = $("#" + CatchParamsVals).val();
            var cpvalarray = cpvals.split('|');
            //alert(cpvals);
            array1.forEach(function (element) {
                _val = '';
                sayval = ReturnSayacVal(sayac);
                if (element == 'P1')
                    link = link.replace(sayval, P1);
                else if (element == 'P2')
                    link = link.replace(sayval, P2);
                else {
                    //alert(element);
                    checkval = element;
                    _val = cpvalarray.find(checkParamVal);
                    //alert(_val);
                    link = link.replace(sayval, _val.split(';')[1]);
                }

                sayac = sayac + 1;
            });

            link = link + "&IslemId=" + IslemId;

            var popwin = window.open(AdGIdToLink(link), '_blank');
            popwin.focus();
        }

        function RefreshAllCollapse() {
            var objIsCard = "<%= txtIsCard.ClientID %>";
            var objISCardVal = $("#" + objIsCard).val();
            try { grid1.PerformCallback(''); } catch (err) { }
            try { grid2.PerformCallback(''); } catch (err) { }
            try { grid3.PerformCallback(''); } catch (err) { }
            try { grid4.PerformCallback(''); } catch (err) { }
            try { grid5.PerformCallback(''); } catch (err) { }
            try { grid6.PerformCallback(''); } catch (err) { }
            try { grid7.PerformCallback(''); } catch (err) { }
            try { grid8.PerformCallback(''); } catch (err) { }
            try { grid9.PerformCallback(''); } catch (err) { }
            try { grid10.PerformCallback(''); } catch (err) { }
            try { grid11.PerformCallback(''); } catch (err) { }
            try { grid12.PerformCallback(''); } catch (err) { }
            try { grid13.PerformCallback(''); } catch (err) { }
            try { grid14.PerformCallback(''); } catch (err) { }
            try { grid15.PerformCallback(''); } catch (err) { }
            if (objISCardVal == "1") document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rp1_iframeView').src = document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rp1_iframeView').src;
        }

        function RefreshFrame(_value) {
            try {
                var objIsCard = "<%= txtIsCard.ClientID %>";
                var objISCardVal = $("#" + objIsCard).val();
                if (objISCardVal == "1") document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rp1_iframeView').src = AdGIdToLink(document.getElementById('ctl00_ContentPlaceHolder1_HiddenpageType').value + _value);
            } catch (e) {

            }
        }



        function ShowModalPopup() {
            $("#" + "<%= txtR0011s.ClientID %>").val('');
            $("#" + "<%= txtR0016s.ClientID %>").val('');
            Writegrid1values();

        }

        function Writegrid1values() {
            if (grid1.GetSelectedRowCount() == 0) {
                alert('Please select booking, and try again!');
                return;
            }
            grid1.GetSelectedFieldValues('R0016', WriteR016);
            grid1.GetSelectedFieldValues('R0011', WriteR011);

        }

        function WriteR011(values) {
            var _vals;
            var ilk = true;
            for (var i = 0; i < values.length; i++) {
                if (ilk) {
                    _vals = values[i];
                    ilk = false;
                }
                else
                    _vals += ',' + values[i];

            }

            $("#" + "<%= txtR0011s.ClientID %>").val(_vals);
            Pop.PerformCallback($("#" + "<%= txtR0011s.ClientID %>").val() + '|' + $("#" + "<%= txtR0016s.ClientID %>").val());
            Pop.Show();


        }

        function WriteR016(values) {
            var _vals;
            for (var i = 0; i < values.length; i++) {
                _vals = values[i];
                if (_vals.length > 0) break;

            }

            $("#" + "<%= txtR0016s.ClientID %>").val(_vals);
        }

    </script>

    <dx:ASPxSplitter Theme="iOS" ID="ASPxSplitter1" ClientInstanceName="xsplitter" runat="server" Height="100%" Width="100%">
        <Panes>
            <dx:SplitterPane ShowCollapseBackwardButton="True" ShowSeparatorImage="True" Size="25%" Name="LeftContainer">
                <Panes>
                    <dx:SplitterPane ShowCollapseBackwardButton="True" ShowSeparatorImage="True" Size="80%" Name="Leftinside">
                        <Panes>
                            <dx:SplitterPane Size="90%" ScrollBars="Auto">
                                <ContentCollection>
                                    <dx:SplitterContentControl ID="SPContent" runat="server">
                                        <table>
                                            <tr>
                                                <td style="width: 70%; height: 100%">
                                                    <dx:ASPxRadioButtonList RepeatDirection="Horizontal" Theme="Glass" runat="server" Width="100%" ID="rblDynamicList"
                                                        OnSelectedIndexChanged="rblDynamicList_SelectedIndexChanged" AutoPostBack="true">
                                                    </dx:ASPxRadioButtonList>

                                                </td>
                                                <td style="width: 30%; height: 100%">
                                                    <dx:ASPxButton ID="btnPopup" runat="server" Text="Create Due Invoice" AutoPostBack="false">
                                                        <ClientSideEvents Click="function(s, e) { ShowModalPopup(); }" />
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                        <dx:ASPxMenu ID="menu1" runat="server" AutoSeparators="RootOnly"
                                            Theme="Glass"
                                            ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                            ShowSubMenuShadow="False" OnItemClick="menu1_ItemClick">
                                            <SubMenuStyle GutterWidth="0px" />
                                            <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                            <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                            </SubMenuItemStyle>
                                            <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                        </dx:ASPxMenu>
                                        <dx:ASPxGridView ID="grid1" ClientInstanceName="grid1" Caption="" runat="server" AutoGenerateColumns="false"
                                            Theme="Glass" EnableTheming="false" DataSourceID="dtgrid1" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                            OnCellEditorInitialize="grid1_CellEditorInitialize" EnableRowsCache="false" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                            Settings-HorizontalScrollBarMode="Auto" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback"
                                            OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnRowDeleting="grid_RowDeleting" OnCustomCallback="grid1_CustomCallback">
                                            <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,1)}" RowDblClick="function(s, e) {  OnGridFocusedRowChanged(s,e,'1');} " />
                                            <Settings />
                                            <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" AllowFocusedRow="true" />
                                            <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                            <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                                            <SettingsCommandButton>
                                                <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                </CancelButton>
                                                <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                </UpdateButton>
                                            </SettingsCommandButton>
                                            <SettingsText EmptyDataRow=" " />
                                            <Styles>
                                                <Cell Paddings-Padding="1"></Cell>
                                                <Header ForeColor="White"></Header>
                                                <HeaderPanel ForeColor="White"></HeaderPanel>
                                                <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                            </Styles>
                                            <SettingsText EmptyDataRow=" " />

                                        </dx:ASPxGridView>

                                        <dx:ASPxGridViewExporter ID="gridExport1" ExportedRowType="All" runat="server" GridViewID="grid1"></dx:ASPxGridViewExporter>
                                    </dx:SplitterContentControl>
                                </ContentCollection>
                            </dx:SplitterPane>
                        </Panes>
                    </dx:SplitterPane>
                </Panes>
            </dx:SplitterPane>
            <dx:SplitterPane ShowCollapseForwardButton="True" ShowSeparatorImage="True" Size="65%" Name="MiddleContainer" ScrollBars="Auto">
                <Panes>
                    <dx:SplitterPane ShowCollapseBackwardButton="True" ShowSeparatorImage="True" Size="80%" Name="Middleinside">
                        <ContentCollection>
                            <dx:SplitterContentControl>
                                <dx:ASPxRoundPanel ID="rp1" Theme="Glass" Collapsed="false" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rp1"
                                    runat="server" Width="100%" Height="80%">
                                    <PanelCollection>
                                        <dx:PanelContent>
                                            <%--<dx:ASPxCallbackPanel ID="iframecallbackPanel" ClientInstanceName="iframecallbackPanel" Height="100%"
                                                runat="server" OnCallback="iframecallbackPanel_Callback">
                                                <PanelCollection>
                                                    <dx:PanelContent>--%>
                                            <iframe id="iframeView" width="100%" height="100%" runat="server"></iframe>
                                            <%-- </dx:PanelContent>
                                                </PanelCollection>
                                            </dx:ASPxCallbackPanel>--%>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){  
                                            if(rp1.GetCollapsed()){                                                  
                                                document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rp1_iframeView').style.display = 'none';
                                            }
                                            else {
                                               document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rp1_iframeView').style.display = 'block';
                                          }
                                        }" />
                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rp2" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rp2" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent>
                                            <dx:ASPxMenu ID="menu2" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu2_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid2" ClientInstanceName="grid2" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid2_CustomCallback" OnCellEditorInitialize="grid2_CellEditorInitialize"
                                                EnableTheming="false" DataSourceID="dtgrid2" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnRowDeleting="grid_RowDeleting"
                                                OnAfterPerformCallback="grid_AfterPerformCallback" SettingsText-PopupEditFormCaption=" ">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,2)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'2');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" AllowFocusedRow="true" />
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport2" ExportedRowType="All" runat="server" GridViewID="grid2"></dx:ASPxGridViewExporter>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){ 
                                            if(!rp2.GetCollapsed()){
                                                grid2.PerformCallback('');
                                            }
                                        }" />
                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rp3" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rp3" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent>
                                            <dx:ASPxMenu ID="menu3" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu3_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid3" ClientInstanceName="grid3" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid3_CustomCallback" OnCellEditorInitialize="grid3_CellEditorInitialize"
                                                EnableTheming="false" DataSourceID="dtgrid3" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,3)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'3');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" AllowFocusedRow="true" />
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport3" ExportedRowType="All" runat="server" GridViewID="grid3"></dx:ASPxGridViewExporter>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){ 
                                            if(!rp3.GetCollapsed()){
                                                grid3.PerformCallback('');
                                            }
                                        }" />
                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rp4" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rp4" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent>
                                            <dx:ASPxMenu ID="menu4" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu4_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid4" ClientInstanceName="grid4" Caption="" runat="server" AutoGenerateColumns="False"
                                                EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid4_CustomCallback"
                                                EnableTheming="false" DataSourceID="dtgrid4" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,4)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'4');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" AllowFocusedRow="true" />
                                                <SettingsPager PageSize="7" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport4" ExportedRowType="All" runat="server" GridViewID="grid4"></dx:ASPxGridViewExporter>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){ 
                                            if(!rp4.GetCollapsed()){
                                                grid4.PerformCallback('');
                                            }
                                        }" />
                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rp5" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rp5" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent>
                                            <dx:ASPxMenu ID="menu5" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu5_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid5" ClientInstanceName="grid5" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid5_CustomCallback" OnCellEditorInitialize="grid5_CellEditorInitialize"
                                                EnableTheming="false" DataSourceID="dtgrid5" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,5)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'5');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" AllowFocusedRow="true" />
                                                <SettingsPager PageSize="7" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport5" ExportedRowType="All" runat="server" GridViewID="grid5"></dx:ASPxGridViewExporter>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){ 
                                             if(!rp5.GetCollapsed()){
                                                grid5.PerformCallback('');
                                            }
                                        }" />
                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rp6" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rp6" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent SupportsDisabledAttribute="True" runat="server">
                                            <dx:ASPxMenu ID="menu6" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu6_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid6" ClientInstanceName="grid6" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid6_CustomCallback" OnCellEditorInitialize="grid6_CellEditorInitialize" Width="100%"
                                                EnableTheming="false" DataSourceID="dtgrid6" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback"
                                                OnHtmlRowPrepared="grid_HtmlRowPrepared" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,6)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'6');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" AllowFocusedRow="true" />
                                                <SettingsPager PageSize="7" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>

                                                </SettingsCommandButton>
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <Styles>
                                                    <Cell Paddings-Padding="1"></Cell>

                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                                <Columns>
                                                    <dx:GridViewDataColumn Width="100px" Caption="Temp1R0030" Visible="true" Name="R0030" FieldName="R0030" VisibleIndex="1">
                                                        <EditItemTemplate>
                                                            <dx:ASPxComboBox ID="cmbR0030" Value='<%# Eval("R0030")%>' DropDownStyle="DropDownList"
                                                                Theme="Glass" Width="100%" runat="server" EnableCallbackMode="true" CallbackPageSize="20">
                                                            </dx:ASPxComboBox>
                                                        </EditItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Width="100px" Caption="Temp1R0032" Visible="true" Name="R0032" FieldName="R0032" VisibleIndex="2">
                                                        <EditItemTemplate>
                                                            <dx:ASPxComboBox ID="cmbR0032" Value='<%# Eval("R0032")%>' DropDownStyle="DropDownList"
                                                                Theme="Glass" Width="100%" runat="server" EnableCallbackMode="true" CallbackPageSize="20">
                                                            </dx:ASPxComboBox>
                                                        </EditItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Width="100px" Caption="Temp1R0034" Visible="true" Name="R0034" FieldName="R0034" VisibleIndex="3">
                                                        <EditItemTemplate>
                                                            <dx:ASPxComboBox ID="cmbR0034" Value='<%# Eval("R0034")%>' DropDownStyle="DropDownList"
                                                                Theme="Glass" Width="100%" runat="server" EnableCallbackMode="true" CallbackPageSize="20">
                                                            </dx:ASPxComboBox>
                                                        </EditItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Width="100px" Caption="Temp1R0036" Visible="true" Name="R0036" FieldName="R0036" VisibleIndex="4">
                                                        <EditItemTemplate>
                                                            <dx:ASPxComboBox ID="cmbR0036" Value='<%# Eval("R0036")%>' DropDownStyle="DropDownList"
                                                                Theme="Glass" Width="100%" runat="server" EnableCallbackMode="true" CallbackPageSize="20">
                                                            </dx:ASPxComboBox>
                                                        </EditItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Width="100px" Caption="Temp1R0028" Visible="true" Name="R0028" FieldName="R0028" VisibleIndex="5">
                                                        <EditItemTemplate>
                                                            <dx:ASPxComboBox ID="cmbR0028" Value='<%# Eval("R0028")%>' DropDownStyle="DropDownList"
                                                                Theme="Glass" Width="100%" runat="server" EnableCallbackMode="true" CallbackPageSize="20">
                                                            </dx:ASPxComboBox>
                                                        </EditItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                </Columns>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport6" ExportedRowType="All" runat="server" GridViewID="grid6"></dx:ASPxGridViewExporter>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){  
                                            if(!rp6.GetCollapsed()){                                                  
                                                 grid6.PerformCallback('');
                                            } 
                                        }" />
                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rp7" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rp7" HeaderText="TASKS" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent SupportsDisabledAttribute="True" runat="server">

                                            <dx:ASPxMenu ID="menu7" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu7_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid7" ClientInstanceName="grid7" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid7_CustomCallback" OnCellEditorInitialize="grid_CellEditorInitialize"
                                                EnableTheming="false" DataSourceID="dtgrid7" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,7)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'7');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" />
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport7" ExportedRowType="All" runat="server" GridViewID="grid7"></dx:ASPxGridViewExporter>

                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){  
                                             if(!rp7.GetCollapsed()){     
                                                 grid7.PerformCallback('');                                       
                                            } 
                                        }" />
                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rp8" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rp8" HeaderText="TASKS" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent SupportsDisabledAttribute="True" runat="server">

                                            <dx:ASPxMenu ID="menu8" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu8_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid8" ClientInstanceName="grid8" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid8_CustomCallback" OnCellEditorInitialize="grid_CellEditorInitialize"
                                                EnableTheming="false" DataSourceID="dtgrid8" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,8)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'8');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" />
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport8" ExportedRowType="All" runat="server" GridViewID="grid8"></dx:ASPxGridViewExporter>

                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){  
                                             if(!rp8.GetCollapsed()){  
                                                 grid8.PerformCallback('');                                    
                                            } 
                                        }" />
                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rp9" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rp9" HeaderText="TASKS" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent SupportsDisabledAttribute="True" runat="server">

                                            <dx:ASPxMenu ID="menu9" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu9_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid9" ClientInstanceName="grid9" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid9_CustomCallback" OnCellEditorInitialize="grid_CellEditorInitialize"
                                                EnableTheming="false" DataSourceID="dtgrid9" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,9)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'9');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" />
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport9" ExportedRowType="All" runat="server" GridViewID="grid9"></dx:ASPxGridViewExporter>

                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){  
                                             if(!rp9.GetCollapsed()){      
                                                 grid9.PerformCallback('');                                        
                                            } 
                                        }" />
                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rp10" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rp10" HeaderText="TASKS" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent SupportsDisabledAttribute="True" runat="server">

                                            <dx:ASPxMenu ID="menu10" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu10_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid10" ClientInstanceName="grid10" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid10_CustomCallback" OnCellEditorInitialize="grid_CellEditorInitialize"
                                                EnableTheming="false" DataSourceID="dtgrid10" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,10)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'10');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" />
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport10" ExportedRowType="All" runat="server" GridViewID="grid10"></dx:ASPxGridViewExporter>

                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){  
                                             if(!rp10.GetCollapsed()){      
                                                 grid10.PerformCallback('');                                        
                                            } 
                                        }" />
                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rp11" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rp11" HeaderText="TASKS" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent SupportsDisabledAttribute="True" runat="server">

                                            <dx:ASPxMenu ID="menu11" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu11_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid11" ClientInstanceName="grid11" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid11_CustomCallback" OnCellEditorInitialize="grid_CellEditorInitialize"
                                                EnableTheming="false" DataSourceID="dtgrid11" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,11)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'11');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" />
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport11" ExportedRowType="All" runat="server" GridViewID="grid11"></dx:ASPxGridViewExporter>

                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){  
                                             if(!rp11.GetCollapsed()){      
                                                 grid11.PerformCallback('');                                        
                                            } 
                                        }" />
                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rp12" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rp12" HeaderText="TASKS" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent SupportsDisabledAttribute="True" runat="server">

                                            <dx:ASPxMenu ID="menu12" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu12_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid12" ClientInstanceName="grid12" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid12_CustomCallback" OnCellEditorInitialize="grid_CellEditorInitialize"
                                                EnableTheming="false" DataSourceID="dtgrid12" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,12)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'12');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" />
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport12" ExportedRowType="All" runat="server" GridViewID="grid12"></dx:ASPxGridViewExporter>

                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){  
                                             if(!rp12.GetCollapsed()){      
                                                 grid12.PerformCallback('');                                        
                                            } 
                                        }" />
                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rp13" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rp13" HeaderText="TASKS" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent SupportsDisabledAttribute="True" runat="server">

                                            <dx:ASPxMenu ID="menu13" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu13_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid13" ClientInstanceName="grid13" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid13_CustomCallback" OnCellEditorInitialize="grid_CellEditorInitialize"
                                                EnableTheming="false" DataSourceID="dtgrid13" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,13)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'13');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" />
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport13" ExportedRowType="All" runat="server" GridViewID="grid13"></dx:ASPxGridViewExporter>

                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){  
                                             if(!rp13.GetCollapsed()){      
                                                 grid13.PerformCallback('');                                        
                                            } 
                                        }" />
                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rp14" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rp14" HeaderText="TASKS" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent SupportsDisabledAttribute="True" runat="server">

                                            <dx:ASPxMenu ID="menu14" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu14_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid14" ClientInstanceName="grid14" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid14_CustomCallback" OnCellEditorInitialize="grid_CellEditorInitialize"
                                                EnableTheming="false" DataSourceID="dtgrid14" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,14)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'14');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" />
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport14" ExportedRowType="All" runat="server" GridViewID="grid14"></dx:ASPxGridViewExporter>

                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){  
                                             if(!rp14.GetCollapsed()){      
                                                 grid14.PerformCallback('');                                        
                                            } 
                                        }" />
                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rp15" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rp15" HeaderText="TASKS" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent SupportsDisabledAttribute="True" runat="server">

                                            <dx:ASPxMenu ID="menu15" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu15_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid15" ClientInstanceName="grid15" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid15_CustomCallback" OnCellEditorInitialize="grid_CellEditorInitialize"
                                                EnableTheming="false" DataSourceID="dtgrid15" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,15)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'15');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" />
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport15" ExportedRowType="All" runat="server" GridViewID="grid15"></dx:ASPxGridViewExporter>

                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){  
                                             if(!rp15.GetCollapsed()){      
                                                 grid15.PerformCallback('');                                        
                                            } 
                                        }" />
                                </dx:ASPxRoundPanel>
                            </dx:SplitterContentControl>
                        </ContentCollection>
                    </dx:SplitterPane>
                </Panes>
            </dx:SplitterPane>


        </Panes>
    </dx:ASPxSplitter>
    <STDT:StDataTable ID="dtgrid1" runat="server" />
    <STDT:StDataTable ID="dtgrid2" runat="server" />
    <STDT:StDataTable ID="dtgrid3" runat="server" />
    <STDT:StDataTable ID="dtgrid4" runat="server" />
    <STDT:StDataTable ID="dtgrid5" runat="server" />
    <STDT:StDataTable ID="dtgrid6" runat="server" />
    <STDT:StDataTable ID="dtgrid7" runat="server" />
    <STDT:StDataTable ID="dtgrid8" runat="server" />
    <STDT:StDataTable ID="dtgrid9" runat="server" />
    <STDT:StDataTable ID="dtgrid10" runat="server" />
    <STDT:StDataTable ID="dtgrid11" runat="server" />
    <STDT:StDataTable ID="dtgrid12" runat="server" />
    <STDT:StDataTable ID="dtgrid13" runat="server" />
    <STDT:StDataTable ID="dtgrid14" runat="server" />
    <STDT:StDataTable ID="dtgrid15" runat="server" />
    <STDT:StDataTable ID="CmbS1" runat="server" />
    <STDT:StDataTable ID="CmbS2" runat="server" />
    <STDT:StDataTable ID="CmbS3" runat="server" />
    <STDT:StDataTable ID="CmbS4" runat="server" />
    <STDT:StDataTable ID="CmbS5" runat="server" />

    <STDT:StDataTable ID="DS11" runat="server" />
    <STDT:StDataTable ID="DS12" runat="server" />
    <STDT:StDataTable ID="DS13" runat="server" />
    <STDT:StDataTable ID="DS14" runat="server" />
    <STDT:StDataTable ID="DS15" runat="server" />
    <STDT:StDataTable ID="DS16" runat="server" />
    <STDT:StDataTable ID="DS17" runat="server" />
    <STDT:StDataTable ID="DS18" runat="server" />
    <STDT:StDataTable ID="DS19" runat="server" />
    <STDT:StDataTable ID="DS110" runat="server" />
    <STDT:StDataTable ID="DS111" runat="server" />
    <STDT:StDataTable ID="DS112" runat="server" />
    <STDT:StDataTable ID="DS113" runat="server" />
    <STDT:StDataTable ID="DS114" runat="server" />
    <STDT:StDataTable ID="DS115" runat="server" />
    <STDT:StDataTable ID="DS116" runat="server" />
    <STDT:StDataTable ID="DS117" runat="server" />
    <STDT:StDataTable ID="DS118" runat="server" />
    <STDT:StDataTable ID="DS119" runat="server" />
    <STDT:StDataTable ID="DS120" runat="server" />

    <STDT:StDataTable ID="DS21" runat="server" />
    <STDT:StDataTable ID="DS22" runat="server" />
    <STDT:StDataTable ID="DS23" runat="server" />
    <STDT:StDataTable ID="DS24" runat="server" />
    <STDT:StDataTable ID="DS25" runat="server" />
    <STDT:StDataTable ID="DS26" runat="server" />
    <STDT:StDataTable ID="DS27" runat="server" />
    <STDT:StDataTable ID="DS28" runat="server" />
    <STDT:StDataTable ID="DS29" runat="server" />
    <STDT:StDataTable ID="DS210" runat="server" />
    <STDT:StDataTable ID="DS211" runat="server" />
    <STDT:StDataTable ID="DS212" runat="server" />
    <STDT:StDataTable ID="DS213" runat="server" />
    <STDT:StDataTable ID="DS214" runat="server" />
    <STDT:StDataTable ID="DS215" runat="server" />
    <STDT:StDataTable ID="DS216" runat="server" />
    <STDT:StDataTable ID="DS217" runat="server" />
    <STDT:StDataTable ID="DS218" runat="server" />
    <STDT:StDataTable ID="DS219" runat="server" />
    <STDT:StDataTable ID="DS220" runat="server" />
    <STDT:StDataTable ID="DS221" runat="server" />
    <STDT:StDataTable ID="DS222" runat="server" />


    <STDT:StDataTable ID="DS31" runat="server" />
    <STDT:StDataTable ID="DS32" runat="server" />
    <STDT:StDataTable ID="DS33" runat="server" />
    <STDT:StDataTable ID="DS34" runat="server" />
    <STDT:StDataTable ID="DS35" runat="server" />
    <STDT:StDataTable ID="DS36" runat="server" />
    <STDT:StDataTable ID="DS37" runat="server" />
    <STDT:StDataTable ID="DS38" runat="server" />
    <STDT:StDataTable ID="DS39" runat="server" />
    <STDT:StDataTable ID="DS310" runat="server" />
    <STDT:StDataTable ID="DS311" runat="server" />
    <STDT:StDataTable ID="DS312" runat="server" />
    <STDT:StDataTable ID="DS313" runat="server" />
    <STDT:StDataTable ID="DS314" runat="server" />
    <STDT:StDataTable ID="DS315" runat="server" />
    <STDT:StDataTable ID="DS316" runat="server" />
    <STDT:StDataTable ID="DS317" runat="server" />
    <STDT:StDataTable ID="DS318" runat="server" />
    <STDT:StDataTable ID="DS319" runat="server" />
    <STDT:StDataTable ID="DS320" runat="server" />

    <STDT:StDataTable ID="DS41" runat="server" />
    <STDT:StDataTable ID="DS42" runat="server" />
    <STDT:StDataTable ID="DS43" runat="server" />
    <STDT:StDataTable ID="DS44" runat="server" />
    <STDT:StDataTable ID="DS45" runat="server" />
    <STDT:StDataTable ID="DS46" runat="server" />
    <STDT:StDataTable ID="DS47" runat="server" />
    <STDT:StDataTable ID="DS48" runat="server" />
    <STDT:StDataTable ID="DS49" runat="server" />
    <STDT:StDataTable ID="DS410" runat="server" />
    <STDT:StDataTable ID="DS411" runat="server" />
    <STDT:StDataTable ID="DS412" runat="server" />
    <STDT:StDataTable ID="DS413" runat="server" />
    <STDT:StDataTable ID="DS414" runat="server" />
    <STDT:StDataTable ID="DS415" runat="server" />
    <STDT:StDataTable ID="DS416" runat="server" />
    <STDT:StDataTable ID="DS417" runat="server" />
    <STDT:StDataTable ID="DS418" runat="server" />
    <STDT:StDataTable ID="DS419" runat="server" />
    <STDT:StDataTable ID="DS420" runat="server" />

    <STDT:StDataTable ID="DS51" runat="server" />
    <STDT:StDataTable ID="DS52" runat="server" />
    <STDT:StDataTable ID="DS53" runat="server" />
    <STDT:StDataTable ID="DS54" runat="server" />
    <STDT:StDataTable ID="DS55" runat="server" />
    <STDT:StDataTable ID="DS56" runat="server" />
    <STDT:StDataTable ID="DS57" runat="server" />
    <STDT:StDataTable ID="DS58" runat="server" />
    <STDT:StDataTable ID="DS59" runat="server" />
    <STDT:StDataTable ID="DS510" runat="server" />
    <STDT:StDataTable ID="DS511" runat="server" />
    <STDT:StDataTable ID="DS512" runat="server" />
    <STDT:StDataTable ID="DS513" runat="server" />
    <STDT:StDataTable ID="DS514" runat="server" />
    <STDT:StDataTable ID="DS515" runat="server" />
    <STDT:StDataTable ID="DS516" runat="server" />
    <STDT:StDataTable ID="DS517" runat="server" />
    <STDT:StDataTable ID="DS518" runat="server" />
    <STDT:StDataTable ID="DS519" runat="server" />
    <STDT:StDataTable ID="DS520" runat="server" />

    <STDT:StDataTable ID="DS61" runat="server" />
    <STDT:StDataTable ID="DS62" runat="server" />
    <STDT:StDataTable ID="DS63" runat="server" />
    <STDT:StDataTable ID="DS64" runat="server" />
    <STDT:StDataTable ID="DS65" runat="server" />
    <STDT:StDataTable ID="DS66" runat="server" />
    <STDT:StDataTable ID="DS67" runat="server" />
    <STDT:StDataTable ID="DS68" runat="server" />
    <STDT:StDataTable ID="DS69" runat="server" />
    <STDT:StDataTable ID="DS610" runat="server" />
    <STDT:StDataTable ID="DS611" runat="server" />
    <STDT:StDataTable ID="DS612" runat="server" />
    <STDT:StDataTable ID="DS613" runat="server" />
    <STDT:StDataTable ID="DS614" runat="server" />
    <STDT:StDataTable ID="DS615" runat="server" />
    <STDT:StDataTable ID="DS616" runat="server" />
    <STDT:StDataTable ID="DS617" runat="server" />
    <STDT:StDataTable ID="DS618" runat="server" />
    <STDT:StDataTable ID="DS619" runat="server" />
    <STDT:StDataTable ID="DS620" runat="server" />

    <STDT:StDataTable ID="DS71" runat="server" />
    <STDT:StDataTable ID="DS72" runat="server" />
    <STDT:StDataTable ID="DS73" runat="server" />
    <STDT:StDataTable ID="DS74" runat="server" />
    <STDT:StDataTable ID="DS75" runat="server" />
    <STDT:StDataTable ID="DS76" runat="server" />
    <STDT:StDataTable ID="DS77" runat="server" />
    <STDT:StDataTable ID="DS78" runat="server" />
    <STDT:StDataTable ID="DS79" runat="server" />
    <STDT:StDataTable ID="DS710" runat="server" />
    <STDT:StDataTable ID="DS711" runat="server" />
    <STDT:StDataTable ID="DS712" runat="server" />
    <STDT:StDataTable ID="DS713" runat="server" />
    <STDT:StDataTable ID="DS714" runat="server" />
    <STDT:StDataTable ID="DS715" runat="server" />
    <STDT:StDataTable ID="DS716" runat="server" />
    <STDT:StDataTable ID="DS717" runat="server" />
    <STDT:StDataTable ID="DS718" runat="server" />
    <STDT:StDataTable ID="DS719" runat="server" />
    <STDT:StDataTable ID="DS720" runat="server" />

    <STDT:StDataTable ID="DS81" runat="server" />
    <STDT:StDataTable ID="DS82" runat="server" />
    <STDT:StDataTable ID="DS83" runat="server" />
    <STDT:StDataTable ID="DS84" runat="server" />
    <STDT:StDataTable ID="DS85" runat="server" />
    <STDT:StDataTable ID="DS86" runat="server" />
    <STDT:StDataTable ID="DS87" runat="server" />
    <STDT:StDataTable ID="DS88" runat="server" />
    <STDT:StDataTable ID="DS89" runat="server" />
    <STDT:StDataTable ID="DS810" runat="server" />
    <STDT:StDataTable ID="DS811" runat="server" />
    <STDT:StDataTable ID="DS812" runat="server" />
    <STDT:StDataTable ID="DS813" runat="server" />
    <STDT:StDataTable ID="DS814" runat="server" />
    <STDT:StDataTable ID="DS815" runat="server" />
    <STDT:StDataTable ID="DS816" runat="server" />
    <STDT:StDataTable ID="DS817" runat="server" />
    <STDT:StDataTable ID="DS818" runat="server" />
    <STDT:StDataTable ID="DS819" runat="server" />
    <STDT:StDataTable ID="DS820" runat="server" />

    <STDT:StDataTable ID="DS91" runat="server" />
    <STDT:StDataTable ID="DS92" runat="server" />
    <STDT:StDataTable ID="DS93" runat="server" />
    <STDT:StDataTable ID="DS94" runat="server" />
    <STDT:StDataTable ID="DS95" runat="server" />
    <STDT:StDataTable ID="DS96" runat="server" />
    <STDT:StDataTable ID="DS97" runat="server" />
    <STDT:StDataTable ID="DS98" runat="server" />
    <STDT:StDataTable ID="DS99" runat="server" />
    <STDT:StDataTable ID="DS910" runat="server" />
    <STDT:StDataTable ID="DS911" runat="server" />
    <STDT:StDataTable ID="DS912" runat="server" />
    <STDT:StDataTable ID="DS913" runat="server" />
    <STDT:StDataTable ID="DS914" runat="server" />
    <STDT:StDataTable ID="DS915" runat="server" />
    <STDT:StDataTable ID="DS916" runat="server" />
    <STDT:StDataTable ID="DS917" runat="server" />
    <STDT:StDataTable ID="DS918" runat="server" />
    <STDT:StDataTable ID="DS919" runat="server" />
    <STDT:StDataTable ID="DS920" runat="server" />


    <STDT:StDataTable ID="DS101" runat="server" />
    <STDT:StDataTable ID="DS102" runat="server" />
    <STDT:StDataTable ID="DS103" runat="server" />
    <STDT:StDataTable ID="DS104" runat="server" />
    <STDT:StDataTable ID="DS105" runat="server" />
    <STDT:StDataTable ID="DS106" runat="server" />
    <STDT:StDataTable ID="DS107" runat="server" />
    <STDT:StDataTable ID="DS108" runat="server" />
    <STDT:StDataTable ID="DS109" runat="server" />
    <STDT:StDataTable ID="DS1010" runat="server" />
    <STDT:StDataTable ID="DS1011" runat="server" />
    <STDT:StDataTable ID="DS1012" runat="server" />
    <STDT:StDataTable ID="DS1013" runat="server" />
    <STDT:StDataTable ID="DS1014" runat="server" />
    <STDT:StDataTable ID="DS1015" runat="server" />
    <STDT:StDataTable ID="DS1016" runat="server" />
    <STDT:StDataTable ID="DS1017" runat="server" />
    <STDT:StDataTable ID="DS1018" runat="server" />
    <STDT:StDataTable ID="DS1019" runat="server" />
    <STDT:StDataTable ID="DS1020" runat="server" />


    <STDT:StDataTable ID="DS1101" runat="server" />
    <STDT:StDataTable ID="DS1102" runat="server" />
    <STDT:StDataTable ID="DS1103" runat="server" />
    <STDT:StDataTable ID="DS1104" runat="server" />
    <STDT:StDataTable ID="DS1105" runat="server" />
    <STDT:StDataTable ID="DS1106" runat="server" />
    <STDT:StDataTable ID="DS1107" runat="server" />
    <STDT:StDataTable ID="DS1108" runat="server" />
    <STDT:StDataTable ID="DS1109" runat="server" />
    <STDT:StDataTable ID="DS1110" runat="server" />
    <STDT:StDataTable ID="DS1111" runat="server" />
    <STDT:StDataTable ID="DS1112" runat="server" />
    <STDT:StDataTable ID="DS1113" runat="server" />
    <STDT:StDataTable ID="DS1114" runat="server" />
    <STDT:StDataTable ID="DS1115" runat="server" />
    <STDT:StDataTable ID="DS1116" runat="server" />
    <STDT:StDataTable ID="DS1117" runat="server" />
    <STDT:StDataTable ID="DS1118" runat="server" />
    <STDT:StDataTable ID="DS1119" runat="server" />
    <STDT:StDataTable ID="DS1120" runat="server" />


    <STDT:StDataTable ID="DS1201" runat="server" />
    <STDT:StDataTable ID="DS1202" runat="server" />
    <STDT:StDataTable ID="DS1203" runat="server" />
    <STDT:StDataTable ID="DS1204" runat="server" />
    <STDT:StDataTable ID="DS1205" runat="server" />
    <STDT:StDataTable ID="DS1206" runat="server" />
    <STDT:StDataTable ID="DS1207" runat="server" />
    <STDT:StDataTable ID="DS1208" runat="server" />
    <STDT:StDataTable ID="DS1209" runat="server" />
    <STDT:StDataTable ID="DS1210" runat="server" />
    <STDT:StDataTable ID="DS1211" runat="server" />
    <STDT:StDataTable ID="DS1212" runat="server" />
    <STDT:StDataTable ID="DS1213" runat="server" />
    <STDT:StDataTable ID="DS1214" runat="server" />
    <STDT:StDataTable ID="DS1215" runat="server" />
    <STDT:StDataTable ID="DS1216" runat="server" />
    <STDT:StDataTable ID="DS1217" runat="server" />
    <STDT:StDataTable ID="DS1218" runat="server" />
    <STDT:StDataTable ID="DS1219" runat="server" />
    <STDT:StDataTable ID="DS1220" runat="server" />

    <STDT:StDataTable ID="DS1301" runat="server" />
    <STDT:StDataTable ID="DS1302" runat="server" />
    <STDT:StDataTable ID="DS1303" runat="server" />
    <STDT:StDataTable ID="DS1304" runat="server" />
    <STDT:StDataTable ID="DS1305" runat="server" />
    <STDT:StDataTable ID="DS1306" runat="server" />
    <STDT:StDataTable ID="DS1307" runat="server" />
    <STDT:StDataTable ID="DS1308" runat="server" />
    <STDT:StDataTable ID="DS1309" runat="server" />
    <STDT:StDataTable ID="DS1310" runat="server" />
    <STDT:StDataTable ID="DS1311" runat="server" />
    <STDT:StDataTable ID="DS1312" runat="server" />
    <STDT:StDataTable ID="DS1313" runat="server" />
    <STDT:StDataTable ID="DS1314" runat="server" />
    <STDT:StDataTable ID="DS1315" runat="server" />
    <STDT:StDataTable ID="DS1316" runat="server" />
    <STDT:StDataTable ID="DS1317" runat="server" />
    <STDT:StDataTable ID="DS1318" runat="server" />
    <STDT:StDataTable ID="DS1319" runat="server" />
    <STDT:StDataTable ID="DS1320" runat="server" />

    <STDT:StDataTable ID="DS1401" runat="server" />
    <STDT:StDataTable ID="DS1402" runat="server" />
    <STDT:StDataTable ID="DS1403" runat="server" />
    <STDT:StDataTable ID="DS1404" runat="server" />
    <STDT:StDataTable ID="DS1405" runat="server" />
    <STDT:StDataTable ID="DS1406" runat="server" />
    <STDT:StDataTable ID="DS1407" runat="server" />
    <STDT:StDataTable ID="DS1408" runat="server" />
    <STDT:StDataTable ID="DS1409" runat="server" />
    <STDT:StDataTable ID="DS1410" runat="server" />
    <STDT:StDataTable ID="DS1411" runat="server" />
    <STDT:StDataTable ID="DS1412" runat="server" />
    <STDT:StDataTable ID="DS1413" runat="server" />
    <STDT:StDataTable ID="DS1414" runat="server" />
    <STDT:StDataTable ID="DS1415" runat="server" />
    <STDT:StDataTable ID="DS1416" runat="server" />
    <STDT:StDataTable ID="DS1417" runat="server" />
    <STDT:StDataTable ID="DS1418" runat="server" />
    <STDT:StDataTable ID="DS1419" runat="server" />
    <STDT:StDataTable ID="DS1420" runat="server" />

    <STDT:StDataTable ID="DS1501" runat="server" />
    <STDT:StDataTable ID="DS1502" runat="server" />
    <STDT:StDataTable ID="DS1503" runat="server" />
    <STDT:StDataTable ID="DS1504" runat="server" />
    <STDT:StDataTable ID="DS1505" runat="server" />
    <STDT:StDataTable ID="DS1506" runat="server" />
    <STDT:StDataTable ID="DS1507" runat="server" />
    <STDT:StDataTable ID="DS1508" runat="server" />
    <STDT:StDataTable ID="DS1509" runat="server" />
    <STDT:StDataTable ID="DS1510" runat="server" />
    <STDT:StDataTable ID="DS1511" runat="server" />
    <STDT:StDataTable ID="DS1512" runat="server" />
    <STDT:StDataTable ID="DS1513" runat="server" />
    <STDT:StDataTable ID="DS1514" runat="server" />
    <STDT:StDataTable ID="DS1515" runat="server" />
    <STDT:StDataTable ID="DS1516" runat="server" />
    <STDT:StDataTable ID="DS1517" runat="server" />
    <STDT:StDataTable ID="DS1518" runat="server" />
    <STDT:StDataTable ID="DS1519" runat="server" />
    <STDT:StDataTable ID="DS1520" runat="server" />

    <div style="display: none">
        <asp:HiddenField runat="server" ID="HiddenLoadqueryText" Value="AA" />
        <asp:HiddenField runat="server" ID="HiddenP1" Value="" />
        <asp:HiddenField runat="server" ID="HiddenP2" Value="" />
        <asp:HiddenField runat="server" ID="HiddenP3" Value="" />
        <asp:HiddenField runat="server" ID="HiddenP4" Value="" />
        <asp:HiddenField runat="server" ID="HiddenP5" Value="" />
        <asp:HiddenField runat="server" ID="HiddenMainStatu" Value="" />
        <asp:HiddenField runat="server" ID="HiddenpageType" Value="" />
        <asp:HiddenField runat="server" ID="HiddenRpath" Value="" />
        <asp:HiddenField runat="server" ID="HiddendirectParam" />
        <asp:HiddenField runat="server" ID="hndBos" />
        <asp:Label ID="lblden" runat="server"></asp:Label>


        <asp:Label ID="lblGId" runat="server"></asp:Label>
        <asp:TextBox ID="txtlink1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink2" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField2" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink3" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField3" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink4" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField4" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink5" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField5" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink6" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField6" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink7" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField7" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink8" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField8" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink9" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField9" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink10" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField10" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink11" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField11" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink12" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField12" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink13" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField13" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink14" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField14" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink15" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField15" runat="server"></asp:TextBox>




        <asp:TextBox ID="txtinsertparams1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams2" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams2" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams2" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams3" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams3" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams3" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams4" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams4" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams4" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams5" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams5" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams5" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams6" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams6" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams6" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams7" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams7" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams7" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams8" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams8" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams8" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams9" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams9" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams9" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams10" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams10" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams10" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams11" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams11" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams11" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams12" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams12" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams12" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams13" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams13" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams13" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams14" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams14" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams14" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams15" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams15" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams15" runat="server"></asp:TextBox>



        <asp:TextBox ID="txtCatchParameters1" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters2" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters3" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters4" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters5" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters6" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters7" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters8" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters9" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters10" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters11" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters12" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters13" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters14" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters15" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParamsAndValues" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchValues1" runat="server" Text=""></asp:TextBox>

        <asp:TextBox ID="txtTransferedParameters1" runat="server" Text=""></asp:TextBox>

        <asp:TextBox ID="txtSubInsertPer2" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer3" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer4" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer5" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer6" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer7" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer8" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer9" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer10" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer11" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer12" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer13" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer14" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer15" runat="server" Text=""></asp:TextBox>

        <asp:TextBox ID="txtCollapseHeader1" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseHeader2" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseHeader3" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseHeader4" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseHeader5" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseHeader6" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseHeader7" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseHeader8" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseHeader9" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseHeader10" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseHeader11" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseHeader12" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseHeader13" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseHeader14" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseHeader15" runat="server" Text=""></asp:TextBox>

        <asp:TextBox ID="txtCollapseStatu1" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseStatu2" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseStatu3" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseStatu4" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseStatu5" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseStatu6" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseStatu7" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseStatu8" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseStatu9" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseStatu10" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseStatu11" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseStatu12" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseStatu13" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseStatu14" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseStatu15" runat="server" Text=""></asp:TextBox>

        <asp:TextBox ID="txtMasterObject2" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtMasterObject3" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtMasterObject4" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtMasterObject5" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtMasterObject6" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtMasterObject7" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtMasterObject8" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtMasterObject9" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtMasterObject10" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtMasterObject11" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtMasterObject12" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtMasterObject13" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtMasterObject14" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtMasterObject15" runat="server" Text=""></asp:TextBox>


        <asp:TextBox ID="txtDHT1" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtDHT2" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtDHT3" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtDHT4" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtDHT5" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtDHT6" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtDHT7" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtDHT8" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtDHT9" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtDHT10" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtDHT11" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtDHT12" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtDHT13" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtDHT14" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtDHT15" runat="server" Text=""></asp:TextBox>

        <asp:TextBox ID="txtRef1" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtRef2" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtRef3" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtRef4" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtRef5" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtRef6" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtRef7" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtRef8" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtRef9" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtRef10" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtRef11" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtRef12" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtRef13" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtRef14" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtRef15" runat="server" Text=""></asp:TextBox>


        <asp:TextBox ID="txtOBJP1" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJP2" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJP3" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJP4" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJP5" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJP6" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJP7" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJP8" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJP9" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJP10" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJP11" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJP12" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJP13" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJP14" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJP15" runat="server" Text=""></asp:TextBox>


        <asp:TextBox ID="txtOBJF1" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJF2" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJF3" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJF4" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJF5" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJF6" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJF7" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJF8" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJF9" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJF10" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJF11" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJF12" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJF13" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJF14" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJF15" runat="server" Text=""></asp:TextBox>


        <asp:TextBox ID="txtR0011s" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtR0016s" runat="server"></asp:TextBox>

        <asp:TextBox ID="txtBos" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOnayStatus" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtlinkType" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtCurLinkFields" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtFilterText" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtcurrentId" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtMessage" runat="server"></asp:TextBox>
        <dx:ASPxTextBox ID="txttask" ClientInstanceName="txttask" runat="server"></dx:ASPxTextBox>
        <asp:TextBox ID="txtPostDurum" runat="server" Text="First"></asp:TextBox>
        <asp:TextBox ID="txtIsCard" runat="server" Text="1"></asp:TextBox>
    </div>
    <dx:ASPxPopupControl ID="Pop" runat="server" CloseAction="CloseButton" CloseOnEscape="true" Modal="true" ClientInstanceName="Pop"
        HeaderText="" AllowDragging="true" PopupAnimationType="Slide" OnWindowCallback="Pop_WindowCallback" EnableViewState="false" ShowCloseButton="true"
        Left="378" Top="156">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <asp:Panel ID="pnlDateContent" runat="server">
                    <dx:ASPxFormLayout ID="Box2" runat="server" Width="100%">
                        <Items>
                            <dx:LayoutGroup ShowCaption="False" ColCount="3">
                                <Items>
                                    <dx:LayoutItem FieldName="serino" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxTextBox ID="txtSeriNo" runat="server"></dx:ASPxTextBox>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem FieldName="haricibelgeno" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxTextBox ID="txtHariciBelgeNo" runat="server"></dx:ASPxTextBox>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem FieldName="identityNo" Border-BorderColor="DarkBlue" Caption="IDENTITY NO" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxTextBox ID="txtIdentityNo" runat="server" Width="100%"
                                                    CssClass="carditemStyle">
                                                </dx:ASPxTextBox>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem FieldName="PassportNo" Border-BorderColor="DarkBlue" Caption="PASSPORT NO" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxTextBox ID="txtPassportNo" runat="server" Width="100%"
                                                    CssClass="carditemStyle">
                                                </dx:ASPxTextBox>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem FieldName="Tarih" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxDateEdit runat="server" ID="txtDate" AutoPostBack="false" DisplayFormatString="dd.MM.yyyy" EditFormatString="dd.MM.yyyy">
                                                    <ClientSideEvents ValueChanged="function(s,e){pnlDovizCinsi.PerformCallback(''); pnlDovizKuru.PerformCallback('');}" />
                                                </dx:ASPxDateEdit>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem FieldName="CurrencyType" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxCallbackPanel ID="pnlDovizCinsi" ClientInstanceName="pnlDovizCinsi" runat="server" OnCallback="pnlDovizCinsi_Callback">
                                                    <PanelCollection>
                                                        <dx:PanelContent>
                                                            <dx:ASPxTextBox ID="txtCurrency" runat="server" ReadOnly="true"></dx:ASPxTextBox>
                                                        </dx:PanelContent>
                                                    </PanelCollection>
                                                </dx:ASPxCallbackPanel>

                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem FieldName="DovizKuru" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxCallbackPanel ID="pnlDovizKuru" ClientInstanceName="pnlDovizKuru" runat="server" OnCallback="pnlDovizKuru_Callback">
                                                    <PanelCollection>
                                                        <dx:PanelContent>
                                                            <dx:ASPxTextBox ID="txtDovizKuru" runat="server"></dx:ASPxTextBox>
                                                        </dx:PanelContent>
                                                    </PanelCollection>
                                                </dx:ASPxCallbackPanel>

                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem FieldName="Faturaekmetin" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxTextBox ID="txtFaturaEkMetin" runat="server"></dx:ASPxTextBox>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                     <dx:LayoutItem FieldName="Ad" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxTextBox ID="txtFaturaAd" runat="server"></dx:ASPxTextBox>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                      <dx:LayoutItem FieldName="Soyad" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxTextBox ID="txtFaturaSoyad" runat="server"></dx:ASPxTextBox>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                     <dx:LayoutItem FieldName="Mail" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxTextBox ID="txtFaturaMail" runat="server"></dx:ASPxTextBox>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                       <dx:LayoutItem FieldName="Sehir" BorderTop-BorderStyle="None" BorderLeft-BorderStyle="None" BorderRight-BorderStyle="None" Border-BorderWidth="1">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxTextBox ID="txtFaturaSehir" runat="server"></dx:ASPxTextBox>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>

                                    <dx:LayoutItem ShowCaption="False">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ShowCaption="False">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxButton ID="btnCreate" runat="server"   Text="PREVIEW INV" AutoPostBack="true" OnClick="btnCreate_Click"></dx:ASPxButton>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ShowCaption="False">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxButton ID="btnPrint" runat="server"  Text="PRINT INV" AutoPostBack="true" OnClick="btnPrint_Click">
                                                    <ClientSideEvents Click="function(s,e) { Pop.Hide();}" />
                                                </dx:ASPxButton>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ShowCaption="False">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxButton ID="btnTemizle" runat="server" Visible="false" Text="Cancel" AutoPostBack="true" OnClick="btnTemizle_Click">
                                                    <ClientSideEvents Click="function(s,e) { Pop.Hide();}" />
                                                </dx:ASPxButton>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem ShowCaption="False">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxLabel ID="lblresult" runat="server" ForeColor="Red">
                                                </dx:ASPxLabel>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                </Items>
                            </dx:LayoutGroup>
                        </Items>
                    </dx:ASPxFormLayout>
                </asp:Panel>
                <div style="display: none">
                    <asp:TextBox ID="txtCurrentInvoiceNo" runat="server"></asp:TextBox>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>


