﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using MSS1.WSGeneric;
using System.ServiceModel;
using System.Data;
using MSS1.Codes;

namespace MSS1
{
    public partial class belgekopyala : System.Web.UI.Page
    {
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        SqlConnection baglanti;
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        public ws_baglantı ws = new ws_baglantı();
        private SqlCommand sorgu;
        public static SessionBase _obj; 


        #region Dil Getir
        public void dilgetir()
        {
            lblBaslik.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "belgekopyala", "lblBaslik");
            lblBelgeNoB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "belgekopyala", "lblBelgeNoB");
            lblBelgeSecB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "belgekopyala", "lblBelgeSecB");
            btnkopyala.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "belgekopyala", "btnkopyala");
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            _obj = TabSessions.ControlState(this.Page, Request.QueryString["GId"].ToString());

            dilgetir();
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            if (!IsPostBack)
            {
                lblbelgeno.Text = Request.QueryString["BelgeNo"];
                autobelge.ContextKey = _obj.Comp.ToString() + ":" + Request.QueryString["Tur"];
            }
        }
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> belgegetir(string prefixText, int count, string contextKey)
        {
            string[] txt = { "", "" };
            txt = prefixText.Split('_');
            string[] tur = contextKey.Split(':');
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
            if (Convert.ToInt32(tur[1]) == 1)
            {
                if (prefixText == "*")
                {
                    string query = "select TOP 150 (sih.[Bill-to Name]+'_'+sih.[External Document No_]+'_'+CONVERT(VARCHAR(10),sih.[Document Date],104)+'_'+(CASE sih.[Currency Code] WHEN '' THEN 'TL' ELSE sih.[Currency Code] END)+'_'+ CONVERT(VARCHAR(10),(SELECT CONVERT(DECIMAL(10,2),SUM(sil.Amount)) FROM [0D_00113_00_SALES_INVOICE_LINE] sil WHERE sil.COMPANY='" + tur[0] + "' and sil.[Document No_]=sih.No_ group by sil.[Document No_]))) as icerik from [0D_00112_01_SALES INVOICE HEADER] sih where sih.COMPANY='" + tur[0] + "' and sih.Type <> 3  and sih.[Type]!=2";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    conn.Open();
                    List<string> bul = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            bul.Add(sdr["icerik"].ToString());
                        }
                    }
                    conn.Close();

                    return bul;
                }
                else
                {
                    string query = "";
                    if (txt.Length > 1)
                    {
                        query = "Select TOP 150 (sih.[Bill-to Name]+'_'+sih.[External Document No_]+'_'+CONVERT(VARCHAR(10),sih.[Document Date],104)+'_'+(CASE sih.[Currency Code] WHEN '' THEN 'TL' ELSE sih.[Currency Code] END)+'_'+ CONVERT(VARCHAR(10),(SELECT CONVERT(DECIMAL(10,2),SUM(sil.Amount)) FROM [0D_00113_00_SALES_INVOICE_LINE] sil WHERE COMPANY='" + tur[0] + "' and sil.[Document No_]=sih.No_ group by sil.[Document No_]))) as icerik from[0D_00112_01_SALES INVOICE HEADER] sih where sih.COMPANY='" + tur[0] + "' and sih.Type <> 3 AND (UPPER(sih.[Bill-to Name]) LIKE '" + txt[0].ToUpper() + "%') AND (('" + txt[1] + "' is null or '" + txt[1] + "'= '')  OR (sih.[External Document No_] LIKE '" + txt[1] + "%')) and sih.[Type]!=2";
                    }
                    else
                    {
                        query = "Select TOP 150  (sih.[Bill-to Name]+'_'+sih.[External Document No_]+'_'+CONVERT(VARCHAR(10),sih.[Document Date],104)+'_'+(CASE sih.[Currency Code] WHEN '' THEN 'TL' ELSE sih.[Currency Code] END)+'_'+ CONVERT(VARCHAR(10),(SELECT CONVERT(DECIMAL(10,2),SUM(sil.Amount)) FROM [0D_00113_00_SALES_INVOICE_LINE] sil WHERE COMPANY='" + tur[0] + "' and sil.[Document No_]=sih.No_ group by sil.[Document No_]))) as icerik from [0D_00112_01_SALES INVOICE HEADER] sih where sih.COMPANY='" + tur[0] + "' and sih.Type <> 3 AND (UPPER(sih.[Bill-to Name]) LIKE '" + txt[0].ToUpper() + "%') and sih.[Type]!=2";

                    }
                    SqlCommand cmd = new SqlCommand(query, conn);
                    conn.Open();
                    List<string> bul = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            bul.Add(sdr["icerik"].ToString());
                        }
                    }
                    conn.Close();

                    return bul;
                }
            }
            else if (Convert.ToInt32(tur[1]) == 2)
            {
                if (prefixText == "*")
                {
                    using (SqlCommand cmd = new SqlCommand("Select TOP 150  (sih.[Buy-from Vendor Name]+'_'+sih.[Vendor Invoice No_]) as icerik from  [0D_00122_00_PURCH INV HEADER] sih where COMPANY='" + _obj.Comp + "' and  sih.[Type]!=2", conn))
                    {
                        conn.Open();
                        List<string> bul = new List<string>();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                bul.Add(sdr["icerik"].ToString());
                            }
                        }
                        conn.Close();

                        return bul;
                    }
                }
                else
                {
                    string query = "";
                    if (txt.Length > 1)
                    {
                        query = "Select TOP 150  (sih.[Buy-from Vendor Name]+'_'+sih.[Vendor Invoice No_]) as icerik from [0D_00122_00_PURCH INV HEADER] sih where COMPANY='" + _obj.Comp.ToString() + "' and  (UPPER(sih.[Buy-from Vendor Name]) LIKE '" + txt[0].ToUpper() + "%') AND (('" + txt[1] + "' is null or '" + txt[1] + "'= '')  OR (sih.[Vendor Invoice No_] LIKE '" + txt[1] + "%')) and sih.[Type]!=2";
                    }
                    else
                    {
                        query = "Select TOP 150  (sih.[Buy-from Vendor Name]+'_'+sih.[Vendor Invoice No_]) as icerik from [0D_00122_00_PURCH INV HEADER] sih where COMPANY='" + _obj.Comp.ToString() + "' and (UPPER(sih.[Buy-from Vendor Name]) LIKE '" + txt[0].ToUpper() + "%') and sih.[Type]!=2";

                    }
                    SqlCommand cmd = new SqlCommand(query, conn);
                    conn.Open();
                    List<string> bul = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            bul.Add(sdr["icerik"].ToString());
                        }
                    }
                    conn.Close();

                    return bul;
                }
            }
            else //tür '3' proforma ödeme olan 
            {
                if (prefixText == "*")
                {
                    using (SqlCommand cmd = new SqlCommand("Select TOP 150  (sih.[Bill-to Name]+'_'+sih.[External Document No_]+'_'+CONVERT(VARCHAR(10),sih.[Document Date],104)+'_'+(CASE sih.[Currency Code] WHEN '' THEN 'TL' ELSE sih.[Currency Code] END)+'_'+ CONVERT(VARCHAR(10),(SELECT CONVERT(DECIMAL(10,2),SUM(sil.Amount)) FROM [0D_00113_00_SALES_INVOICE_LINE]  sil  where COMPANY='" + tur[0] + " and sil.[Document No_]=sih.No_ group by sil.[Document No_]))) as icerik from  [0D_00112_01_SALES INVOICE HEADER] sih where sih.COMPANY='" + tur[0] + "' and  sih.Type = 3 and sih.[Proforma Gösterme] = 0 and sih.[Type]!=2", conn))
                    {
                        conn.Open();
                        List<string> bul = new List<string>();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                bul.Add(sdr["icerik"].ToString());
                            }
                        }
                        conn.Close();

                        return bul;
                    }
                }
                else
                {
                    string query = "";
                    if (txt.Length > 1)
                    {
                        query = "Select TOP 150  (sih.[Bill-to Name]+'_'+sih.[External Document No_]+'_'+CONVERT(VARCHAR(10),sih.[Document Date],104)+'_'+(CASE sih.[Currency Code] WHEN '' THEN 'TL' ELSE sih.[Currency Code] END)+'_'+ CONVERT(VARCHAR(10),(SELECT CONVERT(DECIMAL(10,2),SUM(sil.Amount)) FROM  [0D_00113_00_SALES_INVOICE_LINE]  sil  where COMPANY='" + tur[0] + " and  sil.[Document No_]=sih.No_ group by sil.[Document No_]))) as icerik from  [0D_00112_01_SALES INVOICE HEADER] sih where sih.COMPANY='" + tur[0] + "' and sih.Type = 3 and (UPPER(sih.[Bill-to Name]) LIKE '" + txt[0].ToUpper() + "%') AND (('" + txt[1] + "' is null or '" + txt[1] + "'= '')  OR (sih.[External Document No_] LIKE '" + txt[1] + "%')) and sih.[Proforma Gösterme] = 0 sih.[Type]!=2";
                    }
                    else
                    {
                        query = "Select TOP 150  (sih.[Bill-to Name]+'_'+sih.[External Document No_]+'_'+CONVERT(VARCHAR(10),sih.[Document Date],104)+'_'+(CASE sih.[Currency Code] WHEN '' THEN 'TL' ELSE sih.[Currency Code] END)+'_'+ CONVERT(VARCHAR(10),(SELECT CONVERT(DECIMAL(10,2),SUM(sil.Amount)) FROM [0D_00113_00_SALES_INVOICE_LINE]  sil  where COMPANY='" + tur[0] + " and  sil.[Document No_]=sih.No_ group by sil.[Document No_]))) as icerik from  [0D_00112_01_SALES INVOICE HEADER] sih where sih.COMPANY='" + tur[0] + "' and  sih.Type = 3 and (UPPER(sih.[Bill-to Name]) LIKE '" + txt[0].ToUpper() + "%') and sih.[Proforma Gösterme] = 0 and sih.[Type]!=2";

                    }
                    SqlCommand cmd = new SqlCommand(query, conn);
                    conn.Open();
                    List<string> bul = new List<string>();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            bul.Add(sdr["icerik"].ToString());
                        }
                    }
                    conn.Close();

                    return bul;
                }
            }

        }
        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }
        private void belgenobul(string sirket, string mustno, string haricino, Label lbl, int tur)
        {

            baglanti = new SqlConnection(strConnString);
            baglanti.Open();
            if (tur == 1)
            {
                sorgu = new SqlCommand("Select No_ from [0D_00112_01_SALES INVOICE HEADER] where COMPANY='" + sirket + "' and  [Bill-to Name]='" + mustno + "' AND [External Document No_] ='" + haricino + "' AND [Type]!=2", baglanti);
            }
            else if (tur == 2)
            {
                sorgu = new SqlCommand("Select No_ from [0D_00122_00_PURCH INV HEADER] sih where COMPANY='" + _obj.Comp.ToString() + "' and  [Buy-from Vendor Name]='" + mustno + "' AND [Vendor Invoice No_] ='" + haricino + "' and [Type]!=2", baglanti);
            }
            else if (tur == 3)
            {
                sorgu = new SqlCommand("Select No_ from [0D_00112_01_SALES INVOICE HEADER] sih WHERE COMPANY='" + sirket + "' and [Bill-to Name]='" + mustno + "' AND [External Document No_] ='" + haricino + "' and [Type]!=2", baglanti);

            }
            SqlDataReader sqr;
            sqr = sorgu.ExecuteReader();
            if (sqr.Read())
            {
                lbl.Text = sqr["No_"].ToString();
            }
            else
            {
                lbl.Text = "";
            }

            sqr.Close();
        }

        protected void txtbelge_TextChanged(object sender, EventArgs e)
        {
            string[] txt;
            txt = txtbelge.Text.Split('_');
            if (txt.Length > 1)
            {
                belgenobul(_obj.Comp.ToString(), txt[0], txt[1], lblsipno, Convert.ToInt32(Request.QueryString["Tur"]));
            }
            else
            {
                lblsipno.Text = "";
            }

        }
        protected void btnkopyala_Click(object sender, EventArgs e)
        {
            if (lblsipno.Text != string.Empty)
            {
                string sipno = "";
                if (Convert.ToInt32(Request.QueryString["Tur"]) == 1)
                {
                    sipno = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "SatisBelgeKopyala§" + lblbelgeno.Text + "§" + lblsipno.Text + "§" + _obj.NameSurName.ToString(), _obj.Comp.ToString());

                }
                else if (Convert.ToInt32(Request.QueryString["Tur"]) == 2)
                {
                    sipno = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "SatinBelgeKopyala§" + lblbelgeno.Text + "§" + lblsipno.Text + "§" + _obj.NameSurName.ToString(), _obj.Comp.ToString());

                }
                else
                {
                    sipno = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "ProformaSatisBelgeKopyala§" + lblbelgeno.Text + "§" + lblsipno.Text + "§" + _obj.NameSurName.ToString(), _obj.Comp.ToString());

                }
                if (Convert.ToInt32(Request.QueryString["Tur"]) == 2)
                {
                    if (!ClientScript.IsStartupScriptRegistered("JSFaturaAcik"))
                    {
                        ClientScript.RegisterStartupScript(GetType(), "JSFaturaAcik", @"window.opener.ReloadParentPage('" + sipno + "');self.close();", true);
                    }
                }
            }
            else
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "belgekopyala", "msg019"));
            }
        }

        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }
    }
}