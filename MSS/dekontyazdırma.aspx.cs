﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.ServiceModel;
using MSS1.WSGeneric;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using MSS1.Codes;

namespace MSS1
{
    public partial class dekontyazdırma : Bases
    {
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        SqlConnection baglanti;
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        public ws_baglantı ws = new ws_baglantı();
        SessionBase _obj;

        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }

        #region Dil Getir
        public void dilgetir()
        {
            lblFtTuruB1.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "lblFtTuruB1");
            lblFtTuruB2.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "lblFtTuruB2");
            lblTopFtMetni1.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "lblTopFtMetni1");
            lblTopFtMetni2.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "lblTopFtMetni2");
            lblTopFtMetni3.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "lblTopFtMetni3");
            lblGrup1.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "lblGrup1");
            lblGrup2.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "lblGrup2");
            lblGrup3.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "lblGrup3");
            lblFtEkMetinB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "lblFtEkMetinB");
            lblFtTarihi.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "lblFtTarihi");
            lblticari.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "lblticari");
            lblHariciBelgeNoB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "lblHariciBelgeNoB");
            lblLimanB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "lblLimanB");
            lblTanimB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "lblTanimB");
            lblTasiyiciAdB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "lblTasiyiciAdB");
            lblKonsimentoB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "lblKonsimentoB");
            lblKAPAdetB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "lblKAPAdetB");
            lblKargoB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "lblKargoB");
            lblBirimB.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "lblBirimB");
            btnönizleme.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "btnönizleme");
            btnyazdır.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "btnyazdır");

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            _obj = TabSessions.ControlState(this.Page, Request.QueryString["GId"].ToString());
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }

            dilgetir();
            if (!IsPostBack)
            {
                txtfaturatarihi.Text = Request.QueryString["Tarih"];
                ticariunvankontrol(_obj.Comp.ToString(), Request.QueryString["BelgeNo"].ToString());
                string[] tcevir = txtfaturatarihi.Text.Split('.');

                string[] _params = { _obj.Comp.ToString(), tcevir[1] + tcevir[0] + tcevir[2] };
                txthbno.Text = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "DekontNoAl§" + string.Join("§", _params), _obj.Comp);

            }

        }
        private void ticariunvankontrol(string sirket, string sipno)
        {
            baglanti = new SqlConnection(strConnString);
            baglanti.Open();
            SqlCommand sorgu = new SqlCommand("select C.[Fatura Ticari Ünvan] as ticari,C.[Ticari Unvan] as ticariicerik from [0C_00018_00_CUSTOMER] C where C.COMPANY ='" + sirket + "' and C.No_= (select SH.[Bill-to Customer No_] from [0D_00036_00_SALES HEADER] SH WHERE C.COMPANY ='" + sirket + "' and SH.No_='" + sipno + "')", baglanti);
            SqlDataAdapter sorguDA = new SqlDataAdapter(sorgu);
            SqlDataReader sqr;
            sqr = sorgu.ExecuteReader();
            if (sqr.Read())
            {
                txtticariunvan.Enabled = Convert.ToBoolean(sqr["ticari"]);
                if (txtticariunvan.Enabled == true)
                {
                    txtticariunvan.Text = sqr["ticariicerik"].ToString();
                }

            }
            sqr.Close();
        }
        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }
        protected void btnyazdır_Click(object sender, EventArgs e)
        {
            if (ddlft1.SelectedValue != string.Empty && ddlft2.SelectedValue != string.Empty && txthbno.Text != string.Empty && txtfaturatarihi.Text != string.Empty)
            {
                string[] tcevir = txtfaturatarihi.Text.Split('.');

                string[] _params = { txthbno.Text };
                string _SatisHariciKontrol = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "SatisHariciKontrol§" + string.Join("§", _params), _obj.Comp);

                if (_SatisHariciKontrol == "True")
                {
                    string[] ticaridizi = new string[4];
                    int uzunluk = txtticariunvan.Text.Length - 50;
                    if (txtticariunvan.Text.Length <= 50)
                    {
                        ticaridizi[0] = txtticariunvan.Text.Substring(0, txtticariunvan.Text.Length);
                        ticaridizi[1] = "";
                    }
                    else
                    {
                        ticaridizi[0] = txtticariunvan.Text.Substring(0, 50);
                        ticaridizi[1] = txtticariunvan.Text.Substring(50, txtticariunvan.Text.Length - 50);
                    }
                    try
                    {
                        File.Delete(Server.MapPath(CurrentFolderName(_obj.Comp) + "/PDF/Dnkt" + Request.QueryString["BelgeNo"] + ".pdf"));
                    }
                    catch (Exception)
                    {
                    }
                     string[] _params1 = { Request.QueryString["BelgeNo"].ToString(), txthbno.Text, txtliman.Text, txttanim.Text, txtseferno.Text, txtawb.Text, txtadet.Text, txtagırlık.Text, txtbirim.Text, ddlft2.SelectedValue, tcevir[1] + tcevir[0] + tcevir[2], ddlft1.SelectedValue, txtftmetni1.Text, txtftmetni2.Text, txtftmetni3.Text, txtgrup1a.Text, txtgrup2a.Text, txtgrup3a.Text, ticaridizi[0], ticaridizi[1], "" };
                    GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "SatisSipYazdır§" + string.Join("§", _params1), _obj.Comp);

                    //record.SatisSipYazda141r(Request.QueryString["BelgeNo"], txthbno.Text, txtliman.Text, txttanim.Text, txtseferno.Text, txtawb.Text, txtadet.Text, txtagırlık.Text, txtbirim.Text, Convert.ToInt32(ddlft2.SelectedValue), tcevir[1] + tcevir[0] + tcevir[2], Convert.ToInt32(ddlft1.SelectedValue), txtftmetni1.Text, txtftmetni2.Text, txtftmetni3.Text, txtgrup1a.Text, txtgrup2a.Text, txtgrup3a.Text, ticaridizi[0], ticaridizi[1], "");
                    ReportDocument crystalReport;

                    string reportPath = f.GetReportPath("INVOICE DEKONT.rpt", _obj.Comp);
                    crystalReport = new ReportDocument();
                    crystalReport.Load(reportPath);
                    crystalReport.ReportOptions.EnableSaveDataWithReport = false;
                    crystalReport.SetParameterValue("@DOCNO", Request.QueryString["BelgeNo"]);
                    crystalReport.SetParameterValue("@COMP", _obj.Comp.ToString());
                    crystalReport.SetParameterValue("ŞİRKET", _obj.Comp.ToString());
                    crystalReport.SetParameterValue("EK METİN", txtftekmetin.Text);
                    crystalReport.SetParameterValue("ÖN İZLEME", 1);
                    crystalReport.ExportToDisk(ExportFormatType.PortableDocFormat, Server.MapPath(CurrentFolderName(_obj.Comp) + "/PDF/" + Request.QueryString["BelgeNo"] + ".pdf"));
                    ClientScript.RegisterStartupScript(this.Page.GetType(), "popupOpener", "var popup=window.open('"+ CurrentFolderName(_obj.Comp) + "/PDF/" + Request.QueryString["BelgeNo"] + ".pdf');popup.focus();", true);

                    if (crystalReport != null)
                    {

                        crystalReport.Close();

                        crystalReport.Dispose();

                        crystalReport = null;

                    }
                    if (ddlft2.SelectedIndex == 1)
                    {
                       string[]  _params2 = { Request.QueryString["BelgeNo"], _obj.NameSurName.ToString()};
                        GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "SatisDefNaklet§" + string.Join("§", _params2), _obj.Comp);

                        //record.SatisDefNaklet(Request.QueryString["BelgeNo"], _obj.NameSurName.ToString());
                        if (!ClientScript.IsStartupScriptRegistered("JSFaturaAcik"))
                        {
                            ClientScript.RegisterStartupScript(GetType(), "JSFaturaAcik", @"window.opener.parent.RefreshAllCollapse();self.close();", true);
                        }
                    }
                    else
                    {
                        MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "msg122"));
                    }

                }
                else
                {
                    MessageBox(txthbno.Text + l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "msg123"));
                }

            }
            else
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "msg124"));
            }


        }
        protected void btnönizleme_Click(object sender, EventArgs e)
        {
            if (ddlft1.SelectedValue != string.Empty && ddlft2.SelectedValue != string.Empty && txtfaturatarihi.Text != string.Empty)
            {
                string[] ticaridizi = new string[4];
                int uzunluk = txtticariunvan.Text.Length - 50;
                if (txtticariunvan.Text.Length <= 50)
                {
                    ticaridizi[0] = txtticariunvan.Text.Substring(0, txtticariunvan.Text.Length);
                    ticaridizi[1] = "";
                }
                else
                {
                    ticaridizi[0] = txtticariunvan.Text.Substring(0, 50);
                    ticaridizi[1] = txtticariunvan.Text.Substring(50, txtticariunvan.Text.Length - 50);
                }
                string belgeno = Request.QueryString["BelgeNo"].ToString();
                string[] tcevir = txtfaturatarihi.Text.Split('.');


                try
                {
                    File.Delete(Server.MapPath(CurrentFolderName(_obj.Comp) + "/PDF/Dnkt" + belgeno + ".pdf"));
                }
                catch (Exception)
                {
                }

                string[] _params = { belgeno, txthbno.Text, txtliman.Text, txttanim.Text, txtseferno.Text, txtawb.Text, txtadet.Text, txtagırlık.Text, txtbirim.Text, ddlft2.SelectedValue, tcevir[1] + tcevir[0] + tcevir[2], ddlft1.SelectedValue, txtftmetni1.Text, txtftmetni2.Text, txtftmetni3.Text, txtgrup1a.Text, txtgrup2a.Text, txtgrup3a.Text, ticaridizi[0], ticaridizi[1], "" };
                GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "SatisSipYazdır§" + string.Join("§", _params), _obj.Comp);

                //record.SatisSipYazda141r(belgeno, txthbno.Text, txtliman.Text, txttanim.Text, txtseferno.Text, txtawb.Text, txtadet.Text, txtagırlık.Text, txtbirim.Text, Convert.ToInt32(ddlft2.SelectedValue), tcevir[1] + tcevir[0] + tcevir[2], Convert.ToInt32(ddlft1.SelectedValue), txtftmetni1.Text, txtftmetni2.Text, txtftmetni3.Text, txtgrup1a.Text, txtgrup2a.Text, txtgrup3a.Text, ticaridizi[0], ticaridizi[1], "");
                ReportDocument crystalReport;

                string reportPath = f.GetReportPath("INVOICE DEKONT.rpt", _obj.Comp);
                crystalReport = new ReportDocument();
                crystalReport.Load(reportPath);
                //crystalReport.Load(Server.MapPath("Raporlar/INVOICE DEKONT.rpt"));
                crystalReport.ReportOptions.EnableSaveDataWithReport = false;
                crystalReport.SetParameterValue("@DOCNO", Request.QueryString["BelgeNo"]);
                crystalReport.SetParameterValue("@COMP", _obj.Comp.ToString());
                crystalReport.SetParameterValue("ŞİRKET", _obj.Comp.ToString());
                crystalReport.SetParameterValue("EK METİN", txtftekmetin.Text);
                crystalReport.SetParameterValue("ÖN İZLEME", 2);
                //Response.Write(Server.MapPath("PDF/" + "Dnkt" + belgeno + ".pdf")); 
                crystalReport.ExportToDisk(ExportFormatType.PortableDocFormat, Server.MapPath(CurrentFolderName(_obj.Comp) + "/PDF/" + "Dnkt" + belgeno + ".pdf"));
                ClientScript.RegisterStartupScript(this.Page.GetType(), "popupOpener", "var popup=window.open('"+ CurrentFolderName(_obj.Comp) + "/PDF/" + "Dnkt" + belgeno + ".pdf');popup.focus();", true);

            }
            else
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dekontyazdırma", "msg125"));
            }

        }
        protected void ddlft1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlft1.SelectedIndex == 2)
            {
                txtftmetni1.Enabled = true;
                txtftmetni2.Enabled = true;
                txtftmetni3.Enabled = true;
            }
            else
            {
                txtftmetni1.Enabled = false;
                txtftmetni2.Enabled = false;
                txtftmetni3.Enabled = false;
            }
            if (ddlft1.SelectedIndex == 4)
            {
                txtgrup1a.Enabled = true;
                txtgrup2a.Enabled = true;
                txtgrup3a.Enabled = true;
            }
            else
            {
                txtgrup1a.Enabled = false;
                txtgrup2a.Enabled = false;
                txtgrup3a.Enabled = false;
            }
        }
        protected void ddlft2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlft2.SelectedValue == "2")
            {
                string[] _params = { Request.QueryString["BelgeNo"] };
                txthbno.Text = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "ProformaNoGetir§" + string.Join("§", _params), _obj.Comp);
                //txthbno.Text = record.ProformaNoGetir(Request.QueryString["BelgeNo"]);
                txthbno.Enabled = false;
            }
            else
            {
                txthbno.Text = string.Empty;
                txthbno.Enabled = true;
            }
        }
    }
}