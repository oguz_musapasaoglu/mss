﻿using DevExpress.Web;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.Codes;

namespace MSS1
{
    public partial class OnePageList5 : System.Web.UI.Page
    {
        Hashtable copiedValues = null;

        SessionBase _obj;
        public string OBJ1_pageId { get; set; }
        public string OBJ2_pageId { get; set; }
        public string OBJ3_pageId { get; set; }
        public string OBJ1_fieldId { get; set; }
        public string OBJ2_fieldId { get; set; }
        public string OBJ3_fieldId { get; set; }

        private void QueryStringParameterDesing()
        {
            string[] obj1, obj2, obj3 = { };
            try
            {
                obj1 = Request.QueryString["OJ1"].ToString().Split(';');
                obj2 = Request.QueryString["OJ2"].ToString().Split(';');
                obj3 = Request.QueryString["OJ3"].ToString().Split(';');
            }
            catch (Exception)
            {
                obj1 = Request.QueryString["OJ1"].ToString().Split(';');
                obj2 = Request.QueryString["OJ2"].ToString().Split(';');
            }


            OBJ1_pageId = obj1[0].ToString();
            OBJ1_fieldId = obj1[1].ToString();

            OBJ2_pageId = obj2[0].ToString();
            OBJ2_fieldId = obj2[1].ToString();

            try
            {
                OBJ3_pageId = obj3[0].ToString();
                OBJ3_fieldId = obj3[1].ToString();
                grid3.Visible = true;
            }
            catch (Exception)
            {
                OBJ3_pageId = "";
                OBJ3_fieldId = "";
                grid3.Visible = false;
            }



        }

        webObjectHandler woh = new webObjectHandler();
        ServiceUtilities services = new ServiceUtilities();
        fonksiyonlar f = new fonksiyonlar();
        protected void Page_Load(object sender, EventArgs e)
        {

            QueryStringParameterDesing();
            if (IsCallback | IsPostBack) return;
            lblGId.Text = Request.QueryString["GId"];
            txtlink.Text = f.tekbirsonucdonder("select LINK from WebPages where [Web Page ID]='" + OBJ2_pageId + "' and [Web Object ID]='" + OBJ2_fieldId + "'");

            txtlinkField.Text = f.tekbirsonucdonder("select [LINK FIELD] from WebPages where [Web Page ID]='" + OBJ2_pageId + "' and [Web Object ID]='" + OBJ2_fieldId + "'");

            if (OBJ1_fieldId != "OJ9999")
            {
                dtgrid1.Table = woh.webObjectHandlerExtracted(grid1, menu2, txtinsertparams, txteditparams, txtdeleteparams, _obj.UserName, _obj.Comp, OBJ1_pageId, OBJ1_fieldId, HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString(), HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), 3).Copy();
                DataColumn[] pricols = new DataColumn[1];
                pricols[0] = dtgrid1.Table.Columns["ID"];
                dtgrid1.Table.PrimaryKey = pricols;
                grid1.DataBind();
            }
            else
            {
                grid1.Visible = false;
            }

            tdleft.Attributes.Add("style", "width:100%");
            tdright.Attributes.Add("style", "width:0");

            dtgrid2.Table = woh.webObjectHandlerExtracted(grid2, menu, txtinsertparams, txteditparams, txtdeleteparams, _obj.UserName, _obj.Comp, OBJ2_pageId, OBJ2_fieldId, HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString(), HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), 1).Copy();
            DataColumn[] pricols1 = new DataColumn[1];
            pricols1[0] = dtgrid2.Table.Columns["ID"];
            dtgrid2.Table.PrimaryKey = pricols1;


            //if (OBJ3_pageId.Length > 0 & OBJ3_fieldId.Length > 0)
            //{
            //    dtgrid3.Table = woh.webObjectHandlerExtracted(grid3, menu3, txtinsertparams, txteditparams, txtdeleteparams, _obj.UserName, _obj.Comp, OBJ3_pageId, OBJ3_fieldId, HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString(), HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), 2).Copy();
            //    DataColumn[] pricols2 = new DataColumn[1];
            //    pricols2[0] = dtgrid3.Table.Columns["ID"];
            //    dtgrid3.Table.PrimaryKey = pricols2;

            //}
            grid3CallData("RCOMP='AA'");

            Session["OPL10Grid2Rows"] = webObjectHandler.GetDynamicWebPageObjects(OBJ2_pageId, OBJ2_fieldId, _obj.Comp).Tables[4];



            FillDS1((DataTable)Session["OPL10Grid2Rows"]);
            if (OBJ3_pageId.Length > 0 & OBJ3_fieldId.Length > 0)
            {
                tdleft.Attributes.Add("style", "width:50%");
                tdright.Attributes.Add("style", "width:50%");
                Session["OPL10Grid3Rows"] = webObjectHandler.GetDynamicWebPageObjects(OBJ3_pageId, OBJ3_fieldId, _obj.Comp).Tables[4];
                FillDS2((DataTable)Session["OPL10Grid3Rows"]);
                grid3.DataSourceID = "dtgrid3";
                grid3.DataBind();

            }


            grid2.DataSourceID = "dtgrid2";
            grid2.DataBind();

            RadioListeOrder();

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }
        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }
        private void RadioListeOrder()
        {

            if (OBJ1_pageId == "PG1002")
            {
                rbList.Visible = true;
            }
            else
            {
                rbList.Visible = false;
            }

            if (OBJ2_fieldId == "OJ1002")
            {
                rbList.SelectedIndex = 0;
            }
            else if (OBJ2_fieldId == "OJ1012")
            {
                rbList.SelectedIndex = 1;
            }
            else if (OBJ2_fieldId == "OJ1022")
            {
                rbList.SelectedIndex = 2;
            }

        }
        void FillDS1(DataTable dtRows)
        {
            int sayac = 1;
            string query = string.Empty;
            for (int i = 0; i < dtRows.Rows.Count; i++)
            {
                switch (dtRows.Rows[i]["DX Type1"].ToString())
                {
                    case "DATA":
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Source"].ToString()))
                        {
                            query = webObjectHandler.FillCombobox(dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(),
                                dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), _obj.Comp, _obj.UserName);
                            switch (sayac)
                            {
                                case 1:
                                    DS11.Table = f.datatablegonder(query);
                                    break;
                                case 2:
                                    DS12.Table = f.datatablegonder(query);
                                    break;
                                case 3:
                                    DS13.Table = f.datatablegonder(query);
                                    break;
                                case 4:
                                    DS14.Table = f.datatablegonder(query);
                                    break;
                                case 5:
                                    DS15.Table = f.datatablegonder(query);
                                    break;
                                case 6:
                                    DS16.Table = f.datatablegonder(query);
                                    break;
                                case 7:
                                    DS17.Table = f.datatablegonder(query);
                                    break;
                                case 8:
                                    DS18.Table = f.datatablegonder(query);
                                    break;
                                case 9:
                                    DS19.Table = f.datatablegonder(query);
                                    break;
                                case 10:
                                    DS110.Table = f.datatablegonder(query);
                                    break;
                                case 11:
                                    DS111.Table = f.datatablegonder(query);
                                    break;
                                case 12:
                                    DS112.Table = f.datatablegonder(query);
                                    break;
                                case 13:
                                    DS113.Table = f.datatablegonder(query);
                                    break;
                                case 14:
                                    DS114.Table = f.datatablegonder(query);
                                    break;
                                case 15:
                                    DS115.Table = f.datatablegonder(query);
                                    break;
                                case 16:
                                    DS116.Table = f.datatablegonder(query);
                                    break;
                                case 17:
                                    DS117.Table = f.datatablegonder(query);
                                    break;
                                case 18:
                                    DS118.Table = f.datatablegonder(query);
                                    break;
                                case 19:
                                    DS119.Table = f.datatablegonder(query);
                                    break;
                                case 20:
                                    DS120.Table = f.datatablegonder(query);
                                    break;
                            }
                            sayac++;
                        }
                        break;
                }
            }
        }

        void FillDS2(DataTable dtRows)
        {
            int sayac = 1;
            string query = string.Empty;
            for (int i = 0; i < dtRows.Rows.Count; i++)
            {
                switch (dtRows.Rows[i]["DX Type1"].ToString())
                {
                    case "DATA":
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Source"].ToString()))
                        {
                            query = webObjectHandler.FillCombobox(dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(),
                                dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), _obj.Comp, _obj.UserName);
                            switch (sayac)
                            {
                                case 1:
                                    DS21.Table = f.datatablegonder(query);
                                    break;
                                case 2:
                                    DS22.Table = f.datatablegonder(query);
                                    break;
                                case 3:
                                    DS23.Table = f.datatablegonder(query);
                                    break;
                                case 4:
                                    DS24.Table = f.datatablegonder(query);
                                    break;
                                case 5:
                                    DS25.Table = f.datatablegonder(query);
                                    break;
                                case 6:
                                    DS26.Table = f.datatablegonder(query);
                                    break;
                                case 7:
                                    DS27.Table = f.datatablegonder(query);
                                    break;
                                case 8:
                                    DS28.Table = f.datatablegonder(query);
                                    break;
                                case 9:
                                    DS29.Table = f.datatablegonder(query);
                                    break;
                                case 10:
                                    DS210.Table = f.datatablegonder(query);
                                    break;
                                case 11:
                                    DS211.Table = f.datatablegonder(query);
                                    break;
                                case 12:
                                    DS212.Table = f.datatablegonder(query);
                                    break;
                                case 13:
                                    DS213.Table = f.datatablegonder(query);
                                    break;
                                case 14:
                                    DS214.Table = f.datatablegonder(query);
                                    break;
                                case 15:
                                    DS215.Table = f.datatablegonder(query);
                                    break;
                                case 16:
                                    DS216.Table = f.datatablegonder(query);
                                    break;
                                case 17:
                                    DS217.Table = f.datatablegonder(query);
                                    break;
                                case 18:
                                    DS218.Table = f.datatablegonder(query);
                                    break;
                                case 19:
                                    DS219.Table = f.datatablegonder(query);
                                    break;
                                case 20:
                                    DS220.Table = f.datatablegonder(query);
                                    break;
                            }
                            sayac++;
                        }
                        break;
                }
            }
        }

        private void grid3CallData(string filters)
        {
            if (OBJ3_fieldId == "OJ9999") return;
            if (OBJ3_pageId.Length > 0 & OBJ3_fieldId.Length > 0)
            {
                dtgrid3.Table = woh.webObjectHandlerExtractedSub(grid3, menu3, txtinsertparams, txteditparams, txtdeleteparams, _obj.UserName, _obj.Comp,
                    OBJ3_pageId, OBJ3_fieldId, HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString(),
                    HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), 3).Copy();
                DataColumn[] pricols = new DataColumn[1];
                pricols[0] = dtgrid3.Table.Columns["ID"];
                dtgrid3.Table.PrimaryKey = pricols;
                grid3.DataSourceID = "dtgrid3";
                grid3.KeyFieldName = "ID";
            }
            else
            {
                grid3.Visible = false;
            }
        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {

        }


        protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {

        }

        protected void grid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {

        }



        protected void menu_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                webObjectHandler.ExportPdf(gridExport);
            }
            else if (e.Item.Name.Contains("btnSelected"))
            {

                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(OBJ2_pageId, OBJ2_fieldId, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid2, rows, _obj.UserName, _obj.Comp);

                Response.Redirect(Request.RawUrl);

            }
            else
            {
                this.Response.StatusCode = 500;
                this.Response.End();
            }
        }

        protected void grid3_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(OBJ3_pageId, OBJ3_fieldId, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
            }
            string _filterexpression = grid.FilterExpression;
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
            if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid3.Table = webObjectHandler.fillSubGrid(OBJ3_pageId, OBJ3_fieldId, _obj.Comp, _obj.UserName, OBJ2_pageId, OBJ2_fieldId, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid3.Table.Columns["ID"];
            this.dtgrid3.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid3";
            grid.KeyFieldName = "ID";


            string _sessionname = "OPL10Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";
            grid.FilterExpression = webObjectHandler.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            grid.DataBind();
            grid.FilterExpression = _filterexpression;
        }

        protected void Grid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            ASPxGridView Igrid = sender as ASPxGridView;
            string _sessionname = "OPL10Grid" + Igrid.ID.Substring(Igrid.ID.Length - 1) + "Rows";

            DataRow[] rows = ((DataTable)Session[_sessionname]).Select("[DX MouseOver Text]<>'' and [DX Field ID]='" + e.DataColumn.FieldName + "'");
            if (rows.Length > 0)
            {
                try
                {
                    string _val = Igrid.GetRowValues(e.VisibleIndex, rows[0]["DX MouseOver Text"].ToString()).ToString();
                    e.Cell.ToolTip = _val;
                }
                catch { e.Cell.ToolTip = string.Format("{0}", e.CellValue); }
            }
            else
                e.Cell.ToolTip = string.Format("{0}", e.CellValue);

            try
            {
                if (e.DataColumn is GridViewDataHyperLinkColumn)
                {
                    if (string.IsNullOrEmpty(e.CellValue.ToString()))
                        e.Cell.Controls[0].Visible = false;

                }
            }
            catch
            {

            }

        }


        protected void menu3_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport3.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                webObjectHandler.ExportPdf(gridExport3);
            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(OBJ3_pageId, OBJ3_fieldId, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid3, rows, _obj.UserName, _obj.Comp);


                Response.Redirect(Request.RawUrl);
            }
            else
            {
                this.Response.StatusCode = 500;
                this.Response.End();
            }
        }


        protected void grid_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {

            try
            {

                if (e.RowType == GridViewRowType.Data)
                {

                    ASPxGridView gridI = sender as ASPxGridView;
                    object rfont = e.GetValue("RFONT")
                                , rback = e.GetValue("RBACK")
                                , rftsz = e.GetValue("RFTSZ")
                                , rftwt = e.GetValue("RFTWT")
                                , rftst = e.GetValue("RFTST");
                    if (rfont == null) return;



                    try
                    {
                        for (int columnIndex = 0; columnIndex < e.Row.Cells.Count; columnIndex++)
                        {
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.Color, rfont.ToString()); } catch { }
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.BackgroundColor, rback.ToString()); } catch { }
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.FontSize, rftsz.ToString() + "px"); } catch { }
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.FontWeight, Convert.ToInt32(rftwt.ToString()) == 0 ? "normal" : "bold"); } catch { }
                            try { e.Row.Cells[columnIndex].Style.Add(HtmlTextWriterStyle.FontStyle, Convert.ToInt32(rftst.ToString()) == 0 ? "normal" : "italic"); } catch { }
                        }

                    }
                    catch { }



                }
            }
            catch { }
        }

        protected void gridExport_RenderBrick(object sender, ASPxGridViewExportRenderingEventArgs e)
        {
            ASPxGridViewExporter gve = sender as ASPxGridViewExporter;
            string Sessionn = "OPL10Grid" + gve.GridViewID.Substring(gve.GridViewID.Length - 1) + "Rows";

            DataRow[] rows = ((DataTable)Session[Sessionn]).Select("[DX Display Order]>0 AND [DX Type2]='DATE'");
            foreach (DataRow row in rows)
            {
                e.TextValueFormatString = "dd.MM.yyyy";
            }
        }

        protected void grid2_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(OBJ2_pageId, OBJ2_fieldId, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
            }
            string _filterexpression = grid.FilterExpression;
            this.dtgrid2.Table = webObjectHandler.fillMainGrid(OBJ2_pageId, OBJ2_fieldId, _obj.Comp, _obj.UserName);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid1.Table.Columns["ID"];
            this.dtgrid1.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid2";
            grid.KeyFieldName = "ID";


            string _sessionname = "OPL10Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";
            grid.FilterExpression = webObjectHandler.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));



            grid.DataBind();
            grid.FilterExpression = _filterexpression;
        }
        protected void rbList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string _url = "";
            switch (rbList.SelectedItem.Value.ToString())
            {
                case "A":
                    _url = "~/OnePageList5.aspx?OJ1=PG1002;OJ9999&OJ2=PG1002;OJ1002&OJ3=PG1002;OJ1006&GId=" + Request.QueryString["GId"];
                    break;
                case "B":
                    _url = "~/OnePageList5.aspx?OJ1=PG1002;OJ9999&OJ2=PG1002;OJ1012&OJ3=PG1002;OJ1016&GId=" + Request.QueryString["GId"];
                    break;
                case "C":
                    _url = "~/OnePageList5.aspx?OJ1=PG1002;OJ9999&OJ2=PG1002;OJ1022&OJ3=PG1002;OJ1026&GId=" + Request.QueryString["GId"];
                    break;
            }
            Response.Redirect(_url);
        }

        protected void grid_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            string _sessionname = "OPL10Grid" + gridI.ID.Substring(gridI.ID.Length - 1) + "Rows";
            if (e.ButtonID == "Clone")
            {
                string Sessionn = "OPL10Grid" + gridI.ID.Substring(gridI.ID.Length - 1) + "Rows";
                DataRow[] rows = ((DataTable)Session[Sessionn]).Select("[DX Clone]>0");
                if (rows.Length > 0)
                {
                    copiedValues = new Hashtable();
                    foreach (DataRow row in rows)
                    {
                        copiedValues[row["DX Field ID"].ToString()] = gridI.GetRowValues(e.VisibleIndex, row["DX Field ID"].ToString());
                    }
                }
                Session[gridI.ID + "Cloned"] = "1";
                gridI.AddNewRow();
            }
            else if (e.ButtonID == "SelectAll")
            {
                DataRow[] selectrows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='SELECT' and [DX Display Order]>0");
                string _selcol = selectrows[0]["DX Permision Field"].ToString();
                if (!string.IsNullOrEmpty(_selcol))
                {
                    for (int counter = 0; counter < gridI.VisibleRowCount; counter++)
                    {
                        if (Convert.ToInt32(gridI.GetRowValues(counter, _selcol)) == 1)
                            gridI.Selection.SelectRow(counter);
                    }
                }
                else
                    gridI.Selection.SelectAll();
            }
            else if (e.ButtonID == "UnSelectAll")
            {
                gridI.Selection.UnselectAll();
            }

        }

        protected void Grid_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;
            string _sessionname = "OPL10Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";
            switch (e.ButtonType)
            {
                case ColumnCommandButtonType.Edit:

                    DataRow[] EditRows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='EDIT' and [DX Display Order]>0");
                    if (EditRows.Length > 0)
                    {
                        string _col = EditRows[0]["DX Permision Field"].ToString();
                        if (grid.GetRowValues(e.VisibleIndex, _col) != null)
                        {
                            if (grid.GetRowValues(e.VisibleIndex, _col).ToString() == "0")
                            {
                                e.Visible = false;
                            }
                        }
                    }
                    else
                        e.Visible = false;
                    break;
                case ColumnCommandButtonType.Delete:
                    DataRow[] DeleteRows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='DELETE' and [DX Display Order]>0");
                    if (DeleteRows.Length > 0)
                    {
                        string _col = DeleteRows[0]["DX Permision Field"].ToString();
                        if (grid.GetRowValues(e.VisibleIndex, _col) != null)
                        {
                            if (grid.GetRowValues(e.VisibleIndex, _col).ToString() == "0")
                            {
                                e.Visible = false;
                            }
                        }
                    }
                    else
                        e.Visible = false;
                    break;
                case ColumnCommandButtonType.SelectCheckbox:
                case ColumnCommandButtonType.Select:
                    DataRow[] selectrows = ((DataTable)Session[_sessionname]).Select("[DX Type1]='BUTTON' and [DX Type2]='SELECT' and [DX Display Order]>0");
                    string _selcol = selectrows[0]["DX Permision Field"].ToString();
                    if (!string.IsNullOrEmpty(_selcol))
                    {
                        if (grid.GetRowValues(e.VisibleIndex, _selcol).ToString() == "0")
                            e.Enabled = false;
                    }
                    break;
            }
        }
    }
}