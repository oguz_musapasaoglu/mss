﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using DevExpress.Web;
using MSS1.WSGeneric;
using MSS1.Codes;
using System.ServiceModel;
using System.Data;
using System.Threading;

namespace MSS1
{
    public partial class Default : System.Web.UI.Page
    {
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        ws_baglantı ws = new ws_baglantı();
        string customSirket = "GAC";
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack || IsCallback) return;

            Session.RemoveAll();
            Session["counter"] = 0;
            Session["changePass"] = "0";
        }

        //void FillComp(string _user)
        //{
        //    DSComp.SelectCommand = "select DISTINCT COMPANY Code, COMPANY [Text]  from [0C_50001_03_KULLANICILAR] where [Kullanıcı Adı]='" + _user.ToUpper() + "'";
        //    ddlsirketsec.Items.Clear();
        //    ddlsirketsec.DataBind();

        //}

        void FillComp(DataTable dt)
        {
            ddlsirketsec.Items.Clear();
            ddlsirketsec.DataSource = dt;
            ddlsirketsec.ValueField = "COMPANY";
            ddlsirketsec.TextField = "COMPLIS";
            ddlsirketsec.DataBind();


        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string changePass = string.Empty;
            try
            {
                if (Session["changePass"] != null)
                {
                    changePass = Session["changePass"].ToString();

                }
                Session.Clear();
                Session["changePass"] = changePass;
            }
            catch (Exception)
            {

            }




            if (string.IsNullOrEmpty(TextBox1.Text))
            {
                MessageBox("Please insert to username");
                return;
            }

            if (string.IsNullOrEmpty(TextBox2.Text))
            {
                MessageBox("Please insert to Password");
                return;
            }

            if (ddlsirketsec.Text == string.Empty)
            {
                MessageBox("Please select to company");
                return;
            }

            using (DataSet ds = new DataSet())
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "[SP_0WEB_USER_CONTROL]";
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@USER", TextBox1.Text);
                        cmd.Parameters.AddWithValue("@PASSWORD", TextBox2.Text);

                        using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                        {
                            adp.Fill(ds);

                            if (ds.Tables[0].Rows[0]["STATU"].ToString() == "4")
                            {
                                MessageBox("This user does not have access to the specified company");
                                return;
                            }
                            else if (ds.Tables[0].Rows[0]["STATU"].ToString() == "3")
                            {
                                MessageBox("This user blocked, please contact your system administrator");
                                return;
                            }

                            if (ds.Tables[1].Select("COMPANY='" + ddlsirketsec.Value.ToString() + "'").Count() == 0)
                            {
                                MessageBox("you have not connect to this company, please contact your system administrator");
                                return;
                            }
                        }
                    }
                }

            }

            using (SqlConnection connect = new SqlConnection(strConnString))
            {
                using (SqlCommand query = new SqlCommand("Select İsim,[Kullanıcı Adı] as kulad,Sifre,[Personel ID] as perid,DEPT as departman,BLG as bolge,[İlk Giriş] as ilkgiris from [0C_50001_00_KULLANICILAR] where COMPANY ='" + ddlsirketsec.Value.ToString() + "' and UPPER([Kullanıcı Adı])=@kullanici", connect))
                {

                    query.Parameters.AddWithValue("@Kullanici", TextBox1.Text.ToUpper());
                    query.Parameters.AddWithValue("@sirket", ddlsirketsec.Value.ToString());

                    connect.Open();

                    SqlDataReader reader = query.ExecuteReader();
                    if (reader.Read())
                    {

                        //ReportDocument objrptdoc = new ReportDocument();
                        //Session["Objrptdoc"] = objrptdoc;
                        Session["uyedurum"] = "tamam";
                        string kad = reader["İsim"].ToString();
                        Session["İsim"] = kad;
                        Session["Sirket"] = ddlsirketsec.Value.ToString();
                        Session["kulad"] = reader["kulad"].ToString();
                        Session["perid"] = reader["perid"].ToString();
                        Session["bolge"] = reader["bolge"].ToString();
                        Session["departman"] = reader["departman"].ToString();
                        Session["ilkgiris"] = reader["ilkgiris"].ToString();
                        Session.Timeout = 120;
                        fonksiyonlar.sirket = ddlsirketsec.Value.ToString();
                        Session["SelectedLanguage"] = "TR";
                        Session["SelectedLanguage"] = f.tekbirsonucdonder("select Adi from [0_0000002] where [Kullanıcı Adı]='" + reader["kulad"].ToString() + "' and COMPANY='" + ddlsirketsec.Value.ToString() + "'");
                        _Guid = Guid.NewGuid();
                        lblGuid.Text = _Guid.ToString();
                        TabSessions.Set(_Guid.ToString(), ddlsirketsec.Value.ToString(), reader["kulad"].ToString(), reader["İsim"].ToString(), Session["SelectedLanguage"].ToString(), "");
                        if (!CacheManager._isCacheDuring & CacheManager.CacheActive > 0)
                            CacheManager.StartCache();

                        if (changePass == "1")
                        {
                            Response.Redirect("sifre.aspx?GId=" + lblGuid.Text);
                        }
                        else
                        {
                            Response.Redirect("Home.aspx?GId=" + lblGuid.Text);
                        }

                    }
                    else
                    {
                        MessageBox("This user does not have access to the specified company");
                    }

                    reader.Dispose();
                    reader.Close();

                    connect.Close();
                }
            }


        }
        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }



        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Guid _Guid;

        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }


        protected void cbPanel_Callback(object sender, CallbackEventArgsBase e)
        {
            if (TextBox1.Text == "" | TextBox2.Text == "")
            {
                return;
            }

            string _user = TextBox1.Text;
            string _pass = TextBox2.Text;


            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(strConnString))
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand("SP_0WEB_USER_CONTROL", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@USER", _user);
                        cmd.Parameters.AddWithValue("@PASSWORD", _pass);
                        SqlDataAdapter adp = new SqlDataAdapter(cmd);
                        adp.Fill(ds);
                    }

                    switch (ds.Tables[0].Rows[0]["STATU"].ToString())
                    {
                        case "4":
                            Session["counter"] = Convert.ToInt32(Session["counter"]) + 1;
                            cbPanel.JSProperties["cp_result"] = "ERROR";
                            cbPanel.JSProperties["cp_Message"] = "Username or password is wrong.";
                            break;
                        case "3":
                            cbPanel.JSProperties["cp_result"] = "ERROR";
                            cbPanel.JSProperties["cp_Message"] = "User is blocked. Please contact your administrator..";
                            break;
                        case "2":
                            cbPanel.JSProperties["cp_result"] = "";
                            cbPanel.JSProperties["cp_Message"] = "";
                            Session["changePass"] = "1";
                            Session["counter"] = 0;
                            FillComp(ds.Tables[1]);
                            break;
                        case "1":
                            cbPanel.JSProperties["cp_result"] = "";
                            cbPanel.JSProperties["cp_Message"] = "";
                            Session["counter"] = 0;
                            FillComp(ds.Tables[1]);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    cbPanel.JSProperties["cp_result"] = "ERROR";
                    cbPanel.JSProperties["cp_Message"] = ex.Message.ToString();
                }
                finally
                {
                    conn.Close();
                    ClearTemp();
                }



            }


            Thread th = new Thread(() => UserControl(_user, _pass));
            th.Start();



        }

        private static void ClearTemp()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        public void UserControl(string _user, string _pass)
        {
            string _result = GetWebFonksiyonlari(customSirket).ExecGenericWebFunctionAllComp(_user, "50000", "ControlWebPassword§" + _user.ToUpper() + "§" + _pass + "§" + Session["counter"], customSirket);
            ClearTemp();
        }
    }
}
