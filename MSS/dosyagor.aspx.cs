﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.ServiceModel;
using System.Configuration;
using System.Data.SqlClient;
using MSS1.WSGeneric;
using System.Text;
using System.Threading;
using MSS1.Codes;

namespace MSS1
{
    public partial class dosyagor : Bases
    {
        fonksiyonlar f = new fonksiyonlar();
        SqlConnection baglanti;
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;

        public ws_baglantı ws = new ws_baglantı();

        SessionBase _obj;
        private string pageTur
        {
            get
            {
                try
                {
                    return Request.QueryString["pageTur"].ToString();
                }
                catch (Exception)
                {
                    return "";
                }
            }
        }



        #region Dil Getir
        public void dilgetir()
        {

            Language l = new Language();
            grideklenti.Columns[0].HeaderText = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "dosyagor", "Dosya Turu");
            grideklenti.Columns[1].HeaderText = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "dosyagor", "Dosya No");
            grideklenti.Columns[2].HeaderText = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "dosyagor", "Dosya Adı");
            grideklenti.Columns[3].HeaderText = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "dosyagor", "Yorum");
            grideklenti.Columns[4].HeaderText = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "dosyagor", "Eklenti Durumu");
            btndosyasil.Text = l.dilgetir(_obj.SelectedLanguage, _obj.Comp, _obj.UserName, "dosyagor", "btndosyasil");

        }
        #endregion


        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;

        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            dilgetir();
            if (!IsPostBack)
            {
                HdGId.Value = Request.QueryString["GId"];
                if (Request.QueryString["dtur"].ToString() == "28")
                {
                    grideklenti.Columns[grideklenti.Columns.Count - 3].Visible = false;
                }

                if (Request.QueryString["Y"] != null)
                {
                    if (Request.QueryString["Y"].ToString() == "1")
                    {
                        if (IsAuthorizedForDeleteFile() && pageTur != "2")
                        {
                            btndosyasil.Visible = true;
                        }
                        else
                        {
                            btndosyasil.Visible = false;
                            grideklenti.Columns[grideklenti.Columns.Count - 1].Visible = false;
                        }

                    }
                    else
                    {
                        btndosyasil.Visible = true;
                        // MessageBox("yetkilendirme yok");
                    }

                }
                string dosyano = Request.QueryString["BelgeNo"].ToString();
                if (Request.QueryString["NavType"] != null)
                {
                    string[] _params = { _obj.UserName, Request.QueryString["BelgeNo"].ToString(), Convert.ToInt32(Request.QueryString["NavType"]).ToString(), Convert.ToInt32(Request.QueryString["tur"]).ToString(), Convert.ToInt32(Request.QueryString["dtur"]).ToString() };
                    dosyano = GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "DosyaBelgeNoAl§" + string.Join("§", _params), _obj.Comp);
                    //MessageBox(_obj.UserName + "," + Request.QueryString["BelgeNo"].ToString() + "," + Request.QueryString["NavType"] + "," + Request.QueryString["tur"] + "," + Request.QueryString["dtur"] + "," + dosyano);
                }
                HdBelgeNo.Value = dosyano;
                if (dosyano.Length > 2)
                {
                    if (string.IsNullOrEmpty(Request.QueryString["Silme"]))
                    {
                        if (dosyano.Substring(0, 2).ToUpper() == "FT"
                            || dosyano.Substring(0, 3).ToUpper() == "SİF" || dosyano.Substring(0, 5).ToUpper() == "DNSİF")
                        {
                            //btndosyasil.Visible = false;
                            //grideklenti.Columns[grideklenti.Columns.Count - 1].Visible = false;
                        }
                    }

                }

                dosyadoldur(Convert.ToInt32(Request.QueryString["Tur"]), HdBelgeNo.Value);
                musteriadigetir(HdBelgeNo.Value, lblbaslik);

            }

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }


        private bool IsAuthorizedForDeleteFile()
        {
            return f.WebYetkiKontrol("", "", "AK102", "", "", "", "", _obj.Comp, _obj.UserName);
        }
        private void musteriadigetir(string musterino, Label lblad)
        {
            baglanti = new SqlConnection(strConnString);
            baglanti.Open();
            SqlCommand sorgu = new SqlCommand("Select Name from [0C_00018_00_CUSTOMER] where No_='" + musterino + "'", baglanti);
            SqlDataReader sqr;
            sqr = sorgu.ExecuteReader();
            if (sqr.Read())
            {
                lblad.Text = sqr["Name"].ToString();
            }
            else
            {
                lblad.Text = "";
            }

            sqr.Close();
        }
        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }
        private void dosyadoldur(int tur, string dosyano)
        {
            baglanti = new SqlConnection(strConnString);
            baglanti.Open();
            SqlCommand sorgu = new SqlCommand();

            sorgu.Connection = baglanti;
            sorgu.CommandType = CommandType.StoredProcedure;
            sorgu.CommandText = "[sp_0WEB_DOSYA_DOLDUR]";
            sorgu.Parameters.AddWithValue("@comp", _obj.Comp);
            SqlParameter psecim = new SqlParameter("@tur", SqlDbType.Int);
            psecim.Value = tur;
            sorgu.Parameters.Add(psecim);
            SqlParameter cno = new SqlParameter("@dosyano", SqlDbType.VarChar, 20);
            cno.Value = dosyano;
            sorgu.Parameters.Add(cno);
            SqlDataAdapter sorguDA = new SqlDataAdapter(sorgu);
            DataSet ds = new DataSet();
            sorguDA.Fill(ds);
            baglanti.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                grideklenti.DataSource = ds;
                grideklenti.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                grideklenti.DataSource = ds;
                grideklenti.DataBind();
                int columncount = grideklenti.Rows[0].Cells.Count;
                grideklenti.Rows[0].Cells.Clear();
                grideklenti.Rows[0].Cells.Add(new TableCell());
                grideklenti.Rows[0].Cells[0].ColumnSpan = columncount;
                grideklenti.Rows[0].Cells[0].Text = "Bağlanan Dosya Bulunamadı";
            }

        }
        private string GridViewSortDirection
        {

            get { return ViewState["SortDirection"] as string ?? "ASC"; }

            set { ViewState["SortDirection"] = value; }

        }
        private string GridViewSortExpression
        {

            get { return ViewState["SortExpression"] as string ?? string.Empty; }

            set { ViewState["SortExpression"] = value; }

        }
        private string GetSortDirection()
        {

            switch (GridViewSortDirection)
            {

                case "ASC":

                    GridViewSortDirection = "DESC";

                    break;

                case "DESC":

                    GridViewSortDirection = "ASC";

                    break;
            }

            return GridViewSortDirection;

        }
        protected DataView SortDataTable(DataTable dtable, bool isPageIndexChanging)
        {
            if (dtable != null)
            {
                DataView dview = new DataView(dtable);
                if (GridViewSortExpression != string.Empty)
                {
                    if (isPageIndexChanging)
                    {
                        dview.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                    }
                    else
                    {
                        dview.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                    }
                }
                return dview;
            }
            else
            {
                return new DataView();
            }
        }
        protected void grideklenti_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dosyadoldur(Convert.ToInt32(Request.QueryString["Tur"]), Request.QueryString["BelgeNo"]);
            grideklenti.DataSource = SortDataTable(grideklenti.DataSource as DataTable, true);
            grideklenti.PageIndex = e.NewPageIndex;
            grideklenti.DataBind();
        }

        protected void btndosyasil_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow grdRow in grideklenti.Rows)
            {

                if (((CheckBox)grdRow.FindControl("Chksec")).Checked)
                {
                    Label lblTur = (Label)grideklenti.Rows[grdRow.RowIndex].FindControl("lblTur");
                    Label lblDosyano = (Label)grideklenti.Rows[grdRow.RowIndex].FindControl("lblDosyano");
                    Label lbldosyasno = (Label)grideklenti.Rows[grdRow.RowIndex].FindControl("lbldosyasno");
                    Label lblexe = (Label)grideklenti.Rows[grdRow.RowIndex].FindControl("lblexe");



                    string[] _params = { Convert.ToInt32(lblTur.Text).ToString(), lblDosyano.Text, Convert.ToInt32(lbldosyasno.Text).ToString(), _obj.NameSurName };


                    switch (Convert.ToInt32(lblTur.Text))
                    {
                        case 0:
                            File.Delete(Server.MapPath(CurrentFolderName(_obj.Comp) + "\\Dosya\\SatinAlma\\") + lblDosyano.Text + lbldosyasno.Text + lblexe.Text);
                            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "DosyaSil§" + string.Join("§", _params), _obj.Comp);
                            break;
                        case 1:
                            File.Delete(Server.MapPath(CurrentFolderName(_obj.Comp) + "\\Dosya\\Satis\\") + lblDosyano.Text + lbldosyasno.Text + lblexe.Text);
                            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "DosyaSil§" + string.Join("§", _params), _obj.Comp);
                            break;
                        case 2:
                            File.Delete(Server.MapPath(CurrentFolderName(_obj.Comp) + "\\Dosya\\Masraf\\") + lblDosyano.Text + lbldosyasno.Text + lblexe.Text);
                            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "DosyaSil§" + string.Join("§", _params), _obj.Comp);
                            break;
                        case 3:
                            File.Delete(Server.MapPath(CurrentFolderName(_obj.Comp) + "\\Dosya\\Musteri\\") + lblDosyano.Text + lbldosyasno.Text + lblexe.Text);
                            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "DosyaSil§" + string.Join("§", _params), _obj.Comp);
                            break;
                        case 4:
                            File.Delete(Server.MapPath(CurrentFolderName(_obj.Comp) + "\\Dosya\\IadeSatis\\") + lblDosyano.Text + lbldosyasno.Text + lblexe.Text);
                            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "DosyaSil§" + string.Join("§", _params), _obj.Comp);
                            break;
                        case 6:
                            File.Delete(Server.MapPath(CurrentFolderName(_obj.Comp) + "\\Dosya\\EvrakKayit\\") + lblDosyano.Text + lbldosyasno.Text + lblexe.Text);
                            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "DosyaSil§" + string.Join("§", _params), _obj.Comp);
                            break;
                        case 7:
                            File.Delete(Server.MapPath(CurrentFolderName(_obj.Comp) + "\\Dosya\\Teklif\\") + lblDosyano.Text + lbldosyasno.Text + lblexe.Text);
                            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "DosyaSil§" + string.Join("§", _params), _obj.Comp);
                            break;
                        case 8:
                            File.Delete(Server.MapPath(CurrentFolderName(_obj.Comp) + "\\Dosya\\SBO\\") + lblDosyano.Text + lbldosyasno.Text + lblexe.Text);
                            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "DosyaSil§" + string.Join("§", _params), _obj.Comp);
                            break;
                        case 25:
                            File.Delete(Server.MapPath(CurrentFolderName(_obj.Comp) + "\\Dosya\\Izin\\") + lblDosyano.Text + lbldosyasno.Text + lblexe.Text);
                            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "DosyaSil§" + string.Join("§", _params), _obj.Comp);
                            break;
                        case 26:
                            File.Delete(Server.MapPath(CurrentFolderName(_obj.Comp) + "\\Dosya\\Contact\\") + lblDosyano.Text + lbldosyasno.Text + lblexe.Text);
                            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "DosyaSil§" + string.Join("§", _params), _obj.Comp);
                            break;
                        case 27:
                            File.Delete(Server.MapPath(CurrentFolderName(_obj.Comp) + "\\Dosya\\Iletisim\\") + lblDosyano.Text + lbldosyasno.Text + lblexe.Text);
                            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "DosyaSil§" + string.Join("§", _params), _obj.Comp);
                            break;
                        case 28:
                            File.Delete(Server.MapPath(CurrentFolderName(_obj.Comp) + "\\Dosya\\SatinAlma_Onay\\") + lblDosyano.Text + lbldosyasno.Text + lblexe.Text);
                            GetWebFonksiyonlari("LAMLOJ").ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "DosyaSil§" + string.Join("§", _params), _obj.Comp);
                            break;

                    }
                    dosyadoldur(Convert.ToInt32(Request.QueryString["Tur"]), Request.QueryString["BelgeNo"]);


                    if (!ClientScript.IsStartupScriptRegistered("JsReloadx"))
                    {
                        ClientScript.RegisterStartupScript(GetType(), "JsReloadx", @"<script type=""text/javascript""> "
                        + "window.opener.location.href=window.opener.location.href; self.close();</script>");
                    }

                }
            }

        }
        protected void grideklenti_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblDosyaPath = (Label)e.Row.FindControl("lblDosyaPath");
                HyperLink lnk = (HyperLink)e.Row.FindControl("hlPlus");

                string _filepath = lblDosyaPath.Text;
                string js = string.Format("javascript:open_URL('{0}');", _filepath.Replace(@".\", string.Empty).Replace(@"\", "|"));
                if (lnk != null)
                {
                    lnk.NavigateUrl = js;
                }

            }
        }
        protected void grideklenti_RowCommand(object sender, GridViewCommandEventArgs e)
        {
        }
    }
}