﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.Codes;

namespace MSS1
{
    public partial class Download : Bases
    {
        SessionBase _obj;
        protected void Page_Load(object sender, EventArgs e)
        {
            string sID = this.Request.Params["id"].Replace("'", "");
            if ((sID != null) && (sID.Trim() != "0"))
            {
                string DosyaAdi = this.Request.Params["FileName"].Replace("'", "");

                string _FilePath = Server.MapPath("~/" + CurrentFolderName(_obj.Comp) + "/Dosya/Mutabakat/") + DosyaAdi;
                FileInfo info = new FileInfo(_FilePath);
                string enCodeFileName = Server.UrlEncode(DosyaAdi);
                this.Response.AddHeader("Content-Disposition", "attachment;filename=" + enCodeFileName);
                Response.AddHeader("Content-Length", info.Length.ToString());
                Response.ContentType = "application/octet-stream";
                this.Response.TransmitFile(_FilePath);


            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }


    }
}