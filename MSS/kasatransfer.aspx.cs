﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.ServiceModel;
using System.Data;
using System.Xml.Linq;
using System.Globalization;
using MSS1.WSGeneric;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using MSS1.Codes;

namespace MSS1
{
    public partial class kasatransfer : Bases
    {
        Language l = new Language();
        fonksiyonlar f = new fonksiyonlar();
        SqlConnection baglanti;
        string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;

        SessionBase _obj;

        public ws_baglantı ws = new ws_baglantı();

        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }

            //if (!IsPostBack)
            //{
            kasahareketdoldur(_obj.Comp);
            //}
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }
        protected void btnTransfer_Click(object sender, EventArgs e)
        {
            string reportPath = f.GetReportPath("KASA TESLİM FL.rpt", _obj.Comp);
            using (ReportDocument crystalReport = new ReportDocument())
            {

                crystalReport.Load(reportPath);
                crystalReport.ReportOptions.EnableSaveDataWithReport = false;
                crystalReport.SetParameterValue("ÖNİZLEME YAZDIR", "2");
                crystalReport.SetParameterValue("@COMP", _obj.Comp);
                crystalReport.SetParameterValue("@USER", _obj.UserName);
                crystalReport.ExportToDisk(ExportFormatType.PortableDocFormat, Server.MapPath(CurrentFolderName(_obj.Comp) + "/PDF/TRANSFERPDF/" + lblbelgeno.Text + ".pdf"));
                ClientScript.RegisterStartupScript(this.Page.GetType(), "popupOpener", "var popup=window.open('"+ CurrentFolderName(_obj.Comp) + "/PDF/TRANSFERPDF/" + lblbelgeno.Text + ".pdf');popup.focus();", true);

            }

            //ReportDocument crystalReport;
            //crystalReport = new ReportDocument();
            //crystalReport.Load(Server.MapPath("~/KASA TESLİM FL.rpt").Replace("stena", "Raporlar"));
            //crystalReport.ReportOptions.EnableSaveDataWithReport = false;
            //crystalReport.SetDatabaseLogon("sa", "Password1");
            //crystalReport.SetParameterValue("ÖNİZLEME YAZDIR", "2");
            //crystalReport.ExportToDisk(ExportFormatType.PortableDocFormat, Server.MapPath("PDF/TRANSFERPDF/" + lblbelgeno.Text + ".pdf"));
            //ClientScript.RegisterStartupScript(this.Page.GetType(), "popupOpener", "var popup=window.open('PDF/TRANSFERPDF/" + lblbelgeno.Text + ".pdf');popup.focus();", true);
             
            lblbelgeno.Text = GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName,"50007", "KasaTransfer§" +
              _obj.NameSurName + "§"+ ddlcurrency.SelectedValue + "§" + Convert.ToDecimal(txtTutar.Text).ToString() + "§" + txtteslimkisi.Text + "§" +
              "0",_obj.Comp);



            //MessageBox("Transfer Başarılı.");
            ddlcurrency.SelectedIndex = -1;
            txtTutar.Text = string.Empty;
            txtteslimkisi.Text = string.Empty;
            btnTransfer.Visible = false;
            ClientScript.RegisterStartupScript(this.Page.GetType(), "Javascript", "javascript: proceed();", true);
            kasahareketdoldur(_obj.Comp);
        }

        private void kasahareketdoldur(string sirket)
        {
            baglanti = new SqlConnection(strConnString);
            baglanti.Open();
            SqlCommand sorgu = new SqlCommand("select * from[0T_00271_50_BANK LEGDER_SEALINE_KASA_TRANSFER] where COMPANY = '" + sirket + "' Order By documentno desc", baglanti);
            SqlDataAdapter ad = new SqlDataAdapter(sorgu);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            baglanti.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                MusteriGrid.DataSource = ds;
                MusteriGrid.DataBind();
            }
            else
            {
                //ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                //MusteriGrid.DataSource = ds;
                //MusteriGrid.DataBind();
                //int columncount = MusteriGrid.Rows[0].Cells.Count;
                //MusteriGrid.Rows[0].Cells.Clear();
                //MusteriGrid.Rows[0].Cells.Add(new TableCell());
                //MusteriGrid.Rows[0].Cells[0].ColumnSpan = columncount;
                //MusteriGrid.Rows[0].Cells[0].Text = "Muhasebe hareketi bulunamadı.";
                //Response.Redirect("rezervasyongirisi.aspx");

            }

        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }
        protected void btnOnizle_Click(object sender, EventArgs e)
        {
            if (txtTutar.Text != string.Empty & txtteslimkisi.Text != string.Empty)
            { 
                lblbelgeno.Text = GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName,"50007", "KasaTransfer§" +
                    _obj.NameSurName + "§" + ddlcurrency.SelectedValue + "§" + txtTutar.Text + "§" + txtteslimkisi.Text + "§" + "1", _obj.Comp);
                 
                //ddlcurrency.SelectedIndex = -1;
                //txtTutar.Text = string.Empty;
                this.ClientScript.RegisterClientScriptBlock(this.GetType(), "popupOpener", "var popup=window.open('Raporlar/DynamicReport.aspx?ReportCode=RP063&GId=" + Request.QueryString["GId"]+"');popup.focus();", true);
                btnTransfer.Visible = true;
            }
            else
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "kasatransfer", "msg001"));
                MessageBox("Tutar veya teslim edilen kişi bilgisini girmediniz.");
            }
        }
        protected void ddlcurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnTransfer.Visible = false;  
            GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName,"50007", "KasaTransferSil"   , _obj.Comp);

        }
        protected void txtteslimkisi_TextChanged(object sender, EventArgs e)
        {
            btnTransfer.Visible = false;
        }
        protected void txtTutar_TextChanged(object sender, EventArgs e)
        {
            btnTransfer.Visible = false;
        }

        private string GridViewSortDirection
        {

            get { return ViewState["SortDirection"] as string ?? "ASC"; }

            set { ViewState["SortDirection"] = value; }

        }
        private string GridViewSortExpression
        {

            get { return ViewState["SortExpression"] as string ?? string.Empty; }

            set { ViewState["SortExpression"] = value; }

        }
        private string GetSortDirection()
        {

            switch (GridViewSortDirection)
            {

                case "ASC":

                    GridViewSortDirection = "DESC";

                    break;

                case "DESC":

                    GridViewSortDirection = "ASC";

                    break;
            }

            return GridViewSortDirection;

        }
        protected DataView SortDataTable(DataTable dtable, bool isPageIndexChanging)
        {
            if (dtable != null)
            {
                DataView dview = new DataView(dtable);
                if (GridViewSortExpression != string.Empty)
                {
                    if (isPageIndexChanging)
                    {
                        dview.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                    }
                    else
                    {
                        dview.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                    }
                }
                return dview;
            }
            else
            {
                return new DataView();
            }
        }

        //protected void MusteriGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    kasahareketdoldur(_obj.Comp);
        //    MusteriGrid.DataSource = SortDataTable(MusteriGrid.DataSource as DataTable, true);
        //    MusteriGrid.PageIndex = e.NewPageIndex;
        //    MusteriGrid.DataBind();
        //}

    }
}