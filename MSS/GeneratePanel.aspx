﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GeneratePanel.aspx.cs" Inherits="MSS1.GeneratePanel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Generate Panel</title>
     <link href="style.css" rel="stylesheet" />
    <link rel="stylesheet" runat="server" href="./CKEditor/skins/moono/editor.css" />
    <script type="text/javascript" src="./js/jsnew/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

    <script type="text/javascript" src="./CKEditor/ckeditor.js"></script>
    <script type="text/javascript" src="./CKEditor/config.js"></script>
    <link href="css/MasterPage.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
         
      <textarea runat="server" placeholder="Haber metnini girin." name="editor1" style="height: 700px" id="editor1" rows="10" cols="80">
            </textarea>
                        <script type="text/javascript">
                            // Replace the <textarea id="editor1"> with a CKEditor
                            // instance, using default configuration.
                            // CKEDITOR.replace('BodyContent_editor1');
                            CKEDITOR.replace('<%=editor1.ClientID %>',
                {
                    filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
                    filebrowserImageBrowseUrl: './Upload.ashx/',
                    filebrowserFlashBrowseUrl: '/ckfinder/ckfinder.html?Type=Flash',
                    filebrowserUploadUrl: '/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Files',
                    filebrowserImageUploadUrl: './Upload.ashx',
                    filebrowserFlashUploadUrl: '/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Flash',
                });
                        </script>
        <asp:Button runat="server" Text="Show Text" ID="show_text" OnClick="show_text_Click" />
        <asp:Button runat="server" Text="Generate Panel" ID="generate_panel" OnClick="generate_panel_Click" />
        <div style="margin-top:50px; margin-bottom:50px; border:1px solid black;">
            <asp:Label ID="lblNewsLong"  runat="server" ></asp:Label>
        </div>
          <div style="margin-top:50px; margin-bottom:50px; border:1px solid black;">
         <asp:TextBox runat="server" ID="txtResultPanel" TextMode="MultiLine" Rows="20" style="width:100%" Visible="false"></asp:TextBox>
    </div>
          </div>
    </form>
</body>
</html>
