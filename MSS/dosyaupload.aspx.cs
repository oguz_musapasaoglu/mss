﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.Codes;

namespace MSS1
{
    public partial class dosyaupload : Bases
    {
        ws_baglantı ws = new ws_baglantı();

        SessionBase _obj;
        #region Dil Getir
        public void dilgetir()
        {

            Language l = new Language();
            lblSonuc.Text = l.dilgetir(_obj.SelectedLanguage ,_obj.Comp.ToString(), _obj.UserName.ToString(), "dosyaupload", "lblSonuc");

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
         
            _obj = TabSessions.ControlState(this.Page, Request.QueryString["GId"].ToString());
            if (Session["uyedurum"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            dilgetir();
            string _path = Request.QueryString["DosyaPath"];
            _path = _path.Replace("|", "\\");
            string _comppath = CurrentFolderName(_obj.Comp);
            if (!_path.Contains(_comppath))
                _path = _comppath + @"\\" + _path; 
            if ((Request.Url.ToString()).Contains(ws.dısip))
            {

                Response.Redirect(ws.dısip + _path);
            }
            else
            {
                Response.Redirect(ws.icip + _path);
            }
            ImgAttach.Visible = false;
        }
    }
}