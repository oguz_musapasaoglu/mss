﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="OnePageList2.aspx.cs" Inherits="MSS1.OnePageList2" %>

<%@ Register Src="~/Controls/DataTableControlGAC.ascx" TagName="StDataTable" TagPrefix="STDT" %>

<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript"> 

        function ShowModalDatePopup() {
            DatePopup.Show();
        }

        function HideModalDatePopup(s, e) {
            ConfirmMessage(s, e);
            DatePopup.Hide();
        }

        function ConfirmMessage(s, e) {
            var ktfMessage = document.getElementById('ctl00_ContentPlaceHolder1_callbackKTFMessage_txtKTFmessage_I').value;
            e.processOnServer = confirm(ktfMessage);
        }

        function OnGridFocusedRowChanged(s, e) {
            var link = $("#" + "<%= txtlink.ClientID %>").val();
            var linkfield = "<%= txtlinkField.ClientID %>";
            if (link == "JOB") {
                s.GetRowValues(s.GetFocusedRowIndex(), $("#" + linkfield).val(), OnGetRowValuesJob);
            }
            else if (link == "FILTER") {
                s.GetRowValues(s.GetFocusedRowIndex(), $("#" + linkfield).val(), OnGetRowValuesFilter);
            }
            else {
                s.GetRowValues(s.GetFocusedRowIndex(), $("#" + linkfield).val(), OnGetRowValues);
            }

            var filterobject = "<%= txtFilterText.ClientID %>";
            var param = $("#" + filterobject).val();

        }

        function OnGetRowValues(values) {
            var linkFormat = "<%= txtlink.ClientID %>";
            var link = $("#" + linkFormat).val();
            link = link.replace("XXXXX", values[0]);
            link = link.replace("YYYYY", values[1]);
            window.location.href = AdGIdToLink(link);
            window.focus();
        }

        function AdGIdToLink(_val) {
            if (_val.includes("GId=")) return _val;
            var ObjGId = "<%= lblGId.ClientID %>";
            if (_val.includes("?"))
                _val = _val + "&GId=" + $("#" + ObjGId).text();
            else
                _val = _val + "?GId=" + $("#" + ObjGId).text();

            return _val;

        }

        function OnGetRowValuesFilter(values) {
            var queryText = "";
            var linkFormat = "<%= txtlinkField.ClientID %>";
            var filterobject = "<%= txtFilterText.ClientID %>";
            var link = $("#" + linkFormat).val();
            var filterFields = link.split(';');

            if (filterFields.length == 1) {
                queryText = filterFields[0] + " = '" + values + "'";
            }
            else if (filterFields.length == 2) {
                queryText = filterFields[0] + " = '" + values[0] + "' and " + filterFields[1] + " = '" + values[1] + "'";
            }
            else if (filterFields.length == 3) {
                queryText = filterFields[0] + " = '" + values[0] + "' and " + filterFields[1] + " = '" + values[1] + " and " + filterFields[2] + " = '" + values[2] + "'";
            }
            else if (filterFields.length == 4) {
                queryText = filterFields[0] + " = '" + values[0] + "' and " + filterFields[1] + " = '" + values[1] + " and " + filterFields[2] + " = '" + values[2] + "' and " + filterFields[3] + " = '" + values[3] + "'";
            }
            var splitter = values.toString().split(",");

            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value = '';
            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP2').value = '';

            if (splitter.length == 1) {
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value = splitter[0];

            }
            else if (splitter.length == 2) {
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value = splitter[0];
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP2').value = splitter[1];
            }

            var pane = xsplitter.GetPaneByName('MiddleContainer');
            if (pane.IsCollapsed())
                document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_1_S_CB_Img').click();

            grid3.PerformCallback(queryText);
        }

        function OnGetRowValuesJob(values) {

            var splitter = values.split(';');
            var targetLink = "";
            if (splitter[1] == "1") {
                window.open("/Jobs/mainJobs.aspx?MJID=" + splitter[0]);
            }
            else {

                window.open("/projeolustur.aspx?Tur=1&ProjeNo=" + splitter[0]);
            }
        }

    </script>
    <asp:Label ID="lblden" runat="server"></asp:Label>
    <table>
        <tr>
            <td>
                <dx:ASPxMenu ID="menu2" runat="server" AutoSeparators="RootOnly"
                    Theme="Glass"
                    ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                    ShowSubMenuShadow="False" OnItemClick="menu_ItemClick">
                    <SubMenuStyle GutterWidth="0px" />
                    <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                    <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                    </SubMenuItemStyle>
                    <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                </dx:ASPxMenu>
                <dx:ASPxGridView ID="grid2" ClientInstanceName="grid2" Caption="" runat="server" AutoGenerateColumns="False"
                    Theme="Glass" EnableTheming="false" DataSourceID="dtgrid2" KeyFieldName="ID" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared" OnCustomButtonCallback="grid_CustomButtonCallback"
                    EnableRowsCache="false">
                    <Settings />
                    <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" />
                    <SettingsEditing Mode="EditForm" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                    <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                    <SettingsCommandButton>
                        <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                        </CancelButton>
                        <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                        </UpdateButton>
                    </SettingsCommandButton>
                    <SettingsText EmptyDataRow=" " />
                    <SettingsEditing Mode="EditForm" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                    <Styles>


                        <Header ForeColor="White"></Header>
                        <HeaderPanel ForeColor="White"></HeaderPanel>
                        <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                    </Styles>
                </dx:ASPxGridView>
                <dx:ASPxGridViewExporter ID="gridExport2" ExportedRowType="All" runat="server" GridViewID="grid2"></dx:ASPxGridViewExporter>
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
            <td>&nbsp;
            </td>
        </tr>
        <tr>

            <td style="width: 100%; vertical-align: top">

                <dx:ASPxSplitter Theme="iOS" ID="ASPxSplitter1" Height="480px" ClientInstanceName="xsplitter" runat="server" Width="99%">
                    <Panes>
                        <dx:SplitterPane ShowCollapseBackwardButton="True" ShowSeparatorImage="True" Size="25%" Name="LeftContainer">
                            <Panes>
                                <dx:SplitterPane ShowCollapseBackwardButton="True" ShowSeparatorImage="True" Size="80%" Name="Leftinside">
                                    <Panes>
                                        <dx:SplitterPane Size="90%" ScrollBars="Auto">
                                            <ContentCollection>
                                                <dx:SplitterContentControl ID="SPContent" runat="server">
                                                    <dx:ASPxMenu ID="menu" runat="server" AutoSeparators="RootOnly"
                                                        Theme="Glass"
                                                        ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                        ShowSubMenuShadow="False" OnItemClick="menu_ItemClick">
                                                        <SubMenuStyle GutterWidth="0px" />
                                                        <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                        <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                        </SubMenuItemStyle>
                                                        <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                                    </dx:ASPxMenu>
                                                    <dx:ASPxGridView ID="grid" ClientInstanceName="grid" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                                        Theme="Glass" OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnRowDeleting="grid_RowDeleting" OnCustomButtonCallback="grid_CustomButtonCallback"
                                                        EnableTheming="false" DataSourceID="dtgrid1" KeyFieldName="ID" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared">
                                                        <Settings />
                                                        <ClientSideEvents RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e); } " />
                                                        <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" AllowFocusedRow="true" />
                                                        <SettingsEditing Mode="EditForm" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                        <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                                                        <SettingsText EmptyDataRow=" " />
                                                        <SettingsCommandButton>
                                                            <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                            </CancelButton>
                                                            <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                            </UpdateButton>
                                                        </SettingsCommandButton>
                                                        <SettingsEditing Mode="EditForm" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                        <Styles>


                                                            <Header ForeColor="White"></Header>
                                                            <HeaderPanel ForeColor="White"></HeaderPanel>
                                                            <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                        </Styles>
                                                    </dx:ASPxGridView>
                                                    <dx:ASPxGridViewExporter ID="gridExport" ExportedRowType="All" runat="server" GridViewID="grid"></dx:ASPxGridViewExporter>
                                                </dx:SplitterContentControl>
                                            </ContentCollection>
                                        </dx:SplitterPane>
                                    </Panes>
                                    <ContentCollection>
                                        <dx:SplitterContentControl runat="server"></dx:SplitterContentControl>
                                    </ContentCollection>
                                </dx:SplitterPane>
                            </Panes>
                            <ContentCollection>
                                <dx:SplitterContentControl runat="server"></dx:SplitterContentControl>
                            </ContentCollection>
                        </dx:SplitterPane>
                        <dx:SplitterPane ShowCollapseForwardButton="True" ShowSeparatorImage="True" Size="65%" Name="MiddleContainer" ScrollBars="Auto">
                            <Panes>
                                <dx:SplitterPane ShowCollapseBackwardButton="True" ShowSeparatorImage="True" Size="80%" Name="Middleinside">
                                    <ContentCollection>
                                        <dx:SplitterContentControl>
                                            <dx:ASPxMenu ID="menu3" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu3_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid3" ClientInstanceName="grid3" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid3_CustomCallback" OnCustomButtonCallback="grid_CustomButtonCallback"
                                                EnableTheming="false" DataSourceID="dtgrid3" KeyFieldName="ID" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared">
                                                <Settings />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" AllowFocusedRow="true" />
                                                <SettingsEditing Mode="EditForm" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <SettingsPager PageSize="7" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <SettingsEditing Mode="EditForm" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport3" ExportedRowType="All" runat="server" GridViewID="grid3"></dx:ASPxGridViewExporter>

                                        </dx:SplitterContentControl>
                                    </ContentCollection>
                                </dx:SplitterPane>
                            </Panes>
                            <ContentCollection>
                                <dx:SplitterContentControl runat="server"></dx:SplitterContentControl>
                            </ContentCollection>
                        </dx:SplitterPane>

                    </Panes>
                </dx:ASPxSplitter>

            </td>
        </tr>
    </table>

    <STDT:StDataTable ID="dtgrid1" runat="server" />
    <STDT:StDataTable ID="dtgrid2" runat="server" />
    <STDT:StDataTable ID="dtgrid3" runat="server" />
    <STDT:StDataTable ID="DS11" runat="server" />
    <STDT:StDataTable ID="DS12" runat="server" />
    <STDT:StDataTable ID="DS13" runat="server" />
    <STDT:StDataTable ID="DS14" runat="server" />
    <STDT:StDataTable ID="DS15" runat="server" />
    <STDT:StDataTable ID="DS16" runat="server" />
    <STDT:StDataTable ID="DS17" runat="server" />
    <STDT:StDataTable ID="DS18" runat="server" />
    <STDT:StDataTable ID="DS19" runat="server" />
    <STDT:StDataTable ID="DS110" runat="server" />
    <STDT:StDataTable ID="DS111" runat="server" />
    <STDT:StDataTable ID="DS112" runat="server" />
    <STDT:StDataTable ID="DS113" runat="server" />
    <STDT:StDataTable ID="DS114" runat="server" />
    <STDT:StDataTable ID="DS115" runat="server" />
    <STDT:StDataTable ID="DS116" runat="server" />
    <STDT:StDataTable ID="DS117" runat="server" />
    <STDT:StDataTable ID="DS118" runat="server" />
    <STDT:StDataTable ID="DS119" runat="server" />
    <STDT:StDataTable ID="DS120" runat="server" />


    <STDT:StDataTable ID="DS21" runat="server" />
    <STDT:StDataTable ID="DS22" runat="server" />
    <STDT:StDataTable ID="DS23" runat="server" />
    <STDT:StDataTable ID="DS24" runat="server" />
    <STDT:StDataTable ID="DS25" runat="server" />
    <STDT:StDataTable ID="DS26" runat="server" />
    <STDT:StDataTable ID="DS27" runat="server" />
    <STDT:StDataTable ID="DS28" runat="server" />
    <STDT:StDataTable ID="DS29" runat="server" />
    <STDT:StDataTable ID="DS210" runat="server" />
    <STDT:StDataTable ID="DS211" runat="server" />
    <STDT:StDataTable ID="DS212" runat="server" />
    <STDT:StDataTable ID="DS213" runat="server" />
    <STDT:StDataTable ID="DS214" runat="server" />
    <STDT:StDataTable ID="DS215" runat="server" />
    <STDT:StDataTable ID="DS216" runat="server" />
    <STDT:StDataTable ID="DS217" runat="server" />
    <STDT:StDataTable ID="DS218" runat="server" />
    <STDT:StDataTable ID="DS219" runat="server" />
    <STDT:StDataTable ID="DS220" runat="server" />



    <div style="display: none">
        <asp:Label ID="lblGId" runat="server"></asp:Label>
        <asp:TextBox ID="txtlink" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtFilterText" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams2" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams2" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams2" runat="server"></asp:TextBox>

        <asp:TextBox ID="txtinsertparams3" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams3" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams3" runat="server"></asp:TextBox>

        <asp:HiddenField runat="server" ID="HiddenLoadqueryText" Value="AA" />
        <asp:HiddenField runat="server" ID="HiddenP1" Value="" />
        <asp:HiddenField runat="server" ID="HiddenP2" Value="" />
        <asp:HiddenField runat="server" ID="HiddenMainStatu" Value="" />
        <asp:HiddenField runat="server" ID="HiddenpageType" Value="" />
        <asp:HiddenField runat="server" ID="HiddenIslemId" Value="" />

        <asp:TextBox ID="txtCurGridId" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCurGridObjecId" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCurGridPageId" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCurGridFieldId" runat="server" Text=""></asp:TextBox>
    </div>
    <dx:ASPxPopupControl ID="DatePopup" runat="server" CloseAction="CloseButton" CloseOnEscape="true" Modal="true" ClientInstanceName="DatePopup"
        HeaderText="Tarih Alanını Doldurunuz" AllowDragging="true" PopupAnimationType="Slide" EnableViewState="false"
        Left="378" Top="156">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <asp:Panel ID="pnlDateContent" runat="server">
                    <dx:ASPxFormLayout ID="Box2" runat="server" Width="100%">
                        <Items>
                            <dx:LayoutGroup ShowCaption="False" ColCount="3">
                                <Items>
                                    <dx:LayoutItem FieldName="TARIH">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxDateEdit ID="txtDate" runat="server" EditFormatString="dd.MM.yyyy" DisplayFormatString="dd.MM.yyyy"></dx:ASPxDateEdit>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                    <dx:LayoutItem FieldName="" ShowCaption="False">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxButton ID="BtnProc" runat="server" CssClass="formglobalbuttonStyle" OnClick="BtnProc_Click" Text="İŞLEMİ YAP">                                                    
                                                    <ClientSideEvents Click="function(s,e) { HideModalDatePopup(s,e); }" />
                                                </dx:ASPxButton>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>
                                </Items>
                            </dx:LayoutGroup>
                        </Items>
                    </dx:ASPxFormLayout>
                </asp:Panel>

            </dx:PopupControlContentControl>
        </ContentCollection>

    </dx:ASPxPopupControl>
     <dx:ASPxCallbackPanel ID="callbackKTFMessage" ClientInstanceName="callbackKTFMessage" runat="server" OnCallback="callbackKTFMessage_Callback">
        <PanelCollection>
            <dx:PanelContent>
                <div style="display: none">
                    <dx:ASPxTextBox ID="txtKTFmessage" runat="server"></dx:ASPxTextBox>
                </div>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>

