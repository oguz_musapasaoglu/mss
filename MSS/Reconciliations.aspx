﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Reconciliations.aspx.cs" Inherits="MSS1.Reconciliations" %>

<!DOCTYPE html>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .myButton {
            -moz-box-shadow: inset 0px 0px 0px 0px #f0f7fa;
            -webkit-box-shadow: inset 0px 0px 0px 0px #f0f7fa;
            box-shadow: inset 0px 0px 0px 0px #f0f7fa;
            background-color: transparent;
            border: 1px solid #057fd0;
            display: inline-block;
            color: #ffffff;
            font-family: 'Arial Unicode MS';
            font-size: x-small;
            font-weight: bold;
            padding: 6px 12px;
            text-decoration: none;
            color: #003e83;
        }

        .textbox {
            /*background-color: #ffffff;*/
            border: 2px solid #a7bcd4;
            background-color: transparent;
            -ms-filter: progid:DXImageTransform.Microsoft.Alpha(Opacity = 70);
            filter: alpha(opacity = 70);
            text-indent: 3px;
            font-weight: bold;
            font-size: 13px;
            text-align: justify;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
        }

        .Alan {
            /*background-color: #ffffff;*/
            background-color: transparent;
            -ms-filter: progid:DXImageTransform.Microsoft.Alpha(Opacity = 70);
            filter: alpha(opacity = 70);
            text-indent: 3px;
            font-weight: bold;
            font-size: 13px;
            text-align: left;
        }

        td {
            font-family: 'Arial Unicode MS';
            font-size: x-small;
            font-weight: bold;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function checkTextAreaMaxLength(textBox, e, length) {

            var mLen = textBox["MaxLength"];
            if (null == mLen)
                mLen = length;

            var maxLength = parseInt(mLen);
            if (!checkSpecialKeys(e)) {
                if (textBox.value.length > maxLength - 1) {
                    if (window.event)//IE
                    {
                        e.returnValue = false;
                        return false;
                    }
                    else//Firefox
                        e.preventDefault();
                }
            }
        }

        function fncKeyLength(text, limit) {
            if (window.event.ctrlKey) {
                if (window.event.keyCode == 86) {
                    if (text.value.length > limit) {
                        text.value = '';
                        alert("yorum bilgisi 750 karakterden fazla olamaz lütfen kontrol ediniz!");
                    }
                }
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:HiddenField ID="HdGId" runat="server" />
            <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptLocalization="true"
                EnableScriptGlobalization="true" EnablePageMethods="true">
            </asp:ScriptManager>
            <table style="width: 100%" border="0">
                <tr>
                    <td style="width: 16.6%">
                        <asp:Label ID="lblDovCinsB" CssClass="Alan" Width="95%" runat="server" ForeColor="#403F3F"></asp:Label></td>
                    <td style="width: 16.6%">
                        <asp:DropDownList ID="ddldoviz" CssClass="textbox" Width="97%" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 16.6%">
                        <asp:Label ID="lblMutabakatTarihB" CssClass="Alan" Width="95%" runat="server" ForeColor="#403F3F"></asp:Label></td>
                    <td style="width: 16.6%">
                        <asp:TextBox ID="txtMutabakatTarih" CssClass="textbox" runat="server" Width="95%"
                            Style="text-align: justify" />
                        <cc1:CalendarExtender ID="txtMutabakatTarih_CalendarExtender" runat="server" TargetControlID="txtMutabakatTarih"
                            Format="dd.MM.yyyy" />
                    </td>
                    <td style="width: 16.6%">
                        <asp:Label ID="lblGecerlilikTarihiB" CssClass="Alan" Width="95%" runat="server" ForeColor="#403F3F"></asp:Label></td>
                    <td style="width: 16.6%">
                        <asp:TextBox ID="txtGecerlilikTarihi" CssClass="textbox" runat="server" Width="95%"
                            Style="text-align: justify" />
                        <cc1:CalendarExtender ID="txtGecerlilikTarihi_CalendarExtender" runat="server" TargetControlID="txtGecerlilikTarihi"
                            Format="dd.MM.yyyy" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 16.6%; vertical-align: top">
                        <asp:Label ID="lblMutabikKalinanTutarB" CssClass="Alan" Width="95%" runat="server" ForeColor="#403F3F"></asp:Label></td>
                    <td style="width: 16.6%">
                        <asp:TextBox CssClass="textbox" ID="txtMutabakatTutari" runat="server" Width="95%"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="txtMutabakatTutari_FilteredTextBoxExtender" runat="server"
                            TargetControlID="txtMutabakatTutari" FilterType="Numbers,Custom" ValidChars="..">
                        </cc1:FilteredTextBoxExtender>
                    </td>
                    <td style="width: 16.6%; vertical-align: top">
                        <asp:Label ID="lblMutabikKalindiB" CssClass="Alan" Width="95%" runat="server" ForeColor="#403F3F"></asp:Label></td>
                    <td style="width: 16.6%">
                        <asp:DropDownList ID="ddlMutabikKalindi" Width="97%" CssClass="textbox" runat="server">
                            <asp:ListItem Selected="True" Text="" Value="0"></asp:ListItem>
                            <asp:ListItem Text="EVET" Value="1"></asp:ListItem>
                            <asp:ListItem Text="HAYIR" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 16.6%; vertical-align: top">
                        <asp:Label ID="lblMutabakatTuruB" CssClass="Alan" Width="95%" runat="server" ForeColor="#403F3F"></asp:Label></td>
                    <td style="width: 16.6%">
                        <asp:DropDownList ID="ddlMutabakatTuru" Width="97%" CssClass="textbox" runat="server">
                            <asp:ListItem Selected="True" Text="" Value="0"></asp:ListItem>
                            <asp:ListItem Text="GENEL MUTABAKAT" Value="1"></asp:ListItem>
                            <asp:ListItem Text="NAV MUTABAKAT MEKTUBU" Value="2"></asp:ListItem>
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td style="width: 16.6%; vertical-align: top">
                        <asp:Label ID="lblMutabakatSirketiB" CssClass="Alan" Width="95%" runat="server" ForeColor="#403F3F"></asp:Label></td>
                    <td style="width: 16.6%">
                        <asp:DropDownList ID="ddlMutabakatSirketi" Width="97%" CssClass="textbox" runat="server">
                            <asp:ListItem Selected="True" Text="" Value="0"></asp:ListItem>
                            <asp:ListItem Text="GAC" Value="1"></asp:ListItem>
                            <asp:ListItem Text="LAM" Value="2"></asp:ListItem>
                            <asp:ListItem Text="LAM GLOBAL" Value="3"></asp:ListItem>
                            <asp:ListItem Text="MLH" Value="4"></asp:ListItem>
                            <asp:ListItem Text="LAMKARA" Value="5"></asp:ListItem>
                            <asp:ListItem Text="SEALINE" Value="6"></asp:ListItem>
                            <asp:ListItem Text="MOBILIS" Value="7"></asp:ListItem>
                            <asp:ListItem Text="SINOTRANS" Value="8"></asp:ListItem>
                            <asp:ListItem Text="LUCENT" Value="9"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 16.6%"></td>
                    <td style="width: 16.6%"></td>
                    <td style="width: 16.6%"></td>
                    <td style="width: 16.6%"></td>
                </tr>
                <tr>
                    <td style="width: 16.6%; vertical-align: top">
                        <asp:Label ID="lblYorumB" CssClass="Alan" Width="95%" runat="server" ForeColor="#403F3F"></asp:Label></td>
                    <td colspan="5" style="vertical-align: top">
                        <asp:TextBox ID="txtYorum" MaxLength="750" runat="server" CssClass="textbox"
                            Height="70px" onkeyDown="return checkTextAreaMaxLength(this,event,'750');" onKeyUp="fncKeyLength(this,750);"
                            TextMode="MultiLine" Width="99%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 16.6%; vertical-align: top">
                        <asp:Label ID="lblDosyaB" Text="Dosya Yükle 1" CssClass="Alan" Width="95%" runat="server" ForeColor="#403F3F"></asp:Label></td>
                    <td colspan="5">
                        <asp:FileUpload CssClass="myButton" Width="97%" ID="FlpAttach0" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 16.6%; vertical-align: top">
                        <asp:Label ID="Label1" Text="Dosya Yükle 2" CssClass="Alan" Width="95%" runat="server" ForeColor="#403F3F"></asp:Label></td>
                    <td colspan="5">
                        <asp:FileUpload CssClass="myButton" Width="97%" ID="FlpAttach1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 16.6%; vertical-align: top">
                        <asp:Label ID="Label2" Text="Dosya Yükle 3" CssClass="Alan" Width="95%" runat="server" ForeColor="#403F3F"></asp:Label></td>
                    <td colspan="5">
                        <asp:FileUpload CssClass="myButton" Width="97%" ID="FlpAttach2" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: right">
                        <asp:Button ID="btnSave" runat="server" CssClass="myButton" OnClick="btnSave_Click" />
                    </td>

                </tr>

                <tr>
                    <td colspan="6" style="text-align: left; width: 100%">
                        <asp:GridView ID="GrdLines" runat="server" Width="100%" HorizontalAlign="Center"
                            AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
                            <Columns>
                                <asp:BoundField DataField="MutabakatSirketi" HeaderText="MUTABAKAT ŞİRKETİ" />
                                <asp:BoundField DataField="MutabakatNo" HeaderText="MUTABAKAT NO" />
                                <asp:BoundField DataField="MutabakatTuru" HeaderStyle-Width="150px" HeaderText="MUTABAKAT TÜRÜ">
                                    <HeaderStyle Width="150px"></HeaderStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="MusteriNo" HeaderText="MÜŞTERİ NO" />
                                <asp:BoundField DataField="ParaBirimi" HeaderText="PARA BİRİMİ" />
                                <asp:BoundField DataField="MutabakatTarihi" DataFormatString="{0:dd.MM.yyyy}" HeaderText="MUTABAKAT TARİHİ" />
                                <asp:BoundField DataField="GecerlilikTarihi" DataFormatString="{0:dd.MM.yyyy}" HeaderText="GEÇERLİLİK TARİHİ" />
                                <asp:BoundField DataField="MutabikKalinanTutar" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:n2}" HeaderText="MUTABIK KALINAN TUTAR" />
                                <asp:BoundField DataField="MutabikKalindi" HeaderText="MUTABIK KALINDI" />
                                <asp:BoundField DataField="MutabakatKul" HeaderText="MUTABAKATI GERÇ. KULLANICI" />
                                <asp:BoundField DataField="Yorum" HeaderStyle-Width="250px" HeaderText="YORUM">
                                    <HeaderStyle Width="250px"></HeaderStyle>
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Dosya1">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="AttachHyperLink" runat="server" Text='<%# Eval("FileName0") %>' NavigateUrl='<%# String.Format("~/Download.aspx?id={0}&FileName={1}&GId={2}", Eval("MutabakatNo"),Eval("FileName0"),HdGId.Value) %>' Target="_self" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Dosya2">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="AttachHyperLink" runat="server" Text='<%# Eval("FileName1") %>' NavigateUrl='<%# String.Format("~/Download.aspx?id={0}&FileName={1}&GId={2}", Eval("MutabakatNo"),Eval("FileName1"),HdGId.Value) %>' Target="_self" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Dosya3">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="AttachHyperLink" runat="server" Text='<%# Eval("FileName2") %>' NavigateUrl='<%# String.Format("~/Download.aspx?id={0}&FileName={1}&GId={2}", Eval("MutabakatNo"),Eval("FileName2"),HdGId.Value) %>' Target="_self" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#403F3F" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
