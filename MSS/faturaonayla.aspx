﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="faturaonayla.aspx.cs" Inherits="MSS1.faturaonayla" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/style.css" type="text/css" rel="stylesheet" />
    <link href="css/menu.css" type="text/css" rel="stylesheet" />
    <link href="css/tools.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 400px;
        }
        
        .myButton
        {
            -moz-box-shadow: inset 0px 0px 0px 0px #f0f7fa;
            -webkit-box-shadow: inset 0px 0px 0px 0px #f0f7fa;
            box-shadow: inset 0px 0px 0px 0px #f0f7fa;
            background-color: transparent;
            border: 1px solid #057fd0;
            display: inline-block;
            color: #ffffff;
            font-family: 'Times New Roman';
            font-size: 16px;
            font-weight: bold;
            padding: 6px 12px;
            text-decoration: none;
            color: #003e83;
        }
        
        *
        {
            padding: 0;
            font-family: Arial, Helvetica, Trebuchet MS;
            margin-right: 0;
            margin-top: 0px;
            font-weight: 700;
            color: gray;
        }
    </style>
    <script type="text/javascript">
        function proceed() {
            opener.location.reload(true);
            opener.location = 'satis_sip.aspx';
            self.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table class="style1">
            <tr>
                <td>
                    &nbsp;<asp:Label ID="lblBelgeTarihiB" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtbelgetarihi" CssClass="textbox" runat="server" Width="100px"
                        Style="text-align: justify" ValidationGroup="MKE" AutoPostBack="True" OnTextChanged="txtbelgetarihi_TextChanged" />
                    <cc1:CalendarExtender ID="txtbelgetarihi_CalendarExtender" runat="server" TargetControlID="txtbelgetarihi"
                        Format="dd.MM.yyyy" />
                    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptLocalization="true"
                        EnableScriptGlobalization="true" EnablePageMethods="true">
                    </asp:ScriptManager>
                </td>
                
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblDovizKuru" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtkur" runat="server" Width="50px">0.00</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:Button ID="btnonayla" runat="server" CssClass="myButton" OnClick="btnonayla_Click" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
