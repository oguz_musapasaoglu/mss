﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.Master" EnableViewState="true" CodeBehind="newpage_documentEntrySelector.aspx.cs" Inherits="MSS1.newpage_documentEntrySelector" %>

<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Src="~/Controls/DataTableControlGAC.ascx" TagName="StDataTable" TagPrefix="STDT" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .TD {
            padding: 0px;
            color: black;
            font-weight: 400;
            font-style: normal;
            text-decoration: none;
            font-family: Calibri, sans-serif;
            text-align: center;
            vertical-align: bottom;
            border: none;
        }
    </style>
    <script type="text/javascript">
        function GoToDocumentEntry(DocType) {
            var _val = cmbEmployee.GetValue();
            window.location.href ='newpage_documentEntry.aspx?DocType=' + DocType + '&DocNo=' + _val;

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:SqlDataSource ID="DSPersonel" runat="server" SelectCommand="select No_ Code,[Tam Ad] Adi from [0C_05200_00_EMPLOYEE] where COMPANY=@Sirket Order By [Tam Ad]"
        ConnectionString="<%$ ConnectionStrings:DBConnect %>">
         <SelectParameters>
            <asp:SessionParameter Name="Sirket" SessionField="Sirket" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <h2>
        <asp:Label ID="lblsayfaadi" Text="SEÇİLEN BELGEYE DOKÜMAN EKLE" CssClass="SayfaBaslik" runat="server"></asp:Label></h2>
    <table style="width: 450px">
        <tr>
            <td class="captionStyle">
                <asp:Label ID="lblPersonel" runat="server" Text="PERSONEL SEÇ" CssClass="captionStyle"></asp:Label>
            </td>
            <td style="width: 10px"></td>
            <td colspan="2">
                <dx:ASPxComboBox ID="cmbEmployee" DataSourceID="DSPersonel" TextField="Adi" ValueField="Code" Theme="Glass" ClientInstanceName="cmbEmployee" runat="server" EnableCallbackMode="true" CallbackPageSize="50">
                </dx:ASPxComboBox>

            </td>
            <td style="width: 5px">&nbsp;</td>
            <td style="text-align: left">
                <input type="button" runat="server" id="BtnGoTo" value="DOSYA BAĞLA" style="cursor:pointer" onclick="GoToDocumentEntry(6);" />
                <%--<asp:Button runat="server" CssClass="formglobalbuttonStyle" Text="DOSYA BAĞLA" ID="BtnGoTo" OnClientClick="return GoToDocumentEntry(7);" />--%></td>

        </tr>
    </table>
</asp:Content>


