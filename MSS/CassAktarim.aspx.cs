﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.IO;
using System.Data.OleDb;
using System.Data;
using System.Data.Odbc;
using DevExpress.Spreadsheet;
using DevExpress.Web.Office;
using MSS1.WSGeneric;
using System.ServiceModel;
using DevExpress.Spreadsheet.Export;
using System.Data.SqlClient;
using System.Globalization;
using MSS1.Codes;

namespace MSS1
{
    public partial class CassAktarim : Bases
    {
        Language l = new Language();
        public ws_baglantı ws = new ws_baglantı();
        fonksiyonlar f = new fonksiyonlar();
        Methods method = new Methods();
        DataTable dt, dt2;
        SessionBase _obj;
        string FilePath
        {
            get { return Session["ExcelFilePath"] == null ? String.Empty : Session["ExcelFilePath"].ToString(); }
            set { Session["ExcelFilePath"] = value; }
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            _obj = TabSessions.ControlState(this.Page, Request.QueryString["GId"].ToString());
            if (IsPostBack || IsCallback) return;
            FilePath = String.Empty;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
           

        }

        protected void Upload_FileUploadComplete(object sender, DevExpress.Web.FileUploadCompleteEventArgs e)
        {

            if (System.IO.File.Exists(Server.MapPath(CurrentFolderName(_obj.Comp) + "/Dosya/Cass Aktarim/") + e.UploadedFile.FileName))
                System.IO.File.Delete(Server.MapPath(CurrentFolderName(_obj.Comp) + "/Dosya/Cass Aktarim/") + e.UploadedFile.FileName);


            String uploadedFilePath = Server.MapPath(CurrentFolderName(_obj.Comp) + "/Dosya/Cass Aktarim/") + e.UploadedFile.FileName;
            e.UploadedFile.SaveAs(uploadedFilePath);
            if (!String.IsNullOrEmpty(uploadedFilePath))
                DocumentManager.CloseDocument(uploadedFilePath);
            FilePath = uploadedFilePath;

        }

        protected void Spreadsheet_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(FilePath))
            {
                Spreadsheet.Open(FilePath);
            }
        }

        protected void btnAktar_Click(object sender, EventArgs e)
        {
            try
            {
                Worksheet Ws = Spreadsheet.Document.Worksheets["Billing"];
                Range range = Ws.GetDataRange();
                dt = Ws.CreateDataTable(range, true);
                DataTableExporter exp = Ws.CreateDataTableExporter(range, dt, true);
                exp.Export();

                Worksheet Ws2 = Spreadsheet.Document.Worksheets["Invoice"];
                Range range2 = Ws2.GetDataRange();
                dt2 = Ws2.CreateDataTable(range2, true);
                DataTableExporter exp2 = Ws2.CreateDataTableExporter(range2, dt2, true);
                exp2.Export();
            }
            catch (ArgumentOutOfRangeException ex)
            {

                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "cassaktarim", "msg001"));
                return;
            }

            bool _durum = true;
            if (!chFaturaTuru.Checked)
            {
                string query = "SELECT AWB,INVNOLINE FROM [0C_A0025_02_CASS_INV_HEADER]  where COMPANY='" + _obj.Comp.ToString() + "'";
                DataTable dtKontrol = f.datatablegonder(query);


                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    if (dtKontrol.Select("AWB = '" + dt2.Rows[i][0].ToString() + "-" + dt2.Rows[i][6].ToString() + "' and INVNOLINE = '" + dt2.Rows[i][2].ToString() + "'").Count() > 0)
                    {
                        _durum = false;
                        break;
                    }
                }
            }
            if (_durum)
            {

                string CassAktarimNo = GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "GetCassAktarimNo" , _obj.Comp.ToString());
                GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "CassTempDeleteAll", _obj.Comp.ToString());
                 

                InvoiceHeaderImport(dt, CassAktarimNo);
                InvoiceLineImport(dt2);

                Import(CassAktarimNo);

                Spreadsheet.Document.Worksheets.ActiveWorksheet.Clear(Spreadsheet.Document.Worksheets.ActiveWorksheet.GetDataRange());
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "cassaktarim", "msg002"));
            }
            else
            {
                MessageBox(l.dilgetir(_obj.SelectedLanguage, _obj.Comp.ToString(), _obj.UserName.ToString(), "cassaktarim", "msg003"));
            }



        }

        private void InvoiceHeaderImport(DataTable dt, string AktarimNo)
        { 
            DataTable lastDT = null;

            if (chFaturaTuru.Checked)
                lastDT = dt.Select("[Billing Period]='02'").CopyToDataTable();
            else
                lastDT = dt;


            foreach (DataRow dr in lastDT.Rows)
            {
                string sorgu = "insert into LAMLOJ.dbo.[" + _obj.Comp.ToString() + "$Cass Invoice Header Temp] " +
                    "([AirlinePrefix],[AirlineBranchCode],[Invoice Number],[Name],[Prepaid Weight Charge],[Prepaid Due Airline]," +
                    "[Charges Collect Weight Charge],[Charges Collect Due Agent],[Commission],[Sales Incentive],[Tax Amount],[Payable]," +
                    "[Billing Period],[Aktarim No],[Start Date],[End Date],Duzenleme) values ('" +
                    dr[0].ToString() + "','" +
                    dr[1].ToString() + "','" +
                    dr[11].ToString() + "','" +
                    dr[2].ToString() + "'," +
                    method.StringToDecimal(dr[3].ToString()) + "," +
                    method.StringToDecimal(dr[4].ToString()) + "," +
                    method.StringToDecimal(dr[5].ToString()) + "," +
                    method.StringToDecimal(dr[6].ToString()) + "," +
                    method.StringToDecimal(dr[7].ToString()) + "," +
                    method.StringToDecimal(dr[8].ToString()) + "," +
                    method.StringToDecimal(dr[9].ToString()) + "," +
                    method.StringToDecimal(dr[10].ToString()) + "," +
                    method.StringToDecimal(dr[12].ToString()) + ",'" +
                    AktarimNo + "','" +
                    txtBaslamaTarihi.Text + "','" +
                    txtBitisTarihi.Text + "'," +
                    Convert.ToInt32(chFaturaTuru.Checked)
                    + ")";
                f.updateyap(sorgu);
            }
        }

        private void InvoiceLineImport(DataTable dt)
        {
            foreach (DataRow dr in dt.Rows)
            {
                string sorgu = "insert into LAMLOJ.dbo.[" + _obj.Comp.ToString() + "$Cass Invoice Line Temp] " +
                    "(AirlinePrefix , " +
                    "AirlineBranchCode ," +
                    "[Invoice Number] ," +
                    "Currency ," +
                    "[AWB Serial Number]," +
                    "[Start Date]," +
                    "[End Date]," +
                    "[Airport Origin]," +
                    "[Airport Destination], " +
                    "Weight ," +
                    "[Prepaid Weight Charge]," +
                    "[Prepaid Due Airline], " +
                    "[Charge - Weight], " +
                    "[Charge - Due Airline], " +
                    "Commission ," +
                    "[Sales Incentive]," +
                    "[Net Amount Before TAX]," +
                    "[Tax Amount]," +
                    "[Net Amount Payable], " +
                    "[AWB Execution Date], " +
                    "[Agent Information], " +
                    "SpIn )" +
                    " values " + "('" +
                    dr[0].ToString() + "','" +
                    dr[1].ToString() + "','" +
                    dr[2].ToString() + "','" +
                    dr[3].ToString() + "','" +
                    dr[6].ToString() + "','" +
                    txtBaslamaTarihi.Text + "','" +
                    txtBitisTarihi.Text + "','" +
                    dr[7].ToString() + "','" +
                    dr[8].ToString() + "'," +
                    method.StringToDecimal(dr[9].ToString()) + "," +
                    method.StringToDecimal(dr[10].ToString()) + "," +
                    method.StringToDecimal(dr[11].ToString()) + "," +
                    method.StringToDecimal(dr[12].ToString()) + "," +
                    method.StringToDecimal(dr[13].ToString()) + "," +
                    method.StringToDecimal(dr[14].ToString()) + "," +
                    method.StringToDecimal(dr[15].ToString()) + "," +
                    method.StringToDecimal(dr[16].ToString()) + "," +
                    method.StringToDecimal(dr[17].ToString()) + "," +
                    method.StringToDecimal(dr[18].ToString()) + ",'" +
                    dr[19].ToString() + "','" +
                    dr[20].ToString() + "','" +
                    dr[21].ToString()
                    + "')";

                f.updateyap(sorgu);
            }

        }

        private void DataTableEdit(DataTable dt)
        {

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i][0] = dt.Rows[i][0] == DBNull.Value ? "" : dt.Rows[i][0];
                dt.Rows[i][1] = dt.Rows[i][1] == DBNull.Value ? "" : dt.Rows[i][1];
                dt.Rows[i][2] = dt.Rows[i][2] == DBNull.Value ? "" : dt.Rows[i][2];
                dt.Rows[i][3] = dt.Rows[i][3] == DBNull.Value ? "" : dt.Rows[i][3];
                //dt.Rows[i][4] = dt.Rows[i][4] == DBNull.Value ? "2018-01-01" : dt.Rows[i][4];
                //dt.Rows[i][5] = dt.Rows[i][5] == DBNull.Value ? "2018-01-01" : dt.Rows[i][5];
                dt.Rows[i][6] = dt.Rows[i][6] == DBNull.Value ? "" : dt.Rows[i][6];
                dt.Rows[i][7] = dt.Rows[i][7] == DBNull.Value ? "" : dt.Rows[i][7];
                dt.Rows[i][8] = dt.Rows[i][8] == DBNull.Value ? "" : dt.Rows[i][8];
                dt.Rows[i][9] = dt.Rows[i][9] == DBNull.Value ? 0 : dt.Rows[i][9];
                dt.Rows[i][10] = dt.Rows[i][10] == DBNull.Value ? 0 : dt.Rows[i][10];
                dt.Rows[i][11] = dt.Rows[i][11] == DBNull.Value ? 0 : dt.Rows[i][11];
                dt.Rows[i][12] = dt.Rows[i][12] == DBNull.Value ? 0 : dt.Rows[i][12];
                dt.Rows[i][13] = dt.Rows[i][13] == DBNull.Value ? 0 : dt.Rows[i][13];
                dt.Rows[i][14] = dt.Rows[i][14] == DBNull.Value ? 0 : dt.Rows[i][14];
                dt.Rows[i][15] = dt.Rows[i][15] == DBNull.Value ? 0 : dt.Rows[i][15];
                dt.Rows[i][16] = dt.Rows[i][16] == DBNull.Value ? 0 : dt.Rows[i][16];
                dt.Rows[i][17] = dt.Rows[i][17] == DBNull.Value ? 0 : dt.Rows[i][17];
                dt.Rows[i][18] = dt.Rows[i][18] == DBNull.Value ? 0 : dt.Rows[i][18];
                dt.Rows[i][19] = dt.Rows[i][19] == DBNull.Value ? "" : dt.Rows[i][19];
                dt.Rows[i][20] = dt.Rows[i][20] == DBNull.Value ? "" : dt.Rows[i][20];
                dt.Rows[i][21] = dt.Rows[i][21] == DBNull.Value ? "" : dt.Rows[i][21];
                dt.Rows[i][22] = dt.Rows[i][22] == DBNull.Value ? 0 : dt.Rows[i][22];
            }


            dt.Columns[0].DataType = typeof(string);
            dt.Columns[1].DataType = typeof(string);
            dt.Columns[2].DataType = typeof(string);
            dt.Columns[3].DataType = typeof(string);
            //dt.Columns[4].DataType = typeof(string);
            //dt.Columns[5].DataType = typeof(string);
            dt.Columns[6].DataType = typeof(string);
            dt.Columns[7].DataType = typeof(string);
            dt.Columns[8].DataType = typeof(string);
            //dt.Columns[9].DataType = typeof(int);
            //dt.Columns[10].DataType = typeof(decimal);
            //dt.Columns[11].DataType = typeof(decimal);
            //dt.Columns[12].DataType = typeof(decimal);
            //dt.Columns[13].DataType = typeof(decimal);
            //dt.Columns[14].DataType = typeof(decimal);
            //dt.Columns[15].DataType = typeof(decimal);
            //dt.Columns[16].DataType = typeof(decimal);
            //dt.Columns[17].DataType = typeof(decimal);
            //dt.Columns[18].DataType = typeof(decimal);
            dt.Columns[19].DataType = typeof(string);
            dt.Columns[20].DataType = typeof(string);
            dt.Columns[21].DataType = typeof(string);
            dt.Columns[22].DataType = typeof(int);
        }

        private void Import(string CassAktarimNo)
        {

            string[] _params = { CassAktarimNo, "0",
                        _obj.UserName.ToString(), method.DateToStringNAVType(txtBaslamaTarihi.Date.ToShortDateString()),
                method.DateToStringNAVType(txtBitisTarihi.Date.ToShortDateString()),
                ASPxUploadControl1.UploadedFiles[0].FileName.ToString() };

            GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "CassInvoice§" + string.Join("§", _params), _obj.Comp.ToString());
          
        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }
    }
}