﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LogisticInvoiceSelector.aspx.cs" Inherits="MSS1.LogisticInvoiceSelector" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .myButton {
            -moz-box-shadow: inset 0px 0px 0px 0px #f0f7fa;
            -webkit-box-shadow: inset 0px 0px 0px 0px #f0f7fa;
            box-shadow: inset 0px 0px 0px 0px #f0f7fa;
            background-color: transparent;
            border: 1px solid #057fd0;
            display: inline-block;
            color: #ffffff;
            font-family: 'Arial Unicode MS';
            font-size: x-small;
            font-weight: bold;
            padding: 6px 12px;
            text-decoration: none;
            color: #003e83;
        }

        .textbox {
            /*background-color: #ffffff;*/
            border: 2px solid #a7bcd4;
            background-color: transparent;
            -ms-filter: progid:DXImageTransform.Microsoft.Alpha(Opacity = 70);
            filter: alpha(opacity = 70);
            text-indent: 3px;
            font-weight: bold;
            font-size: 13px;
            text-align: justify;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
        }

        td {
            font-family: 'Arial Unicode MS';
            font-size: x-small;
            font-weight: bold;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 800px" border="0">
                <tr>
                    <td style="width: 100px">MBL NO</td>
                    <td style="width: 175px">
                        <asp:TextBox runat="server" ID="txtMBLNo" CssClass="textbox"></asp:TextBox>
                    </td>
                    <td style="width: 100px">HBL NO</td>
                    <td style="width: 175px">
                        <asp:TextBox runat="server" ID="txtHBLNo" CssClass="textbox"></asp:TextBox>
                    </td>

                </tr>
                <tr>
                    <td style="width: 100px">CONTAINER NO</td>
                    <td style="width: 175px">
                        <asp:TextBox runat="server" ID="txtContainerNo" CssClass="textbox"></asp:TextBox>
                    </td>
                    <td style="width: 100px">
                        <asp:Label ID="lblDovCinsB" runat="server"></asp:Label></td>
                    <td style="width: 175px">
                        <asp:DropDownList ID="ddldoviz" CssClass="textbox" Width="100px" runat="server">
                        </asp:DropDownList>
                    </td>

                </tr>
                <tr>
                    <td style="width: 100px">JOB TRACK NO</td>
                    <td style="width: 175px">
                        <asp:TextBox runat="server" ID="txtJobNo" CssClass="textbox"></asp:TextBox>
                    </td>
                    <td style="width: 100px">Cass</td>
                    <td style="width: 175px"><asp:CheckBox ID="chCass" runat="server" AutoPostBack="false"/></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: right">
                        <asp:Button ID="btnList" runat="server" CssClass="myButton" OnClick="btnList_Click" />
                    </td>
                    <td colspan="2" style="text-align: right">
                        <asp:Button ID="btnCreate" runat="server" CssClass="myButton" OnClick="btnCreate_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="text-align: left">
                        <asp:Button ID="btnSelectAll" runat="server" CssClass="myButton" OnClick="btnSelectAll_Click" />
                        &nbsp;&nbsp;<asp:Button ID="btnUnSelectAll" runat="server" CssClass="myButton" OnClick="btnUnSelectAll_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="text-align: left">
                        <asp:GridView ID="GrdLines" runat="server" Width="100%" HorizontalAlign="Center"
                            AutoGenerateColumns="false" AllowPaging="false" ShowFooter="true">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chksec" runat="server" />
                                        <asp:Literal ID="ltrJobContractEntryNo" Visible="false" runat="server" Text='<%# Eval("Job Contract Entry No_") %>'></asp:Literal>
                                        <asp:Literal ID="ltrGacJobNo" Visible="false" runat="server" Text='<%# Eval("GACJOBNO") %>'></asp:Literal>
                                        <asp:Literal ID="ltrJobNo" Visible="false" runat="server" Text='<%# Eval("Job No_") %>'></asp:Literal>
                                        <asp:Literal ID="ltrVendorNo" Visible="false" runat="server" Text='<%# Eval("Vendor No") %>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="CARINO" HeaderText="CARİ NO" />
                                <asp:BoundField DataField="MBLNO" HeaderText="MBL NO" /> 
                                <asp:BoundField DataField="HBLNO" HeaderText="HBL NO" />
                                <asp:BoundField DataField="Container No" HeaderText="CONTAINER NO" />
                                <asp:BoundField DataField="Job No_" HeaderText="JOB NO" />
                                <asp:BoundField DataField="Description" HeaderText="KAYNAK" />
                                <asp:BoundField DataField="Currency Code" HeaderText="PARA BİRİMİ" />
                                <asp:BoundField DataField="ToplamTutar" DataFormatString="{0:n2}" HeaderText="TOPLAM TUTAR" />
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>


        </div>

    </form>
</body>
</html>
