﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="iletisimgirisiV2.aspx.cs" Inherits="MSS1.iletisimgirisiV2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/GAC_MenuStyle.css?v=33" rel="stylesheet" />
    <link href="css/GAC_NewStyle.css?v=34" rel="stylesheet" />
    <link href="css/style.css?v=V3" rel="stylesheet" type="text/css" />
    <link href="css/tools.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/default.css?v=33" />
    <link rel="stylesheet" type="text/css" href="css/component.css" />
    <script type="text/javascript" src="js/modernizr.custom.js"></script>
    <script type="text/javascript" src="js/classie.js"></script>
    <script type="text/javascript" src="../../js/jquery.js"></script>
    
    <script type="text/javascript" src="../js/popuputilities.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <div>

            <h2>
                <asp:Label ID="lblsayfaadi" runat="server" Text="İLETİŞİM GİR" CssClass="SayfaBaslik"> </asp:Label>
            </h2>
            <table border="0" style="width: 620px" cellspacing="2">
                <tr>
                    <td class="captionStyle" style="width: 100px">
                        <asp:Label ID="lblFirmaB" runat="server" Text="FİRMA *" CssClass="captionlabelStyle"></asp:Label></td>
                    <td style="width: 220px">
                        <asp:TextBox ID="txtCompany" runat="server" AutoPostBack="True" Width="100%" CssClass="textboxStyle"
                            OnTextChanged="txtCompany_TextChanged"></asp:TextBox>
                        <cc1:AutoCompleteExtender ID="txtCompany_AutoCompleteExtender" runat="server" CompletionInterval="100"
                            CompletionListCssClass="listMain" CompletionListHighlightedItemCssClass="itemsSelected"
                            CompletionListItemCssClass="itemmain" CompletionSetCount="30" EnableCaching="false"
                            FirstRowSelected="false" MinimumPrefixLength="1"
                            ServiceMethod="SearchContact" TargetControlID="txtCompany" UseContextKey="true">
                        </cc1:AutoCompleteExtender>
                    </td>
                    <td style="width: 100px">
                        <asp:TextBox ID="txtiletisimtarihi" runat="server" CssClass="textboxStyle" Width="100%" Font-Size="12px"></asp:TextBox>
                        <cc1:CalendarExtender ID="txtiletisimtarihi_CalendarExtender" runat="server" Format="dd.MM.yyyy"
                            TargetControlID="txtiletisimtarihi" />
                    </td>
                    <td style="width: 100px">
                        <asp:TextBox ID="txtBasSaati" runat="server" CssClass="textboxStyle" Width="100%" Font-Size="12px"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="txtBasSaati_MaskedEditExtender" runat="server"
                            AcceptAMPM="False" Filtered=":" Mask="99:99" MaskType="Time"
                            TargetControlID="txtBasSaati">
                        </cc1:MaskedEditExtender>
                    </td>
                    <td style="width: 100px">
                        <asp:TextBox ID="txtBitSaati" runat="server" CssClass="textboxStyle" Width="100%" Font-Size="12px"></asp:TextBox>
                        <cc1:MaskedEditExtender ID="txtBitSaati_MaskedEditExtender" runat="server"
                            AcceptAMPM="False" Filtered=":" Mask="99:99" MaskType="Time"
                            TargetControlID="txtBitSaati">
                        </cc1:MaskedEditExtender>
                    </td>
                </tr>
                <tr>
                    <td class="captionStyle" style="width: 100px">
                        <asp:Label ID="lblIlgiliKisiB" runat="server" Text="İLGİLİ KİŞİ *" CssClass="captionlabelStyle"></asp:Label></td>
                    <td style="width: 220px">
                        <asp:DropDownList ID="ddlilgilikisi" runat="server" AppendDataBoundItems="True"
                            CssClass="dropdownStyle" Width="100%">
                            <asp:ListItem Selected="True"> </asp:ListItem>
                        </asp:DropDownList></td>
                    <td style="width: 100px">
                        <asp:DropDownList ID="ddliletisimturu" runat="server" AppendDataBoundItems="True"
                            CssClass="dropdown-menu dropdownStyle" Width="100%">
                            <asp:ListItem Selected="True"> </asp:ListItem>
                        </asp:DropDownList></td>
                    <td style="width: 100px">
                        <asp:DropDownList ID="ddliletisimnedeni" runat="server" AppendDataBoundItems="True"
                            CssClass="dropdownStyle" Width="100%">
                            <asp:ListItem Selected="True"> </asp:ListItem>
                        </asp:DropDownList></td>
                    <td style="width: 100px">
                        <asp:DropDownList ID="ddliletisimsonucu" runat="server" AppendDataBoundItems="True"
                            CssClass="dropdownStyle" Width="100%">
                            <asp:ListItem Selected="True"> </asp:ListItem>
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td class="captionStyle" style="width: 100px">
                        <asp:Label ID="lblMusTemB" runat="server" Text="MÜŞTERİ TEMS. *" CssClass="captionlabelStyle"></asp:Label>
                    </td>
                    <td style="width: 220px">
                        <asp:DropDownList ID="ddlSatısTemsilcisi" runat="server" AppendDataBoundItems="True"
                            CssClass="dropdownStyle" Width="100%">
                            <asp:ListItem Selected="True"> </asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 100px"></td>
                    <td colspan="2" style="width: 200px"></td>

                </tr>
                <tr>
                    <td style="width: 100px"></td>
                    <td style="width: 220px"></td>
                    <td class="captionStyle" style="width: 100px">
                        <asp:Label ID="lblKatilimciB" runat="server" Text="KATILIMCILAR *" CssClass="captionlabelStyle"></asp:Label>
                    </td>
                    <td colspan="2" style="width: 200px">
                        <table border="0" style="width: 100%" cellspacing="1">
                            <tr>
                                <td style="width: 180px; vertical-align: top">
                                    <asp:DropDownList ID="ddlkatilimci" runat="server" AppendDataBoundItems="True" CssClass="dropdownStyle" Width="100%">
                                        <asp:ListItem Selected="True"> </asp:ListItem>
                                    </asp:DropDownList></td>
                                <td style="width: 20px; vertical-align: top">
                                    <asp:Button ID="btnkatilimciekle" runat="server" CssClass="eklebutonStyle" Text="+"
                                        OnClick="btnkatilimciekle_Click" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px"></td>
                    <td style="width: 220px"></td>
                    <td style="width: 100px"></td>
                    <td colspan="2" style="width: 200px">
                        <asp:ListBox ID="lbkatilimci" runat="server" CssClass="listboxStyle" Width="100%"></asp:ListBox></td>
                </tr>
                <tr>
                    <td class="captionStyle" style="width: 100px; vertical-align: top">
                        <asp:Label ID="lblIletisimDetB" runat="server" Text="İLETİŞİM DETAYI *" CssClass="captionlabelStyle"></asp:Label></td>
                    <td colspan="4" style="width: 420px">
                        <asp:TextBox ID="txtDetay" runat="server" Width="100%" CssClass="textboxStyle" TextMode="MultiLine" Height="82px"
                            onKeyDown="textCounter()" onKeyUp="textCounter()"></asp:TextBox></td>

                </tr>
            </table>


            <table>
                <tr>
                    <td>
                        <asp:Button runat="server" CssClass="formglobalbuttonStyle" Text="İLETİŞİM KAYDET" ID="btnOlustur" OnClick="btnOlustur_Click" />
                    </td>
                    <td>
                        <asp:Button runat="server" CssClass="formglobalbuttonStyle" Text="DOSYA BAĞLA" ID="btnDosyaBagla" OnClick="btnDosyaBagla_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btndosyagoryeni" runat="server" CssClass="formglobalbuttonStyle" OnClick="btndosyagoryeni_Click" />

                    </td>

                </tr>
            </table>

            <div style="display: none">
                <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptLocalization="true"
                    EnableScriptGlobalization="true" EnablePageMethods="true">
                </asp:ScriptManager>

                <asp:TextBox ID="lblKaydetMessage" runat="server"></asp:TextBox>
                <asp:Label ID="lblcontactno" runat="server"></asp:Label>
            </div>





            <script type="text/javascript" language="javascript">



                function textCounter() {
                    var field = document.getElementById('<%=txtDetay.ClientID%>').value;
                    if (field.length > 500) {
                        document.getElementById('<%=txtDetay.ClientID%>').value = field.substring(0, 500);
                    }
                }


                function resetPosition(object, args) {

                    var tb = object._element; // tb.id is the associated textbox ID in the grid.

                    // Get the position with scrolling.
                    var tbposition = [tb.offsetLeft + tb.offsetParent.offsetLeft, tb.offsetTop + tb.offsetParent.offsetTop];

                    var xposition = tbposition[0] + 10;

                    var yposition = tbposition[1] + 335;

                    var ex = object._completionListElement;
                    if (ex)

                        $common.setLocation(ex, new Sys.UI.Point(xposition, yposition));
                } // End resetPosition
                function resetPosition2(object, args) {

                    var tb = object._element; // tb.id is the associated textbox ID in the grid.

                    // Get the position with scrolling.
                    var tbposition = [tb.offsetLeft + tb.offsetParent.offsetLeft, tb.offsetTop + tb.offsetParent.offsetTop];

                    var xposition = tbposition[0] + 10;

                    var yposition = tbposition[1] + 690;

                    var ex = object._completionListElement;
                    if (ex)

                        $common.setLocation(ex, new Sys.UI.Point(xposition, yposition));
                } // End resetPosition
                function resetPosition3(object, args) {

                    var tb = object._element; // tb.id is the associated textbox ID in the grid.

                    // Get the position with scrolling.
                    var tbposition = [tb.offsetLeft + tb.offsetParent.offsetLeft, tb.offsetTop + tb.offsetParent.offsetTop];

                    var xposition = tbposition[0] + 10;

                    var yposition = tbposition[1] + 700;

                    var ex = object._completionListElement;
                    if (ex)

                        $common.setLocation(ex, new Sys.UI.Point(xposition, yposition));
                } // End resetPosition
                function resetPositionItem(object, args) {

                    var tb = object._element; // tb.id is the associated textbox ID in the grid.

                    // Get the position with scrolling.
                    var tbposition = [tb.offsetLeft + tb.offsetParent.offsetLeft, tb.offsetTop + tb.offsetParent.offsetTop];

                    var xposition = tbposition[0] + 10;

                    var yposition = tbposition[1] + 650;

                    var ex = object._completionListElement;
                    if (ex)

                        $common.setLocation(ex, new Sys.UI.Point(xposition, yposition));
                } // End resetPosition
                function resetPositionserino(object, args) {

                    var tb = object._element; // tb.id is the associated textbox ID in the grid.

                    // Get the position with scrolling.
                    var tbposition = [tb.offsetLeft + tb.offsetParent.offsetLeft, tb.offsetTop + tb.offsetParent.offsetTop];

                    var xposition = tbposition[0] + 10;

                    var yposition = tbposition[1] + 650;

                    var ex = object._completionListElement;
                    if (ex)

                        $common.setLocation(ex, new Sys.UI.Point(xposition, yposition));
                } // End resetPosition

                function checkTextAreaMaxLength(textBox, e, length) {

                    var mLen = textBox["MaxLength"];
                    if (null == mLen)
                        mLen = length;

                    var maxLength = parseInt(mLen);
                    if (!checkSpecialKeys(e)) {
                        if (textBox.value.length > maxLength - 1) {
                            if (window.event)//IE
                            {
                                e.returnValue = false;
                                return false;
                            }
                            else//Firefox
                                e.preventDefault();
                        }
                    }
                }
            </script>


            <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
            <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
        </div>
    </form>
</body>
</html>
