﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MSS1
{
    public partial class GeneratePanel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                editor1.Value = "<table style=\"border: 0px solid  navy \"><tr><td rowspan=\"6\" width=\"5\"><img src=\"null\" ></img></td><td class=\"CT08NG\" width=\"75\">T2.RPNL01</td><td class=\"CT10BN\" width=\"330\">R0013</td><td class=\"CT08NG\" width=\"75\">T2.RPNL06</td><td class=\"CT10BN\" width=\"150\">R0047</td><td class=\"CT08NG\" width=\"80\">T2.RPNL13</td><td class=\"CT10BN\" width=\"150\">R0018</td><td class=\"CT08NR\" width=\"70\">T2.RPNL17</td><td class=\"CT11BR\" width=\"50\">R0024</td></tr><tr><td class=\"CT08NG\" width=\"75\">T2.RPNL03</td><td class=\"CT10BN\" width=\"330\">R0016</td><td class=\"CT08NG\" width=\"75\">T2.RPNL08</td><td class=\"CT10BN\" width=\"150\">R0035</td><td class=\"CT08NG\" width=\"80\">T2.RPNL14</td><td class=\"CT10BN\" width=\"150\">R0020</td></tr><tr><td class=\"CT08NG\" width=\"75\">''</td><td class=\"CT10BN\" width=\"330\">R0017</td><td class=\"CT08NG\" width=\"75\">T2.RPNL09</td><td class=\"CT10BN\" width=\"150\">R0048</td><td class=\"CT08NG\" width=\"80\">T2.RPNL15</td><td class=\"CT10BN\" width=\"150\">R0044</td><td class=\"CT08NN\" rowspan=\"3\" width=\"50\">'STATUS INFO...'</td></tr><tr><td class=\"CT08NG\" width=\"75\">T2.RPNL04</td><td class=\"CT10BN\" width=\"330\">R0034</td><td class=\"CT08NG\" width=\"75\">T2.RPNL10</td><td class=\"CT10BN\" width=\"150\">R0049</td><td class=\"CT08NG\" width=\"80\">T2.RPNL12</td><td class=\"CT10BN\" width=\"150\">T1.R0043</td></tr><tr><td class=\"CT08NG\" width=\"75\">T2.RPNL05</td><td class=\"CT10BN\" width=\"330\">R0023</td><td class=\"CT08NG\" width=\"75\">T2.RPNL11</td><td class=\"CT10BN\" width=\"150\">R0022</td><td class=\"CT08NG\" width=\"80\">T2.RPNL16</td><td class=\"CT10BN\" width=\"150\">R0025</td></tr></table>";
            }
           
        }
        protected void show_text_Click(object sender, EventArgs e)
        {
            string desc = editor1.Value.ToString();
            lblNewsLong.Text = Server.HtmlDecode(desc);
        }

        protected void generate_panel_Click(object sender, EventArgs e)
        {
            txtResultPanel.Visible = true;
            txtResultPanel.Text = Regex.Replace(editor1.Value, @">[ ]*([^< ]+[^<][^< ]+)[ ]*<", @">'+$1+'<");
        }
    }
}