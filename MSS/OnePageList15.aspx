﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="OnePageList15.aspx.cs" EnableViewState="true" Inherits="MSS1.OnePageList15" %>

<%@ Register Src="~/controls/DataTableControlGAC.ascx" TagName="StDataTable" TagPrefix="STDT" %>

<%@ Register Assembly="DevExpress.Web.v19.1" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        iframe {
            -moz-transform: scale(0.99, 0.99);
            -webkit-transform: scale(0.99, 0.99);
            -o-transform: scale(0.99, 0.99);
            -ms-transform: scale(0.99, 0.99);
            transform: scale(0.99, 0.99);
            -moz-transform-origin: top left;
            -webkit-transform-origin: top left;
            -o-transform-origin: top left;
            -ms-transform-origin: top left;
            transform-origin: top left;
            border: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        function OnGridFocusedRowChanged(s, e, linkType) {
            var link = $("#" + "<%= txtlink1.ClientID %>").val();
            var linkfield = "<%= txtlinkField1.ClientID %>";
            var transferf = "<%= txtCatchParameters1.ClientID %>";
            var CatchParamsVals = "<%= txtCatchParamsAndValues.ClientID %>";
            switch (linkType) {
                case "1":
                    link = $("#" + "<%= txtlink1.ClientID %>").val();
                    linkfield = "<%= txtlinkField1.ClientID %>";
                    break;
                case "2":
                    link = $("#" + "<%= txtlink2.ClientID %>").val();
                    linkfield = "<%= txtlinkField2.ClientID %>";
                    break;
                case "3":
                    link = $("#" + "<%= txtlink3.ClientID %>").val();
                    linkfield = "<%= txtlinkField3.ClientID %>";
                    break;
                case "4":
                    link = $("#" + "<%= txtlink4.ClientID %>").val();
                    linkfield = "<%= txtlinkField4.ClientID %>";
                    break;
                case "5":
                    link = $("#" + "<%= txtlink5.ClientID %>").val();
                    linkfield = "<%= txtlinkField5.ClientID %>";
                    break;
                case "6":
                    link = $("#" + "<%= txtlink6.ClientID %>").val();
                    linkfield = "<%= txtlinkField6.ClientID %>";
                    break;
                case "7":
                    link = $("#" + "<%= txtlink7.ClientID %>").val();
                    linkfield = "<%= txtlinkField7.ClientID %>";
                    break;
                case "8":
                    link = $("#" + "<%= txtlink8.ClientID %>").val();
                    linkfield = "<%= txtlinkField8.ClientID %>";
                    break;
                case "9":
                    link = $("#" + "<%= txtlink9.ClientID %>").val();
                    linkfield = "<%= txtlinkField9.ClientID %>";
                    break;

            }


            if (link == "JOB") {
                s.GetRowValues(s.GetFocusedRowIndex(), $("#" + linkfield).val(), OnGetRowValuesJob);
            }
            else if (link == "FILTER2") {
                $("#" + "<%= txtCurLinkFields.ClientID %>").val($("#" + linkfield).val());
                s.GetRowValues(s.GetFocusedRowIndex(), $("#" + linkfield).val(), OnGetRowValuesFilterDetail);

            }
            else if (link == "FILTER") {
                $("#" + CatchParamsVals).val('');
                var tfields = $("#" + linkfield).val();
                if ($("#" + transferf).val() != '') tfields += ';' + $("#" + transferf).val();
                s.GetRowValues(s.GetFocusedRowIndex(), tfields, OnGetRowValuesFilter);


            }
            else {
                $("#" + "<%= txtlinkType.ClientID %>").val(linkType);
                s.GetRowValues(s.GetFocusedRowIndex(), $("#" + linkfield).val(), OnGetRowValues);
            }

        }

        function SetGridEditValue(values) {
            if (values != "NULL") {
                grid2.SetEditValue("R0041", values);
                grid2.SetEditValue("R0026", grid2.GetEditValue('R0023') * grid2.GetEditValue('R0041'));
                grid2.SetEditValue("R0028", grid2.GetEditValue('R0023') * grid2.GetEditValue('R0041') * grid2.GetEditValue('R0027') / 100);
                grid2.SetEditValue("R0029", 1 * grid2.GetEditValue('R0028') + 1 * grid2.GetEditValue('R0026'));
            }
            else {
                grid2.SetEditValue("R0041", "");
            }



        }

        function OpenPdfFilter(values) {
            document.getElementById('ctl00_ContentPlaceHolder1_HiddenRpath').value = values;
            document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_framePDF').src = values;
        }

        function StatuFilter(values) {
            var splitter = values.toString().split(",");
            document.getElementById('ctl00_ContentPlaceHolder1_HiddenMainStatu').value = splitter[0];

        }

        function openpdfpopup() {
            var link = document.getElementById('ctl00_ContentPlaceHolder1_HiddenRpath').value;
            var popwin = window.open(link, '_blank');
            popwin.focus();
        }

        function OnGetRowValues(values) {
            var linkType = $("#" + "<%= txtlinkType.ClientID %>").val();
            var linkFormat = "<%= txtlink1.ClientID %>";

            if (linkType == "1")
                linkFormat = "<%= txtlink1.ClientID %>";
            else if (linkType == "2")
                linkFormat = "<%= txtlink2.ClientID %>";
            else if (linkType == "3")
                linkFormat = "<%= txtlink3.ClientID %>";
            else if (linkType == "4")
                linkFormat = "<%= txtlink4.ClientID %>";
            else if (linkType == "5")
                linkFormat = "<%= txtlink5.ClientID %>";
            else if (linkType == "6")
                linkFormat = "<%= txtlink6.ClientID %>";
            else if (linkType == "7")
                linkFormat = "<%= txtlink7.ClientID %>";
            else if (linkType == "8")
                linkFormat = "<%= txtlink8.ClientID %>";
            else if (linkType == "9")
                linkFormat = "<%= txtlink9.ClientID %>";
            var link = $("#" + linkFormat).val();
            link = link.replace("XXXXX", values);
            if (link != '') {
                window.location.href = AdGIdToLink(link);
                window.focus();
            }

        }

        function AdGIdToLink(_val) {
            if (_val.includes("GId=")) return _val;
            var ObjGId = "<%= lblGId.ClientID %>";
            if (_val.includes("?"))
                _val = _val + "&GId=" + $("#" + ObjGId).text();
            else
                _val = _val + "?GId=" + $("#" + ObjGId).text();

            return _val;

        }

        function OnGetRowValuesPopup(values) {
            alert(values);
           <%-- var linkFormat = "<%= txtlink.ClientID %>";
             var link = $("#" + linkFormat).val();
             link = link.replace("XXXXX", values[0]);
             link = link.replace("YYYYY", values[1]);
             window.location.href = link;
             window.focus();--%>
        }

        function OpenDirectLink(link) {
            var P1 = document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value;
            var P2 = document.getElementById('ctl00_ContentPlaceHolder1_HiddenP2').value;
            link = link.replace("XXXXX", P1);
            link = link.replace("YYYYY", P2);
            var popwin = window.open(AdGIdToLink(link), '_blank');
            popwin.focus();
        }

        var checkval = '';
        var checkRefVal = '';

        function checkParamVal(params) {
            return params.startsWith(checkval);
        }

        function checkRefObjs(params) {
            return params.startsWith(checkRefVal);
        }

        function ReturnSayacVal(sayac) {
            if (sayac == 1)
                return "XXXXX";
            else if (sayac == 2)
                return "YYYYY";
            else if (sayac == 3)
                return "ZZZZZ";
            else if (sayac == 4)
                return "TTTTT";
        }

        function OpenDirectLinkWithTransferParams(link, filterobjs) {
            var array1 = filterobjs.split(',');
            var sayac = 1;
            var sayval = '';
            var _val = '';
            var P1 = document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value;
            var P2 = document.getElementById('ctl00_ContentPlaceHolder1_HiddenP2').value;
            var CatchParamsVals = "<%= txtCatchParamsAndValues.ClientID %>";
            var cpvals = $("#" + CatchParamsVals).val();
            var cpvalarray = cpvals.split('|');
            //alert(cpvals);
            array1.forEach(function (element) {
                _val = '';
                sayval = ReturnSayacVal(sayac);
                if (element == 'P1')
                    link = link.replace(sayval, P1);
                else if (element == 'P2')
                    link = link.replace(sayval, P2);
                else {
                    //alert(element);
                    checkval = element;
                    _val = cpvalarray.find(checkParamVal);
                    //alert(_val);
                    link = link.replace(sayval, _val.split(';')[1]);
                }

                sayac = sayac + 1;
            });

            var popwin = window.open(AdGIdToLink(link), '_blank');
            popwin.focus();
        }

        function OnGetRowValuesFilterDetail(values) {


            var splitter = values.toString().split(",");

            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP3').value = '';
            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP4').value = '';
            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP5').value = '';

            if (splitter.length == 1) {
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP3').value = splitter[0];

            }
            else if (splitter.length == 2) {
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP3').value = splitter[0];
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP4').value = splitter[1];
            }
            else if (values.length == 3) {
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP3').value = values[0];
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP4').value = values[1];
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP5').value = values[2];
            }



            try {
                if ($("#" + "<%= txtMasterObject2.ClientID %>").val() != '') {
                    if (rpCargoDetails.GetCollapsed()) {
                        rpCargoDetails.SetCollapsed(false);
                    } else {
                        grid2.PerformCallback('');
                    }
                }
            } catch (e) { }
            try {
                if ($("#" + "<%= txtMasterObject3.ClientID %>").val() != '') {
                    if (rpCostSales.GetCollapsed()) {
                        rpCostSales.SetCollapsed(false);
                    } else {
                        grid3.PerformCallback('');
                    }
                }
            } catch (e) { }
            try {
                if ($("#" + "<%= txtMasterObject4.ClientID %>").val() != '') {
                    if (rpDocuments.GetCollapsed()) {
                        rpDocuments.SetCollapsed(false);
                    } else {
                        grid4.PerformCallback('');
                    }
                }
            } catch (e) { }
            try {
                if ($("#" + "<%= txtMasterObject5.ClientID %>").val() != '') {
                    if (rpComments.GetCollapsed()) {
                        rpComments.SetCollapsed(false);
                    } else {
                        grid5.PerformCallback('');
                    }
                }
            } catch (e) { }
            try {
                if ($("#" + "<%= txtMasterObject6.ClientID %>").val() != '') {
                    if (rpTasks.GetCollapsed()) {
                        rpTasks.SetCollapsed(false);
                    } else {
                        grid6.PerformCallback('');
                    }
                }
            } catch (e) { }

            try {
                if ($("#" + "<%= txtMasterObject7.ClientID %>").val() != '') {
                    if (rp7.GetCollapsed()) {
                        rp7.SetCollapsed(false);
                    } else {
                        grid7.PerformCallback('');
                    }
                }
            } catch (e) { }

            try {
                if ($("#" + "<%= txtMasterObject8.ClientID %>").val() != '') {
                    if (rp8.GetCollapsed()) {
                        rp8.SetCollapsed(false);
                    } else {
                        grid8.PerformCallback('');
                    }
                }
            } catch (e) { }

            try {
                if ($("#" + "<%= txtMasterObject9.ClientID %>").val() != '') {
                    if (rp9.GetCollapsed()) {
                        rp9.SetCollapsed(false);
                    } else {
                        grid9.PerformCallback('');
                    }
                }
            } catch (e) { }

        }

        function OnGetRowValuesFilter(values) {
            var linkFormat = "<%= txtlinkField1.ClientID %>";
            var link = $("#" + linkFormat).val();
            var CatchParamsVals = "<%= txtCatchParamsAndValues.ClientID %>";
            var transferf = "<%= txtCatchParameters1.ClientID %>";
            var objf1 = "<%= txtOBJF1.ClientID %>";

            var cargoCollapseStatu, costCollapseStatu, taskCollapseStatu, commentCollapseStatu, documentCollapseStatu, rp7statu, rp8statu, rp9statu;
            if ($("#" + "<%= txtCollapseStatu2.ClientID %>").val().toString() == "1")
                cargoCollapseStatu = false;
            else
                cargoCollapseStatu = true;

            if ($("#" + "<%= txtCollapseStatu3.ClientID %>").val().toString() == "1")
                costCollapseStatu = false;
            else
                costCollapseStatu = true;

            if ($("#" + "<%= txtCollapseStatu4.ClientID %>").val().toString() == "1")
                documentCollapseStatu = false;
            else
                documentCollapseStatu = true;

            if ($("#" + "<%= txtCollapseStatu5.ClientID %>").val().toString() == "1")
                commentCollapseStatu = false;
            else
                commentCollapseStatu = true;

            if ($("#" + "<%= txtCollapseStatu6.ClientID %>").val().toString() == "1")
                taskCollapseStatu = false;
            else
                taskCollapseStatu = true;

            if ($("#" + "<%= txtCollapseStatu7.ClientID %>").val().toString() == "1")
                rp7statu = false;
            else
                rp7statu = true;

            if ($("#" + "<%= txtCollapseStatu8.ClientID %>").val().toString() == "1")
                rp8statu = false;
            else
                rp8statu = true;

            if ($("#" + "<%= txtCollapseStatu9.ClientID %>").val().toString() == "1")
                rp9statu = false;
            else
                rp9statu = true;



            var splitter = values.toString().split(",");

            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value = '';
            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP2').value = '';
            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP3').value = '';
            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP4').value = '';
            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP5').value = '';

            var CpVals;
            var linksize = link.split(";");
            var catchfields = $("#" + transferf).val().split(";");
            var ilk = true;
            var cfc = 0;
            for (var i = linksize.length; i < splitter.length; i++) {
                if (ilk) {
                    CpVals = catchfields[cfc] + ';' + splitter[i];
                    ilk = false;
                }
                else
                    CpVals += '|' + catchfields[cfc] + ';' + splitter[i];

                cfc++;
            }

            $("#" + CatchParamsVals).val(CpVals)
            '<%Session["CpVals"] = "' + CpVals + '"; %>';
            //alert($("#" + CatchParamsVals).val());

            if (linksize.length == 1) {
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value = splitter[0];

            }
            else if (linksize.length == 2) {
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value = splitter[0];
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP2').value = splitter[1];
            }
            else if (linksize.length == 3) {
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP3').value = values[0];
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP4').value = values[1];
                document.getElementById('ctl00_ContentPlaceHolder1_HiddenP5').value = values[2];
            }

            var _O2 = false, _O3 = false, _O4 = false, _O5 = false, _O6 = false,
                _O7 = false, _O8 = false, _O9 = false;

            try { if (!rpCargoDetails.GetCollapsed()) _O2 = true; rpCargoDetails.SetCollapsed(true); } catch (e) { }
            try { if (!rpCostSales.GetCollapsed()) _O3 = true; rpCostSales.SetCollapsed(true); } catch (e) { }
            try { if (!rpTasks.GetCollapsed()) _O4 = true; rpTasks.SetCollapsed(true); } catch (e) { }
            try { if (!rpComments.GetCollapsed()) _O5 = true; rpComments.SetCollapsed(true); } catch (e) { }
            try { if (!rpDocuments.GetCollapsed()) _O6 = true; rpDocuments.SetCollapsed(true); } catch (e) { }
            try { if (!rp7.GetCollapsed()) _O7 = true; rp7.SetCollapsed(true); } catch (e) { }
            try { if (!rp8.GetCollapsed()) _O8 = true; rp8.SetCollapsed(true); } catch (e) { }
            try { if (!rp9.GetCollapsed()) _O9 = true; rp9.SetCollapsed(true); } catch (e) { }

            try { rpCargoDetails.SetCollapsed(cargoCollapseStatu); } catch (e) { }
            try { rpCostSales.SetCollapsed(costCollapseStatu); } catch (e) { }
            try { rpTasks.SetCollapsed(taskCollapseStatu); } catch (e) { }
            try { rpComments.SetCollapsed(commentCollapseStatu); } catch (e) { }
            try { rpDocuments.SetCollapsed(documentCollapseStatu); } catch (e) { }
            try { rp7.SetCollapsed(rp7statu); } catch (e) { }
            try { rp8.SetCollapsed(rp8statu); } catch (e) { }
            try { rp9.SetCollapsed(rp9statu); } catch (e) { }

            try { if (_O2 & cargoCollapseStatu) rpCargoDetails.SetCollapsed(false); } catch (e) { }
            try { if (_O3 & costCollapseStatu) rpCostSales.SetCollapsed(false); } catch (e) { }
            try { if (_O4 & taskCollapseStatu) rpTasks.SetCollapsed(false); } catch (e) { }
            try { if (_O5 & commentCollapseStatu) rpComments.SetCollapsed(false); } catch (e) { }
            try { if (_O6 & documentCollapseStatu) rpDocuments.SetCollapsed(false); } catch (e) { }
            try { if (_O7 & _cs7) rp7.SetCollapsed(false); } catch (e) { }
            try { if (_O8 & _cs8) rp8.SetCollapsed(false); } catch (e) { }
            try { if (_O9 & _cs9) rp9.SetCollapsed(false); } catch (e) { }


<%--            try { if($("#" + "<%= txtMasterObject2.ClientID %>").val()=='') grid2.PerformCallback(''); } catch (e) { }
            try { if($("#" + "<%= txtMasterObject3.ClientID %>").val()=='') grid3.PerformCallback(''); } catch (e) { }
            try { if($("#" + "<%= txtMasterObject4.ClientID %>").val()=='') grid4.PerformCallback(''); } catch (e) { }
            try { if($("#" + "<%= txtMasterObject5.ClientID %>").val()=='') grid5.PerformCallback(''); } catch (e) { }
            try { if($("#" + "<%= txtMasterObject6.ClientID %>").val()=='') grid6.PerformCallback(''); } catch (e) { }--%>


            try {
                var objf1no = $("#" + objf1).val();
                if (objf1no == "J50010" || objf1no == "J50030") {
                    var link1 = document.getElementById('ctl00_ContentPlaceHolder1_HiddenpageType').value.replace("FaturaOnGiris.aspx", "FaturaCard/Satinalma.aspx");
                    //alert(CpVals);
                    if (CpVals.includes("R0037;1") || CpVals.includes("R0037;4"))
                        document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rpMain_iframeView').src = AdGIdToLink(document.getElementById('ctl00_ContentPlaceHolder1_HiddenpageType').value + splitter[0]);
                    else
                        document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rpMain_iframeView').src = AdGIdToLink(link1 + splitter[0]);


                }
                else
                    document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rpMain_iframeView').src = AdGIdToLink(document.getElementById('ctl00_ContentPlaceHolder1_HiddenpageType').value + splitter[0]);

                var pane = xsplitter.GetPaneByName('MiddleContainer');
                if (pane.IsCollapsed())
                    document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_1_S_CB_Img').click();
                //LeftContainer
            } catch (e) {

            }




            //OnListBoxIndexChanged(values);

        }

        function DoHeaders1(values) {
            try {

                var _HVal = $("#" + "<%= txtDHT1.ClientID %>").val();
                rpMain.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }

        function DoHeaders2(values) {
            try {
                var _HVal = $("#" + "<%= txtDHT2.ClientID %>").val();
                rpCargoDetails.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }

        function DoHeaders3(values) {
            try {
                var _HVal = $("#" + "<%= txtDHT3.ClientID %>").val();
                rpCostSales.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }

        function DoHeaders4(values) {
            try {
                var _HVal = $("#" + "<%= txtDHT4.ClientID %>").val();
                rpDocuments.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }

        function DoHeaders5(values) {
            try {
                var _HVal = $("#" + "<%= txtDHT5.ClientID %>").val();
                rpComments.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }

        function DoHeaders6(values) {
            try {
                var _HVal = $("#" + "<%= txtDHT6.ClientID %>").val();
                rpTasks.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }
        function DoHeaders7(values) {
            try {
                var _HVal = $("#" + "<%= txtDHT7.ClientID %>").val();
                rp7.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }

        function DoHeaders8(values) {
            try {
                var _HVal = $("#" + "<%= txtDHT8.ClientID %>").val();
                rp8.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }

        function DoHeaders9(values) {
            try {
                var _HVal = $("#" + "<%= txtDHT9.ClientID %>").val();
                rp9.SetHeaderText(_HVal + ' ' + values);

            } catch (e) {

            }
        }



        function OnGetRowValuesJob(values) {
            //alert(values);
            var splitter = values.split(';');
            var targetLink = "";
            if (splitter[1] == "1") {
                window.open("/Jobs/mainJobs.aspx?MJID=" + splitter[0]);
            }
            else {

                window.open("/projeolustur.aspx?Tur=1&ProjeNo=" + splitter[0]);
            }
        }


        var postponedCallbackRequired = false;
        function OnListBoxIndexChanged(values) {
            if (iframecallbackPanel.InCallback())
                postponedCallbackRequired = true;
            else
                iframecallbackPanel.PerformCallback(values);
        }
        function OnEndCallback(values) {

            if (postponedCallbackRequired) {
                iframecallbackPanel.PerformCallback(values);
                postponedCallbackRequired = false;
            }
        }


        function OnRefreshAllObjects(s, e, i) {
            if (s.cpIsUpdated) {


                try {

                    var _val2 = s.cpQval;
                    if (_val2.length > 0) {
                        var insertarray = _val2.split("|");
                        if (insertarray.length == 1)
                            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value = insertarray[0];
                        else {
                            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value = insertarray[0];
                            document.getElementById('ctl00_ContentPlaceHolder1_HiddenP2').value = insertarray[1];
                        }
                    }

                } catch (e) {

                }

                try {

                    var _val3 = s.cpMsg;
                    if (_val3.length > 0)
                        alert(_val3.replace("'", " "));

                } catch (e) {

                }


                var ferobjs = $("#ctl00_ContentPlaceHolder1_txtRef" + i.toString()).val();
                var cpvalarray = ferobjs.split(';');
                var _val = '';
                for (var j = 0; j < 10; j++) {

                    checkRefVal = j.toString();
                    _val = cpvalarray.find(checkRefObjs);
                    if (_val != null) {
                        if (_val.length > 0) {
                            if (j == 1)
                                try { grid1.PerformCallback(''); } catch (err) { }
                            else if (j == 2)
                                try { grid2.PerformCallback(''); } catch (err) { }
                            else if (j == 3)
                                try { grid3.PerformCallback(''); } catch (err) { }
                            else if (j == 4)
                                try { grid4.PerformCallback(''); } catch (err) { }
                            else if (j == 5)
                                try { grid5.PerformCallback(''); } catch (err) { }
                            else if (j == 6)
                                try { grid6.PerformCallback(''); } catch (err) { }
                            else if (j == 7)
                                try { grid7.PerformCallback(''); } catch (err) { }
                            else if (j == 8)
                                try { grid8.PerformCallback(''); } catch (err) { }
                            else if (j == 9)
                                try { grid9.PerformCallback(''); } catch (err) { }
                            else if (j == 0)
                                try { document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rpMain_iframeView').src = AdGIdToLink(document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rpMain_iframeView').src); } catch (err) { }

                        }
                    }


                }

                if (i == 1)
                    try { grid1.PerformCallback(''); } catch (err) { }
                else if (i == 2)
                    try { grid2.PerformCallback(''); } catch (err) { }
                else if (i == 3)
                    try { grid3.PerformCallback(''); } catch (err) { }
                else if (i == 4)
                    try { grid4.PerformCallback(''); } catch (err) { }
                else if (i == 5)
                    try { grid5.PerformCallback(''); } catch (err) { }
                else if (i == 6)
                    try { grid6.PerformCallback(''); } catch (err) { }
                else if (i == 7)
                    try { grid7.PerformCallback(''); } catch (err) { }
                else if (i == 8)
                    try { grid8.PerformCallback(''); } catch (err) { }
                else if (i == 9)
                    try { grid9.PerformCallback(''); } catch (err) { }



            }
            if (s.cpIsLinked) {
                OpenSelectDirect(s.cpLink, s.cpQueryStrings, s.cpIslemId);
            }
        }

        function OpenSelectDirect(link, filterobjs, IslemId) {
            var array1 = filterobjs.split(',');
            var sayac = 1;
            var sayval = '';
            var _val = '';
            var P1 = document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value;
            var P2 = document.getElementById('ctl00_ContentPlaceHolder1_HiddenP2').value;
            var CatchParamsVals = "<%= txtCatchParamsAndValues.ClientID %>";
            var cpvals = $("#" + CatchParamsVals).val();
            var cpvalarray = cpvals.split('|');
            //alert(cpvals);
            array1.forEach(function (element) {
                _val = '';
                sayval = ReturnSayacVal(sayac);
                if (element == 'P1')
                    link = link.replace(sayval, P1);
                else if (element == 'P2')
                    link = link.replace(sayval, P2);
                else {
                    //alert(element);
                    checkval = element;
                    _val = cpvalarray.find(checkParamVal);
                    //alert(_val);
                    link = link.replace(sayval, _val.split(';')[1]);
                }

                sayac = sayac + 1;
            });

            link = link + "&IslemId=" + IslemId;

            var popwin = window.open(AdGIdToLink(link), '_blank');
            popwin.focus();
        }

        function RefreshAllCollapse() {
            try { grid1.PerformCallback(''); } catch (err) { }
            try { grid2.PerformCallback(''); } catch (err) { }
            try { grid3.PerformCallback(''); } catch (err) { }
            try { grid4.PerformCallback(''); } catch (err) { }
            try { grid5.PerformCallback(''); } catch (err) { }
            try { grid6.PerformCallback(''); } catch (err) { }
            try { grid7.PerformCallback(''); } catch (err) { }
            try { grid8.PerformCallback(''); } catch (err) { }
            try { grid9.PerformCallback(''); } catch (err) { }
            document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rpMain_iframeView').src = AdGIdToLink(document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rpMain_iframeView').src);
        }

        function RefreshFrame(_value) {
            try {
                document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rpMain_iframeView').src = AdGIdToLink(document.getElementById('ctl00_ContentPlaceHolder1_HiddenpageType').value + _value);
            } catch (e) {

            }
        }

        function OpenFileUpload(fieldId, gridId) {
            var link = 'dosyabagla.aspx?BelgeNo='
                + document.getElementById('ctl00_ContentPlaceHolder1_HiddenP1').value + '&tur=34&dtur=34&ProcType=INSERTFILE&ObjectId='
                + gridId + '&FId=' + fieldId;
            var filewin = window.open(AdGIdToLink(link), 'List', 'scrollbars = no, resizable = no, width = 650, height = 460, top = 200, left = 400');
            filewin.focus();

        }


    </script>

    <dx:ASPxSplitter Theme="iOS" ID="ASPxSplitter1" ClientInstanceName="xsplitter" runat="server" Height="100%" Width="100%">
        <Panes>
            <dx:SplitterPane ShowCollapseBackwardButton="True" ShowSeparatorImage="True" Size="25%" Name="LeftContainer">
                <Panes>
                    <dx:SplitterPane ShowCollapseBackwardButton="True" ShowSeparatorImage="True" Size="80%" Name="Leftinside">
                        <Panes>
                            <dx:SplitterPane Size="90%" ScrollBars="Auto">
                                <ContentCollection>
                                    <dx:SplitterContentControl ID="SPContent" runat="server">
                                        <table>
                                            <tr>
                                                <td style="width: 100%; height: 100%">
                                                    <dx:ASPxRadioButtonList RepeatDirection="Horizontal" Theme="Glass" runat="server" Width="100%" ID="rblDynamicList"
                                                        OnSelectedIndexChanged="rblDynamicList_SelectedIndexChanged" AutoPostBack="true">
                                                    </dx:ASPxRadioButtonList>
                                                </td>
                                            </tr>
                                        </table>
                                        <dx:ASPxMenu ID="menu1" runat="server" AutoSeparators="RootOnly"
                                            Theme="Glass"
                                            ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                            ShowSubMenuShadow="False" OnItemClick="menu1_ItemClick">
                                            <SubMenuStyle GutterWidth="0px" />
                                            <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                            <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                            </SubMenuItemStyle>
                                            <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                        </dx:ASPxMenu>
                                        <dx:ASPxGridView ID="grid1" ClientInstanceName="grid1" Caption="" runat="server" AutoGenerateColumns="false"
                                            Theme="Glass" EnableTheming="false" DataSourceID="dtgrid1" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                            OnCellEditorInitialize="grid1_CellEditorInitialize" EnableRowsCache="false" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                            Settings-HorizontalScrollBarMode="Auto" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback"
                                            OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnRowDeleting="grid_RowDeleting" OnCustomCallback="grid1_CustomCallback">
                                            <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,1)}" RowDblClick="function(s, e) {  OnGridFocusedRowChanged(s,e,'1');} " />
                                            <Settings />
                                            <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" AllowFocusedRow="true" />
                                            <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                            <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                                            <SettingsCommandButton>
                                                <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                </CancelButton>
                                                <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                </UpdateButton>
                                            </SettingsCommandButton>
                                            <SettingsText EmptyDataRow=" " />
                                            <Styles>
                                                <Cell Paddings-Padding="1"></Cell>
                                                <Header ForeColor="White"></Header>
                                                <HeaderPanel ForeColor="White"></HeaderPanel>
                                                <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                            </Styles>
                                            <SettingsText EmptyDataRow=" " />

                                        </dx:ASPxGridView>

                                        <dx:ASPxGridViewExporter ID="gridExport1" ExportedRowType="All" runat="server" GridViewID="grid1"></dx:ASPxGridViewExporter>
                                    </dx:SplitterContentControl>
                                </ContentCollection>
                            </dx:SplitterPane>
                        </Panes>
                    </dx:SplitterPane>
                </Panes>
            </dx:SplitterPane>
            <dx:SplitterPane ShowCollapseForwardButton="True" ShowSeparatorImage="True" Size="65%" Name="MiddleContainer" ScrollBars="Auto">
                <Panes>
                    <dx:SplitterPane ShowCollapseBackwardButton="True" ShowSeparatorImage="True" Size="80%" Name="Middleinside">
                        <ContentCollection>
                            <dx:SplitterContentControl>
                                <dx:ASPxRoundPanel ID="rpMain" Theme="Glass" Collapsed="false" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rpMain" HeaderText="QUOTATION"
                                    runat="server" Width="100%" Height="80%">
                                    <PanelCollection>
                                        <dx:PanelContent>
                                            <%--<dx:ASPxCallbackPanel ID="iframecallbackPanel" ClientInstanceName="iframecallbackPanel" Height="100%"
                                                runat="server" OnCallback="iframecallbackPanel_Callback">
                                                <PanelCollection>
                                                    <dx:PanelContent>--%>
                                            <iframe id="iframeView" width="100%" height="100%" runat="server"></iframe>
                                            <%-- </dx:PanelContent>
                                                </PanelCollection>
                                            </dx:ASPxCallbackPanel>--%>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){  
                                            if(rpMain.GetCollapsed()){                                                  
                                                document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rpMain_iframeView').style.display = 'none';
                                            }
                                            else {
                                               document.getElementById('ctl00_ContentPlaceHolder1_ASPxSplitter1_rpMain_iframeView').style.display = 'block';
                                          }
                                        }" />
                                </dx:ASPxRoundPanel>

                                <dx:ASPxRoundPanel ID="rpTasks" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rpTasks" HeaderText="TASKS" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent SupportsDisabledAttribute="True" runat="server">
                                            <%-- <div style="width: 1275px; overflow-y: hidden; overflow-x: hidden;">--%>
                                            <dx:ASPxMenu ID="menu6" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu6_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid6" ClientInstanceName="grid6" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid6_CustomCallback" OnCellEditorInitialize="grid6_CellEditorInitialize" Width="100%"
                                                EnableTheming="false" DataSourceID="dtgrid6" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,6)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'6');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" AllowFocusedRow="true" />
                                                <SettingsPager PageSize="7" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>

                                                </SettingsCommandButton>
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <Styles>
                                                    <Cell Paddings-Padding="1"></Cell>

                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                                <Columns>
                                                    <dx:GridViewDataColumn Width="100px" Caption="Temp1R0030" Visible="true" Name="R0030" FieldName="R0030" VisibleIndex="1">
                                                        <EditItemTemplate>
                                                            <dx:ASPxComboBox ID="cmbR0030" Value='<%# Eval("R0030")%>' DropDownStyle="DropDownList"
                                                                Theme="Glass" Width="100%" runat="server" EnableCallbackMode="true" CallbackPageSize="20">
                                                            </dx:ASPxComboBox>
                                                        </EditItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Width="100px" Caption="Temp1R0032" Visible="true" Name="R0032" FieldName="R0032" VisibleIndex="2">
                                                        <EditItemTemplate>
                                                            <dx:ASPxComboBox ID="cmbR0032" Value='<%# Eval("R0032")%>' DropDownStyle="DropDownList"
                                                                Theme="Glass" Width="100%" runat="server" EnableCallbackMode="true" CallbackPageSize="20">
                                                            </dx:ASPxComboBox>
                                                        </EditItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Width="100px" Caption="Temp1R0034" Visible="true" Name="R0034" FieldName="R0034" VisibleIndex="3">
                                                        <EditItemTemplate>
                                                            <dx:ASPxComboBox ID="cmbR0034" Value='<%# Eval("R0034")%>' DropDownStyle="DropDownList"
                                                                Theme="Glass" Width="100%" runat="server" EnableCallbackMode="true" CallbackPageSize="20">
                                                            </dx:ASPxComboBox>
                                                        </EditItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Width="100px" Caption="Temp1R0036" Visible="true" Name="R0036" FieldName="R0036" VisibleIndex="4">
                                                        <EditItemTemplate>
                                                            <dx:ASPxComboBox ID="cmbR0036" Value='<%# Eval("R0036")%>' DropDownStyle="DropDownList"
                                                                Theme="Glass" Width="100%" runat="server" EnableCallbackMode="true" CallbackPageSize="20">
                                                            </dx:ASPxComboBox>
                                                        </EditItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Width="100px" Caption="Temp1R0028" Visible="true" Name="R0028" FieldName="R0028" VisibleIndex="5">
                                                        <EditItemTemplate>
                                                            <dx:ASPxComboBox ID="cmbR0028" Value='<%# Eval("R0028")%>' DropDownStyle="DropDownList"
                                                                Theme="Glass" Width="100%" runat="server" EnableCallbackMode="true" CallbackPageSize="20">
                                                            </dx:ASPxComboBox>
                                                        </EditItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                </Columns>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport6" ExportedRowType="All" runat="server" GridViewID="grid6"></dx:ASPxGridViewExporter>
                                            <%--  </div>--%>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){  
                                            txttask.SetText(rpTasks.GetCollapsed());
                                            if(!rpTasks.GetCollapsed()){                                                  
                                                 grid6.PerformCallback(document.getElementById('ctl00_ContentPlaceHolder1_HiddenLoadqueryText').value);
                                            } 
                                        }" />
                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rpCargoDetails" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rpCargoDetails" HeaderText="CARGO DETAILS" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent>
                                            <%--<div style="width: 1275px; overflow-y: hidden; overflow-x: hidden;">--%>
                                            <dx:ASPxMenu ID="menu2" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu2_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid2" ClientInstanceName="grid2" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid2_CustomCallback" OnCellEditorInitialize="grid2_CellEditorInitialize"
                                                EnableTheming="false" DataSourceID="dtgrid2" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnRowDeleting="grid_RowDeleting"
                                                OnAfterPerformCallback="grid_AfterPerformCallback" SettingsText-PopupEditFormCaption=" ">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,2)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'2');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" AllowFocusedRow="true" />
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport2" ExportedRowType="All" runat="server" GridViewID="grid2"></dx:ASPxGridViewExporter>
                                            <%--   </div>--%>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){ 
                                            HiddenCargoCollepseStatu.SetText(rpCargoDetails.GetCollapsed());
                                            if(!rpCargoDetails.GetCollapsed()){
                                                grid2.PerformCallback(document.getElementById('ctl00_ContentPlaceHolder1_HiddenLoadqueryText').value);
                                            }
                                        }" />
                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rpCostSales" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rpCostSales" HeaderText="COST AND SALES" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent>
                                            <%--<div style="width: 1275px; overflow-y: hidden; overflow-x: hidden;">--%>
                                            <dx:ASPxMenu ID="menu3" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu3_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid3" ClientInstanceName="grid3" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid3_CustomCallback" OnCellEditorInitialize="grid3_CellEditorInitialize"
                                                EnableTheming="false" DataSourceID="dtgrid3" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,3)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'3');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" AllowFocusedRow="true" />
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <SettingsPager PageSize="30" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport3" ExportedRowType="All" runat="server" GridViewID="grid3"></dx:ASPxGridViewExporter>
                                            <%--  </div>--%>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){ 
                                            HiddenCostCollepseStatu.SetText(rpCostSales.GetCollapsed());
                                            if(!rpCostSales.GetCollapsed()){
                                                grid3.PerformCallback(document.getElementById('ctl00_ContentPlaceHolder1_HiddenLoadqueryText').value);
                                            }
                                        }" />

                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rpComments" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true"
                                    ClientInstanceName="rpComments" HeaderText="COMMENTS" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent>
                                            <%--<div style="width: 1275px; overflow-y: hidden; overflow-x: hidden;">--%>
                                            <dx:ASPxMenu ID="menu5" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu5_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid5" ClientInstanceName="grid5" Caption="" runat="server" AutoGenerateColumns="False" EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid5_CustomCallback" OnCellEditorInitialize="grid5_CellEditorInitialize"
                                                EnableTheming="false" DataSourceID="dtgrid5" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,5)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'5');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" AllowFocusedRow="true" />
                                                <SettingsPager PageSize="7" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport5" ExportedRowType="All" runat="server" GridViewID="grid5"></dx:ASPxGridViewExporter>
                                            <%-- </div>--%>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){ 
                                            HiddenCommentCollepseStatu.SetText(rpComments.GetCollapsed());
                                            if(!rpComments.GetCollapsed()){
                                                grid5.PerformCallback(document.getElementById('ctl00_ContentPlaceHolder1_HiddenLoadqueryText').value);
                                            }
                                        }" />
                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rpDocuments" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true" ClientInstanceName="rpDocuments" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent>
                                            <%--  <div style="width: 1275px; overflow-y: hidden; overflow-x: hidden;">--%>
                                            <dx:ASPxMenu ID="menu4" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu4_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid4" ClientInstanceName="grid4" Caption="" runat="server" AutoGenerateColumns="False"
                                                EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid4_CustomCallback"
                                                EnableTheming="false" DataSourceID="dtgrid4" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,4)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'4');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" AllowFocusedRow="true" />
                                                <SettingsPager PageSize="7" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport4" ExportedRowType="All" runat="server" GridViewID="grid4"></dx:ASPxGridViewExporter>
                                            <%--  </div>--%>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){ 
                                            HiddenDocumentCollepseStatu.SetText(rpDocuments.GetCollapsed());
                                            if(!rpDocuments.GetCollapsed()){
                                                grid4.PerformCallback(document.getElementById('ctl00_ContentPlaceHolder1_HiddenLoadqueryText').value);
                                            }
                                        }" />
                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rp7" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true" ClientInstanceName="rp7" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent>
                                            <%--  <div style="width: 1275px; overflow-y: hidden; overflow-x: hidden;">--%>
                                            <dx:ASPxMenu ID="menu7" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu7_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid7" ClientInstanceName="grid7" Caption="" runat="server" AutoGenerateColumns="False"
                                                EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid7_CustomCallback"
                                                EnableTheming="false" DataSourceID="dtgrid7" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize" OnCellEditorInitialize="grid7_CellEditorInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,7)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'7');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" AllowFocusedRow="true" />
                                                <SettingsPager PageSize="7" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport7" ExportedRowType="All" runat="server" GridViewID="grid7"></dx:ASPxGridViewExporter>
                                            <%--  </div>--%>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){ 
                                            HiddenDocumentCollepseStatu.SetText(rp7.GetCollapsed());
                                            if(!rp7.GetCollapsed()){
                                                grid7.PerformCallback(document.getElementById('ctl00_ContentPlaceHolder1_HiddenLoadqueryText').value);
                                            }
                                        }" />
                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rp8" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true" ClientInstanceName="rp8" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent>
                                            <%--  <div style="width: 1275px; overflow-y: hidden; overflow-x: hidden;">--%>
                                            <dx:ASPxMenu ID="menu8" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu8_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid8" ClientInstanceName="grid8" Caption="" runat="server" AutoGenerateColumns="False"
                                                EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid8_CustomCallback"
                                                EnableTheming="false" DataSourceID="dtgrid8" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize" OnCellEditorInitialize="grid8_CellEditorInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,8)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'8');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" AllowFocusedRow="true" />
                                                <SettingsPager PageSize="7" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport8" ExportedRowType="All" runat="server" GridViewID="grid8"></dx:ASPxGridViewExporter>
                                            <%--  </div>--%>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){ 
                                            HiddenDocumentCollepseStatu.SetText(rp8.GetCollapsed());
                                            if(!rp8.GetCollapsed()){
                                                grid8.PerformCallback(document.getElementById('ctl00_ContentPlaceHolder1_HiddenLoadqueryText').value);
                                            }
                                        }" />
                                </dx:ASPxRoundPanel>
                                <dx:ASPxRoundPanel ID="rp9" Theme="Glass" Collapsed="true" EnableAnimation="true" ShowCollapseButton="true" AllowCollapsingByHeaderClick="true" ClientInstanceName="rp9" runat="server" Width="100%" Height="20%">
                                    <PanelCollection>
                                        <dx:PanelContent>
                                            <%--  <div style="width: 1275px; overflow-y: hidden; overflow-x: hidden;">--%>
                                            <dx:ASPxMenu ID="menu9" runat="server" AutoSeparators="RootOnly"
                                                Theme="Glass"
                                                ItemSpacing="0px" SeparatorHeight="100%" SeparatorWidth="2px" ShowPopOutImages="True"
                                                ShowSubMenuShadow="False" OnItemClick="menu9_ItemClick">
                                                <SubMenuStyle GutterWidth="0px" />
                                                <RootItemSubMenuOffset FirstItemX="-2" LastItemX="-2" X="-1" />
                                                <SubMenuItemStyle ImageSpacing="19px" PopOutImageSpacing="30px">
                                                </SubMenuItemStyle>
                                                <ItemSubMenuOffset FirstItemX="2" FirstItemY="-12" LastItemX="2" LastItemY="-12" X="2" Y="-12" />
                                            </dx:ASPxMenu>
                                            <dx:ASPxGridView ID="grid9" ClientInstanceName="grid9" Caption="" runat="server" AutoGenerateColumns="False"
                                                EnableRowsCache="false"
                                                Theme="Glass" OnCustomCallback="grid9_CustomCallback"
                                                EnableTheming="false" DataSourceID="dtgrid9" KeyFieldName="ID" OnCommandButtonInitialize="Grid_CommandButtonInitialize" OnCellEditorInitialize="grid9_CellEditorInitialize"
                                                OnInitNewRow="grid_InitNewRow" OnCustomButtonCallback="grid_CustomButtonCallback" OnRowValidating="grid_RowValidating" OnHtmlDataCellPrepared="Grid_HtmlDataCellPrepared"
                                                OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" OnHtmlRowPrepared="grid_HtmlRowPrepared" OnRowDeleting="grid_RowDeleting" OnAfterPerformCallback="grid_AfterPerformCallback">
                                                <ClientSideEvents EndCallback="function(s, e) {OnRefreshAllObjects(s,e,9)}" RowDblClick="function(s, e) { OnGridFocusedRowChanged(s,e,'9');} " />
                                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="true" AutoExpandAllGroups="false" AllowFocusedRow="true" />
                                                <SettingsPager PageSize="7" ShowSeparators="True"></SettingsPager>
                                                <SettingsText EmptyDataRow=" " />
                                                <SettingsCommandButton>
                                                    <CancelButton Image-ToolTip="Cancel" Image-Url="../../../images/cancelnew.png">
                                                    </CancelButton>
                                                    <UpdateButton Image-ToolTip="Done" Image-Url="../../../images/update.gif">
                                                    </UpdateButton>
                                                </SettingsCommandButton>
                                                <SettingsEditing Mode="Inline" EditFormColumnCount="3" NewItemRowPosition="Top"></SettingsEditing>
                                                <Styles>
                                                    <Header ForeColor="White"></Header>
                                                    <HeaderPanel ForeColor="White"></HeaderPanel>
                                                    <GroupPanel CssClass="gridNewGroupPanel"></GroupPanel>
                                                </Styles>
                                            </dx:ASPxGridView>
                                            <dx:ASPxGridViewExporter ID="gridExport9" ExportedRowType="All" runat="server" GridViewID="grid9"></dx:ASPxGridViewExporter>
                                            <%--  </div>--%>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                    <HeaderStyle Paddings-Padding="0" Paddings-PaddingLeft="10" />
                                    <ClientSideEvents CollapsedChanged="function(s,e){ 
                                            HiddenDocumentCollepseStatu.SetText(rp9.GetCollapsed());
                                            if(!rp9.GetCollapsed()){
                                                grid9.PerformCallback(document.getElementById('ctl00_ContentPlaceHolder1_HiddenLoadqueryText').value);
                                            }
                                        }" />
                                </dx:ASPxRoundPanel>
                            </dx:SplitterContentControl>
                        </ContentCollection>
                    </dx:SplitterPane>
                </Panes>
            </dx:SplitterPane>


        </Panes>
    </dx:ASPxSplitter>
    <STDT:StDataTable ID="dtgrid1" runat="server" />
    <STDT:StDataTable ID="dtgrid2" runat="server" />
    <STDT:StDataTable ID="dtgrid3" runat="server" />
    <STDT:StDataTable ID="dtgrid4" runat="server" />
    <STDT:StDataTable ID="dtgrid5" runat="server" />
    <STDT:StDataTable ID="dtgrid6" runat="server" />
    <STDT:StDataTable ID="dtgrid7" runat="server" />
    <STDT:StDataTable ID="dtgrid8" runat="server" />
    <STDT:StDataTable ID="dtgrid9" runat="server" />
    <STDT:StDataTable ID="CmbS1" runat="server" />
    <STDT:StDataTable ID="CmbS2" runat="server" />
    <STDT:StDataTable ID="CmbS3" runat="server" />
    <STDT:StDataTable ID="CmbS4" runat="server" />
    <STDT:StDataTable ID="CmbS5" runat="server" />


    <STDT:StDataTable ID="DS11" runat="server" />
    <STDT:StDataTable ID="DS12" runat="server" />
    <STDT:StDataTable ID="DS13" runat="server" />
    <STDT:StDataTable ID="DS14" runat="server" />
    <STDT:StDataTable ID="DS15" runat="server" />
    <STDT:StDataTable ID="DS16" runat="server" />
    <STDT:StDataTable ID="DS17" runat="server" />
    <STDT:StDataTable ID="DS18" runat="server" />
    <STDT:StDataTable ID="DS19" runat="server" />
    <STDT:StDataTable ID="DS110" runat="server" />
    <STDT:StDataTable ID="DS111" runat="server" />
    <STDT:StDataTable ID="DS112" runat="server" />
    <STDT:StDataTable ID="DS113" runat="server" />
    <STDT:StDataTable ID="DS114" runat="server" />
    <STDT:StDataTable ID="DS115" runat="server" />
    <STDT:StDataTable ID="DS116" runat="server" />
    <STDT:StDataTable ID="DS117" runat="server" />
    <STDT:StDataTable ID="DS118" runat="server" />
    <STDT:StDataTable ID="DS119" runat="server" />
    <STDT:StDataTable ID="DS120" runat="server" />

    <STDT:StDataTable ID="DS21" runat="server" />
    <STDT:StDataTable ID="DS22" runat="server" />
    <STDT:StDataTable ID="DS23" runat="server" />
    <STDT:StDataTable ID="DS24" runat="server" />
    <STDT:StDataTable ID="DS25" runat="server" />
    <STDT:StDataTable ID="DS26" runat="server" />
    <STDT:StDataTable ID="DS27" runat="server" />
    <STDT:StDataTable ID="DS28" runat="server" />
    <STDT:StDataTable ID="DS29" runat="server" />
    <STDT:StDataTable ID="DS210" runat="server" />
    <STDT:StDataTable ID="DS211" runat="server" />
    <STDT:StDataTable ID="DS212" runat="server" />
    <STDT:StDataTable ID="DS213" runat="server" />
    <STDT:StDataTable ID="DS214" runat="server" />
    <STDT:StDataTable ID="DS215" runat="server" />
    <STDT:StDataTable ID="DS216" runat="server" />
    <STDT:StDataTable ID="DS217" runat="server" />
    <STDT:StDataTable ID="DS218" runat="server" />
    <STDT:StDataTable ID="DS219" runat="server" />
    <STDT:StDataTable ID="DS220" runat="server" />


    <STDT:StDataTable ID="DS31" runat="server" />
    <STDT:StDataTable ID="DS32" runat="server" />
    <STDT:StDataTable ID="DS33" runat="server" />
    <STDT:StDataTable ID="DS34" runat="server" />
    <STDT:StDataTable ID="DS35" runat="server" />
    <STDT:StDataTable ID="DS36" runat="server" />
    <STDT:StDataTable ID="DS37" runat="server" />
    <STDT:StDataTable ID="DS38" runat="server" />
    <STDT:StDataTable ID="DS39" runat="server" />
    <STDT:StDataTable ID="DS310" runat="server" />
    <STDT:StDataTable ID="DS311" runat="server" />
    <STDT:StDataTable ID="DS312" runat="server" />
    <STDT:StDataTable ID="DS313" runat="server" />
    <STDT:StDataTable ID="DS314" runat="server" />
    <STDT:StDataTable ID="DS315" runat="server" />
    <STDT:StDataTable ID="DS316" runat="server" />
    <STDT:StDataTable ID="DS317" runat="server" />
    <STDT:StDataTable ID="DS318" runat="server" />
    <STDT:StDataTable ID="DS319" runat="server" />
    <STDT:StDataTable ID="DS320" runat="server" />

    <STDT:StDataTable ID="DS41" runat="server" />
    <STDT:StDataTable ID="DS42" runat="server" />
    <STDT:StDataTable ID="DS43" runat="server" />
    <STDT:StDataTable ID="DS44" runat="server" />
    <STDT:StDataTable ID="DS45" runat="server" />
    <STDT:StDataTable ID="DS46" runat="server" />
    <STDT:StDataTable ID="DS47" runat="server" />
    <STDT:StDataTable ID="DS48" runat="server" />
    <STDT:StDataTable ID="DS49" runat="server" />
    <STDT:StDataTable ID="DS410" runat="server" />
    <STDT:StDataTable ID="DS411" runat="server" />
    <STDT:StDataTable ID="DS412" runat="server" />
    <STDT:StDataTable ID="DS413" runat="server" />
    <STDT:StDataTable ID="DS414" runat="server" />
    <STDT:StDataTable ID="DS415" runat="server" />
    <STDT:StDataTable ID="DS416" runat="server" />
    <STDT:StDataTable ID="DS417" runat="server" />
    <STDT:StDataTable ID="DS418" runat="server" />
    <STDT:StDataTable ID="DS419" runat="server" />
    <STDT:StDataTable ID="DS420" runat="server" />

    <STDT:StDataTable ID="DS51" runat="server" />
    <STDT:StDataTable ID="DS52" runat="server" />
    <STDT:StDataTable ID="DS53" runat="server" />
    <STDT:StDataTable ID="DS54" runat="server" />
    <STDT:StDataTable ID="DS55" runat="server" />
    <STDT:StDataTable ID="DS56" runat="server" />
    <STDT:StDataTable ID="DS57" runat="server" />
    <STDT:StDataTable ID="DS58" runat="server" />
    <STDT:StDataTable ID="DS59" runat="server" />
    <STDT:StDataTable ID="DS510" runat="server" />
    <STDT:StDataTable ID="DS511" runat="server" />
    <STDT:StDataTable ID="DS512" runat="server" />
    <STDT:StDataTable ID="DS513" runat="server" />
    <STDT:StDataTable ID="DS514" runat="server" />
    <STDT:StDataTable ID="DS515" runat="server" />
    <STDT:StDataTable ID="DS516" runat="server" />
    <STDT:StDataTable ID="DS517" runat="server" />
    <STDT:StDataTable ID="DS518" runat="server" />
    <STDT:StDataTable ID="DS519" runat="server" />
    <STDT:StDataTable ID="DS520" runat="server" />

    <STDT:StDataTable ID="DS61" runat="server" />
    <STDT:StDataTable ID="DS62" runat="server" />
    <STDT:StDataTable ID="DS63" runat="server" />
    <STDT:StDataTable ID="DS64" runat="server" />
    <STDT:StDataTable ID="DS65" runat="server" />
    <STDT:StDataTable ID="DS66" runat="server" />
    <STDT:StDataTable ID="DS67" runat="server" />
    <STDT:StDataTable ID="DS68" runat="server" />
    <STDT:StDataTable ID="DS69" runat="server" />
    <STDT:StDataTable ID="DS610" runat="server" />
    <STDT:StDataTable ID="DS611" runat="server" />
    <STDT:StDataTable ID="DS612" runat="server" />
    <STDT:StDataTable ID="DS613" runat="server" />
    <STDT:StDataTable ID="DS614" runat="server" />
    <STDT:StDataTable ID="DS615" runat="server" />
    <STDT:StDataTable ID="DS616" runat="server" />
    <STDT:StDataTable ID="DS617" runat="server" />
    <STDT:StDataTable ID="DS618" runat="server" />
    <STDT:StDataTable ID="DS619" runat="server" />
    <STDT:StDataTable ID="DS620" runat="server" />

    <STDT:StDataTable ID="DS71" runat="server" />
    <STDT:StDataTable ID="DS72" runat="server" />
    <STDT:StDataTable ID="DS73" runat="server" />
    <STDT:StDataTable ID="DS74" runat="server" />
    <STDT:StDataTable ID="DS75" runat="server" />
    <STDT:StDataTable ID="DS76" runat="server" />
    <STDT:StDataTable ID="DS77" runat="server" />
    <STDT:StDataTable ID="DS78" runat="server" />
    <STDT:StDataTable ID="DS79" runat="server" />
    <STDT:StDataTable ID="DS710" runat="server" />
    <STDT:StDataTable ID="DS711" runat="server" />
    <STDT:StDataTable ID="DS712" runat="server" />
    <STDT:StDataTable ID="DS713" runat="server" />
    <STDT:StDataTable ID="DS714" runat="server" />
    <STDT:StDataTable ID="DS715" runat="server" />
    <STDT:StDataTable ID="DS716" runat="server" />
    <STDT:StDataTable ID="DS717" runat="server" />
    <STDT:StDataTable ID="DS718" runat="server" />
    <STDT:StDataTable ID="DS719" runat="server" />
    <STDT:StDataTable ID="DS720" runat="server" />

    <STDT:StDataTable ID="DS81" runat="server" />
    <STDT:StDataTable ID="DS82" runat="server" />
    <STDT:StDataTable ID="DS83" runat="server" />
    <STDT:StDataTable ID="DS84" runat="server" />
    <STDT:StDataTable ID="DS85" runat="server" />
    <STDT:StDataTable ID="DS86" runat="server" />
    <STDT:StDataTable ID="DS87" runat="server" />
    <STDT:StDataTable ID="DS88" runat="server" />
    <STDT:StDataTable ID="DS89" runat="server" />
    <STDT:StDataTable ID="DS810" runat="server" />
    <STDT:StDataTable ID="DS811" runat="server" />
    <STDT:StDataTable ID="DS812" runat="server" />
    <STDT:StDataTable ID="DS813" runat="server" />
    <STDT:StDataTable ID="DS814" runat="server" />
    <STDT:StDataTable ID="DS815" runat="server" />
    <STDT:StDataTable ID="DS816" runat="server" />
    <STDT:StDataTable ID="DS817" runat="server" />
    <STDT:StDataTable ID="DS818" runat="server" />
    <STDT:StDataTable ID="DS819" runat="server" />
    <STDT:StDataTable ID="DS820" runat="server" />

    <STDT:StDataTable ID="DS91" runat="server" />
    <STDT:StDataTable ID="DS92" runat="server" />
    <STDT:StDataTable ID="DS93" runat="server" />
    <STDT:StDataTable ID="DS94" runat="server" />
    <STDT:StDataTable ID="DS95" runat="server" />
    <STDT:StDataTable ID="DS96" runat="server" />
    <STDT:StDataTable ID="DS97" runat="server" />
    <STDT:StDataTable ID="DS98" runat="server" />
    <STDT:StDataTable ID="DS99" runat="server" />
    <STDT:StDataTable ID="DS910" runat="server" />
    <STDT:StDataTable ID="DS911" runat="server" />
    <STDT:StDataTable ID="DS912" runat="server" />
    <STDT:StDataTable ID="DS913" runat="server" />
    <STDT:StDataTable ID="DS914" runat="server" />
    <STDT:StDataTable ID="DS915" runat="server" />
    <STDT:StDataTable ID="DS916" runat="server" />
    <STDT:StDataTable ID="DS917" runat="server" />
    <STDT:StDataTable ID="DS918" runat="server" />
    <STDT:StDataTable ID="DS919" runat="server" />
    <STDT:StDataTable ID="DS920" runat="server" />

    <div style="display: none">
        <asp:HiddenField runat="server" ID="HiddenLoadqueryText" Value="AA" />
        <asp:HiddenField runat="server" ID="HiddenP1" Value="" />
        <asp:HiddenField runat="server" ID="HiddenP2" Value="" />
        <asp:HiddenField runat="server" ID="HiddenP3" Value="" />
        <asp:HiddenField runat="server" ID="HiddenP4" Value="" />
        <asp:HiddenField runat="server" ID="HiddenP5" Value="" />
        <asp:HiddenField runat="server" ID="HiddenMainStatu" Value="" />
        <asp:HiddenField runat="server" ID="HiddenpageType" Value="" />
        <asp:HiddenField runat="server" ID="HiddenRpath" Value="" />
        <asp:HiddenField runat="server" ID="HiddendirectParam" />
        <asp:HiddenField runat="server" ID="hndBos" />

        <dx:ASPxHyperLink runat="server" ID="HiddenTaskCollepseStatu" ClientInstanceName="HiddenTaskCollepseStatu" Text="Test" />
        <dx:ASPxHyperLink runat="server" ID="HiddenCargoCollepseStatu" ClientInstanceName="HiddenCargoCollepseStatu" Text="Test" />
        <dx:ASPxHyperLink runat="server" ID="HiddenCostCollepseStatu" ClientInstanceName="HiddenCostCollepseStatu" Text="Test" />
        <dx:ASPxHyperLink runat="server" ID="HiddenCommentCollepseStatu" ClientInstanceName="HiddenCommentCollepseStatu" Text="Test" />
        <dx:ASPxHyperLink runat="server" ID="HiddenDocumentCollepseStatu" ClientInstanceName="HiddenDocumentCollepseStatu" Text="Test" />
        <dx:ASPxButton ID="btnhat" runat="server" OnClick="btnhat_Click"></dx:ASPxButton>

        <asp:Label ID="lblGId" runat="server"></asp:Label>
        <asp:Label ID="lblden" runat="server"></asp:Label>
        <asp:TextBox ID="txtCurLinkFields" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink2" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField2" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink3" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField3" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink4" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField4" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink5" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField5" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink6" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField6" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink7" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField7" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink8" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField8" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlink9" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkField9" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtlinkType" runat="server"></asp:TextBox>

        <asp:TextBox ID="txtFilterText" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams1" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtinsertparams2" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams2" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams2" runat="server"></asp:TextBox>

        <asp:TextBox ID="txtinsertparams3" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams3" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams3" runat="server"></asp:TextBox>

        <asp:TextBox ID="txtinsertparams4" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams4" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams4" runat="server"></asp:TextBox>

        <asp:TextBox ID="txtinsertparams5" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams5" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams5" runat="server"></asp:TextBox>

        <asp:TextBox ID="txtinsertparams6" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams6" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams6" runat="server"></asp:TextBox>

        <asp:TextBox ID="txtinsertparams7" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams7" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams7" runat="server"></asp:TextBox>

        <asp:TextBox ID="txtinsertparams8" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams8" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams8" runat="server"></asp:TextBox>

        <asp:TextBox ID="txtinsertparams9" runat="server"></asp:TextBox>
        <asp:TextBox ID="txteditparams9" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtdeleteparams9" runat="server"></asp:TextBox>

        <asp:TextBox ID="txtcurrentId" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtMessage" runat="server"></asp:TextBox>
        <dx:ASPxTextBox ID="txttask" ClientInstanceName="txttask" runat="server"></dx:ASPxTextBox>
        <asp:TextBox ID="txtPostDurum" runat="server" Text="First"></asp:TextBox>

        <asp:TextBox ID="txtCatchParameters1" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters2" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters3" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters4" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters5" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters6" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters7" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters8" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParameters9" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchParamsAndValues" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCatchValues1" runat="server" Text=""></asp:TextBox>

        <asp:TextBox ID="txtTransferedParameters1" runat="server" Text=""></asp:TextBox>

        <asp:TextBox ID="txtSubInsertPer2" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer3" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer4" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer5" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer6" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer7" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer8" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtSubInsertPer9" runat="server" Text=""></asp:TextBox>

        <asp:TextBox ID="txtCollapseHeader1" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseHeader2" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseHeader3" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseHeader4" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseHeader5" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseHeader6" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseHeader7" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseHeader8" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseHeader9" runat="server" Text=""></asp:TextBox>

        <asp:TextBox ID="txtCollapseStatu1" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseStatu2" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseStatu3" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseStatu4" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseStatu5" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseStatu6" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseStatu7" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseStatu8" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtCollapseStatu9" runat="server" Text=""></asp:TextBox>

        <asp:TextBox ID="txtMasterObject2" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtMasterObject3" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtMasterObject4" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtMasterObject5" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtMasterObject6" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtMasterObject7" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtMasterObject8" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtMasterObject9" runat="server" Text=""></asp:TextBox>

        <asp:TextBox ID="txtDHT1" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtDHT2" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtDHT3" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtDHT4" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtDHT5" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtDHT6" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtDHT7" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtDHT8" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtDHT9" runat="server" Text=""></asp:TextBox>

        <asp:TextBox ID="txtRef1" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtRef2" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtRef3" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtRef4" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtRef5" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtRef6" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtRef7" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtRef8" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtRef9" runat="server" Text=""></asp:TextBox>

        <asp:TextBox ID="txtOBJP1" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJP2" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJP3" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJP4" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJP5" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJP6" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJP7" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJP8" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJP9" runat="server" Text=""></asp:TextBox>

        <asp:TextBox ID="txtOBJF1" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJF2" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJF3" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJF4" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJF5" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJF6" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJF7" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJF8" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOBJF9" runat="server" Text=""></asp:TextBox>


        <asp:TextBox ID="txtBos" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="txtOnayStatus" runat="server" Text=""></asp:TextBox>

        <dx:ASPxCallback ID="callbackPanel1" ClientInstanceName="callbackPanel1" runat="server"
            OnCallback="callbackPanel1_Callback">
            <ClientSideEvents CallbackComplete="function(s,e){SetGridEditValue(e.result)}" />
        </dx:ASPxCallback>
    </div>

</asp:Content>


