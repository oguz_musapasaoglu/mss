﻿<%@ page title="" language="C#" masterpagefile="~/BlankMasterPage.Master" autoeventwireup="true" codebehind="sifre.aspx.cs" inherits="MSS1.sifre" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2>
        <asp:Label ID="lblBaslik" runat="server"></asp:Label>
    </h2>
    <br />
    <table class="style1" cellspacing="5">
        <tr>
            <td class="style2">
                <asp:Label ID="Label1" runat="server"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtkad" CssClass="textbox" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                <asp:Label ID="Label2" runat="server"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtesifre" CssClass="textbox" runat="server"
                    TextMode="Password" OnTextChanged="txtesifre_TextChanged"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                <asp:Label ID="Label3" runat="server"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtysifre" CssClass="textbox" runat="server"
                    TextMode="Password" OnTextChanged="txtysifre_TextChanged" AutoPostBack="false"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="lblMesaj" runat="server" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style2">
                <asp:Label ID="Label4" runat="server"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtysifret" CssClass="textbox" runat="server"
                    TextMode="Password" OnTextChanged="txtysifret_TextChanged"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style2">&nbsp;
            </td>
            <td>
                <asp:Button ID="Button1" CssClass="myButton" runat="server"
                    OnClick="Button1_Click" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" style="color: red; font-family: Arial; font-size: 12px; width: 300px">The password must contain;
                <br />
                -  at least 9 characters <br />
                -  at least 1 UPPERCASE character <br />
                -  at least 1 lowercase character <br />
                -  at least 1 Numeric character <br />
                -  at least 1 Punctuation mark (( .;/:?!+@#$%^&_-=[](){}) <br />
            </td> 

        </tr>
    </table>
</asp:Content>

