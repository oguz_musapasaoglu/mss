﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MSS1.WSGeneric;
using System.ServiceModel;
using MSS1.Codes;

namespace MSS1
{
    public partial class OnePageList2 : Bases
    {
        SessionBase _obj;
        public string OBJ1_pageId { get; set; }
        public string OBJ2_pageId { get; set; }
        public string OBJ3_pageId { get; set; }
        public string OBJ1_fieldId { get; set; }
        public string OBJ2_fieldId { get; set; }
        public string OBJ3_fieldId { get; set; }

        private void QueryStringParameterDesing()
        {
            string[] obj1, obj2, obj3 = { };
            try
            {
                obj1 = Request.QueryString["OJ1"].ToString().Split(';');
                obj2 = Request.QueryString["OJ2"].ToString().Split(';');
                obj3 = Request.QueryString["OJ3"].ToString().Split(';');
            }
            catch (Exception)
            {
                obj1 = Request.QueryString["OJ1"].ToString().Split(';');
                obj2 = Request.QueryString["OJ2"].ToString().Split(';');
            }


            OBJ1_pageId = obj1[0].ToString();
            OBJ1_fieldId = obj1[1].ToString();

            OBJ2_pageId = obj2[0].ToString();
            OBJ2_fieldId = obj2[1].ToString();

            try
            {
                OBJ3_pageId = obj3[0].ToString();
                OBJ3_fieldId = obj3[1].ToString();
                grid3.Visible = true;
            }
            catch (Exception)
            {
                OBJ3_pageId = "";
                OBJ3_fieldId = "";
                grid3.Visible = false;
            }


            txtlink.Text = f.tekbirsonucdonder("select LINK from [0C_50057_00_WEBPAGES] where COMPANY='" + _obj.Comp + "' AND [Web Page ID]='" + OBJ2_pageId + "' and [Web Object ID]='" + OBJ2_fieldId + "'");

            txtlinkField.Text = f.tekbirsonucdonder("select [LINK FIELD] from [0C_50057_00_WEBPAGES] where COMPANY='" + _obj.Comp + "' AND [Web Page ID]='" + OBJ2_pageId + "' and [Web Object ID]='" + OBJ2_fieldId + "'");
        }

        webObjectHandler woh = new webObjectHandler();
        ServiceUtilities services = new ServiceUtilities();
        fonksiyonlar f = new fonksiyonlar();
        Methods method = new Methods(); 
        public ws_baglantı ws = new ws_baglantı();

        public Web_GenericFunction_PortClient webFonksiyonlari;
        public BasicHttpBinding _binding;
        public EndpointAddress _endpoint;
        private Web_GenericFunction_PortClient GetWebFonksiyonlari(string sirket)
        {
            _binding = new BasicHttpBinding();
            _binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            _binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            _endpoint = new EndpointAddress(new Uri(ws.sirketalCustomFonksiyon(sirket)));
            webFonksiyonlari = new Web_GenericFunction_PortClient(_binding, _endpoint);
            webFonksiyonlari.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(ws.kadiws, ws.sifrews);
            webFonksiyonlari.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return webFonksiyonlari;
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["uyedurum"] == null)
                Response.Redirect("Default.aspx");
            SetSessionValues();

        }

        protected void Page_Load(object sender, EventArgs e)
        {

            QueryStringParameterDesing();
            if (IsCallback | IsPostBack) return;
            lblGId.Text = Request.QueryString["GId"];
            txtDate.Text = string.Empty;

            if (OBJ1_fieldId != "OJ9999")
            {
                dtgrid2.Table = woh.webObjectHandlerExtracted(grid2, menu2, txtinsertparams2, txteditparams2, txtdeleteparams2, _obj.UserName, _obj.Comp, OBJ1_pageId, OBJ1_fieldId, HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString(), HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), 3).Copy();
                DataColumn[] pricols = new DataColumn[1];
                pricols[0] = dtgrid2.Table.Columns["ID"];
                dtgrid2.Table.PrimaryKey = pricols;
                grid2.DataBind();
            }
            else
            {
                grid2.Visible = false;
            }



            dtgrid1.Table = woh.webObjectHandlerExtracted(grid, menu, txtinsertparams1, txteditparams1, txtdeleteparams1, _obj.UserName, _obj.Comp, OBJ2_pageId, OBJ2_fieldId, HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString(), HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), 1).Copy();
            DataColumn[] pricols1 = new DataColumn[1];
            pricols1[0] = dtgrid1.Table.Columns["ID"];
            dtgrid1.Table.PrimaryKey = pricols1;


            CallGrid3Data("RCOMP='AA'");





            Session["OPL5Rows"] = webObjectHandler.GetDynamicWebPageObjects(OBJ2_pageId, OBJ2_fieldId, _obj.Comp).Tables[4];
            Session["OPL10Grid3Rows"] = webObjectHandler.GetDynamicWebPageObjects(OBJ3_pageId, OBJ3_fieldId, _obj.Comp).Tables[4];
            FillDS1((DataTable)Session["OPL5Rows"]);

            grid.DataBind();
            grid3.DataBind();


        }

        void SetSessionValues()
        {
            _obj = TabSessions.ControlState(this, Request.QueryString["GId"].ToString());

        }

        void FillDS1(DataTable dtRows)
        {
            int sayac = 1;
            string query = string.Empty;
            for (int i = 0; i < dtRows.Rows.Count; i++)
            {
                switch (dtRows.Rows[i]["DX Type1"].ToString())
                {
                    case "DATA":
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Source"].ToString()))
                        {
                            query = webObjectHandler.FillCombobox(dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(),
                                dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), _obj.Comp, _obj.UserName);
                            switch (sayac)
                            {
                                case 1:
                                    DS11.Table = f.datatablegonder(query);
                                    break;
                                case 2:
                                    DS12.Table = f.datatablegonder(query);
                                    break;
                                case 3:
                                    DS13.Table = f.datatablegonder(query);
                                    break;
                                case 4:
                                    DS14.Table = f.datatablegonder(query);
                                    break;
                                case 5:
                                    DS15.Table = f.datatablegonder(query);
                                    break;
                                case 6:
                                    DS16.Table = f.datatablegonder(query);
                                    break;
                                case 7:
                                    DS17.Table = f.datatablegonder(query);
                                    break;
                                case 8:
                                    DS18.Table = f.datatablegonder(query);
                                    break;
                                case 9:
                                    DS19.Table = f.datatablegonder(query);
                                    break;
                                case 10:
                                    DS110.Table = f.datatablegonder(query);
                                    break;
                                case 11:
                                    DS111.Table = f.datatablegonder(query);
                                    break;
                                case 12:
                                    DS112.Table = f.datatablegonder(query);
                                    break;
                                case 13:
                                    DS113.Table = f.datatablegonder(query);
                                    break;
                                case 14:
                                    DS114.Table = f.datatablegonder(query);
                                    break;
                                case 15:
                                    DS115.Table = f.datatablegonder(query);
                                    break;
                                case 16:
                                    DS116.Table = f.datatablegonder(query);
                                    break;
                                case 17:
                                    DS117.Table = f.datatablegonder(query);
                                    break;
                                case 18:
                                    DS118.Table = f.datatablegonder(query);
                                    break;
                                case 19:
                                    DS119.Table = f.datatablegonder(query);
                                    break;
                                case 20:
                                    DS120.Table = f.datatablegonder(query);
                                    break;
                            }
                            sayac++;
                        }
                        break;
                }
            }
        }

        void FillDS2(DataTable dtRows)
        {
            int sayac = 1;
            string query = string.Empty;
            for (int i = 0; i < dtRows.Rows.Count; i++)
            {
                switch (dtRows.Rows[i]["DX Type1"].ToString())
                {
                    case "DATA":
                        if (!string.IsNullOrEmpty(dtRows.Rows[i]["DX Source"].ToString()))
                        {
                            query = webObjectHandler.FillCombobox(dtRows.Rows[i]["DX Source"].ToString(), dtRows.Rows[i]["DX Filter"].ToString(),
                                dtRows.Rows[i]["DX Combo Value"].ToString(), dtRows.Rows[i]["DX Combo Text"].ToString(), _obj.Comp, _obj.UserName);
                            switch (sayac)
                            {
                                case 1:
                                    DS21.Table = f.datatablegonder(query);
                                    break;
                                case 2:
                                    DS22.Table = f.datatablegonder(query);
                                    break;
                                case 3:
                                    DS23.Table = f.datatablegonder(query);
                                    break;
                                case 4:
                                    DS24.Table = f.datatablegonder(query);
                                    break;
                                case 5:
                                    DS25.Table = f.datatablegonder(query);
                                    break;
                                case 6:
                                    DS26.Table = f.datatablegonder(query);
                                    break;
                                case 7:
                                    DS27.Table = f.datatablegonder(query);
                                    break;
                                case 8:
                                    DS28.Table = f.datatablegonder(query);
                                    break;
                                case 9:
                                    DS29.Table = f.datatablegonder(query);
                                    break;
                                case 10:
                                    DS210.Table = f.datatablegonder(query);
                                    break;
                                case 11:
                                    DS211.Table = f.datatablegonder(query);
                                    break;
                                case 12:
                                    DS212.Table = f.datatablegonder(query);
                                    break;
                                case 13:
                                    DS213.Table = f.datatablegonder(query);
                                    break;
                                case 14:
                                    DS214.Table = f.datatablegonder(query);
                                    break;
                                case 15:
                                    DS215.Table = f.datatablegonder(query);
                                    break;
                                case 16:
                                    DS216.Table = f.datatablegonder(query);
                                    break;
                                case 17:
                                    DS217.Table = f.datatablegonder(query);
                                    break;
                                case 18:
                                    DS218.Table = f.datatablegonder(query);
                                    break;
                                case 19:
                                    DS219.Table = f.datatablegonder(query);
                                    break;
                                case 20:
                                    DS220.Table = f.datatablegonder(query);
                                    break;
                            }
                            sayac++;
                        }
                        break;
                }
            }
        }



        private void CallGrid3Data(string filters)
        {

            if (OBJ3_fieldId == "OJ9999") return;
            if (OBJ3_pageId.Length > 0 & OBJ3_fieldId.Length > 0)
            {
                dtgrid3.Table = woh.webObjectHandlerExtractedSub(grid3, menu3, txtinsertparams3, txteditparams3, txtdeleteparams3, _obj.UserName, _obj.Comp,
                    OBJ3_pageId, OBJ3_fieldId, HttpContext.Current.Request.Cookies["screenWidth"].Value.ToString(),
                    HttpContext.Current.Request.Cookies["screenHeight"].Value.ToString(), 3).Copy();
                DataColumn[] pricols = new DataColumn[1];
                pricols[0] = dtgrid3.Table.Columns["ID"];
                dtgrid3.Table.PrimaryKey = pricols;
                grid3.DataSourceID = "dtgrid3";
                grid3.KeyFieldName = "ID";
            }
            else
            {
                grid3.Visible = false;
            }
        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }

        protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            switch (gridI.ID)
            {
                case "grid":
                    if (txtinsertparams1.Text.Length > 0)
                    {
                        string[] _temp = txtinsertparams1.Text.Split(',');
                        string[] _array = new string[txtinsertparams1.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else
                            {
                                if (e.NewValues[_temp[i].ToString()] != null)
                                    _array[i - 1] = e.NewValues[_temp[i].ToString()] != null ? e.NewValues[_temp[i].ToString()].ToString() : "";
                                else
                                    _array[i - 1] = "";
                            }
                        }

                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;
                case "grid2":
                    if (txtinsertparams2.Text.Length > 0)
                    {
                        string[] _temp = txtinsertparams2.Text.Split(',');
                        string[] _array = new string[txtinsertparams2.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else
                            {
                                if (e.NewValues[_temp[i].ToString()] != null)
                                    _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                                else
                                    _array[i - 1] = "";
                            }
                        }

                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;
                case "grid3":
                    if (txtinsertparams3.Text.Length > 0)
                    {
                        string[] _temp = txtinsertparams3.Text.Split(',');
                        string[] _array = new string[txtinsertparams3.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else
                            {
                                if (e.NewValues[_temp[i].ToString()] != null)
                                    _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                                else
                                    _array[i - 1] = "";
                            }
                        }

                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;
            }

        }
        protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            switch (gridI.ID)
            {
                case "grid":
                    if (txteditparams1.Text.Length > 0)
                    {
                        string[] _temp = txteditparams1.Text.Split(',');
                        string[] _array = new string[txteditparams1.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else
                            {
                                if (e.NewValues[_temp[i].ToString()] != null)
                                    _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                                else
                                    _array[i - 1] = "";
                            }
                        }

                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;
                case "grid2":
                    if (txteditparams2.Text.Length > 0)
                    {
                        string[] _temp = txteditparams2.Text.Split(',');
                        string[] _array = new string[txteditparams2.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else
                            {
                                if (e.NewValues[_temp[i].ToString()] != null)
                                    _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                                else
                                    _array[i - 1] = "";
                            }
                        }

                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;
                case "grid3":
                    if (txteditparams3.Text.Length > 0)
                    {
                        string[] _temp = txteditparams3.Text.Split(',');
                        string[] _array = new string[txteditparams3.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else
                            {
                                if (e.NewValues[_temp[i].ToString()] != null)
                                    _array[i - 1] = e.NewValues[_temp[i].ToString()].ToString();
                                else
                                    _array[i - 1] = "";
                            }
                        }

                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;

                default:
                    break;
            }

        }

        protected void grid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            switch (gridI.ID)
            {
                case "grid":
                    if (txtdeleteparams1.Text.Length > 0)
                    {
                        string[] _temp = txtdeleteparams1.Text.Split(',');
                        string[] _array = new string[txtdeleteparams1.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else
                            {
                                if (e.Values[_temp[i].ToString()] != null)
                                    _array[i - 1] = e.Values[_temp[i].ToString()].ToString();
                                else
                                    _array[i - 1] = "";
                            }
                        }
                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;
                case "grid2":
                    if (txtdeleteparams2.Text.Length > 0)
                    {
                        string[] _temp = txtdeleteparams2.Text.Split(',');
                        string[] _array = new string[txtdeleteparams2.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else
                            {
                                if (e.Values[_temp[i].ToString()] != null)
                                    _array[i - 1] = e.Values[_temp[i].ToString()].ToString();
                                else
                                    _array[i - 1] = "";
                            }
                        }
                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;
                case "grid3":
                    if (txtdeleteparams3.Text.Length > 0)
                    {
                        string[] _temp = txtdeleteparams3.Text.Split(',');
                        string[] _array = new string[txtdeleteparams3.Text.Split(',').Length - 1];
                        for (int i = 1; i < _temp.Length; i++)
                        {
                            if (_temp[i].ToString() == "COMP")
                            {
                                _array[i - 1] = _obj.Comp;
                            }
                            else if (_temp[i].ToString() == "USER")
                            {
                                _array[i - 1] = _obj.UserName;
                            }
                            else if (_temp[i].ToString() == "P1")
                            {
                                _array[i - 1] = this.HiddenP1.Value;
                            }
                            else if (_temp[i].ToString() == "P2")
                            {
                                _array[i - 1] = this.HiddenP2.Value;
                            }
                            else
                            {
                                if (e.Values[_temp[i].ToString()] != null)
                                    _array[i - 1] = e.Values[_temp[i].ToString()].ToString();
                                else
                                    _array[i - 1] = "";
                            }
                        }
                        services.NavFunctionsStatic(_obj.UserName, _obj.Comp, _temp[0].ToString(), _array);
                    }
                    break;


            }

        }

        protected void grid_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            ASPxGridView gridI = sender as ASPxGridView;
            if (e.ButtonID == "SelectAll")
            {
                gridI.Selection.SelectAll();
            }
            else if (e.ButtonID == "UnSelectAll")
            {
                gridI.Selection.UnselectAll();
            }
        }

        protected void menu_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                webObjectHandler.ExportPdf(gridExport);
            }
            else if (e.Item.Name.Contains("btnSelected"))
            {

                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(OBJ2_pageId, OBJ2_fieldId, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string[] filter = rows[0]["DX Filter"].ToString().Split(',');
                if (filter.Contains("DATE"))
                {
                    if (txtDate.Text == string.Empty)
                    {
                        txtCurGridId.Text = "1";
                        txtCurGridFieldId.Text = FieldId;
                        txtCurGridObjecId.Text = OBJ2_fieldId;
                        txtCurGridPageId.Text = OBJ2_pageId;
                        DatePopup.ShowOnPageLoad = true;
                    }
                    return;
                }
                ServiceUtilities.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);

                MessageBox("İşlem Başarıyla Tamamlanmıştır.");
            }
            else
            {
                this.Response.StatusCode = 500;
                this.Response.End();
            }
        }

        protected void grid3_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            ASPxGridView grid = (ASPxGridView)sender;
            if (e.Parameters.Contains("SelectButton|"))
            {
                string FieldId = e.Parameters.Split('|')[1];
                DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(OBJ3_pageId, OBJ3_fieldId, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                ServiceUtilities.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp);
            }
            string values = string.Empty;
            if (!string.IsNullOrEmpty(HiddenP1.Value)) values = HiddenP1.Value;
            if (!string.IsNullOrEmpty(HiddenP2.Value)) values += ";" + HiddenP2.Value;
            if (string.IsNullOrEmpty(values)) return;
            this.dtgrid3.Table = webObjectHandler.fillSubGrid(OBJ3_pageId, OBJ3_fieldId, _obj.Comp, _obj.UserName, OBJ2_pageId, OBJ2_fieldId, values);
            DataColumn[] pricols = new DataColumn[1];
            pricols[0] = this.dtgrid3.Table.Columns["ID"];
            this.dtgrid3.Table.PrimaryKey = pricols;
            grid.DataSourceID = "dtgrid3";
            grid.KeyFieldName = "ID";

            string _sessionname = "OPL10Grid" + grid.ID.Substring(grid.ID.Length - 1) + "Rows";
            grid.FilterExpression = webObjectHandler.DefaultSearchGrid(((DataTable)Session[_sessionname]).Select("[DX Default Search]<>'' AND [DX Display Order]>0"));

            string _filterexpression = grid.FilterExpression;
            grid.DataBind();
            grid.FilterExpression = _filterexpression;
        }

        protected void Grid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            ASPxGridView Igrid = sender as ASPxGridView;

            string _sessionname = "OPL5Rows";

            DataRow[] rows = ((DataTable)Session[_sessionname]).Select("[DX MouseOver Text]<>'' and [DX Field ID]='" + e.DataColumn.FieldName + "'");
            if (rows.Length > 0)
            {
                try
                {
                    string _val = Igrid.GetRowValues(e.VisibleIndex, rows[0]["DX MouseOver Text"].ToString()).ToString();
                    e.Cell.ToolTip = _val;
                }
                catch { e.Cell.ToolTip = string.Format("{0}", e.CellValue); }
            }
            else
                e.Cell.ToolTip = string.Format("{0}", e.CellValue);

            try
            {
                if (e.DataColumn is GridViewDataHyperLinkColumn)
                {
                    if (string.IsNullOrEmpty(e.CellValue.ToString()))
                        e.Cell.Controls[0].Visible = false;

                }
            }
            catch
            {

            }
        }

        protected void menu3_ItemClick(object source, MenuItemEventArgs e)
        {
            if (e.Item.Name.Equals("btnExcel"))
            {
                gridExport3.WriteXlsToResponse();

            }
            else if (e.Item.Name.Equals("btnPdf"))
            {
                webObjectHandler.ExportPdf(gridExport3);
            }
            else if (e.Item.Name.Contains("btnSelected"))
            {
                string FieldId = e.Item.Name.Replace("btnSelected", string.Empty);
                DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(OBJ3_pageId, OBJ3_fieldId, _obj.Comp).Tables[0].Select("[DX Field ID]='" + FieldId + "'");
                string[] filter = rows[0]["DX Filter"].ToString().Split(',');
                if (filter.Contains("DATE"))
                {
                    if (txtDate.Text == string.Empty)
                    {
                        txtCurGridId.Text = "3";
                        txtCurGridFieldId.Text = FieldId;
                        txtCurGridObjecId.Text = OBJ3_fieldId;
                        txtCurGridPageId.Text = OBJ3_pageId;
                        callbackKTFMessage_Callback(null, null);
                        DatePopup.ShowOnPageLoad = true;
                    }
                    return;
                }
                ServiceUtilities.ProcSelectedRows(grid3, rows, _obj.UserName, _obj.Comp);

                MessageBox("İşlem Başarıyla Tamamlanmıştır.");

            }
            else
            {
                this.Response.StatusCode = 500;
                this.Response.End();
            }
        }

         

        protected void BtnProc_Click(object sender, EventArgs e)
        {

            if (txtDate.Text == string.Empty)
            {
                MessageBox("Lütfen Tarih alanını doldurunuz.");
                return;
            }


            DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(txtCurGridPageId.Text, txtCurGridObjecId.Text, _obj.Comp).Tables[0].Select("[DX Field ID]='" + txtCurGridFieldId.Text + "'");

            switch (txtCurGridId.Text)
            {
                case "1":
                    ServiceUtilities.ProcSelectedRows(grid, rows, _obj.UserName, _obj.Comp   );
                    grid.Selection.UnselectAll();
                    break;
                case "2":
                    ServiceUtilities.ProcSelectedRows(grid2, rows, _obj.UserName, _obj.Comp );
                    grid2.Selection.UnselectAll();
                    break;
                case "3": 
                    GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "KTFCreateInvoiceByAktarim§" + HiddenIslemId.Value + "§" + method.DateToStringNAVType(txtDate.Text) + "§" + _obj.UserName, _obj.Comp);
                     grid3.Selection.UnselectAll();
                    break;
            }
            txtDate.Text = string.Empty;
            MessageBox("Faturalar Başarıyla Oluşturulmuştur.");
            //Response.Redirect(Request.RawUrl);

        }

        protected void callbackKTFMessage_Callback(object sender, CallbackEventArgsBase e)
        {
            Guid IslemId = Guid.NewGuid();
            HiddenIslemId.Value = IslemId.ToString();
            DataRow[] rows = webObjectHandler.GetDynamicWebPageObjects(OBJ3_pageId, OBJ3_fieldId, _obj.Comp).Tables[0].Select("[DX Field ID]='" + txtCurGridFieldId.Text + "'");
            ServiceUtilities.ProcSelectedRows(grid3, rows, _obj.UserName, _obj.Comp);
            txtKTFmessage.Text = GetWebFonksiyonlari(CustomUserSirketAdi).ExecGenericWebFunctionAllComp(_obj.UserName,"50000", "KTFInvoicesPreviewByAktarim§" + IslemId.ToString() + "§" + _obj.UserName  , _obj.Comp);
             
        }
    }
}